import numpy as np
import matplotlib.pyplot as plt
import auxiliary_functions as aux
import itertools
import math


def getAxesList(
    DataToPlotShapeList,
    width_colorbar_horizontal=0.04,
    width_colorbar_vertical=0.04,
    length_colorbar_vertical_percent=0.8,
    length_colorbar_horizontal_percent=0.8,
    length_colorbar_above_percent=0.8,
    width_colorbar_above=0.04,
    skip_colorbars=None,
    skip_axes=None,
    colorbar_multi_row_column=False,
    hspace=0,
    right=0.02,
    left=0.02,
    bottom=0.12,
    top=0.02,
    colorbar_vertical_offset=0.02,
    colorbar_horizontal_offset=0.02,
    colorbar_above_offset=0.02,
    wspace=0.0,
    figsizex="A4",
    colorbarVertical=False,
    SingleColorbar=False,
    ShowAxisTicks=False,
    show_colorbar_above=True,
    show_colorbar=True,
    single_colorbar_above=False,
    dpi=440,
):
    """
    DataToPlotShapeList: e.g. [[[64,24],[64,24],[64,24]],[[64,24],[64,24],[64,24]]]
    -> will give two columns with three rows

    returns:
        fig, Axes, AxesColorbar, axes_colorbar_above
    """
    if colorbar_multi_row_column:
        if colorbarVertical:
            if wspace < width_colorbar_vertical:
                wspace = width_colorbar_vertical * 2
        else:
            if hspace < width_colorbar_horizontal:
                hspace = width_colorbar_horizontal * 2
    NumberPlotsY = np.shape(DataToPlotShapeList)[1]
    NumberPlotsX = np.shape(DataToPlotShapeList)[0]
    if colorbarVertical:
        width_colorbar_horizontal = 0
        colorbar_horizontal_offset = 0
    else:
        width_colorbar_vertical = 0
        colorbar_vertical_offset = 0
    if not show_colorbar_above:
        width_colorbar_above = 0
        colorbar_above_offset = 0
    if not show_colorbar:
        width_colorbar_horizontal = 0
        width_colorbar_vertical = 0
        colorbar_horizontal_offset = 0
        colorbar_vertical_offset = 0
    aux.print_dic(locals())
    print(f"skip_colorbars: {skip_colorbars}")
    if skip_colorbars is None:
        skip_colorbars = []
    if skip_axes is None:
        skip_axes = []
    if skip_axes and colorbar_multi_row_column or single_colorbar_above:
        skip_axes = make_pairs(skip_axes, x_size=NumberPlotsX, y_size=NumberPlotsY)
    if skip_colorbars and colorbar_multi_row_column:
        skip_colorbars = make_pairs(
            skip_colorbars, x_size=NumberPlotsX, y_size=NumberPlotsY
        )
    print(f"skip_colorbars: {skip_colorbars}")
    print(f"skip_axes: {skip_axes}")
    # exit()
    # relative size of image dimension to be plotted, have to be of same shape in row!!!
    Nx = [DataToPlotShapeList[0][i][1] for i in range(NumberPlotsY)]
    Ny = [DataToPlotShapeList[0][i][0] for i in range(NumberPlotsY)]

    sizex = (
        1
        - (
            left
            + right
            + (NumberPlotsX - 1) * wspace
            + width_colorbar_vertical  # * (1 + (NumberPlotsX-1) * colorbar_multi_row_column)
            + colorbar_vertical_offset  # * (1 + (NumberPlotsX-1) * colorbar_multi_row_column)
        )
    ) / NumberPlotsX
    sizey = [(sizex * np.float128(NY) / np.float128(NX)) for NY, NX in zip(Ny, Nx)]
    sizey = np.float128(sizey)
    RevCumSizeY, TotalSizeY = _get_total_rever_size(sizey)
    if type(figsizex) == str:
        figsizex_value = 1
    else:
        figsizex_value = figsizex
    figsizey = (
        TotalSizeY
        * figsizex_value
        / (
            1
            - (
                width_colorbar_above
                + colorbar_above_offset
                + width_colorbar_horizontal  # * (1 + (NumberPlotsX-1) * colorbar_multi_row_column)
                + bottom
                + top
                + colorbar_horizontal_offset  # * (1 + (NumberPlotsX-1) * colorbar_multi_row_column)
                + hspace * (NumberPlotsY - 1)
            )
        )
    )
    print(f"figsizey: {figsizey}")
    if figsizex == "A4":
        if figsizey / figsizex_value > 11.69 / 8.27:
            figsizex_value = figsizex_value / figsizey * 11.69
            figsizey = 11.69
        else:
            figsizey = figsizey / figsizex_value * 8.27
            figsizex_value = 8.27
    WidthPixel = round(figsizey * dpi)
    if WidthPixel % 2:
        WidthPixel += 1
    figsizey = WidthPixel / dpi
    fig = plt.figure(figsize=np.array([figsizex_value, figsizey]), dpi=dpi)
    print(f"figsize, figsizey, dpi {figsizex} {figsizey} {dpi}")
    print(f"actual dpi: {fig.get_dpi()}")
    # scaleY = figsizex / figsizey
    scaleY = 1.0
    sizey = (
        (
            1
            - (
                width_colorbar_above
                + colorbar_above_offset
                + width_colorbar_horizontal  # * (1 + (NumberPlotsX-1) * colorbar_multi_row_column)
                + bottom
                + top
                + colorbar_horizontal_offset  # * (1 + (NumberPlotsX-1) * colorbar_multi_row_column)
                + hspace * (NumberPlotsY - 1)
            )
        )
        * sizey
        / np.sum(sizey)
    )  # * figsizey/figsizex
    RevCumSizeY, TotalSizeY = _get_total_rever_size(sizey)
    Axes = [[[] for y in range(NumberPlotsY)] for x in range(NumberPlotsX)]
    AxesColorbar = None
    if SingleColorbar and colorbar_multi_row_column:
        print("using colorbar_multi_row_column mode for single colorbar!")
        colorbar_multi_row_column = False
    if show_colorbar:
        if not colorbar_multi_row_column:
            if SingleColorbar:
                AxesColorbar = [[]]
            elif colorbarVertical:
                AxesColorbar = [[] for x in range(NumberPlotsY)]
            else:
                AxesColorbar = [[] for x in range(NumberPlotsX)]
            if not SingleColorbar:
                length_colorbar_horizontal = sizex * length_colorbar_horizontal_percent
                length_colorbar_vertical = sizey * length_colorbar_vertical_percent
            else:
                length_colorbar_horizontal = (
                    sizex * NumberPlotsX + wspace * (NumberPlotsX - 1)
                ) * length_colorbar_horizontal_percent
                length_colorbar_vertical = TotalSizeY * length_colorbar_vertical_percent
        else:
            AxesColorbar = [
                [[] for y in range(NumberPlotsY)] for x in range(NumberPlotsX)
            ]
            length_colorbar_horizontal = sizex * length_colorbar_horizontal_percent
            length_colorbar_vertical = sizey * length_colorbar_vertical_percent
    axes_colorbar_above = None
    skip_right = 0
    skip_left = 0
    if show_colorbar_above:
        if single_colorbar_above:
            if skip_colorbars and max(skip_colorbars) > 0:
                skip_left = max(skip_colorbars)
            if skip_colorbars and min(skip_colorbars) < 0:
                skip_right = abs(min(skip_colorbars))
            # give number of axes skipped on left with positive number and skipped ones from right with negative number in: skip_colorbars
            length_colorbar_above = length_colorbar_above_percent * sizex * (
                NumberPlotsX - len(skip_colorbars)
            ) + wspace * max(0, NumberPlotsX - len(skip_colorbars) - 1)
            axes_colorbar_above = [[]]
        else:
            length_colorbar_above = sizex * length_colorbar_above_percent
            axes_colorbar_above = [[] for x in range(NumberPlotsX)]

    for Y in range(NumberPlotsY):
        for X in range(NumberPlotsX):
            if [X, Y] in skip_axes:
                continue
            Axes[X][Y] = fig.add_axes(
                [
                    left + sizex * X + wspace * X,
                    (
                        width_colorbar_horizontal  # * (NumberPlotsY - Y)
                        + RevCumSizeY[NumberPlotsY - Y - 1]
                        + (hspace) * (NumberPlotsY - Y - 1)
                        + bottom
                        + colorbar_horizontal_offset  # * (NumberPlotsY - Y)
                    )
                    * scaleY,
                    sizex,
                    sizey[Y] * scaleY,
                ]
            )  # top left
    if show_colorbar and not colorbar_multi_row_column:
        if not colorbarVertical:
            for X in range(NumberPlotsX):
                if X in skip_colorbars and not SingleColorbar:
                    continue
                AxesColorbar[X] = fig.add_axes(
                    [
                        left
                        + wspace * X
                        + (
                            sizex * (1 + SingleColorbar * (NumberPlotsX - 1))
                            + (wspace * (SingleColorbar * (NumberPlotsX - 1)))
                            - length_colorbar_horizontal
                        )
                        / 2
                        + (sizex) * (X),
                        bottom * scaleY,
                        length_colorbar_horizontal,
                        width_colorbar_horizontal * scaleY,
                    ]
                )
                if SingleColorbar and 0 == X:
                    break
        else:
            for X in range(NumberPlotsY):
                if X in skip_colorbars and not SingleColorbar:
                    continue
                AxesColorbar[X] = fig.add_axes(
                    [
                        left + NumberPlotsX * sizex + colorbar_vertical_offset,
                        (
                            bottom
                            + (
                                sizey[NumberPlotsY - X - 1]
                                - length_colorbar_vertical[NumberPlotsY - X - 1]
                            )
                            / 2
                            + hspace * (NumberPlotsY - X - 1)
                            + RevCumSizeY[NumberPlotsY - X - 1]
                        )
                        * scaleY,
                        width_colorbar_vertical,
                        length_colorbar_vertical[NumberPlotsY - X - 1] * scaleY,
                    ]
                )
                if SingleColorbar and 0 == X:
                    break
    print(f"RevCumSizeY: {RevCumSizeY}")
    if show_colorbar and colorbar_multi_row_column:
        for Y in range(NumberPlotsY):
            for X in range(NumberPlotsX):
                if [X, Y] in skip_colorbars:
                    continue
                if not colorbarVertical:
                    AxesColorbar[X][Y] = fig.add_axes(
                        [
                            left
                            + wspace * X
                            + (sizex - length_colorbar_horizontal) / 2
                            + (sizex) * (X),
                            (
                                bottom
                                + int(Y != NumberPlotsY - 1)
                                * (
                                    width_colorbar_horizontal
                                    + colorbar_horizontal_offset
                                )
                                - int(Y != NumberPlotsY - 1)
                                * (
                                    colorbar_horizontal_offset
                                    + width_colorbar_horizontal
                                )
                                + RevCumSizeY[NumberPlotsY - Y - 1]
                                + (hspace) * (NumberPlotsY - Y - 1)
                            )
                            * scaleY,
                            length_colorbar_horizontal,
                            width_colorbar_horizontal * scaleY,
                        ]
                    )
                else:
                    AxesColorbar[X][Y] = fig.add_axes(
                        [
                            left
                            + (sizex) * (X + 1)
                            + wspace * X
                            + colorbar_vertical_offset,
                            (
                                bottom
                                + (
                                    sizey[NumberPlotsY - Y - 1]
                                    - length_colorbar_vertical[NumberPlotsY - Y - 1]
                                )
                                / 2
                                + hspace * (NumberPlotsY - Y - 1)
                                + RevCumSizeY[NumberPlotsY - Y - 1]
                            )
                            * scaleY,
                            width_colorbar_vertical,
                            length_colorbar_vertical[NumberPlotsY - Y - 1] * scaleY,
                        ]
                    )
    print(f"width_colorbar_horizontal: {width_colorbar_horizontal}")
    print(f"colorbar_horizontal_offset: {colorbar_horizontal_offset}")
    print(f"colorbar_above_offset: {colorbar_above_offset}")
    # exit()
    if show_colorbar_above:
        for X in range(NumberPlotsX):
            if single_colorbar_above:
                from_left = left + (sizex) * (X + skip_left)
            else:
                from_left = (
                    left
                    + (
                        sizex
                        * (
                            1
                            + single_colorbar_above
                            * (NumberPlotsX - skip_left - skip_right - 1)
                        )
                        - length_colorbar_above
                    )
                    / 2
                    + (sizex) * (X + skip_left)
                )

            axes_colorbar_above[X] = fig.add_axes(
                [
                    from_left,
                    (
                        bottom
                        + TotalSizeY
                        + width_colorbar_horizontal
                        + colorbar_horizontal_offset
                        + hspace * (NumberPlotsY - 1)
                        + colorbar_above_offset
                    )
                    * scaleY,
                    length_colorbar_above,
                    width_colorbar_above * scaleY,
                ]
            )
            if single_colorbar_above and 0 == X:
                break

    if not ShowAxisTicks:
        for Y in range(NumberPlotsY):
            for X in range(NumberPlotsX):
                Axes[X][Y].axes.get_xaxis().set_ticks([])
                Axes[X][Y].axes.get_yaxis().set_ticks([])

    return fig, Axes, AxesColorbar, axes_colorbar_above


def _get_total_rever_size(sizey):
    TotalSizeY = np.sum(sizey, dtype=np.float128)
    Reverse = list(reversed(sizey))
    Reverse = [0] + Reverse
    RevCumSizeY = np.cumsum(Reverse, dtype=np.float128)
    return RevCumSizeY, TotalSizeY


def make_pairs(pairs, x_size, y_size):
    r = []
    for x, y in pairs:
        if x == -1:
            x = x_size - 1
        elif x == "all":
            x = np.arange(x_size)
        elif type(x) == str:
            if ":" in x:
                x1, x2 = [int(i) for i in x.split(":")]
                x = np.arange(x1, x2)
        if y == -1:
            y = y_size - 1
        elif y == "all":
            y = np.arange(y_size)
        elif type(y) == str:
            if ":" in y:
                y1, y2 = [int(i) for i in y.split(":")]
                y = np.arange(y1, y2)
        list1 = aux.make_iterable(x, list)
        list2 = aux.make_iterable(y, list)
        if len(list1) == 1:
            r.extend([[x, y] for x, y in zip(list1 * len(list2), list2)])
        else:
            r.extend(
                [
                    list(list(zip(each_permutation, list2))[0])
                    for each_permutation in itertools.permutations(list1, len(list2))
                ]
            )
    return r


def get_xy_shape(numbers, numbers_x_min=3):
    # return numbers_x, numbers_y
    if numbers == 0:
        raise Exception("can't create xyshape for 0 plots!")
    elif numbers <= numbers_x_min:
        numbers_x = numbers
        numbers_y = 1
    else:
        numbers_y = math.ceil(numbers ** (1 / 2.0))
        numbers_x = math.ceil(numbers / numbers_y)
    return numbers_x, numbers_y


def determine_xy_index(condition, index_1, index_2):
    if condition:
        return index_1, index_2
    else:
        return index_2, index_1


if __name__ == "__main__":
    x_size = 7
    y_size = 8
    x_n = 3
    y_n = 1
    shape_list = [[[x_size, y_size] for i in range(y_n)] for j in range(x_n)]
    space = 0.08
    space_below = 0.25
    fig, Axes, AxesColorbar, axes_colorbar_above = getAxesList(
        shape_list,
        show_colorbar_above=False,
        show_colorbar=False,
        left=space,
        right=0.02,
        hspace=space_below,
        wspace=space,
        bottom=space_below,
        ShowAxisTicks=True,
    )
    print(np.shape(Axes))
    for i in range(y_n):
        for j in range(x_n):
            x = np.linspace(0, 1, 100)
            ax = Axes[j][i]
            ax.plot(x, x ** 2)
            ax.set_ylabel(r"this is the y label")
            ax.set_xlabel(r"this is the x label")
    fig.savefig("testaxes.pdf")
    for i in range(1, 20):
        print(i, get_xy_shape(i))
