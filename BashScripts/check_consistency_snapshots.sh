#!/bin/bash
#input FOLDER1 FOLDER2
#use listed snaps in folder 1 to compare to files with same name in folder 2
#thus folder 1 should be the freshly copied files
#
CWD=$(pwd)
REDO=0

#check if folders exist
function main(){
if [[ ! -d "$1" ]] || [[ ! -d "$2" ]] || [[ "$#" -ne 2 ]]; then
#check if sufficient arguments supplied
    if [[ "$#" -ne 2 ]]; then
	echo "Number of arguments supplied: $#, but need 2!"
    else
	if [[ ! -d "$1" ]]; then
	    echo "$1 doesnt exist!"
	fi
	if [[ ! -d "$2" ]]; then
	    echo "$2 doesnt exist!"
	fi
     fi
else
    FOLDER1="$(readlink -f $1)/"
    FOLDER2="$(readlink -f $2)/"
#move to folder to create
#FolderExt='snapdir_*/snap_*' 
    FolderExt='snap_*.hdf5' 
#FileExt='/snap_*' 
    logfile='logfile.txt'
    combined=$FOLDER1$FolderExt
    diffFpath=$FOLDER1
#diffFile rename or remove old file if exists!
    diffFname='diffFolder.txt'
    diffFnameHits='diffFolderHits.txt'
    diffFile=$diffFpath$diffFname
    echo "Comparing snap files in $FOLDER1 with snap files in $FOLDER2!"
    if [ -f "$diffFile" ]; then
	echo "$diffFile exist"
	echo "remove file or rename diffFname: $diffFile!"
	echo "You want to remove file (y/n)? or rename file (name)?"
	read -p "(y,n,name): " dealWithFile
	if [ "$dealWithFile" = "y" ]
	then
	    echo "remove log file"
	    rm "$diffFile"
	    REDO=1
	elif ([ "$dealWithFile" = "n" ] || [ "$dealWithFile" = "" ])
	then
	    echo "nothing deleted/renamed"
	else
	    echo "renamed diffFile => $dealWithFile"
	    mv $diffFile "$dealWithFile"
	    REDO=1
	fi
    else
#echo $combined
	cd $FOLDER1
	FilesToCheck=$( find . -name "$FolderExt" )
	Ncount=$(wc -w <<< "$FilesToCheck")
	n=0
	echo "Comparing snap files in $FOLDER1 with snap files in $FOLDER2!" >> diffFile
	for i in $FilesToCheck; do
	    n=$(expr $n + 1)
	    echo "working on file $n/$Ncount"
	    echo "working on $FOLDER1$i" >> $diffFile
	    echo `md5sum $i` > $FOLDER1$logfile
	    cd $FOLDER2
	    LOG2="$(md5sum $i)"
	    echo $LOG2 > $FOLDER2$logfile
#check if file doesnt exist in folder 2
	    if [[ "$LOG2" == "" ]]; then
		echo " couldn find file"
		echo "NOTFOUND $FOLDER2$i" >> $diffFile
	    else
		diff $FOLDER2$logfile $FOLDER1$logfile >> $diffFile
	    fi
	    cd $FOLDER1	    
	done
	echo "The following files were not copied correctly:" > $diffFnameHits
	cat $diffFile | grep -B 2 '<' | grep 'working' | awk '{ printf("%s\n", $3) }' >> $diffFnameHits
	echo "The following files could not be found:" >> $diffFnameHits
	cat $diffFile | grep 'NOTFOUND' | awk '{ printf("%s\n", $2) }'  >> $diffFnameHits
	#cat $diffFile
	cat $diffFnameHits
    fi
fi
cd $CWD
}

main $1 $2
if [ $REDO -eq 1 ]; then
    main $1 $2
fi
