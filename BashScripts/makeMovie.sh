folder=/home/kristian/analysis/figures/MultiProjections/coldjetcrash
part0=VaVeSoSpRa_Nu
part1=_FoJeCCNoMiVo-NoMiVo-NoMiVoR6-R5CCTNGEf0p001-R6JeCCNoMiVo_H100_W100_D4_R320_A0-2_PlSldpi500
filename=JeCCNoMiVo-NoMiVo-NoMiVoR6-R5CCTNGEf0p001-R6JeCCNoMiVo_HW100
convPdfPngSplit ${folder}/${part0} ${part1} 1000 10

makeMovieSplit ${folder}/${part0} ${part1} $filename 6
