import clipTut
import numpy as np

array_1 = np.random.uniform(0, 1000, size=(3000, 2000)).astype(np.double)
array_2 = np.random.uniform(0, 1000, size=(3000, 2000)).astype(np.double)
a = 4
b = 3
c = 9


def compute_np(array_1, array_2, a, b, c):
    return np.clip(array_1, 2, 10) * a + array_2 * b + c


import timeit

mysetup = "import clipTut"
myCode = """\
clipTut.compute(array_1, array_2, a, b, c) 
"""
N = 100
print(
    "cython:",
    timeit.timeit(setup=mysetup, stmt=myCode, number=N, globals=globals()) / N,
)

mysetup = " "
myCode = """\
compute_np(array_1, array_2, a, b, c)
"""
N = 100
print(
    "numpy:", timeit.timeit(setup=mysetup, stmt=myCode, number=N, globals=globals()) / N
)
print(compute_np(array_1, array_2, a, b, c))
