import matplotlib as mpl
import os
import SuperClasses

# mpl.use("Qt5Agg")
# BACKEND = os.environ.get("BACKEND")
# if BACKEND is None:
#     print("no Backend specified, defaulting to Agg")
#     BACKEND = "Agg"
# mpl.use(BACKEND)
import debug

mpl.use("Agg")
mpl.rcParams["text.usetex"] = True
import os.path as path
import numpy as np

try:
    from matplotlib import pyplot as plt, font_manager
except:
    print("plt and font_manager couldnt be imported")
try:
    import gadget
except:
    print("arepo not imprted")
import auxiliary_functions as aux
import time as TimeModule
import copy
import scipy.stats
import FitUtils as Fit
import multiprocessing as mp
import Multithread as mt
import sys
import Param as param
import matplotlib.patheffects as path_effects
import math
import re
import warnings
import multiprocessing as mp


def compactSingleImshow(
    Data,
    Extent,
    Time=None,
    log=False,
    vmin=None,
    vmax=None,
    colormap="jet",
    figsizex=10,
    deltamx=0.0,
    deltamy=0.0,
    deltax=0.03,
    deltayUp=0.03,
    deltayDown=0.07,
    heightcolorbar=0.02,
    cbarLabel=r"$\rho$",
    fontsize=16,
    figname="../figures/text.png",
    plotContour=False,
    ContourVariable="JetTracer",
    levels=[1e-3, 1e-2],
    axis=[2, 0],
    box=[0.1, 0.12],
    MultiMasks=True,
    howmanyX=3,
    howmanyY=5,
    center=None,  # [0.5 / 0.67] * 3,
    noTicks=True,
    note="note here",
    showNote=False,
    noteColor="black",
    TicksRelativeToCenter=False,
    colors="black",
    labelsizeTicks=8,
):
    shape = np.shape(Data)
    NumberImages = shape[0]
    # NumberImages = 3
    # relative size of image dimension to be plotted, have to be of same shape!!!
    Ny, Nx = np.shape(Data[0])
    # Ny, Nx = 40,20

    sizex = (1 - (2 * deltax + (NumberImages - 1) * deltamx)) / NumberImages
    sizey = sizex * float(Ny) / float(Nx)

    figsizey = (sizey + heightcolorbar + deltayDown + deltayUp + deltamy) * figsizex
    fig = plt.figure(figsize=np.array([figsizex, figsizey]))
    scaleY = figsizex / figsizey
    Axes = {}
    for index in range(NumberImages):
        Axes[index] = plt.axes(
            [
                deltax + sizex * index + deltamx * index,
                (heightcolorbar + deltamy + deltayDown) * scaleY,
                sizex,
                sizey * scaleY,
            ]
        )  # top left
    axColorbar = plt.axes(
        [deltax, deltayDown * scaleY, 1 - deltax * 2, heightcolorbar * scaleY]
    )  # top left
    import matplotlib as mpl

    for index, (data, ext) in enumerate(zip(Data, Extent)):
        if vmax != 0 and log:
            img = Axes[index].imshow(
                data,
                cmap=colormap,
                origin="lower",
                aspect="auto",
                extent=ext,
                norm=mpl.colors.LogNorm(vmin=vmin, vmax=vmax),
            )  # mpl.cm.CMRmap
        else:
            img = Axes[index].imshow(
                data,
                cmap=colormap,
                origin="lower",
                aspect="auto",
                extent=ext,
                vmin=vmin,
                vmax=vmax,
            )  # , norm=mpl.colors.LogNorm(v
        if plotContour:
            if not MultiMasks:
                ContourVariable[index] = [ContourVariable[index]]
            NContours = np.shape(ContourVariable[index])[0]
            colors = makeIterable(colors, NContours, List=True)
            for indexCont, contourVar in enumerate(ContourVariable[index]):
                CS = Axes[index].contour(
                    contourVar[0],
                    contourVar[1],
                    contourVar[2],
                    levels,
                    colors=colors[indexCont],
                    linewidths=1,
                )
                print("using color and levels for compact single imshow:")
                print(colors, levels)
        if showNote:
            Axes[0].text(
                0.1,
                0.9,
                note,
                fontsize=fontsize,
                horizontalalignment="left",
                color=noteColor,
                transform=Axes[index].transAxes,
            )

        if Time is not None:
            Axes[0].text(
                0.1,
                0.9,
                Time[index],
                fontsize=fontsize,
                horizontalalignment="left",
                color=noteColor,
                transform=Axes[index].transAxes,
            )
    centerBox = None  # [0.5 / 0.67 for x in range(3)]
    if TicksRelativeToCenter:
        centerBox = center

    def myFormatterX(x, p):
        return "%.0f" % ((x - centerBox[axis[0]]) * 1.0e3)

    def myFormatterY(x, p):
        return "%.0f" % ((x - centerBox[axis[1]]) * 1.0e3)

    for index in range(NumberImages):
        if noTicks:
            Axes[index].axes.get_xaxis().set_ticks([])
            Axes[index].axes.get_yaxis().set_ticks([])
        else:
            import matplotlib
            from matplotlib.ticker import FuncFormatter

            xtickLabelsCurrent = getCenteredTickLabels(
                center[axis[0]] - 0.5 * box[0],
                center[axis[0]] + 0.5 * box[0],
                howmanyX,
                centered=False,
            )
            ytickLabelsCurrent = getCenteredTickLabels(
                center[axis[1]] - 0.5 * box[1],
                center[axis[1]] + 0.5 * box[1],
                howmanyY,
                centered=False,
            )
            Axes[index].set_xticks(xtickLabelsCurrent)
            Axes[index].set_yticks(ytickLabelsCurrent)

            Axes[index].xaxis.set_major_formatter(FuncFormatter(myFormatterX))
            Axes[index].yaxis.set_major_formatter(FuncFormatter(myFormatterY))
            Axes[index].set_xlim(
                center[axis[0]] - 0.5 * box[0], center[axis[0]] + 0.5 * box[0]
            )
            Axes[index].set_ylim(
                center[axis[1]] - 0.5 * box[1], center[axis[1]] + 0.5 * box[1]
            )
            #            Axes[index].axes.tick_params(axis='x', colors=noteColor, direction='in', length=0,labelbottom = True, labeltop =True, labelleft=True, labelright=True,pad=-12)
            Axes[index].axes.tick_params(
                axis="y",
                colors=noteColor,
                direction="in",
                length=0,
                labelleft=True,
                labelright=True,
                pad=-2 * labelsizeTicks,
                labelsize=labelsizeTicks,
            )
            Axes[index].axes.tick_params(
                axis="x",
                colors=noteColor,
                direction="in",
                length=0,
                labelbottom=True,
                labeltop=True,
                pad=-1.3 * labelsizeTicks,
                labelsize=labelsizeTicks,
            )

    if log:
        cbar = plt.colorbar(
            img,
            cax=axColorbar,
            format=mpl.ticker.LogFormatterMathtext(),
            orientation="horizontal",
        )
    else:
        cbar = plt.colorbar(
            img, cax=axColorbar, orientation="horizontal", format="%.0e"
        )

    cbar.ax.tick_params(labelsize=fontsize)
    # cbar.ax.get_xaxis().labelpad = -2
    cbar.ax.set_xlabel(cbarLabel, rotation=0, fontsize=fontsize)

    aux.create_dirs_for_file(figname)
    print("... saving figure %s" % figname)
    aux.checkExistence_delete_file(figname)

    fig.savefig(figname, dpi=300)
    return fig


def CalcLogSin(x, method="log10Sign"):
    print("using method %s to calculate log quiver!" % method)
    # exit()
    if method == "log10Sign":
        return log10Sign(x)
    elif method == "tanh":
        return tanh(x)


def tanh(x):
    return np.tanh(x)


def log10Sign(x):
    return np.sign(x) * np.log10(np.abs(x)) ** 3


def symlog(x, y, norm=None, giveNormalisation=True, method="log10Sign"):
    x = copy.deepcopy(x)
    y = copy.deepcopy(y)
    """ Returns the symmetric log10 value """
    if giveNormalisation:
        norm = np.min([np.min(abs(x)), np.min(abs(y))]) * 1.0e-2
    angles = np.arctan2(y, x) * 180.0 / np.pi  # calculate angles manually
    x /= norm
    y /= norm
    if np.min(abs(x)) < 1 or np.min(abs(y)) < 1:
        print(np.min(abs(x)), np.min(abs(y)))
        print("normalisaton too low")

    print(np.min(abs(x)), np.min(abs(y)))
    print(np.max(abs(x)), np.max(abs(y)))
    print(np.min(CalcLogSin(x, method=method)), np.min(CalcLogSin(y, method=method)))
    print(np.max(CalcLogSin(x, method=method)), np.max(CalcLogSin(y, method=method)))

    # exit()
    if giveNormalisation:
        scaleFactor = (
            np.average(
                np.sqrt(
                    CalcLogSin(x, method=method) ** 2
                    + CalcLogSin(y, method=method) ** 2
                )
            )
            / (1.0 / np.shape(x)[1])
            * 1.8
        )

    if not giveNormalisation:
        return CalcLogSin(x, method=method), CalcLogSin(y, method=method), angles
    else:
        return (
            CalcLogSin(x, method=method),
            CalcLogSin(y, method=method),
            angles,
            norm,
            scaleFactor,
        )


def getCenteredTickLabels(min, max, howmany, centered=True):
    if howmany % 2 == 0:
        exit("need odd number of ticklabels for centered ticklabels")
    elif type(howmany) != int:
        exit("number of ticks should be integer")
    value = (max - min) / float(howmany)
    DistanceBetweenTicks = round(value, 3)
    if centered:
        ticklabels = DistanceBetweenTicks * (np.arange((howmany)) - ((howmany - 1)) / 2)
    else:
        ticklabels = (
            DistanceBetweenTicks * (np.arange(howmany) - (howmany - 1) / 2)
            + min
            + (max - min) / 2.0
        )
    return ticklabels


def get_Extent(center, axis, box):
    if np.size(box) == 3:
        return (
            center[axis[0]] - box[axis[0]] * 0.5,
            center[axis[0]] + (box[axis[0]]) * 0.5,
            center[axis[1]] - (box[axis[1]]) * 0.5,
            center[axis[1]] + (box[axis[1]]) * 0.5,
        )
    elif np.size(box) == 2:
        return (
            center[axis[0]] - box[0] * 0.5,
            center[axis[0]] + (box[0]) * 0.5,
            center[axis[1]] - (box[1]) * 0.5,
            center[axis[1]] + (box[1]) * 0.5,
        )
    else:
        exit("box not correct size for extent")
        return 0


def setLogTicks(
    vmin, vmax, numberMax=4, minormajorOverlap=False, returnOnlyMajorTicks=False
):
    if vmin < 0 or vmax < 0:
        print("Negative value in log ticks; vmin: %g; vmax: %g" % (vmin, vmax))
    powerMin = math.ceil(np.log10(vmin))
    powerMax = math.floor(np.log10(vmax))
    number = powerMax - powerMin
    number += 1
    spacing = 1
    Maxnumber = number
    while number > numberMax:
        spacing += 1
        number = Maxnumber / float(spacing)
    print(number, spacing, powerMax, powerMin)
    majorticks = 10 ** (np.arange(powerMin, powerMax + 1, spacing, dtype=np.float128))
    # offset might be necessary due to rounding errors
    print(f"vmin {vmin}, vmax {vmax}, powerMin {powerMin}")
    firstDigitMin = math.ceil((vmin - vmin * 1e-7) / 10.0 ** (powerMin - 1))
    firstDigitMax = math.floor(vmax / 10.0 ** (powerMax))
    if firstDigitMin > 10:
        firstDigitMin = 1
    elif firstDigitMin == 10:
        firstDigitMin = 1
        powerMin += 1
    minorticks = np.arange(
        firstDigitMin * 10 ** (powerMin - 1),
        10 ** powerMin,
        10 ** (powerMin - 1),
        dtype=np.float128,
    )
    newpower = powerMin
    if np.max(minorticks) > vmax:
        minorticks = np.arange(
            firstDigitMin * 10 ** powerMax,
            firstDigitMax * 10 ** powerMax,
            10 ** powerMax,
            dtype=np.float128,
        )
    else:
        while newpower <= powerMax - 1:
            MoreTicks = np.arange(10 ** newpower, 10 ** (newpower + 1), 10 ** newpower)
            minorticks = np.concatenate((minorticks, MoreTicks))
            newpower += 1
        EndTicks = np.arange(
            10 ** powerMax,
            (firstDigitMax) * 10 ** (powerMax) + 10 ** (powerMax - 1),
            10 ** (powerMax),
            dtype=np.float128,
        )

        minorticks = np.concatenate((minorticks, EndTicks))
    if not minormajorOverlap:
        minorticks = np.array([x for x in minorticks if x not in majorticks])
    if returnOnlyMajorTicks:
        return majorticks
    return majorticks, minorticks


def setLinTicksAlt(vmin, vmax, numberMax=8, symmetricAroundZero=False):
    # test: 2000 10000
    if vmin == vmax:
        return [vmin - 1, vmin, vmin + 1]
    if vmin > vmax:
        print("WARNING: your vmin is larger than vmax!!!!")
        vmin, vmax = vmax, vmin
    vminNeg = False
    doubleNegative = False
    if vmin < 0 and vmax <= 0:
        doubleNegative = True
        vmin, vmax = np.abs(vmax), np.abs(vmin)
    elif vmin < 0:
        vminNeg = True
        vmin = abs(vmin)
    powerMax = math.floor(np.log10(vmax))
    firstDigitMax = math.floor(vmax / 10.0 ** (powerMax))
    if vmin == 0:
        firstDigitMin = 0
        powerMin = powerMax - 1
    else:
        powerMin = math.floor(np.log10(vmin + vmin * 1e-7))
        firstDigitMin = math.ceil((vmin - vmin * 1e-7) / 10.0 ** (powerMin))
    if firstDigitMin == 0:
        start = 0
    elif powerMin < powerMax:
        start = 10.0 ** (powerMax - 1)
    else:
        start = firstDigitMin * 10.0 ** (powerMax)
    if vminNeg:
        start = 0
        if not doubleNegative:
            numberMax = int((numberMax + 1) / 2)

    vmaxMethod1 = vmax
    vminMethod1 = vmin
    if vminNeg and not doubleNegative:
        if abs(vminMethod1) <= vmaxMethod1:
            vminMethod1 = 0
        else:
            vmaxMethod1 = abs(vminMethod1)
        vminMethod1 = 0
    for power in np.arange(powerMin, powerMax + 2):
        for increment in [
            0.1,
            0.125,
            0.15,
            0.2,
            0.25,
            0.3,
            0.35,
            0.4,
            0.5,
            0.6,
            0.7,
            0.75,
            0.8,
            0.9,
        ]:
            increment = increment * 10.0 ** float(power)
            # print 'increment',increment
            remainder = (vmaxMethod1 - vminMethod1) % increment
            # print 'vmax -vmin',vmaxMethod1-vminMethod1
            # print 'remainder',remainder
            if remainder < 1e-6 * increment or abs(remainder - increment) < 1e-6:
                seq = np.concatenate(
                    (
                        np.arange(vminMethod1, vmaxMethod1, increment),
                        np.array([vmaxMethod1]),
                    )
                )
                Nresult = np.size(seq)
                if Nresult <= numberMax:
                    ticks = seq
                    if vminNeg:
                        ticks = np.append(-1 * ticks[::-1], ticks[1:])
                    if doubleNegative:
                        ticks = ticks[::-1] * -1
                    return ticks

    incrementUse = None
    herebefore = False
    for i in range(int(vmax / 10.0 ** (powerMax - 1)) - 1):
        i += 1
        incrementCur = i * 10.0 ** (powerMax - 1)
        numberNecessary = np.size(
            np.arange(start, vmax + 1e-7 * vmax, incrementCur)
        )  # int((vmax-start)/incrementCur)
        if numberNecessary <= numberMax:
            if not herebefore:
                highestPossibleIncrement = incrementCur
                herebefore = True
                # if vmax%incrementCur<1e-7*vmax:
                #    incrementUse = incrementCur
                break
    if incrementUse is None:
        incrementUse = highestPossibleIncrement
    ticks = np.arange(start, vmax + incrementUse, incrementUse)
    if vminNeg:
        ticks = np.append(-1 * ticks[::-1], ticks[1:])

    if doubleNegative:
        ticks = ticks[::-1] * -1
    return ticks


@aux.timing
def quickImport(
    time,
    folder="./",
    FieldsToLoad=None,
    FieldsLoadExtra=[],
    useTime=False,
    rewriteTimes=False,
    onlyHeader=False,
    differenceTime=2,
    considering_h=False,
    IC=False,
    INFO=None,
    alignWithJet=False,
    loadonlytype=False,
    lazy_load=True,
    convAddit=True,
    loadParameterFile=True,
    loadConfigFile=True,
    verbose=False,
):
    if verbose:
        print(f"quickimport: time: {time}, folder: {folder}")
    if IC:
        print(
            f"lazy load somehow buggy for ICs: switching it off! loading folder: {folder}"
        )
        lazy_load = False
    if type(time) == str:
        if folder == "./" or folder is None:
            folder = time
        else:
            folder = "%s%s" % (folder, time)
        time = None
    snap = readSnap(
        folder,
        time,
        FieldsToLoad=FieldsToLoad,
        useTime=useTime,
        returnSnapNumber=False,
        rewriteTimes=rewriteTimes,
        onlyHeader=onlyHeader,
        differenceTime=differenceTime,
        FieldsLoadExtra=FieldsLoadExtra,
        loadonlytype=loadonlytype,
        lazy_load=lazy_load,
        convAddit=convAddit,
        loadParameterFile=loadParameterFile,
        loadConfigFile=loadConfigFile,
        v=verbose,
    )
    snap, time = getGeneralInformation(
        snap,
        convertedkpc=False,
        considering_h=considering_h,
        StringTime=True,
        Precision=None,
        Round10=False,
        Round=1,
        returnNotStringTime=False,
        IC=IC,
        onlyHeader=onlyHeader,
    )
    if alignWithJet:
        import SZTESTING as SZ

        snap, DistanceJetHeadBH = SZ.AlignSnapWithJet(
            snap,
            up=True,
            BH_pos=[snap.boxsize / 2.0] * 3,
            center=[snap.boxsize / 2.0] * 3,
            INFO=INFO,
        )
    return snap


def getGeneralInformation(
    snap,
    convertedkpc=False,
    considering_h=False,
    StringTime=True,
    Precision=None,
    Round10=False,
    Round=1,
    returnNotStringTime=False,
    IC=False,
    onlyHeader=False,
    latexTime=False,
    INFO=None,
    posFactor=None,
    verbose=False,
):
    if considering_h:
        raise Exception("old remnant!")
    if snap.hubbleparam == 0:
        considering_h = False
    if not IC:
        time = aux.get_Time(snap, considering_h=considering_h)
    else:
        time = 0
    notStringTime = copy.deepcopy(time)
    if Precision is not None:
        originalTime = np.copy(time)
        time = int(time) / Precision * Precision
        if abs(originalTime - time) > Precision * 0.5:
            time = time + Precision
    if Round10:
        if verbose:
            print("Initial Time %f" % time)
        time = "%g" % time
        time = float(time)
        time = int(
            round(
                time / 10 ** np.ceil((np.log10(time))), int(np.ceil(np.log10(time))) - 1
            )
            * 10 ** np.ceil(np.log10(time))
        )
        if verbose:
            print("Rounded time on 10 %f" % time)
        time = float(time)
    print(f"time: {time}")
    if StringTime:
        if not Round10:
            if not Precision:
                if latexTime:
                    time = "%.1f\\ \\mathrm{Myr}" % (round(time, Round))
                else:
                    if latexTime:
                        time = "%.1f\\ \\mathrm{Myr}" % (round(time, Round))
                    else:
                        time = "%.1f Myr" % (round(time, Round))
            else:
                if latexTime:
                    time = "%i\\ \\mathrm{Myr}" % time  # (round(time,Round))
                else:
                    time = "%i Myr" % time  # (round(time,Round))
        else:
            if latexTime:
                time = "%i\\ \\mathrm{Myr}" % time
            else:
                time = "%i Myr" % time
    # if convertedkpc and not onlyHeader:
    #   	snap.data['pos'][snap.data['type']==0][:] *= 1000.#to_unit(arepo.units.kpc)
    #   	snap.boxsize *= 1000.#.to_unit(arepo.units.kpc**3)
    #   	snap.data['vol'][:] *= 1000.**3#.to_unit(arepo.units.kpc**3)

    if considering_h and not onlyHeader:
        if not IC:
            snap.data["vol"][:] /= snap.hubbleparam ** 3
        else:
            import Param

            snap.data["vol"][:] /= Param.HubbleParam ** 3
    if posFactor and not onlyHeader:
        print("WARNING: multiplying pos, center, boxsize with factor ", posFactor)
        snap.pos *= posFactor
        snap.center *= posFactor
        snap.boxsize *= posFactor
        debug.printDebug(snap.pos[:, 0], "snap.pos[:,0]")
        print("snap.center, snap.boxsize", snap.center, snap.boxsize)
    if aux.checkExistenceKey(INFO, "SetGeneraloffSetXAxis", False):
        snap = do_AxisShift(snap, INFO)
    if IC and not onlyHeader:
        # have to correct for h's as mass imported with /h but rho needs *h**2 -> need factor h**3
        snap.data["rho"] = snap.data["mass"][snap.type == 0] * snap.hubbleparam ** 3
        if "vol" in snap.data.keys() and snap.data["vol"] is not None:
            snap.data["mass"][snap.type == 0] = (
                snap.data["rho"] * snap.data["vol"]
            )  # h's should be correct for mass then
    # if verbose:
    print("generalInfo time: ", time)
    if returnNotStringTime:
        return snap, time, notStringTime
    else:
        return snap, time


def do_AxisShift(snap, INFO, overallMask=None):
    offSetAxis = calcSetXAxis(INFO)
    AxisForOffset = aux.checkExistenceKey(INFO, "AxisForOffset", 0)
    if offSetAxis is None:
        offSetAxis = aux.checkExistenceKey(INFO, "offSetAxis", None)
    if offSetAxis is not None:
        snap = changePosRelAxis(
            snap, axis=AxisForOffset, offSet=offSetAxis, overallMask=overallMask
        )
    print(offSetAxis)
    return snap


def calcSetXAxis(INFO):
    maxAngle = aux.checkExistenceKey(INFO, "MaxAngleRotatedLOS", None)
    distBHJet = aux.checkExistenceKey(
        INFO, "OffsetsBHJets", None, duplicateList=2, duplicateAlt=0
    )
    rotateAlongLineOfSightAngle = aux.checkExistenceKey(
        INFO, "rotateAlongLineOfSightAngle", None
    )
    if maxAngle is None:
        return None
    else:
        offSetXAxis = np.zeros(2)
        if distBHJet is None:
            print(maxAngle, distBHJet)
            exit("need to specify distance to jets!")
        else:
            offSetXAxis[0] = distBHJet[0] * (1.0 - np.cos(maxAngle))
            offSetXAxis[1] = distBHJet[1] * (1.0 - np.cos(maxAngle))
            if rotateAlongLineOfSightAngle is not None:
                offSetXAxis[0] -= distBHJet[0] * (
                    1.0 - np.cos(rotateAlongLineOfSightAngle)
                )
                offSetXAxis[1] -= distBHJet[1] * (
                    1.0 - np.cos(rotateAlongLineOfSightAngle)
                )
                if any([x < 0 for x in offSetXAxis]):
                    print(offSetXAxis, INFO)
                    exit(
                        "shouldnt be negative, probably round off error in calcSetXAxis!"
                    )
            return offSetXAxis


def changePosRelAxis(snap, axis=0, offSet=None, overallMask=None):
    # offset 0 deals w/ upper values of position along axis
    # offset 1 deals w/ lower values of position along axis
    if axis is None or offSet is None:
        print("no cut relative to any axis")
        return snap
    if np.size(offSet) == 1:
        offSet = [offSet] * 2
    boxHalf = snap.boxsize / 2.0
    mask1 = np.logical_and(
        snap.data["pos"][snap.data["type"] == 0][:, axis] >= boxHalf,
        snap.data["pos"][snap.data["type"] == 0][:, axis] <= boxHalf + offSet[0],
    )
    mask2 = snap.data["pos"][snap.data["type"] == 0][:, axis] > boxHalf + offSet[0]
    mask3 = np.logical_and(
        snap.data["pos"][snap.data["type"] == 0][:, axis] < boxHalf,
        snap.data["pos"][snap.data["type"] == 0][:, axis] >= boxHalf - offSet[1],
    )
    mask4 = snap.data["pos"][snap.data["type"] == 0][:, axis] < boxHalf - offSet[1]
    if overallMask is not None:
        mask1 = np.logical_and(mask1, overallMask)
        mask2 = np.logical_and(mask2, overallMask)
        mask3 = np.logical_and(mask3, overallMask)
        mask4 = np.logical_and(mask4, overallMask)
    restDiff = np.zeros(2)
    restDiff[0] = boxHalf - offSet[0]
    restDiff[1] = boxHalf - offSet[1]
    if any([x < 0 for x in restDiff]):
        print(boxHalf, offSet, restDiff)
        exit("changing position cannot be done if boxhalf < offset!")
    snap.data["pos"][snap.data["type"] == 0][:, axis][mask1] += restDiff[0]
    snap.data["pos"][snap.data["type"] == 0][:, axis][mask2] -= offSet[0]
    snap.data["pos"][snap.data["type"] == 0][:, axis][mask3] -= restDiff[1]
    snap.data["pos"][snap.data["type"] == 0][:, axis][mask4] += offSet[1]
    print("cut relative to axis: %i w/ values %g and %g" % (axis, offSet[0], offSet[1]))
    return snap


def initialiseListNew(sizeX, sizeY, sizeZ):
    # returns empty list with sizes as specified in respective size, first array always x..z
    if sizeZ == 0:
        List = [[[] for y in range(sizeY)] for x in range(sizeX)]
    elif sizeY == 0 and sizeZ == 0:
        List = [[] for x in range(sizeX)]
    elif sizeX != 0 and sizeY != 0 and sizeZ != 0:
        List = [
            [[[] for z in range(sizeZ)] for y in range(sizeY)] for x in range(sizeX)
        ]
    else:
        print(sizeX, sizeY, sizeZ)
        exit("combintaion not expected in initialiseListNew")
    return List


def createTimeFile_old(
    folder, maxFiles=1000, considering_h=False, snapbase="snap_", snapdirbase="snapdir_"
):

    print("have to write time file!")
    Time = []
    File = []
    for i in range(maxFiles + 1):
        path = os.path.join(folder, f"{snapbase}%03d.hdf5" % i)
        pathAlt = os.path.join(
            folder, f"{snapdirbase}%03d/" % i, "{snapbase}%03d.0.hdf5" % i
        )
        if os.path.isfile(path) or os.path.isfile(pathAlt):
            snap = readSnap(folder, i, useTime=False, onlyHeader=True)
            time = aux.get_Time(snap, considering_h=considering_h)
            Time.append(time)
            File.append(i)

    if np.size(Time) == 0:
        print(folder)
        print("%ssnap_0%02d.hdf5" % (folder, i))
        print(pathAlt)
        raise Exception("couldnt extract times correctly!")
    Time = np.stack((File, Time), axis=1)
    np.savetxt(
        "%sTimeFile.txt" % folder, Time, fmt="%i %1.4e"
    )  # use exponential notation


def retrieve_time(number, folder):
    snap = readSnap(folder, number, useTime=False, onlyHeader=True)
    print(f"snap.time: {snap.time}")
    time = aux.get_Time(snap, considering_h=False)
    return np.array([number, time])


def createTimeFile(folder, snapbase="snap_", snapdirbase="snapdir_", verbose=False):
    if verbose:
        print("have to write time file!")
    numbers = get_number_snapshots(folder, snapbase=snapbase, snapdirbase=snapdirbase)
    print(f"numbers: {numbers}")
    global bd
    bd = mt.BigData(
        retrieve_time,
        [x for x in np.arange(len(numbers))],
        [numbers],
        1,
        None,
        "numpy",
        folder,
    )
    data = np.array(bd.do_multi(16))
    print(f"data: {data}")
    mask = np.argsort(data[:, 0])
    data = data[mask]
    data = np.array(data, dtype=object)
    data[:, 0] = np.array(data[:, 0], dtype=int)
    np.savetxt(os.path.join(folder, "TimeFile.txt"), data, fmt="%i %1.4e")


def load_time_file(
    folder,
    filename="TimeFile.txt",
    check_completion=True,
    maxFiles=1000,
    snapbase="snap_",
    snapdirbase="snapdir_",
    check_criteria="max",
    verbose=False,
):
    file_path = os.path.join(folder, filename)
    if os.path.exists(file_path) and (
        (
            check_completion
            and check_timefile_for_completion(
                folder,
                snapbase=snapbase,
                snapdirbase=snapdirbase,
                time_filename=filename,
                check_criteria=check_criteria,
                verbose=verbose,
            )
        )
        or not check_completion
    ):
        pass
    else:
        try:
            createTimeFile(
                folder, snapbase=snapbase, snapdirbase=snapdirbase, verbose=verbose,
            )
        except AssertionError:
            createTimeFile_old(
                folder,
                maxFiles=1000,
                considering_h=False,
                snapbase=snapbase,
                snapdirbase=snapdirbase,
            )
    return np.loadtxt(file_path)


_check_time_dic = {}


def check_timefile_for_completion(
    folder,
    snapbase="snap_",
    snapdirbase="snapdir_",
    time_filename="TimeFile.txt",
    check_criteria="max",
    min_mins_between_checks=30,
    verbose=False,
):
    """

    Parameters
    ----------
    folder: output folder of simulation
    snapbase
    snapdirbase
    time_filename
    check_criteria: max files > size of time file -> redo creation of file

    Returns
    -------

    """
    global _check_time_dic
    time_cur = TimeModule.time()
    may_check = False
    if folder not in _check_time_dic.keys():
        _check_time_dic[folder] = time_cur
        may_check = True
    else:
        if (time_cur - _check_time_dic[folder]) / 60 > min_mins_between_checks:
            may_check = True
            _check_time_dic[folder] = time_cur
    data = np.loadtxt(os.path.join(folder, time_filename))
    N_file = len(data)
    numbers = get_number_snapshots(folder, snapbase=snapbase, snapdirbase=snapdirbase)
    N = len(numbers)
    if verbose:
        print(f"found {N} files/folders in output and {N_file} entries in time file")
    if check_criteria == "max" and N > N_file and may_check:
        return False
    elif check_criteria != "max":
        raise Exception(f"check_criteria: {check_criteria} not implemented!")
    return True


def get_number_snapshots(
    folder, snapbase="snap_", snapdirbase="snapdir_",
):
    lis = os.listdir(folder)
    sn = [
        int(re.compile(f"{snapbase}([0-9]+).hdf5").search(x).group(1))
        for x in lis
        if re.compile(f"{snapbase}([0-9]+).hdf5").search(x) is not None
    ]
    fo = [
        int(re.compile(f"{snapdirbase}([0-9]+).hdf5").search(x).group(1))
        for x in lis
        if re.compile(f"{snapdirbase}([0-9]+).hdf5").search(x) is not None
    ]
    if len(sn) and len(fo):
        raise Exception(f"Detected folders and snapshots in folder: {folder}")
    N = max([len(sn), len(fo)])
    if N == 0:
        raise Exception(f"no snapshots found in folder: {folder}")
    if len(sn) > len(fo):
        return sn
    else:
        return fo


def get_check_common_max_snapshot_number(
    folders,
    returnTime=True,
    useTime=True,
    difference_time=4,
    time_interval=None,
    time_number=None,
    start_time=0,
    max_times=True,
    try_to_get_more=False,
):
    if sum([time_interval is not None, time_number is not None, max_times]) != 1:
        raise Exception(
            f"need to pick exactly one between time_interval: {time_interval}, time_number: {time_number}, max_times: {max_times}"
        )
    if not aux.isIterable(folders):
        folders = aux.makeNIterableList(folders, 1)
    if not returnTime:
        raise NotImplementedError
    if try_to_get_more:
        raise NotImplementedError
    data = []
    for index_folder, f in enumerate(folders):
        d = load_time_file(
            f,
            filename="TimeFile.txt",
            check_completion=True,
            maxFiles=1000,
            snapbase="snap_",
            snapdirbase="snapdir_",
            check_criteria="max",
        )
        data.append(d)
    data = np.array(data)
    fails = []
    final_time = None
    times_to_check = np.unique(data[:, :, int(useTime)])
    all_times = data[:, :, int(useTime)]
    sorted = np.sort(times_to_check.flatten())[::-1]
    for s in sorted:
        if len(folders) > 1:
            check = np.sum(np.abs(all_times - s) < difference_time, axis=1)
            if sum(check) == len(folders):
                final_time = s
                break
        elif len(folders) == 1:
            check = np.sum(np.abs(all_times - s) < difference_time, axis=1)
            if sum(check) > 0:
                final_time = s
                break
        else:
            raise Exception(f"invalid number of folders: {folders}")
        fails.append(s)
    print(f"final_time: {final_time}")
    if time_interval is not None:
        potential_times = np.arange(
            start_time, final_time + time_interval, time_interval
        )
        print(f"potential_times: {potential_times}")
        times = get_similar_numbers(
            all_times, difference_time, folders, potential_times
        )
    elif time_number is not None:
        potential_times = np.linspace(start_time, final_time, time_number)
        times = get_similar_numbers(
            all_times, difference_time, folders, potential_times
        )
    else:
        return final_time
    return times


def get_similar_numbers(
    all_times, difference_time, folders, potential_times, similar=True
):
    times = []
    for t in potential_times:
        check = np.sum(np.abs(all_times - t) < difference_time, axis=1)
        if similar:
            if sum(check) == len(folders):
                times.append(t)
        else:
            if sum(check) != len(folders):
                times.append(t)
    return times


def getSnapNumber(
    folder,
    time,
    differenceTime=2,
    returnType="single",
    returnAllTimes=True,
    returnTimeAndNumber=False,
    v=1,
):
    """
	returnType: 'single', 'all', 'skip' (e.g. skip_N2, skip_until100)
	returns number of snapshot corresponding to 'time' in Myr if within 'differenceTime'
	if returnALl: can decide between returning all times or all snapnumbers
	"""
    if type(time) == str:
        raise Exception(f"time: {time} cant be string!")
    folder = check_snapshot_folder(folder, snapbase=None, time=None, v=0)
    data = load_time_file(
        folder,
        filename="TimeFile.txt",
        check_completion=True,
        maxFiles=1000,
        snapbase="snap_",
        snapdirbase="snapdir_",
        check_criteria="max",
    )
    if returnType != "single":
        if not re.findall(r"until(\d+)", returnType):
            until = None
        else:
            until = int(re.findall(r"until(\d+)", returnType)[0])
        mask = None
        if "all" in returnType:
            mask = slice(until)
        elif "skip" in returnType:
            mask = slice(0, until, int(re.findall(r"N(\d+)", returnType)[0]))
        if not returnTimeAndNumber:
            return data[mask, int(returnAllTimes)]
        else:
            # print(data)
            try:
                np.shape(data)[1]
                print("not single values")
                singleValue = False
            except:
                print("single values")
                singleValue = True
            if not singleValue:
                return data[mask, 0], data[mask, 1]
            else:
                return np.array([data[0]]), np.array([data[1]])
    if np.size(data) == 2:
        print(f"data: {data}")
        if v > 0:
            print("found only single time returning")
        if abs(data[1] - time) > differenceTime:
            print(data)
            print(folder)
            raise Exception(
                "Found time %g which is larger than specified maximum difference (%g) in regard to required time %g!"
                % (data[1], differenceTime, time)
            )
        if not returnTimeAndNumber:
            return data[0]
        else:
            return data[0], data[1]
    if time == -1:
        print(data[-1, 0], data[-1, 1])
        return data[-1, 0], data[-1, 1]
    Ny, Nx = np.shape(data)
    diffDataOld = abs(time - np.inf)
    number = 0
    for i in range(0, Ny):
        diffDataNew = abs(data[i, 1] - time)
        if diffDataNew < diffDataOld:
            number = data[i, 0]
            diffDataOld = diffDataNew
        else:
            break
    if differenceTime is not None:
        if differenceTime < diffDataOld:
            print(i)
            print(data)
            print(folder)
            raise Exception(
                "Found time %g which is larger than specified maximum difference (%g) in regard to required time %g!"
                % (data[i, 1], differenceTime, time)
            )
    if not returnTimeAndNumber:
        return number
    else:
        return number, time


def get_times_in_range(folder, min, max, useTime=True):
    if max < min:
        raise Exception(f"min {min} should be larger than max {max}!")
    times = getSnapNumber(folder, None, returnType="all", returnTimeAndNumber=True)[
        useTime
    ]
    return times[np.logical_and(times >= min, times <= max)]


def readSnap(
    folder,
    time,
    FieldsToLoad=None,
    useTime=True,
    returnSnapNumber=False,
    rewriteTimes=False,
    onlyHeader=False,
    differenceTime=2,
    FieldsLoadExtra=[],
    loadonlytype=False,
    snapbase="snap_",
    snapdirbase="snapdir_",
    lazy_load=True,
    convAddit=True,
    loadParameterFile=True,
    loadConfigFile=True,
    v=0,
    identifier_process=False,
):
    if v > 2:
        print("folder,snapbase,time")
        print(folder, snapbase, time)
    print("folder,snapbase,time")
    print(folder, snapbase, time)
    folder = check_snapshot_folder(folder, snapbase, time, v)
    if os.path.isfile(folder):
        warnings.warn(f"importing file as specified as folder: {folder}\n"
                      f"setting time to None")
        time = None
    if useTime and time is not None:
        if time is not None:
            if not rewriteTimes:
                pass
            else:
                createTimeFile(folder)
            number, time = getSnapNumber(
                folder,
                time,
                differenceTime=differenceTime,
                v=v,
                returnTimeAndNumber=True,
            )

    else:
        number = time
    if type(FieldsToLoad) == str:
        if FieldsToLoad == "basic":
            fields = [
                "pos",
                "mass",
                "u",
                "rho",
                "cren",
                "Machnumber",
                "jetr",
                "MagneticField",
                "vel",
                "MagneticFieldDivergence",
                "vol",
                "CRPressureGradient",
            ]
        elif FieldsToLoad == "all":
            fields = None
            parttype = None
        elif FieldsToLoad == "ONLYHEADER":
            onlyHeader = True
            fields = None
        else:
            FieldsToLoad = False
    else:
        fields = FieldsToLoad
    if len(FieldsLoadExtra) > 0:
        fields += [x for x in FieldsLoadExtra if x not in fields]
    if v > 2:
        print("folder, time", folder, time)
    if time is None:
        if v > 0:
            print("time", time)
        file_path = folder
        filename = os.path.basename(folder)
        folder = os.path.dirname(folder)
        filenameNoHDf5 = re.split("(.hdf5)", filename)[0]
        split = re.split(r"(\.\d+$)", filenameNoHDf5)
        if v > 2:
            print("filenameNoHDf5, split", filenameNoHDf5, split)
        if len(split) > 1:
            if v > 2:
                print(folder + split[0] + ".0.hdf5")
            if path.exists(folder + "/" + split[0] + ".0.hdf5") and path.exists(
                folder + "/" + split[0] + ".1.hdf5"
            ):
                if v > 2:
                    print("detected multiple files!")
                file_path = folder + "/" + split[0]
    else:
        file_path = "%s/%s" % (folder, snapbase)
        file_path += "%03d" % number
        file_path += ".hdf5"

    if fields is not None:
        fields = list(fields)
    try:
        TESTSNAP = bool(int(os.environ.get("TESTSNAP")))
        if v:
            print("TESTSNAP set! -> using modified snapshotclass")
    except TypeError:
        TESTSNAP = True
        if v:
            print("TESTSNAP set to true automatically")
    if TESTSNAP:
        from SnapshotClass import snapshot

        snap = snapshot(
            file_path,
            lazy_load=lazy_load,
            onlyHeader=onlyHeader,
            loadonly=fields,
            loadonlytype=loadonlytype,
            convAddit=convAddit,
            loadParameterFile=loadParameterFile,
            loadConfigFile=loadConfigFile,
            verbose=v,
        )
    else:
        snap = gadget.gadget_readsnapname(
            file_path,
            lazy_load=lazy_load,
            onlyHeader=onlyHeader,
            loadonly=fields,
            loadonlytype=loadonlytype,
            convAddit=convAddit,
            loadParameterFile=loadParameterFile,
            loadConfigFile=loadConfigFile,
            verbose=v,
            identifier_process=identifier_process,
        )
    if not onlyHeader:
        if not hasattr(snap, "vol") and (
            "rho" in snap.data.keys() and "mass" in snap.data.keys()
        ):
            snap.data["vol"][:] = (
                snap.data["mass"][snap.data["type"] == 0] / snap.data["rho"]
            )
    # primtive hack to fix max recursion error for lazy dics
    if "type" in snap.data.keys():
        if hasattr(snap.data, "_loaded_keys"):
            snap.data._loaded_keys.append("type")
    if v > 0:
        print("Found snap with time (code): %g" % snap.time)
    if returnSnapNumber:
        return snap, number
    else:
        return snap


def check_snapshot_folder(folder, snapbase=None, time=None, v=0):
    if "ll1701" in folder:
        folderReplacer = "ll1701"
        folderAlternative = "lustre"
    else:
        folderReplacer = "lustre"
        folderAlternative = "ll1701"
    pathErebos = folder.replace(folderReplacer, "store/erebos")
    pathLustreLl1701 = folder.replace(folderReplacer, folderAlternative)
    if v:
        print(f"folder: {folder}")
        print(f"pathLustreLl1701: {pathLustreLl1701}")
    if os.path.dirname(folder) == "":
        folder = "./" + folder
    if os.path.isdir(os.path.dirname(folder)):
        pass
    elif os.path.isdir(pathErebos):
        folder = pathErebos
    elif os.path.isdir(pathLustreLl1701):
        folder = pathLustreLl1701
    else:
        print("os.path.dirname(folder)", os.path.dirname(folder))
        print("pathErebos", pathErebos)
        print("folder", folder)
        raise Exception("Couldnt find folder!")
    if v > 2:
        if v:
            print("folder,snapbase,time")
            print(folder, snapbase, time)
    return folder


def getVariable(
    snap, variable, considering_h=False, INFO=None, returnINFO=False, verbose=1
):
    VARIABLE = aux.get_value(
        variable,
        snap,
        considering_h=considering_h,
        convert_units=True,
        INFO=INFO,
        returnINFO=returnINFO,
        verbose=verbose,
    )
    return VARIABLE


def getWeights(
    snap, weight, considering_h=False, INFO=None, useWeightsThresholds=False
):
    WEIGHTS = aux.get_value(
        weight, snap, considering_h=considering_h, convert_units=False, INFO=INFO
    )
    if useWeightsThresholds:
        if weight == "JetTracer":
            WEIGHTS[WEIGHTS < 1e-10] = 1e-10
        if weight == "PassiveScalars":
            WEIGHTS[WEIGHTS < 1e-10] = 1e-10
        if weight == "CosmicRayEnergyDensity":
            WEIGHTS[WEIGHTS < 1e-30] = 1e-30
        if weight == "JetVolumeFraction":
            WEIGHTS[WEIGHTS < 1e-20] = 1e-20
        if weight == "JetDensityFraction":
            WEIGHTS[WEIGHTS < 1e-20] = 1e-20
        if weight == "JetMassFraction":
            WEIGHTS[WEIGHTS < 1e-20] = 1e-20
    return WEIGHTS


def genericObjectInitialisation(object, sizeX, sizeY, singleObject=False):
    if np.size(object) == 1 or singleObject:
        object = [[object for y in range(sizeY)] for x in range(sizeX)]
    elif isinstance(object[0], list):
        if not np.size(object[0]) == np.size(object[1]):
            exit("if want to specifiy object for every element have to rectangular")
    else:
        if object[-1] == 0:
            object = [[x for y in range(sizeY)] for x in object[:-1]]
        elif object[-1] == 1:
            object = [[y for y in object[:-1]] for x in range(sizeX)]
        else:
            print(object)
            exit("have to specify 0,1 at the end of list")

        if np.shape(object) != (sizeX, sizeY):
            print(object)
            print(np.shape(object))
            print(sizeX, sizeY)
            exit(
                "expected different shape for object, specified too few/many items in list"
            )
    return object


def genericObjectInitialisationList(
    object, sizeX, sizeY, singleObject=False, flexibleListSize=False
):
    if type(object[0]) != list and type(object[0]) != np.ndarray:
        object = [[object for y in range(sizeY)] for x in range(sizeX)]
    # elif isinstance(object[0][0], list):
    #    if not np.size(object[0]) == np.size(object[1]):
    #        exit('if want to specifiy object for every element have to rectangular')
    else:
        if object[-1] == 0:
            object = [[x for y in range(sizeY)] for x in object[:-1]]
        elif object[-1] == 1:
            object = [[y for y in object[:-1]] for x in range(sizeX)]
        else:
            print(object)
            exit("have to specify 0,1 at the end of list")

        if not flexibleListSize:
            if np.shape(object)[:-1] != (sizeX, sizeY):
                print(object)
                print(np.shape(object))
                print(sizeX, sizeY)
                exit(
                    "expected different shape for object, specified too few/many items in list"
                )
        else:
            if len(np.shape(object)) == 2:
                if np.shape(object)[:] != (sizeX, sizeY):
                    print(object)
                    print(np.shape(object))
                    print(sizeX, sizeY)
                    exit(
                        "expected different shape for object, specified too few/many items in list"
                    )
            elif len(np.shape(object)) > 2:
                if np.shape(object)[:-1] != (sizeX, sizeY):
                    print(object)
                    print(np.shape(object))
                    print(sizeX, sizeY)
                    exit(
                        "expected different shape for object, specified too few/many items in list"
                    )
    return object


def do_Slice(snap, VARIABLE, res, box, center, numthreads, axis, verbose=True):
    if type(VARIABLE) != str:
        # dont call this data otherwise will confuse data dictionary in snapshot!!
        variableName = "datadata"
        snap.data[variableName] = np.array(VARIABLE)
    else:
        variableName = VARIABLE
    nx = None
    ny = None
    if len(res) == 1:
        pass
    elif len(res) == 2:
        nx = res[0]
        ny = res[1]
        res = res[0]
    else:
        raise Exception("wrong fromat for resoluiton of slice" + res)
    if verbose:
        debug.printDebug(snap.pos, name="snap.pos")
        print(f"in do_Slice")
        print(f"variableName: {variableName}")
        print(f"res: {res}")
        print(f"box: {box}")
        print(f"center: {center}")
        print(f"numthreads: {numthreads}")
        print(f"axes: {axis}")
        print(f"nx: {nx}")
        print(f"ny: {ny}")
    data = snap.get_Aslice(
        variableName,
        res=res,
        box=box,
        center=center,
        numthreads=numthreads,
        axes=axis,
        nx=nx,
        ny=ny,
    )
    data = data["grid"].T
    return data


def validateArray(thing, Dim=3, thingDtype=list, dtype="original"):
    import collections

    if np.size(thing) == Dim:
        thing = np.array(thing)
    elif np.size(thing) == 1:
        if isinstance(thing, collections.Iterable):
            thing = np.array([thing[0] for i in range(Dim)])
        else:
            thing = np.array([thing for i in range(Dim)])
    else:
        print(thing)
        raise Exception(
            "wrong size (%i) to be converted to array of dim %i" % (np.size(thing), Dim)
        )
    if dtype != "original" and not any([x is None for x in thing]):
        # print( thing)
        thing = np.array([dtype(x) for x in thing], dtype=dtype)
    return thingDtype(thing)


def getAgrid(
    snap,
    VARIABLE,
    res=512,
    ProjBox=1.0,
    center=None,
    numthreads=16,
    res_critical=1024,
    verbose=1,
    use_only_cells=None,
):
    if not type(VARIABLE) == str:
        # do not call this data as it will interfere with dictionary "data" on snapshot
        variableName = "datadata"
        snap.data[variableName] = VARIABLE
    else:
        variableName = VARIABLE
    if center is None:
        print("using boxsize/2 as center for grid!")
        center = [snap.boxsize / 2.0] * 3
        print("center", center)
    center = validateArray(center, dtype=np.float128)
    res = validateArray(res, dtype=int)
    ProjBox = validateArray(ProjBox, dtype=np.float128)
    if type(res) == int:
        print(res)
        raise Exception("res should be converted to array by now, not an int!")
    NExtraGrids = np.sum([x > res_critical for x in res])
    if NExtraGrids == 0:
        data = snap.mapOnCartGrid(
            variableName,
            center=center,
            box=ProjBox,
            res=res,
            saveas=False,
            numthreads=numthreads,
            verbose=verbose,
            use_only_cells=use_only_cells,
        )
    elif NExtraGrids == 1:
        data = getSingleGrid(
            snap,
            variableName,
            res=res,
            ProjBox=ProjBox,
            center=center,
            numthreads=numthreads,
            resCritical=res_critical,
            verbose=verbose,
            use_only_cells=use_only_cells,
        )
    elif NExtraGrids == 2:
        data = getDoubleGrid(
            snap,
            variableName,
            res=res,
            ProjBox=ProjBox,
            center=center,
            numthreads=numthreads,
            resCritical=res_critical,
            verbose=verbose,
            use_only_cells=use_only_cells,
        )
    if NExtraGrids == 3:
        data = getTripleGrid(
            snap,
            variableName,
            res=res,
            ProjBox=ProjBox,
            center=center,
            numthreads=numthreads,
            resCritical=res_critical,
            verbose=verbose,
            use_only_cells=use_only_cells,
        )
    return data


def getTripleGrid(
    snap,
    variableName,
    res=512,
    ProjBox=1.0,
    center=None,
    numthreads=16,
    resCritical=1024,
    verbose=1,
    use_only_cells=None,
):
    res = np.array(res)
    if len(res[res > resCritical]) != 3:
        print(res)
        raise Exception(
            "only look at resolution exceeding resCritical %i in three dimensions!"
            % resCritical
        )
    axisRelevant = np.arange(3)[np.where(res > resCritical)]
    axMaxRes = axisRelevant[0]
    print("getTripleGrid: axMaxRes:", axMaxRes)
    maxRes = res[axMaxRes]
    Nloops = int(maxRes) / int(resCritical)
    ResAxMax = [resCritical] * int(Nloops)
    restRes = int(maxRes) % int(resCritical)
    if restRes > 0:
        Nloops += 1
        ResAxMax += [restRes]
    restemp = copy.deepcopy(res)
    projboxtemp = copy.deepcopy(ProjBox)
    centertemp = copy.deepcopy(center)
    centertemp[axMaxRes] -= (
        ProjBox[axMaxRes] / 2.0
    )  # shift center all way to lower boundary of ProjBox
    DataTup = tuple()
    print("centertemp before loop", centertemp)
    cumResAxmax = 0
    cumProjBox = 0
    for resaxmax in ResAxMax:
        cumResAxmax += resaxmax
        fracCovBox = np.float128(resaxmax) / np.float128(maxRes)
        actlCovBox = fracCovBox * np.float128(ProjBox[axMaxRes])
        cumProjBox += actlCovBox
        restemp[axMaxRes] = resaxmax
        centertemp[axMaxRes] += actlCovBox / 2.0
        print("centertemp in loop", centertemp)
        projboxtemp[axMaxRes] = actlCovBox
        data = getDoubleGrid(
            snap,
            variableName,
            center=centertemp,
            ProjBox=projboxtemp,
            res=restemp,
            numthreads=numthreads,
            resCritical=resCritical,
            verbose=verbose,
            use_only_cells=use_only_cells,
        )
        DataTup += (data,)
        centertemp[axMaxRes] += (
            actlCovBox / 2.0
        )  # always move center to end of current projbox to be insensitive of current projbox size
        print("centertemp in loop later", centertemp)
    if not math.isclose(cumProjBox, ProjBox[axMaxRes]):
        print(cumProjBox, ProjBox[axMaxRes])
        raise Exception("didnt cover full box!")
    if cumResAxmax != maxRes:
        print(cumResAxmax, ResAxMax)
        raise Exception("cumulative resolution not same as needed!")
    data = np.concatenate(DataTup, axis=axMaxRes)
    return data


def getDoubleGrid(
    snap,
    variableName,
    res=512,
    ProjBox=1.0,
    center=None,
    numthreads=16,
    resCritical=1024,
    verbose=1,
    use_only_cells=None,
):
    res = np.array(res)
    if len(res[res > resCritical]) != 2:
        print(res)
        raise Exception(
            "only look at resolution exceeding resCritical %i in two dimensions!"
            % resCritical
        )
    axisRelevant = np.arange(3)[np.where(res > resCritical)]
    axMaxRes = axisRelevant[0]
    print("getDoubleGrid: axMaxRes:", axMaxRes)
    maxRes = res[axMaxRes]
    Nloops = int(maxRes) / int(resCritical)
    ResAxMax = [resCritical] * int(Nloops)
    restRes = int(maxRes) % int(resCritical)
    if restRes > 0:
        Nloops += 1
        ResAxMax += [restRes]
    restemp = copy.deepcopy(res)
    projboxtemp = copy.deepcopy(ProjBox)
    centertemp = copy.deepcopy(center)
    centertemp[axMaxRes] -= (
        ProjBox[axMaxRes] / 2.0
    )  # shift center all way to lower boundary of ProjBox
    DataTup = tuple()
    print("centertemp before loop", centertemp)
    cumResAxmax = 0
    cumProjBox = 0
    for resaxmax in ResAxMax:
        cumResAxmax += resaxmax
        fracCovBox = np.float128(resaxmax) / np.float128(maxRes)
        actlCovBox = fracCovBox * np.float128(ProjBox[axMaxRes])
        cumProjBox += actlCovBox
        restemp[axMaxRes] = resaxmax
        centertemp[axMaxRes] += actlCovBox / 2.0
        print("centertemp in loop", centertemp)
        projboxtemp[axMaxRes] = actlCovBox
        data = getSingleGrid(
            snap,
            variableName,
            center=centertemp,
            ProjBox=projboxtemp,
            res=restemp,
            numthreads=numthreads,
            resCritical=resCritical,
            verbose=verbose,
            use_only_cells=use_only_cells,
        )
        DataTup += (data,)
        centertemp[axMaxRes] += (
            actlCovBox / 2.0
        )  # always move center to end of current projbox to be insensitive of current projbox size
        print("centertemp in loop later", centertemp)
    if not math.isclose(cumProjBox, ProjBox[axMaxRes]):
        print(cumProjBox, ProjBox[axMaxRes])
        raise Exception("didnt cover full box!")
    if cumResAxmax != maxRes:
        print(cumResAxmax, ResAxMax)
        raise Exception("cumulative resolution not same as needed!")
    data = np.concatenate(DataTup, axis=axMaxRes)
    return data


def getSingleGrid(
    snap,
    variableName,
    res=512,
    ProjBox=1.0,
    center=None,
    numthreads=16,
    resCritical=1024,
    verbose=1,
    use_only_cells=None,
):
    res = np.array(res)
    if len(res[res > resCritical]) > 1:
        print(res)
        raise Exception(
            "only look at resolution exceeding %i in one dimension!" % resCritical
        )
    maxRes = np.max(res)
    axMaxRes = np.arange(3)[res == maxRes][0]
    print("getSingleGrid: axMaxRes:", axMaxRes)
    Nloops = int(maxRes) / int(resCritical)
    ResAxMax = [resCritical] * int(Nloops)
    restRes = int(maxRes) % int(resCritical)
    if restRes > 0:
        Nloops += 1
        ResAxMax += [restRes]
    restemp = copy.deepcopy(res)
    projboxtemp = copy.deepcopy(ProjBox)
    centertemp = copy.deepcopy(center)
    centertemp[axMaxRes] -= (
        ProjBox[axMaxRes] / 2.0
    )  # shift center all way to lower boundary of ProjBox
    DataTup = tuple()
    print("centertemp before loop", centertemp)
    cumResAxmax = 0
    cumProjBox = 0
    for resaxmax in ResAxMax:
        cumResAxmax += resaxmax
        fracCovBox = np.float128(resaxmax) / np.float128(maxRes)
        actlCovBox = fracCovBox * np.float128(ProjBox[axMaxRes])
        cumProjBox += actlCovBox
        restemp[axMaxRes] = resaxmax
        centertemp[axMaxRes] += actlCovBox / 2.0
        # print('type actl cov box', type(actlCovBox/2.))
        print("centertemp in loop", centertemp)
        projboxtemp[axMaxRes] = actlCovBox
        # 			data = snap.get_Agrid(np.array(VARIABLE, dtype=np.float128), res=restemp, box=projboxtemp, center=centertemp ,numthreads=numthreads)['grid']
        data = snap.mapOnCartGrid(
            variableName,
            center=centertemp,
            box=projboxtemp,
            res=restemp,
            saveas=False,
            use_only_cells=use_only_cells,
            numthreads=numthreads,
            verbose=verbose,
        )
        DataTup += (data,)
        centertemp[axMaxRes] += (
            actlCovBox / 2.0
        )  # always move center to end of current projbox to be insensitive of current projbox size
        print("centertemp in loop later", centertemp)
    if not math.isclose(cumProjBox, ProjBox[axMaxRes]):
        print(cumProjBox, ProjBox[axMaxRes])
        raise Exception("didnt cover full box!")
    if cumResAxmax != maxRes:
        print(cumResAxmax, ResAxMax)
        raise Exception("cumulative resolution not same as needed!")
    data = np.concatenate(DataTup, axis=axMaxRes)
    return data


def do_Projection(
    snap,
    VARIABLE,
    WEIGHTS=None,
    res=128,
    ProjBox=1.0,
    center=None,
    numthreads=16,
    axis=[2, 0],
    inputGrids=False,
    sumAlongAxis=False,
    INFO=None,
    res_critical=1024,
    verbose=1,
    reimportStrVar=True,
):
    ignore_zeros = aux.checkExistenceKey(INFO, "projectionIgnoreZeros", False)
    # does actual projection: 3 different kinds:
    # 1) weighted projection: sum var*wei & wei along projection axis then: var*wei/wei
    # 2) non-weighted projection: mean var along projection axis (WEIGHTS is None)
    # 3) line of sight integral: sum var along projection axis (sumAlongAxis=True and WEIGHTS is None)
    ProjBox = validateArray(ProjBox, dtype=np.float128)
    if center is None:
        center = np.array([x for x in snap.center])
    center = validateArray(center, dtype=np.float128)
    res = validateArray(res, dtype=int)

    if type(VARIABLE) == str:
        if reimportStrVar or VARIABLE not in snap.data.keys():
            VARIABLE = aux.get_value(VARIABLE, snap, INFO=INFO)
        else:
            VARIABLE = snap.data[VARIABLE]
    if type(WEIGHTS) == str:
        if reimportStrVar or WEIGHTS not in snap.data.keys():
            WEIGHTS = getWeights(snap, WEIGHTS, False, INFO, useWeightsThresholds=False)
        else:
            WEIGHTS = snap.data[WEIGHTS]
    VARIABEL = copy.deepcopy(VARIABLE)
    WEIGHTS = copy.deepcopy(WEIGHTS)
    ProjAxis = 3 - np.sum(axis)
    OverflowVar = False
    mask = None
    if snap is None:
        mask = aux.getLocationMaskGrid(snap, INFO, res, ProjBox, center, numthreads)
    if WEIGHTS is not None:
        if np.min(abs(VARIABEL)) < 1e-10:
            VARIABEL *= 1e10
            OverflowVar = True
        if np.min(abs(WEIGHTS)) < 1e-10:
            WEIGHTS *= 1e10
        if not inputGrids:
            data = getAgrid(
                snap,
                VARIABEL * WEIGHTS,
                res=res,
                ProjBox=ProjBox,
                center=center,
                numthreads=numthreads,
                res_critical=res_critical,
                verbose=verbose,
            )
            data2 = getAgrid(
                snap,
                WEIGHTS,
                res=res,
                ProjBox=ProjBox,
                center=center,
                numthreads=numthreads,
                res_critical=res_critical,
                verbose=verbose,
            )
        else:
            data = VARIABEL
            data2 = WEIGHTS
        if mask is not None:
            mask = np.logical_not(mask)
            data[mask] = 0.0
        data = np.sum(data, axis=ProjAxis)
        data2 = np.sum(data2, axis=ProjAxis)
        time_start = TimeModule.time()
        data2[data2 == 0] = 1.0
        data /= data2

        if axis[0] < axis[1]:
            data = data.T
        if OverflowVar:
            data /= 1e10
    else:
        if np.min(abs(VARIABEL)) < 1e-10:
            VARIABEL *= 1e10
            OverflowVar = True
        if not inputGrids:
            data = getAgrid(
                snap,
                VARIABEL,
                res=res,
                ProjBox=ProjBox,
                center=center,
                numthreads=numthreads,
                res_critical=res_critical,
                verbose=verbose,
            )
        else:
            data = VARIABEL
        if mask is not None:
            mask = np.logical_not(mask)
            data[mask] = 0.0
        if sumAlongAxis:
            data = np.sum(data, axis=ProjAxis)
            spacing = res[ProjAxis]
            if aux.checkExistenceKey(INFO, "ignoreSpacingProj", False):
                spacing = 1.0
            ProjLength = ProjBox[ProjAxis] / spacing  # * 3*10**18*10**3
            print(
                "spacing: ",
                spacing,
                "ProjBox[ProjAxis]",
                ProjBox[ProjAxis],
                "ProjLength",
                ProjLength,
            )
            if aux.checkExistenceKey(INFO, "ConvertProjCM", True):
                print("converting projected distance to cm!")
                ProjLength *= snap.UnitLength_in_cm
            unit_conversion_factor = 1
            if aux.checkExistenceKey(INFO, "ConvertSummedProjMsolPcM2", False):
                print("converting summed values to to Msol/pc^2!")
                unit_conversion_factor = param.PC_in_cm ** 2 / param.SOLARMASS_in_g
            data = np.array(data) * float(ProjLength) * unit_conversion_factor
        else:
            if ignore_zeros:
                counts = np.count_nonzero(data, axis=ProjAxis)
                data = np.sum(data, axis=ProjAxis)
                data /= counts
            else:
                data = np.mean(data, axis=ProjAxis)
        if axis[0] < axis[1]:
            data = data.T
        if OverflowVar:
            data /= 1e10
    return data


def do_ProjectionTwice(
    snap,
    folder2,
    FieldsToLoad,
    VARIABLE,
    WEIGHTS,
    res,
    ProjBox,
    center,
    numthreads,
    axis,
    INFO,
    volumeIntegral=False,
    useTime=True,
    useWeightsThresholds=False,
    inputGrids=False,
    res_critical=1024,
):
    try:
        variableTwice = INFO["ProjTwiceVariable"]
        weightTwice = INFO["ProjTwiceWeight"]
        SnapNumTwice = INFO["ProjTwiceSnapNum"]
    except:
        print(INFO)
        exit(
            "Need to specify ProjTwiceVariable, ProjTwiceWeight, ProjTwiceSnapNum in INFO to make projection twice work!"
        )
    snap2 = readSnap(
        folder2,
        SnapNumTwice,
        FieldsToLoad=FieldsToLoad,
        rewriteTimes=False,
        useTime=useTime,
    )
    snap2, time = getGeneralInformation(
        snap2, False, True
    )  # Precision=None,returnNotStringTime=False)
    VARIABLE2 = getVariable(snap2, variableTwice, True, INFO)
    WEIGHTS2 = getWeights(
        snap2, weightTwice, False, INFO, useWeightsThresholds=useWeightsThresholds
    )

    grid1 = do_Projection(
        snap,
        VARIABLE,
        WEIGHTS,
        res,
        ProjBox,
        center,
        numthreads,
        axis,
        volumeIntegral=False,
        inputGrids=inputGrids,
        res_critical=res_critical,
    )
    grid2 = do_Projection(
        snap2,
        VARIABLE2,
        WEIGHTS2,
        res,
        ProjBox,
        center,
        numthreads,
        axis,
        volumeIntegral=volumeIntegral,
        inputGrids=False,
        res_critical=res_critical,
    )
    print("ProjectionTwice: plot higher values above lower values")
    mask = grid1 < grid2
    grid1[mask] = grid2[mask]
    return grid1


def calcMean(data, weights):
    if weights is not None:
        Mean = np.sum(data * weights) / np.sum(weights)
    else:
        Mean = np.mean(data)
    return Mean


def calcDispersion(data, weights, Mean, Ndata):
    if np.size(data) == 1:
        Dispersion = 0
    elif weights is not None:
        Dispersion = np.sqrt(
            np.sum(weights * (data - Mean) ** 2)
            / ((Ndata - 1) * np.sum(weights) / Ndata)
        )
    else:
        Dispersion = np.sqrt(1.0 / (Ndata - 1) * np.sum(data - Mean) ** 2)
    return Dispersion


def getHistogram(
    data,
    weights=None,
    binNumber=40,
    binLog=False,
    binRange=None,
    fit="Gaussian",
    plot=True,
    plotSave=True,
    plotFilename="test.pdf",
    plotdpi=400,
    plotColors=["lightcoral", "royalblue"],
    plotFontsize=12,
    plotShow=True,
    bin_edges=None,
    include_outlier_bins=False,
    show_outlier_bins=False,
    normalize=None,
    ignore_zeros=False,
    average_style=None,
    verbose=False,
):
    if show_outlier_bins and not include_outlier_bins:
        raise Exception(
            f"cant show_outlier_bins {show_outlier_bins} if not include_outlier_bins {include_outlier_bins}"
        )
    print(f"normalize: {normalize}")
    if average_style is not None:
        print(f"averaging {len(data)} histograms!")
    if bin_edges is None:
        bin_edges = get_Bins(
            Range=binRange, Data=data, Nbins=binNumber, log=binLog, returnCenters=False
        )
    bin_edges = np.array(bin_edges)
    if average_style is None:
        data = [data]
        weights = [weights]
    else:
        if weights is None:
            weights = [None] * len(data)
    hist_list = []
    for d, w in zip(data, weights):
        if ignore_zeros:
            m = np.nonzero(d)
            d = d[m]
            if w is not None:
                w = w[m]
        d = np.copy(d).flatten()
        if w is None:
            w = np.ones_like(d)
        w = np.array(w)
        if len(d) > 0:
            if verbose:
                print(f"bin_edges: {bin_edges}")
                print(f"weights: {w}")
                print(f"data: {d}")
            if np.count_nonzero(d) != 0:
                hist, bin_edges, binnumber = scipy.stats.binned_statistic(
                    d, w, "sum", bin_edges
                )
            else:
                hist = np.zeros(len(bin_edges) - 1)
                binnumber = np.zeros_like(d)
            if include_outlier_bins:
                new_min_edge = min(bin_edges)
                min_data = d[binnumber == 0]
                if len(min_data):
                    new_min_edge = np.min(min_data)
                new_max_edge = max(bin_edges)
                max_data = d[binnumber == len(bin_edges)]
                if len(max_data):
                    new_max_edge = np.max(max_data)
                bin_edges = np.concatenate(([new_min_edge], bin_edges, [new_max_edge]))

                new_min_value = 0
                if len(min_data):
                    new_min_value = np.sum(w[binnumber == 0])
                new_max_value = 0
                if len(max_data):
                    # len(bin_edges) - 2 as bin_edges just increased by two more values!
                    new_max_value = np.sum(w[binnumber == len(bin_edges) - 2])
                hist = np.concatenate(([new_min_value], hist, [new_max_value]))
        else:
            hist = np.zeros_like(bin_edges[:-1])
            binnumber = np.zeros_like(bin_edges[:-1])
        hist_list.append(hist)
    err = None
    if average_style is not None:
        if "percentiles" in average_style:
            percentile = 84.13  # sigma # 97.73 #2sigma
            err = np.array(
                [
                    np.percentile(hist_list, 100 - percentile, axis=0),
                    np.percentile(hist_list, percentile, axis=0),
                ]
            )
        if "mean" in average_style:
            if verbose:
                print(f"hist_list: {hist_list}")
            hist = np.mean(hist_list, axis=0)
            if verbose:
                print(f"hist: {hist} after averaging")
        elif "median" in average_style:
            if verbose:
                print(f"hist_list: {hist_list}")
            hist = np.median(hist_list, axis=0)
            if verbose:
                print(f"hist: {hist} after averaging")
        else:
            raise Exception(f"average_histogram_style: {average_style} not understood!")
    # if "ABS" in average_style:
    #     hist = np.abs(hist)
    #     if err is not None:
    #         err = np.abs(hist)
    if normalize is None:
        pass
    elif normalize == "bin_edges":
        bin_widths = np.diff(bin_edges)
        m = bin_widths != 0
        hist[m] = hist[m] / bin_widths[m]
        if err is not None:
            err = err / bin_widths[m]
    elif normalize == True:
        summed_hist = np.sum(hist)
        hist = hist / summed_hist
        if verbose:
            print(f"normalize: {normalize}, summed_hist: {summed_hist}")
        if err is not None:
            err = err / summed_hist
        # length_data = len(hist)
        # if include_outlier_bins:
        #     length_data = len(hist) - 2
        # hist = hist / length_data
        # if err is not None:
        #     err = err / length_data
        # if len(d) and average_style is None:
        #     hist = hist / len(data)  # * np.sum(np.diff(np.log10(bin_edges)))
        #     if err is not None:
        #         err = err / len(err)
        if not binLog:
            bin_widths = np.diff(bin_edges)
        else:
            bin_widths = np.diff(np.log10(bin_edges))
        if verbose:
            print(np.log10(bin_edges))
            debug.printDebug(np.log10(bin_edges), "np.log10(bin_edges)")
            debug.printDebug(bin_edges, "bin_edges", dontShow=[])
        m = bin_widths != 0
        hist[m] = hist[m] / bin_widths[m]
        if err is not None:
            err[:, m] = err[:, m] / bin_widths[m]
        if verbose:
            print(
                f"np.sum(np.diff(np.log10(bin_edges))): {np.sum(np.diff(np.log10(bin_edges)))}"
            )
    else:
        raise Exception(f"normalization option: {normalize} unknown!")
    bins = bin_edges[:-1] + np.diff(bin_edges) / 2
    returnDict = {"bins": bins, "bin_edges": bin_edges, "hist": hist, "err": err}
    if fit == "Gaussian":
        paras, perror, fitfunction = Fit.fitSinglePeak(
            bins, hist, hist, returnFitFunction=True
        )
        parameterNames = ["\\mathrm{A}", "\\mu", "\\sigma"]
    elif fit is None:
        pass
    else:
        raise Exception(f"fit {fit} not implemented yet!")
    if fit is not None:
        returnDict["paras"] = paras
        returnDict["perror"] = perror
        returnDict["fitfunction"] = fitfunction
        returnDict["parameter_names"] = parameterNames
    if plot:
        plotColors = makeIterable(plotColors, 1 + int(fit is not None))
        fig = plt.figure()
        ax = plt.axes([0.1, 0.1, 0.84, 0.84])
        ax.bar(
            bins, hist, width=np.diff(bin_edges), color=plotColors[0], edgecolor="black"
        )
        if fit is not None:
            ax.plot(bins, fitfunction(bins, *paras), color=plotColors[1])
            note = []
            for para, perr, pname in zip(paras, perror, parameterNames):
                note += [r"$%s=%.2g \pm %.2g$" % (pname, para, perr)]
            note = "\n".join(map(str, note))
            ax.text(
                0.065,
                0.8,
                note,
                fontsize=plotFontsize,
                horizontalalignment="left",
                color="black",
                transform=ax.transAxes,
            )
        if plotSave:
            aux.checkExistence_delete_file(plotFilename)
            aux.create_dirs_for_file(plotFilename)
            savePlot(fig, plotFilename, dpi=plotdpi)
        if not plotShow:
            plt.close(fig)
    return returnDict


def fitGaussians(
    data,
    weights,
    resFit=15,
    onlyFirstPeak=True,
    incrementesResFitFactor=0.1,
    Nit=10,
    verbose=False,
    plotFit=False,
    closeFig=True,
    filename="test",
    threshFracMax=1e-2,
    MaxResFit=60,
    percentFitMaxError=0.5,
    percentFitMaxErrorPerfection=0.01,
    saveHistogram=True,
    maxSigma=70,
    filenameEnd="",
    onlySaveNPZ=False,
    saveAll=False,
):
    # print('data,weights in fit gaussian',data,weights)
    Paras = None
    import scipy.stats

    if onlyFirstPeak:
        Nit = 1
    mask = np.logical_and(weights != 0, data != 0)
    # if np.sum(mask)==0:
    # 	raise Exception('empty data!')
    if np.sum(mask) == 0:
        warnings.warn("fitGaussians: NO FIT POSSIBLE, No-nonzero values!")
        return resFit, [[0, 0, 0]], [[0, 0, 0]]
    MIN = np.min(data[mask])
    MAX = np.max(data[mask])
    if MAX - MIN <= 1.5 * resFit:
        if verbose:
            print(np.unique(data))
            print("couldnt create bins")
        return resFit, [[0, 0, 0]], [[0, 0, 0]]
    counter = 0
    while Paras is None:
        # increase bin size to find neughbouring peaks if necessary
        if resFit > MaxResFit:
            print("bin sizes %g getting too large, fit impossible" % resFit)
            return resFit, [[0, 0, 0]], [[0, 0, 0]]
        if counter != 0:
            resFit += incrementesResFitFactor * resFit
        counter += 1
        bins = np.arange(
            int(np.min(data) / resFit) * resFit - resFit,
            np.max(data) + resFit + resFit / 2.0,
            resFit,
        )
        hist, bin_edges, binnumber = scipy.stats.binned_statistic(
            data, weights, "sum", bins
        )
        if np.size(hist[hist != 0]) < 2:
            mask = np.argsort(data)
            if verbose:
                print(
                    "fitGauss:Exc:data,weights,hist,bin_edges,MAX,MIN",
                    data[mask],
                    weights[mask],
                    hist,
                    bin_edges,
                    MAX,
                    MIN,
                )
            print("ERROR: not enough values to calculate histogram!")
            return resFit, [[0, 0, 0]], [[0, 0, 0]]
            # raise Exception('ERROR: not enough values to calculate histogram!')
        bins = bin_edges[:-1] + np.diff(bin_edges) / 2.0
        Paras, Errors = Fit.plotfitMultiGaussians(
            bins, hist, toCalc="paras", Nit=Nit, threshFracMax=threshFracMax
        )
        if counter > 1000:
            Ppart, Fpart = os.path.split(filename)
            Ppart += "/tooManyBins/"
            filename = Ppart + Fpart
            save_histogram(
                data,
                weights=weights,
                bins=bin_edges,
                filename=filename + "BUG",
                parameterNames=["\\mathrm{A}", "\\langle v \\rangle", "\\sigma"],
                plotFunctionParameters=Paras,
                closeFig=closeFig,
                plotFunctionErrors=Errors,
                xlabel=r"$v\ [\mathrm{km}\ \mathrm{s}^{-1}]$",
                ylabel=r"$\mathrm{counts}\ [\mathrm{arbitrary}\ \mathrm{units}]$",
                filenameEnd=filenameEnd,
                onlySaveNPZ=onlySaveNPZ,
            )
            raise Exception(
                "couldnt find neighbouring cells to peak after %i iterations!" % counter
            )
    if counter != 1 or verbose:
        print(
            "found neighbouring peaks after %i iteration with resFit %g"
            % (counter, resFit)
        )
    if saveAll:
        print(
            r"SAVEALL: saving histogram with values (sigma=%.2g, A=%.2g+%.2g (%f), mu=%.2g+%.2g (%f), sigma=%.2g+%.2g (%f)), resFit=%f"
            % (
                Paras[0][2],
                Paras[0][0],
                Errors[0][0],
                Errors[0][0] / Paras[0][0],
                Paras[0][1],
                Errors[0][1],
                Errors[0][1] / Paras[0][1],
                Paras[0][2],
                Errors[0][2],
                Errors[0][2] / Paras[0][2],
                resFit,
            )
        )
        Ppart, Fpart = os.path.split(filename)
        Ppart += "/all/"
        filename = Ppart + Fpart
        if saveHistogram:
            save_histogram(
                data,
                weights=weights,
                bins=bin_edges,
                filename=filename,
                parameterNames=["\\mathrm{A}", "\\langle v \\rangle", "\\sigma"],
                plotFunctionParameters=Paras,
                plotFunctionErrors=Errors,
                xlabel=r"$v\ [\mathrm{km}\ \mathrm{s}^{-1}]$",
                ylabel=r"$\mathrm{counts}\ [\mathrm{arbitrary}\ \mathrm{units}]$",
                filenameEnd=filenameEnd,
                onlySaveNPZ=onlySaveNPZ,
                closeFig=closeFig,
            )
    elif Paras[0][2] > maxSigma or any(
        [abs(p * percentFitMaxError) < abs(e) for p, e in zip(Paras[0], Errors[0])]
    ):
        print(
            r"saving histogram as problematic values (sigma=%.2g, A=%.2g+%.2g (%f), mu=%.2g+%.2g (%f), sigma=%.2g+%.2g (%f))"
            % (
                Paras[0][2],
                Paras[0][0],
                Errors[0][0],
                Errors[0][0] / Paras[0][0],
                Paras[0][1],
                Errors[0][1],
                Errors[0][1] / Paras[0][1],
                Paras[0][2],
                Errors[0][2],
                Errors[0][2] / Paras[0][2],
            )
        )
        Ppart, Fpart = os.path.split(filename)
        Ppart += "/horrible/"
        filename = Ppart + Fpart
        if saveHistogram:
            save_histogram(
                data,
                weights=weights,
                bins=bin_edges,
                filename=filename,
                parameterNames=["\\mathrm{A}", "\\langle v \\rangle", "\\sigma"],
                plotFunctionParameters=Paras,
                plotFunctionErrors=Errors,
                xlabel=r"$v\ [\mathrm{km}\ \mathrm{s}^{-1}]$",
                ylabel=r"$\mathrm{counts}\ [\mathrm{arbitrary}\ \mathrm{units}]$",
                filenameEnd=filenameEnd,
                onlySaveNPZ=onlySaveNPZ,
                closeFig=closeFig,
            )
    elif all(
        [
            abs(p * percentFitMaxErrorPerfection) > abs(e)
            for p, e in zip(Paras[0], Errors[0])
        ]
    ):
        print(
            r"saving histogram as perfect values (sigma=%.2g, A=%.2g+%.2g (%f), mu=%.2g+%.2g (%f), sigma=%.2g+%.2g (%f))"
            % (
                Paras[0][2],
                Paras[0][0],
                Errors[0][0],
                Errors[0][0] / Paras[0][0],
                Paras[0][1],
                Errors[0][1],
                Errors[0][1] / Paras[0][1],
                Paras[0][2],
                Errors[0][2],
                Errors[0][2] / Paras[0][2],
            )
        )
        Ppart, Fpart = os.path.split(filename)
        Ppart += "/perfect/"
        filename = Ppart + Fpart
        if saveHistogram:
            save_histogram(
                data,
                weights=weights,
                bins=bin_edges,
                filename=filename,
                parameterNames=["\\mathrm{A}", "\\langle v \\rangle", "\\sigma"],
                plotFunctionParameters=Paras,
                plotFunctionErrors=Errors,
                xlabel=r"$v\ [\mathrm{km}\ \mathrm{s}^{-1}]$",
                ylabel=r"$\mathrm{counts}\ [\mathrm{arbitrary}\ \mathrm{units}]$",
                filenameEnd=filenameEnd,
                onlySaveNPZ=onlySaveNPZ,
                closeFig=closeFig,
            )
    if percentFitMaxError is not None:
        if any(
            [abs(p * percentFitMaxError) < abs(e) for p, e in zip(Paras[0], Errors[0])]
        ):
            return resFit, [[0, 0, 0]], [[0, 0, 0]]
    return resFit, Paras, Errors


def getGaussianFit(
    ij,
    data,
    weights,
    reduceMethod,
    plotHist=True,
    figNameExt="",
    folderExt="",
    ignoreZeros=False,
    Hist="dataweights",
    feature=None,
    resFit=15,
    threshFracMax=1e-2,
    saveAll=False,
    saveHistogram=True,
    onlySaveData=True,
    plotScatter=False,
    NThresh=10,
    plotNThresh=10,
    saveFeature=True,
    verbose=False,
):
    startTime = TimeModule.time()
    i, j = ij
    if weights is not None:
        if np.sum(weights) == 0 or np.sum(data) == 0:
            if reduceMethod in [
                "mean",
                "dispersion",
                "count",
                "meanfit",
                "dispersionfit",
            ]:
                print(
                    "%i,%i: ended any zeros: %g" % (i, j, TimeModule.time() - startTime)
                )
                # print(weights,data)
                return [i, j, 0]
            else:
                print("reduce method requested:", reduceMethod)
                raise Exception("reduce Method not yet implemented in gaussian fit!")
    if feature is not None:
        if weights is not None:
            origWeights = np.copy(weights)
        else:
            weights = feature
    if ignoreZeros:
        mask = data != 0
        if weights is not None:
            mask1 = weights != 0
            mask = np.logical_and(mask, mask1)
            weights = weights[mask]
        data = data[mask]
    Ndata = np.size(data)
    Nuniq = np.size(np.unique(data))
    if Nuniq <= NThresh:
        if verbose:
            print("Nuniq<NThresh: %i < %i" % (Nuniq, NThresh))
        return [i, j, 0]
    if verbose:
        print("ended ignore zeros: %g" % (TimeModule.time() - startTime))
    startTime = TimeModule.time()
    if feature is not None and Ndata > 1:
        if not ignoreZeros:
            mask = [None]
        weights = origWeights * feature
        if saveFeature:
            feature = np.zeros(np.shape(origWeights))
            feature[mask] = weights[mask] / origWeights[mask]
            feature = feature[mask]
            feature = feature.flatten()
        weights = weights[mask]
    if verbose:
        print("ended feature: %g" % (TimeModule.time() - startTime))
    data = data.flatten()
    if weights is not None:
        weights = weights.flatten()

    if plotHist or "mean" == reduceMethod or "dispersion" == reduceMethod:
        Mean = calcMean(data, weights)
    if plotHist or "dispersion" == reduceMethod:
        Dispersion = calcDispersion(data, weights, Mean, Ndata)
    if (
        "meanfit" in reduceMethod
        or "dispersionfit" in reduceMethod
        or "resFit" in reduceMethod
    ):
        figNameExt += "_%i_%i" % (i, j)
        filename = "../figures/Histogram/%s/GausWeighdataweights%s" % (
            folderExt,
            figNameExt,
        )
        plotFit = plotHist and Ndata >= plotNThresh and "dispersionfit" in reduceMethod
        resFit, Paras, Errors = fitGaussians(
            data,
            weights,
            onlyFirstPeak=True,
            resFit=resFit,
            Nit=10,
            plotFit=plotFit,
            saveHistogram=saveHistogram,
            filename=filename,
            threshFracMax=threshFracMax,
            percentFitMaxError=0.5,
            percentFitMaxErrorPerfection=0.1,
            onlySaveNPZ=onlySaveData,
            saveAll=saveAll,
        )
        Ampl, Means, Dispersions = np.array(Paras).T
    startTime = TimeModule.time()
    if np.isnan(np.sum(Dispersion)) or np.isnan(np.sum(Mean)):
        print(Mean, Dispersion, weights, data)
        raise Exception("value of gaussian fit unacceptable!")
    if abs(Mean) < 1 and Ndata >= 1:
        if verbose:
            print("gauss: mean smaller than 1")
    if reduceMethod == "mean":
        get = Mean
    elif reduceMethod == "dispersion":
        get = Dispersion
    elif reduceMethod == "count":
        get = Ndata
    elif reduceMethod == "countUnique":
        get = Nuniq
    elif reduceMethod == "meanfit":
        get = Means[0]
    elif reduceMethod == "dispersionfit":
        get = Dispersions[0]
    elif reduceMethod == "resFit":
        get = resFit
    else:
        raise Exception("%s method not yet included in pixel integral" % reduceMethod)
    if verbose:
        print("i,j,get", [i, j, get])
    return [i, j, get]  # , WP2


def do_RMmap(
    snap,
    VARIABLE,
    resolution=128,
    ProjBox=1.0,
    center=None,
    numthreads=16,
    axis=[2, 0],
    INFO=None,
    v=1,
    threeDim=False,
    plotCumsum=False,
    resDepth=None,
    depth=None,
    res_critical=1024,
    **kwargs,
):
    print(ProjBox)
    # magnetic field component in line of sight should be gven as VARIABLE
    print(f"resDepth {resDepth}, axis {axis}, ProjBox {ProjBox}")
    if type(VARIABLE) == str:
        VARIABLE = aux.get_value(VARIABLE, snap, INFO=INFO)
    ProjBox = validateArray(ProjBox, dtype=np.float128)
    if center is None:
        center = np.array([x for x in snap.center])
    center = validateArray(center, dtype=np.float128)
    resolution = validateArray(resolution, dtype=int)
    if resDepth is not None:
        resolution[3 - np.sum(axis)] = resDepth
    if depth is not None:
        ProjBox[3 - np.sum(axis)] = depth
    if v > 0:
        print("setup RMMap")
        print("VARIABLE, resolution, ProjBox, center, numthreads, axis, INFO")
        print(VARIABLE, resolution, ProjBox, center, numthreads, axis, INFO)
    # snap.jetr
    # snap.id

    magneticGrid = getAgrid(
        snap,
        VARIABLE,
        res=resolution,
        ProjBox=ProjBox,
        center=center,
        numthreads=numthreads,
        res_critical=res_critical,
    )
    # snap.data["ElectronNumberDensity".lower()] = aux.get_value(
    #     "ElectronNumberDensity", snap, considering_h=False
    # )
    ne = aux.get_value(
        "ElectronNumberDensityWarmPhase", snap, considering_h=False
    )

    electronNGrid = getAgrid(
        snap,
        ne,
        res=resolution,
        ProjBox=ProjBox,
        center=center,
        numthreads=numthreads,
        res_critical=res_critical,
    )
    print(f"looking at masks for rm map: INFO: {INFO}")
    mask = aux.getLocationMaskGrid(snap, INFO, resolution, ProjBox, center, numthreads)
    ProjAxis = 3 - np.sum(axis)
    fullDist = ProjBox[ProjAxis] / float(resolution[ProjAxis])
    if mask is not None:
        fullDist = ProjBox[ProjAxis] / float(resolution[ProjAxis]) * mask
        mask = np.logical_not(mask)
        electronNGrid[mask] = 0.0
        magneticGrid[mask] = 0.0
        print("..........do_RmMap full distance array:")
        debug.printDebug(np.sum(fullDist, axis=ProjAxis))
    distanceInt = fullDist * snap.UnitLength_in_cm
    RM = electronNGrid * magneticGrid * distanceInt
    prefac = 2.64e-17  # e^3/(2 pi m_e^2 c^4)
    # convert cellDepth to cm
    RM *= prefac * 1e4  # to convert to rad m^-2
    if plotCumsum or aux.checkExistenceKey(INFO, "plotRMCumsum", False):
        plotCumSumAlongAxis(
            RM,
            ProjBox=ProjBox,
            axis=axis,
            plotN=100,
            xlabel=r"$r\ [\mathrm{Mpc}]$",
            ylabel=r"$\sum_\mathrm{cum}\mathrm{RM}\ [\mathrm{rad}\ \mathrm{m}^{-2}]$",
            saveFold="../figures/RMCumsum/",
            prefixFile="",
            addFile="",
            color=None,
            alpha=0.6,
            linewidth=2,
            linestyle="-",
            xlog=False,
            ylog=False,
            coloring=True,
            cmap="Spectral",
            **kwargs,
        )
    if not threeDim:
        RM = np.sum(RM, axis=ProjAxis)
        if axis[0] < axis[1]:
            RM = RM.T
    return RM


def do_SZmap(snap, VARIABLE, resolution, ProjBox, center, numthreads, axis, INFO=None):
    # electron number density should be gven as VARIABLE
    import Param

    if "SZTemperatureCutInKeV" in list(INFO.keys()):
        SZTemperatureCut = INFO["SZTemperatureCutInKeV"]
    else:
        SZTemperatureCut = None
    print("SZTemperatureCut")
    print(SZTemperatureCut)
    temp_in_keV = aux.get_Temperature_inkV(snap)
    # temp_in_keV = aux.get_Temperature(snap, considering_h=False, no_conversion = False) #in Kelvin to test!  !!!!!
    # print 'temo in klevin to test!!'
    if SZTemperatureCut is not None:
        mask = temp_in_keV < SZTemperatureCut
        densitygrid = np.zeros(np.shape(VARIABLE))
        densitygrid[mask] = VARIABLE[mask]
    else:
        densitygrid = VARIABLE
    densitygrid = snap.get_Agrid(
        np.array(densitygrid, dtype=np.float128),
        res=resolution,
        box=ProjBox,
        center=center,
        numthreads=numthreads,
    )
    densitygrid = densitygrid["grid"]
    if SZTemperatureCut is not None:
        temperaturegrid = np.zeros(np.shape(temp_in_keV))
        temperaturegrid[mask] = temp_in_keV[mask]
    else:
        temperaturegrid = temp_in_keV
    print("SZTemperatureCut")
    print(SZTemperatureCut)
    temperaturegrid = snap.get_Agrid(
        np.array(temperaturegrid, dtype=np.float128),
        res=resolution,
        box=ProjBox,
        center=center,
        numthreads=numthreads,
    )
    temperaturegrid = temperaturegrid["grid"]
    temperaturegrid *= Param.UnitkeV_in_erg  # convert to erg #Param.BOLTZMANN_cgs/
    ProjAxis = 3 - np.sum(axis)
    SZ = densitygrid * temperaturegrid
    SZ = np.sum(SZ, axis=ProjAxis)
    # convert cellDepth to cm
    distanceInt = (
        ProjBox[ProjAxis] / float(resolution[ProjAxis]) * snap.UnitLength_in_cm
    )
    SZ = (
        Param.THOMSONCROSSSECTION_cgs
        / (Param.ElECTRONMASS_cgs * Param.SPEEDOFLIGHT_cgs ** 2)
        * SZ
        * distanceInt
    )  # to convert to rad m^-2
    if axis[0] < axis[1]:
        SZ = SZ.T
    return SZ


def doPlot(
    projection, snap, VARIABLE, resolution, ProjBox, center, numthreads, axis, INFO=None
):
    if projection == 3:
        data = do_RMmap(
            snap,
            VARIABLE,
            resolutionProjection,
            ProjBox,
            center,
            numthreads,
            axis,
            INFO=INFO,
        )
    else:
        raise Exception("projcetion %i not implemented yet" % projection)
    return data


def gaussianNormed2D(x, y, meanx, meany, variance):
    return (
        1.0
        / (2 * np.pi * variance ** 2)
        * np.exp(
            -(
                (x - meanx) ** 2 / (2 * variance ** 2)
                + (y - meany) ** 2 / (2 * variance ** 2)
            )
        )
    )


def get2DGaussianFilter(iCenter, jCenter, NI, NJ, variance):
    # choose sigma as half of smallest NI/NJ
    # shape of gauss: (NI, NJ)
    x, y = np.arange(NI), np.arange(NJ)
    xx, yy = np.meshgrid(x, y)
    gauss = gaussianNormed2D(xx, yy, iCenter, jCenter, variance)
    return gauss


# getGaussianFit, args=(i,j,
# 					VariableGrid[(i-gaussianWidth)*group:(i+1+gaussianWidth)*group,:,(j-gaussianWidth)*group:(j+1+gaussianWidth)*group],
# 					WeightGrid[(i-gaussianWidth)*group:(i+1+gaussianWidth)*group,:,(j-gaussianWidth)*group:(j+1+gaussianWidth)*group],
# 					reduceMethod, plotHist, figNameExt, folderExt, ignoreZeros, Hist,
# 					gaussianFilter[:,None,:], resFit)) for i,j in zip(indicesX,indicesY)
_tm = 0


def stopwatch(msg=""):
    tm = TimeModule.time()
    global _tm
    if _tm == 0:
        _tm = tm
        return
    print("%s: %.2f seconds" % (msg, tm - _tm))
    _tm = tm


class BigData:
    def __init__(self, function, allIndices, DataNeedPart, NNeedPart, names, *args):
        # set names to none otherwise name entered as first element in function
        self.allIndices = allIndices
        self.DataNeedPart = DataNeedPart
        self.NNeedPart = NNeedPart
        self.args = args
        self.function = function
        self.names = names

    def do_chunk_of_chunks(self, k_start, k_end):
        if self.names is not None:
            s = [
                self.function(
                    self.names[k],
                    *(
                        dnp[tuple(self.allIndices[k])]
                        for dnp in [self.DataNeedPart[i] for i in range(self.NNeedPart)]
                    ),
                    *self.args,
                )
                for k in range(k_start, k_end)
            ]
        else:
            s = [
                self.function(
                    *(
                        dnp[tuple(self.allIndices[k])]
                        for dnp in [self.DataNeedPart[i] for i in range(self.NNeedPart)]
                    ),
                    *self.args,
                )
                for k in range(k_start, k_end)
            ]
        sys.stderr.write(".")
        return s

    def do_multi(self, numproc):
        procs = []
        pool = mp.Pool(numproc)
        stopwatch("\nPool setup")
        jobs = list(
            map(int, np.linspace(0, np.shape(self.allIndices)[0], numproc * 4 + 1))
        )
        for i in range(len(jobs) - 1):
            jobs_start, jobs_end = jobs[i : i + 2]
            p = pool.apply_async(do_chunk_wrapper, (jobs_start, jobs_end))
            procs.append(p)
        stopwatch("Jobs queued (%d processes)" % numproc)
        results = []
        for k, p in enumerate(procs):
            results += p.get()  # timeout=5) # timeout allows ctrl-C interrupt
            if k == 0:
                stopwatch("\nFirst get() done")
        stopwatch("Jobs done")
        pool.close()
        pool.join()
        return results


def do_chunk_wrapper(k_start, k_end):
    return bd.do_chunk_of_chunks(k_start, k_end)


def number_of_digits_post_decimal(x):
    count = 0
    residue = x - int(x)
    if residue != 0:
        multiplier = 1
        while not (x * multiplier).is_integer():
            count += 1
            multiplier = 10 * multiplier
        return count
    else:
        return count


def checkNiceNumber(number):
    if number_of_digits_post_decimal(number) < 6:
        return True
    else:
        return False


def determineCorrectWidthPlot(
    resolution,
    group,
    height,
    width,
    maxChange=0.01,
    incrementInc=0.0001,
    firstCall=True,
):
    # returns height and width where spacing in x/y give integer number of pixels
    spacing = height / (resolution / group)
    remainder = width % spacing
    corr = width
    somethingFound = True
    if remainder != 0:
        corr += remainder
        i = 0
        while not checkNiceNumber(corr):
            if i * spacing > maxChange:
                somethingFound = False
                break
            remainder = (width + i * spacing) % spacing
            corr = (width + i * spacing) - remainder
            i += 1
            print(
                "1st while i",
                i,
                "corr",
                corr,
                "remainder",
                "(width+i*spacing)%spacing",
                (width + i * spacing) % spacing,
                "corr% spacing",
                corr % spacing,
            )
    if not somethingFound:
        somethingFound = True
        i = 0
        corr = width + width % spacing
        while not checkNiceNumber(corr):
            if i * spacing > maxChange or width - i * spacing < 0:
                somethingFound = False
                break
            remainder = (width - i * spacing) % spacing
            corr = (width - i * spacing) - remainder
            i += 1
            print("2nd while i", i)
    if somethingFound:
        print("return found result")
        return height, corr
    if not firstCall:
        return None
    if not somethingFound:
        for i in range(int(maxChange / incrementInc)):
            ret = determineCorrectWidthPlot(
                resolution,
                group,
                height + i * incrementInc,
                width,
                maxChange=0.01,
                incrementInc=0.0001,
                firstCall=False,
            )
            if ret is not None:
                return ret
        for i in range(int(maxChange / incrementInc)):
            if height - i * incrementInc > 0:
                ret = determineCorrectWidthPlot(
                    resolution,
                    group,
                    height - i * incrementInc,
                    width,
                    maxChange=0.01,
                    incrementInc=0.0001,
                    firstCall=False,
                )
            if ret is not None:
                return ret
    print("nothing found!!!")
    return corr


def get_vmin_vmax(
    vmin_initial,
    vmax_initial,
    info,
    time_float,
    data,
    symlog=False,
    variable=None,
    model_vmin_vmax=None,
    vmin_mapping=lambda data, vmax: 10.0 ** (int(np.log10(vmax)) - 3),
    vmax_mapping=lambda x: np.max(x),
    vmin_negative_mapping=lambda data, vmax: np.min(data),
):
    if symlog:
        data = np.abs(data)
    negativeVMin = False
    if type(vmin_initial) == str:
        print(f"vmin_initial is string!!: {vmin_initial}")
    if vmin_initial is not None and vmin_initial < 0:
        negativeVMin = True
    elif vmin_initial is None and np.min(data) < 0:
        negativeVMin = True
    if vmax_initial is None:
        vmax = vmax_mapping(data)  # np.max(data)
    else:
        vmax = vmax_initial
        if model_vmin_vmax is not None:
            if variable in model_vmin_vmax:
                vmax = info["modelfunc"](time_float, info["modelPara"])
    if (vmin_initial is None) and vmax != 0 and not negativeVMin:
        vmin = vmin_mapping(data, vmax)  # 10.0 ** (int(np.log10(vmax)) - 3)
    elif vmin_initial is None and negativeVMin:
        vmin = vmin_negative_mapping(data, vmax)  # np.min(data)
    else:
        vmin = vmin_initial
        if model_vmin_vmax is not None:
            if variable in model_vmin_vmax:
                vmin = vmax * 1e-3
    if vmin is None or vmax is None:
        if (
            aux.checkIfInDictionary("allow0VminVax", info, False)
            and np.min(data) == 0
            and np.max(data) == 0
        ):
            vmin = 0
            vmax = 0
        elif vmin is None and vmax == 0 and not negativeVMin:
            # if self.verbose:
            print("all values are 0 -> setting vmin=0.1, vmax=1 (so log is availale)")
            vmin = 0.1
            vmax = 1
        else:
            raise Exception("vmin or vmax is None")
    if aux.checkIfInDictionary("vmin_vmax_symmetric_zero", info, True) and (
        vmin < 0 and vmax > 0
    ):
        vmin, vmax = -np.max([abs(vmin), vmax]), np.max([abs(vmin), vmax])
    return vmin, vmax


def do_PixelIntegral(
    snap,
    variable,
    weight,
    resolution,
    ProjBox,
    center,
    numthreads,
    axis,
    INFO,
    plotHist=True,
    Hist="dataweights",
    histNameExt="velocities",
    verbose=False,
    save_histograms_single_thread=False,
    res_critical=2048,
):
    aux.timer("do_PixelIntegral")
    start_time = TimeModule.time()
    VARIABLE = variable
    if weight is not None:
        if type(weight) == str:
            WEIGHTS = aux.get_value(
                weight,
                snap,
                check_for_0=False,
                convert_units=False,
                considering_h=False,
                INFO=INFO,
            )
        else:
            WEIGHTS = weight[:]
        # WEIGHTS = copy.deepcopy(weight)
    VARIABEL = copy.deepcopy(VARIABLE)

    ProjAxis = 3 - np.sum(axis)
    OverflowVar = False
    # variableAgrid = snap.get_Agrid( np.array(VARIABLE,dtype=np.float128), res=resolution, box=ProjBox, center=center ,numthreads=numthreads)
    # VariableGrid = variableAgrid['grid']
    VariableGrid = getAgrid(
        snap,
        VARIABLE,
        res=resolution,
        ProjBox=ProjBox,
        center=center,
        numthreads=numthreads,
        res_critical=res_critical,
    )

    if weight is not None:
        WeightGrid = getAgrid(
            snap,
            WEIGHTS,
            res=resolution,
            ProjBox=ProjBox,
            center=center,
            numthreads=numthreads,
            res_critical=res_critical,
        )
        # snap.get_Agrid( np.array(WEIGHTS,dtype=np.float128), res=resolution, box=ProjBox, center=center,numthreads=numthreads)
        # WeightGrid = weightAgrid['grid']
        # aux.printDebug(WeightGrid,e=False,zero=True)
    group = aux.checkExistenceKey(INFO, "group", 1)
    reduceMethod = aux.checkExistenceKey(INFO, "PixIntRetu", False)
    ignoreZeros = aux.checkIfInDictionary("ignoreZerosPixelInt", INFO, False)
    gaussianSmooth = aux.checkIfInDictionary("gaussianSmoothPixelInt", INFO, False)
    gaussianWidth = aux.checkIfInDictionary(
        "gaussianIncludeWidthInGroupsPixelInt", INFO, 10
    )
    gaussianSigma = aux.checkIfInDictionary("gaussianSigmaInGroupsPixelInt", INFO, 10)
    saveAll = aux.checkIfInDictionary("gaussianSaveAllHistograms", INFO, False)
    saveHistogram = aux.checkIfInDictionary("gaussiansaveHistogram", INFO, True)
    print(
        "group: %i, reduceMethod: %s, ignoreZeros: %i, gaussianSmooth: %i, gaussianWidth: %i, gaussianSigma: %i, saveAll: %i, saveHistogram: %i"
        % (
            group,
            reduceMethod,
            ignoreZeros,
            gaussianSmooth,
            gaussianWidth,
            gaussianSigma,
            saveAll,
            saveHistogram,
        )
    )
    if not gaussianSmooth:
        gaussianWidth = gaussianSigma = 0
    resFit = aux.checkIfInDictionary("fitGaussianBinInKmsPixelInt", INFO, 15)
    print("resFit used for pixel integral: %i" % resFit)
    threshFracMax = aux.checkIfInDictionary("fitGaussianthreshFracMax", INFO, 0)
    if verbose:
        print(
            "pixel int reduce %s, gaussianSmooth:%i with width %i and sigma %i, resFit %i"
            % (reduceMethod, gaussianSmooth, gaussianWidth, gaussianSigma, resFit)
        )
    # checking which resolution would not cut off pixels;
    if resolution[axis[0]] % group > 1e-6:
        print(resolution, axis, group)
        if resolution[axis[0]] < resolution[axis[1]]:
            resolutionTheo = resolution[axis[1]]
            resTest = resolutionTheo / resolution[axis[1]] * resolution[axis[0]]
            while resTest % group > 1e-6:
                resolutionTheo += 1
                resTest = resolutionTheo / resolution[axis[1]] * resolution[axis[0]]
            print("Ax0U: increase resolution to %i" % (resolutionTheo))
        else:
            resolutionTheo = resolution[axis[0]]
            resTest = resolutionTheo
            while resTest % group > 1e-6:
                resolutionTheo += 1
                resTest = resolutionTheo
            print("Ax0L: increase resolution to %i" % (resolutionTheo))
        print(resolution[axis[0]], group)
        raise Exception(
            "Missing %i values as resolution %i elements not divisible by grouping factor %i in pixel integral!!!"
            % (resolution[axis[0]] % group, resolution[axis[0]], group)
        )
    Nx = int(resolution[axis[0]] / group)
    # checking which resolution would not cut off pixels;
    if resolution[axis[1]] % group > 1e-6:
        if resolution[axis[1]] < resolution[axis[0]]:
            resolutionTheo = resolution[axis[0]]
            resTest = resolutionTheo / resolution[axis[0]] * resolution[axis[1]]
            while resTest % group > 1e-6:
                resolutionTheo += 1
                resTest = resolutionTheo / resolution[axis[0]] * resolution[axis[1]]
            print("Ax1U: increase resolution to %i" % (resolutionTheo))
        else:
            resolutionTheo = resolution[axis[1]]
            resTest = resolutionTheo
            while resTest % group > 1e-6:
                resolutionTheo += 1
                resTest = resolutionTheo
            print("Ax1L: increase resolution to %i" % (resolutionTheo))
        print(resolution[axis[1]], group)
        raise Exception(
            "Missing %i values as resolution %i elements not divisible by grouping factor %i in pixel integral!!!"
            % (resolution[axis[1]] % group, resolution[axis[1]], group)
        )
    Ny = int(resolution[axis[1]] / group)
    if axis[0] != 2 or axis[1] != 0:
        print("axis,", axis)
        raise Exception("only axis [2,0] implemented in pixel integral!")
    if gaussianSmooth and axis[0] == 2 and axis[1] == 0:
        VariableGrid = np.pad(
            VariableGrid,
            (
                (group * gaussianWidth, group * gaussianWidth),
                (0, 0),
                (group * gaussianWidth, group * gaussianWidth),
            ),
            "constant",
            constant_values=0,
        )
        WeightGrid = np.pad(
            WeightGrid,
            (
                (group * gaussianWidth, group * gaussianWidth),
                (0, 0),
                (group * gaussianWidth, group * gaussianWidth),
            ),
            "constant",
            constant_values=0,
        )
        gaussianFilter = get2DGaussianFilter(
            group * gaussianWidth,
            group * gaussianWidth,
            (2 * gaussianWidth + 1) * group,
            (2 * gaussianWidth + 1) * group,
            gaussianSigma,
        )[:, None, :]
    else:
        gaussianFilter = None
    if ignoreZeros:
        ZeroCheck = np.sum(VariableGrid * WeightGrid, axis=3 - np.sum(axis))
        indicesX, indicesY = np.where(ZeroCheck != 0)
        indicesXZeros, indicesYZeros = np.where(ZeroCheck == 0)
        # indicesX, indicesY = indicesX-group*gaussianWidth, indicesY-group*gaussianWidth
        indicesX, indicesY = np.unique(
            [
                (indicesX - indicesX % group) / group,
                (indicesY - indicesY % group) / group,
            ],
            axis=1,
        )
        indicesXZeros, indicesYZeros = np.unique(
            [
                (indicesXZeros - indicesXZeros % group) / group,
                (indicesYZeros - indicesYZeros % group) / group,
            ],
            axis=1,
        )
        # indicesX, indicesY = np.unique([(indicesX-indicesX%group)/group,(indicesY-indicesY%group)/group], axis=1)
        indicesX, indicesY = (
            np.array(indicesX, dtype=int),
            np.array(indicesY, dtype=int),
        )
        indicesXZeros, indicesYZeros = (
            np.array(indicesXZeros, dtype=int),
            np.array(indicesYZeros, dtype=int),
        )
    else:
        indicesX, indicesY = np.meshgrid(np.arange(Ny), np.arange(Nx))
        indicesX, indicesY = indicesX.flatten(), indicesY.flatten()
        if gaussianSmooth:
            indicesX += gaussianWidth
            # indicesY += gaussianWidth
    if numthreads != 1:
        allIndices = [
            [
                slice((i - gaussianWidth) * group, (i + 1 + gaussianWidth) * group),
                slice(None),
                slice((j - gaussianWidth) * group, (j + 1 + gaussianWidth) * group),
            ]
            for i, j in zip(indicesX, indicesY)
        ]
        figNameExt = "%s_%s_R%s_G%i" % (
            histNameExt,
            aux.getSimName(snap),
            aux.convert_params_to_string(resolution),
            group,
        )
        folderExt = "%s/%s/Group%i/ResFit%i/" % (
            aux.getSimName(snap),
            histNameExt,
            group,
            resFit,
        )
        aux.create_dirs_for_file("../figures/Histogram/%s/test.txt" % folderExt)
        args = (
            reduceMethod,
            plotHist,
            figNameExt,
            folderExt,
            ignoreZeros,
            Hist,
            gaussianFilter,
            resFit,
            threshFracMax,
            saveAll,
            saveHistogram,
            save_histograms_single_thread,
        )
        global bd
        bd = BigData(
            getGaussianFit,
            allIndices,
            [VariableGrid, WeightGrid],
            2,
            np.array([indicesX, indicesY]).T,
            *args,
        )
        results = bd.do_multi(numthreads)
        results = np.array(results).T
        Results = np.zeros((Nx, Ny))
        try:
            Results[
                (
                    np.array(results[1] - gaussianWidth, dtype=int),
                    np.array(results[0] - gaussianWidth, dtype=int),
                )
            ] = results[2]
        except:
            Results[
                (
                    np.array(results[1] - gaussianWidth, dtype=int),
                    np.array(results[0] - gaussianWidth, dtype=int),
                )
            ] = results[2]

    else:
        Results = np.zeros((Nx, Ny))
        figNameExt = "%s_%s_R%s_G%i" % (
            histNameExt,
            aux.getSimName(snap),
            aux.convert_params_to_string(resolution),
            group,
        )
        folderExt = "%s/%s/Group%i/ResFit%i/" % (
            aux.getSimName(snap),
            histNameExt,
            group,
            resFit,
        )
        if axis[0] == 2 and axis[1] == 0:
            if not gaussianSmooth:
                for i in range(Nx):
                    for j in range(Ny):
                        idI, idY, Results[i, j] = getGaussianFit(
                            i,
                            j,
                            VariableGrid[
                                j * group : (j + 1) * group,
                                :,
                                i * group : (i + 1) * group,
                            ],
                            WeightGrid[
                                j * group : (j + 1) * group,
                                :,
                                i * group : (i + 1) * group,
                            ],
                            reduceMethod,
                            plotHist,
                            figNameExt,
                            folderExt,
                            ignoreZeros,
                            Hist,
                            None,
                            resFit,
                            threshFracMax,
                        )
            else:
                for i in range(gaussianWidth, gaussianWidth + Nx):
                    for j in range(gaussianWidth, gaussianWidth + Ny):
                        (
                            idI,
                            idY,
                            Results[i - gaussianWidth, j - gaussianWidth],
                        ) = getGaussianFit(
                            i,
                            j,
                            VariableGrid[
                                (j - gaussianWidth)
                                * group : (j + 1 + gaussianWidth)
                                * group,
                                :,
                                (i - gaussianWidth)
                                * group : (i + 1 + gaussianWidth)
                                * group,
                            ],
                            WeightGrid[
                                (j - gaussianWidth)
                                * group : (j + 1 + gaussianWidth)
                                * group,
                                :,
                                (i - gaussianWidth)
                                * group : (i + 1 + gaussianWidth)
                                * group,
                            ],
                            reduceMethod,
                            plotHist,
                            figNameExt,
                            folderExt,
                            ignoreZeros,
                            Hist,
                            gaussianFilter[:, None, :],
                            resFit,
                            threshFracMax,
                        )

        else:
            raise Exception("Error: This axis is not implemented yet!")

    # save figures from saved data (as not possible in parallel)
    if save_histograms_single_thread:
        filenameList = findAllFilesWithEnding(
            "../figures/Histogram/%s/" % folderExt, ".npz"
        )
        print("gonna save %i files as pdf" % np.size(filenameList))
        aux.timer("saveALLFILES")
        for indexFile, filename in enumerate(filenameList):
            aux.timer("save_single_file%i" % indexFile)
            file = np.load(filename)
            save_histogram(
                file["data"],
                weights=file["weights"],
                bins=file["bins"],
                filename=filename[:-4],
                parameterNames=["\\mathrm{A}", "\\langle v \\rangle", "\\sigma"],
                plotFunctionParameters=file["plotFunctionParameters"],
                plotFunctionErrors=file["plotFunctionErrors"],
                xlabel=r"$v\ [\mathrm{km}\ \mathrm{s}^{-1}]$",
                ylabel=r"$\mathrm{counts}\ [\mathrm{arbitrary}\ \mathrm{units}]$",
                filenameEnd="",
                onlySaveNPZ=False,
            )

            try:
                os.remove(filename)
            except:
                print("couldnt remove file %s!" % filename)
            aux.timer("save_single_file%i" % indexFile, end=1)
        aux.timer("", printAll=1)
        aux.timer("saveALLFILES", end=1)
    # get projection data
    # get image from averaging
    print("pixel integral calcs took %g" % (TimeModule.time() - start_time))
    if axis[0] > axis[1]:
        Results = Results.T
    aux.timer("do_PixelIntegral", end=1, delete=1)
    return Results


def findAllFilesWithEnding(FOLDERS, ending, verbose=0):
    if type(FOLDERS) == str:
        FOLDERS = [FOLDERS]
    returnFiles = []
    for folder in FOLDERS:
        if not os.path.isdir(folder):
            if verbose:
                print("findAllFilesWithEnding: folder, %s , doesnt exist" % (folder))
            continue
        for root, dirs, files in os.walk(folder):
            for file in files:
                if file.endswith(ending):
                    returnFiles.append(os.path.join(root, file))
    return returnFiles


def sortVminVmax(
    VminVmax,
    findCommonAlong="y",
    newEveryColumn=True,
    commonVminVmax=False,
    saveVminVmax=False,
    header=None,
    filename="test.txt",
):
    if saveVminVmax and header is None:
        print(header)
        exit("need header to save vminvmax!")

    print("VminVmax at start of sortVminVmax")
    print(VminVmax)
    print("findCommonAlong", findCommonAlong)
    sizeX, sizeY = np.shape(VminVmax)[0], np.shape(VminVmax)[1]
    if not findCommonAlong == "x":
        vmin = [VminVmax[i][0][0] for i in range(sizeX)]
        vmax = [VminVmax[i][0][1] for i in range(sizeX)]
    if not commonVminVmax:
        vmin = [[x[0] for x in y] for y in VminVmax]
        vmax = [[x[1] for x in y] for y in VminVmax]

    elif findCommonAlong == "xy":
        vmin = VminVmax[0][0][0]
        vmax = VminVmax[0][0][1]
        for i in range(sizeX):
            for j in range(sizeY):
                if VminVmax[i][j][0] < vmin:
                    vmin = VminVmax[i][j][0]
                if VminVmax[i][j][1] > vmax:
                    vmax = VminVmax[i][j][1]
        if vmin == vmax:
            vmax = 2 * vmin + 1
            if vmin == 0:
                vmax = 1
        vmin = [[vmin for y in range(sizeY)] for x in range(sizeX)]
        vmax = [[vmax for y in range(sizeY)] for x in range(sizeX)]

    elif findCommonAlong == "y":
        for i in range(sizeX):
            for j in range(sizeY):
                if VminVmax[i][j][0] < vmin[i]:
                    vmin[i] = VminVmax[i][j][0]
                if VminVmax[i][j][1] > vmax[i]:
                    vmax[i] = VminVmax[i][j][1]
        vmin = [[x for i in range(sizeY)] for x in vmin]
        vmax = [[x for i in range(sizeY)] for x in vmax]

    elif findCommonAlong == "x":
        print("findCommonAlong == x start")
        print(VminVmax)
        vmin = [VminVmax[0][i][0] for i in range(sizeY)]
        vmax = [VminVmax[0][i][1] for i in range(sizeY)]
        print("VminVmax first step")
        print(VminVmax)
        print(vmin)
        print(vmax)
        for i in range(sizeX):
            for j in range(sizeY):
                if VminVmax[i][j][0] < vmin[j]:
                    vmin[j] = VminVmax[i][j][0]
                if VminVmax[i][j][1] > vmax[j]:
                    vmax[j] = VminVmax[i][j][1]
        print("VminVmax second step")
        print(VminVmax)
        print(vmin)
        print(vmax)
        vmin = [[x for x in vmin] for i in range(sizeX)]
        vmax = [[x for x in vmax] for i in range(sizeX)]
        print(vmin, vmax)
        print("findCommonAlong == x done")
        # exit()

    else:
        exit("not implemented yet in sortVminVmax()")
    if saveVminVmax:
        vminSave = np.array(vmin).flatten()
        vmaxSave = np.array(vmax).flatten()
        combined = list(vminSave) + list(vmaxSave)
        if np.size(combined) / 2 != np.size(header):
            print(combined)
            print(header)
            exit("incorrect header size to save vmin vmaxes!")
        else:
            import MultiImshow as MI

            header = ["%sVmin" % x for x in header] + ["%sVmax" % x for x in header]
            MI.appendFile(filename, tuple(combined), tuple(header))
    print(vmin, vmax)
    print("vmin vmax in sortVminVmax")

    return vmin, vmax


def getVminVmax(
    vmin,
    vmax,
    data,
    ChooseVmax=True,
    ChooseVmin=True,
    time=None,
    modelVmax=False,
    modelVmin=False,
    modelfunc=None,
    modelPara=None,
    variable=None,
    modelvariables=None,
    symmetricZero=True,
    INFO=None,
):
    negativeVMin = False
    if vmin is not None and vmin < 0:
        negativeVMin = True
    elif vmin is None and np.min(data) < 0:
        negativeVMin = True
    print("negativeVMin %i" % int(negativeVMin))
    if ChooseVmax or vmax is None:
        vmax = np.max(data)
        print(data)
        print("max data %g" % vmax)
    else:
        vmax = vmax
        if modelVmax:
            if variable in modelvariables:
                print(vmax)
                vmaxPre = copy.deepcopy(vmax)
                vmax = modelfunc(
                    time, modelPara
                )  # float(vmaxPre)*np.exp(-0.3*time/20.)
                print("vmax for CR energy density")
                print(vmax)
    if (ChooseVmin or vmin is None) and vmax != 0 and not negativeVMin:
        print("here")
        vmin = 10.0 ** (int(np.log10(vmax)) - 3)
        if vmin < np.min(data):
            vmin = np.min(data)
        print("--------------")
        print("missing fraction %g" % (np.sum(data[data < vmin]) / np.sum(data)))
        print("---------------")
    elif ChooseVmin or vmin is None and negativeVMin:
        vmin = np.min(data)
    else:
        vmin = vmin
        if modelVmin:
            if variable in modelvariables:
                print(vmin)
                vminPre = copy.deepcopy(vmin)
                vmin = vmax * 1e-3  # float(vminPre)*np.exp(-0.3*time/20.)
                print("vmin for CR energy density")
                print(vmin)
    if vmin is None or vmax is None:
        if (
            aux.checkIfInDictionary("allow0VminVax", INFO, False)
            and np.min(data) == 0
            and np.max(data) == 0
        ):
            vmin = 0
            vmax = 0
        elif vmin is None and vmax == 0 and not negativeVMin:
            print("all values are 0 -> setting vmin=0.1, vmax=1 (so log is availale)")
            vmin = 0.1
            vmax = 1
        else:
            print(vmin, vmax, ChooseVmax, ChooseVmin)
            debug.printDebug(data, zero=True)
            raise Exception("vmin or vmax is None")
    if symmetricZero and (vmin < 0 and vmax > 0):
        vmin, vmax = -np.max([abs(vmin), vmax]), np.max([abs(vmin), vmax])
    return vmin, vmax


def getExtension(Box, center, axis):
    if len(Box) == 3:
        Box = [Box[axis[0]], Box[axis[1]]]
    ext = (
        center[axis[0]] - (Box[0]) * 0.5,
        center[axis[0]] + (Box[0]) * 0.5,
        center[axis[1]] - (Box[1]) * 0.5,
        center[axis[1]] + (Box[1]) * 0.5,
    )
    return ext


def get_colors_examples_gradients(sample="mute", skip_colors=[]):
    if sample == "default":
        colorstart = [
            # "#b6bfc8",
            "#e57f7f",
            "#99eaea",
            # "#a0a9ab",
            "#d2c9ff",
            "#82b983",
            "#f5e9e5",
            "#8e768a",
            "#4cdbdb",
            "#fbf3b0",
            "#daae8a",
            "#a4b4d4",
            "#cfdbc3",
            "#d691d3",
            "#75ffcf",
            "#916373",
            "#bab496",
            "#ffc284",
            "#cff767",
            "#72c0d5",
        ]
        colorfinish = [
            # "#091d32",
            "#510000",
            "#003d3d",
            # "#273134",
            "#4c4766",
            "#264d27",
            "#a5938e",
            "#2f1e2c",
            "#005151",
            "#ada244",
            "#5b2f0b",
            "#253555",
            "#58614e",
            "#571154",
            "#0c7f57",
            "#f6c0d2",
            "#6d684a",
            "#b27d47",
            "#699101",
            "#0e6982",
        ]
    elif sample == "mute":
        colorstart = [
            "#b6bfc8",
            "#e57f7f",
            "#f5e9e5",
            "#8e768a",
            "#d2c9ff",
            "#82b983",
            "#99eaea",
            "#a0a9ab",
            "#4cdbdb",
            "#fbf3b0",
            "#daae8a",
            "#a4b4d4",
            "#cfdbc3",
            "#d691d3",
            "#75ffcf",
            "#916373",
            "#bab496",
            "#ffc284",
            "#cff767",
            "#72c0d5",
            "#f6b2b2",
            "#c7e9ed",
            "#a4c0d6",
            "#a5d1ee",
            "#b2ffe5",
            "#9fefe7",
            "#a5a5a5",
            "#9cd9bb",
            "#ff7f7f",
            "#ded6d6",
            "#f5ecec",
            "#af5552",
            "#ede7f9",
            "#c6e6b9",
            "#dbf0f3",
            "#e5ddfc",
            "#f6eee6",
            "#e9ead2",
            "#e9ddc5",
            "#c1c5ce",
            "#e4e4e4",
            "#e85fc0",
            "#59995a",
            "#eee878",
            "#d9e0f6",
            "#b25e6c",
            "#b7ed93",
            "#6bb1c0",
            "#c89b54",
            "#754eab",
            "#c1f1b3",
            "#c2d3fc",
        ]
        colorfinish = [
            "#091d32",
            "#510000",
            "#a5938e",
            "#2f1e2c",
            "#4c4766",
            "#264d27",
            "#003d3d",
            "#273134",
            "#005151",
            "#ada244",
            "#5b2f0b",
            "#253555",
            "#58614e",
            "#571154",
            "#0c7f57",
            "#f6c0d2",
            "#6d684a",
            "#b27d47",
            "#699101",
            "#0e6982",
            "#904c4c",
            "#7b9ca1",
            "#537895",
            "#3c82b0",
            "#65cca9",
            "#33b3a6",
            "#545454",
            "#489a71",
            "#cc0000",
            "#999393",
            "#928c8c",
            "#7c221f",
            "#8a8594",
            "#847e78",
            "#93a5a8",
            "#b2aac9",
            "#dad0c6",
            "#afb090",
            "#b6aa92",
            "#858a94",
            "#9b9b9b",
            "#9c1274",
            "#106312",
            "#b9b232",
            "#a0a9c2",
            "#741424",
            "#74a354",
            "#237384",
            "#b1710c",
            "#2e026c",
            "#74a466",
            "#869ac8",
        ]
    elif sample == "bright":
        colorstart = [
            "red",
            "mediumblue",
            "forestgreen",
            "orange",  # orange goldenrod
            "mediumorchid",
            "turquoise",
            "springgreen",
            "chocolate",
            "#9bb5cc",
            "#ffcbd3",
            "#b2b2d8",
            "#70b766",
        ]
        colorstart = [aux.color_to_hex(c) for c in colorstart]
        colorfinish = [
            "maroon",
            "midnightblue",
            "darkgreen",
            "peru",  # darkorange
            "darkmagenta",
            "cadetblue",
            "aquamarine",
            "peru",
            "#021526",
            "#66484d",
            "#00004c",
            "#084400",
        ]
        colorfinish = [aux.color_to_hex(c) for c in colorfinish]
    else:
        raise Exception(f"sample {sample} not available!")
    if skip_colors:
        if len(skip_colors) == 1:
            colorstart = colorstart[skip_colors[0] + 1 :]
            colorfinish = colorfinish[skip_colors[0] + 1 :]
        else:
            index_color = 0
            cs, cf = [], []
            for index_skip, s in enumerate(skip_colors):
                index_color += s
                # if s == 0 and index_skip != 0:
                #     index_color += 1
                cs.append(colorstart[index_color])
                cf.append(colorfinish[index_color])
                index_color += 1
            cs.extend(colorstart[index_color:])
            cf.extend(colorfinish[index_color:])
            colorstart, colorfinish = cs, cf
        if not colorstart or not colorfinish:
            raise Exception(
                f"skipped too many colors! colorstart: {colorstart}, colorfinish: {colorfinish}"
            )
    return colorstart, colorfinish


def get_colors_sample_grouped(from_dark_to_light=True):
    colors = [
        ["lightcoral", "indianred", "rosybrown", "maroon",],  # /red
        ["sandybrown", "gold", "orange",],  # yellows/oranges
        ["yellowgreen", "limegreen", "olive", "forestgreen",],  # greens
        ["lightseagreen", "teal", "lightskyblue", "aqua",],  # turquoise
        [
            "deepskyblue",  # blue
            # "lightskyblue",
            "cornflowerblue",
            "navy",
        ],
        ["rebeccapurple", "indigo", "fuchsia", "magenta", "orchid",],  # violet
    ]
    if from_dark_to_light:
        colors = [x[::-1] for x in colors]
    return colors


def get_linestyles():
    linestyle_tuple = [
        ("loosely dashed", (0, (5, 10))),
        ("dashed", (0, (5, 5))),
        ("densely dashed", (0, (5, 1))),
        ("loosely dashdotted", (0, (3, 10, 1, 10))),
        ("dashdotted", (0, (3, 5, 1, 5))),
        ("densely dashdotted", (0, (3, 1, 1, 1))),
        ("dashdotdotted", (0, (3, 5, 1, 5, 1, 5))),
        ("loosely dashdotdotted", (0, (3, 10, 1, 10, 1, 10))),
        ("densely dashdotdotted", (0, (3, 1, 1, 1, 1, 1))),
        ("loosely dotted", (0, (1, 10))),
        ("dotted", (0, (1, 5))),
        ("densely dotted", (0, (1, 1))),
    ]
    dic = {}
    for x in linestyle_tuple:
        dic[x[0]] = x[1]
    return dic


def get_alpha_grouped(grouping, min=0.3):
    alphas = []
    for index, g in enumerate(grouping):
        alphas.extend(np.linspace(0.3, 1, g))
    return alphas


def get_colors_grouped(
    grouping,
    colors_start=None,
    colors_finish=None,
    group_via_gradient=False,
    color_type="mute",
    invert_finish_start=False,
    skip_colors=[],
):
    grouping = [int(x) for x in grouping]
    cs, cf = get_colors_examples_gradients(color_type, skip_colors=skip_colors)
    colors_grouped = get_colors_sample_grouped()
    if colors_start is None:
        colors_start = cs[:]
    if colors_finish is None:
        colors_finish = cf[:]
    if invert_finish_start:
        colors_start, colors_finish = colors_finish, colors_start
    colors = []
    for index, g in enumerate(grouping):
        if group_via_gradient:
            c = aux.linear_gradient(colors_start[index], colors_finish[index], n=g)
        else:
            spacing = find_max_spacing_list(g, len(colors_grouped))
            print(f"index: {index}")
            print(f"spacing: {spacing}")
            print(f"colors_grouped: {colors_grouped}")
            i = index * spacing
            if i == len(colors_grouped):
                i -= 1
            c = colors_grouped[i][:g]
        colors.extend(c)
    return colors


def get_linestyles_grouped(grouping, linestyles=None, n_groups_linestyles=None):
    if linestyles is None:
        linestyles = get_linestyles()
        n_groups_linestyles = 4
    if len(grouping) > n_groups_linestyles:
        raise Exception(
            f"not sufficient groups of linestyles {n_groups_linestyles} for required groups {grouping}"
        )
    styles = []
    ls_per_group = len(linestyles) // n_groups_linestyles
    for index, g in enumerate(grouping):
        if g > ls_per_group:
            raise Exception(
                f"not sufficient ls_per_group {ls_per_group} for group {g} (grouping: {grouping})"
            )
        if g > 0:
            sub_styles = list(linestyles.values())[
                ls_per_group * index : ls_per_group * index + ls_per_group
            ]
            styles.extend(sub_styles[:: find_max_spacing_list(g, ls_per_group)])
    return styles


def find_max_spacing_list(n_objects, length):
    spacing = length // n_objects
    if n_objects == 2 and length % n_objects:
        spacing += 1
    return spacing


def getColor(
    x,
    y,
    z,
    sizeX,
    sizeY,
    sizeZ,
    useGradient=True,
    AlongColor=1,
    AlongColorGradient=2,
    color=None,
    colorstart=None,
    colorfinish=None,
    color_type="mute",
    skip_colors=[],
):
    colorstart, colorfinish = get_colors_examples_gradients(
        color_type, skip_colors=skip_colors
    )
    if color is None:
        color = colorstart[:]
    List = [x, y, z]
    sizeList = [sizeX, sizeY, sizeZ]
    if AlongColor == AlongColorGradient:
        exit("color cant change at same axis of gradient!")
    if AlongColor == 0:
        if sizeX > np.size(color):
            exit("need to specify more colors")
    elif AlongColor == 1:
        if sizeY > np.size(color):
            exit("need to specify more colors")
    elif AlongColor == 2:
        if sizeZ > np.size(color):
            exit("need to specify more colors")
    else:
        exit(
            "need to specify along which axis want different colors, choose between x-z -> 0-2"
        )

    if type(AlongColorGradient) == str:
        pass
    elif AlongColorGradient == 0:
        if sizeX > np.size(colorstart) or sizeX > np.size(colorfinish):
            exit("need to specify more finish or start colors for color gradient")
    elif AlongColorGradient == 1:
        if sizeY > np.size(colorstart) or sizeY > np.size(colorfinish):
            exit("need to specify more finish or start colors for color gradient")
    elif AlongColorGradient == 2:
        if sizeZ > np.size(colorstart) or sizeZ > np.size(colorfinish):
            exit("need to specify more finish or start colors for color gradient")
    else:
        exit(
            "need to specify along which axis want different color gradient, choose between x-z -> 0-2"
        )
    if useGradient:
        if type(AlongColorGradient) == str:
            print(
                [
                    List[int(x)]
                    if index == 0
                    else List[int(x)]
                    * np.prod([sizeList[int(s)] for s in AlongColorGradient[0:index]])
                    for index, x in enumerate(AlongColorGradient)
                ]
            )
            start = colorstart[
                sum(
                    [
                        List[int(x)]
                        if index == 0
                        else List[int(x)]
                        * np.prod(
                            [sizeList[int(s)] for s in AlongColorGradient[0:index]]
                        )
                        for index, x in enumerate(AlongColorGradient)
                    ]
                )
            ]
            end = colorfinish[
                sum(
                    [
                        List[int(x)]
                        if index == 0
                        else List[int(x)]
                        * np.prod(
                            [sizeList[int(s)] for s in AlongColorGradient[0:index]]
                        )
                        for index, x in enumerate(AlongColorGradient)
                    ]
                )
            ]
        else:
            start = colorstart[List[AlongColorGradient]]
            end = colorfinish[List[AlongColorGradient]]
        colors = aux.linear_gradient(start, end, n=sizeList[AlongColor],)
    else:
        colors = color
    return colors[List[AlongColor]]


def checkDimensionList2D(List):
    try:
        np.shape(List[0][0])
        return True
    except:
        return False


def save_1PlotPlain(
    X,
    Y,
    Label=None,
    logX=False,
    logY=True,
    TickStyle="-",
    color=None,
    Ymin=None,
    Ymax=None,
    Xlabel=None,
    Ylabel=None,
    fontsize=8,
    Xmin=None,
    Xmax=None,
    alpha=1.0,
    name="Test.png",
):
    fig = plt.figure(figsize=np.array([10, 6]))
    ax = plt.axes([0.1, 0.1, 0.7, 0.85])
    Label = makeIterable(Label, len(X))
    for x, y, l in zip(X, Y, Label):
        plot_1PlotPlain(
            ax,
            x,
            y,
            l,
            logX=logX,
            logY=logY,
            TickStyle=TickStyle,
            color=color,
            Ymin=Ymin,
            Ymax=Ymax,
            Xlabel=Xlabel,
            Ylabel=Ylabel,
            fontsize=fontsize,
            Xmin=Xmin,
            Xmax=Xmax,
            alpha=alpha,
        )
    if logX:
        ax.set_xscale("log")
    if logY:
        ax.set_yscale("log")
    if Xlabel is not None:
        ax.set_xlabel(Xlabel, fontsize=fontsize)
        if intXLabel:
            from matplotlib.ticker import FormatStrFormatter

            ax.xaxis.set_major_formatter(FormatStrFormatter("%i"))
    if Ylabel is not None:
        ax.set_ylabel(Ylabel, fontsize=fontsize)
    ax.tick_params(labelsize=fontsize)
    # ax.set_xlim([np.min(Time),np.max(Time)])
    ax.set_ylim([Ymin, Ymax])
    ax.set_xlim([Xmin, Xmax])
    if any([x is not None for x in Label]):
        ax.legend()
    aux.create_dirs_for_file(name)
    aux.checkExistence_delete_file(name)
    print(".. saving %s" % name)

    fig.savefig(name, dpi=400, rasterized=True)
    plt.close(fig)


def plot_1PlotPlain(
    ax,
    X,
    Y,
    Label,
    logX=False,
    logY=True,
    TickStyle="-",
    color=None,
    Ymin=None,
    Ymax=None,
    Xlabel=None,
    Ylabel=None,
    fontsize=8,
    Xmin=None,
    Xmax=None,
    alpha=1.0,
    linewidth=None,
    intXLabel=False,
):
    plot = ax.plot(
        X, Y, TickStyle, label=Label, color=color, alpha=alpha, linewidth=linewidth
    )
    return plot


def makeIterable(object, Number, List=False, enforce_correct_number=False):
    import collections

    if object is None:
        if List:
            return [[None] for x in range(Number)]
        else:
            return [None for x in range(Number)]
    if List:
        if not isinstance(object[0], collections.Iterable):
            object = [object for x in range(Number)]
    else:
        if not (
            type(object) == np.ndarray or type(object) == list or type(object) == tuple
        ):  # isinstance(object, collections.Iterable):
            object = [object for x in range(Number)]
    if Number > 1 and enforce_correct_number:
        if not List:
            if all([x == object[0] for x in object]):
                print(f"object: {object}, desired length {Number}")
                if len(object) < Number:
                    object = [object[0] for x in range(Number)]
            # if len(object)<Number:
            #     raise Exception(f"wrong length {Number} object {object}")
    return object


def PhaseDiagram(
    snap,
    x_variable,
    y_variable,
    Weights,
    considering_h=False,
    Xmin=None,
    Xmax=None,
    Ymin=None,
    Ymax=None,
    Xbins=50,
    Ybins=50,
    logX=True,
    logY=True,
    select=None,
    normaliseWeights=True,
    colormap_log=False,
    colormap_vmin=None,
    colormap_vmax=None,
    name="../figures/Test.png",
    overflowBinY=False,
    overflowBinX=False,
    TimeColor="white",
    plotFit=False,
    method="sum",
    fitfunc=None,
    initialGuess=None,
    save=False,
    xlabel=None,
    ylabel=None,
    functions=None,
    colorbarlabel=None,
    cmap="cool",
    fontsize=12.9,
    dpi=440,
    return_fields="centers",
    time_format=None,
    length_major_ticks=5,
    length_minor_ticks=3,
    axes_text=None,
    axes_text_position="time",
):
    """

    Parameters
    ----------
    snap
    x_variable
    y_variable
    Weights
    mask
    considering_h
    Xmin
    Xmax
    Ymin
    Ymax
    Xbins
    Ybins
    log
    normaliseWeights
    colormap_log
    colormap_vmin
    colormap_vmax
    use_Jet_tracers
    Threshold_tracer
    InJetTracers
    name
    overflowBinY
    overflowBinX
    TimeColor
    plotFit
    fitfunc
    initialGuess
    save
    xlabel
    ylabel
    colorbarlabel
    cmap
    fontsize
    dpi

    Returns: x,y,H
    -------

    """
    import matplotlib.colors

    markersize = 4 * fontsize / 22.0
    linewidth = 0.6 * markersize
    font = {"family": "serif", "serif": ["Arial"], "size": fontsize}
    ticks_font = {"family": "sans-serif", "serif": ["Helvetica"]}  # ,
    mpl.rc("text", usetex=True)
    mpl.rcParams["text.latex.preamble"] = [
        r"\usepackage{amsmath,bm}\newcommand{\sbf}[1]{\textsf{\textbf{#1}}}"
    ]
    mpl.rc("font", **font)
    if functions is None:
        functions = []
    if plotFit and (fitfunc is None or initialGuess is None):
        exit("Error: Need to give function to be optimized and initial guess!")
    time = aux.get_Time(snap, considering_h=considering_h)
    print(type(time))
    if isIterable(time):
        warnings.warn("time iterable!")
        time = time[0]
    if time_format is None:
        time = "%.0f Myr" % (round(time))
    else:
        time = format_integer(time, time_format)
    if type(x_variable) == str:
        x_values, INFO = aux.get_value(
            x_variable, snap, considering_h=considering_h, returnINFO=True
        )
    else:
        x_values = x_variable
    if type(y_variable) == str:
        y_values, INFO = aux.get_value(
            y_variable, snap, considering_h=considering_h, returnINFO=True
        )
    else:
        y_values = y_variable
    if Weights is None:
        weights = np.ones_like(x_values)
    elif type(Weights) == str:
        weights, INFO = aux.get_value(
            Weights,
            snap,
            check_for_0=False,
            convert_units=True,
            considering_h=considering_h,
            returnINFO=True,
        )
    else:
        weights = Weights
    if type(select) == str:
        mask = aux.get_selection(select, snap)
    else:
        mask = select
    if mask is not None:
        debug.printDebug(mask, name="mask")
        debug.printDebug(x_values, name="x_values")
        x_values = x_values[mask]
        y_values = y_values[mask]
        weights = weights[mask]
    Xmin = aux.check_alternative(Xmin, aux.minValue(x_values, log=logX))
    Xmax = aux.check_alternative(Xmax, aux.maxValue(x_values, log=logX))
    Ymin = aux.check_alternative(Ymin, aux.minValue(y_values, log=logY))
    Ymax = aux.check_alternative(Ymax, aux.maxValue(y_values, log=logY))
    if not logX:
        xedges = np.linspace(Xmin, Xmax, Xbins + 1)
    else:
        xedges = 10 ** np.linspace(np.log10(Xmin), np.log10(Xmax), Xbins + 1)
    if not logY:
        yedges = np.linspace(Ymin, Ymax, Ybins + 1)
    else:
        yedges = 10 ** np.linspace(np.log10(Ymin), np.log10(Ymax), Ybins + 1)
    if overflowBinX:
        MIN = np.min(x_values)
        MAX = np.max(x_values)
        if MIN >= np.min(xedges):
            pass
        else:
            print("MIN")
            print("xedges[1:]")
            print(MIN)
            print(xedges[1:])
            xedges = np.hstack((MIN, xedges[1:]))
        if MAX <= np.max(xedges):
            pass
        else:
            xedges = np.hstack((xedges[:-1], MAX))
    if overflowBinY:
        MIN = np.min(y_values)
        MAX = np.max(y_values)
        if MIN >= np.min(yedges):
            pass
        else:
            yedges = np.hstack((MIN, yedges[1:]))
        if MAX <= np.max(yedges):
            pass
        else:
            yedges = np.hstack((yedges[:-1], MAX))
    if normaliseWeights and weights is not None:
        weights = weights / np.sum(weights)
    # Fit
    if plotFit:
        from scipy import optimize

        errfunc = lambda p, x, y: fitfunc(p, x) - y  # Distance to the target function
        FitParas, success = optimize.leastsq(
            errfunc, initialGuess[:], args=(x_values, y_values)
        )
    # if False:#method == "sum":
    #     H, xedges, yedges = np.histogram2d(
    #         x_values, y_values, bins=(xedges, yedges), weights=weights
    #     )
    # else:
    print(f"method: {method}")
    H, xedges, yedges, binnumber = scipy.stats.binned_statistic_2d(
        x_values, y_values, weights, statistic=method, bins=(xedges, yedges)
    )
    print(H)
    print("%s max: %g" % (y_variable, np.max(y_values)))
    debug.printDebug(H, name="H")
    H = H.T  # Let each row list bins with common y range.
    X, Y = np.meshgrid(xedges, yedges)
    print("total volume maybe covered %g" % np.sum(H))
    fig = plt.figure()
    ax = plt.axes([0.15, 0.10, 0.85, 0.85])

    if colormap_log:
        if colormap_vmax and colormap_vmin:
            color_mesh = ax.pcolor(
                X,
                Y,
                H,
                norm=matplotlib.colors.LogNorm(vmin=colormap_vmin, vmax=colormap_vmax),
                cmap=cmap,
                antialiased=False,
                linewidth=0.0,
                zorder=-1,
                alpha=1,
                rasterized=True,
            )  # cool
        else:
            color_mesh = ax.pcolor(
                X,
                Y,
                H,
                norm=matplotlib.colors.LogNorm(vmin=None, vmax=H[~np.isnan(H)].max()),
                cmap=cmap,
            )
        cb = plt.colorbar(
            color_mesh,
            ax=ax,
            orientation="vertical",
            # ticks=LogLocator(subs=list(range(10))),
        )

    else:
        color_mesh = ax.pcolormesh(
            X, Y, H, vmin=colormap_vmin, vmax=colormap_vmax, cmap=cmap
        )
        cb = plt.colorbar(color_mesh, ax=ax, orientation="vertical")
    if colorbarlabel is not None:
        pass
    else:
        if type(Weights) == str:
            colorbarlabel = "%s %s" % (method, Weights)
        else:
            colorbarlabel = "%s Counts" % method
    cb.ax.set_ylabel(colorbarlabel, rotation=270, labelpad=fontsize * 1.18)

    if plotFit:
        ax.plot(xedges, fitfunc(FitParas, xedges), "r-")
        print("fitted params are:")
        print(FitParas)
    for f in functions:
        alpha = 0.7
        print(f"working on function {f}")
        x_centers = xedges[:-1] + np.diff(xedges) / 2.0
        if logX:
            x_centers = 10 ** np.linspace(
                np.log10(min(xedges)), np.log10(max(xedges)), 100
            )
        if f == "eosfitTne":
            f = lambda a, b, c, d, e, x: a * (b + c * x ** d) ** e
            paras = [79.709, -64.096, 83.306, 0.063278, 2.3570]
            y = f(*paras, x_centers)  # 1.1 *
            ax.plot(x_centers, y, label=f"eEOS", alpha=alpha, color="#cc0000")
        elif f == "unity":
            y = x_centers
            ax.plot(x_centers, y, label=f"y=x")
        elif f == "pressure1M9":
            # assuming x=ElectronNumberDensity
            y = (
                param.ElectronProtonFraction
                * param.meanmolecularweight
                * 1e-9
                / x_centers
                / param.BOLTZMANN_cgs
            )
            ax.plot(
                x_centers,
                y,
                label=r"$P_\mathrm{th}=10^{-9}\,\mathrm{dyn}\,\mathrm{cm}^{-2}$",
                alpha=alpha,
                # color="#20124d"
                color="#674ea7",
            )
        elif f == "pressure1M10":
            # assuming x=ElectronNumberDensity
            y = (
                param.ElectronProtonFraction
                * param.meanmolecularweight
                * 1e-10
                / x_centers
                / param.BOLTZMANN_cgs
            )
            ax.plot(
                x_centers,
                y,
                label=r"$P_\mathrm{th}=10^{-10}\,\mathrm{dyn}\,\mathrm{cm}^{-2}$",
                alpha=alpha,
                # color="#783f04"
                color="#6fa8dc",
            )
        elif f == "pressure1M11":
            # assuming x=ElectronNumberDensity
            y = (
                param.ElectronProtonFraction
                * param.meanmolecularweight
                * 1e-11
                / x_centers
                / param.BOLTZMANN_cgs
            )
            ax.plot(
                x_centers,
                y,
                label=r"$P_\mathrm{th}=10^{-11}\,\mathrm{dyn}\,\mathrm{cm}^{-2}$",
            )
        elif f == "massresolution":
            target_mass = (
                snap.parameterfile["ReferenceGasPartMass"]
                / snap.hubbleparam
                * snap.UnitMass_in_g
                / param.SOLARMASS_in_g
            )
            drawCompleteVerticalLines(
                ax,
                [target_mass / 2.0, target_mass],
                color="black",
                alpha=[0.3, 0.6],
                labels=[r"resolution/2", r"resolution"],
            )
        elif f == "bhhsml":
            bh_hsml = snap.data["bhhs"] * snap.UnitLength_in_cm / param.KiloParsec_in_cm
            drawCompleteVerticalLines(
                ax, [bh_hsml], color="black", alpha=[0.6], labels=[r"$r_\mathrm{hsml}$"]
            )
        elif f == "thermalpressures":
            drawCompleteVerticalLines(
                ax,
                [1e-10, 1e-11],
                color="black",
                alpha=[0.3, 0.6],
                labels=[
                    r"$10^{-10}\,\mathrm{dyn}\,\mathrm{cm}^{-2}$",
                    r"$10^{-11}\,\mathrm{dyn}\,\mathrm{cm}^{-2}$",
                ],
            )
        else:
            raise Exception("function {f} not recognized!")
    if len(functions) > 0:
        ax.legend()
    # cb.ax.minorticks_on()
    # s = []
    # N = 5
    # if not  colormap_vmin is None and not  colormap_vmax is None:
    #  for i in np.arange(int(round(np.log10( colormap_vmin))),int(round(np.log10( colormap_vmax)))):
    #    s.append(np.linspace(10**i*10./N,10**(i+1),N))
    #  minorticks = [x for sub in s for x in sub]
    #  cb.ax.yaxis.set_ticks(minorticks, minor=True)

    #     ax.set_title('Phase Diagram')
    if xlabel is not None:
        pass
    else:
        if type(x_variable) != str:
            x_variable = "X"
        xlabel = "%s [cgs]" % x_variable
    if ylabel is not None:
        pass
    else:
        if type(y_variable) != str:
            y_variable = "X"
        ylabel = "%s [cgs]" % y_variable
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    #      ax.xaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    #      ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    ax.set_xlim(Xmin, Xmax)
    ax.set_ylim(Ymin, Ymax)
    # ax.tick_params(axis="both")  # , labelsize=14)
    if axes_text is not None:
        if axes_text_position == "time":
            time = axes_text + "\n" + "(" + time + ")"
        else:
            add_text_plot(
                axes_text, position=axes_text_position, axes=ax, color=TimeColor,
            )
    add_text_plot(
        time, position="lower left", axes=ax, color=TimeColor,
    )

    # ax.text(
    #     0.1,
    #     0.1,
    #     time,
    #     # fontsize=10,
    #     horizontalalignment="left",
    #     color=TimeColor,
    #     transform=ax.transAxes,
    # )
    if return_fields == "center":
        X = xedges[:-1] + np.diff(xedges) / 2.0
        Y = yedges[:-1] + np.diff(yedges) / 2.0
        X, Y = np.meshgrid(X, Y)
    if logX == "symlog":
        ax.set_xscale("symlog")
    elif logX:
        ax.set_xscale("log")
    if logY == "symlog":
        ax.set_yscale("symlog")
    elif logY:
        ax.set_yscale("log")

    if x_variable == "ElectronNumberDensity":
        getTwin(
            ax,
            axes="x",
            factor=1.90070808e-24,
            logY=logY,
            logX=logX,
            ylabel=r"$\rho\,[\mathrm{g}\,\mathrm{cm}^{-3}]$",
            ticks_position="in",
            length_major_ticks=length_major_ticks,
            length_minor_ticks=length_minor_ticks,
            pad=7,
        )
    set_tick_positions(
        ax,
        y_ticks_both=True,
        y_tick_labels_both=False,
        major_tick_length=length_major_ticks,
        minor_tick_length=length_minor_ticks,
        fontsize=fontsize,
        labelleft=True,
        labelbottom=True,
    )
    if save:
        aux.create_dirs_for_file(name)
        aux.checkExistence_delete_file(name)
        print("... saving %s" % name)

        fig.savefig(name, dpi=dpi, bbox_inches="tight")
        plt.close(fig)
    else:
        pass
        # plt.show()
    return X, Y, H


def fmt(x, pos):
    a, b = "{:.1e}".format(x).split("e")
    b = int(b)
    return r"${}\times10^{{{}}}$".format(a, b)


def drawCompleteVerticalLines(
    axes, positions, color=None, alpha=1, labels=None, linestyles="-", linewidth=1
):
    # position:x
    positions = makeIterable(positions, 1, List=False)
    color = makeIterable(color, np.shape(positions)[0], List=False)
    labels = makeIterable(labels, np.shape(positions)[0], List=False)
    alpha = makeIterable(alpha, np.shape(positions)[0], List=False)
    linestyles = makeIterable(linestyles, np.shape(positions)[0], List=False)
    linewidth = makeIterable(linewidth, np.shape(positions)[0], List=False)
    for pos, c, l, a, ls, lw in zip(
        positions, color, labels, alpha, linestyles, linewidth
    ):
        print(c)
        print("vertica")
        print(pos)
        axes.axvline(pos, color=c, alpha=a, label=l, linestyle=ls, linewidth=lw)


def drawCompleteHorizontalLines(axes, positions, color=None, alpha=1, labels=None):
    # position:x,y1,y2 drawn at position x from y1 to y2
    positions = makeIterable(positions, 1, List=False)
    color = makeIterable(color, np.shape(positions)[0], List=False)
    labels = makeIterable(labels, np.shape(positions)[0], List=False)
    alpha = makeIterable(alpha, np.shape(positions)[0], List=False)
    for pos, c, l, a in zip(positions, color, labels, alpha):
        print(c)
        print("vertica")
        print(pos)
        axes.axhline(pos, color=c, alpha=a, label=l)


def drawCompleteVerticalShading(axes, positions, color=None, alpha=1):
    positions = makeIterable(positions, 1, List=True)
    color = makeIterable(color, np.shape(positions)[0], List=False)
    for pos, c in zip(positions, color):
        axes.axvspan(pos[0], pos[1], alpha=alpha, color=c)


def plotText(
    axes, positions, text, fontsize=None, color="black", horizontalalignment="center"
):
    positions = makeIterable(positions, 1, List=True)
    text = makeIterable(text, 1, List=False)
    color = makeIterable(color, np.shape(positions)[0], List=False)
    if np.shape(positions)[0] != np.shape(text)[0]:
        exit("need same amount of positions and texts!")
    for te, pos, c in zip(text, positions, color):
        print(c)
        print("tex")
        print(pos)
        print(te)
        axes.text(
            pos[0],
            pos[1],
            te,
            fontsize=fontsize,
            horizontalalignment=horizontalalignment,
            color=c,
        )


def gaussian(x, amplitude, mean, variance):
    return amplitude * np.exp(-((x - mean) ** 2) / (2 * variance ** 2))


def save_histogram(
    data,
    weights=None,
    bins=20,
    filename="test",
    log=False,
    title=None,
    normed=False,
    plotScatter=False,
    plotFunction=gaussian,
    parameterNames=None,
    plotFunctionParameters=None,
    plotFunctionErrors=None,
    dpi=150,
    fontsize=12,
    logX=False,
    closeFig=True,
    saveNdata=True,
    saveHistArray="raw",
    feature=None,
    labels=None,
    resFit=None,
    onlySaveData=False,
    xlabel=None,
    ylabel=None,
    filenameEnd="",
    onlySaveNPZ=False,
):
    mpl.rcParams["text.usetex"] = False
    # make list of parameters iterable
    # if pass list of bins, make sure to pass bin edges not bin centers!
    if onlySaveNPZ:
        filename = "%s%s.npz" % (filename, filenameEnd)
        aux.checkExistence_delete_file(filename)
        aux.create_dirs_for_file(filename, tryexcept=True)
        np.savez(
            filename,
            data=data,
            weights=weights,
            bins=bins,
            plotFunctionParameters=plotFunctionParameters,
            plotFunctionErrors=plotFunctionErrors,
        )
        print(".... saved %s" % filename)
        return
    if not onlySaveData:
        fig = plt.figure()
        ax = plt.axes([0.1, 0.1, 0.84, 0.84])
        MIN = np.min(data)
        MAX = np.max(data)
        if resFit is not None:
            bins = np.arange(
                int(np.min(data) / resFit) * resFit,
                np.max(data) + resFit + resFit / 2.0,
                resFit,
            )
        elif type(bins) == int:
            if logX:
                bins = 10 ** np.linspace(
                    np.log10(0.99 * MIN), np.log10(1.01 * MAX), bins, endpoint=True
                )
                ax.set_xscale("log")
            else:
                bins = np.linspace((MIN), (MAX), bins, endpoint=True)
        n, bin_edges, patches = ax.hist(
            data, bins=bins, weights=weights, density=normed
        )
        if weights is not None:
            weightsNormalized = weights / np.max(weights) * np.max(n)
        else:
            weightsNormalized = np.ones(np.shape(data)) * np.max(n)
        if title is not None:
            ax.set_title(title)
        if plotScatter:
            ax.scatter(data, weightsNormalized, s=weightsNormalized, marker="o")
        note = []  # + "\n"
        if plotFunction is not None and plotFunctionParameters is not None:
            if plotFunctionErrors is None:
                raise Exception("please cite errors as well!")
            x = np.linspace(MIN, MAX, 100)
            if parameterNames is not None:
                if len(parameterNames) != len(plotFunctionParameters[0]):
                    raise Exception(
                        "number of labels for parameters doesnt match number of parameteres!"
                    )
            for index, (paras, errors) in enumerate(
                zip(plotFunctionParameters, plotFunctionErrors)
            ):
                for na, pa, err in zip(parameterNames, paras, errors):
                    pass
                    if pa < 1e-3:
                        note += [r"$%s=%e \pm %e$" % (na, pa, err)]  # + "\n"
                    else:
                        note += [r"$%s=%.2g \pm %.2g$" % (na, pa, err)]  # + "\n"
                ax.plot(x, plotFunction(x, *paras), label="peak %i" % index)
            note += [r"$N_\mathrm{data}=%i$" % np.size(data)]
            note += [r"$N_\mathrm{unique}=%i$" % np.size(np.unique(data))]
            note += [r"$\Delta v_\mathrm{bin}=%g$" % np.diff(bins)[0]]
            ax.legend(loc="upper right")
        note = "\n".join(map(str, note))
        ax.text(
            0.075,
            0.65,
            note,
            fontsize=fontsize,
            horizontalalignment="left",
            color="black",
            transform=ax.transAxes,
        )
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if ylabel is not None:
        ax.set_ylabel(ylabel)

    if log:
        ax.set_yscale("log")
    if saveNdata:
        filename += "_Nuq%i" % (np.size(np.unique(data)))
    if not onlySaveData:
        name = "%s%s.pdf" % (filename, filenameEnd)
        aux.checkExistence_delete_file(name)
        aux.create_dirs_for_file(name, tryexcept=True)
        # print(note)
        # try:
        aux.timer("saveindFig")
        fig.savefig(name, dpi=dpi, rasterized=True)
        aux.timer("saveindFig", end=1, remove=1)
        print("... saved %s" % name)
        # except:
        # print('couldn save %s, proabably due to thread locking' %name)
        if closeFig:
            plt.close(fig)

    if saveHistArray is not None:
        if saveHistArray == "used":
            if onlySaveData:
                raise Exception(
                    "ERROR: save_histogram: can only save used data when plotting!"
                )
            filenameArray = "%s.npy" % filename
            bins = bin_edges[:-1] + np.diff(bin_edges) / 2.0
            toSave = [n, bins]
        elif saveHistArray == "raw":
            filenameArray = "%sRAW.npy" % filename
            toSave = [data, weights]
            if feature is not None:
                toSave += [feature]
        else:
            raise Exception("save version of histogram not yet implemented!")
        # 		try:
        aux.checkExistence_delete_file(filenameArray)
        print(f"... saving {filenameArray}")
        np.save(filenameArray, toSave)


def getTheoryRadprof(variableTheo, x, error_if_not_found=True):
    folderVantygham = "DataExtraction/Vantyghem2014/"
    y, label = None, None
    if "ElectronNumberDensityProfile" == variableTheo:
        # data = np.loadtxt("Data/ICProfile/IC_profile_M15_R_rho_u_codeunits.txt")
        # radius, density, internalEnergy = data.T
        data = np.loadtxt("../turbulent_field/profileMS0735.txt")
        radius, density, internalEnergy = data
        ## get individual arrays
        radius = radius / param.HubbleParam
        density = (
            density
            * param.UnitMass_in_g
            / param.HubbleParam
            / param.UnitLength_in_cm
            / param.UnitLength_in_cm
            / param.UnitLength_in_cm
            * param.HubbleParam ** 3
        )
        y = density / param.PROTONMASS_cgs * param.ElectronProtonFraction
        x = radius * 1e3
        label = r"$n_\mathrm{e,M15}$"
        # label = "ElectronNumberDensityTheory"
    elif "MS0735fit" == variableTheo:
        y = aux.get_double_beta_profile(x, 0.05, 100, -4.9, 0.01, 400, -1.6)
        label = r"$n_\mathrm{e,fit}$"
    elif "TemperatureInKevVantyghemMS0735" == variableTheo:
        x, y = np.loadtxt(folderVantygham + "TemperatureDeprj.txt", delimiter=",").T
        label = r"$T_\mathrm{obs}$"
        index = np.argsort(x)
        x = x[index]
        y = y[index]
    elif "NumberDensityVantyghemMS0735" == variableTheo:
        x, y = np.loadtxt(folderVantygham + "DensityDeprj.txt", delimiter=",").T
        index = np.argsort(x)
        x = x[index]
        y = y[index]
        label = r"$n_\mathrm{e,obs}$"
    elif "ThermalPressureVantyghemMS0735" == variableTheo:
        x, y = np.loadtxt(folderVantygham + "PressureDeprj.txt", delimiter=",").T
        index = np.argsort(x)
        x = x[index]
        y = y[index]
        label = r"$P_\mathrm{th,obs}$"
    else:
        if error_if_not_found:
            raise Exception(f"variableTheo: {variableTheo} not found!")
    return x, y, label


@SuperClasses.checkLoadNoMemory(
    namesreturnvariables=["x", "y", "err", "label",],
    loadPlotData=1,
    savePlotDataFolder="../figures/Radprof/data_time_average/",
    savePlotDataNameInclude=[
        "variable",
        "weight",
        "method",
        "name_of_sim_save",
        "average_style",
        "average_style_error",
    ],
    Ignore=["fields_to_load", "name_of_sim", "name_of_sim_save", "differenceTime",],
    checkadditional=[],
    verbose=False,
)
def do_radprof_snap_time_average(
    variable=None,
    folder="./",
    numbers=None,
    weight=None,
    select=None,
    average_style="mean",
    average_style_error="percentiles",
    name_of_sim=None,
    name_of_sim_save=None,
    ics=False,
    info=None,
    fields_to_load=None,
    nbins=100,
    range=[None, None],
    same_number_points_bin=False,
    method="mean",
    postprocess=None,
    method_error="percentiles",
    error_bar_style="shaded",
    log=False,
    differenceTime=4,
    time_averaging_range=None,
    central_time=None,
):
    X = []
    Y = []
    if method_error is not None:
        ERRU = []
        ERRL = []
    else:
        ERRU = None
        ERRL = None
    LABEL = []
    if numbers is None and (
        time_averaging_range is not None and central_time is not None
    ):
        snap = quickImport(
            central_time,
            folder,
            FieldsToLoad=fields_to_load,
            useTime=True,
            differenceTime=differenceTime,
            IC=ics,
            INFO=info,
        )
        numbers = list(
            get_times_in_range(
                snap.folder,
                central_time - time_averaging_range / 2.0,
                central_time + time_averaging_range / 2.0,
            )
        )
        del snap
    elif numbers is None:
        raise Exception("numbers not found, need to define ")
    for number in numbers:
        x, y, err, label = do_radprof_snap(
            number=number,
            variable=variable,
            weight=weight,
            select=select,
            folder=folder,
            name_of_sim=name_of_sim,
            name_of_sim_save=name_of_sim_save,
            ics=ics,
            info=info,
            fields_to_load=fields_to_load,
            nbins=nbins,
            range=range,
            same_number_points_bin=same_number_points_bin,
            method=method,
            postprocess=postprocess,
            method_error=method_error,
            error_bar_style=error_bar_style,
            log=log,
            differenceTime=differenceTime,
        )
        X.append(x)
        Y.append(y)
        if method_error is not None:
            ERRU.append(err[0])
            ERRL.append(err[1])
        LABEL.append(label)
    Y = np.array(Y)
    if method_error is not None:
        ERRU = np.array(ERRU)
        ERRL = np.array(ERRL)
    err = np.zeros((2, len(y)))
    if "zerosmean" in average_style:
        y = np.nanmean(np.nan_to_num(Y, nan=0), axis=0)
    elif "mean" in average_style:
        y = np.nanmean(Y, axis=0)
    elif "zerosmedian" in average_style:
        y = np.median(np.nan_to_num(Y, nan=0), axis=0)
    elif "median" in average_style:
        y = np.nanmedian(Y, axis=0)
    else:
        raise Exception(f"average_style {average_style} not understood")
    v = get_sigma_value(average_style_error)
    print(f"average_style_error value: {v}")
    if "notnanpercentiles" in average_style_error:
        err[0] = np.percentile(Y, v, axis=0)
        err[1] = np.percentile(Y, 100 - v, axis=0)
    elif "zerospercentiles" in average_style_error:
        err[0] = np.percentile(np.nan_to_num(Y, nan=0), v, axis=0)
        err[1] = np.percentile(np.nan_to_num(Y, nan=0), 100 - v, axis=0)
    elif "percentiles" in average_style_error:
        err[0] = np.nanpercentile(Y, v, axis=0)
        err[1] = np.nanpercentile(Y, 100 - v, axis=0)
    else:
        raise Exception(f"average_style_error {average_style_error} not understood")
    return x, y, err, label


@SuperClasses.checkLoadNoMemory(
    namesreturnvariables=["x", "y", "err", "label",],
    loadPlotData=1,
    savePlotDataFolder="../figures/Radprof/data/",
    savePlotDataNameInclude=[
        "number",
        "variable",
        "weight",
        "method",
        "name_of_sim_save",
    ],
    Ignore=[
        "fields_to_load",
        "name_of_sim",
        "name_of_sim_save",
        "differenceTime",
        "snap",
    ],
    checkadditional=[],
    verbose=False,
)
def do_radprof_snap(
    number=None,
    variable=None,
    weight=None,
    select=None,
    mask=None,
    folder="./",
    name_of_sim=None,
    name_of_sim_save=None,
    ics=False,
    info=None,
    fields_to_load=None,
    nbins=100,
    range=None,
    same_number_points_bin=False,
    method="mean",
    postprocess=None,
    method_error="std",
    error_bar_style="shaded",
    log=False,
    differenceTime=4,
    snap=None,
):
    if snap is None:
        snap = quickImport(
            number,
            folder,
            FieldsToLoad=fields_to_load,
            useTime=True,
            differenceTime=differenceTime,
            IC=ics,
            INFO=info,
        )
    dist = aux.get_DistanceToCenterInKpc(snap, INFO=info, no_conversion=False)
    VARIABLE = aux.convert_to_list(variable)
    variable_number = len(VARIABLE)
    KEEP_LENGTH_METHODS = ["2corr"]
    print(f"variable_number: {variable_number}")
    MASK = makeIterable(mask, variable_number)
    WEIGHT = makeIterable(weight, variable_number)
    SELECT = makeIterable(select, variable_number)
    label = {}
    label["variable"] = ""
    label["weight"] = ""
    y_values = []
    x_values = []
    err_values = []
    for variable, weight, select, mask in zip(
        VARIABLE[:], WEIGHT[:], SELECT[:], MASK[:]
    ):
        err = None
        y = None
        split = method.split("_")
        method = split[0]
        method_dic = {}
        if len(split) > 1:
            for s in split[1:]:
                k, v = s.split(":")
                method_dic[k] = float(v)
        if method == "2corr":
            from Point2CorrFunction import (
                get_shift_indices,
                get_2pointcorrelation_function_two_variables,
                velocity_correlation,
                get_2pointcorrelation_function_two_variables_bootstrap,
            )

            positions = (
                snap.pos[snap.type == 0]
                * snap.UnitLength_in_cm
                / param.KiloParsec_in_cm
            )
            masks = [np.where(aux.get_selection(s, snap))[0] for s in select]
            variables = [
                aux.get_value(v, snap, considering_h=False, INFO=info) for v in variable
            ]
            weights = None
            if weight is not None:
                weights = [
                    aux.get_value(w, snap, considering_h=False, INFO=info)
                    for w in weight
                ]
            bin_edges, min_bin_edges, max_bin_edges = aux.getBinsMMs(
                Variable=positions / 2.0, Min=range[0], Max=range[1], N=nbins, log=log
            )
            x = bin_edges[0:-1] + np.diff(bin_edges) / 2.0
            sample_number = aux.checkIfInDictionary("samples", method_dic, 10000)
            shift_indices = get_shift_indices(masks, sample_number=int(sample_number))
            y = get_2pointcorrelation_function_two_variables(
                shift_indices,
                positions,
                variables,
                masks,
                bin_edges,
                weights=weights,
                function=velocity_correlation,
            )
            y = y[1:-1]
            if method_error is None:
                pass
            elif method_error == "bootstrap":
                N_bootstrap = int(aux.checkIfInDictionary("Nbootstrap", method_dic, 10))
                err = get_2pointcorrelation_function_two_variables_bootstrap(
                    shift_indices,
                    positions,
                    variables,
                    masks,
                    bin_edges,
                    weights=weights,
                    function=velocity_correlation,
                    N_bootstrap=N_bootstrap,
                )
                if error_bar_style == "shaded":
                    err = [y - err / 2, y + err / 2]
                elif error_bar_style == "errorbars":
                    err = [err / 2.0]
                else:
                    raise Exception(
                        f"error_bar_style: {error_bar_style} not understood"
                    )
                if err[0] is None:
                    raise Exception(f"err is None: {err}")
            else:
                raise Exception(f"method_error: {method_error} not understood!")
            variable = "-".join(variable)
            if weight is not None:
                weight = "-".join(weight)
        else:
            if select is not None and select != "None":
                selection = aux.get_selection(select, snap)
                if mask is not None:
                    mask = np.logical_and(selection)
                else:
                    mask = selection
            x, y, _ = getTheoryRadprof(
                variable,
                get_histogram_bins(
                    nbins,
                    same_number_points_bin,
                    dist,
                    log,
                    range,
                    convert_to_centers=True,
                )[2],
                error_if_not_found=False,
            )
            if y is None:
                values_variable = aux.get_value(
                    variable, snap, considering_h=False, INFO=info
                )
                values_weight = None
                if weight is not None:
                    values_weight = getWeights(
                        snap,
                        weight,
                        considering_h=False,
                        INFO=info,
                        useWeightsThresholds=False,
                    )
                x, y, err, bin_edges = do_Radprof(
                    dist,
                    values_variable,
                    weight=values_weight,
                    range=range,
                    log=log,
                    NBins=nbins,
                    typeCalc=method,
                    errbar=method_error,
                    errbarStyle=error_bar_style,
                    SameNumberPointsPerBin=same_number_points_bin,
                    return_binedges=True,
                    mask=mask,
                    info=info,
                )
        label["variable"] += variable
        label["weight"] += weight if weight is not None else "none"
        y_values.append(y)
        x_values.append(x)
        err_values.append(err)
    if postprocess is not None:
        postprocess = postprocess.split("_")
        for proc in postprocess:
            if proc == "SQRT":
                y_values = np.sqrt(y_values)
                err_values = [np.sqrt(x) if x is not None else None for x in err_values]
            elif proc == "ABS":
                y_values = np.abs(y_values)
                err_values = [np.abs(x) if x is not None else None for x in err_values]
            elif proc == "CUMSUM":
                y_values = [np.cumsum(x) for x in y_values]
                err_values = [np.abs(x) if x is not None else None for x in err_values]
            elif "NORMALIZEUPTOMAXRAD" in proc:
                variable_for_normalization = proc.split("-")[1]
                var = aux.get_value(variable_for_normalization, snap)
                if range[1] is None:
                    raise Exception(
                        f"need to know endpoint of NORMALIZEUPTOMAXRAD! but range={range}"
                    )
                norm = np.sum(var[dist < range[1]])
                y_values = [x / norm for x in y_values]
                err_values = [x / norm if x is not None else None for x in err_values]
            elif proc == "DIVIDEBY":
                if len(y_values) < 2:
                    raise Exception(
                        "not sufficient variables defined to divide by other variable"
                    )
                y_values[0] = y_values[0] / y_values[1]
                y_values = y_values[:1]
                err_values = [None]
            elif proc == "RELATIVEERROR":
                y_values[0] = 1 - y_values[0] / y_values[1]
                y_values = y_values[:1]
                err_values = [None]
            elif proc == "BINNORM":
                if log:
                    norm = np.diff(np.log10(bin_edges))
                else:
                    norm = np.diff(bin_edges)
                y_values = [x / norm for x in y_values]
                err_values = [x / norm if x is not None else None for x in err_values]
            elif "INTERPOLATE" in proc:
                x_theory = proc.split("-")[1]
                x, y, _ = getTheoryRadprof(
                    x_theory,
                    get_histogram_bins(
                        nbins,
                        same_number_points_bin,
                        dist,
                        log,
                        range,
                        convert_to_centers=True,
                    )[2],
                    error_if_not_found=False,
                )
                f = scipy.interpolate.interp1d(x_values[0], y_values[0])
                if err_values[0] is not None:
                    f_err_1 = scipy.interpolate.interp1d(x_values[0], err_values[0][0])
                    f_err_2 = scipy.interpolate.interp1d(x_values[0], err_values[0][1])
                    err_values[0][0] = f_err_1(x)
                    err_values[0][1] = f_err_2(x)
                y_values[0] = f(x)
                x_values[0] = x
            else:
                raise Exception(f"couldnt find postprocessing: {proc}")
    if len(y_values) == 1:
        y = y_values[0]
        x = x_values[0]
        err = err_values[0]
    else:
        raise Exception(
            f"should be single array (len(y_values)={len(y_values)}) now! y_values: {y_values}"
        )
    print(f"method_error: {method_error}")
    print(f"err_values: {err_values}")
    print(f"err: {err}")
    print(f"postprocess: {postprocess is None}")
    label["averaging_style"] = method
    label["postprocess"] = postprocess
    label["name_of_sim"] = name_of_sim
    label["time_in_myr"] = snap.time_in_myr
    return x, y, err, label


def do_Radprof(
    dist,
    variable,
    weight=None,
    range=[None, None],
    log=False,
    NBins=100,
    typeCalc="median",
    SameNumberPointsPerBin=False,
    errbar="std",
    errbarStyle="shaded",
    return_binedges=False,
    return_errbar=False,
    v=2,
    lower=0.1,
    upper=0.9,
    convertNanTypeCalcTo=0,
    mask=None,
    info=None,
):
    """
	dist: radial distance to center; can use snap instead of dist, to calc dist automatically
	typeCalc:		
			mean
			meanweighted
			medianweighted
	errbar:
			percentilesweighted: own errorbars set errbar='medianweighted' or 'percentilesweighted'
			percentiles
			std
			stdweighted
	errbarStyle:
			'shaded': return list with [lower values, upper values] to be used with plt.fill_between(x, 0, y1)
			'errorbars': return as (N,) or (2,N) as required by plt.errorbar(x, y, yerr=None, xerr=None) depending on presence of errorbar symmetry 
	"""
    if weight is not None:
        print(f"weight is not None but typeCalc={typeCalc}")
        if typeCalc == "median":
            typeCalc = "medianweighted"
        elif typeCalc == "mean":
            typeCalc = "meanweighted"
        import warnings

        warnings.warn(f"change typeCalc to: {typeCalc}")
    if errbar is not None:
        print(f"calculate errbors as {errbar}")

    known_errbarstyles = ["shaded", "errorbars"]
    if errbarStyle not in known_errbarstyles:
        raise Exception("errbarStyle %s not supported" % errbarStyle)

    if type(dist) != np.ndarray and type(dist) != list:
        try:
            dist = dist.data["DistInKpc"]
        except:
            print("trying to calc dist from dist for radprof")
            values = aux.get_DistanceToCenterInKpc(dist, no_conversion=False)
            dist.data["DistInKpc"] = values
            if v > 1:
                print("added DistInKpc to snap")
            dist = values
    if mask is not None:
        dist = dist[mask]
        variable = variable[mask]
        if weight is not None:
            print("before mask")
            debug.printDebug(weight, "weight")
            weight = weight[mask]
            debug.printDebug(weight, "weight after mask")
    MAX, MIN, bins = get_histogram_bins(NBins, SameNumberPointsPerBin, dist, log, range)
    debug.printDebug(bins, name="bins in do_Radprof")
    variable = np.array(variable, dtype=np.float64)
    dist = np.array(dist, dtype=np.float64)

    if weight is not None:
        if v > 0:
            debug.printDebug(weight, "weight")

    statistics = [typeCalc]
    if errbar is not None:
        statistics += [errbar]

    dic, bin_centers, bin_edges = aux.calcRadprofStatistics(
        dist,
        variable,
        bins=bins,
        range=[MIN, MAX],
        statistics=statistics,
        weights=weight,
        INFO=info,
        lower=lower,
        upper=upper,
        returnBinedges=True,
        v=v,
    )

    statistic = dic[typeCalc]
    # not sure if necessary: np.nan_to_num(dic[typeCalc], nan=convertNanTypeCalcTo)

    Errbar = None
    if errbar is not None:
        if errbar + "L" in dic.keys():
            # lower and upper values different
            errL = dic[errbar + "L"]
            errU = dic[errbar + "U"]
            if errbarStyle == "shaded":
                Errbar = [errL, errU]
            elif errbarStyle == "errorbars":
                Errbar = [statistic - errL, errU - statistic]
        else:
            err = dic[errbar]
            if errbarStyle == "shaded":
                Errbar = [statistic - err / 2, statistic + err / 2]
            elif errbarStyle == "errorbars":
                Errbar = [err / 2.0]

    if np.size(bin_centers) != np.size(statistic):
        exit("Should have same number of bin centres as of values in radprof!")

    if return_binedges:
        return bin_centers, statistic, Errbar, bin_edges
    elif return_errbar:
        return bin_centers, statistic, Errbar
    else:
        return bin_centers, statistic


def interpolate_nans(x, y):
    nans = np.isnan(y)
    y[nans] = np.interp(x[nans], x[~nans], y[~nans])
    return y


def get_histogram_bins(
    NBins, SameNumberPointsPerBin, dist, log, range, convert_to_centers=False
):
    if range[0] is None:
        MIN = np.min(dist)
    else:
        MIN = range[0]
    if log and MIN <= 0:
        minEarly = MIN
        MIN = np.min(dist[dist > 0])
        import warnings

        warnings.warn(
            "do_Radprof: setting MIN from %g to %g to use log" % (minEarly, MIN)
        )
    if range[1] is None:
        MAX = np.max(dist)
    else:
        MAX = range[1]
    if SameNumberPointsPerBin:
        bins = aux.histedges_equalN(dist, NBins, getAll=True)
    elif log:
        bins = 10 ** np.linspace(np.log10(MIN), np.log10(MAX), NBins)
    else:
        bins = np.linspace(MIN, MAX, NBins)
    if convert_to_centers:
        bins = bins[:-1] + np.diff(bins) / 2.0
    return MAX, MIN, bins


def get_sigma_value(average_style):
    sigma_match = re.search("(\d+)sigma", average_style)
    percent_match = re.search("(\d+)percent", average_style)
    if sigma_match and percent_match:
        raise Exception(
            f"average_style={average_style} cant match both sigma {sigma_match} and percentile {percent_match}!"
        )
    if sigma_match:
        s = int(sigma_match.group(1))
        if s == 1:
            v = 68.27
        elif s == 2:
            v = 95.45
        elif s == 3:
            v = 99.73
        else:
            raise Exception(
                f"sigma {sigma_match} doesnt fit 1,2,3! for {average_style}"
            )
    elif percent_match:
        v = float(percent_match.group(1))
    else:
        raise Exception(
            f"average_style: percentile values not defined for {average_style}"
        )
    return v


def get_CellsBetweenJets(
    snap,
    TracerThresh=1e-6,
    CumTracThresh=[0.46, 0.01, 0.01],
    AreaToCheck=[0.02, 0.02, 0.02],
    Rmax=0.01,
    returnBoxCenter=False,
):
    center = [float(snap.boxsize) / 2.0 for x in range(3)]
    print(center)
    print(snap.data["pos"][snap.data["type"] == 0])
    centerOff = aux.ScreenJetTracer(
        snap,
        bins=100,
        up=True,
        TracerThresh=TracerThresh,
        CumTracThresh=CumTracThresh,
        AreaToCheck=AreaToCheck,
        ensureOnRespectiveSide=True,
        center=center,
    )
    ZLengthUp = centerOff[0] - center[0]
    centerOff = aux.ScreenJetTracer(
        snap,
        bins=100,
        up=False,
        TracerThresh=TracerThresh,
        CumTracThresh=CumTracThresh,
        AreaToCheck=AreaToCheck,
        ensureOnRespectiveSide=True,
        center=center,
    )
    ZLengthDown = center[0] - centerOff[0]
    if centerOff < 0:
        print("CENTEROFF negative")
        exit()

    print("Zlength up %g, Zlength down %g" % (ZLengthUp, ZLengthDown))

    r, phi, z = aux.get_CylindricalCoordinates(
        snap, degrees=False, center=center, z=0, y=2, x=1
    )
    mask = np.logical_and.reduce((r < Rmax, (z) < ZLengthUp, (z) > -ZLengthDown))
    if returnBoxCenter:
        box = [ZLengthUp + ZLengthDown, Rmax * 2, Rmax * 2]
        center[0] += (ZLengthUp - ZLengthDown) / 2.0
        print("box, center from get_CellsBetweenJets!")
        print(box, center)
        return mask, box, center
    print(mask)
    print(snap.data["jetr"][mask])
    return mask


def checkNumberdims(array, dimensionToChek):
    try:
        dim = np.shape(array)[dimensionToChek]
    except:
        dim = 0
    return dim


def checkDimN(list, N):
    try:
        np.shape(list)[N - 1]
        return 1
    except:
        return 0


def plot_ProjectionContour(
    snap,
    deltaAngle=np.pi / 4.0,
    axis=[2, 0],
    figname="ProjectionContour.pdf",
    showTime=False,
    vmin=None,
    vmax=None,
    cbarLabel=r"$colorbar$",
    plotContour=True,
    mask=None,
    INFO=None,
    variable="Density",
    box=[0.1, 0.1],
    center=None,
    numthreads=8,
    noTicks=False,
    ProjBox=None,
    res=128,
    ContourWeights="JetMassFraction",
    howmanyX=3,
    howmanyY=5,
    ContourProjection=True,
    useContourWeights=False,
    slice=True,
    weights_variable="Volume",
    plotDim=0,
    log=True,
    colorsContour="k",
    MaskArray=True,
    initialTilt=False,
    deltaAngleInitial=3.0 * np.pi / 8.0,
    RotAxisInitial=[1, 0, 0],
):
    DATA = []
    EXTENT = []
    Nplots = int(np.pi / deltaAngle)
    CONTOUR = [[] for i in range(Nplots)]
    TIMES = []
    Angle = []
    res = aux.resolution_calculator_projections(res, ProjBox)
    if center is None:
        center = [float(snap.boxsize) / 2.0 for x in range(3)]
    VARIABLE = aux.get_value(
        variable, snap, considering_h=False, convert_units=True, INFO=INFO
    )
    maxDims = checkNumberdims(VARIABLE, 1)
    print(variable)
    # print VARIABLE
    if maxDims != 0:
        if maxDims < plotDim:
            exit("cant plot this dim")
        else:
            VARIABLE = VARIABLE[:, plotDim]
    # print VARIABLE
    # print np.min(VARIABLE)
    if mask is None:
        plotContour = False
    if plotContour:
        if not MaskArray:
            mask = [mask]
    if aux.checkExistenceKey(INFO, "OffsetRealJet", False):
        snap = do_AxisShift(snap, INFO, overallMask=snap.data["jetr"] > 1e-3)
    if initialTilt:
        snap = aux.RotatePositions(snap, center, deltaAngleInitial, RotAxisInitial)
    for index in range(Nplots):
        RotAxis = [1, 0, 0]
        rotAngle = deltaAngle
        if index == 0:
            rotAngle = 0
        snap = aux.RotatePositions(snap, center, rotAngle, RotAxis)
        print(np.shape(mask))
        if plotContour:
            for ma in mask:
                print("plotting contour")
                print(np.shape(ma))
                ContourData = np.zeros(np.shape(ma))
                ContourData[ma] = 2
                print("painted %i cells within mask" % np.size(ContourData[ma]))
                X, Y, Z, levels, colors, linewidths = aux.plot_contour(
                    None,
                    snap,
                    ContourData,
                    axis,
                    box,
                    center,
                    levels=None,
                    numthreads=numthreads,
                    colors=colorsContour,
                    projection=not ContourProjection,
                    projBox=ProjBox,
                    linewidths=1,
                    useweights=useContourWeights,
                    weights=ContourWeights,
                    considering_h=False,
                    res=res,
                    info=INFO,
                    Filter=None,
                    filternum=None,
                    ReturnValues=True,
                    alpha=None,
                    capAt=2,
                )
                CONTOUR[index].append([X, Y, Z])

        if slice:
            print("plotting slice")
            Data = do_Slice(snap, VARIABLE, res, box, center, numthreads, axis)
        else:
            print("plotting projection")
            if weights_variable is not None:
                weights = getWeights(
                    snap,
                    weights_variable,
                    considering_h=False,
                    INFO=INFO,
                    useWeightsThresholds=True,
                )
            Data = do_Projection(
                snap,
                VARIABLE,
                weights,
                res,
                ProjBox,
                center,
                numthreads,
                axis,
                volumeIntegral=False,
                inputGrids=False,
            )
        Extent = get_Extent(center, axis, box)
        print("extent")
        print(EXTENT)
        print(center)
        print(axis)
        print(box)
        # exit()
        time = aux.get_Time(snap, considering_h=False)
        time = "%.1f Myr" % (round(time, 1))
        DATA.append(Data)
        TIMES.append(time)
        EXTENT.append(Extent)
    if showTime:
        Time = TIMES
    else:
        Time = None
    if np.min(DATA) < 0:
        log = False
    if vmax is None:
        vmax = np.max(DATA)
    if vmin is None:
        vmin = np.min(DATA)
        if vmin == 0 and log:
            vmin = 10 ** (np.log10(vmax) - 4)
        elif np.log10(vmax) - np.log10(vmin) > 5:
            vmin = 10 ** (np.log10(vmax) - 4)
    compactSingleImshow(
        DATA,
        EXTENT,
        Time=Time,
        log=log,
        vmin=vmin,
        vmax=vmax,
        colormap="jet",
        figsizex=10,
        deltamx=0.0,
        deltamy=0.0,
        deltax=0.02,
        deltayUp=0.03,
        deltayDown=0.07,
        heightcolorbar=0.01,
        cbarLabel=cbarLabel,
        fontsize=12,
        howmanyX=howmanyX,
        howmanyY=howmanyY,
        figname=figname,
        plotContour=plotContour,
        ContourVariable=CONTOUR,
        levels=[1, 2, 3, 4, 5],
        axis=axis,
        box=box,
        center=center,
        noTicks=noTicks,
        note="%sMyr %s"
        % (time, aux.convert_params_to_string(center, string=False, Round=3)),
        showNote=False,
        noteColor="black",
        TicksRelativeToCenter=True,
        colors=colorsContour,
    )


def get_MostDistantJetCells(snap, TracerThresh=1e-3, up=True):
    mask = snap.data["jetr"] > TracerThresh
    if up:
        positions = snap.data["pos"][snap.data["type"] == 0][mask][
            snap.data["pos"][snap.data["type"] == 0][:, 0][mask]
            == np.max(snap.data["pos"][snap.data["type"] == 0][:, 0][mask])
        ]
    else:
        positions = snap.data["pos"][snap.data["type"] == 0][mask][
            snap.data["pos"][snap.data["type"] == 0][:, 0][mask]
            == np.min(snap.data["pos"][snap.data["type"] == 0][:, 0][mask])
        ]
    if np.shape(positions)[0] != 1:
        print(positions)
        print(np.size(snap.data["jetr"][mask]))
        print(
            np.max(
                snap.data["pos"][snap.data["type"] == 0][:, 0][snap.Jet_Tracer > mask]
            )
        )
        exit("not one position of max distance cells in jet lobe")
    positions = [float(x) for x in positions[0]]
    return positions


def ArrAy(thing):
    if type(thing) == list:
        thing = np.array(thing)
    return thing


def alignWithBubble(snap, center=None, BH_pos=None):
    if center is None:
        center = [float(snap.boxsize) / 2.0 for x in range(3)]
    if BH_pos is None:
        BH_pos = [float(snap.boxsize) / 2.0 for x in range(3)]
    tip_position = get_MostDistantJetCells(snap, TracerThresh=1e-3, up=True)
    diffVect = ArrAy(tip_position) - ArrAy(BH_pos)
    RotCenter = ArrAy(center) + 1.0 / 2.0 * diffVect
    RotAxis = aux.normalVectorPlane(diffVect, np.array([1, 0, 0]))
    theta = aux.AngleBetweenVectors(np.array([1, 0, 0]), diffVect)
    aux.RotatePositions(snap, RotCenter, theta, RotAxis)
    return snap


def reduceArray(array, reductionType, axis):
    if "average" == reductionType:
        result = np.average(array, axis=axis)
    elif "median" == reductionType:
        result = np.median(array, axis=axis)
    elif "averageroot" == reductionType:
        result = np.average(array, axis=axis)
        result = np.sqrt(result)
    elif "medianroot" == reductionType:
        result = np.median(array, axis=axis)
        result = np.sqrt(result)
    else:
        exit("ReduceGrids: reduction type not available!!")
    return result


def ReduceGrids(
    GObjects, MinNcells, reductionType="average", includeTopPart=True, showAll=False
):
    NGobjects = np.shape(GObjects)[0]
    dims = GObjects[0].dim
    for i in range(NGobjects):
        GObjects[i].setIncludeTopPart(includeTopPart)
        GObjects[i].LimitLength(MinNcells)
        GObjects[i].reduceTo1D()
    Stacked = [[] for i in range(dims)]
    for j in range(dims):
        for i in range(NGobjects):
            if np.size(Stacked[j]) > 0:
                Stacked[j] = np.hstack((Stacked[j], GObjects[i].grids[j]))
            else:
                Stacked[j] = GObjects[i].grids[j]
    NAll = dims
    if showAll:
        NAll = dims + dims * NGobjects
    Result = [[] for i in range(NAll)]
    for i in range(dims):
        Result[i] = reduceArray(Stacked[i], reductionType, 1)
        print("shape result")
        print(i)
        print(np.shape(Result[i]))
        if showAll:
            for j in range(NGobjects):
                print("showAll adding radprofgs")
                print(i, j)
                print(np.shape(GObjects[j].grids[i]))
                Result[dims + i + j] = reduceArray(
                    GObjects[j].grids[i], reductionType, 1
                )
    return Result


def get_Bins(
    Data=None,
    Range=None,
    Nbins=100,
    log=False,
    min=None,
    max=None,
    returnCenters=False,
    minNonZero=False,
    sameNumberPointsPerBin=False,
    symmetric_zero=False,
    prevent_same_bins=False,
):
    if min is not None and max is not None:
        Range = [min, max]
    if Range is not None:
        min = Range[0]
        max = Range[1]
    if min is None:
        min = np.min(Data)
        if log and minNonZero:
            if np.count_nonzero(Data) == 0:
                min = 2
                max = 3
            else:
                min = np.min(np.array(Data)[np.array(Data) > 0])
    if max is None:
        max = np.max(Data)
    if symmetric_zero:
        max = np.max([abs(min), abs(max)])
        min = -max
    if sameNumberPointsPerBin:
        bins = aux.histedges_equalN(Data, Nbins, getAll=True)
    elif log:
        bins = 10 ** np.linspace(np.log10(min), np.log10(max), Nbins)
    else:
        bins = np.linspace(min, max, Nbins)
    if prevent_same_bins and np.max(bins) == np.min(bins):
        bins = np.linspace(min - 1, max, Nbins)
    if np.max(bins) == np.min(bins):
        raise Exception(f"bins all same! {bins}")
    if returnCenters:
        bin_centers = bins[1:] - np.diff(bins) / 2.0
        return bins, bin_centers
    else:
        return bins


def SmoothFunction(
    x,
    y,
    Nsmooth=100,
    log=False,
    Range=None,
    Data=None,
    Min=None,
    Max=None,
    limits=None,
    order=4,
):
    # aux.printDebug(y, "y1")
    # y = np.polyfit(x, y, 1)
    # y = np.poly1d(y)(x)
    # bspl = splrep(x, y, s=3)
    # y = splev(x, bspl)
    # aux.printDebug(x, "x")
    # aux.printDebug(y, "y2")
    # return x, y
    # exit("das")
    from scipy.signal import savgol_filter

    i = 0
    for v in y:
        if v == 0:
            i += 1
        else:
            break
    y[i:] = savgol_filter(y[i:], 11, 1)
    return x, y
    bins, bin_centers = get_Bins(
        Range=Range,
        Data=Data,
        Nbins=Nsmooth,
        log=log,
        min=Min,
        max=Max,
        returnCenters=True,
    )
    yNew = scipy.interpolate.splrep(x, y, k=order)  # ,order=3)
    xNew = bin_centers
    yNew = scipy.interpolate.splev(xNew, yNew, der=0)
    # exit()
    if limits is not None:
        xNew = np.array(xNew)
        yNew = np.array(yNew)
        x = np.array(x)
        y = np.array(y)
        if np.size(limits) != 2:
            exit(
                "Error: Need upper and lower limit for smooth function, if not needed state None!"
            )
        if limits[0] is not None:
            xNewOld = np.copy(xNew)
            xNew = np.concatenate((x[x <= limits[0]], xNew[xNew > limits[0]]))
            yNew = np.concatenate((y[x <= limits[0]], yNew[xNewOld > limits[0]]))
        if limits[1] is not None:
            xNewOld = np.copy(xNew)
            xNew = np.concatenate((xNew[xNew < limits[1]], x[x >= limits[1]]))
            yNew = np.concatenate((yNew[xNewOld < limits[1]], y[x >= limits[1]]))
    return xNew, yNew


def getCalcRequiredDistanceBH(distanceMaxAngle, snapshotMaxAngle, angleMax, angleTild):
    distance = distanceMaxAngle * np.cos(angleMax)
    if angleTild != 0.0:
        distance = distance / np.cos(angleTild)
    return distance


def findNearestInd(array, value):
    # find index of value in array closest to value, if some similar takes first one it finds from beginning
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


def get_NumberWRTAngle(INFO, up=True):
    # only looking at jet so far
    maxAngle = aux.checkExistenceKey(INFO, "MaxAngleRotatedLOS", None)
    snapshotMaxAngle = aux.checkExistenceKey(INFO, "MaxAngleRotatedSnapshot", None)
    folderFileDistances = aux.checkExistenceKey(
        INFO, "DistanceLowerPartJetFolder", None
    )
    angleRotated = aux.checkExistenceKey(INFO, "rotateAlongLineOfSightAngle", 0.0)
    if maxAngle is None or snapshotMaxAngle is None or folderFileDistances is None:
        print(maxAngle, snapshotMaxAngle, folderFileDistances)
        exit(
            "need to Specify max angle, snapshot in myr of this jet and file for distance of BH-lower part Jet!"
        )
    dataDistance = np.loadtxt(folderFileDistances, delimiter=",").T
    index = findNearestInd(dataDistance[0], snapshotMaxAngle)
    distanceNecessary = getCalcRequiredDistanceBH(
        dataDistance[2 - int(up)][index], snapshotMaxAngle, maxAngle, angleRotated
    )
    if distanceNecessary < 0:
        print(distanceNecessary)
        print(angleRotated)
        exit(
            "distance negative! sure that this rotations makes sense? maybe larger theta > pi/2?"
        )
    index = findNearestInd(dataDistance[2 - int(up)], distanceNecessary)
    print(
        "needed distance BH JET: %g, found %g"
        % (distanceNecessary, dataDistance[2 - int(up)][index])
    )
    print(
        "MaxAngle: %g, Snapshot: %.2g, Angle rotated: %g, final snapshot %.2g"
        % (maxAngle, snapshotMaxAngle, angleRotated, dataDistance[0][index])
    )
    return dataDistance[0][index]


def modifyPlot(
    ax,
    xlog=False,
    ylog=False,
    xlabel="",
    ylabel="",
    Xrange=[None, None],
    Yrange=[None, None],
):
    if xlog:
        ax.set_xscale("log")
    if ylog:
        ax.set_yscale("log")
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xlim(*Xrange)
    ax.set_ylim(*Yrange)
    return ax


def makesureLimitsShowPlot(ax, x, y, xlog=False, ylog=False, Xrange=None, Yrange=None):
    Xrange = np.copy(Xrange)
    Yrange = np.copy(Yrange)
    if None in Xrange:
        x = x[~np.isnan(x)]
        if len(x) > 0:
            xlimCur = ax.get_xbound()
            if not xlog:
                xlims = [np.min(x), np.max(x)]
            else:
                xlims = [np.min(x[x > 0]), np.max(x[x > 0])]
            xnewLims = [min((xlimCur[0], xlims[0])), max((xlimCur[1], xlims[1]))]
            for i, val in enumerate(Xrange):
                if val is None:
                    Xrange[i] = xnewLims[i]
    if None in Yrange:
        if len(y) > 0 and np.max(y) != np.min(y):
            print(y[y > 0])
            y = y[~np.isnan(y)]
            ylimCur = ax.get_ylim()
            if not ylog:
                ylims = [np.min(y), np.max(y)]
            else:
                ylims = [np.min(y[y > 0]), np.max(y[y > 0])]
                ylimCur = [
                    ylimCur[0] if ylimCur[0] > 0 else np.inf,
                    ylimCur[1] if ylimCur[1] > 0 else -np.inf,
                ]
            ynewLims = [min((ylimCur[0], ylims[0])), max((ylimCur[1], ylims[1]))]
            for i, val in enumerate(Yrange):
                if val is None:
                    Yrange[i] = ynewLims[i]
    return Xrange, Yrange


def savePlot(fig, filename, dpi=400, bbox_inches="tight"):
    aux.create_dirs_for_file(filename)
    aux.checkExistence_delete_file(filename)
    print(".. saving %s" % filename)
    fig.savefig(filename, dpi=dpi, bbox_inches=bbox_inches)
    return True


def plotErrorbar(
    ax, err, x, y=None, alphaError=0.3, errbarStyle="shaded", color="black", **kwargs
):
    if errbarStyle == "shaded":
        plot = ax.fill_between(
            x, err[0], err[1], alpha=alphaError, color=color, **kwargs
        )
    elif errbarStyle == "errorbars":
        plot = ax.errorbar(x, y, yerr=err, color=color, **kwargs)
    else:
        raise Exception("errbarStyle %s, not implemented!" % errbarStyle)
    return plot


def plot_radprof(
    x,
    y,
    err=None,
    ax=None,
    save_plot=False,
    filename="test.pdf",
    fig=None,
    vlines=None,
    hlines=None,
    **kwargs,
):
    if fig is None:
        fig = plt.figure()
    if ax is None:
        ax = plt.axes()
    if err is not None:
        if len(np.shape(err)) == 2:
            plot = ax.fill_between(x, err[0], err[1], alpha=0.6, **kwargs)
            plot = ax.plot(x, y, **kwargs)
        elif len(np.shape(err)) == 1 and len(err) == len(y):
            plot = ax.errorbar(x, y, yerr=err, **kwargs)
        else:
            raise Exception(
                f"len(np.shape(err)): {len(np.shape(err))} cant plot err {err} with y {y}"
            )
    else:
        plot = ax.plot(x, y, **kwargs)
    if vlines is not None:
        for v in vlines:
            ax.axvline(x=v)
    if hlines is not None:
        for h in hlines:
            ax.axhline(y=h)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.legend()
    if save_plot:
        aux.create_dirs_for_file(filename)
        aux.checkExistence_delete_file(filename)
        fig.savefig(filename)
        print(f"... saved {filename}")
        plt.close(fig)
    return fig


def getFigure(
    legendExternal=False,
    quadraticFigure=False,
    nolegendExtAxSize=[0.12, 0.12, 0.87, 0.87],
    legendExtAxSize=[0.12, 0.12, 0.55, 0.86],
    secondXaxis=False,
    figsize=None,
):
    if figsize is None:
        if quadraticFigure:
            figsize = np.array([7, 7])
        elif secondXaxis:
            figsize = np.array([9, 7])
        else:
            figsize = np.array([10, 7])
    fig = plt.figure(figsize=figsize)
    if not legendExternal:
        axesSize = nolegendExtAxSize
    else:
        axesSize = legendExtAxSize
    print("axesSize", axesSize)
    if secondXaxis:
        axesSize[3] = 0.76
    ax = plt.axes(axesSize)
    return fig, ax


def getPlotIndex(
    useTimeIndexAsColor,
    useFolderIndexAsColor,
    useTimeIndexAsTickStyle,
    useFolderIndexAsTickStyle,
    useTimeIndexAsAlpha,
    useFolderIndexAsAlpha,
    indexTime,
    indexFolder,
    indexRest,
    verbose=1,
):
    if useTimeIndexAsColor:
        colorIndex = indexTime
        colorIndexText = "indexTime"
    elif useFolderIndexAsColor:
        colorIndex = indexFolder
        colorIndexText = "indexFolder"
    else:
        colorIndex = indexRest
        colorIndexText = "indexRest"
    if useTimeIndexAsTickStyle:
        tickIndex = indexTime
        tickIndexText = "indexTime"
    elif useFolderIndexAsTickStyle:
        tickIndex = indexFolder
        tickIndexText = "indexFolder"
    else:
        tickIndex = indexRest
        tickIndexText = "indexRest"
    if useTimeIndexAsAlpha:
        alphaIndex = indexTime
        alphaIndexText = "indexTime"
    elif useFolderIndexAsAlpha:
        alphaIndex = indexFolder
        alphaIndexText = "indexFolder"
    else:
        alphaIndex = indexRest
        alphaIndexText = "indexRest"
    if verbose > 0:
        print(
            "getPlotIndex: colorIndex: %s, tickIndex: %s, alphaIndex: %s"
            % (colorIndexText, tickIndexText, alphaIndexText)
        )
    return colorIndex, tickIndex, alphaIndex


def plotGrid(
    grid,
    log=False,
    axis=None,
    vmin=None,
    vmax=None,
    labelsizeTicks=16,
    fontsize=20,
    prefixFile="",
    addFile="",
    colormap="jet",
    pixelToPlot="half",
    ProjBox=None,
    center=None,
    saveFold="../figures/Misc/",
):
    grid = np.array(grid)
    ProjBox = validateArray(ProjBox)
    if center is None:
        center = np.array([x for x in snap.center])
    dim = len(np.shape(grid))
    if dim not in [2, 3]:
        raise Exception("can only plot 2D or 3D fields, not %iD!" % dim)
    if dim == 3:
        if axis is None:
            axis = [2, 0]
            axisPerp = 3 - np.sum(axis)
        if pixelToPlot == "half":
            pixelToPlot = int(np.shape(grid)[axisPerp] / 2)
        maskGrid = [slice(None), slice(None), slice(None)]
        maskGrid[axisPerp] = pixelToPlot
        grid = grid[tuple(maskGrid)]
        if axis[0] < axis[1]:  # grid[0] is plotted along y axis
            grid = grid.T
    resolution = np.shape(grid)
    dxl = 0.04
    dxr = 0.14
    dc = 0.02
    deltac = 0.05
    deltax = 1 - dxl - dxr - dc - deltac
    dy = dxl
    deltay = deltax * resolution[0] / resolution[1]
    figX = 7
    figY = figX * (2 * dy + deltay)  # /(2*dx+dc+deltay*)
    fig, ax = getFigure(
        figsize=[figX, figY],
        nolegendExtAxSize=[dxl, dy, deltax, 1 - 2 * dy * figX / figY],
    )
    axCo = plt.axes([dxl + deltax + dc, dy, deltac, 1 - 2 * dy * figX / figY])
    ext = None
    if axis is not None:
        ext = getExtension(ProjBox, center, axis)
    vmin, vmax = getVminVmax(
        vmin,
        vmax,
        grid,
        ChooseVmax=False,
        ChooseVmin=False,
        time=None,
        modelVmax=False,
        modelVmin=False,
        variable=None,
    )
    if log:
        import matplotlib.colors

        img = ax.imshow(
            grid,
            cmap=colormap,
            origin="lower",
            interpolation="none",
            aspect="auto",
            extent=ext,
            norm=matplotlib.colors.LogNorm(vmin=vmin, vmax=vmax),
        )
    else:
        img = ax.imshow(
            grid,
            cmap=colormap,
            origin="lower",
            interpolation="none",
            aspect="auto",
            extent=ext,
            vmin=vmin,
            vmax=vmax,
        )
    ax.axes.tick_params(
        axis="y",
        colors="black",
        direction="in",
        length=0,
        labelleft=True,
        labelright=True,
        pad=-2.5 * labelsizeTicks,
        labelsize=labelsizeTicks,
    )
    ax.axes.tick_params(
        axis="x",
        colors="black",
        direction="in",
        length=0,
        labelbottom=True,
        labeltop=True,
        pad=-1.5 * labelsizeTicks,
        labelsize=labelsizeTicks,
    )
    ax = modifyPlot(ax, xlog=False, ylog=False, xlabel="", ylabel="")
    cbar = plt.colorbar(
        img, cax=axCo, orientation="vertical"
    )  # , ticks = ticks)#,format='%.2g')
    cbar.ax.tick_params(labelsize=fontsize)
    cbar.ax.yaxis.get_offset_text().set(size=fontsize)
    filename = saveFold + "%sFieldProj%sRes%s%s.pdf" % (
        prefixFile,
        aux.convert_params_to_string(ProjBox),
        aux.convert_params_to_string(resolution),
        addFile,
    )
    savePlot(fig, filename, dpi=400)


def lineColormap(xs, ys, c, ax=None, **kwargs):
    from matplotlib.collections import LineCollection

    # taken from function multiline:
    # https://stackoverflow.com/questions/38208700/matplotlib-plot-lines-with-colors-through-colormap
    """Plot lines with different colorings

	Parameters
	----------
	xs : iterable container of x coordinates
	ys : iterable container of y coordinates
	c : iterable container of numbers mapped to colormap
	ax (optional): Axes to plot on.
	kwargs (optional): passed to LineCollection
		!!!!	e.g. cmap='bwr'		!!!!

	Notes:
		len(xs) == len(ys) == len(c) is the number of line segments
		len(xs[i]) == len(ys[i]) is the number of points for each line (indexed by i)

	Returns
	-------
	lc : LineCollection instance.
	"""

    # find axes
    ax = plt.gca() if ax is None else ax

    # create LineCollection
    segments = [np.column_stack([x, y]) for x, y in zip(xs, ys)]
    lc = LineCollection(segments, **kwargs)

    # set coloring of line segments
    #    Note: I get an error if I pass c as a list here... not sure why.
    lc.set_array(np.asarray(c))

    # add lines to axes and rescale
    #    Note: adding a collection doesn't autoscalee xlim/ylim
    ax.add_collection(lc)
    ax.autoscale()
    return lc


def plotCumSumAlongAxis(
    grid,
    ProjBox=1,
    axis=[2, 0],
    plotN=20,
    xlabel=r"$r\ [\mathrm{Mpc}]$",
    ylabel=r"$\sum_\mathrm{cum}\mathrm{RM}\ [\mathrm{rad}\ \mathrm{m}^{-2}]$",
    saveFold="../figures/Misc/",
    prefixFile="",
    addFile="",
    color=None,
    alpha=0.6,
    linewidth=2,
    linestyle="-",
    xlog=False,
    ylog=False,
    coloring=True,
    cmap="Spectral",
    **kwargs,
):
    if not coloring:
        cmap = None
    # calculates cum sum along perp axis 3-np.sum(axis)
    axisPerp = 3 - np.sum(axis)
    # grid = np.array(grid)
    ProjBox = validateArray(ProjBox)
    shapeGrid = np.shape(grid)
    if len(shapeGrid) != 3:
        raise Exception("need 3D grids!")
    Nsqrt = int(np.sqrt(plotN))
    radius = np.linspace(
        -ProjBox[axisPerp] / 2.0, ProjBox[axisPerp] / 2.0, shapeGrid[axisPerp]
    )
    fig, ax = getFigure(
        legendExternal=False, nolegendExtAxSize=[0.12, 0.10, 0.87, 0.80]
    )
    spacing = np.array([x / y for x, y in zip(ProjBox, shapeGrid)])
    ys = []
    c = []
    Nplots = 0
    for i in range(
        0, int(shapeGrid[axis[0]]), int(np.max([int(shapeGrid[axis[0]] / Nsqrt), 1]))
    ):
        for j in range(
            0, shapeGrid[axis[1]], np.max([int(shapeGrid[axis[1]] / Nsqrt), 1])
        ):
            maskGrid = [slice(None), slice(None), slice(None)]
            maskGrid[axis[0]] = i
            maskGrid[axis[1]] = j
            cur = grid[tuple(maskGrid)]
            cumsum = np.cumsum(cur)
            # Label = r'$%i,%i, max: %1.2g$' %(i,j,np.max(cumsum))
            if ylog:
                cumsum = np.abs(cumsum)
            ys.append(cumsum)
            dist = np.sqrt(
                ((i - shapeGrid[axis[0]] / 2) * spacing[axis[0]]) ** 2
                + ((j - shapeGrid[axis[1]] / 2) * spacing[axis[0]]) ** 2
            )
            c.append(dist)
            Nplots += 1
    xs = [radius for k in range(Nplots)]
    lc = lineColormap(
        xs,
        ys,
        c,
        ax=ax,
        alpha=alpha,
        linewidth=linewidth,
        linestyle=linestyle,
        cmap=cmap,
        **kwargs,
    )
    axcb = fig.colorbar(lc)
    axcb.set_label(r"$r_\mathrm{center}$")
    # plot = ax.plot(radius, cumsum, TickStyle, label=Label, color=color, alpha=alpha, linewidth=linewidth)
    ax.ticklabel_format(axis="both", style="sci", scilimits=(-2, 2))
    if plotN < 30:
        ax.legend(framealpha=0.5, fancybox=True)
    modifyPlot(ax, xlog=xlog, ylog=ylog, xlabel=xlabel, ylabel=ylabel)
    filename = saveFold + "%sRadAlgGrid%sRes%s%s.pdf" % (
        prefixFile,
        aux.convert_params_to_string(ProjBox),
        aux.convert_params_to_string(shapeGrid),
        addFile,
    )
    savePlot(fig, filename, dpi=400)


def getTwinY(ax, factor=1e3, ylog=False, ylabel=r"", invertValues=False):
    if ax is None:
        ax = plt.gca()
    ax2 = ax.twinx()
    numberMaxLogTicks = numberMaxLinTicks = len(ax.get_yticks()) * 1.2  # * 2
    Ymin, Ymax = ax.get_ylim()
    NewYmin = Ymin * factor if not invertValues else 1.0 / Ymax * factor
    NewYmax = Ymax * factor if not invertValues else 1.0 / Ymin * factor
    if ylog:
        majorTicks, minorTicks = setLogTicks(
            NewYmin, NewYmax, numberMax=numberMaxLogTicks, minormajorOverlap=False
        )
        ticks = majorTicks[:]
        ax2.set_yscale("log")
    else:
        ticks = setLinTicksAlt(NewYmin, NewYmax, numberMax=numberMaxLinTicks)

    def tickConvert(X):
        return X / factor if not invertValues else 1 / (X / factor)

    minor = False
    # minorTicks = None
    if ylog:
        if minorTicks is not None and len(minorTicks) < numberMaxLogTicks:
            showMinorLabels = True
        elif len(majorTicks) <= 2:
            print("reducing minor ticks")
            showMinorLabels = True
            print(f"minor ticks: {minorTicks}")
            reducedMinorNumber = minorTicks[:]
            # index = 2
            # while len(reducedMinorNumber) > numberMaxLogTicks:
            base_first_value = 10 ** int(np.log10(minorTicks[0]))
            start = 0
            if minorTicks[0] % (2 * base_first_value):
                start = 1
            print(
                f"minorTicks[0]: {minorTicks[0]}, 2*base_first_value: {2*base_first_value}, minorTicks[0] % 2*base_first_value {minorTicks[0] % 2*base_first_value}"
            )
            reducedMinorNumber = minorTicks[start::2]
            # index += 1
            # print(f'in while index: {index}')
            print(f"reducedMinorNumber: {reducedMinorNumber}")
            minorTicks = reducedMinorNumber[:]
            print(f"reducedMinorNumber: {reducedMinorNumber}")
        else:
            showMinorLabels = False
        minor = True
        ticks = majorTicks
    print(
        f"twinY ticks: {ticks}, minorticks {minorTicks}, number of ticks possible: {numberMaxLogTicks}"
    )
    print(f"minor: {minor}")
    ax2.set_ylim(ax.get_ylim())
    if minor:
        ax2.set_yticks(tickConvert(minorTicks), minor=True)
        if showMinorLabels:
            ax2.set_yticklabels(["%g" % x for x in minorTicks], minor=True)
        else:
            ax2.set_yticklabels(["" for x in minorTicks], minor=True)
    ax2.set_yticks(tickConvert(ticks), minor=False)
    ax2.set_yticklabels(["%g" % x for x in ticks], minor=False)
    ax2.set_ylabel(ylabel)
    return ax2


def getTwin(
    ax,
    axes="x",
    factor=1e3,
    logY=False,
    ylabel=r"",
    invertValues=False,
    logX=False,
    ticks_position="out",
    length_minor_ticks=3,
    length_major_ticks=5,
    pad=None,
):
    minorTicks = None
    if ax is None:
        ax = plt.gca()
    if axes == "x":
        ax2 = ax.twiny()
    else:
        ax2 = ax.twinx()
    if axes == "x":
        numberMaxLogTicks = numberMaxLinTicks = len(ax.get_xticks()) * 1.2  # * 2
    elif axes == "y":
        numberMaxLogTicks = numberMaxLinTicks = len(ax.get_yticks()) * 1.2  # * 2
    else:
        raise Exception(f"axes : {axes} not understood!")
    if axes == "x":
        Ymin, Ymax = ax.get_xlim()
    else:
        Ymin, Ymax = ax.get_ylim()
    NewYmin = Ymin * factor if not invertValues else 1.0 / Ymax * factor
    NewYmax = Ymax * factor if not invertValues else 1.0 / Ymin * factor
    if logY:
        majorTicks, minorTicks = setLogTicks(
            NewYmin, NewYmax, numberMax=numberMaxLogTicks, minormajorOverlap=False
        )
        ticks = majorTicks[:]
        if axes == "x":
            ax2.set_xscale("log")
        else:
            ax2.set_yscale("log")
    else:
        ticks = setLinTicksAlt(NewYmin, NewYmax, numberMax=numberMaxLinTicks)

    def tickConvert(X):
        return X / factor if not invertValues else 1 / (X / factor)

    minor = False
    if logY:
        if len(minorTicks) < numberMaxLogTicks:
            showMinorLabels = True
        elif len(majorTicks) <= 2:
            print("reducing minor ticks")
            showMinorLabels = True
            print(f"minor ticks: {minorTicks}")
            reducedMinorNumber = minorTicks[:]
            # index = 2
            # while len(reducedMinorNumber) > numberMaxLogTicks:
            base_first_value = 10 ** int(np.log10(minorTicks[0]))
            start = 0
            if minorTicks[0] % (2 * base_first_value):
                start = 1
            print(
                f"minorTicks[0]: {minorTicks[0]}, 2*base_first_value: {2*base_first_value}, minorTicks[0] % 2*base_first_value {minorTicks[0] % 2*base_first_value}"
            )
            reducedMinorNumber = minorTicks[start::2]
            # index += 1
            # print(f'in while index: {index}')
            print(f"reducedMinorNumber: {reducedMinorNumber}")
            minorTicks = reducedMinorNumber[:]
            print(f"reducedMinorNumber: {reducedMinorNumber}")
        else:
            showMinorLabels = False
        minor = True
        ticks = majorTicks
    print(
        f"twinY ticks: {ticks}, minorticks {minorTicks}, number of ticks possible: {numberMaxLogTicks}"
    )
    print(f"minor: {minor}")
    if axes == "x":
        ax2.set_xlim(ax.get_xlim())
    else:
        ax2.set_ylim(ax.get_ylim())
    if minor:
        if axes == "x":
            ax2.set_xticks(tickConvert(minorTicks), minor=True)
        else:
            ax2.set_yticks(tickConvert(minorTicks), minor=True)
        if showMinorLabels:
            if axes == "x":
                ax2.set_xticklabels(["%g" % x for x in minorTicks], minor=True)
            else:
                ax2.set_yticklabels(["%g" % x for x in minorTicks], minor=True)
        else:
            if axes == "x":
                ax2.set_xticklabels(["" for x in minorTicks], minor=True)
            else:
                ax2.set_yticklabels(["" for x in minorTicks], minor=True)
    if axes == "x":
        ax2.set_xticks(tickConvert(ticks), minor=False)
        if logX:
            ax2.set_xticklabels(format_floats_latex(ticks), minor=False)
        else:
            ax2.set_xticklabels(["%g" % x for x in ticks], minor=False)
        ax2.set_xlabel(ylabel)
    else:
        ax2.set_yticks(tickConvert(ticks), minor=False)
        ax2.set_yticklabels(["%g" % x for x in ticks], minor=False)
        ax2.set_ylabel(ylabel)
    # if axes == "x":
    ax2.tick_params(
        axis=axes, which="minor", direction=ticks_position, length=length_minor_ticks
    )
    ax2.tick_params(
        axis=axes, which="major", direction=ticks_position, length=length_major_ticks
    )
    if pad:
        ax2.xaxis.labelpad = pad
        # ax2.tick_params(axis=axes, which='major', pad=pad)
    return ax2


def add_text_plot(
    text, position="lower left", axes=None, alpha=1, color="black", alignment=None
):
    if axes is None:
        axes = plt.gca()
    if alignment is None and "right" in position:
        alignment = "right"
    if alignment is None:
        alignment = "left"
    if position == "lower left":
        from_left = 0.06
        from_bottom = 0.08
    elif position == "lower right":
        from_left = 0.94
        from_bottom = 0.08
    elif position == "central left" or position == "center left":
        from_left = 0.04
        from_bottom = 0.35
    elif position == "upper right":
        from_left = 0.94
        from_bottom = 0.87
    elif position == "upper left":
        from_left = 0.06
        from_bottom = 0.87
    elif position == "far upper left":
        from_left = 0.06
        from_bottom = 0.93
    else:
        raise Exception(f"position {position} not yet implemented!")
    axes.text(
        from_left,
        from_bottom,
        text,
        horizontalalignment=alignment,
        transform=axes.transAxes,
        color=color,
        alpha=alpha,
    )


def generateFilename(strings, stringNames, ending=".pdf"):
    name = []
    for st, nam in zip(strings, stringNames):
        if st is None:
            pass
        if type(st) == str:
            name += [nam + st]
        else:
            typ, ifcom = aux.checkCommonType(st)
            name += [nam + aux.convert_params_to_string(st, string=typ == str)]
    name = "_".join(name)
    name += ending
    return name


### LIST MANIPULATION


def isIterable(obj):
    if (
        type(obj) == np.array
        or type(obj) == np.ndarray
        or type(obj) == list
        or type(obj) == tuple
    ):
        return True
    return False


def initializeList(List, initilisation_element=None):
    depth = 1
    if isIterable(List[0]):
        depth = 2
        if isIterable(List[0][0]):
            depth = 3
    if depth == 1:
        init = [initilisation_element for x in List]
    elif depth == 2:
        init = [[initilisation_element for x in y] for y in List]
    elif depth == 3:
        init = [[[initilisation_element for z in x] for x in y] for y in List]
    return init


def save_pcolormesh(
    data,
    box_3d,
    axis,
    resx,
    resy,
    cmap="RdBu_r",
    log=False,
    vmin=None,
    vmax=None,
    arepo_positions=False,
    center=None,
    variable_name="name",
    variable_name_units="units",
    filename="../figures/maps/test.pdf",
):
    fig = plt.figure()
    ax = plt.axes([0.1, 0.1, 0.8, 0.8])
    ax_cb = plt.axes([0.9, 0.1, 0.1, 0.8])
    box_3d = np.array(box_3d, dtype=float)
    debug.printDebug(data, "data to plot")
    debug.printDebug(box_3d, "box_3d to plot")
    debug.printDebug(box_3d, "box_3d to plot")
    debug.printDebug(resx, "data to plot")
    debug.printDebug(resy, "resy to plot")
    vmin, vmax = get_vmin_vmax(
        vmin, vmax, None, None, data, symlog=True if log == "symlog" else False,
    )
    image = plot_pcolormesh(
        ax,
        data,
        box_3d,
        axis,
        resx,
        resy,
        cmap=cmap,
        log=log,
        vmin=vmin,
        vmax=vmax,
        arepo_positions=arepo_positions,
        center=center,
    )
    plot_colorbars(
        ax_cb,
        image,
        data,
        max_tick_labels=5,
        vmin=vmin,
        vmax=vmax,
        log=log,
        colorbar_vertical=True,
        variable_name=variable_name,
        variable_name_units=variable_name_units,
        show_label=True,
    )
    savePlot(fig, filename, dpi=400, bbox_inches="tight")


def plot_pcolormesh(
    ax,
    data,
    box_3d,
    axis,
    resx,
    resy,
    cmap="RdBu_r",
    log=False,
    vmin=None,
    vmax=None,
    arepo_positions=False,
    center=None,
    return_extent=False,
):
    if not arepo_positions:
        extent = [
            -box_3d[axis[0]] / 2,
            box_3d[axis[0]] / 2,
            -box_3d[axis[1]] / 2,
            box_3d[axis[1]] / 2,
        ]
    else:
        if center is None:
            raise Exception("need to define center if require arepo_positions!")
        extent = [
            center[axis[0]] - box_3d[axis[0]] / 2.0,
            center[axis[0]] + box_3d[axis[0]] / 2.0,
            center[axis[1]] - box_3d[axis[1]] / 2.0,
            center[axis[1]] + box_3d[axis[1]] / 2.0,
        ]
    print(f"plot_pcolormesh; extent: {extent}")
    yy = np.linspace(extent[2], extent[3], resx + 1,)
    xx = np.linspace(extent[0], extent[1], resy + 1,)
    norm = None
    if vmax != 0 and log == "symlog":
        norm = mpl.colors.SymLogNorm(linthresh=vmin, vmax=vmax, vmin=-vmax)
        vmin = None
        vmax = None
    elif vmax != 0 and log:
        norm = mpl.colors.LogNorm(vmax=vmax, vmin=vmin,)
        vmin = None
        vmax = None
    image = ax.pcolormesh(
        xx,
        yy,
        data,
        cmap=cmap,
        shading="flat",
        norm=norm,
        rasterized=True,
        vmin=vmin,
        vmax=vmax,
    )
    if return_extent:
        return image, extent
    return image


def plot_colorbars(
    ax_cb,
    image,
    data,
    max_tick_labels=5,
    vmin=None,
    vmax=None,
    log=False,
    colorbar_vertical=False,
    variable_name="name",
    variable_name_units="units",
    show_label=True,
    fontsize=10,
    ticks_above=False,
    label_above=False,
):
    colormap_orientation = "horizontal"
    if colorbar_vertical:
        colormap_orientation = "vertical"
    if log:
        major, minor = setLogTicks(
            vmin,
            vmax,
            numberMax=max_tick_labels,
            minormajorOverlap=True,
            returnOnlyMajorTicks=False,
        )
        if log == "symlog":
            major = [-x for x in major][::-1] + list(major)
            minor = [-x for x in minor][::-1] + list(minor)
        if len(major) < 2:
            ticks = minor
        else:
            ticks = major
        format = mpl.ticker.LogFormatterMathtext()
    else:
        ticks = setLinTicksAlt(
            vmin, vmax, numberMax=max_tick_labels, symmetricAroundZero=True,
        )
        format = mpl.ticker.ScalarFormatter()
        format = "%g"
    debug.printDebug(ticks, "ticks")
    ticks = np.array(ticks, dtype=float)
    cbar = plt.colorbar(
        image, cax=ax_cb, orientation=colormap_orientation, ticks=ticks, format=format,
    )
    units, offset = get_offset_units(
        cbar, ticks, variable_name_units, log, colorbar_vertical,
    )
    if ticks_above:
        cbar.ax.xaxis.set_ticks_position("top")
    if colorbar_vertical:
        ax_cb.set_xticklabels([])
        ax_cb.get_xaxis().set_visible(False)
    else:
        ax_cb.set_yticklabels([])
        ax_cb.get_yaxis().set_visible(False)
    if show_label:
        if label_above:
            cbar.ax.set_title(r"${}{}$".format(variable_name, units))
        elif colorbar_vertical:
            cbar.ax.set_ylabel(
                r"${}{}$".format(variable_name, units),
                rotation=270,
                labelpad=fontsize * 1.3,
            )
        else:
            cbar.ax.set_xlabel(r"${}{}$".format(variable_name, units),)
    else:
        if offset:
            if not colorbar_vertical:
                cbar.ax.set_xlabel(
                    r"$\times{}$".format(offset), x=1.12, labelpad=-1.8 * fontsize
                )
    return


def get_offset_units(cbar, ticks, name_units, log, colorbar_vertical=False, power=2):
    offset = ""
    units = ""
    if not log:
        max_power = int(np.floor(np.log10(np.max(ticks))))
        if abs(max_power) > power:
            if not colorbar_vertical:
                cbar.ax.set_xticklabels([x / 10 ** max_power for x in ticks])
            else:
                cbar.ax.set_yticklabels([x / 10 ** max_power for x in ticks])
            offset = "10^{%i}" % (max_power)
    if name_units and offset:
        units = "\\ [{}\\ {}]".format(offset, name_units)
    elif name_units:
        units = "\\ [{}]".format(name_units)
    elif offset:
        units = "\\times\\,{}".format(offset)
    print(f"offset: {offset}")
    print(f"name_units: {name_units}")
    return units, offset


def set_ticks(
    ax,
    axis="x",
    max_tick_labels=5,
    vmin=None,
    vmax=None,
    log=False,
    power=2,
    format="%g",
):
    from matplotlib.ticker import FormatStrFormatter

    offset = None
    if log:
        major, minor = setLogTicks(
            vmin,
            vmax,
            numberMax=max_tick_labels,
            minormajorOverlap=True,
            returnOnlyMajorTicks=False,
        )
        if len(major) < 2:
            ticks = minor
        else:
            ticks = major
        # format = mpl.ticker.LogFormatterMathtext()
    else:
        ticks = setLinTicksAlt(
            vmin, vmax, numberMax=max_tick_labels, symmetricAroundZero=True,
        )
        # format = mpl.ticker.ScalarFormatter()
    if not log:
        max_power = int(np.floor(np.log10(np.max(ticks))))
        if abs(max_power) > power:
            warnings.warn("OFFSET SHOULD BE TESTED AFTER CHANGES!!!")
            print(
                f"[x / 10 ** max_power for x in ticks]: {[x / 10 ** max_power for x in ticks]}"
            )
            if axis == "x":
                ax.set_xticks([x for x in ticks])
                ax.set_xticklabels([format % (x / 10 ** max_power) for x in ticks])
            else:
                ax.set_yticks([x for x in ticks])
                ax.set_yticklabels([format % (x / 10 ** max_power) for x in ticks])
            offset = "10^{%i}" % (max_power)

    if axis == "x":
        ax.set_xticks([x for x in ticks])
        ax.set_xticklabels([format % x for x in ticks])
    else:
        ax.set_yticks([x for x in ticks])
        ax.set_yticklabels([format % x for x in ticks])

    # if axis=="x":
    #     ax.xaxis.set_major_formatter(FormatStrFormatter(format))
    # else:
    #     ax.yaxis.set_major_formatter(FormatStrFormatter(format))
    print(f"ticks: {ticks}")
    return offset


def set_tick_positions(
    ax,
    y_ticks_both=True,
    y_tick_labels_both=False,
    major_tick_length=None,
    minor_tick_length=None,
    fontsize=None,
    labelleft=True,
    labelbottom=True,
):
    if y_ticks_both and y_tick_labels_both:
        ax.tick_params(
            labelsize=fontsize,
            direction="in",
            which="major",
            length=major_tick_length,
            left=True,
            right=True,
            labelleft=labelleft,
            labelright=True,
            labelbottom=labelbottom,
        )
    elif y_ticks_both:
        ax.tick_params(
            labelsize=fontsize,
            direction="in",
            which="major",
            length=major_tick_length,
            left=True,
            right=True,
            labelleft=labelleft,
            labelright=False,
            labelbottom=labelbottom,
        )
    else:
        ax.tick_params(
            labelsize=fontsize,
            direction="in",
            which="major",
            length=major_tick_length,
            labelleft=labelleft,
            labelbottom=labelbottom,
        )
    if y_ticks_both and y_tick_labels_both:
        ax.tick_params(
            labelsize=fontsize,
            direction="in",
            which="minor",
            length=minor_tick_length,
            left=True,
            right=True,
            labelleft=labelleft,
            labelright=True,
            labelbottom=labelbottom,
        )
    elif y_ticks_both:
        ax.tick_params(
            labelsize=fontsize,
            direction="in",
            which="minor",
            length=minor_tick_length,
            left=True,
            right=True,
            labelleft=labelleft,
            labelright=False,
            labelbottom=labelbottom,
        )
    else:
        ax.tick_params(
            labelsize=fontsize,
            direction="in",
            which="minor",
            length=minor_tick_length,
            labelbottom=labelbottom,
        )
    return


def _getVminVmax(
    vmin_initial,
    vmax_initial,
    info,
    time_float,
    data,
    variable,
    model_vmin_vmax,
    vmin_mapping=lambda data, vmax: 10.0 ** (int(np.log10(vmax)) - 3),
    vmax_mapping=lambda x: np.max(x),
    vmin_negative_mapping=lambda data, vmax: np.min(data),
    log=None,
):
    if log == "symlog":
        data = np.abs(data)
    negativeVMin = False
    if vmin_initial is not None and vmin_initial < 0:
        negativeVMin = True
    elif vmin_initial is None and np.min(data) < 0:
        negativeVMin = True
    print("negativeVMin %i" % int(negativeVMin))
    if vmax_initial is None:
        vmax = vmax_mapping(data)  # np.max(data)
        print("max data %g" % vmax)
    else:
        vmax = vmax_initial
        if model_vmin_vmax is not None:
            if variable in model_vmin_vmax:
                vmax = info["modelfunc"](time_float, info["modelPara"])
    if (vmin_initial is None) and vmax != 0 and not negativeVMin:
        vmin = vmin_mapping(data, vmax)  # 10.0 ** (int(np.log10(vmax)) - 3)
        if vmin < np.min(data):
            vmin = np.min(data)
        print("--------------")
        print("missing fraction %g" % (np.sum(data[data < vmin]) / np.sum(data)))
        print("---------------")
    elif vmin_initial is None and negativeVMin:
        vmin = vmin_negative_mapping(data, vmax)  # np.min(data)
    else:
        vmin = vmin_initial
        if model_vmin_vmax is not None:
            if variable in model_vmin_vmax:
                vmin = vmax * 1e-3
                print(f"vmin for CR energy density: {vmin}")
    if vmin is None or vmax is None:
        if (
            aux.checkIfInDictionary("allow0VminVax", info, False)
            and np.min(data) == 0
            and np.max(data) == 0
        ):
            vmin = 0
            vmax = 0
        elif vmin is None and vmax == 0 and not negativeVMin:
            print("all values are 0 -> setting vmin=0.1, vmax=1 (so log is availale)")
            vmin = 0.1
            vmax = 1
        else:
            print(vmin, vmax)
            debug.printDebug(data, zero=True)
            raise Exception("vmin or vmax is None")
    if aux.checkIfInDictionary("vmin_vmax_symmetric_zero", info, True) and (
        vmin < 0 and vmax > 0
    ):
        vmin, vmax = -np.max([abs(vmin), vmax]), np.max([abs(vmin), vmax])
    return vmin, vmax


def _text_time_folder(
    ax,
    text="text",
    text_color="black",
    text_position="bottomleft",
    text_style="",
    fontsize=10,
):
    from_left = 0.04
    from_bottom = 0.96
    verticalalignment = "top"
    horizontalalignment = "left"
    if "left" in text_position:
        from_left = 0.04
        horizontalalignment = "left"
    elif "right" in text_position:
        from_left = 0.96
        horizontalalignment = "right"
    if "bottom" in text_position:
        from_bottom = 0.04
        verticalalignment = "bottom"
    elif "top" in text_position:
        verticalalignment = "top"
        from_bottom = 0.96
    text = ax.text(
        from_left,
        from_bottom,
        text,
        horizontalalignment=horizontalalignment,
        verticalalignment=verticalalignment,
        transform=ax.transAxes,
        color=text_color,
        alpha=1.0,
        rasterized=True,
    )
    _set_text_effects(text, text_style, fontsize)


def _scale_bar(
    axes,
    extent,
    scale_bar="default",
    scale_bar_label=None,
    scale_bar_position="default",
    color="black",
    scale_bar_show_label=True,
    text_style="",
    fontsize=10,
    linewidth=1,
):
    box_x = extent[1] - extent[0]
    box_y = extent[3] - extent[2]
    if scale_bar == "default":
        scale_bar = box_x / 4
        print(f"using default value scale_bar: {scale_bar}")
    rel_offset_x = 0.16
    rel_offset_y = 0.85
    rel_offset_x_left = 0.07
    pos1 = extent[0] + rel_offset_x * box_x + scale_bar
    pos0 = extent[0] + rel_offset_x * box_x
    print(f"scale_bar_position: {scale_bar_position}")
    if scale_bar_position == "default":
        scale_bar_position = "upperright"
    if scale_bar_position == "lowerleft":
        rel_offset_y = 0.07
    if scale_bar_position == "lowerupperleft":
        rel_offset_y = 0.2
    if scale_bar_position == "upperright":
        rel_offset_x_right = 0.07
    if scale_bar_position == "farupperright":
        rel_offset_x_right = 0.05
        rel_offset_y = 0.87
    if scale_bar_position == "lowerright":
        rel_offset_x_right = 0.07
        rel_offset_y = 0.07
    if scale_bar_position == "centerleft":
        rel_offset_y = 0.5
    if "right" in scale_bar_position:
        pos1 = extent[0] + box_x - rel_offset_x_right * box_x
        pos0 = extent[0] + box_x - rel_offset_x_right * box_x - scale_bar
    elif "left" in scale_bar_position:
        pos0 = extent[0] + rel_offset_x_left * box_x
        pos1 = extent[0] + scale_bar + rel_offset_x_left * box_x
    pos = [
        pos0,
        pos1,
        extent[2] + rel_offset_x * box_y,
        extent[2] + rel_offset_y * box_y,
    ]
    if "border" in text_style:
        outline = [path_effects.withStroke(linewidth=2, foreground="black")]
    else:
        outline = None
    axes.plot(
        (pos[0], pos[1]),
        (pos[3], pos[3]),
        "-",
        color=color,
        linewidth=linewidth,
        path_effects=outline,
    )
    if scale_bar_label is None:
        scale_bar_label = f"{scale_bar} kpc"
    if scale_bar_show_label:
        text = axes.text(
            pos[0] + (pos[1] - pos[0]) / 2.0,
            extent[2] + rel_offset_y * box_y + 0.03 * box_y,
            scale_bar_label,
            horizontalalignment="center",
            color=color,
        )
        _set_text_effects(text, text_style, fontsize)


def _set_text_effects(text, style, fontsize):
    if "border" in style:
        text.set_path_effects(
            [
                path_effects.Stroke(linewidth=fontsize / 6.0, foreground="black"),
                path_effects.Normal(),
            ]
        )


def prepare_mesh_plot(
    ProjBox,
    resolution=100,
    depth=None,
    res_depth=None,
    axis=[2, 0],
    snap=None,
    center=None,
    return_center=False,
):
    if return_center and center is None:
        center = snap.center
    box_3d = [ProjBox for x in range(3)]
    resolution_3d = [resolution for x in range(3)]
    axis_depth = 3 - sum(axis)
    resolution_3d[axis_depth] = res_depth
    box_3d[axis_depth] = depth
    if return_center:
        return box_3d, resolution_3d, center
    return box_3d, resolution_3d


def get_mesh_data(
    snap,
    axis,
    box_3d,
    variable,
    resolution_3d,
    center,
    weight=None,
    info=None,
    plot_type="isolate_features",
    contour=False,
    numthreads=4,
    use_weight_thresholds=False,
    reimportStrVar=True,
    **kwargs,
):
    from feature_extraction import do_IsolateFeatures

    res_slice = [resolution_3d[axis[0]], resolution_3d[axis[1]]]
    box_2d = [box_3d[axis[0]], box_3d[axis[1]]]
    data = _get_plot_data(
        snap,
        axis,
        variable,
        weight,
        info,
        plot_type,
        resolution_3d,
        res_slice,
        box_3d,
        box_2d,
        center,
        verbose=0,
        use_weight_thresholds=use_weight_thresholds,
        threads=numthreads,
        reimportStrVar=reimportStrVar,
        **kwargs,
    )
    contour_data = None
    if contour:
        contour_data = aux.get_contour_data(
            data,
            axis,
            box_3d,
            center=center,
            filter=None,
            filter_num=None,
            max_value=None,
            info=None,
            return_dic=True,
        )
    return data, contour_data


def _get_plot_data(
    snap,
    axis,
    variable,
    weight,
    info,
    plot_type,
    res,
    res_slice,
    box_3d,
    box_2d,
    center,
    name_sim_save="test",
    verbose=0,
    use_weight_thresholds=False,
    threads=4,
    reimportStrVar=True,
    **kwargs,
):
    from feature_extraction import do_IsolateFeatures

    if plot_type != "external" and variable not in snap.data.keys():
        VARIABLE, INFO = getVariable(
            snap, variable, INFO=info, returnINFO=True, verbose=verbose
        )
        if verbose:
            print("variable min %g and max %g" % (np.min(VARIABLE), np.max(VARIABLE)))
    else:
        if info is not None:
            INFO = info.copy()
        else:
            INFO = None
        VARIABLE = variable
    if weight in snap.data.keys():
        WEIGHTS = weight
    elif weight is not None and plot_type not in ["slice"]:
        WEIGHTS = getWeights(
            snap, weight, INFO=info, useWeightsThresholds=use_weight_thresholds,
        )
        if verbose:
            print("weights min%g and max %g" % (np.min(WEIGHTS), np.max(WEIGHTS)))
            print(
                "number cells with nonero value of weights %g"
                % np.size(WEIGHTS[WEIGHTS != 0])
            )
    else:
        WEIGHTS = None
    if plot_type == "slice":
        data = do_Slice(
            snap, VARIABLE, res_slice, box_2d, center, threads, axis, verbose=verbose,
        )
    elif plot_type == "pixel_integral":
        histNameExt = _get_histogram_name_pixel_int()
        data = do_PixelIntegral(
            snap,
            VARIABLE,
            WEIGHTS,
            res,
            box_3d,
            center,
            threads,
            axis,
            INFO=info,
            histNameExt=histNameExt,
        )
    elif plot_type == "isolate_features":
        data = do_IsolateFeatures(
            snap,
            VARIABLE,
            box_3d,
            weight=WEIGHTS,
            resolution=res,
            center=center,
            numthreads=threads,
            axis=axis,
            INFO=info,
            verbose=verbose,
            **kwargs,
        )
    elif plot_type == "rotation_measure":
        data = do_RMmap(snap, VARIABLE, res, box_3d, center, threads, axis, INFO=info,)
    elif plot_type == "projection_twice":
        data = do_ProjectionTwice(
            snap,
            folder,
            fields_loaded,
            VARIABLE,
            WEIGHTS,
            res,
            box_3d,
            center,
            threads,
            axis,
            info,
            volumeIntegral=False,
            useTime=use_time,
            useWeightsThresholds=use_weight_thresholds,
            inputGrids=False,
        )
    elif plot_type == "sz_map":
        data = do_SZmap(snap, VARIABLE, res, box_3d, center, threads, axis, INFO=info,)
    elif plot_type == "sz_effect":
        sz = _prepare_sz_effect_plot(VARIABLE)
        data = sz.get_SZEffect(
            variable=info["variable_to_get"],
            Variables=VARIABLE,
            spatialSpacingArrays_in_cm=box_3d[proj_axis]
            / res[proj_axis]
            / length_to_unit_length_code
            * snap.UnitLength_in_cm,
            integratingAxis=proj_axis,
            INFO=info,
        )
    elif plot_type == "sum_projection":
        # do projection individually for dic of arrays and sum them afterwards
        for variable in list(VARIABLE.keys()):
            VARIABLE[variable] = do_Projection(
                snap,
                VARIABLE[variable],
                None,
                res,
                box_3d,
                center,
                threads,
                axis,
                INFO=info,
            )
        data = np.zeros(np.shape(VARIABLE[list(VARIABLE.keys())[0]]))
        for variable in list(VARIABLE.keys()):
            data += VARIABLE[variable]
        if aux.checkExistenceKey(info, "AbsSZSignal", False):
            data = abs(data)
    elif plot_type == "integral":
        data = do_Projection(
            snap,
            VARIABLE,
            WEIGHTS,
            res,
            box_3d,
            center,
            threads,
            axis,
            sumAlongAxis=True,
            INFO=info,
        )
    elif plot_type == "projection":
        data = do_Projection(
            snap,
            VARIABLE,
            WEIGHTS,
            res,
            box_3d,
            center,
            threads,
            axis,
            sumAlongAxis=False,
            INFO=INFO,
            reimportStrVar=reimportStrVar,
        )
    elif plot_type == "external":
        filename = os.path.join("Data/GridPlot/", name_sim_save, variable + ".txt")
        data = np.loadtxt(filename)
        data = data.T
    else:
        raise Exception(f"plot_type {plot_type} not understood!")
    if aux.checkExistenceKey(info, "ABS", False):
        data = abs(data)
    if np.isnan(np.sum(data)):
        warnings.warn(f"data: {data} \n Nan in self.data! {snap.file_name_abs}")
        data[np.isnan(data)] = 0
    return data


def do_pcolormesh_plot(
    data,
    box_3d,
    axis,
    resolution,
    center,
    axes=None,
    axes_colorbar=None,
    contour_data=None,
    contour_levels=None,
    scale_bar=None,
    vmin=None,
    vmax=None,
    log=False,
    fontsize=6,
    variable_name="\\mathrm{feature}",
    variable_name_units="",
    cmap="Spectral",
    alpha_contour=1,
):
    vmin, vmax = _getVminVmax(vmin, vmax, None, None, data, "None", [], log=log)
    image, extent = plot_pcolormesh(
        axes,
        data,
        box_3d,
        axis,
        resolution,
        resolution,
        cmap=cmap,
        log=log,
        vmin=vmin,
        vmax=vmax,
        arepo_positions=True,
        center=center,
        return_extent=True,
    )
    contour_image = None
    if contour_data is not None:
        colors_start, colors_finish = get_colors_examples_gradients()
        colors = aux.linear_gradient(
            colors_start[0], colors_finish[0], n=len(contour_levels)
        )
        contour_image = axes.contour(
            contour_data["X"],
            contour_data["Y"],
            contour_data["Z"],
            contour_levels,
            colors=colors,
            linewidths=1,
            alpha=alpha_contour,
        )
    if scale_bar is not None:
        _scale_bar(
            axes,
            extent,
            scale_bar=scale_bar,
            scale_bar_label=r"$%i\,\mathrm{kpc}$" % scale_bar,
            scale_bar_position="farupperright",
            color="black",
            scale_bar_show_label=True,
            text_style="",
            fontsize=fontsize,
            linewidth=1.5,
        )
    plot_colorbars(
        axes_colorbar,
        image,
        data,
        max_tick_labels=5,
        vmin=vmin,
        vmax=vmax,
        log=log,
        colorbar_vertical=True,
        variable_name=variable_name,
        variable_name_units=variable_name_units,
        fontsize=fontsize,
    )
    axes.set_aspect(box_3d[axis[0]] / box_3d[axis[1]])
    axes.get_xaxis().set_ticks([])
    axes.get_yaxis().set_ticks([])
    return contour_image, extent


if __name__ == "__main__":

    colors_start = [
        "#b6bfc8",
        "#e57f7f",
        "#99eaea",
        "#a0a9ab",
        "#d2c9ff",
        "#82b983",
        "#f5e9e5",
        "#8e768a",
        "#4cdbdb",
    ] * 2
    colors_end = [
        "#091d32",
        "#510000",
        "#003d3d",
        "#273134",
        "#4c4766",
        "#264d27",
        "#a5938e",
        "#2f1e2c",
        "#005151",
    ] * 2
    for d in [[1, 0, 0], [0, 1, 0], [0, 1, 1], [0, 0, 0]]:
        x, y, z = d
        print(x, y, z)
        c = getColor(
            x,
            y,
            z,
            3,
            2,
            2,
            useGradient=True,
            AlongColor=2,
            AlongColorGradient="01",
            color=colors_start,
            colorstart=colors_start,
            colorfinish=colors_end,
        )
        print(c)


def format_floats_latex(x):
    def num(s):
        """ 3.0 -> 3, 3.001000 -> 3.001 otherwise return s """
        s = str(s)
        try:
            int(float(s))
            return s.rstrip("0").rstrip(".")
        except ValueError:
            return s

    # import matplotlib.ticker as mticker
    # f = mticker.ScalarFormatter(useOffset=False, useMathText=True)
    # g = lambda x, pos: "${}$".format(f._formatSciNotation('%1.10e' % x))
    # ax2.xaxis.set_major_formatter(mticker.FuncFormatter(g))
    x = ["{:.0e}".format(i).replace("e", "\\times10^{").replace("+0", "") for i in x]
    if all(i.startswith("1\\times") for i in x):
        x = [i.replace("1\\times", "") for i in x]
    return [r"$%s}$" % i for i in x]


def format_integer(value, format):
    if format == "integer" or format == "int":
        string = r"%i Myr" % (value)
    elif format == "integer10" or format == "int10":
        string = r"%i Myr" % round(value, -1)
    else:
        raise Exception(f"format: {format} not understood!")
    return string


def do_Plot(
    X,
    Y,
    fig=None,
    ax=None,
    Label=None,
    Colors=None,
    Marker=None,
    Linestyle=None,
    xlog=False,
    ylog=False,
    ylabel=None,
    xlabel=None,
    individualPlot=True,
    folder="figures/",
    filename="test",
    filetype="pdf",
    savefig=True,
    xlim=None,
    ylim=None,
    dpi=450,
):
    filename = folder + filename + "." + filetype
    X, Y, Label, Colors, Marker, Linestyle = makeIterableMassMax(
	(X, 1), (Y, 1), (Label, 0), (Colors, 0), (Marker, 0), (Linestyle, 0)
    )

    if fig is None:
        plt.figure()
    if ax is None:
        ax = plt.gca()
    for x, y, lab, col, marker, ls in zip(X, Y, Label, Colors, Marker, Linestyle):
        ax.plot(x, y, ls=ls, marker=marker, color=col, label=lab)
    if any([l is not None for l in Label]):
        ax.legend(loc="upper right", frameon=True, numpoints=1, labelspacing=0.2)
    if xlog:
        ax.set_xscale("log")
    if ylog:
        ax.set_yscale("log")
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    if individualPlot:
        ax.set_xlabel(xlabel)
    else:
        plt.setp(ax.get_xticklabels(), visible=False)
    ax.set_ylabel(ylabel)
    aux.create_dirs_for_file(filename)
    aux.checkExistence_delete_file(filename)

    if savefig:
        plt.savefig(filename, dpi=dpi)
        print("... saved %s" % filename)
    return fig, ax


def makeIterableMassMax(*args):
    # make iterable all objects given in list if list:(object, 1) else (object,2)                                                                                                                                  
    Num = []
    for obj in args:
        obj, List = obj
        num = 1
        if List and not checkDimensionList2D(obj):
            pass
        else:
            if obj is not None:
                num = len(obj)
        Num.append(num)
    MAX = np.max(Num)
    rtn = []
    for obj in args:
        obj, List = obj
        obj = makeIterable(obj, MAX, List=False)
        rtn.append(obj)
    return rtn
