import numpy as np
import os
import auxiliary_functions as aux
import FigureMove as Fig
import time as TimeModule
import collections
import copy
import re
import csv
import debug

# deals with most file management
def checkListArrayTuple(object):
    if type(object) == list or type(object) == np.array or type(object) == tuple:
        return True
    return False


def makeIterable(object, Number):
    import collections

    if not checkListArrayTuple(object):
        object = [object for x in range(Number)]
        # print(object)
    else:
        if not checkListArrayTuple(object[0]):
            # print(object)
            object = [object for x in range(Number)]
    return object


def calculateRow(
    function,
    functionParameters,
    relevantElementsNumbers,
    relevantElements,
    lengthHeader,
):
    # order output of function in same way should appear in row!!
    try:
        values = function(*functionParameters)
    except RecursionError:
        raise Exception(
            f"recursion error; "
            f"function: {function}, "
            f"functionParameters: {functionParameters}, "
            f"relevantElements: {relevantElements}, "
        )
    # print(type(values))
    # print(values)
    values = makeIterable(values, 1)
    if lengthHeader != np.shape(values)[0] + len(relevantElements):
        print(lengthHeader)
        print(values)
        print(relevantElements)
        print(np.shape(values)[0])
        print(len(relevantElements))
        # print lengthHeader
        exit("incorrect number of values to write row")
    row = [None for i in range(lengthHeader)]
    indexRelevant = 0
    indexValues = 0
    for i in range(lengthHeader):
        if i in relevantElementsNumbers:
            row[i] = relevantElements[indexRelevant]
            indexRelevant += 1
        else:
            row[i] = values[indexValues]
            indexValues += 1
    return row


def initializeFileCSV(filename, header, row):
    aux.create_dirs_for_file(filename)
    with open(filename, "w") as csvfile:
        spamwriter = csv.writer(csvfile)
        spamwriter.writerow(header)
        spamwriter.writerow(row)


def appendFileCSV(filename, header, row):
    with open(filename, mode="a") as infile:
        writer = csv.writer(infile)
        writer.writerow(row)


#    else:
#        print 'number of values does not match headersize'


def isString(value):
    import ast

    try:
        value = ast.literal_eval(value)
        return value
    except ValueError:
        return value


def convertValueFromFile(value):
    value = isString(value)
    if checkListArrayTuple(value):
        value = [x if type(x) == str else float(x) for x in value]
        return value
    if type(value) != str and type(value) is not np.str_:
        value = float(value)
    return value


def checkRowExistsCSV(Rows, comparer, relevantElements):
    NumberLines, NumberColumns = np.shape(Rows)
    NrelevantElements = len(relevantElements)
    for i in range(NumberLines):
        correctnes = 0
        for j in range(NumberColumns):
            if comparer[j] == convertValueFromFile(Rows[i][j]):  # ast.literal_eval()):
                correctnes += 1
        if correctnes == NrelevantElements:
            return [convertValueFromFile(x) for x in Rows[i]]
    return None


def createComparerCSV(lengthHeader, relevantElements, relevantElementsNumbers):
    comp = [None for i in range(lengthHeader)]
    for index, i in enumerate(relevantElementsNumbers):
        comp[i] = convertValueFromFile(relevantElements[index])
    return comp


def readFileCSVFAST(filename, return_header=False):
    Rows = []
    with open(filename, mode="rt") as infile:
        reader = csv.reader(infile)
        header = next(reader)
        for r in reader:
            Rows.append(np.array(r))
    if not return_header:
        return np.array(Rows)
    else:
        return np.array(Rows), header


def readFileCSV(filename):
    Rows = []
    with open(filename, mode="rt") as infile:
        reader = csv.reader(infile)
        header = next(reader)
        for r in reader:
            Rows.append(r)
    return Rows


def extractDataRowCSV(row, relevantElementsNumbers):
    s = []
    for i in range(len(row)):
        if i in relevantElementsNumbers:
            continue
        s.append(row[i])
    return s


def extractDataRowsCSV(
    data,
    checkIndices=[0],
    checks=[[1]],
    arrayToCheckIndex=0,
    dtype_check=float,
    filename=None,
):
    """
        returns columns whose data in row at position checkIndices corresponds to respective checks (in ascending order according to arraycheck), 
        in addition return integers that couldn't be found

        data: array storing data from file with Nrow, Ncol = np.shape(data)
        checkIndices: which position in check Indices corresponds to arrayToCheck
        checks: list of parameters for corresponding columns specified in checkIndices, arrayToCheck is listed here as well
        arrayIndex: index of checkIndices that corresponds to position of arrayToCheck in checks/checkIndices

    """
    assert len(checks) != 0, "need to check for something"
    assert len(checkIndices) == len(checks), "checkindices and check need same length"
    try:
        Nrow, Ncol = np.shape(data)
    except:
        raise Exception(
            f"couldnt determine Nrow, Ncol = np.shape(data), data: {data}, filename: {filename}"
        )
    indexCheckArray = checkIndices[arrayToCheckIndex]
    checkArray = checks[arrayToCheckIndex]
    if not aux.isIterable(checkArray):
        checkArray = [checkArray]
    checkOther = []
    if len(checks) > 1:
        checkOther = [x for index, x in enumerate(checks) if index != arrayToCheckIndex]
        checkIndicesOther = [
            x for index, x in enumerate(checkIndices) if index != arrayToCheckIndex
        ]
    Nother = len(checkOther)

    def checkAll():
        if Nother == 0:
            return True
        mask = data[:, checkIndicesOther[0]] == str(checkOther[0])
        for i in range(1, Nother):
            newMask = data[:, checkIndicesOther[i]] == str(checkOther[i])
            mask = mask * newMask
        return mask

    mask = checkAll()
    mask = np.isin(data[:, indexCheckArray], np.array(checkArray, dtype=str)) * mask
    maskSort = np.argsort(np.array(data[:, indexCheckArray][mask], dtype=dtype_check))
    # toReturn = []
    toReturn = []
    print(f"maskSort: {maskSort}")
    print(f"data[:, indexCheckArray][mask]: {data[:, indexCheckArray][mask]}")
    print(f"checkIndices: {checkIndices}")
    for ind in range(Ncol):
        if ind not in checkIndices:
            print(f"(data[:, ind][mask])[maskSort]: {(data[:, ind][mask])[maskSort]}")
            toReturn.append((data[:, ind][mask])[maskSort])
    # toReturn = np.array(toReturn)[mask]
    return (
        toReturn,
        np.array(
            [
                x
                for x in np.array(checks[indexCheckArray], dtype=str)
                if x not in data[:, indexCheckArray][mask]
            ]
        ),
    )


def checkRetrieveValueCSVFAST(
    filename,
    header,
    dtypesFloat=float,
    relevantElementsNumbers=[0],
    relevantElements=[0],
    removeFile=False,
    arrayToCheckIndex=0,
    ifAllFoundReturn=[None],
):
    """
    #more efficient if specify list of elements as first relevantElements
    #assume first element snapshot number
    
    return (True/False if data exists/doesnt exist, Values, missing arrayvalues) 
    """
    file_exists = os.path.isfile(filename)
    if file_exists and removeFile:
        os.remove(filename)
        file_exists = False
    for index, elem in enumerate(relevantElements):
        typ, com = aux.checkCommonType(elem)
        if typ is not None and typ is not str:
            relevantElements[index] = aux.convertDataType(
                relevantElements[index], dtype=dtypesFloat
            )
    lengthHeader = len(header)
    NotFoundArrayValues = relevantElements[arrayToCheckIndex]
    Values = None
    # exit()
    if file_exists:
        if len(relevantElements) != len(relevantElementsNumbers):
            print(relevantElements, relevantElementsNumbers)
            raise Exception(
                "need same amount of elements as numbers where found in line!"
            )
        data = readFileCSVFAST(filename)
        Values, NotFoundArrayValues = extractDataRowsCSV(
            data,
            checkIndices=relevantElementsNumbers,
            checks=relevantElements,
            arrayToCheckIndex=arrayToCheckIndex,
            filename=filename,
        )
        Values = aux.convertDataType(Values, dtype=convertValueFromFile)
        # row = [convertValueFromFile(x) for x in Rows[0]]
    dataExists = len(NotFoundArrayValues) == 0
    if dataExists:
        NotFoundArrayValues = [np.nan]
    NotFoundArrayValues = [dtypesFloat(x) for x in NotFoundArrayValues]
    return dataExists, Values, NotFoundArrayValues


def addValueCSVFAST(
    filename,
    header,
    function,
    functionParameters,
    dtypesFloat=float,
    relevantElementsNumbers=[0],
    relevantElements=[0],
):
    """
        add value to csv file
    """
    for index, elem in enumerate(relevantElements):
        typ, com = aux.checkCommonType(elem)
        if typ is not None and typ is not str:
            relevantElements[index] = aux.convertDataType(
                relevantElements[index], dtype=dtypesFloat
            )
    lengthHeader = len(header)
    row = calculateRow(
        function,
        functionParameters,
        relevantElementsNumbers,
        relevantElements,
        lengthHeader,
    )
    file_exists = os.path.isfile(filename)
    if not file_exists:
        print(f"filename: {filename}, file_exists: {file_exists}")
        initializeFileCSV(filename, header, row)
    else:
        print(f"filename: {filename}, appendFileCSV: {file_exists}")
        appendFileCSV(filename, header, row)
    return row


def check_retrieve_write_values_csv_fast(
    filename,
    header,
    function,
    functionParameters,
    dtypesFloat=float,
    relevantElementsNumbers=[0],
    relevantElements=[0],
    removeFile=False,
    arrayToCheckIndex=0,
    ifAllFoundReturn=[None],
):
    dataExists, Values, NotFoundArrayValues = checkRetrieveValueCSVFAST(
        filename,
        header,
        dtypesFloat=dtypesFloat,
        relevantElementsNumbers=relevantElementsNumbers,
        relevantElements=relevantElements,
        removeFile=removeFile,
        arrayToCheckIndex=arrayToCheckIndex,
        ifAllFoundReturn=ifAllFoundReturn,
    )
    if dataExists:
        return Values

    def retrieve_values(x, numbers):
        if x == "numbers":
            return numbers
        else:
            return x

    relevantElements = [
        retrieve_values(x, NotFoundArrayValues) for x in relevantElements
    ]
    addValueCSVFAST(
        filename,
        header,
        function,
        functionParameters,
        dtypesFloat=dtypesFloat,
        relevantElementsNumbers=relevantElementsNumbers,
        relevantElements=relevantElements,
    )
    dataExists, Values, NotFoundArrayValues = checkRetrieveValueCSVFAST(
        filename,
        header,
        dtypesFloat=dtypesFloat,
        relevantElementsNumbers=relevantElementsNumbers,
        relevantElements=relevantElements,
        removeFile=removeFile,
        arrayToCheckIndex=arrayToCheckIndex,
        ifAllFoundReturn=ifAllFoundReturn,
    )
    if not dataExists:
        raise Exception("couldnt retrieve values after trying to add!")
    else:
        return Values


def checkRetrieveValueCSV(
    filename,
    header,
    function,
    functionParameters,
    relevantElementsNumbers=[0],
    relevantElements=[0],
    removeFile=False,
    onlycheckDataExist=False,
):
    file_exists = os.path.isfile(filename)
    if file_exists and removeFile:
        os.remove(filename)
    lengthHeader = len(header)
    row = None
    # exit()
    if file_exists:
        if len(relevantElements) != len(relevantElementsNumbers):
            exit("need same amount of elements as numbers where found in line!")
        Rows = readFileCSV(filename)
        if len(relevantElements) > 0:
            comparer = createComparerCSV(
                lengthHeader, relevantElements, relevantElementsNumbers
            )
            row = checkRowExistsCSV(Rows, comparer, relevantElements)
        else:
            row = [convertValueFromFile(x) for x in Rows[0]]
    if onlycheckDataExist:
        if row is None:
            return False
        else:
            return True
    if row is None:
        row = calculateRow(
            function,
            functionParameters,
            relevantElementsNumbers,
            relevantElements,
            lengthHeader,
        )
        if not file_exists:
            initializeFileCSV(filename, header, row)
        else:
            appendFileCSV(filename, header, row)
    row = extractDataRowCSV(row, relevantElementsNumbers)
    return row[0]


if __name__ == "__main__":

    def nothing(N):
        print("doing nothing x 3")
        return N  # , 1321 * N, N * 2

    for i in range(7):
        filename = "test.txt"
        header = ("N", "Those")  # ('This',) #,
        function = nothing
        functionParameters = (i,)
        relevantElementsNumbers = [0]  # []#
        relevantElements = [[1, 2]]  # tuple()#
        addValueCSVFAST(
            filename,
            header,
            function,
            functionParameters,
            dtypesFloat=float,
            relevantElementsNumbers=relevantElementsNumbers,
            relevantElements=relevantElementsNumbers,
        )
        values = checkRetrieveValueCSVFAST(
            filename=filename,
            header=header,
            relevantElementsNumbers=relevantElementsNumbers,
            relevantElements=relevantElements,
            removeFile=False,
        )
    print(values)


def saveYML(fname, data):
    import yaml

    with open(fname, "w") as outfile:
        yaml.dump(data, outfile, default_flow_style=False)


def loadYML(fname):
    import yaml

    with open(fname) as file:
        # The FullLoader parameter handles the conversion from YAML
        # scalar values to Python the dictionary format
        data = yaml.load(file, Loader=yaml.FullLoader)
    return data


def kwrgsToDic(**kwrgs):
    return locals()["kwrgs"]


def reduceDicInDic(dictionary):
    # convert dic in dic to keywords and values of main dic
    for keyword in list(dictionary.keys()):
        if type(dictionary[keyword]) == dict:
            subDic = dictionary[keyword]
            for keySub in list(subDic.keys()):
                if keySub not in dictionary.keys():
                    dictionary[keySub] = subDic[keySub]
                else:
                    raise Exception(
                        'key of subdic "%s" already present in dictionary!' % keySub
                    )
            del dictionary[keyword]
    return dictionary


def mergeDictionaries(OverwrittenDic, DicWithPriority, allowOverwrittenKeywords=False):
    if allowOverwrittenKeywords:
        dicNew = mergeDictionariesOverwrite(OverwrittenDic, DicWithPriority)
    else:
        dicNew = createDictionary(
            OverwrittenDic=OverwrittenDic, DicWithPriority=DicWithPriority
        )
    return dicNew


def mergeDictionariesOverwrite(OverwrittenDic, DicWithPriority):
    dicNew = {**OverwrittenDic, **DicWithPriority}
    return dicNew


def createDictionary(reduceSubDics=True, **kwrgs):
    dic = kwrgsToDic(**kwrgs)
    if reduceSubDics:
        dic = reduceDicInDic(dic)
    return dic


def checkRedo(pathSaveFiles, dicCheckParameters, fileString, ignore=[], verbose=1):
    import h5py

    any_path_exists = 0
    for path in pathSaveFiles:
        if os.path.exists(path):
            if verbose:
                print(f"path {path} exists")
            any_path_exists = 1
    if not any_path_exists:
        return False, None
    # checks all files in pathSaveFiles that contain fileString if attr of file consistent with reduced dictionary entries in dicCheckParameters
    files = Fig.findAllFilesWithEnding(pathSaveFiles, ".hdf5")
    files = checkName(files, fileString)
    file_same = False
    if verbose:
        print(f"gonna check {len(files)} files")
    for file in files:
        try:
            with h5py.File(file, "r") as f:
                try:
                    file_same = compareAttributesFileWithDic(
                        f, dicCheckParameters, ignore=ignore
                    )
                except IOError:
                    print(
                        " inner loop.............................. ========> removing buggy file!"
                    )
                    aux.checkExistence_delete_file(file)
                    file_same = False
                except:
                    print("unexpected error!")
                    raise
                if file_same:
                    if verbose:
                        print("matching file %s found!" % file)
                    break
        except IOError:
            print(".............................. ========> removing buggy file!")
            aux.checkExistence_delete_file(file)
            file_same = False
    if file_same:
        return True, file
    else:
        if verbose:
            print(".............................. ========> no matching file found!")
        return False, None


def convertSubDicsToKeywords(dic, addAdditional="DICT__", inplace=False, ignore=[None]):
    if not inplace:
        dic = copy.deepcopy(dic)
    subs = []
    subInitName = []
    for k, v in dic.items():
        if type(v) == dict:
            subs.append(
                {k + addAdditional + i: j for i, j in v.items() if j not in ignore}
            )
            subInitName.append(k)
    for k in subInitName:
        dic.pop(k)
    for s in subs:
        dic = {**dic, **s}  # left overwritten if keys same !
    return dic


def convertKeywordsToSubDics(dic, addAdditional="DICT__", inplace=False):
    if not inplace:
        dic = copy.deepcopy(dic)
    keyInitName = []
    NewDics = {}
    for k, v in dic.items():
        dicName, match, keyName = k.partition(addAdditional)
        if bool(match):
            if dicName not in NewDics.keys():
                NewDics[dicName] = {keyName: v}
            else:
                NewDics[dicName][keyName] = v
            keyInitName.append(k)
    for name in keyInitName:
        dic.pop(name)
    dic = {**dic, **NewDics}  # left overwritten if keys same !
    return dic


def checkName(files, fileString):
    # return files that contain fileString
    FILES = []
    for file in files:
        if fileString in os.path.basename(file):
            FILES.append(file)
    return FILES


def getFileMaxNum(files):
    import re

    if len(files) == 0:
        print("no file found, max num set to -1")
        return -1
    else:
        Num = []
        for f in files:
            num = re.findall(r"_([0-9]+).hdf5$", f)
            if len(num) != 1:
                raise Exception("should only find single number here!")
            Num.append(num)
        maxNum = np.max(np.array(Num).astype(np.float))
    return maxNum


def getNameFile(
    fType=".hdf5", prefix="PW", maxkwrgs=99, replace=None, replace_with=None, **kwrgs
):
    if replace is None:
        replace = [".", "/"]
        replace_with = ["p", "-"]
    # supports kwrgs that are not iterable!
    dic = locals()["kwrgs"]
    name = "%s" % prefix
    for index, key in enumerate(dic.keys()):
        if index >= maxkwrgs:
            break
        value = aux.convert_params_to_string(
            dic[key], Round=2, replace=replace, replace_with=replace_with
        )
        key = aux.convert_params_to_string(
            key, Round=2, replace=["."], replace_with="-"
        ).upper()
        name += "_%s%s" % (key, value)
    name = aux.shortenFileName(name)
    return name


def compareAttributesFileWithDic(
    file, dictionary, verbose=0, acceptFalse=False, ignore=None
):
    if ignore is None:
        ignore = []
    else:
        ignore = ignore[:]
    verbose = 0
    # ignore attributes that are returned:
    for k in file.attrs.keys():
        if k.startswith("FIELDS_"):
            ignore.append(k)
    reduced_keys = reduceDictByKeysNotInDic(dictionary, ignore).keys()
    for keyword in reduced_keys:
        # check all keys of dictionary present in file
        if keyword not in file.attrs:
            if not aux.isIterable(dictionary[keyword]):
                if convertHdf5ValToValues(dictionary[keyword]) is None or (
                    acceptFalse and dictionary[keyword] == False
                ):
                    print(
                        "keyword: %s not in file.attrs! but set to %s, so acceptable!"
                        % (keyword, dictionary[keyword])
                    )
                    continue
            return False
        else:
            itemDic = dictionary[keyword]
            itemFile = convertHdf5ValToValues(file.attrs[keyword])
            # check multidim iterables
            if aux.isIterable(itemDic) and (type(itemDic) != str):
                if Fig.checkDimN(itemDic, 2) or Fig.checkDimN(itemFile, 2):
                    itemFile = itemFile.tolist()
                    if itemDic != itemFile:
                        return False
                elif not aux.isIterable(itemFile):
                    if verbose:
                        print(
                            "keyword:",
                            keyword,
                            ": itemFile",
                            itemFile,
                            "not iterable in contrast to itemDic",
                            itemDic,
                        )
                    return False
                if len(itemDic) != len(itemFile):
                    if verbose:
                        print(
                            f"keyword ({keyword}): len() ({itemDic}) {len(itemDic)}!={len(itemFile)} ({itemFile})"
                        )
                    return False
                elif not all(
                    [
                        x == y
                        if type(itemDic[0]) != str or type(y) != list
                        else x == y.decode("UTF-8")
                        for x, y in zip(itemDic, itemFile)
                    ]
                ):
                    if verbose:
                        print(
                            f"type(itemDic[0]):{type(itemDic[0])}, type(itemFile[0]):{type(itemFile[0])}"
                        )
                        print("keyword:", keyword, ": not all same:", itemDic, itemFile)
                    return False
            # check single item
            elif aux.isIterable(itemFile) and (type(itemFile) != str):
                if verbose:
                    print(
                        f"keyword ({keyword}): itemDic {itemDic} not iterable but itemFile {itemFile}"
                    )
                return False
            else:
                if itemDic != itemFile:
                    if verbose:
                        print("keyword:", keyword, ": ", itemDic, "!=", itemFile)
                    return False
    # check all keys of file present in dictionary
    for keyword in reduceDictByKeysNotInDic(file.attrs, ignore):  # .keys():
        if keyword not in dictionary.keys():
            if convertHdf5ValToValues(file.attrs[keyword]) is None:
                if verbose:
                    print(file.attrs[keyword])
                    print(
                        "keyword not in dictionary.keys(), but value is NONE:",
                        keyword,
                        "not in",
                        dictionary.keys(),
                    )
                return True
            else:
                if verbose:
                    print(file.attrs[keyword])
                    print(
                        "if keyword not in dictionary.keys():",
                        keyword,
                        "not in",
                        dictionary.keys(),
                    )
                return False
    return True


def loadFieldsFromFile(fname, dicLoadFields):
    import h5py

    start_time = TimeModule.time()
    # load fields from hdf5 file fname of all fields with value True in dicLoadFields
    FIELDS = {}
    with h5py.File(fname, "r") as f:
        if dicLoadFields == "all":
            dicLoadFields = list(f.keys())
        for key in dicLoadFields:
            if any(x.startswith("FIELDS_" + key) for x in f.attrs.keys()):
                for k in f.attrs.keys():
                    if k.startswith("FIELDS_" + key):
                        FIELDS[k[len("FIELDS_") :]] = convertHdf5ValToValues(f.attrs[k])
            elif convertHdf5ValToValues(f[key].shape):
                FIELDS[key] = convertHdf5ValToValues(f[key][:])
            else:
                FIELDS[key] = None
    print(f"loading of {fname} took {TimeModule.time()-start_time}s")
    return FIELDS


def saveFieldsToFile(fname, dicSaveFields, dicAttributes, dtypes=None, v=0):
    import h5py

    start_time = TimeModule.time()
    if dtypes is None:
        dtypes = ["f"] * len(dicSaveFields.keys())
    # save fields from hdf5 file fname of all fields present in dicSaveFields and attributes given in dicAttributes
    aux.create_dirs_for_file(fname)
    aux.checkExistence_delete_file(fname)
    try:
        with h5py.File(fname, "x") as f:  # create file, fail if exists!
            for keyDic in dicAttributes.keys():
                if v:
                    print(f"converting: {dicAttributes[keyDic]}, key: {keyDic}")
                try:
                    f.attrs[keyDic] = convertValuesToHdf5Val(dicAttributes[keyDic])
                except TypeError:
                    raise Exception(
                        f"cant be saved as hdf5 attribute: {convertValuesToHdf5Val(dicAttributes[keyDic])}"
                    )
                except:
                    raise Exception(
                        f"cant be saved as hdf5 attribute (no type error): {convertValuesToHdf5Val(dicAttributes[keyDic])}"
                    )
                # except:
                #     print(f"cant be saved as hdf5 attribute (no type error): {convertValuesToHdf5Val(dicAttributes[keyDic])}")
            for keyField, dt in zip(list(dicSaveFields.keys()), dtypes):
                if dt == "int":
                    if np.max(dicSaveFields[keyField]) > 2147483647:
                        dt = "int64"
                    else:
                        dt = "int32"
                if type(dicSaveFields[keyField]) == np.ndarray:
                    if np.max(abs(dicSaveFields[keyField])) > 1e38:
                        dt = "float64"
                f.create_dataset(keyField, np.shape(dicSaveFields[keyField]), dtype=dt)
                f[keyField][...] = convertValuesToHdf5Val(
                    dicSaveFields[keyField], forceFloat=True, fields=True,
                )
            if v > 0:
                print("-------------saving----------------------------")
                debug.visualizeH5pyAttr(f)
                print("--------------------------------------------------")
                debug.visualizeH5pyAttr(f)
    except Exception as e:
        print(f"couldnt save file:")
        for keyDic in dicAttributes.keys():
            print(
                f"couldnt save file: {keyDic}: {convertValuesToHdf5Val(dicAttributes[keyDic])}"
            )
        for keyField, dt in zip(list(dicSaveFields.keys()), dtypes):
            if dt == "int":
                if np.max(dicSaveFields[keyField]) > 2147483647:
                    dt = "int64"
                else:
                    dt = "int32"
            if type(dicSaveFields[keyField]) == np.ndarray:
                if np.max(abs(dicSaveFields[keyField])) > 1e38:
                    dt = "float64"
            print(
                f"{keyField}: {convertValuesToHdf5Val(dicSaveFields[keyField], forceFloat=True)}"
            )
            print(e)
    if v:
        print("... saved %s" % fname)
    print(f"saving took {TimeModule.time()-start_time}s")


def convertValuesToHdf5Val(value, forceFloat=False, fields=False):
    if not aux.isIterable(value) or type(value) == str:
    # if not isinstance(value, collections.Iterable) or type(value) == str:
        if value is None:
            if forceFloat:
                return 0.0
            return "None"
        if type(value) == str:
            if re.match(r"^\s+$", value) or len(value) == 0:
                return "emptystring"
        if value == "None":
            raise Exception("Do not use str None in rest of code!")
    try:
        if not fields and aux.isIterable(value) and None in value:
            value = [convertValuesToHdf5Val(x) for x in value]
            if all([type(x) == str for x in value]):
                value = np.string_(value)
    except Exception as e:
        print(f"value: {value}")
        print(f"aux.isIterable(value): {aux.isIterable(value)}")
        print(e)
    return value


def convertHdf5ValToValues(value):
    if type(value) == np.bytes_:
        value = value.decode("UTF-8")
    # if not isinstance(value, collections.Iterable) or type(value) == str:
    if not aux.isIterable(value) or type(value) == str:
        if value == "None":
            return None
        if value == "emptystring":
            return ""
    elif aux.isIterable(value) and "None" in value:
        value = [convertHdf5ValToValues(x) for x in value]
    return value


def reduceDictBySubStr(dic, phrase, match="phraseOnly", removePhrase=False):
    """
        return subset of dictionary based on reduction type
        removePhrase: removes identifying phrase from key
    """
    subDic = {}
    for key, value in dic.items():
        if phrase in key and match == "phraseOnly":
            if removePhrase:
                key = re.sub("^%s" % phrase, "", key)
            subDic[key] = value
    return subDic


def reduceDictByKeys(dic, keys):
    """
        return subset of dictionary if key found in dic
    """
    return {k: v for k, v in dic.items() if k in keys}


def reduceDictByKeysNotInDic(dic, keys, regex=False):
    """
        allows regex as well
        return subset of dictionary if key not found in dic
    """
    if regex:
        return {
            k: v
            for k, v in dic.items()
            if not any([bool(re.search(x, k)) for x in keys])
        }
    # print(f"len(keys): {len(keys)}, len(dic): {len(dic)}")
    return {k: v for k, v in dic.items() if not k in keys}


import inspect


def get_default_args(func):
    signature = inspect.signature(func)
    return {
        k: v.default
        for k, v in signature.parameters.items()
        if v.default is not inspect.Parameter.empty
    }


def decide_attribute_field_hdf5(attributes_dic, fields_dic, inplace=True):

    if not inplace:
        attributes_dic = attributes_dic.copy()
        fields_dic = fields_dic.copy()
    changed_fields_to_attributes = []

    for k, v in fields_dic.items():
        change = False
        if type(v) == dict:
            change = True
        if aux.isIterable(v):
            if all([type(x) == str for x in v]):
                change = True
        if type(v) == str:
            change = True
        if v is None:
            change = True
        if change:
            attributes_dic["FIELDS_" + k] = fields_dic[k]
            changed_fields_to_attributes.append(k)
    for k in changed_fields_to_attributes:
        fields_dic.pop(k)
    return attributes_dic, fields_dic
