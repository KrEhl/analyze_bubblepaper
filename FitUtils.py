import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize, interpolate
import scipy


def calculateAreaGaussian(amplitude, variance):
    return amplitude * variance * np.sqrt(2 * np.pi)


def gaussianDerivativeVariance(x, amplitude, mean, derivative):
    from sympy.solvers import solve
    from sympy import Symbol, exp

    sigma = Symbol("sigma")
    solutions = solve(
        derivative
        + amplitude
        * (x - mean)
        / sigma ** 2
        * exp(-((x - mean) ** 2) / (2 * sigma ** 2)),
        sigma,
    )
    if len(solutions) == 0:
        return None
    corSol = None
    for sol in solutions:
        deriv = gaussianDerivative(x, amplitude, mean, np.float128(sol))
        if np.abs(deriv - derivative) < 1e-6 * derivative:
            corSol = sol
    if corSol is None:
        print(
            "couldnt find satisfactory solution for gaussian: estimated deriv: %g, epected deriv: %g"
            % (deriv, derivative)
        )
    return corSol


def maskIsolatedPeak(peak, dataAll):
    print("masking isolated peak")
    # print('just one data point left, have to include data from all!')
    maskNonZero = np.array(peak) != 0
    nonZeroData = np.array(peak)[maskNonZero]
    where = np.where(maskNonZero)
    whereIndex = where[0][0]
    # print('have to include cell neighbours which are non-zero!')
    minIn = whereIndex
    for index in reversed(range(whereIndex)):
        if dataAll[index] != 0:
            minIn = index
            break
    maxIn = whereIndex
    for index in range(whereIndex + 1, len(peak)):
        if dataAll[index] != 0:
            maxIn = index
            break
    if minIn == whereIndex - 1 or maxIn == whereIndex + 1:
        maxIn = whereIndex + 1
        minIn = whereIndex - 1
    valMaxIn = dataAll[maxIn]
    valMinIn = dataAll[minIn]
    peakVal = peak[whereIndex]
    # print(valMinIn,valMaxIn,peakVal,'valMinIn,valMaxIn,peakVal')
    if valMinIn > peakVal and valMaxIn > peakVal:
        if valMaxIn > valMinIn:
            maxIn = whereIndex
        else:
            minIn = whereIndex
    elif valMaxIn > peakVal:
        maxIn = whereIndex
    elif valMinIn > peakVal:
        minIn = whereIndex
    peak = (
        [0] * np.size(peak[:minIn])
        + list(dataAll)[minIn : maxIn + 1]
        + [0] * np.size(peak[maxIn + 1 :])
    )
    if minIn == maxIn:
        print(
            "Exception(minIn and maxIn same when trying find peak!)",
            "minIn, maxIn",
            minIn,
            maxIn,
            "peak",
            peak,
            "dataAll",
            dataAll,
        )
        raise Exception("minIn and maxIn same when trying find peak!")
    return peak


def maskPeak(data, dataAll, ensureNeighbouringBin=False, thresh=0, verbose=False):
    import warnings

    np.array(data)[np.array(data) <= thresh] = 0
    maskNonZero = np.array(data) != 0
    nonZeroData = np.array(data)[maskNonZero]
    if np.size(np.unique(dataAll)) == 1:
        print("maskPeakExcepton", "data,dataAll", data, dataAll)
        raise Exception("cannot mask peak with only one point in dataset!")
    #     print(nonZeroData)
    if np.size(nonZeroData) == 1:
        # if data==dataAll:
        #     return None, None
        if not ensureNeighbouringBin:
            peak = maskIsolatedPeak(data, dataAll)
        else:
            return None, None
        where = np.where(maskNonZero)
        whereIndex = where[0][0]
        left = list(data)[:whereIndex] + [0] + list(data)[whereIndex + 1 :]
        return left, peak
    data = list(np.copy(data))
    gradient = np.diff(data + [0])
    #     print(gradient)
    maxV = np.max(data)
    if verbose:
        print("maxV", maxV)
    indexMaxV = np.where(np.array(data) == maxV)
    if np.size(indexMaxV) > 1:
        if verbose:
            print(indexMaxV)
            print("too many maxima!")
    indexMaxV = indexMaxV[0][0]
    # going to left
    threshGradient = 0
    cutLowIndex = 0
    if verbose:
        print(indexMaxV)
    for index in reversed(range(indexMaxV - 1)):
        if gradient[index] < threshGradient:
            cutLowIndex = index + 1
            break
    # going to right
    cutUpperIndex = len(data)
    for index in range(indexMaxV, len(data)):
        if gradient[index] > threshGradient:
            cutUpperIndex = index + 1
            break

    left = (
        data[:cutLowIndex] + [0] * (cutUpperIndex - cutLowIndex) + data[cutUpperIndex:]
    )
    peak = (
        [0] * np.size(data[:cutLowIndex])
        + data[cutLowIndex:cutUpperIndex]
        + [0] * np.size(data[cutUpperIndex:])
    )
    #     print('cutUpperIndex,cutLowIndex',cutUpperIndex,cutLowIndex)
    if (
        cutUpperIndex - cutLowIndex == 1
        or np.size(np.array(peak)[np.array(peak) != 0]) <= 1
        or not checkElementHigherThresh(peak, thresh, 2)
    ):
        if not ensureNeighbouringBin:
            peak = maskIsolatedPeak(peak, dataAll)
        else:
            return None, None
    return left, peak


def checkElementHigherThresh(elements, threshold, elementNumber):
    # print(elements, threshold, elementNumber)
    return bool(np.sort(elements)[-elementNumber] > threshold)


def gaussian(x, amplitude, mean, variance):
    return amplitude * np.exp(-((x - mean) ** 2) / (2 * variance ** 2))


def gaussianDerivative(x, amplitude, mean, variance):
    return (
        -amplitude
        * (x - mean)
        / (variance ** 2)
        * np.exp(-((x - mean) ** 2) / (2.0 * variance ** 2))
    )


def gaussianVariance(x, amplitude, mean, y):
    # print('x,amplitude,mean,y')
    # print(x,amplitude,mean,y)
    # print('x-mean,amplitude/y,np.log(amplitude/y)')
    # print(x-mean,amplitude/y,np.log(amplitude/y))
    div = amplitude / y
    if div == 1:
        return np.nan
    sigma2 = (x - mean) ** 2 / (2 * np.log(div))
    if sigma2 < 0:
        print("sigma2 smaller than 0")
        sigma = None
    else:
        sigma = np.sqrt(sigma2)
    return sigma


def fitSinglePeak(bins, data, dataAll, returnFitFunction=False):
    bins = np.copy(bins)
    data = np.copy(data)
    widthBins = np.diff(bins)[0]
    guessAmpl = np.max(data)
    guessMean = bins[data == np.max(data)][0]
    # estimate variance
    mask = data > 0
    binsNoZero = bins[mask]
    dataNoZero = data[mask]
    # binsDerivNoZero = binsNoZero[:-1]+np.diff(binsNoZero)/2.
    # dataDerivNoZero = np.diff(binsNoZero)/binsDerivNoZero
    # if 0 in binsDerivNoZero:
    #     print(binsDerivNoZero,bins,binsNoZero)
    guessVar = []
    guessVarDeriv = []
    for x, y in zip(binsNoZero, dataNoZero):
        guessVar.append(gaussianVariance(x, guessAmpl, guessMean, y))
        # print(x,y,guessVar)
    #     for x,y in zip(binsDerivNoZero,dataDerivNoZero):
    #         print(x,y,guessVarDeriv)
    #         guessVarDeriv.append(gaussianDerivativeVariance(x, guessAmpl, guessMean, y))
    #         print(x,y,guessVarDeriv)
    #     print(guessVar+guessVarDeriv)
    guessVariance = np.nanmean(guessVar + guessVarDeriv)
    guesses = [guessAmpl, guessMean, guessVariance]
    rangeData = np.max(binsNoZero) - np.min(binsNoZero)
    bounds = (
        np.array(
            [0.99 * guessAmpl, guessMean - rangeData * 0.5, -np.inf], dtype=np.float128
        ),
        np.array(
            [1.01 * guessAmpl, guessMean + rangeData * 0.5, np.inf], dtype=np.float128
        ),
    )
    if any([a >= b for a, b in zip(bounds[0], bounds[1])]):
        print("bounds", bounds)
        print("guesses", guesses)
        print("rangeData", rangeData)
        print("data", data)
        print("dataAll", dataAll)
        raise Exception("lower bounds higher than upper bounds!!!")
    # print('guesses',guesses)
    # print('data', data)
    # print('bins', bins)
    # print('bounds', bounds)
    # sigma = (data/np.max(data))**(-3)
    #     print('sigma',sigma)
    sigma = np.ones(np.size(data))
    sigma[data > 0] = 1e-2
    #     print('sigma',sigma)
    #     print(sigma,'sigma')
    paras, perror = scipy.optimize.curve_fit(
        gaussian,
        bins,
        dataAll,
        p0=guesses,
        sigma=sigma,
        bounds=bounds,
        maxfev=100000000,
    )
    perror = np.sqrt(np.diag(perror))
    if not returnFitFunction:
        return paras, perror
    else:
        return paras, perror, gaussian


# def plotfitMultiGaussians(Nit=10):
#     alpha=0.4
#     morebins = np.linspace(np.min(bins)-5*np.diff(bins)[0],np.max(bins)+5*np.diff(bins)[0],500)
#     thresh = 1e-2*np.max(n)

#     left, peak = maskPeak(n,n)
#     posFirstPeak = bins[peak==np.max(peak)]
#     plt.bar(bins,peak,width=np.diff(bins)[0])
#     plt.show()
#     for stuff in [None]:
#         for index in range(Nit):
#             paras = fitSinglePeak(bins,peak,n)
#             plt.plot(morebins,gaussian(morebins,*paras))
#             areaCovFrac = np.sum(np.diff(bins)[0]*np.array(peak))/calculateAreaGaussian(paras[0], paras[2])
#             plt.bar(bins,peak,width=np.diff(bins)[0],label=r'$%i,A=%g,\mu=%g,\sigma=%g,A_\mathrm{hist}/A_\mathrm{fit}=%g$'%(index,paras[0],paras[1],paras[2],areaCovFrac),alpha=alpha);
#             leftNonZero = np.array(left)[np.array(left)!=0]
#             if np.size(leftNonZero)==0 or np.max(leftNonZero) < thresh:
#                 print('found %i maxima' %(index+1))
#                 break
#             left, peak = maskPeak(left, n)
#         if index == Nit:
#             print('would have run longer than %i iterations' %Nit)

#         Ndata = np.size(data)
#         Mean = np.sum(data*weights)/np.sum(weights)
#         WP1 = np.sqrt(np.sum(weights*(data-Mean)**2)/((Ndata-1)*np.sum(weights)/Ndata)) #old WP2
#         areaCovFrac = np.sum(np.diff(bins)[0]*np.array(n))/calculateAreaGaussian(np.max(n), WP1)
#         plt.plot(morebins,gaussian(morebins,*[np.max(n),Mean,WP1]),label=r'$\mathrm{0-th}\ \mathrm{order}:,A=%g,\mu=%g,\sigma=%g,A_\mathrm{hist}/A_\mathrm{fit}=%g$' %(np.max(n),Mean,WP1,areaCovFrac))
#         # plt.ylim([YMin,YMax])
#         plt.legend(loc='center left', fancybox=True, bbox_to_anchor=(1,0.5))


def plotfitMultiGaussians(
    bins, data, toCalc="paras", Nit=10, threshFracMax=1e-2, verbose=False
):
    thresh = threshFracMax * np.max(data)
    # fit first peak
    FitParas = []
    FitErrors = []
    left = data
    for index in range(Nit):
        left, peak = maskPeak(
            left, data, ensureNeighbouringBin=index == 0, thresh=thresh
        )
        # if left is None and peak is None:
        #     return FitParas
        if left is None and peak is None:
            return None, None
        paras, perror = fitSinglePeak(bins, peak, data)
        FitParas.append(paras)
        FitErrors.append(perror)
        leftNonZero = np.array(left)[np.array(left) > thresh]
        if np.size(leftNonZero) <= 1 or np.max(leftNonZero) < thresh:
            if verbose:
                print("found %i maxima" % (index + 1))
            break
        # left, peak = maskPeak(left, data)
    if index == Nit - 1:
        if verbose:
            print("would have run longer than %i iterations" % Nit)
    if toCalc == "paras":
        return FitParas, FitErrors
    else:
        raise Exception("cannot calculate %s in plotfitMultiGaussians!" % toCalc)


if __name__ == "__main__":

    import Param as param
    import auxiliary_functions as aux
    import FigureMove as Fig

    x, y = np.loadtxt("DataExtraction/Vantyghem2014/DensityDeprj.txt", delimiter=",").T
    y *= np.sqrt(0.67 / 0.7)
    x *= 0.7 / 0.67
    log = True
    nbins = 70
    function_to_fit = (
        lambda r, n0, r0, a0, n1, r1, a1: n0 * (1 + (r / r0) ** 2) ** a0
        + n1 * (1 + (r / r1) ** 2) ** a1
    )
    guess = [0.05, 100, -4.9, 0.01, 400, -1.6]
    bounds = [
        [0.01, 0.1],
        [1, np.max(x)],
        [-10, 0],
        [0.0001, 0.01],
        [1, np.max(x)],
        [-10, 0],
    ]
    l = []
    u = []
    for b in bounds:
        l.append(b[0])
        u.append(b[1])
    bounds = [l, u]
    print(f"bounds {bounds}")
    para, perr = aux.fitFunction(
        x,
        y,
        guess,
        function_to_fit,
        method="curve_fit",
        bounds=bounds,
        # sigma=np.abs(err[1] - err[0]),
        maxfev=int(1e8),
    )
    y_fit = function_to_fit(x, *para)
    plt.figure()
    plt.xscale("log")
    plt.yscale("log")
    plt.plot(x, y_fit, label=f"fit {para}")
    nice_fit = function_to_fit(x, *[0.054, 53, -2.37, 0.00502, 490, -2.42])
    plt.plot(x, nice_fit, label=f"nice fit")
    old_fit = function_to_fit(x, *[0.05, 100, -4.9, 0.01, 400, -1.6])
    plt.plot(x, old_fit, label=f"old fit")
    plt.scatter(x, y, label="data")
    plt.legend()
    plt.show()
