import matplotlib as mpl

import debug
from SuperClasses import checkLoadNoMemory

# mpl.use('Agg')
mpl.rcParams["text.usetex"] = True
import collections
import FigureMove as Fig
import auxiliary_functions as aux
import numpy as np
import matplotlib.pyplot as plt
import time
import FileWorks as FW
from SuperClasses import SAVER
import wrapt
import Param as param


def checkRerunLoad(s=1):
    @wrapt.decorator
    def wrapper(wrapped, instance, args, kwargs):
        return wrapped(*args, **kwargs)

    return wrapper


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if "log_time" in kw:
            name = kw.get("log_name", method.__name__.upper())
            kw["log_time"][name] = int((te - ts) * 1000)
        else:
            # pass
            print("%r  %2.2f s" % (method.__name__, (te - ts)))
        return result

    return timed


def x1(n, n_pad, delta):
    return (-(n + n_pad) / 2 + 1) * delta


class FTObj:
    def __init__(
        self,
        spaceDim=3,
        fieldDim=1,
        n=128,
        delta=0.4 / 128,
        n_pad=0,
        realFT=False,
        ignoreTwoPi=True,
        numthreads=32,
        k_factor=1,
        v=0,
    ):  # , **Method_Variables):

        # super().__init__()

        # specs: check if plot already exists before plotting
        """
        delta (list): seperation in real space between grid points
        spaceDim (int, list): number of dimensions of space 3D=3, 2D=2, ...
        fieldDim (int, list): number of dimensions of field B_x=1, B=3, ...
        fieldType: 'Grid', 'RMmap', 'Projection'
        loadPlot: check if given parameters vary in file (do not check if file has additional parameters!)
        profileVariable: if given profile rescaled with given variable
        Method_Variable: state parameters in form 'Method_Variable'=value, e.g. Projection_resDepth=128
        INFO: add 'getFieldSnap_' to keyword for getFieldSnap()
        """

        self.spaceDim = spaceDim
        self.fieldDim = fieldDim
        self.n = n
        self.delta = delta
        self.n_pad = n_pad
        self.realFT = realFT
        self.ignoreTwoPi = ignoreTwoPi
        self.v = v
        self.numthreads = numthreads
        self.k_factor = k_factor
        self.field = None
        # --------------------------------------------------------------
        # need to include all parameters relevant for specific methods here!
        # super().addCheckParameters(
        #     [
        #         "_getPlot",
        #         "initializeKVectors",
        #         "initializeXVectors",
        #         "getFieldSnap",
        #         "getFieldTheo",
        #         "getFieldSnapPlot",
        #         "getTheoFieldPlot",
        #     ],
        #     True,
        #     spaceDim=self.spaceDim,
        #     fieldDim=self.fieldDim,
        #     n=self.n,
        #     delta=self.delta,
        #     n_pad=self.n_pad,
        #     realFT=self.realFT,
        #     ignoreTwoPi=self.ignoreTwoPi,
        # )
        return

    @property
    def n(self):
        return self.__n

    @n.setter
    def n(self, n):
        self.__n = aux.makeIterable(n, self.spaceDim, enforceType=int)

    @property
    def delta(self):
        return self.__delta

    @delta.setter
    def delta(self, delta):
        self.__delta = aux.makeIterable(delta, self.spaceDim, enforceType=float)

    @property
    def n_pad(self):
        return self.__n_pad

    @n_pad.setter
    def n_pad(self, n_pad):
        self.__n_pad = aux.makeIterable(n_pad, self.spaceDim, enforceType=int)

    @checkRerunLoad()
    def initializeKVectors(self):
        factor = self.k_factor
        if not self.ignoreTwoPi:
            factor = 2.0 * np.pi
        if not self.realFT:
            self.ks = np.array(
                np.meshgrid(
                    *(
                        np.fft.fftfreq(self.n[i] + self.n_pad[i], d=self.delta[i])
                        * factor
                        for i in range(self.spaceDim)
                    )
                )
            )
        else:
            self.ks = np.array(
                np.meshgrid(
                    *(
                        np.fft.rfftfreq(self.n[i] + self.n_pad[i], d=self.delta[i])
                        * factor
                        if i == 0
                        else np.fft.fftfreq(self.n[i] + self.n_pad[i], d=self.delta[i])
                        * factor
                        for i in range(self.spaceDim)
                    )
                )
            )
        self.kAbs = np.sqrt(np.sum(self.ks ** 2, axis=0))

    @checkRerunLoad()
    def initializeXVectors(self):
        def x1(n, n_pad, delta):
            return (-(n + n_pad) / 2 + 1) * delta

        self.xs = np.array(
            np.meshgrid(
                *(
                    np.arange(
                        x1(self.n[i], self.n_pad[i], self.delta[i]),
                        x1(self.n[i], self.n_pad[i], self.delta[i])
                        + (self.n[i] + self.n_pad[i]) * self.delta[i],
                        self.delta[i],
                    )
                    for i in range(self.spaceDim)
                )
            )
        )
        self.xAbs = np.sqrt(np.sum(self.xs ** 2, axis=0))

    def getTrafoFactor(self):
        if not self.realFT:
            factorGrid = np.meshgrid(
                *(np.arange(0, self.n[i] + self.n_pad[i]) for i in range(self.spaceDim))
            )
            self.factor = np.array(
                [
                    np.sqrt(self.n[i] / (2 * np.pi))
                    * self.delta[i]
                    * np.exp(
                        -2
                        * np.pi
                        * np.complex(0, 1)
                        * x1(self.n[i], self.n_pad[i], self.delta[i])
                        / (self.n[i] * self.delta[i])
                        * np.array(factorGrid[i], dtype=np.complex)
                    )
                    for i in range(self.spaceDim)
                ]
            )
            self.factor = np.prod(self.factor, axis=0)
        else:
            factorGrid = np.meshgrid(
                *(
                    np.arange(0, (self.n[i] + self.n_pad[i]) / 2 + 1)
                    if i == 0
                    else np.arange(0, self.n[i] + self.n_pad[i])
                    for i in range(self.spaceDim)
                )
            )
            self.factor = np.array(
                [
                    np.sqrt(self.n[i] / (2 * np.pi))
                    * self.delta[i]
                    * np.exp(
                        -2
                        * np.pi
                        * np.complex(0, 1)
                        * x1(self.n[i], self.n_pad[i], self.delta[i])
                        / (self.n[i] * self.delta[i])
                        * np.array(factorGrid[i], dtype=np.complex)
                    )
                    for i in range(self.spaceDim)
                ]
            )
            self.factor = np.prod(self.factor, axis=0)

    def doFFT(self, field, factor=1.0):
        if self.realFT:
            transform = np.fft.rfftn(field, norm="ortho") * factor
        else:
            transform = np.fft.fftn(field, norm="ortho") * factor
        return transform

    def doIFFT(self, field, factor=1.0):
        if self.realFT:
            transform = np.fft.irfftn(field, norm="ortho") * 1 / (factor)
        else:
            transform = np.fft.ifftn(field, norm="ortho") * 1 / (factor)
        return transform

    def kspace(self, ignoreFactor=True, inplace=True, absTrafo=False):
        if self.statusField == "kspace":
            if self.v > 1:
                print("already in kspace!")
            if absTrafo:
                self.rspace()
                self.field = np.abs(self.field)
                self.kspace(absTrafo=False)
            return self.field
        if absTrafo:
            self.field = np.abs(self.field)
        factor = 1.0
        if not ignoreFactor:
            self.getTrafoFactor()
            factor = self.factor
        transform = self.doFFT(self.field, factor=factor)
        if inplace:
            self.field = transform
            self.statusField = "kspace"
        return transform

    def rspace(self, ignoreFactor=True, inplace=True):
        if self.statusField == "rspace":
            if self.v > 1:
                print("already in rspace!")
            return self.field
        factor = 1.0
        if not ignoreFactor:
            self.getTrafoFactor()
            factor = self.factor
        transform = self.doIFFT(self.field, factor=factor)
        if inplace:
            self.field = transform
            self.statusField = "rspace"
        return transform

    @checkRerunLoad(
        # checkLoadFromFile=False,
        # # Ignore=["^snap$"],
        # methodSpecficVariables=True,
        # otherMethodsVariables=["_getPlot"],
    )
    def getFieldSnap(
        self,
        snap_header=None,
        snapName=None,
        variable="SqrtMagneticFieldEnergyDensity",
        initializeVariableInKspace=False,
        initializeProfileInKspace=True,
        fieldType="Grid",
        variableProfile=None,
        length_scale="kpc",
        ics=False,
        INFO={},
    ):
        self.variable = variable
        self.initializeVariableInKspace = initializeVariableInKspace
        self.initializeProfileInKspace = initializeProfileInKspace
        self.fieldType = fieldType
        self.variableProfile = variableProfile
        self.INFO = INFO
        self.initializeKVectors()
        debug.printDebug(self.ks, name="self.ks")
        self.initializeXVectors()
        if not self.initializeVariableInKspace:
            coords = self.xAbs
        else:
            coords = self.kAbs

        if snap_header is not None:
            file_name = snap_header.file_name
            print(f"ics: {ics}")
            self.snap = Fig.quickImport(file_name, IC=ics, differenceTime=8)
            self.ProjBox = [
                nn * dd / self.snap.UnitLength_in_cm * param.KiloParsec_in_cm
                for nn, dd in zip(self.n, self.delta)
            ]
            if len(self.ProjBox) and fieldType == "RMmap":
                raise Exception("broken too messy code... spacedim=3 doesnt work ")
            if self.snapName is None:
                self.snapName = aux.convert_params_to_string(
                    self.snap.file_name, string=True
                )
            if self.variable not in self.snap.data.keys():
                INFO = aux.reduceDicByKeyWords(
                    self.INFO, self.variable, removeKeyword=True
                )
                self.snap.data[self.variable] = aux.get_value(
                    self.variable, self.snap, INFO=INFO
                )
                if self.fieldDim is None:
                    self.fieldDim = np.shape(self.snap.data[self.variable])[0]
        else:
            self.field = []
            INFO = aux.reduceDicByKeyWords(self.INFO, self.variable, removeKeyword=True)
            for i in range(self.fieldDim):
                field = aux.get_valueTheoretical(self.variable, coords, **INFO)
                self.field.append(field)

        if self.variableProfile is not None:
            if not self.initializeProfileInKspace:
                coords = self.xAbs
            else:
                coords = self.kAbs
            self.profile = []
            for i in range(self.fieldDim):
                profile = aux.get_valueTheoretical(self.variableProfile, coords)
                self.profile.append(profile)
        if self.fieldType == "Grid" and self.snap is not None:
            INFO = aux.reduceDicByKeyWords(
                self.INFO, self.fieldType, removeKeyword=True
            )
            self.field = Fig.getAgrid(
                self.snap,
                self.variable,
                res=self.n,
                ProjBox=self.ProjBox,
                numthreads=self.numthreads,
                verbose=1,
                **INFO,
            )
            print("snap grid")
            debug.printDebug(self.field)
        elif self.snap is not None:
            if self.fieldType == "RMmap":
                INFO = aux.reduceDicByKeyWords(
                    self.INFO, self.fieldType, removeKeyword=True
                )
                self.field = Fig.do_RMmap(
                    self.snap,
                    self.variable,
                    ProjBox=self.ProjBox,
                    resolution=self.n[0],
                    numthreads=self.numthreads,
                    **INFO,
                )
            elif self.fieldType == "Projection":
                INFO = aux.reduceDicByKeyWords(
                    self.INFO, self.fieldType, removeKeyword=True
                )
                self.field = Fig.do_Projection(
                    self.snap,
                    self.variable,
                    res=self.n,
                    numthreads=self.numthreads,
                    inputGrids=False,
                    sumAlongAxis=True,
                    resCritical=1024,
                    verbose=self.v,
                    ProjBox=self.ProjBox,
                    **INFO,
                )
            else:
                raise Exception("fieldType %s not understood" % self.fieldType)
        if not self.initializeVariableInKspace:
            self.statusField = "rspace"
        else:
            self.statusField = "kspace"
        return self.field, snapName

    @checkRerunLoad(
        # checkLoadFromFile=False,
        # # Ignore=["^snap$"],
        # methodSpecficVariables=True,
        # otherMethodsVariables=["_getPlot"],
    )
    def getFieldTheo(
        self,
        snap=None,
        variable="MagneticFieldTurbulent1D",
        initializeVariableInKspace=False,
        initializeProfileInKspace=True,
        fieldType="Grid",
        variableProfile=None,
        combineProfileField=None,
        INFO={},
    ):
        self.initializeKVectors()
        self.initializeXVectors()
        if not self.initializeVariableInKspace:
            coords = self.xAbs
            self.statusField = "rspace"
        else:
            coords = self.kAbs
            self.statusField = "kspace"

        if snap is None:
            self.field = aux.get_valueTheoretical(variable, coords, INFO=INFO)
        else:
            self.field = aux.get_value(variable, snap, INFO=INFO)

        if self.variableProfile is not None:
            if not self.initializeProfileInKspace:
                coordsProf = self.xAbs
            else:
                coordsProf = self.kAbs
            self.profile = aux.get_valueTheoretical(self.variableProfile, coords)
            if self.field.shape != self.profile.shape:
                raise Exception("profile and field need to have same shape!")
            if self.initializeProfileInKspace != self.initializeVariableInKspace:
                if self.initializeProfileInKspace == "kspace":
                    self.kspace()
                else:
                    self.rspace()
            if combineProfileField == "add":
                self.field = self.profile + self.field
            else:
                raise Exception(
                    "combineProfileField %s not yet implemented!" % combineProfileField
                )

        if self.fieldType == "Grid":
            self.field = self.field.reshape(*self.n)
            debug.printDebug(self.field)

            # self.field.swapaxes(1,0).reshape(3,n,n,n)
        elif self.fieldType == "RMmap":
            raise Exception("doesnt work!")
            self.field = Fig.do_RMmap(
                self.snap, self.variableProfile, numthreads=self.numthreads, **INFO
            )
        elif self.fieldType == "Projection":
            self.field = Fig.do_Projection(
                None,
                self.variableProfile,
                numthreads=self.numthreads,
                inputGrids=False,
                sumAlongAxis=True,
                resCritical=1024,
                verbose=self.v,
                ProjBox=self.ProjBox,
            )
        else:
            raise Exception("fieldType %s not understood" % self.fieldType)

        return self.field

    def cleanDivergence(self):
        self.kspace()
        if self.ksNormed is None:
            kabsInvert = np.nan_to_num(1.0 / self.kAbs)
            self.ksNormed = self.ks * kabsInvert
        #         KdotB = np.sum(self.ksNormed*self.field, axis=0)
        self.field = self.field - self.ksNormed * np.sum(
            self.ksNormed * self.field, axis=0
        )
        return

    def computeDivergence(self):
        field = self.kspace(inplace=False)
        if self.ksNormed is None:
            kabsInvert = np.nan_to_num(1.0 / self.kAbs)
            self.ksNormed = self.ks * kabsInvert
        return np.sum(np.abs(np.sum(field * self.ksNormed, axis=0)) ** 2) / np.sum(
            np.abs(field) ** 2
        )

    def getSigmaToBe(self, NBins=401):

        from scipy import optimize
        import FitUtils as Fit

        field = np.array((self.rspace(inplace=False)).real).flatten()
        # cords = np.array(self.xAbs).flatten()
        y, x = np.histogram(
            field, bins=np.linspace(np.min(field), np.max(field), NBins)
        )
        x = x[0:-1] + np.diff(x) / 2.0
        if self.guessesSigma is None:
            mean = np.mean(field)
            self.guessesSigma = [
                np.max(y),
                mean,
                np.sqrt(np.sum((field - mean) ** 2) / np.size(field)),
            ]  # self.profileParameters
            if self.v > 1:
                print("getSigmaToBe: guesses:", self.guessesSigma)
        fitfunction = Fit.gaussian
        resFit = (np.max(field) - np.min(field)) / NBins
        res, fitParas, error = Fig.fitGaussians(
            field,
            np.ones(np.shape(field)),
            resFit=resFit,
            MaxResFit=resFit * 10,
            filename=self.folderBase + "SigmaToBe/Sigma",
            saveHistogram=False,
            percentFitMaxError=np.inf,
            maxSigma=np.inf,
        )
        fitParas = fitParas[0]
        error = error[0]
        # errfunc = lambda p, x, y: Fit.gaussian(x, *p) - y # Distance to the target function
        # FitParas, success = optimize.leastsq(errfunc, fitParas, args=(x,y))
        if self.v > 1:
            print("gaussian fit with res %g" % res)
            print(res, fitParas, error)
        # errfunc = lambda p, x, y: fitfunction(x, *p) - y # Distance to the target function
        # fitParas, success = optimize.leastsq(errfunc, guesses, args=(x,y))
        # paras, success = scipy.optimize.curve_fit(getFunctionY, bin_centresSqrtConsEpsB, statisticSqrtConsEpsB, p0=[5e-6, 50., 0.4], sigma=None,
        #                                   bounds=(np.array([1e-6,1,0]),np.array([1e-5,100,4.])),
        #                                  maxfev=10000000000)
        if True:
            fig, ax = Fig.getFigure(
                legendExternal=1, legendExtAxSize=[0.1, 0.14, 0.55, 0.76]
            )
            ax.plot(x, y, label="data")
            ax.plot(x, fitfunction(x, *fitParas), label="fit")
            text = (
                r"\begin{eqnarray*}"
                + r"A &=& %g\pm%g\\" % (fitParas[0], error[0])
                + r"\mu &=& %g\pm%g\\" % (fitParas[1], error[1])
                + r"\sigma &=& %g\pm%g\\" % (fitParas[2], error[2])
                + r"\end{eqnarray*}"
            )
            if self.v > 1:
                print(self.variable)
            title = "%s" % str(self.variable)
            ax.set_title(title)
            ax.text(
                0.5, 0.8, text, transform=ax.transAxes
            )  # , {'color': 'C2', 'fontsize': 18}, va="top", ha="right")
            Fig.modifyPlot(ax, xlog=0, ylog=0, xlabel=r"variable", ylabel=r"N")
            ax.legend(
                frameon=True,
                loc="center left",
                framealpha=0.8,
                fancybox=True,
                bbox_to_anchor=(1, 0.5),
            )
            saveFold = "../figures/FourierSimple/SigmaFit/"
            filename = saveFold + "Sigma%s%sN%i.pdf" % (
                self.simName,
                self.variable,
                NBins,
            )
            Fig.savePlot(fig, filename)
        return fitParas[2]

    def plotField(
        self,
        log=False,
        axis=[2, 0],
        vmin=None,
        vmax=None,
        labelsizeTicks=14,
        fontsize=20,
        addFile="",
        colormap="jet",
        simName=None,
    ):
        if self.spaceDim > 2:
            print(
                "can only plot fields with spatial dimension >= 2, spaceDim: ",
                self.spaceDim,
            )
            return
        self.rspace()
        data = self.field
        if self.v > 2:
            print("plotField: np.shape(self.field): ", np.shape(self.field))
        if self.spaceDim == 3:
            if len(np.shape(self.field)) == 5:
                data = self.field[0, 0, :, :, int(self.n[0] / 2)]
            elif len(np.shape(self.field)) == 4:
                data = self.field[0, :, :, int(self.n[0] / 2)]
            else:
                data = self.field[:, :, int(self.n[0] / 2)]
        if any([x == type(data[0, 0]) for x in [np.complex64, np.complex128]]):
            data = data.real
        (
            fig,
            ax,
        ) = (
            plt.subplots()
        )  # Fig.getFigure(quadraticFigure=True, nolegendExtAxSize=[0.14,0.14,0.72,0.72])
        # axCo = plt.axes([0.82,0.14,0.02,0.82])
        box = [self.n[0] * self.delta[0] for i in range(3)]
        ext = Fig.getExtension(box, [x / 2.0 for x in box], axis)
        vmin, vmax = Fig.getVminVmax(
            vmin,
            vmax,
            data,
            ChooseVmax=False,
            ChooseVmin=False,
            time=None,
            modelVmax=False,
            modelVmin=False,
            variable=None,
        )
        yy = np.linspace(ext[2], ext[3], np.shape(data)[0] + 1)
        xx = np.linspace(ext[0], ext[1], np.shape(data)[1] + 1)
        if log:
            import matplotlib.colors

            img = ax.pcolormesh(
                xx,
                yy,
                data,
                cmap=colormap,
                shading="flat",
                norm=matplotlib.colors.LogNorm(vmin=vmin, vmax=vmax),
            )
        else:
            img = ax.pcolormesh(
                xx, yy, data, cmap=colormap, shading="flat", vmin=vmin, vmax=vmax
            )
        ax.axes.tick_params(
            axis="y",
            colors="black",
            direction="in",
            length=0,
            labelleft=True,
            labelright=True,
            pad=-2.5 * labelsizeTicks,
            labelsize=labelsizeTicks,
        )
        ax.axes.tick_params(
            axis="x",
            colors="black",
            direction="in",
            length=0,
            labelbottom=True,
            labeltop=True,
            pad=-1.5 * labelsizeTicks,
            labelsize=labelsizeTicks,
        )
        ax = Fig.modifyPlot(ax, xlog=False, ylog=False, xlabel="", ylabel="")
        cbar = plt.colorbar(
            img, orientation="vertical"
        )  # , ticks = ticks)#,format='%.2g')
        cbar.ax.tick_params(labelsize=fontsize)
        cbar.ax.yaxis.get_offset_text().set(size=fontsize)
        saveFold = "../figures/FourierSimple/Field/"
        prefix = ""
        if simName is not None:
            prefix += "%s" % simName
        filename = saveFold + "Field%s%sTy%sProj%sRes%s%s.pdf" % (
            prefix,
            self.variable,
            self.fieldType,
            aux.convert_params_to_string(box, efficientNumber=False),
            aux.convert_params_to_string(self.n, efficientNumber=False),
            addFile,
        )
        Fig.savePlot(fig, filename, dpi=400)

    def plotFT(
        self,
        typ="snap",
        coordinate="kAbs",
        comp=0,
        absTrafo=False,
        plotVariableInKspace=True,
        absolute=True,
        sqrt=False,
        factor=None,
        binData=True,
        NBins=100,
        binLog=True,
        powerspec=True,
        scatter=False,
        scatterData=False,
        scatterShowN=1000,
        binTypeCalc="median",
        binSameNumberPoints=False,
        errbar="percentiles",
        fnamePlotDataSavedLoad=None,
        savePlotData=True,
        loadPlotData=True,
        savePlotDataFolder="figures/DataPlots/",
        loadPlotDataCheckFolders=[],
        saveloadPlotDataKeys=["x", "y", "err", "scatterDataX", "scatterDataY"],
        ax=None,
        errbarStyle="shaded",
        alphaError=0.2,
        color="black",
        nameMethod="plotFT",
        PowSpecFilter=None,
        xlog=True,
        ylog=True,
        xlabel=None,
        ylabel=None,
        Xrange=[None, None],
        Yrange=[None, None],
        savePlot=True,
        savePlotFolder="figures/",
        savePlotFilename="test.pdf",
        snap=None,
        snapName=None,
        variable="SqrtMagneticFieldEnergyDensity",
        initializeVariableInKspace=False,
        initializeProfileInKspace=True,
        fieldType="Grid",
        variableProfile=None,
        INFO={},
        showlegend=False,
        ics=False,
        **kwrgs,
    ):
        """
            snap: snap object or iterable with [folder, number, useTime]
            typ: snap, theory
        """
        if plotVariableInKspace:
            if xlabel is None:
                xlabel = r"$k\ [\mathrm{Mpc}^{-1}]$"
            if ylabel is None:
                ylabel = r"PS"
        else:
            if xlabel is None:
                xlabel = r"$x\ \mathrm{Mpc}$"
            if ylabel is None:
                ylabel = r"$\mathrm{%s}\ [\mathrm{cgs}]$" % variable
        if typ == "snap":
            # if snapName is None and hasattr(snap, "file_name"):
            #     snapName = aux.convert_params_to_string(snap.file_name, string=True)
            # if aux.isIterable(snap) and snapName is None:
            time, folder, useTime = snap
            snap_header = Fig.quickImport(
                time,
                folder=folder,
                useTime=useTime,
                onlyHeader=True,
                IC=ics,
                differenceTime=8,
            )
            self.snapName = aux.convert_params_to_string(
                snap_header.file_name, string=True
            )
            (
                self.x,
                self.y,
                self.err,
                self.scatterDataX,
                self.scatterDataY,
            ) = self.getFieldSnapPlot(
                snap_header=snap_header,
                snapName=snapName,
                variable=variable,
                initializeVariableInKspace=initializeVariableInKspace,
                initializeProfileInKspace=initializeProfileInKspace,
                fieldType=fieldType,
                variableProfile=variableProfile,
                INFO=INFO,
                coordinate=coordinate,
                comp=comp,
                absTrafo=absTrafo,
                plotVariableInKspace=plotVariableInKspace,
                absolute=absolute,
                sqrt=sqrt,
                factor=factor,
                binData=binData,
                NBins=NBins,
                binLog=binLog,
                powerspec=powerspec,
                scatter=scatter,
                scatterData=scatterData,
                scatterShowN=scatterShowN,
                nameMethod=nameMethod,
                PowSpecFilter=PowSpecFilter,
                binTypeCalc=binTypeCalc,
                binSameNumberPoints=binSameNumberPoints,
                errbar=errbar,
                fnamePlotDataSavedLoad=fnamePlotDataSavedLoad,
                savePlotData=savePlotData,
                loadPlotData=loadPlotData,
                savePlotDataFolder=savePlotDataFolder,
                loadPlotDataCheckFolders=loadPlotDataCheckFolders,
                saveloadPlotDataKeys=saveloadPlotDataKeys,
                ics=ics,
            )
        elif typ == "theory":
            snapName = "Theo"
            (
                self.x,
                self.y,
                self.err,
                self.scatterDataX,
                self.scatterDataY,
            ) = self.getTheoFieldPlot(
                snap=snap,
                variable=variable,
                initializeVariableInKspace=initializeVariableInKspace,
                initializeProfileInKspace=initializeProfileInKspace,
                fieldType=fieldType,
                variableProfile=variableProfile,
                INFO=INFO,
                snapName=snapName,
                coordinate=coordinate,
                comp=comp,
                absTrafo=absTrafo,
                plotVariableInKspace=plotVariableInKspace,
                absolute=absolute,
                sqrt=sqrt,
                factor=factor,
                binData=binData,
                NBins=NBins,
                binLog=binLog,
                powerspec=powerspec,
                scatter=scatter,
                scatterData=scatterData,
                scatterShowN=scatterShowN,
                nameMethod=nameMethod,
                PowSpecFilter=PowSpecFilter,
                binTypeCalc=binTypeCalc,
                binSameNumberPoints=binSameNumberPoints,
                errbar=errbar,
                fnamePlotDataSavedLoad=fnamePlotDataSavedLoad,
                savePlotData=savePlotData,
                loadPlotData=loadPlotData,
                savePlotDataFolder=savePlotDataFolder,
                loadPlotDataCheckFolders=loadPlotDataCheckFolders,
                saveloadPlotDataKeys=saveloadPlotDataKeys,
            )
        else:
            raise Exception("need to specify valid typ of data basis")
        self.doPlot(
            self.x,
            self.y,
            self.err,
            self.scatterDataX,
            self.scatterDataY,
            ax=ax,
            errbarStyle=errbarStyle,
            alphaError=alphaError,
            color=color,
            savePlot=savePlot,
            savePlotFolder=savePlotFolder,
            savePlotFilename=savePlotFilename,
            xlog=xlog,
            ylog=ylog,
            xlabel=xlabel,
            ylabel=ylabel,
            Xrange=Xrange,
            Yrange=Yrange,
            showlegend=showlegend,
            **kwrgs,
        )
        return self.plot

    # @checkRerunLoad(
    #     checkLoadFromFile=True,
    #     Ignore=[
    #         "fnamePlotDataSavedLoad",
    #         "savePlot",
    #         "savePlotFolder",
    #         "savePlotFilename",
    #         "savePlotData",
    #         "loadPlotData",
    #         "savePlotDataFolder",
    #         "loadPlotDataCheckFolders",
    #         "saveloadPlotDataKeys",
    #         "ax",
    #         # "^snap$",
    #         "INFODICT__variable_to_get",
    #     ],
    #     methodSpecficVariables=True,
    # )
    @checkLoadNoMemory(
        namesreturnvariables=["x", "y", "err", "scatterDataX", "scatterDataY"],
        loadPlotData=1,
        savePlotDataFolder="../figures/FT/data/",
        savePlotDataNameInclude=[
            "variable",
            "snapName",
            "fieldType",
            "self.n",
            "self.delta",
        ],
        Ignore=[
            "verbose",
            "saveas",
            "numthreads",
            "fnamePlotDataSavedLoad",
            "savePlot",
            "savePlotFolder",
            "savePlotFilename",
            "savePlotData",
            "loadPlotData",
            "savePlotDataFolder",
            "loadPlotDataCheckFolders",
            "saveloadPlotDataKeys",
            "ax",
            "snap",
            "snap_header",
        ],
        checkadditional=[
            "snap_header.rotation_angle",
            "snap_header.rotation_axis",
            "snap_header.rotation_quiver_angle",
            "snap_header.rotation_quiver_axis",
            "snap_header.file_name_abs",
            "self.spaceDim",
            "self.fieldDim",
            "self.n",
            "self.delta",
            "self.ignoreTwoPi",
            "self.n_pad",
            "self.realFT",
            "self.k_factor",
        ],
    )
    def getFieldSnapPlot(
        self,
        snap_header=None,
        snapName=None,
        variable="SqrtMagneticFieldEnergyDensity",
        initializeVariableInKspace=False,
        initializeProfileInKspace=True,
        fieldType="Grid",
        variableProfile=None,
        INFO={},
        coordinate="kAbs",
        comp=0,
        absTrafo=False,
        plotVariableInKspace=True,
        absolute=True,
        sqrt=False,
        factor=None,
        binData=True,
        NBins=100,
        binLog=True,
        powerspec=True,
        scatter=False,
        scatterData=False,
        scatterShowN=1000,
        binTypeCalc="median",
        binSameNumberPoints=False,
        errbar="percentiles",
        fnamePlotDataSavedLoad=None,
        nameMethod=None,
        PowSpecFilter=None,
        savePlotData=True,
        loadPlotData=True,
        savePlotDataFolder="../figures/DataPlots/",
        loadPlotDataCheckFolders=[],
        saveloadPlotDataKeys=["x", "y", "err", "scatterDataX", "scatterDataY"],
        ics=False,
    ):

        _, snapName = self.getFieldSnap(
            snap_header=snap_header,
            snapName=snapName,
            variable=variable,
            initializeVariableInKspace=initializeVariableInKspace,
            initializeProfileInKspace=initializeProfileInKspace,
            fieldType=fieldType,
            variableProfile=variableProfile,
            INFO=INFO,
            ics=ics,
        )
        print(f"snapName {snapName}")
        # ------------------------------------
        self.x, self.y, self.err, self.scatterDataX, self.scatterDataY = self._getPlot(
            coordinate=coordinate,
            comp=comp,
            absTrafo=absTrafo,
            plotVariableInKspace=plotVariableInKspace,
            absolute=absolute,
            sqrt=sqrt,
            factor=factor,
            binData=binData,
            NBins=NBins,
            binLog=binLog,
            powerspec=powerspec,
            scatter=scatter,
            scatterData=scatterData,
            scatterShowN=scatterShowN,
            binTypeCalc=binTypeCalc,
            binSameNumberPoints=binSameNumberPoints,
            errbar=errbar,
            fnamePlotDataSavedLoad=fnamePlotDataSavedLoad,
            nameMethod=nameMethod,
            snapName=snapName,
            PowSpecFilter=PowSpecFilter,
            savePlotData=savePlotData,
            loadPlotData=loadPlotData,
            savePlotDataFolder=savePlotDataFolder,
            loadPlotDataCheckFolders=loadPlotDataCheckFolders,
            saveloadPlotDataKeys=saveloadPlotDataKeys,
        )
        return self.x, self.y, self.err, self.scatterDataX, self.scatterDataY

    @checkRerunLoad(
        # checkLoadFromFile=True,
        # Ignore=[
        #     "fnamePlotDataSavedLoad",
        #     "savePlot",
        #     "savePlotFolder",
        #     "savePlotFilename",
        #     "savePlotData",
        #     "loadPlotData",
        #     "savePlotDataFolder",
        #     "loadPlotDataCheckFolders",
        #     "saveloadPlotDataKeys",
        #     "^ax$",
        #     # "^snap$",
        #     "INFODICT__variable_to_get",
        # ],
        # methodSpecficVariables=True,
    )
    def getTheoFieldPlot(
        self,
        snap=None,
        variable="MagneticFieldTurbulent1D",
        initializeVariableInKspace=False,
        initializeProfileInKspace=True,
        fieldType="Grid",
        variableProfile=None,
        combineProfileField=None,
        INFO={},
        snapName=None,
        coordinate="kAbs",
        comp=0,
        absTrafo=False,
        plotVariableInKspace=True,
        absolute=True,
        sqrt=False,
        factor=None,
        binData=True,
        NBins=100,
        binLog=True,
        powerspec=True,
        scatter=False,
        scatterData=False,
        scatterShowN=1000,
        binTypeCalc="median",
        binSameNumberPoints=False,
        errbar="percentiles",
        fnamePlotDataSavedLoad=None,
        nameMethod=None,
        PowSpecFilter=None,
        savePlotData=True,
        loadPlotData=True,
        savePlotDataFolder="../figures/DataPlots/",
        loadPlotDataCheckFolders=[],
        saveloadPlotDataKeys=["x", "y", "err", "scatterDataX", "scatterDataY"],
    ):

        self.getFieldTheo(
            snap=snap,
            variable=variable,
            initializeVariableInKspace=initializeVariableInKspace,
            initializeProfileInKspace=initializeProfileInKspace,
            fieldType=fieldType,
            variableProfile=variableProfile,
            combineProfileField=combineProfileField,
            INFO=INFO,
        )

        # ------------------------------------
        self.x, self.y, self.err, self.scatterDataX, self.scatterDataY = self._getPlot(
            coordinate=coordinate,
            comp=comp,
            absTrafo=absTrafo,
            plotVariableInKspace=plotVariableInKspace,
            absolute=absolute,
            sqrt=sqrt,
            factor=factor,
            binData=binData,
            NBins=NBins,
            binLog=binLog,
            powerspec=powerspec,
            scatter=scatter,
            scatterData=scatterData,
            scatterShowN=scatterShowN,
            binTypeCalc=binTypeCalc,
            binSameNumberPoints=binSameNumberPoints,
            errbar=errbar,
            fnamePlotDataSavedLoad=fnamePlotDataSavedLoad,
            nameMethod=nameMethod,
            snapName=snapName,
            savePlotData=savePlotData,
            loadPlotData=loadPlotData,
            savePlotDataFolder=savePlotDataFolder,
            loadPlotDataCheckFolders=loadPlotDataCheckFolders,
            saveloadPlotDataKeys=saveloadPlotDataKeys,
            PowSpecFilter=PowSpecFilter,
        )

        return self.x, self.y, self.err, self.scatterDataX, self.scatterDataY

    @timeit
    def getAValue(self):
        if self.v > 0:
            print("compute A value with sum of kspace field and nofp ", self.nofp)
        # taking kspace or rpsace doesnt really matter, just depends on normalization of implemented Fourier trans, default using 1/sqrt(N) for both so sums equivalent
        # print('getAValue: sum of get_PowerSpec: ', np.sum(np.abs(np.sqrt(get_PowerSpec(self.kAbs.flatten(), self.kL, self.kinj, self.kcrit, 1, self.a, self.b, self.c)))**2))
        # print('getAValue: sum of initializeField1D: ', np.sum(np.abs(self.initializeField1D(useAasUnity=True))**2))
        field = self.kspace(inplace=False)
        # print('getAValue: sum field: ', np.sum(np.abs(field)**2))
        # print('getAValue: A=sum_l |field_l|**2/sum_k|field_create,k|**2: ', np.sum(np.abs(field)**2)/np.sum(np.abs(self.initializeField1D(useAasUnity=True))**2))
        # self.A = np.sum(np.abs(field)**2)/self.nofp
        # normalize with powerspectrum where A=1
        self.A = np.sum(np.abs(field) ** 2) / np.sum(
            np.abs(
                np.sqrt(
                    aux.PowerSpec(
                        self.kAbs.flatten(),
                        kL=1 / np.sqrt(3),
                        kinj=40,
                        kcrit=80,
                        A=1.0,
                        a=0,
                        b=-11 / 3.0,
                        c=0,
                    )
                )
            )
            ** 2
        )
        if self.v > 0:
            print("choose A=sum(abs(field)**2)/self.nofp")
        rfield = self.rspace(inplace=False)
        # print('computed A: ', self.A)
        if self.v > 0:
            print(
                "getAValue: assuming mean=0, sigma: ",
                np.sqrt(np.sum(np.abs(field) ** 2) / self.nofp),
            )
        return self.A

    def doPlot(
        self,
        x,
        y,
        err,
        scatterDataX,
        scatterDataY,
        ax=None,
        errbarStyle="shaded",
        alphaError=0.2,
        color="black",
        savePlot=False,
        savePlotFolder="../figures/",
        savePlotFilename="test.pdf",
        secondXaxis=False,
        newPlot=False,
        xlog=False,
        ylog=False,
        xlabel="",
        ylabel="",
        Xrange=[None, None],
        Yrange=[None, None],
        showlegend=False,
        **kwargs,
    ):
        if ax is None or newPlot:
            fig, ax = plt.subplots()
            print("creating new axes")
        else:
            # fig = plt.gcf()
            print("using passed axes")
        if scatterDataX and scatterDataY:
            ax.scatter(scatterDataX, scatterDataY, **kwargs)
        if err is None:
            self.plot = ax.plot(x, y, color=color, **kwargs)
        elif errbarStyle == "shaded":
            self.plot = ax.fill_between(
                x, err[0], err[1], alpha=alphaError, color=color
            )
            self.plot = ax.plot(x, y, color=color, **kwargs)
        elif errbarStyle == "errorbars":
            self.plot = ax.errorbar(x, y, yerr=err, *args, color=color, **kwargs)
        else:
            raise Exception("errbarstyle %s not implemented yet" % errbarStyle)
        if scatterDataX and scatterDataY:
            self.plot = ax.scatter(x, y, color=color, **kwargs)

        if secondXaxis:
            ax2 = ax.twiny()
            numberMaxLogTicks = numberMaxLinTicks = len(ax.get_xticks()) * 2
            Xmin, Xmax = ax.get_xlim()
            if xlog and Xmin < 0:
                Xmin = np.min(x[x > 0])
            NewXmin = 1 / Xmax * 1e3
            NewXmax = 1 / Xmin * 1e3
            if xlog:
                ticks, minorTicks, NmajorTicks = Fig.setLogTicks(
                    NewXmin,
                    NewXmax,
                    numberMax=numberMaxLogTicks,
                    minormajorOverlap=True,
                )
                ax2.set_xscale("log")
            else:
                ticks = Fig.setLinTicksAlt(
                    NewXmin, NewXmax, numberMax=numberMaxLinTicks
                )

            def tickConvert(X):
                return 1 / (X / 1e3)

            ax2.set_xlim([Xmin, Xmax])
            ax2.set_xticks(tickConvert(ticks))
            ax2.set_xticklabels(["%g" % x for x in ticks])
            ax2.set_xlabel(r"$r\ [\mathrm{kpc}]$")
        if showlegend:
            ax.legend()
        Xrange, Yrange = Fig.makesureLimitsShowPlot(
            ax, x, y, xlog=xlog, ylog=ylog, Xrange=Xrange, Yrange=Yrange
        )
        Fig.modifyPlot(
            ax,
            xlog=xlog,
            ylog=ylog,
            xlabel=xlabel,
            ylabel=ylabel,
            Xrange=Xrange,
            Yrange=Yrange,
        )

        if savePlot:
            filename = savePlotFolder + savePlotFilename
            Fig.savePlot(fig, filename, dpi=400)

    @checkRerunLoad(
        # checkLoadFromFile=False,
        # Ignore=[
        #     "fnamePlotDataSavedLoad",
        #     "savePlot",
        #     "savePlotFolder",
        #     "savePlotFilename",
        #     "savePlotData",
        #     "loadPlotData",
        #     "savePlotDataFolder",
        #     "loadPlotDataCheckFolders",
        #     "saveloadPlotDataKeys",
        #     "^ax$",
        # ],
        # methodSpecficVariables=True,
    )
    def _getPlot(
        self,
        coordinate="kAbs",
        comp=0,
        absTrafo=False,
        plotVariableInKspace=True,
        absolute=True,
        sqrt=False,
        factor=None,
        ax=None,
        binData=True,
        NBins=100,
        binLog=True,
        powerspec=True,
        scatter=False,
        scatterData=False,
        scatterShowN=1000,
        binTypeCalc="median",
        binSameNumberPoints=False,
        errbar="percentiles",
        fnamePlotDataSavedLoad=None,
        PowSpecFilter=None,
        nameMethod=None,
        snapName=None,
        savePlotData=True,
        loadPlotData=True,
        savePlotDataFolder="figures/DataPlots/",
        loadPlotDataCheckFolders=[],
        saveloadPlotDataKeys=["x", "y", "err", "scatterDataX", "scatterDataY"],
    ):
        scatterDataX, scatterDataY, err = None, None, None

        if PowSpecFilter == "MexicanTopHatFilter":
            binData = False
            powerspec = False

        # manipulate field--------------------
        print("abstrafo", absTrafo)
        if plotVariableInKspace:
            y = np.copy(self.kspace(inplace=False, absTrafo=absTrafo))
        else:
            y = np.copy(self.rspace(inplace=False))

        if self.fieldDim > 1:
            y = y[comp]

        if PowSpecFilter == "MexicanTopHatFilter":
            x, y = self.getPSWithAppliedFilter(Npicks=NBins, logPicks=binLog)
            y = y * x ** 2
            print("x.shape, y.shape")
            print(x.shape, y.shape)
        else:
            y = np.array(y).flatten()
            x = np.array(self.__dict__[coordinate]).flatten()

        # ------------------------------
        if powerspec:
            if not absolute:
                raise Exception("need to have absolute when plotting powerspec!")
        # ------------------
        if PowSpecFilter is None:
            if powerspec:
                y = y ** 2
        elif PowSpecFilter == "MexicanTopHatFilter":
            pass
        else:
            raise Exception("Filter %s not implemented yet" % PowSpecFilter)

        if absolute:
            x = np.abs(x)
            y = np.abs(y)
        if factor is not None:
            y = y * factor
        if sqrt:
            y = np.sqrt(y)

        # save scatteredDataseperateley
        if scatterData:
            sizeData = np.size(x)
            Nskip = max([int(sizeData / scatterShowN), 1])
            scatterDataX = x[::Nskip]
            if powerspec:
                scatterDataY = y[::Nskip] ** 2 * x[::Nskip] ** 2
            else:
                scatterDataY = y[::Nskip]

        if binData:
            if self.v > 1:
                print("bin data: shapes x,y", np.shape(x), np.shape(y))
            x, y, err = Fig.do_Radprof(
                x,
                y,
                weight=None,
                range=[None, None],
                log=binLog,
                NBins=NBins,
                typeCalc=binTypeCalc,
                errbar=errbar,
                return_binedges=False,
                SameNumberPointsPerBin=binSameNumberPoints,
                return_errbar=True,
                v=self.v,
                info=self.INFO,
            )
        if powerspec:
            y = y * x ** 2
            if errbar is not None:
                print("err!!!!!!!!!!")
                err = [i * x ** 2 for i in err]
        sort = np.argsort(x)
        x = x[sort]
        y = y[sort]
        if self.v > 0:
            print("x", x)
            print("y", y)

        if savePlotData:
            if nameMethod is None:
                raise Exception("need to specify nameMethod to save Plot!")
            if self.snapName is None:
                raise Exception("need to specify snapName, i.e. use snap.file_name!")
            self.fnamePlotSavedLoad = FW.getNameFile(
                fType="",
                prefix="PW",
                Sim=snapName,
                Var=self.variable,
                FielTy=self.fieldType,
                PS=powerspec,
                TyCal=binTypeCalc,
                NBi=NBins,
                n=self.n,
                delta=self.delta,
            )
            fileNumber = (
                FW.getFileMaxNum(
                    Fig.findAllFilesWithEnding(savePlotDataFolder, ".hdf5")
                )
                + 1
            )
            # fnamePlotSavedLoad =
            # self.fnamePlotSavedLoad = "%s%s_%i.hdf5" % (
            #     savePlotDataFolder,
            #     fnamePlotSavedLoad,
            #     fileNumber,
            # )
            # self.dicLoadSaveFields = FW.createDictionary(
            #     x=x, y=y, err=err, scatterDataX=scatterDataX, scatterDataY=scatterDataY
            # )
            # FW.saveFieldsToFile(
            #     fnamePlotSavedLoad,
            #     dicLoadSaveFields,
            #     getDicForComparingWFile("_getPlot")[1],
            # )
        return x, y, err, scatterDataX, scatterDataY

    def getPSWithAppliedFilter(self, Npicks, logPicks):
        _, picks = Fig.get_Bins(
            Data=self.kAbs,
            Nbins=Npicks,
            log=logPicks,
            returnCenters=True,
            minNonZero=True,
        )
        epsilon = aux.checkIfInDictionary("tophatepsilon", self.INFO, 1e-3)
        # x, y = aux.applyTopHatFilter(np.copy(self.rspace(inplace=False).real), self.kAbs, np.prod(self.delta), self.spaceDim, epsilon)
        x, y = aux.applyTopHatFilter(
            np.copy(self.rspace(inplace=False).real),
            picks,
            np.prod(self.delta),
            self.spaceDim,
            epsilon,
        )
        return x.flatten(), y.flatten()
