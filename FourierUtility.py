import numpy as np
import scipy.stats

import auxiliary_functions as aux
import FigureMove as Fig
import debug


def remove_scales(
    grid, proj_box, scales_to_remove, real_trafo=True, space_dim=None, n=None
):
    save_plot_folder = "../figures/IsolateFeatures/"
    assert all(
        len(x) == 2 for x in scales_to_remove
    ), "scales_to_remove couples of min max boundary in kspace"
    if space_dim is None:
        space_dim = len(np.shape(grid))
    if n is None:
        n = grid.shape
    if not aux.isIterable(proj_box):
        proj_box = [proj_box for x in range(space_dim)]
    delta = [x / y for x, y in zip(proj_box, n)]
    grid = get_fourier_transform(grid, real_trafo)
    ks = get_fourier_frequencies(delta, n, real_trafo, space_dim)
    debug.printDebug(grid, "about to save PS grid")
    debug.printDebug(ks, "ks freq")
    ks = np.sqrt(np.sum(ks ** 2, axis=0))
    save_powerspectrum(
        ks.flatten(), np.abs(grid).flatten(), filename=save_plot_folder + "FT.pdf"
    )
    debug.printDebug(grid, "grid")
    debug.printDebug(ks, "ks abs")
    for s in scales_to_remove:
        mask = np.logical_and(ks > s[0], ks < s[1])
        grid[mask] = 0
    save_powerspectrum(
        ks.flatten(),
        np.abs(grid).flatten(),
        filename=save_plot_folder + "FTScalesRemoved.pdf",
    )
    grid = get_fourier_inverse_transform(grid, real_trafo)
    return grid


def save_powerspectrum(
    ks, grid, log=True, filename="../figures/IsolateFeatures/FT.pdf"
):
    bins = Fig.get_Bins(ks, log=log, Nbins=100, minNonZero=True)
    print(f"PS: shape ks {ks.shape}")
    print(f"ks: {ks}")
    print(f"ks: {grid}")
    print(f"bins: {bins}")
    print(f"PS: shape grid {grid.shape}")
    statistics, bin_edges, binnumber = scipy.stats.binned_statistic_dd(
        ks, grid, "mean", bins=[bins]
    )
    bin_edges = bin_edges[0]
    print(statistics)
    bin_centers = bin_edges[:-1] + np.diff(bin_edges) / 2
    print(f"bin_centers {bin_centers}")
    Fig.save_1PlotPlain(
        [bin_centers],
        [statistics * bin_centers ** 2],
        name=filename,
        logX=log,
        alpha=0.6,
    )


def get_fourier_transform(grid, real_trafo):
    if real_trafo:
        grid = np.fft.rfftn(grid, norm="ortho")
    else:
        grid = np.fft.fftn(grid, norm="ortho")
    return grid


def get_fourier_inverse_transform(grid, real_trafo):
    if real_trafo:
        grid = np.fft.irfftn(grid, norm="ortho")
    else:
        grid = np.fft.ifftn(grid, norm="ortho")
    return grid


def get_fourier_frequencies(delta, n, real_trafo, space_dim):
    if real_trafo:
        ks = np.array(
            np.meshgrid(
                *(
                    np.fft.rfftfreq(n[i], d=delta[i])
                    if i == space_dim - 1
                    else np.fft.fftfreq(n[i], d=delta[i])
                    for i in range(space_dim)
                ),
                indexing="ij",
            )
        )
    else:
        ks = np.array(
            np.meshgrid(
                *(np.fft.fftfreq(n[i], d=delta[i]) for i in range(space_dim)),
                indexing="ij",
            )
        )
    return ks


def get_realspace_coordinates(n, delta, space_dim):
    def x1(n, n_pad, delta):
        return (-(n + n_pad) / 2 + 1) * delta

    xs = np.array(
        np.meshgrid(
            *(
                np.arange(
                    x1(n[i], 0, delta[i]),
                    x1(n[i], 0, delta[i]) + (n[i] + 0) * delta[i],
                    delta[i],
                )
                for i in range(space_dim)
            )
        )
    )
    return xs
