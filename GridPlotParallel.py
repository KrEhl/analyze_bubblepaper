import matplotlib as mpl
import re
import multiprocessing as mp
import Param
import auxiliary_functions as aux
import FigureMove as Fig
import numpy as np
import string_manipulation as sm
import matplotlib.pyplot as plt
import os.path
import FileWorks as FW
import Multithread as mt
import time
import datetime
import argparse
import argparse_helpers
from matplotlib.ticker import (
    AutoMinorLocator,
    LogLocator,
)
import AxesGeneration
import gridplot_setups
from gridplot_setups import (
    get_grid_variable,
    RetrivingValuesFromStrings,
    _box_k_smoothing,
    get_reduction,
)
from statsmodels.stats.weightstats import DescrStatsW
import collections
from copy import copy

MAXSEARCHRADIUS = 50


marked_setups = collections.OrderedDict()


def _convert_plot_type_index(index_x, index_y):
    index_string = f"{index_x + 100 * index_y}"
    if index_string not in marked_setups:
        marked_setups[index_string] = len(marked_setups)
    return marked_setups[index_string]


def _get_index(string):
    return int(re.search("(\d)+", string)[0])


def convertInfoDicToParameters(numbers, folder, INFO):
    filename = INFO["filename"]
    functionParameters = INFO["functionParameters"]
    # if "snap" in functionParameters:
    filename = folder + "AnalysisData/" + filename + ".txt"
    # else:
    #     filename = "Data/" + filename + ".txt"
    # header
    header = INFO["header"]
    # relevantElements
    relevantElements = INFO["relevantElements"]
    snap = None
    relevantElements = [
        RetrivingValuesFromStrings(x, snap, INFO, numbers) for x in relevantElements
    ]  # relevantElementsNumbers
    relevantElementsNumbers = np.arange(len(relevantElements)).tolist()
    # function
    function = INFO["function"]
    # functionParameters
    functionParameters = INFO["functionParameters"]
    return (
        filename,
        header,
        relevantElementsNumbers,
        relevantElements,
        function,
        functionParameters,
    )


def checkIfAllData(
    js,
    table,
    arrayToCheckIndex=0,
    newColumns=["dataExists", "Values", "NotFoundArrayValues"],
    removeSaveTextFile=False,
):
    def retrieve(numbers, folder, info, arrayToCheckIndex):
        numbers, folder, INFO = table[numbers], table[folder], table[info]
        print(numbers)
        (
            filename,
            header,
            relevantElementsNumbers,
            relevantElements,
            function,
            functionParameters,
        ) = convertInfoDicToParameters(numbers, folder, INFO)
        return FW.checkRetrieveValueCSVFAST(
            filename,
            header,
            dtypesFloat=float,
            relevantElementsNumbers=relevantElementsNumbers,
            relevantElements=relevantElements,
            removeFile=removeSaveTextFile,
            arrayToCheckIndex=arrayToCheckIndex,
        )

    temp = list(
        zip(
            *js.df.apply(
                lambda x: retrieve(
                    x["numbers"], x["folder"], x["info"], arrayToCheckIndex
                ),
                axis=1,
            )
        )
    )
    # unzip new columns
    temp = [[js.df.job.convertToHash(x, table) for x in y] for y in temp]
    for i, c in enumerate(newColumns):
        js.df[c] = temp[i]
    # hash new columns
    # js.df.job.hashByColumn(newColumns, table=table)
    return js


def combine_Datas(d_x, d_y, dlog_x, dlog_y, i, j, logfirst=True):
    if len(d_x) == 0:
        return dlog_x, dlog_y
    if len(dlog_x) == 0:
        return d_x, d_y
    length_d = len(d_x.keys())
    length_d_log = len(dlog_x.keys())
    if not logfirst:
        for key in list(dlog_x.keys()).copy():
            dlog_x["%ix" % (_get_index(key) + length_d)] = dlog_x.pop(key)
        for key in list(dlog_y.keys()).copy():
            dlog_y["%iy" % (_get_index(key) + length_d)] = dlog_y.pop(key)
        for dic in dlog_x.values():
            dic["IndexPlotType"] += i
        for dic in dlog_y.values():
            dic["IndexPlotType"] += i
    else:
        d_x_copy = d_x.copy()
        d_x = {}
        for key in d_x_copy.keys():
            d_x["%ix" % (_get_index(key) + length_d_log)] = d_x_copy[key]
        d_y_copy = d_y.copy()
        d_y = {}
        for key in d_y_copy.keys():
            d_y["%iy" % (_get_index(key) + length_d_log)] = d_y_copy[key]
        for dic in d_x.copy().values():
            if "IndexPlotType" in dic.copy():
                dic["IndexPlotType"] += j
            if "IndexPlot" in dic.copy():
                dic["IndexPlot"] += j
        for dic in d_y.copy().values():
            if "IndexPlotType" in dic:
                dic["IndexPlotType"] += j
            if "IndexPlot" in dic.copy():
                dic["IndexPlot"] += j
    return {**d_x, **dlog_x}, {**d_y, **dlog_y}


def combineData(
    Times,
    Folders,
    INFOArrayX,
    SetupNamX,
    variableLabelX,
    INFOArrayY,
    SetupNamY,
    variableLabelY,
):
    """
		first X then Y plots! in combined list
	"""
    plotTag = []
    for index, dic in enumerate(INFOArrayX):
        plotTag.append("x")
    for index, dic in enumerate(INFOArrayY):
        plotTag.append("y")
    INFOList = INFOArrayX.tolist() + INFOArrayY.tolist()
    SetupNames = SetupNamX + SetupNamY
    VariableLabels = variableLabelX + variableLabelY

    Data = []
    for indexFolderTimes, (TimeNumber, folder) in enumerate(zip(Times, Folders)):
        numbers, times = TimeNumber
        print(f"numbers: {numbers}")
        print(f"times: {times}")
        print(f"times.tolist(): {times.tolist()}")
        print(f"numbers.tolist(): {numbers.tolist()}")
        for indexSetups, (info, setups, labels, tag) in enumerate(
            zip(INFOList, SetupNames, VariableLabels, plotTag)
        ):
            index = indexFolderTimes * int(len(plotTag) / 2) + indexSetups % int(
                len(plotTag) / 2
            )
            tag = f"{index}{tag}"
            Data.append(
                [numbers.tolist(), times.tolist(), folder, info, setups, labels, tag]
            )
            index += 1
    names = ["numbers", "times", "folder", "info", "setups", "lables", "tag"]
    return names, Data


def calculateData(df, table):
    # load snap first then do all operations on this snap
    for index, row in df.iterrows():
        number, folder, INFO = (
            table[row["NotFoundArrayValues"]],
            table[row["folder"]],
            table[row["info"]],
        )
        (
            filename,
            header,
            relevantElementsNumbers,
            relevantElements,
            function,
            functionParameters,
        ) = convertInfoDicToParameters(number, folder, INFO)
        onlyHeader = False
        FieldsToLoad = aux.checkExistenceKey(INFO, "FieldsToLoad", "basic")
        if number is None and aux.checkExistenceKey(
            INFO, "MaxAngleRotatedSnapshot", False
        ):
            number = Fig.get_NumberWRTAngle(INFO, up=True)
        if aux.checkExistenceKey(
            INFO, "individualFolder", False
        ) and aux.checkExistenceKey(INFO, "individualNumber", False):
            folder = aux.checkExistenceKey(INFO, "individualFolder", False)
            number = aux.checkExistenceKey(INFO, "individualNumber", False)
        snap = Fig.readSnap(
            folder,
            number,
            FieldsToLoad=FieldsToLoad,
            rewriteTimes=False,
            useTime=False,
            v=0,
        )
        if number is None:
            IC = True
        else:
            IC = False
        if FieldsToLoad == "ONLYHEADER":
            onlyHeader = True
        snap, time = Fig.getGeneralInformation(
            snap,
            convertedkpc=False,
            considering_h=False,
            StringTime=True,
            IC=IC,
            Precision=5,
            latexTime=True,
            INFO=INFO,
            onlyHeader=onlyHeader,
        )
        if index == 0:
            break
    for index, row in df.iterrows():
        number, folder, INFO = (
            table[row["NotFoundArrayValues"]],
            table[row["folder"]],
            table[row["info"]],
        )
        (
            filename,
            header,
            relevantElementsNumbers,
            relevantElements,
            function,
            functionParameters,
        ) = convertInfoDicToParameters(number, folder, INFO)
        relevantElements = [
            RetrivingValuesFromStrings(x, snap, INFO, number) for x in relevantElements
        ]
        functionParameters = [
            RetrivingValuesFromStrings(x, snap, INFO, number)
            for x in functionParameters
        ]
        FW.addValueCSVFAST(
            filename,
            header,
            function,
            functionParameters,
            dtypesFloat=float,
            relevantElementsNumbers=relevantElementsNumbers,
            relevantElements=relevantElements,
        )
    return True


def convertData(js, table):
    dataX = {}
    dataY = {}
    folderList = []
    plotTypeList = []
    folderList_x = []
    plotTypeList_x = []
    for index, row in js.df.iterrows():
        returnLabels = [
            "folder",
            "times",
            "Values",
            "NotFoundArrayValues",
            "lables",
            "tag",
        ]
        hashValues = [row[x] for x in returnLabels]
        values = [table[x] for x in hashValues]
        if "x" in values[-1]:
            data = dataX
            # 000
            if values[0] not in folderList_x:
                folderList_x.append(values[0])
            if row["info"] not in plotTypeList_x:
                plotTypeList_x.append(row["info"])
            indexFolder_x = folderList_x.index(values[0])
            indexPlotType_x = plotTypeList_x.index(row["info"])
            # 000
        else:
            data = dataY
            if values[0] not in folderList:
                folderList.append(values[0])
            if row["info"] not in plotTypeList:
                plotTypeList.append(row["info"])
            indexFolder = folderList.index(values[0])
            indexPlotType = plotTypeList.index(row["info"])
        data[values[-1]] = {k: v for k, v in zip(returnLabels[:-1], values[:-1])}
        if "x" not in values[-1]:
            data[values[-1]]["IndexFolder"] = indexFolder
            data[values[-1]]["IndexPlotType"] = indexPlotType
            data[values[-1]]["IndexPlot"] = int(values[-1][0])
        else:
            data[values[-1]]["IndexFolder"] = indexFolder_x
            data[values[-1]]["IndexPlotType"] = indexPlotType_x
            data[values[-1]]["IndexPlot"] = int(values[-1][0])
    # exit()
    return dataX, dataY


def getDataLogFile(
    Folders,
    SetupNamXLog,
    SetupNamYLog,
    variableLabel_logX,
    variableLabel_logY,
    info_x,
    info_y,
):
    print(f"Folders: {Folders}")
    data_x = {}
    data_y = {}
    for x_y, setup_name_array, variable_label_array, info_array in zip(
        ["x", "y"],
        [SetupNamXLog, SetupNamYLog],
        [variableLabel_logX, variableLabel_logY],
        [info_x, info_y],
    ):
        if x_y == "x":
            dic = data_x
        else:
            dic = data_y
        index_plot = 0
        for index_setup, (setup_name, variable_label, info) in enumerate(
            zip(setup_name_array, variable_label_array, info_array)
        ):
            for index_folder, fold in enumerate(Folders):
                sub_dic = {}
                sub_dic["folder"] = fold
                sub_dic["IndexFolder"] = index_folder
                sub_dic["IndexPlotType"] = index_setup
                print(f"sub_dic['IndexPlotType': {index_setup}")
                sub_dic["lables"] = variable_label
                function_parameters = [
                    RetrivingValuesFromStrings(x, None, info, None, folder=fold)
                    for x in info["functionParameters"]
                ]
                sub_dic["Values"] = info["function"](*function_parameters)
                if x_y == "x":
                    sub_dic["times"] = aux.get_blackholes_logfile(
                        fold, ["time"], False, info
                    )[0]
                    print(f"sub_dic[times]: {sub_dic['times']}")
                dic[f"{index_plot}{x_y}"] = sub_dic
                index_plot += 1
    return data_x, data_y


def getData(
    Times,
    Folders,
    INFOArrayX,
    SetupNamX,
    variableLabelX,
    INFOArrayY,
    SetupNamY,
    variableLabelY,
    numthreads=4,
    removeSaveTextFile=False,
):
    """
		obtains data for plot
		Folders/Times zipped, e.g.,
		Folders = ['x', 'y', 'z']
		Times = [[1,2,3],[3,4,12,12],[4,2]]]

		1) check what plots necessary to be computed:
	"""
    names, Data = combineData(
        Times,
        Folders,
        INFOArrayX,
        SetupNamX,
        variableLabelX,
        INFOArrayY,
        SetupNamY,
        variableLabelY,
    )
    js = mt.JobsSheduler(Data, names)
    table = js.hashAll()
    js = checkIfAllData(
        js, table, arrayToCheckIndex=0, removeSaveTextFile=removeSaveTextFile
    )
    # unhash relevant columns for job sheduler sort
    relevantSorter = ["folder", "NotFoundArrayValues"]
    js.getJobs(
        sortBy=relevantSorter,
        expandByDtypesAfter=[float],
        expandBy=["NotFoundArrayValues"],
        sortCheckUniqueness=["info"],
        table=table,
    )
    allIndices = np.array(
        [
            js.jobs["JobIndex"].values == x
            for x in np.unique(np.unique(js.jobs["JobIndex"]))
        ]
    )
    global bd
    bd = mt.BigData(calculateData, allIndices, [js.jobs], 1, None, "pandas", (table))
    results = bd.do_multi(numthreads)
    js = checkIfAllData(js, table, arrayToCheckIndex=0)
    dataX, dataY = convertData(js, table)
    return dataX, dataY


TickStyle = ["x", "o", "v", "^", "h", "+", "h", "D", ".", ",", "<", ">", "1", "2", "3"]
color = [
    "#bb596d",
    "#2565ca",
    "#199967",
    "#E4A41A",
    "#ff3232",
    "#201104",
    "#664200",
    "#40e0d0",
    "#0e2850",
    "#54784c",
    # "#0b3f00",
    # "#031200",
    "#5B3475",
    # "#e13e20",
    "#d2548c",
    "#49ff66",
    "#cfc0a1",
]  #'black',
color_start = [
    "#b6bfc8",
    "#e57f7f",
    "#99eaea",
    # "#a0a9ab",
    # "#d2c9ff",
    "#82b983",
    "#f5e9e5",
    "#8e768a",
    "#4cdbdb",
    "#fbf3b0",
    "#daae8a",
    "#a4b4d4",
    "#cfdbc3",
    "#d691d3",
    "#75ffcf",
    "#916373",
    "#bab496",
    "#ffc284",
    "#cff767",
    "#72c0d5",
]
color_finish = [
    # "#091d32",
    # "#510000",
    # "#003d3d",
    # "#273134",
    # "#4c4766",
    # "#264d27",
    "#a5938e",
    # "#2f1e2c",
    "#005151",
    "#ada244",
    "#5b2f0b",
    "#253555",
    "#58614e",
    "#571154",
    "#0c7f57",
    "#f6c0d2",
    "#6d684a",
    "#b27d47",
    "#699101",
    "#0e6982",
]
COLORS = []
for s, f in zip(color_start, color_finish):
    COLORS += [s]
    COLORS += [f]
print(COLORS)
Xmin = 0
Xmax = 2000
Ymin = None
Ymax = None
default_folder = "_M15mhdcrsLL"
Test = False
if os.path.exists("/home/kristian/Analysis/") or os.path.exists(
    "/home/kristian/analysis/"
):
    print("using test!")
    Test = True

if Test:
    default_folder = "_testt"

start_time = time.time()
parser = argparse.ArgumentParser(
    description="grid plots of different variables!",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    f"--folder", type=argparse_helpers.fancy_eval, nargs="+", default=[default_folder]
)
parser.add_argument(
    f"--setup",
    type=argparse_helpers.fancy_eval,
    nargs="+",
    default=[
        "wang",
        "mdot",
        "cooling",
        "coldgas",
        "massgrowth",
        "radius",
        "gasphase",
        "jetenergynob",
        "jetenergy",
    ],
)
parser.add_argument(
    f"--xmin", type=argparse_helpers.fancy_eval, nargs=1, default=[Xmin]
)
parser.add_argument(
    f"--xmax", type=argparse_helpers.fancy_eval, nargs=1, default=[Xmax]
)
parser.add_argument(
    f"--logx", type=argparse_helpers.fancy_eval, nargs=1, default=[None]
)
parser.add_argument(
    f"--logy", type=argparse_helpers.fancy_eval, nargs=1, default=[None]
)
parser.add_argument(
    f"--ymin", type=argparse_helpers.fancy_eval, nargs=1, default=[Ymin]
)
parser.add_argument(
    f"--ymax", type=argparse_helpers.fancy_eval, nargs=1, default=[Ymax]
)
parser.add_argument(
    f"--time_values_min", type=argparse_helpers.fancy_eval, nargs=1, default=[None]
)
parser.add_argument(
    f"--time_values_max", type=argparse_helpers.fancy_eval, nargs=1, default=[None]
)
parser.add_argument(
    f"--box_smoothing", type=argparse_helpers.fancy_eval, nargs="+", default=[False]
)
parser.add_argument(
    f"--box_statistics", type=argparse_helpers.fancy_eval, nargs="+", default=["mean"]
)
parser.add_argument(
    f"--hist_bin_number", type=argparse_helpers.fancy_eval, nargs="+", default=40
)
parser.add_argument(
    f"--threads", type=argparse_helpers.fancy_eval, nargs="+", default=32
)
parser.add_argument(f"--ignore_histogram_weights", action="store_true", default=False)
parser.add_argument(
    f"--vertical", type=argparse_helpers.fancy_eval, nargs="+", default=[None]
)
parser.add_argument(
    f"--grouping_colors", type=argparse_helpers.fancy_eval, nargs="+", default=[]
)
parser.add_argument(
    f"--grouping_alphas", type=argparse_helpers.fancy_eval, nargs="+", default=[]
)
parser.add_argument(
    f"--skip_colors", type=argparse_helpers.fancy_eval, nargs="+", default=[]
)
parser.add_argument(
    f"--skip_folders", type=argparse_helpers.fancy_eval, nargs="+", default=[]
)
parser.add_argument(
    f"--invert_finish_start", type=argparse_helpers.fancy_eval, nargs=1, default=False
)
parser.add_argument(
    f"--grouping_linestyle", type=argparse_helpers.fancy_eval, nargs="+", default=[]
)
parser.add_argument(
    f"--show_reductions_x_min", type=argparse_helpers.fancy_eval, nargs="+", default=[]
)
parser.add_argument(
    f"--show_reductions_x_max", type=argparse_helpers.fancy_eval, nargs="+", default=[]
)
parser.add_argument(
    f"--alpha_min", type=argparse_helpers.fancy_eval, nargs=1, default=[0.3]
)
parser.add_argument(f"--alpha_inverted", action="store_true", default=False)
parser.add_argument(
    f"--alpha_folder", type=argparse_helpers.fancy_eval, nargs="+", default=False
)
parser.add_argument(f"--set_yticks_both", action="store_true", default=False)
parser.add_argument(f"--set_ytick_label_both", action="store_true", default=False)
parser.add_argument(f"--split_setups_in_panels", action="store_false", default=True)
parser.add_argument(
    f"--split_by_folder", type=argparse_helpers.fancy_eval, nargs=1, default=[None]
)
parser.add_argument(f"--single_xlabel", action="store_true", default=False)
parser.add_argument(f"--single_ylabel", action="store_true", default=False)
parser.add_argument(f"--minimalist", action="store_true", default=False)
parser.add_argument(f"--folder_color_no_tickstyle", action="store_false", default=True)
parser.add_argument(f"--fontsize", type=argparse_helpers.fancy_eval, default=[None])
parser.add_argument(f"--wspace", type=argparse_helpers.fancy_eval, default=[None])
parser.add_argument(f"--space_below", type=argparse_helpers.fancy_eval, default=[None])
parser.add_argument(f"--align_text", type=argparse_helpers.fancy_eval, default=[None])
parser.add_argument(f"--paper", action="store_true", default=False)
parser.add_argument(
    f"--legend_folder_position", type=argparse_helpers.fancy_eval, default=[None]
)

input = parser.parse_args().__dict__.copy()
input = {k: v for k, v in input.items() if v is not None}
import script_helpers

script_helpers.get_parameters_from_yaml(input)
# exit()
print(input)
FOLDER = [input["folder"]]
ROOT = [input["root"]]
NAMEOFSIMSAVE = [input["name_sim_save"]]
NAMEOFSIM = [input["name_sim"]]
if (
    len(FOLDER) != len(ROOT)
    or len(FOLDER) != len(NAMEOFSIMSAVE)
    or len(FOLDER) != len(NAMEOFSIM)
):
    print(f"FOLDER: {FOLDER}")
    print(f"ROOT: {ROOT}")
    print(f"NAMEOFSIMSAVE: {NAMEOFSIMSAVE}")
    print(f"NAMEOFSIM: {NAMEOFSIM}")
    raise Exception("folder numbers not consistent!")
positionsVertical = input["vertical"][0]
xmin_initial = input["xmin"][0]
xmax_initial = input["xmax"][0]
Ymin = input["ymin"][0]
Ymax = input["ymax"][0]
logX = input["logx"][0]
logY = input["logy"][0]
numthreads = input["threads"][0]
time_values_min = input["time_values_min"][0]
time_values_max = input["time_values_max"][0]
show_reductions_x_min = input["show_reductions_x_min"]
if not show_reductions_x_min:
    show_reductions_x_min = None
show_reductions_x_max = input["show_reductions_x_max"]
if not show_reductions_x_max:
    show_reductions_x_max = None
box_smoothing = input["box_smoothing"]
box_statistics = input["box_statistics"][0]
ignore_histogram_weights = input["ignore_histogram_weights"][0]
hist_bin_number = input["hist_bin_number"][0]
setup = input["setup"]
alpha_folder = (
    input["alpha_folder"][0]
    if len(input["alpha_folder"]) == 1
    else input["alpha_folder"]
)
alpha_min = input["alpha_min"][0]
alpha_inverted = input["alpha_inverted"][0]
split_setups_in_panels = input["split_setups_in_panels"][0]
single_xlabel = input["single_xlabel"][0]
single_ylabel = input["single_ylabel"][0]
minimalist = input["minimalist"][0]
split_by_folder = input["split_by_folder"][0]
folder_color_no_tickstyle = input["folder_color_no_tickstyle"][0]
fontsize = input["fontsize"][0]
wspace = input["wspace"][0]
space_below = input["space_below"][0]
legend_folder_position = input["legend_folder_position"][0]
if legend_folder_position is None:
    legend_folder_position = "best"
paper = input["paper"][0]
align_text = input["align_text"][0]
set_yticks_both = input["set_yticks_both"][0]
set_ytick_label_both = input["set_ytick_label_both"][0]
set_yticks_both_rightmost_plot = False
if paper:
    set_yticks_both = True
    # align_text = "\\eta"
# if split_by_folder is not None:
#     print(f"split_by_folder: setting single_xlabel -> False")
#     single_xlabel = False
print(f"alpha_folder: {alpha_folder}")
print(f"alpha_min: {alpha_min}")
print(f"split_setups_in_panels: {split_setups_in_panels}")
print(f"single_xlabel: {single_xlabel}")
print(f"split_by_folder: {split_by_folder}")
ymax_reset = False
single_y_tick_label = False
single_x_tick_label = False
if minimalist:
    single_y_tick_label = True
    single_x_tick_label = True
    set_yticks_both = False
    set_yticks_both_rightmost_plot = True
logX_reset = False
if logX is None:
    logX_reset = True
logY_reset = False
if logX is None:
    logY_reset = True
if Ymax is None:
    ymax_reset = True
ymin_reset = False
if Ymin is None:
    ymin_reset = True
xmax_reset = False
if xmax_initial is None:
    xmax_reset = True
xmin_reset = False
if xmin_initial is None:
    xmin_reset = True
single_xlabel_reset = False
if single_xlabel:
    single_xlabel_reset = True
box_smoothing_reset = False
if box_smoothing != [False]:
    box_smoothing_reset = True
invert_finish_start = input["invert_finish_start"][0]
skip_colors = input["skip_colors"]
skip_folders = input["skip_folders"]
print(skip_folders)
if skip_colors is None:
    skip_colors = []
grouping_colors = input["grouping_colors"]
grouping_colors_reset = False
if not grouping_colors:
    grouping_colors = None
    grouping_colors_reset = True
grouping_alphas = input["grouping_alphas"]
grouping_alphas_reset = False
if not grouping_alphas:
    grouping_alphas = None
    grouping_alphas_reset = True
grouping_linestyle = input["grouping_linestyle"]
grouping_linestyle_reset = False
if not grouping_linestyle:
    grouping_linestyle = None
    grouping_linestyle_reset = True
include_outliers_in_histogram = True
legend_folder_axes = None

def do_box_smoothing(
    x,
    y,
    box_smoothing,
    IndexPlotType,
    statistics_x="mean",
    statistics_y="mean",
    use_x_as_weights=False,
):
    if box_smoothing != [False]:
        if box_smoothing[IndexPlotType]:
            print(
                f"IndexPlotType: {IndexPlotType}, box_smoothing[IndexPlotType]: {box_smoothing[IndexPlotType]}, statistics_x: {statistics_x}, statistics_y: {statistics_y}"
            )
            # exit()
            if use_x_as_weights:
                y = _box_k_smoothing(
                    box_smoothing[IndexPlotType],
                    y,
                    time_values,
                    statistics=statistics_y,
                    weights=x,
                )
                x = np.ones_like(y)
            else:
                x = _box_k_smoothing(
                    box_smoothing[IndexPlotType],
                    x,
                    time_values,
                    statistics=statistics_x,
                )
                y = _box_k_smoothing(
                    box_smoothing[IndexPlotType],
                    y,
                    time_values,
                    statistics=statistics_y,
                )
    return x, y


for Folders, Roots, NameOfSimSave, NameOfSim in zip(
    FOLDER, ROOT, NAMEOFSIMSAVE, NAMEOFSIM
):
    Folders = [x + y for x, y in zip(Roots, Folders)]

    FoldersTranslate = {}
    for f, name in zip(Folders, NameOfSim):
        FoldersTranslate[f] = name
    normalize_histogram = None
    horizontalalignment = "center"
    useShadingErrors = True
    alphaFill = 0.2
    legend_variable_position = "best"
    LegendLoc = "best"
    offsetX = 0
    framealpha = 0.4
    alpha = 0.8
    legend_external = "variable"
    useTimeIndexAsColor = False
    useFolderIndexAsColor = False
    useTimeIndexAsTickStyle = False
    useFolderIndexAsTickStyle = True
    useTimeIndexAsAlpha = False
    useFolderIndexAsAlpha = True
    scaleMarkerSizeTime = False
    plotMarkerAndLine = False
    LinFitEachTime = False
    plotLineEqual = False
    plotLegendFolder = True
    plotVertical = False
    plotVerticalShading = False
    plotText = False
    alphaVertical = alphaVerticalShading = 0.6
    alphaHorizontal = 0.6
    colorsHorizontal = colorText = colorsVertical = colorsVerticalShading = "black"
    xignoreLinTicks = True
    yignoreLinTicks = True
    minorXTicks = True
    minorYTicks = True
    spacingMinorYTicks = spacingMinorXTicks = 10
    reduce_minor_x_ticks = False
    reduce_minor_y_ticks = False
    legendFolderNcol = 1
    xticks = None
    numberMaxLinXTicks = 7
    numberMaxLinYTicks = 7
    use_own_x_ticks = False
    useTime = True
    MinDiffTimeInMyr = None
    showOnOtherYAxis = []

    setup1 = False
    JetDistance = False
    JetEnergy = False
    JetDistTrac = False
    JetDistComp = False

    MassBHStarsGas = False
    Massgrowth = True
    ColdGasWithinRadius = False
    accretionRadius = False
    CoolingPowers = False
    plotting_style = "default"

    useErrorbars = True
    Times = [20, 60, 180]
    returnType = "all"  # skipN1_until100
    Timesetup = returnType  # [50,180]#'all' #[50,180,280]#[50,120,450]#,80]#,80]#10,30,80,180,450]

    removeSaveTextFile = False
    if removeSaveTextFile:
        import warnings

        warnings.warn(f"deleting text files! -> use for debugging!")
    if numthreads > mp.cpu_count():
        numthreads = mp.cpu_count() - 1
        print(f"numthreads: {numthreads}")
    for s in setup:
        plot_x_line_value = None
        positionsVerticalShading = None
        box_smoothing_use_x_as_weights = True
        histogram_include_outlier_bins = False
        histogram_plot_reduction = False
        positionsHorizontal = []
        theory_plot_x = None
        theory_plot_y = None
        if logX_reset:
            logX = False
        if logY_reset:
            logY = True
        show_reductions = None
        if grouping_colors_reset:
            grouping_colors = None
        if grouping_linestyle_reset:
            grouping_linestyle = None
        if grouping_alphas_reset:
            grouping_alphas = None
        if ymin_reset:
            Ymin = None
        if ymax_reset:
            Ymax = None
        if xmin_reset:
            Xmin = None
        else:
            Xmin = xmin_initial
        if xmax_reset:
            Xmax = None
        else:
            Xmax = xmax_initial
        TickStyle = [
            "-",
            "--",
            "-.",
            "D",
            "x",
            ",",
            ".",
            "X",
            "|",
            "P",
            "h",
            "v",
            "^",
            "+",
        ]
        color = COLORS[:]
        linestyle = None
        useFolderIndexAsTickStyle = False
        force_show_folder = False
        plotting_order = "default"
        if single_xlabel_reset:
            single_xlabel = True
        plotLineEqual = False
        if len(Folders) > 1:
            useFolderIndexAsColor = False
            if alpha_folder:
                useFolderIndexAsTickStyle = True
                useFolderIndexAsAlpha = True
                useFolderIndexAsColor = False
                if type(alpha_folder) != list:
                    if not alpha_inverted:
                        alpha = np.linspace(alpha_min, 1, len(Folders))[::-1]
                    else:
                        alpha = np.linspace(alpha_min, 1, len(Folders))
                else:
                    alpha = alpha_folder
        if folder_color_no_tickstyle:
            useFolderIndexAsColor = True
            useFolderIndexAsTickStyle = False
            # useFolderIndexAsAlpha = False
            TickStyle = [
                "-",
            ]
        if split_setups_in_panels:
            useFolderIndexAsColor = True
        if s == "setup1":
            useTimeIndexAsColor = False
            useFolderIndexAsColor = True
            useTimeIndexAsTickStyle = False
            useFolderIndexAsTickStyle = True
            useFolderIndexAsAlpha = True
            FigNameMain = "DistBowShock"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = r"$d\ [\mathrm{kpc}]$"
            setX = [0]  # [0,0,0]
            setY = [1]  # [1,2,3]
            offsetX = 0.5
            Xmin = 0
            Xmax = 3000  # 460
            Ymin = 0
            Ymax = 100  # 140
            plotVerticalShading = False
            alphaVerticalShading = 0.3
            positionsVerticalShading = [0, 150]
            plotText = True
            colorsVerticalShading = colorText = "#065535"  #'#00405c'#'#005980'
            TickStyle = ["-", "--"]
            positionsText = [85, 60]
            Texts = ["jet is active"]
            legend_variable_position = "upper left"
        elif s == "jetdistcollect":
            useTimeIndexAsColor = False
            useFolderIndexAsColor = True
            useTimeIndexAsTickStyle = False
            useFolderIndexAsTickStyle = True
            useFolderIndexAsAlpha = True
            alphaMin = 0.3
            alphaMax = 0.55
            alpha = [alphaMax, alphaMin, alphaMax, alphaMin, alphaMax, alphaMin]
            # color = ['#bb596d','#760E23','#2565ca','#06285c','#5ee3af','#199967']
            # color = ['#760E23','#913e4e','#06285c','#50688c','#0F8557','#6fb59a']
            color = ["#760E23", "#760E23", "#06285c", "#06285c", "#0F8557", "#0F8557"]
            FigNameMain = "DistJetComp"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = r"$d\ [\mathrm{kpc}]$"
            setX = [0]  # ,0]#[0,0,0]
            setY = [1]  # ,2]#[1,2,3]
            offsetX = 0.5
            Xmin = 0  # 0
            Xmax = 150  # 280#460
            Ymin = 0  # 0
            Ymax = 160  # 620#140
            TickStyle = ["-", "--", "-", "--", "-", "--"]
            plotVerticalShading = True
            alphaVerticalShading = 0.3
            positionsVerticalShading = [0, 25]
            plotText = True
            colorsVerticalShading = "#6b6b6b"  #'#065535'#'#00405c'#'#005980'
            colorText = "#2d2d2d"
            positionsText = [12.5, 110]
            Texts = ["jet is \n active"]
            legend_variable_position = "upper left"
            horizontalalignment = "center"
            MinDiffTimeInMyr = 5
            legendFolderNcol = 3
            framealpha = 0.6
            useShadingErrors = True

        elif s == "jetdisttracer":
            FigNameMain = "DistJetTrac"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = r"$d\ [\mathrm{kpc}]$"
            setX = [0]  # [0,0,0]
            setY = [1]  # [1,2,3]
            offsetX = 0.5
            Xmin = None  # 0
            # Xmax = None  # 280#460
            Ymin = None  # 0
            Ymax = None  # 620#140
            TickStyle = "-"
            plotVerticalShading = True
            alphaVerticalShading = 0.3
            positionsVerticalShading = [0, 25]
            plotText = True
            colorsVerticalShading = colorText = "#065535"  #'#00405c'#'#005980'
            positionsText = [20, 60]
            Texts = ["jet is active"]
            legend_variable_position = "upper left"

        elif s == "jetdist":
            FigNameMain = "DistJet"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = r"$d\ [\mathrm{kpc}]$"
            setX = [0, 0, 0]
            setY = [1, 2, 3]
            offsetX = 0.5
            # Xmax = None  # 280#460
            Ymin = 0
            logY = False
            Ymax = None  # 620#140
        elif s == "mcoldvssfr":
            FigNameMain = "mcoldvssfr"
            Xlabel = r"$\mathrm{SFR}\ [\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$"
            Ylabel = [
                r"${M}_\mathrm{cold}\ [\mathrm{M}_\odot]$",
                r"$L_\mathrm{jet}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${L}_\mathrm{ICM}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            TickStyle = "x"
            setX = [8, 8, 8]
            setY = [6.54, 9.1, 9.01]
            offsetX = 0.5
            Ymin = [2e6, 1e43, 8e44]
            Ymax = [1e11, 1e46, 2e46]
            Xmin = 2e-2
            Xmax = 2e2
            logY = True
            logX = True
        elif s == "massgrowth":
            FigNameMain = "Massgrowth"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = r"$M_\mathrm{cum}\ [M_\odot]$"
            setX = [0, 0, 0]  # [0,0,0]
            setY = [6, 7.3, 6.61]  # [1,2,3]
            setX = [0] * len(setY)
            offsetX = 0.5
            logY = True
        elif s == "jetenergy":
            FigNameMain = "JetEnergy"
            Xlabel = r"$t\,[\mathrm{Myr}]$"
            Ylabel = r"$E\,[\mathrm{erg}]$"
            setY = [
                4.1,
                4.2,
                4.11,
                4.4,
                4.401,
                4.21,
                4.5,
                4.51,
                4.41,
                4.61,
                # 4.62,
                4.101,
            ] + [4.1111]
            setX = (len(setY) - 1) * [0] + [0.001]
            offsetX = 0.5
            logY = True
            single_xlabel = False
        elif s == "jetenergynob":
            FigNameMain = "JetEnergy"
            Xlabel = r"$t\,[\mathrm{Myr}]$"
            Ylabel = r"$E\,[\mathrm{erg}]$"
            setY = [4.1, 4.4, 4.11, 4.401, 4.5, 4.51, 4.41, 4.61, 4.62, 4.101,] + [
                4.1111
            ]
            setX = (len(setY) - 1) * [0] + [0.001]
            offsetX = 0.5
            logY = True
            single_xlabel = False
        elif s == "jetkinvsth":
            FigNameMain = "jetkinvsth"
            Ylabel = r"$E_\mathrm{jet,th}\,[\mathrm{erg}]$"
            Xlabel = r"$E_\mathrm{jet,kin}\,[\mathrm{erg}]$"
            setY = [4.4]
            setX = [4.1]
            minorXTicks = minorYTicks = False
            # color = aux.flatten([[x] * 2 for x in color])
            offsetX = 0.5
            xignoreLinTicks = yignoreLinTicks = True
            Ymin = None
            Ymax = None  # 620#140
            logY = False
            # TickStyle = aux.flatten([["-", "--"] * len(color)])
        elif s == "jetenergycrs":
            FigNameMain = "JetEnergy"
            Xlabel = r"$t\,[\mathrm{Myr}]$"
            Ylabel = r"$E\,[\mathrm{erg}]$"
            setY = [
                4.1,
                4.11,
                4.2,
                4.21,
                4.3,
                4.31,
                4.32,
                4.321,
                4.322,
                4.3221,
                4.41,
                4.5,
                4.61,
                4.62,
            ]
            setX = len(setY) * [0]
            # color = aux.flatten([[x] * 2 for x in color])
            offsetX = 0.5
            Ymin = None
            Ymax = None  # 620#140
            # TickStyle = aux.flatten([["-", "--"] * len(color)])
            logY = True
            single_xlabel = False
        elif s == "jetenergycrslog":
            FigNameMain = "JetEnergy"
            Xlabel = r"$t\,[\mathrm{Myr}]$"
            Ylabel = r"$E\,[\mathrm{erg}]$"
            setY = [4.1111]
            setY += [4.33]
            setY += [
                4.1,
                4.11,
                4.2,
                4.21,
                4.3,
                4.31,
                4.32,
                4.321,
                4.322,
                4.3221,
                4.41,
                4.5,
                4.61,
                4.62,
            ]
            setX = [0.001] + [0.0001] + (len(setY) - 2) * [0]
            offsetX = 0.5
            logY = True
            single_xlabel = False
        elif s == "energycum" or s == "cumenergy":
            FigNameMain = "energycum"
            Xlabel = r"$t\,[\mathrm{Myr}]$"
            Ylabel = r"$E\,[\mathrm{erg}]$"
            setY = [9.11, 9.5111, 9.0111, 8.11]  # , 4.3, 4.31]
            setX = len(setY) * [0]
            offsetX = 0.5
            Ymin = None
            Ymax = None  # 1e2
            # Ymin = 1e57
            # Ymax = 1e63  # 1e2
            logY = True
        elif s == "jetenergycheck":
            FigNameMain = "JetEnergy"
            Xlabel = r"$E_\mathrm{RM}\,[\mathrm{erg}]$"
            Ylabel = r"$E\,[\mathrm{erg}]$"
            setY = [9.11]  # , 4.62]  # , 4.3, 4.31]
            setX = [4.61]  # , 4.61]
            offsetX = 0.5
            Xmin = 1e57
            Xmax = 1e63  # 1e2
            Ymin = 1e57
            Ymax = 1e63  # 1e2
            logY = True
            logX = True
            plotLineEqual = True
        elif s == "jetenergykintotal":
            FigNameMain = "JetEnergy"
            Ylabel = [
                r"$E_\mathrm{RM}\,[\mathrm{erg}]$",
                r"$E_\mathrm{QM}\,[\mathrm{erg}]$",
                r"$E_\mathrm{RM}\,[\mathrm{erg}]$",
            ]
            Xlabel = None  # r"$E_\mathrm{kin,inj}\,[\mathrm{erg}]$"
            setX = [4.1, 4.1, 4.101]  # , 4.3, 4.31]
            setY = [4.61, 4.62, 4.61]
            offsetX = 0.5
            Xmin = 1e57
            Xmax = 1e63  # 1e2
            Ymin = 1e57
            Ymax = 1e63  # 1e2
            logY = True
            logX = True
            plotLineEqual = True
        # mdot 7 sfr 8
        # cum stars formed: 5 from txtfile and 6 from snapshot
        elif s == "kineticenergy":
            FigNameMain = "kineticenergy"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = [
                r"$E_{\mathrm{kin,r<200\,\mathrm{kpc}}}\,[\mathrm{erg}]$",
                # r"$E_{\mathrm{kin,r<500\,\mathrm{kpc}}}\,[\mathrm{erg}]$",
                r"$\bar{v}_{r<200\,\mathrm{kpc}}\,[\mathrm{km}\,\mathrm{s}^{-1}]$",
                r"$\bar{v}_{r<100\,\mathrm{kpc}}\,[\mathrm{km}\,\mathrm{s}^{-1}]$",
                r"$\bar{v}_{\mathrm{mass},r<200\,\mathrm{kpc}}\,[\mathrm{km}\,\mathrm{s}^{-1}]$",
                r"$\bar{v}_{\mathrm{mass},r<100\,\mathrm{kpc}}\,[\mathrm{km}\,\mathrm{s}^{-1}]$",
                # r"$\bar{v}_{r<500\,\mathrm{kpc}}\,[\mathrm{km}\,\mathrm{s}^{-1}]$",
            ]
            setY = [
                6.81,
                # 6.811,
                6.82,
                6.8211,
                6.822,
                6.8221,
                # 6.821
            ]
            setX = [0] * len(setY)
            offsetX = 0.5
            alpha = 0.5
            show_reductions = ["mean"] * len(setY)
            logX = False
            logY = False
            Ymin = None
            Ymax = None
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "kineticenergyreduced":
            FigNameMain = "kineticenergy"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = [
                r"$E_{\mathrm{kin},r<200\,\mathrm{kpc}}\,[\mathrm{erg}]$",
                r"$\bar{v}_{r<200\,\mathrm{kpc}}\,[\mathrm{km}\,\mathrm{s}^{-1}]$",
            ]
            setY = [
                6.81,
                6.822,
            ]
            setX = [0] * len(setY)
            offsetX = 0.5
            alpha = 0.5
            show_reductions = ["mean"] * len(setY)
            logX = False
            logY = False
            Ymin = 0
            Ymax = [3e60, 150]
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "MassBHStarsGas" or s == "mdotstars":
            FigNameMain = "SFRBHMdot"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            # Ylabel = r'$\mathrm{SFR}\ \dot{M}_\mathrm{BH}\ [M_\odot\ \mathrm{yr}^{-1}]$'
            Ylabel = r"$\dot{M}\ [M_\odot\ \mathrm{yr}^{-1}]$"
            setX = [0, 0, 0]
            setY = [7, 8, 7.01]
            plotting_order = [0, 1, 0]
            show_reductions = ["mean", "mean", "mean"]
            offsetX = 0.5
            Ymin = None
            Ymax = 1e3  # 1e2
            logY = True
            useFolderIndexAsColor = False
            useFolderIndexAsTickStyle = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "mdot" or s == "mdotpresentation":
            FigNameMain = "Mdot"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            # Ylabel = r'$\mathrm{SFR}\ \dot{M}_\mathrm{BH}\ [M_\odot\ \mathrm{yr}^{-1}]$'
            Ylabel = [r"$\dot{M}\ [M_\odot\ \mathrm{yr}^{-1}]$"]
            if s == "mdotpresentation":
                Ylabel = [r"$\mathrm{Accretion}\,\mathrm{rate}\,[M_\odot\ \mathrm{yr}^{-1}]$"]
            setX = [0]
            setY = [7]
            plotting_order = [0]
            offsetX = 0.5
            Ymin = None
            Ymax = 6e1  # 1e2
            logY = True
            legend_folder_position = "upper left"
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "cgpjsfr" or s == "wang":
            FigNameMain = "wang_cgpjsfr"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            # Ylabel = r'$\mathrm{SFR}\ \dot{M}_\mathrm{BH}\ [M_\odot\ \mathrm{yr}^{-1}]$'
            Ylabel = [
                r"${M}_\mathrm{cold}\ [M_\odot]$",
                r"$\mathrm{P}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$\mathrm{SFR}\ [M_\odot\ \mathrm{yr}^{-1}]$",
                r"$M_\mathrm{star,cum}\ [M_\odot]$",
            ]
            setY = [6.54, 6.544, 7, 8]
            setY = [6.54, 6.544, 9, 9.1, 8]  # 6.545,
            setY = [6.54, 9.1, 8, 5.1]
            plotting_order = [0, 0, 1, 2]
            plotting_order = [0, 0, 1, 1, 2]
            plotting_order = [0, 1, 2, 3]
            show_reductions = [None, "mean", "mean", None]
            setX = len(setY) * [0]
            offsetX = 0.5
            Ymin = None
            Ymax = None  # 1e2
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "coldpowerstarsbh" or s == "wangp":
            FigNameMain = "wangp_coldpowerstarsbh"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = [
                r"${M}_\mathrm{cold}\ [\mathrm{M}_\odot]$",
                r"$\mathrm{SFR}\ [\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
                r"$L_\mathrm{jet}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${M}_\mathrm{bh}\ [\mathrm{M}_\odot]$",
            ]
            # setY = [6.54, 8, 9.1, 7.31]
            setY = [6.54, 8, 9.1, 7.32]
            setX = len(setY) * [0]
            offsetX = 0.5
            legend_folder_position = "lower right"
            # Ymin = [2e6, 2e-1, 2e42, None]
            Ymin = [2e6, 2e-1, 1e43, None]
            Ymax = [1e11, 2e3, 1e46, None]  # 1e2
            logY = True
            show_reductions = [None, "mean", "mean", None]
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "wangpreduced" or s == "wangpreducedpresentation":
            FigNameMain = "wangpreduced_jetpowersfr"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = [
                r"$\mathrm{SFR}\ [\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
                r"$L_\mathrm{jet}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            if s == "wangpreducedpresentation":
                Ylabel = [
                    r"$\mathrm{Star}\,\mathrm{formation}\,\mathrm{rate}\,[\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
                    r"$\mathrm{Jet}\,\mathrm{power}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                ]
            setY = [8, 9.1]
            setX = len(setY) * [0]
            offsetX = 0.5
            legend_folder_position = "upper left"
            # Ymin = [2e6, 2e-1, 2e42, None]
            Ymin = [2e-1, 1e43]
            Ymax = [2e3, 1e46]  # 1e2
            logY = True
            force_show_folder = True
            show_reductions = ["mean", "mean"]
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "sfronly":
            FigNameMain = "sfronly"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = [
                r"$\mathrm{SFR}\ [\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
            ]
            # setY = [6.54, 8, 9.1, 7.31]
            setY = [8]
            setX = len(setY) * [0]
            offsetX = 0.5
            legend_folder_position = "upper left"
            force_show_folder = True
            # Ymin = [2e6, 2e-1, 2e42, None]
            Ymin = [2e-1]
            Ymax = [2e3]  # 1e2
            logY = True
            show_reductions = ["mean"]
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "coldpowerstarsbhxraycav" or s == "wangpextended":
            FigNameMain = "wangp_coldpowerstarsbhxraycav"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = [
                r"${M}_\mathrm{cold}\ [\mathrm{M}_\odot]$",
                r"$\mathrm{SFR}\ [\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
                r"$L_\mathrm{jet}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${L}_\mathrm{ICM}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${L}_\mathrm{ICM,theory}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${L}_\mathrm{cav}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${M}_\mathrm{bh}\ [\mathrm{M}_\odot]$",
            ]
            # setY = [6.54, 8, 9.1, 7.31]
            setY = [6.54, 8, 9.1, 9.01, 9.02, 9.51, 7.32]
            setX = len(setY) * [0]
            offsetX = 0.5
            legend_folder_position = "lower right"
            # Ymin = [2e6, 2e-1, 2e42, None]
            Ymin = [2e6, 2e-1, 1e43, 1e43, 1e43, 1e43, None]
            Ymax = [1e11, 2e3, 1e46, 1e46, 1e46, 1e46, None]  # 1e2
            logY = True
            show_reductions = [None, "mean", "mean", "mean", "mean", "mean", None]
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "coldpowerstarsbh1" or s == "wangp1":
            FigNameMain = "wangp_coldpowerstarsbh"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = [
                r"${M}_\mathrm{cold}\ [\mathrm{M}_\odot]$",
                r"$\mathrm{SFR}\ [\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
            ]
            setY = [6.54, 8]
            setX = len(setY) * [0]
            offsetX = 0.5
            legend_folder_position = "lower right"
            Ymin = [2e6, 2e-1]
            Ymax = [1e11, 2e3]  # 1e2
            logY = True
            show_reductions = [None, "mean"]
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "coldpowerstarsbh2" or s == "wangp2":
            FigNameMain = "wangp_coldpowerstarsbh"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = [
                r"$L_\mathrm{jet}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${M}_\mathrm{bh}\ [\mathrm{M}_\odot]$",
            ]
            setY = [9.1, 7.31]
            setX = len(setY) * [0]
            offsetX = 0.5
            legend_folder_position = "lower right"
            Ymin = [2e42, None]
            Ymax = [1e46, None]  # 1e2
            logY = True
            show_reductions = ["mean", None]
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetpowerhist":
            FigNameMain = "JetPower_hist"
            Xlabel = [
                r"$L_\mathrm{jet}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{cav}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{ICM}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            Ylabel = [
                r"$\mathrm{PDF}$",
                r"$\mathrm{PDF}$",
                r"$\mathrm{PDF}$",
            ]
            setY = [9.1, 9.51, 9.012]  # , 9.01]  # + [7.001]
            setX = [99.001] * len(setY)  # + [99.01]
            offsetX = 0.5
            alpha = 0.5
            normalize_histogram = True
            plotting_style = "histogram"
            box_smoothing_use_x_as_weights = False
            histogram_plot_reduction = "median"
            # box_smoothing = [10]
            histogram_include_outlier_bins = True
            logX = True
            logY = False
            Xmin = 8e42
            Xmax = 2e46
            Ymin = 0
            Ymax = 5.9
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetpowerhisthighres":
            FigNameMain = "JetPower_hist"
            Xlabel = [
                r"$L_\mathrm{jet}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{cav,box=150,res=100}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{cav,box=300}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{cav,res=200}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{ICM}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            Ylabel = [
                r"$\mathrm{pdf}$",
                r"$\mathrm{pdf}$",
                r"$\mathrm{pdf}$",
                r"$\mathrm{pdf}$",
                r"$\mathrm{pdf}$",
            ]
            setY = [9.1, 9.51, 9.503, 9.502, 9]  # , 9.01]  # + [7.001]
            setX = [99.001] * len(setY)  # + [99.01]
            offsetX = 0.5
            alpha = 0.5
            normalize_histogram = True
            plotting_style = "histogram"
            box_smoothing_use_x_as_weights = False
            # box_smoothing = [10]
            histogram_include_outlier_bins = True
            logX = True
            logY = False
            Xmin = 1e42
            Xmax = 4e46
            Xmin = 8e42
            Xmax = 2e46
            Ymin = 0
            Ymax = 4.2
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetpowerevolution":
            FigNameMain = "jetpowerevolution"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = [
                r"$L_\mathrm{jet}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{cav}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{ICM}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            setY = [9.1, 9.51, 9]  # , 9.01]  # + [7.001]
            setX = len(setY) * [0]
            offsetX = 0.5
            alpha = 0.5
            logX = False
            logY = True
            Ymin = 2e42
            Ymax = 1e46
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetpowergasmass":
            FigNameMain = "jetpowergasmass"
            Xlabel = [
                r"$M_\mathrm{SF}\,[\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
                r"$M_{<10\,\mathrm{Myr},\mathrm{No}\,\mathrm{SF}}\,[\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
                r"$M_{10\,\mathrm{Myr}<t_\mathrm{cool}<100\,\mathrm{Myr}}\,[\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
                r"$M_\mathrm{SF}\,[\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
                r"$M_{<10\,\mathrm{Myr},\mathrm{No}\,\mathrm{SF}}\,[\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
                r"$M_{10\,\mathrm{Myr}<t_\mathrm{cool}<100\,\mathrm{Myr}}\,[\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
            ]
            Ylabel = [
                r"$L_\mathrm{cav}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{cav}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{cav}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{jet}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{jet}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{jet}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            setX = [6.5, 6.51, 6.52, 6.5, 6.51, 6.52]
            setY = [9.51, 9.51, 9.51, 9.1, 9.1, 9.1]  # , 9.01]  # + [7.001]
            offsetX = 0.5
            alpha = 0.5
            logX = True
            logY = True
            TickStyle = "x"
            Ymin = Ymax = None
            Xmin = Xmax = None
            # Ymin = 2e42
            # Ymax = 1e46
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetpowerevolutiondebug":
            FigNameMain = "jetpowerevolutiondebug"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = [
                r"$L_\mathrm{jet,log}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{jet}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{cav}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$L_\mathrm{ICM}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            setY = [9.1, 9.51, 9] + [7.001]
            setX = (len(setY) - 1) * [0] + [0.01]
            offsetX = 0.5
            alpha = 0.5
            show_reductions = ["mean", "mean", "mean", "mean"]
            logX = False
            logY = True
            Ymin = 2e42
            Ymax = 1e46
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetpowerhistlog":
            FigNameMain = "JetPower_hist"
            Xlabel = r"$\mathrm{P}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            Ylabel = [
                r"$N_\mathrm{jet,log,deltat}$",
                r"$N_\mathrm{jet}$",
                r"$N_\mathrm{cav}$",
                r"$N_\mathrm{cool,30kpc}$",
                r"$N_\mathrm{cool,100kpc}$",
            ]
            setY = [7.001] + [9.1, 9.51, 9, 9.01]
            setX = [99.01] + [99.001] * 4
            offsetX = 0.5
            alpha = 0.5
            normalize_histogram = True
            plotting_style = "histogram"
            box_smoothing_use_x_as_weights = True
            # box_smoothing = [40]
            logX = True
            logY = False
            Xmin = 1e42
            Xmax = 1e47
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetpowersimobs":
            FigNameMain = "JetPowerSimVsObs"
            Xlabel = r"${L}_\mathrm{jet}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            Ylabel = [
                r"${L}_\mathrm{cav}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            setY = [9.51]
            setX = [9.1]
            offsetX = 0.5
            alpha = 0.5
            box_smoothing_use_x_as_weights = False
            # box_smoothing = [30]
            TickStyle = "x"
            Xmin = None
            Xmax = None
            Ymin = None
            Ymax = None  # 1e2
            logX = True
            plotLineEqual = True
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetpowerentropy":
            theory_plot_x = ["entropyPfrommer2012"]
            theory_plot_y = ["pcavPfrommer2012"]
            FigNameMain = "jetpowerentropy"
            Xlabel = r"${K}_{e,\,0}\ [\mathrm{keV}\,\mathrm{cm}^{2}]$"
            Ylabel = [
                r"${L}_\mathrm{cav}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            setY = [9.51]
            setX = [14.1]
            offsetX = 0.5
            alpha = 0.5
            TickStyle = "x"
            Xmin = None
            Xmax = None
            Ymin = None
            Ymax = None  # 1e2
            logX = True
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetpowervscooling" or s == "jetpowervscoolingpresentation":
            FigNameMain = "JetPowerVsCooling"
            Xlabel = [
                r"${L}_\mathrm{ICM}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${L}_\mathrm{cav}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            Ylabel = [
                r"${L}_\mathrm{jet}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${L}_\mathrm{jet}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            setX = [9.012, 9.51]
            setY = [9.1, 9.1]
            offsetX = 0.5
            alpha = 0.5
            TickStyle = "x"
            Xmin = [2e44, 8e42]  # 1e42
            Xmax = [7e45, 2e46]
            Ymin = 1e42
            Ymax = 2e46  # 1e2
            if s == "jetpowervscoolingpresentation":
                Xlabel = [
                    r"$\mathrm{Cavity}\,\mathrm{power}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                    r"$\mathrm{Cooling}\,\mathrm{luminosity}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                ]
                Ylabel = [
                    r"$\mathrm{Jet}\,\mathrm{power}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                    r"$\mathrm{Jet}\,\mathrm{power}\,[\mathrm{erg}\,\mathrm{s}^{-1}]$",
                ]
                Xmin = Xmin[::-1]
                Xmax = Xmax[::-1] #[2e47, 3e45]
                setX = setX[::-1]
                setY = setY[::-1]
                legend_folder_axes = [1,0]
            alphaVerticalShading = 0.15
            logX = True
            plotLineEqual = True
            logY = True
            legend_folder_position = "lower right"
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "coolingvsxraycooling":
            FigNameMain = "coolingvsxraycooling"
            Xlabel = [
                r"${L}_\mathrm{X,ICM}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${L}_\mathrm{X,ICM}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${L}_\mathrm{X,ICM}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            Ylabel = [
                r"${L}_\mathrm{jet}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${L}_\mathrm{ICM}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${L}_\mathrm{X,debug,ICM}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            setX = [9.02, 9.02, 9.02]
            setY = [9.1, 9.01, 9.021]
            offsetX = 0.5
            alpha = 0.5
            TickStyle = "x"
            Ymin = None  # [6e44, 8e42]  # 1e42
            Ymax = None  # 2e46  # [2e46, 2e46]
            Xmin = None  # 1e42
            Xmax = None  # 2e46  # 1e2
            alphaVerticalShading = 0.15
            logX = True
            plotLineEqual = True
            logY = True
            legend_folder_position = "lower right"
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "coolingvssfr":
            FigNameMain = "coolingvssfr"
            Xlabel = [
                r"${L}_\mathrm{ICM}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            Ylabel = [
                r"$\mathrm{SFR}\ [\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
            ]
            setX = [9.01]
            setY = [8]
            offsetX = 0.5
            alpha = 0.5
            TickStyle = "x"
            Xmin = None
            Xmax = None  # [2e46, 2e46]
            Ymin = None
            Ymax = None  # 1e2
            logX = True
            plotLineEqual = False
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "mdotvssfr":
            FigNameMain = "coolingvssfr"
            Xlabel = [
                r"$\dot{M}_\mathrm{bh}\ [\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
            ]
            Ylabel = [
                r"$\mathrm{SFR}\ [\mathrm{M}_\odot\,\mathrm{yr}^{-1}]$",
            ]
            setX = [7]
            setY = [8]
            offsetX = 0.5
            alpha = 0.5
            TickStyle = "x"
            Xmin = None
            Xmax = None  # [2e46, 2e46]
            Ymin = None
            Ymax = None  # 1e2
            logX = True
            plotLineEqual = True
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetpowertimescale":
            FigNameMain = "JetPowerVsBubbleTimescaleafterfix"
            Xlabel = r"$\mathrm{t}_\mathrm{bubble}\ [\mathrm{Myr}]$"
            Ylabel = [
                r"$\mathrm{L}_\mathrm{cav}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"$\mathrm{L}_\mathrm{jet}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            setY = [9.51, 9.1]
            setX = [9.612, 9.612]
            offsetX = 0.5
            alpha = 0.5
            TickStyle = "x"
            Xmin = None
            Xmax = None
            Ymin = None
            Ymax = None  # 1e2
            logX = True
            # plotLineEqual = True
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetpowertimescaledebug":
            FigNameMain = "JetPowerVsBubbleTimescale"
            Xlabel = r"$\mathrm{t}_\mathrm{bubble,r100}\ [\mathrm{Myr}]$"
            Ylabel = r"$\mathrm{t}_\mathrm{bubble,r300}\ [\mathrm{Myr}]$"
            setX = [9.61]
            setY = [9.62]
            offsetX = 0.5
            alpha = 0.5
            TickStyle = "x"
            Xmin = None
            Xmax = None
            Ymin = None
            Ymax = None  # 1e2
            logX = True
            plotLineEqual = True
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetpowerheating":
            FigNameMain = "JetPower_hist"
            Xlabel = r"$\mathrm{L}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            # Ylabel = r'$\mathrm{SFR}\ \dot{M}_\mathrm{BH}\ [M_\odot\ \mathrm{yr}^{-1}]$'
            Ylabel = [
                r"${L}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
                r"${L}_\mathrm{log}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            normalize_histogram = True
            setY = [9.1, 7.001]
            setX = [0, 0.01]
            offsetX = 0.5
            alpha = 0.5
            Ymin = None
            Ymax = None  # 1e2
            logX = True
            logY = False
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetstars":
            FigNameMain = "jet_stars"
            Xlabel = r"$\mathrm{SFR}\ [M_\odot\ \mathrm{yr}^{-1}]$"
            Ylabel = [
                # r"${M}_\mathrm{cold}\ [M_\odot]$",
                r"${L}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$",
            ]
            setY = [9.1]
            setX = [8]
            logX = True
            Xmin = 1e-1
            Xmax = 1e2
            offsetX = 0.5
            Ymin = None
            Ymax = None  # 1e2
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "pdvcool":
            FigNameMain = "pdvcool"
            TickStyle = "x"
            Xlabel = r"${L}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            Ylabel = [r"$\mathrm{pdV}\ [\mathrm{erg}]$"]
            setY = [9.4]
            setX = [9]
            logX = True
            Xmin = 1e-1
            Xmax = 1e2
            Xmin = Xmax = None
            offsetX = 0.5
            Ymin = None
            Ymax = None  # 1e2
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "pdvcoollowres":
            FigNameMain = "pdvcoollowres"
            TickStyle = "x"
            Xlabel = r"${L}_\mathrm{cool}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            Ylabel = [r"$\mathrm{pdV}\ [\mathrm{erg}]$"]
            setY = [9.41]
            setX = [9]
            logX = True
            Xmin = 1e-1
            Xmax = 1e2
            Xmin = Xmax = None
            offsetX = 0.5
            Ymin = 1e56
            Ymax = 1e61  # 1e2
            logY = True
            k_average = 30
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "bubblepowerlowres":
            FigNameMain = "bubblepowerlowres"
            TickStyle = "x"
            Xlabel = r"${L}_\mathrm{cool}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            Ylabel = [r"${L}_\mathrm{cav}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"]
            Xlabel = r"${L}_\mathrm{jet}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            setY = [9.51]
            setX = [9.1]
            logX = True
            # Xmin = 8e44
            # Xmax = 2e46
            offsetX = 0.5
            Xmin = Ymin = 1e42
            Xmax = Ymax = 1e46  # 1e2
            logY = True
            plotLineEqual = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "bubblepoweraxis":
            FigNameMain = "bubblepowerlowres"
            TickStyle = "x"
            Xlabel = r"${L}_\mathrm{cav,ax=1,0}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            Ylabel = [r"${L}_\mathrm{cav,ax=2,0}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"]
            setY = [9.51]
            setX = [9.501]
            logX = True
            offsetX = 0.5
            logY = True
            plotLineEqual = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "bubblepowerres":
            FigNameMain = "bubblepowerlowres"
            TickStyle = "x"
            # Xlabel = (
            #     r"$\mathrm{P}_\mathrm{cav,res=200}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            # )
            Xlabel = r"${L}_\mathrm{cav,res=300}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            Ylabel = [r"${L}_\mathrm{cav,res=100}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"]
            # setX = [9.502]
            setX = [9.503]
            setY = [9.51]
            logX = True
            Xmin = None
            Xmax = None
            offsetX = 0.5
            logY = True
            plotLineEqual = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "bubblepowerboxsize":
            FigNameMain = "bubblepowerlowres"
            TickStyle = "x"
            Xlabel = (
                r"$\mathrm{L}_\mathrm{cav,box=300kpc}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            )
            Ylabel = [
                r"$\mathrm{L}_\mathrm{cav,box=150kpc}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            ]
            setX = [9.503]
            setY = [9.51]
            logX = True
            offsetX = 0.5
            logY = True
            plotLineEqual = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "coldgassimple":
            FigNameMain = "colgassimple"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            # Ylabel = r'$\mathrm{SFR}\ \dot{M}_\mathrm{BH}\ [M_\odot\ \mathrm{yr}^{-1}]$'
            Ylabel = [
                r"${M}_\mathrm{cold}\ [M_\odot]$",
            ]
            setY = [6.54]
            setX = len(setY) * [0]
            offsetX = 0.5
            Ymin = None
            Ymax = None  # 1e2
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "sfr":
            FigNameMain = "SFR"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            # Ylabel = r'$\mathrm{SFR}\ \dot{M}_\mathrm{BH}\ [M_\odot\ \mathrm{yr}^{-1}]$'
            Ylabel = r"$\dot{M}\ [M_\odot\ \mathrm{yr}^{-1}]$"
            setX = [0]
            setY = [8]
            offsetX = 0.5
            Ymin = None
            Ymax = 1e3  # 1e2
            useFolderIndexAsColor = True
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "ColdGasWithinRadius" or s == "coldgas":
            FigNameMain = "CCGasWithinRadius"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            # Ylabel = r'$\mathrm{SFR}\ \dot{M}_\mathrm{BH}\ [M_\odot\ \mathrm{yr}^{-1}]$'
            Ylabel = r"$M\ [M_\odot]$"
            setX = [0, 0, 0, 0]
            setY = [6.2, 6.21, 6.22, 6.23]
            setY = [6.255, 6.244, 6.24, 6.25, 6.1]  # , 6.4]
            setX = [0] * len(setY)
            offsetX = 0.5
            # Ymin = 1e5
            # Ymax = 5e10  # 1e2
            Ymin = Ymax = None
            logY = 1
            legend_variable_position = "lower right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "starvsjetmass":
            FigNameMain = "starvsjetmass"
            Xlabel = r"$M_\mathrm{SF}\ [M_\odot]$"
            Ylabel = r"$M_{\mathrm{jet}}\ [M_\odot]$"
            setX = [6.5]
            setY = [6.71]
            offsetX = 0.5
            Ymin = Ymax = None
            TickStyle = "x"
            logY = logX = 1
            legend_variable_position = "lower right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "starvsjetmassbhhsml":
            FigNameMain = "starvsjetmass_bhhsml"
            Xlabel = r"$M_\mathrm{SF,r<\mathrm{BHhsml}}\ [M_\odot]$"
            Ylabel = r"$M_{\mathrm{jet},r<\mathrm{BHhsml}}\ [M_\odot]$"
            setX = [6.503]
            setY = [6.711]
            offsetX = 0.5
            Ymin = Ymax = None
            logY = logX = 1
            TickStyle = "x"
            legend_variable_position = "lower right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "starvsjetvolbhhsml":
            FigNameMain = "starvsjetvol_bhhsml"
            Xlabel = r"$V_\mathrm{SF,r<\mathrm{BHhsml}}\ [\mathrm{kpc}^{3}]$"
            Ylabel = r"$V_{\mathrm{jet},r<\mathrm{BHhsml}}\ [\mathrm{kpc}^{3}]$"
            setX = [6.5031]
            setY = [6.7111]
            offsetX = 0.5
            Ymin = Ymax = None
            logY = logX = 1
            TickStyle = "x"
            legend_variable_position = "lower right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "gasphases" or s == "gasphase":
            FigNameMain = "GasPhases"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            # Ylabel = r'$\mathrm{SFR}\ \dot{M}_\mathrm{BH}\ [M_\odot\ \mathrm{yr}^{-1}]$'
            Ylabel = r"$M\ [M_\odot]$"
            setY = [6.5, 6.501, 6.502, 6.51, 6.52, 6.53, 6.533, 6.503, 6.513, 6.523]
            setY = [6.5, 6.501, 6.503, 6.51, 6.511, 6.513, 6.52, 6.521, 6.523]
            setX = [0] * len(setY)
            # grouping_colors = [3, 3, 3]
            offsetX = 0.5
            ymin_0 = ymin_1 = ymin_2 = 1e6
            ymax_0, ymax_1, ymax_2 = 1e11, 1e11, 1e11
            Ymin = ([ymin_0] + [ymin_1] + [ymin_2]) * 3
            Ymax = ([ymax_0] + [ymax_1] + [ymax_2]) * 3
            # Ymin = Ymax = None
            logY = 1
            single_xlabel = False
            legend_variable_position = "lower right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "gasphasesreduced":
            FigNameMain = "GasPhases"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = [
                "$M_\mathrm{SF}\,[M_\odot]$",
                "$M_{\mathrm{SF},\,r<r_\mathrm{acc}}\,[{M}_\odot]$",
                "$M_{<10\,\mathrm{Myr},\mathrm{No}\,\mathrm{SF}}\,[{M}_\odot]$",
                "$M_{<10\,\mathrm{Myr},\mathrm{No}\,\mathrm{SF},r<r_\mathrm{acc}}\,[{M}_\odot]$",
            ]
            setY = [6.5, 6.503, 6.51, 6.513]
            setX = [0] * len(setY)
            offsetX = 0.5
            ymin_0 = ymin_1 = ymin_2 = 1e6
            ymax_0, ymax_1, ymax_2 = 1e11, 1e11, 1e11
            Ymin = ymin_0
            Ymax = ymax_0
            logY = 1
            set_ytick_label_both = False
            single_xlabel = False
            legend_variable_position = "lower right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "jetpower":
            FigNameMain = "CoolingPowerWithinRadius"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            # Ylabel = r'$\mathrm{SFR}\ \dot{M}_\mathrm{BH}\ [M_\odot\ \mathrm{yr}^{-1}]$'
            Ylabel = r"$P_\mathrm{AGN}\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            setX = [0]
            setY = [9.1]
            offsetX = 0.5
            Ymin = None
            Ymax = None  # 1e2
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "coolingpower" or s == "cooling":
            FigNameMain = "CoolingPowerWithinRadius"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            # Ylabel = r'$\mathrm{SFR}\ \dot{M}_\mathrm{BH}\ [M_\odot\ \mathrm{yr}^{-1}]$'
            Ylabel = r"$P\ [\mathrm{erg}\,\mathrm{s}^{-1}]$"
            setX = [0, 0]
            setY = [9, 9.1]
            offsetX = 0.5
            Ymin = None
            Ymax = None  # 1e2
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "accretionradius" or s == "radius":
            FigNameMain = "NgbRadius"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = r"$r_\mathrm{ngb} [\mathrm{kpc}]$"
            setX = [0, 0, 0]
            setY = [10, 10.1, 10.5]
            offsetX = 0.5
            Ymin = None
            Ymax = None  # 1e2
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "mdotconsistency" or s == "mdotcons":
            FigNameMain = "MdotConsistency"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = r"$\dot{M}\ [M_\odot\ \mathrm{yr}^{-1}]$"
            setX = [0, 0, 0, 0.01]  # , 0, 0, 0,] # 0.01]
            setY = [7, 7.23, 7.231, 7.0001]  # , 7.24, 7.01, 7.1] #, 7.0001]
            offsetX = 0.5
            Ymin = None
            Ymax = None  # 1e2
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
        elif s == "mdotcc":
            FigNameMain = "MdotConsistency"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = r"$\dot{M}\ [M_\odot\ \mathrm{yr}^{-1}]$"
            setX = [0, 0, 0, 0, 0, 0.01]
            setY = [7, 7.23, 7.24, 7.01, 7.02, 7.0001]
            offsetX = 0.5
            Ymin = None
            Ymax = None  # 1e2
            logY = True
            legend_variable_position = "upper right"
            LegendLoc = "lower right"
            showOnOtherYAxis = []
            useFolderIndexAsColor = False
            useTimeIndexAsColor = False
        elif s == "maxspeed" or s == "maxspeeds":
            FigNameMain = "maxspeeds"
            Xlabel = r"$t\ [\mathrm{Myr}]$"
            Ylabel = [
                "$v_\mathrm{A,max}\ [\mathrm{km}\,\mathrm{s}^{-1}]$",
                "$v_\mathrm{s,max}\ [\mathrm{km}\,\mathrm{s}^{-1}]$",
            ]
            setX = [0, 0]
            setY = [4.12, 4.13]
            positionsHorizontal = [Param.SPEEDOFLIGHT_cgs / 1e5]
            logY = True
        else:
            raise Exception(f"setup {s} not found!")

        if len(setX) != len(setY) and plotting_style == "default":
            print(setX, setY)
            raise Exception("Error: need same elements of variables on both axis!")
        if aux.isIterable(Ylabel):
            assert len(Ylabel) == len(
                setY
            ), f"len(Ylabel) {len(Ylabel)} != len(setY) {len(setY)}"
        if aux.isIterable(Xlabel):
            assert len(Xlabel) == len(
                setX
            ), f"len(Xlabel) {len(Xlabel)} != len(setX) {len(setX)}"
        for indexSet, Set in enumerate([setX, setY]):
            i = 0
            j = 0
            INFO = {}
            INFO_log = {}
            INFO["FieldsToLoad"] = "basic"
            INFO["ShowWhichValue"] = "all"
            if setup1:
                INFO["ShowWhichValue"] = 1  #'all'
            SetupNam = []
            variableLabel = []
            SetupNam_log = []
            variableLabel_log = []

            index = 0
            LOGFILE_SETUPS = [
                0.01,
                0.001,
                0.0001,
                4.1111,
                4.33,
                7.001,
                7.0001,
                99.01,
                99.1,
            ]
            print(f"current log setups: {LOGFILE_SETUPS}")
            for setNum in Set:
                print(f"setNum: {setNum}")
                if setNum in LOGFILE_SETUPS:
                    index = j
                    info = INFO_log
                    setup_name = SetupNam_log
                else:
                    index = i
                    info = INFO
                    setup_name = SetupNam
                (
                    index,
                    setup_name,
                    variable_label,
                    info,
                    setup_style,
                ) = get_grid_variable(
                    index, setNum, [], [], info, MAXSEARCHRADIUS=MAXSEARCHRADIUS
                )
                if setup_style == "logfile":
                    j += 1
                    INFO_log = info
                    variableLabel_log.extend(variable_label)
                    SetupNam_log.extend(setup_name)
                elif setup_style == "default":
                    i += 1
                    INFO = info
                    variableLabel.extend(variable_label)
                    SetupNam.extend(setup_name)
                else:
                    raise Exception(f"setup_style {setup_style} not understood!")
            if indexSet == 0:
                if not any([x.startswith("0-") for x in INFO.keys()]):
                    INFO = {}
                INFOArrayX = aux.INFOSorter(INFO.copy(), FinalDimension=[i])
                SetupNamX = SetupNam
                variableLabelX = variableLabel

                INFOArrayX_log = aux.INFOSorter(INFO_log.copy(), FinalDimension=[j])
                SetupNamX_log = SetupNam_log
                variableLabelX_log = variableLabel_log
            else:
                if not any([x.startswith("0-") for x in INFO.keys()]):
                    INFO = {}
                INFOArrayY = aux.INFOSorter(INFO.copy(), FinalDimension=[i])
                SetupNamY = SetupNam
                variableLabelY = variableLabel

                INFOArrayY_log = aux.INFOSorter(INFO_log.copy(), FinalDimension=[j])
                SetupNamY_log = SetupNam_log
                variableLabelY_log = variableLabel_log
        if Timesetup is not None:
            TimesName = Timesetup
        else:
            TimesName = Times
        if not useTime:
            TimesName = ""
            if len(INFOArrayX):
                INFOArrayX = INFOAddKey(INFOArrayX, "useTime", False)
            if len(INFOArrayY):
                INFOArrayY = INFOAddKey(INFOArrayY, "useTime", False)

        addInfo = ""
        if setup1:
            addInfo += "%s" % str(INFO["ShowWhichValue"])
        print(f"box_smoothing: {box_smoothing}")
        if box_smoothing != [False]:
            addInfo += "Bs%s%s" % (
                aux.convert_params_to_string(box_smoothing),
                box_statistics,
            )
            if box_smoothing_use_x_as_weights:
                addInfo += "XWei"
            if histogram_include_outlier_bins:
                addInfo += "OUT"
        if plotting_style == "histogram":
            addInfo += "His%i" % (hist_bin_number)
            if normalize_histogram:
                addInfo += "Nor"
            if ignore_histogram_weights:
                addInfo += "NoWEI"
            if histogram_plot_reduction:
                addInfo += f"Pl{histogram_plot_reduction.upper()[:3]}"
        if time_values_min:
            addInfo += f"TVMi%i" % time_values_min
        if time_values_max:
            addInfo += f"TVMa%i" % time_values_max
        print(f"TimesName:{TimesName}")
        filetxt = "Grid_%s%s_%s_%s_%s_%s_%s_Xm%sLog%i%i.pdf" % (
            addInfo,
            aux.convert_params_to_string(SetupNamX, string=True),
            aux.convert_params_to_string(
                SetupNamY, string=True, reduce_same_starting_string=True
            ),
            aux.convert_params_to_string(SetupNamX_log, string=True),
            aux.convert_params_to_string(SetupNamY_log, string=True),
            aux.convert_params_to_string(
                NameOfSimSave, string=True, reduce_same_starting_string=True
            ),
            aux.convert_params_to_string(TimesName, string=False),
            aux.convert_params_to_string(Xmax, string=False),
            int(logX),
            int(logY),
        )
        filetxt = aux.shortenFileName(filetxt)
        name = "../figures/Grids/%s/%s" % (FigNameMain, filetxt)
        name_length = len(os.path.basename(name))
        print(f"... working on {name}")
        if name_length > 255:
            raise Exception(
                f"filename {os.path.basename(name)} too long! ({name_length}>255)"
            )
        if len(INFOArrayX):
            Times = []
            for folder in Folders:
                Times.append(
                    Fig.getSnapNumber(
                        folder,
                        None,
                        returnType=returnType,
                        returnAllTimes=useTime,
                        returnTimeAndNumber=True,
                    )
                )
            DATA_X, DATA_Y = getData(
                Times,
                Folders,
                INFOArrayX,
                SetupNamX,
                variableLabelX,
                INFOArrayY,
                SetupNamY,
                variableLabelY,
                numthreads=numthreads,
                removeSaveTextFile=removeSaveTextFile,
            )
        else:
            DATA_X, DATA_Y = {}, {}
        Data_X_Log, Data_Y_Log = getDataLogFile(
            Folders,
            SetupNamX_log,
            SetupNamY_log,
            variableLabelX_log,
            variableLabelY_log,
            INFOArrayX_log,
            INFOArrayY_log,
        )
        DATA_X, DATA_Y = combine_Datas(DATA_X, DATA_Y, Data_X_Log, Data_Y_Log, i, j)
        PLOTSTime = []
        LABELFolder = []
        PLOTSFolder = []
        LABELRest = []
        LABELRestX = []
        PLOTSRest = []
        numberPlots = int(len(DATA_X.keys()))
        plot_type = []
        folder_type = []
        for indexPlot in range(numberPlots):
            ytag = f"{indexPlot}y"
            xtag = f"{indexPlot}x"
            index_plot_type_y = DATA_Y[ytag]["IndexPlotType"]
            index_plot_type_x = DATA_X[xtag]["IndexPlotType"]
            index_folder = DATA_Y[ytag]["IndexFolder"]
            plot_type.append(index_plot_type_x * 100 + index_plot_type_y)
            folder_type.append(index_folder)
        number_unique_setups = len(np.unique(plot_type))
        number_unique_folders = len(np.unique(folder_type))
        if fontsize is None:
            if not split_setups_in_panels:
                fontsize = 24  # 28
            if not single_xlabel:
                fontsize = 8  # 28
                if split_by_folder:
                    fontsize = (5 / number_unique_folders) ** (2 / 3.0) * 6
            else:
                fontsize = 12  # 28
        if not split_setups_in_panels:
            tick_length_major = 12
            tick_length_minor = 7
        if not single_xlabel:
            tick_length_major = 3
            tick_length_minor = 1.4
        else:
            tick_length_major = 5
            tick_length_minor = 3
        print(f"tick_length_major: {tick_length_major}")
        print(f"tick_length_minor: {tick_length_minor}")
        markersize = 4 * fontsize / 22.0
        linewidth = 0.6 * markersize
        font = {"family": "serif", "serif": ["Arial"], "size": fontsize}
        ticks_font = {"family": "sans-serif", "serif": ["Helvetica"]}  # ,
        mpl.rc("text", usetex=True)
        mpl.rcParams[
            "text.latex.preamble"
        ] = r"\usepackage{amsmath,bm}\newcommand{\sbf}[1]{\textsf{\textbf{#1}}}"
        mpl.rc("font", **font)
        mpl.use("Agg")
        if plotting_order == "default":
            plotting_order = np.arange(number_unique_setups)
        number_unique_setup_plots = len(np.unique(plotting_order))
        print(f"number_unique_setup_plots: {number_unique_setup_plots}")
        print(f"plotting_order: {plotting_order}")
        if box_smoothing != [False]:
            if len(box_smoothing) == 1:
                box_smoothing = box_smoothing * number_unique_setup_plots
            elif len(box_smoothing) != number_unique_setups:
                raise Exception(
                    f"box_smoothing: {box_smoothing} not sufficient entries for {number_unique_setup_plots} setups!"
                )
        else:
            box_smoothing = [False]
        print(f"box_smoothing: {box_smoothing}")
        if legend_external is not None:
            figsize = (14, 8)
        else:
            figsize = (10, 8)
        if not split_setups_in_panels and not split_by_folder:
            number_axes = 1
            fig = plt.figure(figsize=figsize)
            marginLeft = 0.12 * fontsize / 24.0
            marginBottom = 0.1 * fontsize / 24.0
            topExtra = 0.015 * fontsize / 24.0
            rightExtra = 0.022 * fontsize / 24.0
            if legend_external is not None:
                rightExtra = 0.022 * fontsize / 24.0 + 0.3
            ax1 = plt.axes(
                [
                    marginLeft,
                    marginBottom,
                    1 - marginLeft - rightExtra,
                    1 - marginBottom - topExtra,
                ]
            )
        else:
            number_axes = number_unique_setup_plots  # number_unique_setups
            x_n, y_n = AxesGeneration.get_xy_shape(number_axes)
            x_size = 7
            y_size = 12
            if single_xlabel and split_by_folder is None:
                x_n = 1
                y_n = len(np.unique(plotting_order))
                x_size = 8
                y_size = 24
                number_axes = len(np.unique(plotting_order))
            if split_by_folder is not None and split_setups_in_panels:
                x_n = (
                    number_unique_folders
                    if split_by_folder == "x"
                    else number_unique_setup_plots
                )
                y_n = (
                    number_unique_setup_plots
                    if split_by_folder == "x"
                    else number_unique_folders
                )
                x_size = 8
                y_size = 20
                number_axes = x_n * y_n
            shape_list = [[[x_size, y_size] for i in range(y_n)] for j in range(x_n)]
            if wspace is None:
                wspace = 0.07
                if split_by_folder and number_unique_folders > 2:
                    wspace = 0.06 / number_unique_folders * 4
                if single_xlabel and minimalist:
                    wspace = 0.01
            if space_below is None:
                space_below = 0.06
                if split_by_folder and number_unique_folders > 2:
                    space_below = 0.06 / number_unique_folders * 4
                if single_xlabel and minimalist:
                    space_below = 0.01
            left = wspace
            bottom = space_below
            fig, Axes, AxesColorbar, axes_colorbar_above = AxesGeneration.getAxesList(
                shape_list,
                show_colorbar_above=False,
                show_colorbar=False,
                left=left,
                right=0.02,
                hspace=space_below,
                wspace=wspace,
                bottom=bottom,
                ShowAxisTicks=True,
            )

        # numberNumbers = np.shape(LABELS)[1]
        number_iterable = numberPlots + number_unique_folders
        color = Fig.makeIterable(
            color, number_iterable, List=False, enforce_correct_number=True
        )
        TickStyle = Fig.makeIterable(
            TickStyle, number_iterable, List=False, enforce_correct_number=True
        )
        alpha = Fig.makeIterable(
            alpha, number_iterable, List=False, enforce_correct_number=True
        )
        skipTimes = False
        yy = []
        xx = []
        reduction_values = [[[] for i in range(y_size)] for j in range(x_size)]
        reduction_colors = [[[] for i in range(y_size)] for j in range(x_size)]
        reduction_alphas = [[[] for i in range(y_size)] for j in range(x_size)]
        xs = [[[] for i in range(y_size)] for j in range(x_size)]
        ys = [[[] for i in range(y_size)] for j in range(x_size)]
        plot_dict = {}
        for indexPlot in range(numberPlots):
            no_labels_yet = True
            ytag = f"{indexPlot}y"
            xtag = f"{indexPlot}x"
            indexTime = slice(np.shape(DATA_Y[ytag]["Values"])[1])
            print(f"indexTime: {indexTime}")
            indexFolder = DATA_Y[ytag]["IndexFolder"]
            IndexPlotType = DATA_Y[ytag]["IndexPlotType"]
            IndexPlotType_x = DATA_X[xtag]["IndexPlotType"]
            IndexPlotType = _convert_plot_type_index(IndexPlotType_x, IndexPlotType)
            index_plot_type_overall = IndexPlotType
            print(f"skip_folders: {skip_folders}")
            print(f"type(skip_folders): {type(skip_folders)}")
            for x in skip_folders:
                print(x)
            print(f"indexFolder: {indexFolder}")
            print(f"index_plot_type_overall: {index_plot_type_overall}")
            if skip_folders:
                if len(skip_folders) > 1:
                    if len(skip_folders[index_plot_type_overall]):
                        if indexFolder in skip_folders[index_plot_type_overall]:
                            continue
                else:
                    if indexFolder in skip_folders:
                        continue
            if not split_setups_in_panels:
                if IndexPlotType not in showOnOtherYAxis:
                    ax = ax1
                else:
                    ax2 = ax1.twinx()
            else:
                index_x = IndexPlotType % x_n
                if y_n == 1:
                    index_y = 0
                else:
                    index_y = IndexPlotType // x_n
                if single_xlabel or split_by_folder:
                    index_x = 0
                    index_y = plotting_order[IndexPlotType]
                    if split_by_folder:
                        index_x = (
                            indexFolder
                            if split_by_folder == "x"
                            else plotting_order[IndexPlotType]
                        )
                        index_y = (
                            plotting_order[IndexPlotType]
                            if split_by_folder == "x"
                            else indexFolder
                        )

                    index_plot_type_overall = IndexPlotType
                ax = Axes[index_x][index_y]
                if IndexPlotType in showOnOtherYAxis:
                    ax2 = Axes[index_x][index_y].twinx()
            print(
                f"index_x: {index_x}, index_y: {index_y}, ytag: {ytag}, xtag: {xtag}, numberPlots: {numberPlots}"
            )
            if grouping_linestyle is not None:
                linestyle = Fig.get_linestyles_grouped(grouping_linestyle)
            print(f"useTimeIndexAsColor: {useTimeIndexAsColor}")
            print(f"useFolderIndexAsColor: {useFolderIndexAsColor}")
            if useTimeIndexAsColor:
                colorIndex = indexTime
            elif useFolderIndexAsColor:
                colorIndex = indexFolder
            else:
                colorIndex = IndexPlotType
            if useTimeIndexAsTickStyle:
                tickIndex = indexTime
            elif useFolderIndexAsTickStyle:
                tickIndex = indexFolder
            else:
                tickIndex = IndexPlotType
            if useTimeIndexAsAlpha:
                alphaIndex = indexTime
            elif useFolderIndexAsAlpha:
                alphaIndex = indexFolder
            else:
                alphaIndex = IndexPlotType
            if scaleMarkerSizeTime:
                raise Exception("not correctly implemented yet!")
                markersizeUse = markersize * (indexTime + 1) / 2.0
            else:
                markersizeUse = markersize
            print(f"alphaIndex: {alphaIndex}")
            print(f"useFolderIndexAsAlpha: {useFolderIndexAsAlpha}")
            print(f"useTimeIndexAsAlpha: {useTimeIndexAsAlpha}")
            print(f"alpha: {alpha}")
            xmin = gridplot_setups.get_axes_limits(Xmin, IndexPlotType)
            xmax = gridplot_setups.get_axes_limits(Xmax, IndexPlotType)
            x = np.array(DATA_X[xtag]["Values"][0], dtype=np.float128)
            maskSort = np.argsort(x)
            y = np.array(DATA_Y[ytag]["Values"][0], dtype=np.float128)
            YShapeLen = len(np.shape(y))
            if YShapeLen == 3:
                useErrorbars = True
            else:
                useErrorbars = False
            if useErrorbars:
                y = y.T
                y = y[:, maskSort]
            else:
                try:
                    y = y[maskSort]
                except IndexError:
                    print(DATA_X[xtag])
                    print(DATA_Y[ytag])
                    print(f"label: {Ylabel[:] if type(Ylabel) == list else LABELRest[:]}")
                    raise Exception(
                        f"y: {np.shape(y)} and x: {np.shape(x)} wrong dimension: indexTime: {indexTime}, indexFolder: {indexFolder}, IndexPlotType: {IndexPlotType}, might be empty lines when creating data files!"
                    )
            x = x[maskSort]
            time_values = np.array(DATA_X[xtag]["times"], dtype=np.float128)
            time_values = time_values[maskSort]
            m = np.ones_like(time_values, dtype=bool)
            if time_values_min is not None:
                m = np.logical_and(time_values > time_values_min, m)
            if time_values_max is not None:
                m = np.logical_and(time_values < time_values_max, m)
            time_values = time_values[m]
            x = x[m]
            y = y[m]
            if DATA_X[xtag]["folder"] != DATA_Y[ytag]["folder"]:
                raise Exception("should be same folder")
            labelFolder = FoldersTranslate[DATA_X[xtag]["folder"]]
            labelPlotType = r"${}$".format(DATA_Y[ytag]["lables"])
            labelPlotTypeX = r"${}$".format(DATA_X[xtag]["lables"])
            marker = None
            ls = None
            if linestyle is None:
                marker = TickStyle[tickIndex]
            else:
                ls = linestyle[tickIndex]
            if marker in [
                "-",
                "--",
                "-.",
                ":",
                "None",
                " ",
                "",
                "solid",
                "dashed",
                "dashdot",
                "dotted",
            ]:
                ls = marker
                marker = None
            if marker in ["x"]:
                ls = "None"
            print(f"marker: {marker}, ls: {ls}, colorIndex: {colorIndex}")
            print(f"invert_finish_start: {invert_finish_start}")
            if grouping_alphas is not None:
                if not alpha_inverted:
                    alpha_current = Fig.get_alpha_grouped(
                        grouping_alphas, min=alpha_min
                    )[::-1][alphaIndex]
                else:
                    alpha_current = Fig.get_alpha_grouped(
                        grouping_alphas, min=alpha_min
                    )[alphaIndex]
            else:
                alpha_current = alpha[alphaIndex]
            if grouping_colors is not None:
                color_current = Fig.get_colors_grouped(
                    grouping_colors,
                    color_type="bright",
                    group_via_gradient=True,
                    invert_finish_start=invert_finish_start,
                    skip_colors=skip_colors,
                )[colorIndex]
            elif paper:
                color_current = Fig.getColor(
                    colorIndex,
                    0,
                    0,
                    8,
                    1,
                    1,
                    useGradient=False,
                    AlongColor=0,
                    AlongColorGradient=2,
                    color=None,
                    colorstart=None,
                    colorfinish=None,
                    color_type="bright",
                    skip_colors=skip_colors,
                )
            else:
                color_current = color[colorIndex]
            if plotting_style == "default":
                if useErrorbars:
                    if not useShadingErrors:
                        plot = ax.errorbar(
                            np.array(x) + IndexPlotType * offsetX,
                            np.average(y, axis=0),
                            yerr=abs(np.average(y, axis=0) - y[0]),
                            color=color_current,
                            alpha=alpha_current,
                            marker=marker,
                            ls=ls,
                            markersize=markersizeUse,
                            linewidth=linewidth,
                        )
                    else:
                        yAv = np.average(y, axis=0)
                        yErr = abs(np.average(y, axis=0) - y[0])
                        ax.fill_between(
                            x,
                            yAv - yErr,
                            yAv + yErr,
                            color=color_current,
                            alpha=alphaFill,
                        )
                        plot = ax.plot(
                            x,
                            yAv,
                            marker=marker,
                            ls=ls,
                            color=color_current,
                            alpha=alpha_current,
                            markersize=markersizeUse,
                            linewidth=linewidth,
                        )
                else:
                    x, y = do_box_smoothing(
                        x,
                        y,
                        box_smoothing,
                        IndexPlotType,
                        statistics_x=box_statistics,
                        statistics_y=box_statistics,
                        use_x_as_weights=box_smoothing_use_x_as_weights,
                    )
                    plot = ax.plot(
                        x,
                        y,
                        marker=marker,
                        ls=ls,
                        color=color_current,
                        alpha=alpha_current,
                        markersize=markersizeUse,
                        linewidth=linewidth,
                    )
                if plotMarkerAndLine:
                    plot = ax.plot(
                        x,
                        y,
                        "-",
                        color=color_current,
                        alpha=alpha_current,
                        markersize=1,
                        linewidth=linewidth,
                    )
            elif plotting_style == "histogram":
                print(
                    f"box_smoothing: {box_smoothing}, IndexPlotType: {IndexPlotType}, labelFolder: {labelFolder}"
                )
                x, y = do_box_smoothing(
                    x,
                    y,
                    box_smoothing,
                    IndexPlotType,
                    statistics_x="sum",
                    statistics_y=box_statistics,
                )
                histogram_weights = np.array(x, dtype=np.float64)
                if ignore_histogram_weights:
                    histogram_weights = None
                hist = Fig.getHistogram(
                    np.array(y, dtype=np.float64),
                    weights=histogram_weights,
                    binNumber=hist_bin_number,
                    binLog=logX,
                    binRange=[xmin, xmax],
                    fit=None,
                    plot=False,
                    normalize=normalize_histogram,
                    include_outlier_bins=histogram_include_outlier_bins,
                )
                plot = ax.bar(
                    hist["bins"],
                    hist["hist"],
                    width=np.diff(hist["bin_edges"]),
                    color=color_current,
                    edgecolor="black",
                    alpha=alpha_current,
                    linewidth=linewidth,
                )
                if histogram_plot_reduction:
                    if histogram_plot_reduction == "mean":
                        positions = DescrStatsW(
                            data=np.array(y, dtype=np.float64),
                            weights=histogram_weights,
                        ).mean
                    elif histogram_plot_reduction == "median":
                        positions = DescrStatsW(
                            data=np.array(y, dtype=np.float64),
                            weights=histogram_weights,
                        ).quantile(probs=np.array([0.5]), return_pandas=False)
                    else:
                        raise Exception(
                            f"histogram_plot_mean: {histogram_plot_reduction} not understood!"
                        )
                    print(f"histogram_plot_mean: positions: {positions}")
                    Fig.drawCompleteVerticalLines(
                        ax,
                        positions,
                        color="black",
                        alpha=1,
                        labels=None,
                        linestyles="--",
                        linewidth=linewidth,
                    )
            if index_plot_type_overall == 0:
                LABELFolder.append(labelFolder)
                PLOTSFolder.append(plot)
            if indexFolder == 0 or no_labels_yet:
                no_labels_yet = False
                LABELRest.append(labelPlotType)
                LABELRestX.append(labelPlotTypeX)
                PLOTSRest.append(plot)
                plot_dict[index_plot_type_overall] = plot
                plot_dict[f"{index_plot_type_overall}LabelX"] = labelPlotTypeX
                plot_dict[f"{index_plot_type_overall}LabelY"] = labelPlotType
            yy.append(y)
            xx.append(x)
            xs[index_x][index_y].append(x)
            ys[index_x][index_y].append(y)
            if show_reductions_x_min is not None:
                if len(show_reductions_x_min) == 1 and len(show_reductions) > 1:
                    show_reductions_x_min *= len(show_reductions)
            if show_reductions_x_max is not None:
                if len(show_reductions_x_max) == 1 and len(show_reductions) > 1:
                    show_reductions_x_max *= len(show_reductions)
            if show_reductions is not None:
                print(f"show_reductions_x_max: {show_reductions_x_max}")
                xmin_redu, xmax_redu = xmin, xmax
                if (
                    show_reductions_x_min is not None
                    and show_reductions_x_min[IndexPlotType] is not None
                ):
                    xmin_redu = show_reductions_x_min[IndexPlotType]
                if (
                    show_reductions_x_max is not None
                    and show_reductions_x_max[IndexPlotType] is not None
                ):
                    xmax_redu = show_reductions_x_max[IndexPlotType]
                reduction_value = get_reduction(
                    show_reductions[IndexPlotType], x, y, [xmin_redu, xmax_redu]
                )
                reduction_values[index_x][index_y].append(reduction_value)
                reduction_colors[index_x][index_y].append(color_current)
                reduction_alphas[index_x][index_y].append(alpha_current)
        if theory_plot_x is not None:
            assert len(theory_plot_x) == IndexPlotType + 1
            assert len(theory_plot_y) == IndexPlotType + 1
        y_label_legend = None
        if len(plotting_order) != len(np.unique(plotting_order)):
            _ylabels = Ylabel[:] if type(Ylabel) == list else LABELRest[:]
            y_label_legend = [
                [l for l, po in zip(_ylabels, plotting_order) if po == INDEX]
                if len(np.array(plotting_order)[np.array(plotting_order) == INDEX]) > 1
                else []
                for INDEX, ll in enumerate(_ylabels)
            ]
            y_label_indices = [
                [
                    _index_setup
                    for l, po, _index_setup in zip(
                        _ylabels, plotting_order, np.arange(len(plotting_order))
                    )
                    if po == INDEX
                ]
                if len(np.array(plotting_order)[np.array(plotting_order) == INDEX]) > 1
                else []
                for INDEX, ll in enumerate(_ylabels)
            ]
            print(f"y_label_legend: {y_label_legend}")
        for i in range(number_axes):
            if not split_setups_in_panels:
                number_axes = 1
                index_ylabel = index_setup = index_folder = 0
                if IndexPlotType not in showOnOtherYAxis:
                    ax = ax1
                else:
                    ax2 = ax1.twinx()
            else:
                index_x = i % x_n
                if y_n == 1:
                    index_y = 0
                else:
                    index_y = i // x_n
                if single_xlabel and split_by_folder is None:
                    index_x = 0
                    index_y = i
                ax = Axes[index_x][index_y]
                index_ylabel = index_setup = index_y
                index_folder = 0
                if not split_by_folder and split_setups_in_panels:
                    index_setup = i
                    index_folder = (
                        index_x
                        if split_by_folder == "x" or split_by_folder is None
                        else index_y
                    )
                    index_ylabel = i if split_setups_in_panels == True else index_setup
                    if split_by_folder:
                        index_ylabel = index_x if split_by_folder == "y" else index_y
                elif split_by_folder or split_setups_in_panels:
                    index_setup = (
                        index_y
                        if split_by_folder == "x"
                        or (split_by_folder is None and single_xlabel)
                        else index_x
                    )
                    index_folder = (
                        index_x
                        if split_by_folder == "x" or split_by_folder is None
                        else index_y
                    )
                    index_ylabel = i if split_setups_in_panels == True else index_setup
                    if split_by_folder:
                        index_ylabel = index_x if split_by_folder == "y" else index_y
            print(f"index_ylabel: {index_ylabel}")
            print(f"index_setup: {index_setup}")
            print(f"index_y: {index_y}")
            print(f"index_x: {index_x}")
            print(f"LABELRest: {LABELRest}")
            print(f"LABELRestX: {LABELRestX}")
            if theory_plot_x is not None:
                if theory_plot_x[index_setup] is not None:
                    x = gridplot_setups.get_theory_grid_variable(
                        theory_plot_x[index_setup]
                    )
                    y = gridplot_setups.get_theory_grid_variable(
                        theory_plot_y[index_setup]
                    )
                    ax.plot(x, y, "bx")
            index_xlabel = index_ylabel
            ymin = gridplot_setups.get_axes_limits(Ymin, index_setup)
            ymax = gridplot_setups.get_axes_limits(Ymax, index_setup)
            xmin = gridplot_setups.get_axes_limits(Xmin, index_setup)
            xmax = gridplot_setups.get_axes_limits(Xmax, index_setup)
            print(f"ymin: {ymin}")
            print(f"ymax: {ymax}")
            print(f"xmin: {xmin}")
            print(f"xmax: {xmax}")
            for v, c, a in zip(
                reduction_values[index_x][index_y],
                reduction_colors[index_x][index_y],
                reduction_alphas[index_x][index_y],
            ):
                length_reduction = Xmax * 0.08
                if paper:
                    length_reduction = 100
                if v is not None:
                    ax.hlines(
                        v,
                        xmax - length_reduction,
                        xmax,
                        color=c,
                        ls="--",
                        linewidth=0.75 * linewidth,
                        alpha=a,
                    )
            if logX:
                ax.set_xscale("log")
            if logY:
                ax.set_yscale("log")
            if LinFitEachTime:
                for index_fit in np.arange(0, indexTime + 1):
                    if Both and any(
                        [stuff in YVariables for stuff in DistanceVariables]
                    ):
                        yFit = np.array(yy)[:, index_fit, :, 0].flatten()
                        xFit = np.array(xx)[:, index_fit, :].flatten()
                    else:
                        yFit = np.array(yy)[:, index_fit, :].flatten()
                        xFit = np.array(xx)[:, index_fit, :].flatten()
                    paras, success = aux.fitFunction(xFit, yFit, initialGuess, fitfunc)
                    ax.plot(
                        XEq,
                        fitfunc(paras, XEq),
                        "--",
                        color=color[index_fit],
                        label="fit",
                        alpha=0.7,
                        markersize=markersize,
                    )

            if plotLineEqual:
                MINX = gridplot_setups.get_global_extrema(xx, xmin, max=False)
                MAXX = gridplot_setups.get_global_extrema(xx, xmax, max=True)
                MINY = gridplot_setups.get_global_extrema(yy, ymin, max=False)
                MAXY = gridplot_setups.get_global_extrema(yy, ymax, max=True)
                plot = ax.plot(
                    np.linspace(MINX * 0.5, MAXX * 1.5, 100),
                    np.linspace(MINX * 0.5, MAXX * 1.5, 100),
                    "--",
                    color="black",
                    label="equal",
                    alpha=0.3,
                    markersize=markersize,
                )
            labelleft = True
            labelbottom = True
            if single_y_tick_label and index_x != 0:
                labelleft = False
            if single_x_tick_label and index_y != y_n - 1:
                labelbottom = False
            if not logX:
                ax.ticklabel_format(style="sci", scilimits=(-3, 4), axis="x")
            if not logY:
                ax.ticklabel_format(style="sci", scilimits=(-3, 4), axis="y")
            Fig.set_tick_positions(
                ax,
                y_ticks_both=set_yticks_both,
                y_tick_labels_both=set_ytick_label_both,
                major_tick_length=tick_length_major,
                minor_tick_length=tick_length_minor,
                fontsize=fontsize,
                labelleft=labelleft,
                labelbottom=labelbottom,
            )
            if set_yticks_both_rightmost_plot and index_x == x_n - 1:
                Fig.set_tick_positions(
                    ax,
                    y_ticks_both=True,
                    y_tick_labels_both=set_ytick_label_both,
                    major_tick_length=tick_length_major,
                    minor_tick_length=tick_length_minor,
                    fontsize=fontsize,
                    labelleft=labelleft,
                    labelbottom=labelbottom,
                )
            if logX:
                ax.set_xscale("log")
            if logY:
                ax.set_yscale("log")
            if ymin is None and not split_setups_in_panels:
                if not logY:
                    ymin = np.min(aux.flatten(yy))
                else:
                    if (
                        np.size(
                            np.array(aux.flatten(yy))[np.array(aux.flatten(yy)) > 0]
                        )
                        > 0
                    ):
                        ymin = np.min(
                            np.array(aux.flatten(yy))[np.array(aux.flatten(yy)) > 0]
                        )
                        print(ymin)
                    if plotLineEqual and np.min(YEq) < ymin:
                        ymin = np.min(YEq)
                    if ymin is not None:
                        ymin = ymin - ymin * 0.01
            if ymax is None and not split_setups_in_panels:
                ymax = np.max(aux.flatten(yy))
                if plotLineEqual and np.max(YEq) > ymax:
                    ymax = np.max(YEq)
                ymax = ymax + ymax * 0.01
            if not single_xlabel or not single_x_tick_label or index_y == y_n - 1:
                if Xlabel is None:
                    ax.set_xlabel(LABELRestX[index_xlabel], fontsize=fontsize)
                elif aux.isIterable(Xlabel):
                    ax.set_xlabel(Xlabel[index_xlabel], fontsize=fontsize)
                else:
                    ax.set_xlabel(Xlabel, fontsize=fontsize)
            if not single_ylabel or index_x == 0:
                if split_setups_in_panels:
                    if type(Ylabel) == list:
                        ax.set_ylabel(Ylabel[index_ylabel], fontsize=fontsize)
                    else:
                        ax.set_ylabel(LABELRest[index_ylabel], fontsize=fontsize)
                else:
                    if Ylabel is not None:
                        ax.set_ylabel(Ylabel[0], fontsize=fontsize)
            if (indexFolder > 0 and plotLegendFolder and len(LABELFolder) > 1) or force_show_folder:
                bbox_to_anchor = None
                if legend_external == "folder":
                    bbox_to_anchor = (1, 0.5)
                if (
                    split_setups_in_panels
                    and split_by_folder is not None
                    and index_setup == 0
                ):
                    Fig.add_text_plot(
                        LABELFolder[index_folder], position="upper left", axes=ax
                    )
                elif ((i == 0 and legend_folder_axes is None) or not split_setups_in_panels) or legend_folder_axes == [index_x, index_y]:
                    if split_setups_in_panels:
                        legend1 = ax.legend(
                            [x[0] for x in PLOTSFolder],
                            sm.align_texts(LABELFolder, align_text),
                            loc=legend_folder_position,
                            fontsize=fontsize,
                            framealpha=framealpha,
                            fancybox=True,
                            bbox_to_anchor=bbox_to_anchor,
                            ncol=legendFolderNcol,
                        )
                    else:
                        legend1 = plt.legend(
                            [x[0] for x in PLOTSFolder],
                            sm.align_texts(LABELFolder, align_text),
                            loc=legend_folder_position,
                            fontsize=fontsize,
                            framealpha=framealpha,
                            fancybox=True,
                            bbox_to_anchor=bbox_to_anchor,
                            ncol=legendFolderNcol,
                        )
                        ax.add_artist(legend1)

            if y_label_legend is not None:
                print(f"plot_dict: {plot_dict}")
                # exit()
                if y_label_legend[index_ylabel]:
                    _labels = "LabelY"
                    print(f"y_label_indices: {y_label_indices}")
                    print(f"y_label_legend: {y_label_legend}")
                    print(
                        f"y_label_legend[index_ylabel]: {y_label_legend[index_ylabel]}"
                    )
                    print(
                        [
                            plot_dict[f"{_index}{_labels}"][0]
                            for _index in y_label_indices[index_ylabel]
                        ]
                    )
                    print(f"plot_dict: {plot_dict}")
                    legend1 = ax.legend(
                        [
                            plot_dict[_index][0]
                            for _index in y_label_indices[index_ylabel]
                        ],
                        y_label_legend[index_ylabel],
                        loc=legend_variable_position,
                        fontsize=fontsize,
                        framealpha=framealpha,
                        fancybox=True,
                    )
            if positionsVertical is not None:
                for pos in positionsVertical[index_setup]:
                    Fig.drawCompleteVerticalLines(
                        ax, pos, color=colorsVertical, alpha=alphaVertical
                    )
            for pos in positionsHorizontal:
                Fig.drawCompleteHorizontalLines(
                    ax, pos, color=colorsHorizontal, alpha=alphaHorizontal
                )
            if positionsVerticalShading is not None:
                for pos in positionsVerticalShading[index_setup]:
                    Fig.drawCompleteVerticalShading(
                        ax,
                        pos,
                        color=colorsVerticalShading,
                        alpha=alphaVerticalShading,
                    )
            if plotText:
                Fig.plotText(
                    ax,
                    positionsText,
                    Texts,
                    fontsize=fontsize,
                    color=colorText,
                    horizontalalignment=horizontalalignment,
                )
            if not xignoreLinTicks and xmin is not None and xmax is not None:
                # if xticks is None:
                print("setting own xticks!")
                if not logX and use_own_x_ticks:
                    xticks = Fig.setLinTicksAlt(
                        xmin, xmax, numberMax=numberMaxLinXTicks
                    )
                else:
                    xticks = Fig.setLogTicks(xmin, xmax, numberMax=numberMaxLinXTicks)
                ax.xaxis.set_ticks(xticks)
            if not yignoreLinTicks and ymin is not None and ymax is not None:
                # if yticks is None:
                print("setting own yticks!")
                if not logY:
                    yticks = Fig.setLinTicksAlt(
                        ymin, ymax, numberMax=numberMaxLinYTicks
                    )
                else:
                    yticks = Fig.setLogTicks(
                        ymin,
                        ymax,
                        numberMax=numberMaxLinYTicks,
                        returnOnlyMajorTicks=True,
                    )
                ax.yaxis.set_ticks(yticks)
            if minorXTicks:
                # minorLocator = MultipleLocator(spacingMinorXTicks)
                minorLocator = AutoMinorLocator()
                if logX:
                    # minorLocator = LogLocator(base=10)
                    if reduce_minor_x_ticks:
                        minorLocator = LogLocator(
                            base=10.0, subs=(0.2, 0.4, 0.6, 0.8), numticks=12
                        )
                    else:
                        minorLocator = LogLocator(
                            base=10.0, subs=np.arange(1, 10) / 10  # , numticks=12
                        )
                ax.xaxis.set_minor_locator(minorLocator)
            if minorYTicks:
                minorLocator = AutoMinorLocator()
                if logY:
                    if reduce_minor_y_ticks:
                        minorLocator = LogLocator(
                            base=10.0, subs=(0.2, 0.4, 0.6, 0.8), numticks=12
                        )
                    else:
                        minorLocator = LogLocator(
                            base=10.0, subs=np.arange(1, 10) / 10  # , numticks=12
                        )
                    # minorLocator = MultipleLocator(spacingMinorYTicks)
                    # minorLocator = LogLocator(base=10)
                ax.yaxis.set_minor_locator(minorLocator)
            # print(ymin, ymax)
            ax.set_ylim([ymin, ymax])
            ax.set_xlim([xmin, xmax])
        aux.create_dirs_for_file(name)
        aux.checkExistence_delete_file(name)
        print(".. saving %s" % name)
        fig.savefig(name, dpi=400, bbox_inches="tight")  # , rasterized=True)
        plt.close(fig)
print(f"finished plots at {datetime.datetime.now()}")
print(
    f"plotting took: {'%i'%((time.time()-start_time)//60)} min  {'%.2f'%((time.time()-start_time)%60)} s"
)
