import numpy as np
import multiprocessing as mp
import time as TimeModule
import sys
import pandas as pd
import auxiliary_functions as aux
import itertools
import json
import traceback
from multiprocessing.pool import Pool
import multiprocessing

# https://stackoverflow.com/questions/6728236/exception-thrown-in-multiprocessing-pool-not-detected
# Shortcut to multiprocessing's logger
def error(msg, *args):
    return multiprocessing.get_logger().error(msg, *args)


class LogExceptions(object):
    def __init__(self, callable):
        self.__callable = callable

    def __call__(self, *args, **kwargs):
        try:
            result = self.__callable(*args, **kwargs)

        except Exception as e:
            # Here we add some debugging help. If multiprocessing's
            # debugging is on, it will arrange to log the traceback
            error(traceback.format_exc())
            # Re-raise the original exception so the Pool worker can
            # clean up
            raise

        # It was fine, give a normal answer
        return result


class LoggingPool(Pool):
    def apply_async(self, func, args=(), kwds={}, callback=None):
        return Pool.apply_async(self, LogExceptions(func), args, kwds, callback)


"""
working simple example:
from Multithread import initializeBigData

def calculate(x,b,c):
	return np.sum(x*b+c)

b = 2.
c = 5.	
args = tuple((b, c))
X = np.random.rand(100,10)
global bd

bd = initializeBigData(calculate, np.arange(100), X, None,None, *args)
results = bd.do_multi(2)
"""

_tm = 0


def stopwatch(msg=""):
    tm = TimeModule.time()
    global _tm
    if _tm == 0:
        _tm = tm
        return
    print("%s: %.2f seconds" % (msg, tm - _tm))
    _tm = tm


class BigData:
    def __init__(
        self,
        function,
        allIndices,
        DataNeedPart,
        NNeedPart,
        names,
        selector,
        *args,
        trivial_distribution=False,
    ):
        """
		set names to none otherwise name entered as first element in function
		selector determines how big data split -> numpy, pandas possible
		"""
        self.allIndices = allIndices
        self.DataNeedPart = DataNeedPart
        self.NNeedPart = NNeedPart
        self.args = args
        self.function = function
        self.names = names
        self.selector = selector  # pandas
        self.trivial_distribution = trivial_distribution

    def do_chunk_wrapper(self, k_start, k_end):
        return self.do_chunk_of_chunks(k_start, k_end)

    def do_chunk_of_chunks(self, k_start, k_end):
        if self.selector == "numpy":
            if self.names is not None:
                s = [
                    self.function(
                        self.names[k],
                        *(
                            dnp[tuple(self.allIndices[k])]
                            for dnp in [
                                self.DataNeedPart[i] for i in range(self.NNeedPart)
                            ]
                        ),
                        *self.args,
                    )
                    for k in range(k_start, k_end)
                ]
            elif self.trivial_distribution:
                raise Exception("needs to be fixed!")
                s = [
                    self.function(*(self.DataNeedPart[self.allIndices[k]]), *self.args,)
                    for k in range(k_start, k_end)
                ]
            else:
                print("list(range(k_start, k_end))")
                print(list(range(k_start, k_end)))
                print("self.DataNeedPart[0][self.allIndices[0]]")
                print(self.DataNeedPart[0][self.allIndices[0]])
                s = [
                    self.function(
                        *(
                            dnp[self.allIndices[k]]
                            for dnp in [
                                self.DataNeedPart[i] for i in range(self.NNeedPart)
                            ]
                        ),
                        *self.args,
                    )
                    for k in range(k_start, k_end)
                ]
        elif self.selector == "pandas":
            if self.names is not None:
                s = [
                    self.function(
                        self.names[k],
                        *(
                            dnp.loc[self.allIndices[k]]
                            for dnp in [
                                self.DataNeedPart[i] for i in range(self.NNeedPart)
                            ]
                        ),
                        *self.args,
                    )
                    for k in range(k_start, k_end)
                ]
            else:
                s = [
                    self.function(
                        *(
                            dnp.loc[self.allIndices[k]]
                            for dnp in [
                                self.DataNeedPart[i] for i in range(self.NNeedPart)
                            ]
                        ),
                        *self.args,
                    )
                    for k in range(k_start, k_end)
                ]
        else:
            raise Exception(f"selector: {self.selector} not included yet!")
        sys.stderr.write(".")
        return s

    def do_multi(self, numproc):
        procs = []
        stopwatch("\nPool setup")
        n_jobs = np.shape(self.allIndices)[0]
        if n_jobs < numproc:
            numproc = np.shape(self.allIndices)[0]
        if numproc < 1:
            numproc = 1
        # if np.shape(self.allIndices)[0] == numproc:
        #     jobs = list(range(np.shape(self.allIndices)[0]))
        #     print("here")
        # else:
        pool = LoggingPool(processes=numproc)

        v = np.arange(1, n_jobs % numproc + 1)[::-1]
        rest = np.zeros(numproc + 1)
        rest[0:n_jobs % numproc] = v
        jobs = np.array(np.linspace(0, n_jobs - n_jobs % numproc, numproc + 1) + rest[::-1], dtype=int)
        # jobs = list(map(int, np.linspace(0, np.shape(self.allIndices)[0], numproc+1)))
        joblist = range(len(jobs) - 1) if len(jobs) > 1 else [0]
        if len(jobs) == 1:
            jobs += [1]
        print(f"numproc: {numproc}")
        print(f"joblist: {list(joblist)}")
        print(f"jobs: {jobs}")
        for i in joblist:
            jobs_start, jobs_end = jobs[i : i + 2]
        # jobs = list(map(int, np.linspace(0, np.shape(self.allIndices)[0], numproc+1)))
        # pool = LoggingPool(processes=numproc)
        # joblist = range(len(jobs) - 1) if len(jobs) > 1 else [0]
        # if len(jobs) == 1:
        #     jobs += [1]
        # print(f"numproc: {numproc}")
        # print(f"joblist: {list(joblist)}")
        # print(f"jobs: {jobs}")
        # # exit()
        # for i in joblist:
        #     jobs_start, jobs_end = jobs[i : i + 2]
            p = pool.apply_async(self.do_chunk_wrapper, (jobs_start, jobs_end))
            procs.append(p)
        stopwatch("Jobs queued (%d processes)" % numproc)
        results = []
        for k, p in enumerate(procs):
            results += p.get()  # timeout=5) # timeout allows ctrl-C interrupt
            if k == 0:
                stopwatch("\nFirst get() done")
        stopwatch("Jobs done")
        pool.close()
        pool.join()
        return results


class JobsSheduler(object):
    """
		df: pass df of jobs which corresponds to extended pandas data frame class
		groupy: group jobs by certain column
		zippedType: 'list', 'single'
	"""

    def __init__(self, data=None, columns=None):
        # allows to see all columns on multiple lines in console
        pd.set_option("display.max_columns", 40)
        # pd.set_option('display.width', 1000)
        if data is not None and columns is not None:
            self.df = pd.DataFrame(data, columns=columns)
        else:
            self.df = pd.DataFrame()

    def hashAll(self, table=None):
        table, self.df = self.df.job.hashAll(table)
        return table

    def getJobs(
        self, sortBy, expandByDtypesAfter, expandBy, sortCheckUniqueness, table
    ):
        self.jobs = self.df.copy()
        print("unhash")
        self.jobs.job.unhashByColumn(sortBy, table)
        dtypes = self.jobs.dtypes
        # for col in relevantSorter: js.df[col] = js.df[col].map(lambda x: js.df.job.convertToValue(x, table))
        print("expand")
        self.jobs = self.jobs.job.expandBy(expandBy, expandByDtypesAfter)
        print("sortby")
        self.jobs = self.jobs.job.sortBy(sortBy, sortCheckUniqueness)
        print("map")
        for col, dtyp in zip(expandBy, expandByDtypesAfter):
            self.jobs[col] = self.jobs[col].map(lambda x: dtyp(x))
        print("hash")
        self.jobs.job.hashByColumn(sortBy, table=table)
        self.jobs = self.jobs.astype(np.int64)


@pd.api.extensions.register_dataframe_accessor("job")
class JobAccessor:
    def __init__(self, pandas_obj):
        self._obj = pandas_obj

    def __add__(self, other):
        if not self._obj.size:
            return other._obj
        elif not other._obj.size:
            return self._obj
        return pd.concat(
            [
                pd.DataFrame(
                    np.repeat(other._obj.values, self._obj.shape[0], axis=0),
                    columns=other._obj.columns,
                ),
                pd.concat([self._obj] * other._obj.shape[0], ignore_index=1),
            ],
            axis=1,
        )

    def add(self, other):
        return self.__add__(
            other
        )  # pd.concat([pd.DataFrame(np.repeat(other.values,self._obj.shape[0],axis=0), columns=other.columns),pd.concat([self._obj]*other.shape[0], ignore_index=1)], axis=1)

    def addZipped(self, names, zipped, types, toEmpty=False):
        lll = []
        for zz in zip(*zipped):
            NvaluesBlock = 0
            i = 0
            ll = []
            for z, typ in zip(zz, types):
                if typ == "list":
                    if i != 0:
                        if NvaluesBlock != len(z):
                            raise Exception(
                                "blocks of listed data need to have same size"
                            )
                    i += 1
                    NvaluesBlock = len(z)
            for z, typ in zip(zz, types):
                if typ == "list":
                    ll.append(z)
                elif typ == "single":
                    ll.append([z] * NvaluesBlock)
            lll.extend(list(map(list, zip(*ll))))  # transpose of list
        other = pd.DataFrame(np.asarray(lll), columns=names)
        if not toEmpty:
            return self.__add__(other.job)
        else:
            return other

    def convertToHash(self, x, table):
        tag = hash(json.dumps(x, sort_keys=True, default=str))
        if tag not in table.keys():
            table[tag] = x
        return tag

    def convertToValue(self, x, table):
        return table[x]

    def hashByColumn(self, columns, table=None):
        # inplace
        if table is None:
            table = {}
        for col in columns:
            self._obj[col] = self._obj[col].map(lambda x: self.convertToHash(x, table))
        return table

    def unhashByColumn(self, columns, table=None):
        # inplace
        if table is None:
            table = {}
        print("unhash")
        for col in columns:
            self._obj[col] = self._obj[col].map(lambda x: self.convertToValue(x, table))
        return table

    def hashAll(self, table=None):
        if table is None:
            table = {}
        return table, self._obj.applymap(lambda x: self.convertToHash(x, table))

    def expandBy(self, expandBy, expandByDtypesAfter):
        if not aux.isIterable(expandBy):
            expandBy = [expandBy]
        dtypesOrig = self._obj.dtypes
        names = self._obj.columns.to_list()
        zipped = self._obj.to_numpy().T
        types = ["single" if x not in expandBy else "list" for x in names]
        df = self.addZipped(names, zipped, types, toEmpty=True)
        for ex, ty in zip(expandBy, expandByDtypesAfter):
            df[ex] = df[ex].astype(ty)
        return df

    def sortBy(self, columns, checkUniqueness=None, inplace=False):
        """
			empty lines have np.nan/None
			sort by specifiec column names
			empty entries in any column => -1
		"""
        if not inplace:
            df = self._obj.copy()
        else:
            df = self._obj
        df["JobIndex"] = -2

        def emptyLists(row, relevantRows):
            return row.isnull().values.any()

        # df['NotFoundArrayValues'] = np.nan

        # self.jobs = self._obj['NotFoundArrayValues'].astype(np.int64)
        df.loc[df.apply(lambda row: emptyLists(row, columns), axis=1), "JobIndex"] = -1
        # exclude nan rows
        df = df.loc[df["JobIndex"] != -1]
        uq = []
        print("find unique")
        for col in columns:
            uq.append(df[col].unique())
        combs = list(itertools.product(*uq))
        index = 1

        def compare(row, columns, combs):
            for index, cm in enumerate(combs):
                if all([row[x] == y for x, y in zip(columns, cm)]):
                    return index
            return row["JobIndex"]

        print("compare")
        print(f"found shape combs: {np.shape(combs)}")

        df["JobIndex"] = df.groupby(columns).ngroup()
        # df['JobIndex'] = df.apply(lambda x: compare(x, columns, combs), axis=1)
        # exclude duplicates
        print("remove duplicated")
        df = df[~df.duplicated(columns + checkUniqueness, keep="first")]
        return df

    def chunkBy(self, columns):
        if not aux.isIterable(columns):
            columns = [columns]
        # define numpy functions for encoding problematic types: list and function/callable
        def checkForTypes(x):
            return callable(x) or type(x) == list

        vfunc = np.vectorize(checkForTypes)
        strfunc = np.vectorize(lambda x: str(x))

        # generate encoded dataframe
        values = self._obj.to_numpy()
        mask = vfunc(values)
        dic = {}
        for val in values[mask]:
            dic[str(val)] = val
        values[mask] = strfunc(values[mask])
        df = pd.DataFrame(values, columns=self._obj.columns)

        # find unique values in all relevant columns
        uq = []
        for col in columns:
            uq.append(df[col].unique())
        combs = list(itertools.product(*uq))

        # decoder
        def decode(s, dic):
            if s in dic.keys():
                return dic[s]
            else:
                return s

        # get decoded chunks
        chunks = []
        for c in combs:
            query = [
                f'{col}=="{val}"' if type(val) == str else f"{col} == {val}"
                for col, val in zip(columns, c)
            ]
            query = " & ".join(query)
            qq = df.query(query, parser="pandas", engine="python")
            if qq.shape[0] > 0:
                data = qq.to_numpy().tolist()
                sublist = []
                for da in data:
                    sublist.append([decode(x, dic) for x in da])
                chunks.append(sublist)
        return chunks


if __name__ == "__main__":
    pass
    # jj1 = pd.DataFrame(np.arange(6).reshape(3, 2), columns=["second", "third"])
    # jj2 = pd.DataFrame()  # np.arange(3).reshape(3,1), columns=['fourth'])
    # print(jj2)
    # print(jj2.job + jj1.job)
    # jj3 = pd.DataFrame()
    #
    # def f1():
    #     return 132
    #
    # x = [1, 2, 3]
    # t = [[3], 5, 6]
    # y = [[0, 5, 2, 1], [0, f1, 2], [5, 8]]
    # z = [[-0, -5, -2, -1], [-0, -1, -2], [-5, -8]]
    # print(jj1)
    # # x = pd.DataFrame(ar([ar(x),ar(t),ar(y)]).T,columns=['x','t','y'])
    # # x = [1,2,['1','2','3'],[1,2,3]]
    # TYPES = ["single", "single", "list", "list"]
    # names = ["x", "y", "z", "a"]
    # zipped = (x, t, y, z)
    # jj3 = jj3.job.addZipped(names, zipped, TYPES)
    # print(jj3)
    # lll = []
    # for zz in zip(*zipped):
    # 	NvaluesBlock = 0
    # 	i = 0
    # 	ll = []
    # 	for z,typ in zip(zz,TYPES):
    # 		if typ == 'list':
    # 			if i != 0:
    # 				if NvaluesBlock != len(z):
    # 					raise Exception('blocks of listed data need to have same size')
    # 			i += 1
    # 			NvaluesBlock = len(z)
    # 	for z,typ in zip(zz,TYPES):
    # 		if typ == 'list':
    # 			ll.append(z)
    # 		elif typ == 'single':
    # 			ll.append([z]*NvaluesBlock)
    # 	print(list(map(list, zip(*ll))))
    # 	lll.extend(list(map(list, zip(*ll))))
    # lll = np.asarray(lll)
    # print(lll)
    # print(list(map(list, zip(*lll))))
    # print(np.repeat(np.array([x,t,y]).T,2))
    # print(x)
    # print(x.job+jj1.job)
