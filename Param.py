"""
collection of mathematical / physical constants, conversion factors and few global parameters at end
"""
M_PI = 3.14159265358979323846
GAMMA = 5.0 / 3.0
GAMMA_MINUS1 = GAMMA - 1.0
BOLTZMANN_cgs = 1.38064852e-16  # erg/K
BOLTZMANN_eV = 8.6173303e-5  # eV/K
BOLTZMANN_keV = 8.6173303e-8  # keV/K
PROTONMASS_cgs = PROTONMASS = 1.6726231e-24  # g
ATOMICUNIT_cgs = ATOMICUNIT = 1.6605e-24 #g
HYDROGENMASS_cgs = HYDROGENMASS = 1.008  * ATOMICUNIT # g
BOLTZMANN_in_eV_per_K = 8.61733034e-5  # eV/K
GRAVITATIONALCONSTANT_cgs = 6.67259e-8  # cm^3 g^-1 s^-2
THOMSONCROSSSECTION_cgs = 6.6524587158e-25  # cm^2
SPEEDOFLIGHT_cgs = 2.99792458e10  # cm/s
ElECTRONMASS_cgs = 9.10938356e-28  # g
ElECTRONMASS_in_keV = 511.0  # keV
ELECTRONCHARGE_cgs = 4.80320425e-10 # in statcoulombs
PLANCKSCONSTANT_cgs = 6.6260755e-27  # ergs s
TEMPERATURECMB_K = 2.725
SOLARMASS_in_g = 1.988435e33
PC_in_cm = 3.086e18
h = 0.67
# assuming total ionisation and X = 0.76, Y = 0.24, Z = 0
# yhelium = (1.0 - 0.76) / (4.0 * 0.76)
HYDROGEN_MASSFRAC = 0.76
meanmolecularweight = mu = 4.0 / (
    5.0 * HYDROGEN_MASSFRAC + 3.0
)  # 0.588 #for fully ionized gas, up to 1.22 not ionized
ElectronProtonFraction = (
    ne_fac
) = 0.88  # for H mass fraction 0.76, Y helium mass fraction 0.24, e_pro_p=(X+1)/2 electrons per protons
UnitLength_in_cm = 3.085678e24
MegaParsec_in_cm = 3.085678e24
KiloParsec_in_cm = 3.085678e21
UnitMass_in_g = 1.989e43
UnitVelocity_in_cm_per_s = 1e5
UnitkeV_in_erg = 1.6021766e-9
erg_in_keV = 1 / UnitkeV_in_erg
SEC_PER_MEGAYEAR = 3.15576e13
SEC_PER_YEAR = 3.15576e7
HubbleParam = 0.67
CRITICAL_DENSITY_UNIVERSE_cgs = 8.5e-30  # for h=0.674
SolarMassPerYearC2_to_Erg = (
    SPEEDOFLIGHT_cgs ** 2 * SOLARMASS_in_g / (SEC_PER_MEGAYEAR / 1e6)
)
# code units conversion
UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s
UnitTime_in_Megayears = UnitTime_in_s / SEC_PER_MEGAYEAR
UnitDensity_in_cgs = UnitMass_in_g / (UnitLength_in_cm ** 3)
UnitPressure_in_cgs = UnitMass_in_g / UnitLength_in_cm / (UnitTime_in_s ** 2)
UnitCoolingRate_in_cgs = UnitPressure_in_cgs / UnitTime_in_s
UnitEnergy_in_cgs = UnitMass_in_g * (UnitLength_in_cm ** 2) / (UnitTime_in_s ** 2)
UnitSpecificEnergy_in_cgs = UnitVelocity_in_cm_per_s * UnitVelocity_in_cm_per_s

G = 43.0187
HUBBLE = 100
HUBBLECONSTANT100_cgs = 100 * 1e5 / MegaParsec_in_cm

NFW_C = 5.87
NFW_M200 = 1.0e4
NFW_Eps = 0.001
NFW_DARKFRACTION = 1.0

NEED_ONLY_ONE_SNAPSHOT = ["JetEnergy", "JetActivity"]
MAXSEARCHRADIUS = 50
