import sys

sys.path.insert(0, "../analyze_clusternfw")
import matplotlib as mpl
import time
import argparse, argparse_helpers
import script_helpers
import datetime

mpl.use("Agg")
import FigureMove as Fig
import numpy as np
import os.path
import Multithread as mt

start_time = time.time()
FieldsToLoad = [
    "pos",
    "rho",
    "vol",
    "mass",
    "jetr",
    "vel",
    "u",
    "bfld",
    "Vorticity",
    "cren",
    "CRPressureGradient",
]
Test = False

fontsize = 12
TickStyle = ["-", "x", "--", "o", "<", "p"]
colors = [
    "#c2473e",
    "#b4ccdb",
    "#daa520",
    "#5eba7b",
    "#60007a",
    "#90481b",
    "#e5a4ad",
    "#cfa5df",
    "#2d74b2",
    "#095115",
]

default_folder = "_M15mhdcrsLL"
default_numbers = [0, 100, 200, 300, 400, 500, 600]
if os.path.exists("/home/kristian/Analysis/") or os.path.exists(
    "/home/kristian/analysis/"
):
    print("using test!")
    Test = True

if Test:
    default_folder = "_testp"
    default_numbers = [0, 400, 800]

parser = argparse.ArgumentParser(
    description="phase diagrams plots of different variables!",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    f"--numbers", type=argparse_helpers.fancy_eval, nargs="+", default=[default_numbers]
)
parser.add_argument(
    f"--functions", type=argparse_helpers.fancy_eval, nargs="+", default="default"
)
parser.add_argument(
    f"--folder", type=argparse_helpers.fancy_eval, default=[default_folder]
)
parser.add_argument(
    f"--setup",
    type=argparse_helpers.fancy_eval,
    nargs="+",
    default=["tempne", "coolradius", "tempnecenter20"],
)
parser.add_argument(f"--numbers_min", type=argparse_helpers.fancy_eval, default=False)
parser.add_argument(f"--numbers_max", type=argparse_helpers.fancy_eval, default=False)
parser.add_argument(f"--numbers_n", type=argparse_helpers.fancy_eval, default=8)
parser.add_argument(f"--threads", type=argparse_helpers.fancy_eval, nargs=1, default=8)
parser.add_argument(f"--numbers_step", type=argparse_helpers.fancy_eval, default=False)
parser.add_argument(f"--xmin", type=argparse_helpers.fancy_eval, default=None)
parser.add_argument(f"--xmax", type=argparse_helpers.fancy_eval, default=None)
parser.add_argument(f"--ymin", type=argparse_helpers.fancy_eval, default=None)
parser.add_argument(f"--ymax", type=argparse_helpers.fancy_eval, default=None)
parser.add_argument(f"--subfolder", type=argparse_helpers.fancy_eval, default="")
parser.add_argument(f"--paper", action="store_true", default=False)
parser.add_argument(f"--fontsize", type=argparse_helpers.fancy_eval, default=12.9)
parser.add_argument(
    f"--axes_text", type=argparse_helpers.fancy_eval, nargs="+", default=[None],
)
parser.add_argument(
    f"--axes_text_position", type=argparse_helpers.fancy_eval, default="time"
)
input = parser.parse_args().__dict__.copy()
input = {k: v for k, v in input.items() if v is not None}
script_helpers.get_parameters_from_yaml(input)
print(input)
Folders = input["folder"]
Roots = input["root"] if "root" in input.keys() else [""] * len(Folders)
NameOfSimSave = (
    input["name_sim_save"]
    if "name_sim_save" in input.keys()
    else ["test"] * len(Folders)
)
NameOfSim = input["name_sim"] if "name_sim" in input.keys() else ["test"] * len(Folders)
SETUPS = input["setup"]
functions = input["functions"][0]
xmin = input["xmin"][0] if "xmin" in input.keys() else None
xmax = input["xmax"][0] if "xmax" in input.keys() else None
ymin = input["ymin"][0] if "ymin" in input.keys() else None
ymax = input["ymax"][0] if "ymax" in input.keys() else None
numbers = input["numbers"]
threads = input["threads"][0]
subfolder = input["subfolder"][0]
numbers_min = input["numbers_min"][0]
numbers_max = input["numbers_max"][0]
numbers_n = input["numbers_n"][0]
numbers_step = input["numbers_step"][0]
paper = input["paper"][0]
fontsize = input["fontsize"][0]
axes_text = input["axes_text"][0]
axes_text_position = input["axes_text_position"][0]
print(numbers_min, numbers_max)
print(bool(numbers_min), bool(numbers_max))
print(numbers)
if (numbers_min or numbers_min == 0) and numbers_max != False:
    numbers = np.linspace(numbers_min, numbers_max, numbers_n)
if (numbers_min or numbers_min == 0) and numbers_step != False and numbers_max != False:
    numbers = np.arange(numbers_min, numbers_max, numbers_step)
print(numbers)
overflowBinX = True
overflowBinY = True
time_format = None
cmap = "tab20c"
if paper:
    time_format = "integer10"
    overflowBinX = False
    overflowBinY = False
    cmap = "plasma_r"
    cmap = "cividis_r"
    cmap = "viridis_r"
    # fontsize = 15

useTime = True
differenceTime = 10

# if numbers == "default":
#     numbers = np.arange(0, 2000, 200)
# if numbers == "default" and (folder == "test" or folder == "perseusjets"):
#     numbers = [0, 40, 80]


colormap_vmax = 1e0
colormap_vmin = 1e-4
Weights = "Mass"
normaliseWeights = True
colormap_log = True
method = "sum"
select = None
sfr_weight = False
input_list = []
for setup in SETUPS:
    func = []
    xlabel = None
    ylabel = None
    colorbarlabel = None
    if functions != "default":
        func += functions
    if setup == "tempne":
        x_variable = "ElectronNumberDensity"
        y_variable = "Temperature"
        func += ["eosfitTne", "pressure1M10", "pressure1M11"]
    elif setup == "tempnecenter20":
        x_variable = "ElectronNumberDensity"
        y_variable = "Temperature"
        func += ["eosfitTne", "pressure1M10", "pressure1M11"]
        select = "DistanceToCenterInKpc<20"
    elif setup == "tempnecenterbh":
        Weights = "MassInSolar"  # None
        method = "sum"  # "sum"
        xmin = 1e-4
        xmax = 1e1
        ymin = 1e5
        ymax = 1e11
        colormap_vmax = 1e10
        colormap_vmin = 1e2
        normaliseWeights = False
        x_variable = "ElectronNumberDensity"
        y_variable = "Temperature"
        select = "DistanceToCenterInKpc<BHhsml"
        func += ["eosfitTne", "pressure1M9", "pressure1M10", "pressure1M11"]
    elif setup == "tempmasscenter40":
        x_variable = "MassInSolar"
        y_variable = "Temperature"
        func += ["massresolution"]
        select = "DistanceToCenterInKpc<40"
    elif setup == "tempnecenter40bondi":
        x_variable = "ElectronNumberDensity"
        y_variable = "Temperature"
        func += ["eosfitTne", "pressure1M10", "pressure1M11"]
        method = "median"
        normaliseWeights = False
        Weights = "BondiMdot"
        select = "DistanceToCenterInKpc<40"
        colormap_vmax = 1e4
        colormap_vmin = 1e-5
    elif setup == "tempmasscenter40tracer":
        x_variable = "MassInSolar"
        y_variable = "Temperature"
        method = "median"
        normaliseWeights = False
        Weights = "JetTracer"
        func += ["massresolution"]
        select = "DistanceToCenterInKpc<40"
    elif setup == "temppressurecenter40":
        x_variable = "ThermalPressure"
        y_variable = "Temperature"
        func += ["thermalpressures"]
        select = "DistanceToCenterInKpc<40"
    elif setup == "temppressurecenter40tracer":
        x_variable = "ThermalPressure"
        y_variable = "Temperature"
        func += ["thermalpressures"]
        method = "median"
        normaliseWeights = False
        Weights = "JetTracer"
        select = "DistanceToCenterInKpc<40"
    elif setup == "tempnecenter40":
        x_variable = "ElectronNumberDensity"
        y_variable = "Temperature"
        func += ["eosfitTne", "pressure1M10", "pressure1M11"]
        select = "DistanceToCenterInKpc<40"
        xmin = 1e-4
        xmax = 1e1
        ymin = 1e5
        ymax = 1e11
        Weights = "MassInSolar"  # None
        method = "sum"  # "sum"
        colormap_vmax = 1e10
        colormap_vmin = 1e2
        normaliseWeights = False
    elif setup == "tempnecenter100":
        x_variable = "ElectronNumberDensity"
        y_variable = "Temperature"
        func += ["eosfitTne", "pressure1M9", "pressure1M10"]
        select = "DistanceToCenterInKpc<100"
        xmin = 8e-5
        xmax = 5e1
        ymin = 1e4
        ymax = 1e11
        Weights = "MassInSolar"  # None
        method = "sum"  # "sum"
        colormap_vmax = 1e10
        colormap_vmin = 1e2
        normaliseWeights = False
        xlabel = "$n_\\mathrm{e}\\ [\\mathrm{cm}^{-3}]$"
        ylabel = "$T\\ [\\mathrm{K}]$"
        colorbarlabel = "$M\\,[\\mathrm{M}_\\odot]$"
    elif setup == "tempnecenter100tcool10":
        x_variable = "ElectronNumberDensity"
        y_variable = "Temperature"
        func += ["eosfitTne", "pressure1M9", "pressure1M10"]
        select = "DistanceToCenterInKpc<100"  # "AND" "StarFormationRate==0" "AND" "CoolingTimescaleInMyr<10"
        xmin = 8e-5
        xmax = 5e1
        ymin = 1e4
        ymax = 1e11
        Weights = "CoolingTimescaleInMyr"  # None
        method = "mean"  # "sum"
        colormap_vmax = 1e10
        colormap_vmin = 1e2
        colormap_vmax = None
        colormap_vmin = None
        normaliseWeights = False
        xlabel = "$n_\\mathrm{e}\\ [\\mathrm{cm}^{-3}]$"
        ylabel = "$T\\ [\\mathrm{K}]$"
        colorbarlabel = "$M\\,[\\mathrm{M}_\\odot]$"
        colorbarlabel = "$\\bar{t}_\\mathrm{cool}\\,[\\mathrm{Myr}]$"
    elif setup == "electronnumberdensityeos":
        x_variable = "ElectronNumberDensity"
        y_variable = "ElectronNumberDensityWarmPhase"
        select = None #"StarFormationRate>0"  # "AND" "StarFormationRate==0" "AND" "CoolingTimescaleInMyr<10"
        Weights = "MassInSolar"  # None
        func += ["unity"]
        method = "mean"  # "sum"
        colormap_vmax = None
        colormap_vmin = None
        normaliseWeights = False
        xlabel = "$n_\\mathrm{e}\\ [\\mathrm{cm}^{-3}]$"
        ylabel = "$n_\\mathrm{e,warm}\\ [\\mathrm{cm}^{-3}]$"
        colorbarlabel = "$M\\,[\\mathrm{M}_\\odot]$"
    elif setup == "tempdistance":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "Temperature"
        # func += ["eosfitTne", "pressure1M10", "pressure1M11"]
        select = "DistanceToCenterInKpc<80"
    elif setup == "tempdistancecount":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "Temperature"
        Weights = None
        method = "sum"
        normaliseWeights = False
        colormap_vmax = None
        colormap_vmin = None
        ymin = 1e7
        ymax = 1e8
    elif setup == "bondidistance":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "BondiMdot"
        func += ["bhhsml"]
        select = "DistanceToCenterInKpc<40"
        xmin = 6e-1
        xmax = 40
        ymin = 1e-5
        ymax = 1e3
    elif setup == "tempnecoolingtime":
        x_variable = "ElectronNumberDensity"
        y_variable = "Temperature"
        func += ["eosfitTne"]
        Weights = "CoolingTimescaleInMyr"
        select = "CoolingTimescaleInMyr>0"
        method = "median"
        colormap_vmax = 1e4
        colormap_vmin = 1e0
        normaliseWeights = False
    elif setup == "coolingtimexb":
        x_variable = "XB"
        y_variable = "CoolingTimeInMyr"
        Weights = "MassInSolar"
        method = "sum"
        colormap_vmax = None
        colormap_vmin = None
        normaliseWeights = False
    elif setup == "coolingtimexbcenter60":
        x_variable = "XB"
        y_variable = "CoolingTimeInMyr"
        Weights = "MassInSolar"
        method = "sum"
        colormap_vmax = None
        colormap_vmin = None
        normaliseWeights = False
        select = "DistanceToCenterInKpc<60"
    elif setup == "coolingtimexbcenter60density":
        x_variable = "XB"
        y_variable = "CoolingTimeInMyr"
        Weights = "Density"
        method = "mean"
        colormap_vmax = None
        colormap_vmin = None
        normaliseWeights = False
        select = "DistanceToCenterInKpc<60"
    elif setup == "coolingtimeradius":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "CoolingTimeInMyr"
        # select = "StarFormationRate==0" "AND" "DistanceToCenterInKpc<40"
        select = "DistanceToCenterInKpc<100"
        Weights = "MassInSolar"  # None
        method = "sum"  # "sum"
        colormap_vmax = 1e10
        colormap_vmin = 1e2
        xmax = 1e2
        xmin = 2e-1
        ymax = 1e8
        ymin = 1e-2
        normaliseWeights = False
    elif setup == "vrradius":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "VelocityRadial"
        # select = "StarFormationRate==0" "AND" "DistanceToCenterInKpc<40"
        select = "DistanceToCenterInKpc<100"
        Weights = "MassInSolar"  # None
        method = "sum"  # "sum"
        colormap_vmax = 1e10
        colormap_vmin = 1e2
        # xmax = 1e2
        # xmin = 2e-1
        # ymax = 1e8
        # ymin = 1e-2
        normaliseWeights = False
    elif setup == "coolingb":
        x_variable = "AbsoluteMagneticField"
        y_variable = "CoolingTimeInMyr"
        # select = "StarFormationRate==0" "AND" "DistanceToCenterInKpc<40"
        # select = "DistanceToCenterInKpc<100"
        Weights = "MassInSolar"  # None
        method = "sum"  # "sum"
        colormap_vmax = 1e10
        colormap_vmin = 1e2
        # xmax = 1e2
        # xmin = 2e-1
        # ymax = 1e8
        # ymin = 1e-2
        normaliseWeights = False
    elif setup == "jetbcenter120":
        x_variable = "AbsoluteMagneticField"
        y_variable = "JetTracer"
        # select = "StarFormationRate==0" "AND" "DistanceToCenterInKpc<40"
        select = "DistanceToCenterInKpc<100"
        Weights = "MassInSolar"  # None
        method = "sum"  # "sum"
        colormap_vmax = 1e10
        colormap_vmin = 1e2
        # xmax = 1e2
        # xmin = 2e-1
        # ymax = 1e8
        # ymin = 1e-2
        normaliseWeights = False
    elif setup == "jetbdistancecenter120":
        x_variable = "AbsoluteMagneticField"
        y_variable = "JetTracer"
        # select = "StarFormationRate==0" "AND" "DistanceToCenterInKpc<40"
        select = "DistanceToCenterInKpc<100"
        Weights = "DistanceToCenterInKpc"  # None
        method = "median"  # "sum"
        colormap_vmax = 120
        colormap_vmin = 0
        xmax = 1e-3
        xmin = 1e-8
        ymax = 1e0
        ymin = 1e-33
        normaliseWeights = False
    elif setup == "potcrenergyvsdistance":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "PassiveScalarCRAccelerationEnergy"
        colormap_vmax = 120
        colormap_vmin = 0
        # select = "DistanceToCenterInKpc<100"
        # Weights = "Mass"  # None
        method = "sum"  # "sum"
        normaliseWeights = False
    elif setup == "epsilontemperature100":
        x_variable = "Temperature"
        y_variable = "AngularMomentumEpsilon"
        # select = "StarFormationRate==0" "AND" "DistanceToCenterInKpc<40"
        select = "DistanceToCenterInKpc<100"
        Weights = "MassInSolar"  # None
        method = "sum"  # "sum"
        colormap_vmax = None
        colormap_vmin = None
        xmax = None
        xmin = None
        ymax = None
        ymin = None
        normaliseWeights = False
    elif setup == "epsilondensity100":
        x_variable = "Temperature"
        y_variable = "AngularMomentumEpsilon"
        # select = "StarFormationRate==0" "AND" "DistanceToCenterInKpc<40"
        select = "DistanceToCenterInKpc<100"
        Weights = "MassInSolar"  # None
        method = "sum"  # "sum"
        colormap_vmax = None
        colormap_vmin = None
        xmax = None
        xmin = None
        ymax = None
        ymin = None
        normaliseWeights = False
    elif setup == "bdistancjetecenter120":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "AbsoluteMagneticField"
        # select = "StarFormationRate==0" "AND" "DistanceToCenterInKpc<40"
        select = "DistanceToCenterInKpc<100"
        Weights = "JetTracer"  # None
        method = "median"  # "sum"
        colormap_vmax = 1e0
        colormap_vmin = 1e-10
        xmax = 120
        xmin = 0.3
        ymax = 1e-3
        ymin = 1e-8
        normaliseWeights = False
    elif setup == "coolingtimecoolingratiocenter40":
        x_variable = "CoolingFreeFallTimeRatio"
        y_variable = "CoolingFreeFallTimeRatio"
        Weights = "MassInSolar"
        select = "StarFormationRate==0" "AND" "DistanceToCenterInKpc<40"
        method = "sum"
        colormap_vmax = None
        colormap_vmin = None
        normaliseWeights = False
    elif setup == "coolingtimeratioradiuscenter100":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "CoolingFreeFallTimeRatio"
        Weights = "MassInSolar"
        select = "StarFormationRate==0" "AND" "DistanceToCenterInKpc<100"
        method = "sum"
        colormap_vmax = 1e10
        colormap_vmin = 1e2
        xmin = 5e-1
        xmax = 1e2
        ymin = 1e-4
        ymax = 1e8
        normaliseWeights = False
    elif setup == "coolingratioentropycenter40":
        x_variable = "CoolingFreeFallTimeRatio"
        y_variable = "Entropy"
        Weights = "MassInSolar"
        select = "StarFormationRate==0" "AND" "DistanceToCenterInKpc<40"
        method = "sum"
        colormap_vmax = None
        colormap_vmin = None
        normaliseWeights = False
    elif setup == "totalpressuremagneticpressure":
        select = "DistanceToCenterInKpc<50"
        x_variable = "Pressure"
        y_variable = "MagneticPressure"
        func += ["unity"]
        Weights = "MassInSolar"  # None
        method = "sum"  # "sum"
        colormap_vmax = None
        colormap_vmin = None
        normaliseWeights = False
    elif setup == "pressureequilcritradius":
        x_variable = "PressureEquilbriumCondensationCriteriumCellRadiusInKpc"
        y_variable = "CellRadiusInKpc"
        func += ["unity"]
        Weights = None  # None
        method = "sum"  # "sum"
        colormap_vmax = None
        colormap_vmin = None
        normaliseWeights = False
    elif setup == "pressureequilcrittemperature":
        x_variable = "PressureEquilbriumCondensationCriteriumCellRadiusInKpc"
        y_variable = "Temperature"
        Weights = None  # None
        method = "sum"  # "sum"
        colormap_vmax = None
        colormap_vmin = None
        normaliseWeights = False
    elif setup == "rhoxb":
        x_variable = "Density"
        y_variable = "XB"
        Weights = "MassInSolar"  # None
        method = "sum"  # "sum"
        colormap_vmax = None
        colormap_vmin = None
        normaliseWeights = False
    elif setup == "magneticrho60":
        x_variable = "Density"
        y_variable = "AbsoluteMagneticField"
        Weights = "MassInSolar"  # None
        method = "sum"  # "sum"
        select = "DistanceToCenterInKpc<60"
        colormap_vmax = 1e10
        colormap_vmin = 1e2
        xmax = 1e-23
        xmin = 1e-28
        ymax = 1e-3
        ymin = 1e-8
        normaliseWeights = False
    elif setup == "tempnepressurequilcritwrong":
        x_variable = "ElectronNumberDensity"
        y_variable = "Temperature"
        Weights = None  # None
        method = "sum"  # "sum"
        func += ["eosfitTne"]
        select = (
            "CellRadiusInKpc>PressureEquilbriumCondensationCriteriumCellRadiusInKpc"
            "AND"
            "StarFormationRate==0"
        )
        colormap_vmax = None
        colormap_vmin = None
        normaliseWeights = False
    elif setup == "alfvenvelocity":
        x_variable = "AlfvenVelocity"
        y_variable = "CellRadiusInKpc"
        Weights = None
        method = "sum"
        colormap_vmax = None
        colormap_vmin = None
        normaliseWeights = False
    elif setup == "alfvenvelocityjettracer":
        x_variable = "AlfvenVelocity"
        y_variable = "CellRadiusInKpc"
        Weights = "JetTracer"
        method = "mean"
        colormap_vmax = 1e0
        colormap_vmin = 1e-6
        normaliseWeights = False
    elif setup == "tempdensity":
        x_variable = "Density"
        y_variable = "Temperature"
    elif setup == "coolradius":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "CoolingTimescaleInMyr"
    elif setup == "coolradiuscenter100":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "CoolingTimescaleInMyr"
        select = "DistanceToCenterInKpc<100"
        Weights = "MassInSolar"  # None
        method = "sum"  # "sum"
        colormap_vmax = 1e10
        colormap_vmin = 1e2
        xmax = 1e2
        xmin = 2e-1
        ymax = 1e8
        ymin = 1e-2
        normaliseWeights = False
    elif setup == "tempradius":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "Temperature"
    elif setup == "cellradius":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "CellRadiusInKpc"
    elif setup == "cellradiusjetr":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "CellRadiusInKpc"
        Weights = "JetTracer"
        method = "median"
        normaliseWeights = False
    elif setup == "massradius":
        x_variable = "DistanceToCenterInKpc"
        y_variable = "MassInSolar"
    elif setup == "resjeans":
        x_variable = "JeansLengthInKpc"
        y_variable = "CellRadiusInKpc"
    else:
        raise Exception(f"setup {setup} not found!")
    if sfr_weight:
        Weights = "StarFormationRate"
        colormap_vmax = None
        colormap_vmin = None
        colormap_log = False
        normaliseWeights = False
        cmap = "plasma_r"

    plotFit = False
    fitfunc = lambda p, x: p[0] * x ** p[1]
    fitfunc = None
    initialGuess = [1.0e23, 2.0 / 3.0]

    Xbins = 100
    Ybins = Xbins
    if np.size(NameOfSimSave) == np.size(NameOfSim) == np.size(Folders):
        print(NameOfSimSave)
        print(NameOfSim)
        print(Folders)
    else:
        exit("need same number of sims, simnames, simnames to save!")

    for number in numbers:
        for root, folder, nameSave in zip(Roots, Folders, NameOfSimSave):
            folder = root + folder
            if "FOLDER" in subfolder:
                s = subfolder.replace("FOLDER", nameSave)
            else:
                s = subfolder
            figname = "../figures/PhaseDiagrams/%s%s/%s%s_W%s_%s_%s_%i.pdf" % (
                setup + "/",
                s,
                x_variable,
                y_variable,
                Weights,
                nameSave,
                method,
                number if number is not None else 0,
            )
            input = {}
            input["figname"] = figname
            input["folder"] = folder
            input["number"] = number
            input["FieldsToLoad"] = FieldsToLoad
            input["useTime"] = useTime
            input["differenceTime"] = differenceTime
            input["number"] = number
            input["x_variable"] = x_variable
            input["y_variable"] = y_variable
            input["Weights"] = Weights
            input["xmin"] = xmin
            input["xmax"] = xmax
            input["ymin"] = ymin
            input["ymax"] = ymax
            input["Xbins"] = Xbins
            input["Ybins"] = Ybins
            input["select"] = select
            input["normaliseWeights"] = normaliseWeights
            input["colormap_log"] = colormap_log
            input["colormap_vmin"] = colormap_vmin
            input["colormap_vmax"] = colormap_vmax
            input["figname"] = figname
            input["cmap"] = cmap
            input["method"] = method
            input["overflowBinY"] = overflowBinY
            input["overflowBinX"] = overflowBinX
            input["plotFit"] = plotFit
            input["initialGuess"] = initialGuess
            input["fitfunc"] = fitfunc
            input["func"] = func
            input["xlabel"] = xlabel
            input["ylabel"] = ylabel
            input["colorbarlabel"] = colorbarlabel
            input["axes_text"] = axes_text
            input["axes_text_position"] = axes_text_position
            input_list.append(input)


def main_loop(input):
    snap = Fig.readSnap(
        input["folder"],
        input["number"],
        FieldsToLoad=input["FieldsToLoad"],
        useTime=input["useTime"],
        differenceTime=input["differenceTime"],
    )
    snap, time = Fig.getGeneralInformation(
        snap,
        convertedkpc=False,
        considering_h=False,
        StringTime=True,
        IC=input["number"] is None,
    )
    Fig.PhaseDiagram(
        snap,
        input["x_variable"],
        input["y_variable"],
        input["Weights"],
        considering_h=False,
        Xmin=input["xmin"],
        Xmax=input["xmax"],
        Ymin=input["ymin"],
        Ymax=input["ymax"],
        Xbins=input["Xbins"],
        Ybins=input["Ybins"],
        logX=True,
        logY=True,
        xlabel=input["xlabel"],
        ylabel=input["ylabel"],
        colorbarlabel=input["colorbarlabel"],
        select=input["select"],
        normaliseWeights=input["normaliseWeights"],
        colormap_log=input["colormap_log"],
        colormap_vmin=input["colormap_vmin"],
        colormap_vmax=input["colormap_vmax"],
        name=input["figname"],
        cmap=input["cmap"],
        method=input["method"],
        overflowBinY=input["overflowBinY"],
        overflowBinX=input["overflowBinX"],
        TimeColor="black",
        plotFit=input["plotFit"],
        initialGuess=input["initialGuess"],
        fitfunc=input["fitfunc"],
        functions=input["func"],
        axes_text=input["axes_text"],
        axes_text_position=input["axes_text_position"],
        save=True,
        fontsize=fontsize,
        dpi=150,
        time_format=time_format,
    )


print(input_list)
print(len(input_list))
print([i["figname"] for i in input_list])
if threads > 1 and len(input_list) > 1:
    if len(input_list) < threads:
        threads = len(input_list)
    bd = mt.BigData(
        main_loop,
        [x for x in np.arange(len(input_list))],
        [input_list],
        1,
        None,
        "numpy",
    )
    bd.do_multi(threads)
else:
    for input in input_list:
        main_loop(input)
print(f"finished plots at {datetime.datetime.now()}")
print(
    f"plotting took: {'%i'%((time.time()-start_time)//60)} min  {'%.2f'%((time.time()-start_time)%60)} s"
)
