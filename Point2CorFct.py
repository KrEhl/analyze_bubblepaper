import numpy as np
import FigureMove as Fig
from scipy.spatial import distance
import auxiliary_functions as aux


def calcDistance(array1, array2):
    # array with shape (N,dim)
    return np.sqrt(np.sum((array1 - array2) ** 2, axis=1))


def calcDistance1D(array1, array2):
    # array with shape (N,dim)
    return np.abs(array1 - array2)


# def scipy_distance(array1, array2):
#     return list(map(distance.euclidean, array1, array2))


def rotate(a, n=1):
    if len(a) == 0:
        return a
    n = -n % len(a)  # flip rotation direction
    return np.concatenate((a[n:], a[:n]))


def calc2PointCorrForIndices(indices, pos, value, bins, functionValues=calcDistance):
    numbercounts = np.zeros(len(bins) + 1)
    valuecounts = np.zeros(len(bins) + 1)
    NCounts = np.size(bins) + 1
    for index, i in enumerate(indices):
        binIndicies = np.digitize(calcDistance(pos, rotate(pos, i)), bins)
        # if np.unique(binIndicies)==2:
        # 	print(pos,i,index,binIndicies)
        # 	raise Exception('only two bins')
        numbercounts += np.bincount(binIndicies, None, minlength=NCounts)
        valuecounts += np.bincount(
            binIndicies, functionValues(value, rotate(value, i)), minlength=NCounts
        )
        # except:
        # 	print('couldnt add numbercounts')
        # 	print(np.unique(binIndicies))
        # 	print(pos,rotate(pos, i),i,index,binIndicies)
    return np.array([numbercounts, valuecounts])


def get2PointCorr(
    pos, value, bins, calculateIds="all", numthreads=4, functionValues=calcDistance
):
    # results = calc2PointCorrForIndices(np.arange(1,10), pos, value, bins, functionValues=calcDistance)
    if calculateIds == "all":
        calculateIds = np.arange(1, np.shape(pos)[0])
    if np.shape(pos)[0] != np.shape(value)[0]:
        print("np.shape(pos),np.shape(value)", np.shape(pos), np.shape(value))
        raise Exception("need same amount of positions as values!")
    args = (pos, value, bins, functionValues)
    bd = initializeBigData(
        calc2PointCorrForIndices, calculateIds, None, None, None, *args
    )
    results = bd.do_multi(numthreads)
    counts = []
    values = []
    for res in results:
        counts.append(res[0])
        values.append(res[1])
    counts = np.sum(counts, axis=0)
    print("total counts in correlation: ", np.sum(counts))
    values = np.sum(values, axis=0)
    corr = values / counts
    return bins, corr, counts


if __name__ == "__main__":

    bins, corr = get2PointCorr(
        np.array([[1, 2, 3], [2, 2, 3], [1, 2, 3]]),
        np.array([[2, 2, 2], [3, 3, 3], [4, 4, 4]]),
        np.linspace(0, 100, 11),
        calculateIds="all",
        numthreads=12,
    )
    print(bins, corr)
# 	array1 = np.random.rand(10000,3)
# 	array2 = np.random.rand(10000,3)
# 	bins = np.linspace(0,1,10)
# 	# get2PointCorr(array1, array2, bins)

# 	import timeit
# 	mysetup = ' '
# 	myCode = '''\
# rotate(array1, 100)
# '''
# 	N = 100
# 	print('rotate:',timeit.timeit(setup=mysetup, stmt=myCode, number=N, globals=globals())/N)

# 	mysetup = ' '
# 	myCode = '''\
# get2PointCorr(array1, array2, bins)
# '''
# 	N = 10
# 	print('get2PointCorr:',timeit.timeit(setup=mysetup, stmt=myCode, number=N, globals=globals())/N)

# mysetup = ' '
# myCode = '''\
# scipy_distance(array1, array2)
# '''
# N = 100
# print('scipy:',timeit.timeit(setup=mysetup, stmt=myCode, number=N, globals=globals())/N)
# bins = np.linspace(0,200,101)
# # print(bins)
# bins, corr = point.get2PointCorr(np.array([[1,2,3],[2,2,3],[1,2,3]]), np.array([[2,2,2],[3,3,3],[4,4,4]]), bins, calculateIds='all', numthreads=1)
# # print(bins,corr)
