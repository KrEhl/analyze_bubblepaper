import os

import numpy as np
import FigureMove as Fig
from Multithread import BigData
from scipy.spatial import distance
import auxiliary_functions as aux
import time
import random
import warnings


def calcDistance(array1, array2):
    # array with shape (N,dim)
    return np.sqrt(np.sum((array1 - array2) ** 2, axis=1))


# def scipy_distance(array1, array2):
#     return list(map(distance.euclidean, array1, array2))


def rotate(a, n=1):
    if len(a) == 0:
        return a
    n = -n % len(a)  # flip rotation direction
    return np.concatenate((a[n:], a[:n]))


def rotated_mask(a, n, max_len):
    len_a = len(a)
    if n > len_a:
        raise Exception(f"n: {n} larger than len(a) {len(a)}")
    if len_a == 0:
        return a
    return a.take(range(n, n + max_len), mode="wrap")


def abs_value(a):
    return np.sqrt(np.sum(a ** 2, axis=1))


def velocity_correlation(vel1, vel2):
    return np.sum(vel1 * vel2, axis=1) / (abs_value(vel1) * abs_value(vel2))


def get_shift_indices(masks, sample_number=None):
    s, b = _get_large_small_index(masks)
    indices = np.arange(len(masks[s]))
    if sample_number is not None:
        if sample_number > len(indices):
            warnings.warn(
                f"reducing sample ({sample_number}) as insufficient data in mask ({len(indices)})!"
            )
            sample_number = len(indices)
        indices = random.sample(set(indices), sample_number)
    return indices


def calc_2pointcorrelation_for_two_variables(
    shift_indices,
    pos,
    variables,
    masks,
    bins,
    weights=None,
    function=velocity_correlation,
):
    s, b = _get_large_small_index(masks)
    numbercounts = np.zeros(len(bins) + 1)
    valuecounts = np.zeros(len(bins) + 1)
    nothing_found = 0
    start_time = time.time()
    mask_b = None
    for count, index in enumerate(shift_indices):
        mask_b = rotated_mask(masks[b], index, len(masks[s]))
        bin_indices = np.digitize(calcDistance(pos[masks[s]], pos[mask_b]), bins)
        # if lennp.unique(bin_indices) == 2:
        #     raise Exception(f"only 2 points; bin_indices: {bin_indices}, index: {index}, bins: {bins}")
        if len(bin_indices):
            idx = slice(0, max(bin_indices) + 1)
            if weights is None:
                numbercounts[idx] += np.bincount(bin_indices, None)
                valuecounts[idx] += np.bincount(
                    bin_indices, function(variables[s][masks[s]], variables[b][mask_b])
                )
            else:
                numbercounts[idx] += np.bincount(
                    bin_indices, weights[s][masks[s]] * weights[b][mask_b]
                )
                valuecounts[idx] += np.bincount(
                    bin_indices,
                    function(variables[s][masks[s]], variables[b][mask_b])
                    * weights[s][masks[s]]
                    * weights[b][mask_b],
                )
        else:
            nothing_found += 1
    print(
        f"calc took: {time.time()-start_time}, nothing_found: {nothing_found}, "
        f"fraction {nothing_found/max((len(shift_indices),1)) * 100}%"
    )
    if mask_b is not None:
        print(
            f"shape: pos[masks[s]]: {np.shape(pos[masks[s]])}, "
            f"shape: pos[mask_b]: {np.shape(pos[mask_b])}"
        )
        # print(numbercounts)
    return np.array([numbercounts, valuecounts])


def get_2pointcorrelation_function_two_variables_bootstrap(
    shift_indices,
    pos,
    variables,
    masks,
    bins,
    weights=None,
    function=velocity_correlation,
    threads=1,
    N_bootstrap=10,
):
    bootstrap = np.zeros((N_bootstrap, len(bins) - 1))
    from numpy.random import default_rng

    rng = default_rng()
    for i in range(N_bootstrap):
        indices = rng.integers(0, len(shift_indices), len(shift_indices))
        bootstrap[i] = get_2pointcorrelation_function_two_variables(
            np.array(shift_indices)[indices],
            pos,
            variables,
            masks,
            bins,
            weights=None,
            function=velocity_correlation,
            threads=1,
        )[1:-1]
    return bootstrap.std(0, ddof=1)


def get_2pointcorrelation_function_two_variables(
    shift_indices,
    pos,
    variables,
    masks,
    bins,
    weights=None,
    function=velocity_correlation,
    threads=1,
):
    if threads > 1:
        results = calc_2pointcorrelation_for_two_variables(
            shift_indices, pos, variables, masks, bins, function=velocity_correlation
        )

        bd = BigData(
            calc2PointCorrForIndices, calculateIds, None, 1, None, "numpy", *args
        )
        results = bd.do_multi(threads)
    else:
        results = calc_2pointcorrelation_for_two_variables(
            shift_indices,
            pos,
            variables,
            masks,
            bins,
            weights=weights,
            function=velocity_correlation,
        )
        counts = []
        values = []
        # for res in results:
        counts.append(results[0])
        values.append(results[1])
        counts = np.sum(counts, axis=0)
        values = np.sum(values, axis=0)
    corr = np.zeros_like(counts)
    m = np.nonzero(counts)
    corr[m] = values[m] / counts[m]
    return corr


def _get_large_small_index(masks):
    if len(masks) != 2:
        raise Exception(f"expected two masks!, got {len(masks)}")
    if np.shape(masks[0])[0] > np.shape(masks[1])[0]:
        b = 0
        s = 1
    else:
        b = 1
        s = 0
    return s, b


def calc2PointCorrForIndices(indices, pos, value, bins, functionValues=calcDistance):
    numbercounts = np.zeros(len(bins) + 1)
    valuecounts = np.zeros(len(bins) + 1)
    for index, i in enumerate(indices):
        binIndicies = np.digitize(calcDistance(pos, rotate(pos, i)), bins)
        numbercounts += np.bincount(binIndicies, None)
        valuecounts += np.bincount(binIndicies, functionValues(value, rotate(value, i)))
    return np.array([numbercounts, valuecounts])


def get2PointCorr(pos, value, bins, calculateIds="all", numthreads=4):
    # results = calc2PointCorrForIndices(np.arange(1,10), pos, value, bins, functionValues=calcDistance)
    if calculateIds == "all":
        calculateIds = np.arange(1, np.shape(pos)[0])
    if np.shape(pos)[0] != np.shape(value)[0]:
        print("np.shape(pos),np.shape(value)", np.shape(pos), np.shape(value))
        raise Exception("need same amount of positions as values!")
    args = (pos, value, bins, calcDistance)
    bd = BigData(calc2PointCorrForIndices, calculateIds, None, 1, None, "numpy", *args)
    results = bd.do_multi(numthreads)
    counts = []
    values = []
    for res in results:
        counts.append(res[0])
        values.append(res[1])
    counts = np.sum(counts, axis=1)
    values = np.sum(values, axis=1)
    m = np.nonzero(counts)
    corr = np.zeros_like(values)
    corr[m] = values[m] / counts[m]
    return bins, corr


if __name__ == "__main__":
    import random
    import matplotlib.pyplot as plt
    import Param as param

    sample_number = 100000
    min_dist = 1e0
    max_dist = 100
    n_bins = 100
    log_x = True
    folder = os.environ.get("testp")
    # folder = "../TestSim/CoolRuns/Perseus/R6/11_cooling_jet_bondi_RQEff1M1_BHhsml2kpc_Tf2_XB0p001_L30_XBjM3/output/"
    snap = Fig.quickImport(800, folder=folder, useTime=True)
    temp = aux.get_Temperature(snap)
    positions = (
        snap.pos[snap.type == 0] * snap.UnitLength_in_cm / param.KiloParsec_in_cm
    )
    masks = [np.where(temp > 1e6)[0], np.where(temp < 1e6)[0]]
    variables = [aux.get_Velocity(snap, no_conversion=False)] * 2
    bin_edges, min_bin_edges, max_bin_edges = aux.getBinsMMs(
        Variable=None, Min=min_dist, Max=max_dist, N=n_bins, log=log_x
    )
    bins = bin_edges[0:-1] + np.diff(bin_edges) / 2.0
    shift_indices = get_shift_indices(masks, sample_number=sample_number)
    corr = get_2pointcorrelation_function_two_variables(
        shift_indices,
        positions,
        variables,
        masks,
        bin_edges,
        function=velocity_correlation,
    )
    plt.plot(bins, corr[1:-1])
    if log_x:
        plt.xscale("log")
    plt.show()
