import matplotlib

matplotlib.use("Agg")
font = {"family": "serif", "serif": "Computer Modern Sans serif", "size": 20}
matplotlib.rc("font", **font)
# matplotlib.rcParams['lines.markersize'] = 3
import numpy as np
import FigureMove as Fig
import auxiliary_functions as aux
import FourierClass as FC
import os.path
import time
import datetime
import argparse_helpers
import argparse
import copy

axis = [2, 0]
depth = 1 / 0.67
resDepth = 16384  # 16384#800
secondXaxis = True
xlog = True
numthreads = 32

legendLocTime = "lower right"
legendLocFolder = "upper left"
LegendLocRest = "upper right"
ShadedRegionXrange = [[]]
framealpha = 0.6
plotLegendFolder = True
plotLegendTime = True
plotLegendRest = True

useTime = True
pathSaveFilesPlotData = "../figures/DataPlots/PS/"
pathCheckExtraFolders = [
    "../figuresErebos/DataPlots/PS/",
    "../../figures/DataPlots/PS/",
]

useTimeIndexAsColor = True
useFolderIndexAsColor = False
useTimeIndexAsTickStyle = False
useFolderIndexAsTickStyle = True
useTimeIndexAsAlpha = True
useFolderIndexAsAlpha = False

prefix = "TestFirstPSSqtThenTimesX2"
prefix = prefix + "NOTALTERNATIVE_RMMAP"

errbar = "percentiles"
forceRedoPlot = False
# errbar = None
Yrange = [None, None]

verbose = 0
ylabel = r"$\mathrm{P}$"
Alphas = [1, 0.75, 0.55, 0.3, 0.1]
Markers = [
    "-",
    "--",
    ":",
    "-.",
    "x",
    "o",
    "v",
    "^",
    "h",
    "+",
    "D",
    ".",
    ",",
    "<",
    ">",
    "1",
    "2",
    "3",
]
Colors = [
    "#bb596d",
    "#2565ca",
    "#199967",
    "#E4A41A",
    "#ff3232",
    "#201104",
    "#664200",
    "#0e2850",
    "#664200",
    "#54784c",
    "#0b3f00",
    "#031200",
]
# Colors = ['#697dd0','#2946bd','#0c1538','#d86270','#c82033','#3c090f','#4e7c4c','#034500','#011b00'] #sequentiel three blues getting darker then three reds darkening, then greens...
theoryMarker = ["+", ".", "v", ">"]
theoryColor = "grey"
theoryColor = ["#262626", "#595959", "#808080", "#b2b2b2"]
theoryAlpha = 0.85

LineCaptions = None  # [r"$\propto k^{2}$", r"$\propto k^{-5/3}$"]
LinePower = None  # [2, -5.0 / 3]
LineCaptionPositions = None  # [[10, 5e-9], [80, 2e-9]]
LineXrange = None  # [[8, 20], [40, 200]]
LineYstart = None  # [8e-9, 3e-8]
LineLineStyles = [None]  # ["dotted", "dotted"]

LinePower = [1.1666666666666665, 1/3]
LineCaptions = [r"$\propto k^{%g}$"%LinePower[0], r"$\propto k^{%g}$"%LinePower[1]]
LineCaptionPositions = [[3e-2, 1e12], [1e-1, 3e12]]
LineXrange = [[2e-2, 1e-1], [2e-2, 1e-1]]
LineYstart = [1e12, 3e12]
LineLineStyles = ["dotted", "dotted"]

# LineCaptions = [r"$\propto k^{2}$", r"$\propto k^{-5/3}$"]
# LinePower = [2, -5.0 / 3]
# LineCaptionPositions = [[10, 5e-9], [80, 2e-9]]
# LineXrange = [[8, 20], [40, 200]]
# LineYstart = [8e-9, 3e-8]
# LineLineStyles = ["dotted", "dotted"]

linewidth = 1.78
markersize = 6 * linewidth


INFO = {}

Xmin = None
Xmax = None
Ymin = None
Ymax = None

default_n = 400  # 1024 #512
default_resDepth = 16384  # 16384#800#128
default_folder = "_M15mhdcrsLL"
default_numbers = [0, 400]
default_l = 500
Test = False
if os.path.exists("/home/kristian/Analysis/") or os.path.exists(
    "/home/kristian/analysis/"
):
    print("using test!")
    Test = True

if Test:
    default_folder = "_test"
    default_n = 128
    default_resDepth = 256

start_time = time.time()
parser = argparse.ArgumentParser(
    description="grid plots of different variables!",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    f"--folder", type=argparse_helpers.fancy_eval, nargs="+", default=[default_folder]
)
parser.add_argument(
    f"--setup",
    type=argparse_helpers.fancy_eval,
    nargs="+",
    default=["velocity", "magneticfield",],
)
parser.add_argument(
    f"--xmin", type=argparse_helpers.fancy_eval, nargs=1, default=[Xmin]
)
parser.add_argument(
    f"--xmax", type=argparse_helpers.fancy_eval, nargs=1, default=[Xmax]
)
parser.add_argument(
    f"--ymin", type=argparse_helpers.fancy_eval, nargs=1, default=[Ymin]
)
parser.add_argument(
    f"--ymax", type=argparse_helpers.fancy_eval, nargs=1, default=[Ymax]
)
parser.add_argument(
    f"--n", type=argparse_helpers.fancy_eval, nargs=1, default=[default_n]
)
parser.add_argument(
    f"--l", type=argparse_helpers.fancy_eval, nargs=1, default=[default_l]
)
parser.add_argument(
    f"--resdepth", type=argparse_helpers.fancy_eval, nargs=1, default=[default_resDepth]
)
parser.add_argument(
    f"--vertical", type=argparse_helpers.fancy_eval, nargs="+", default=[]
)
parser.add_argument(f"--ics", type=argparse_helpers.fancy_eval, nargs="+", default=None)
parser.add_argument(f"--alpha_folder", action="store_false", default=True)
parser.add_argument(f"--single_folder_plots", action="store_true", default=False)
parser.add_argument(
    f"--numbers", type=argparse_helpers.fancy_eval, nargs="+", default=default_numbers,
)
parser.add_argument(f"--numbers_min", type=argparse_helpers.fancy_eval, default=False)
parser.add_argument(f"--numbers_max", type=argparse_helpers.fancy_eval, default=False)
parser.add_argument(f"--ignore_two_pi", type=argparse_helpers.fancy_eval, default=True)
parser.add_argument(f"--numbers_n", type=argparse_helpers.fancy_eval, default=8)
parser.add_argument(f"--k_factor", type=argparse_helpers.fancy_eval, default=1.0)

input = parser.parse_args().__dict__.copy()
input = {k: v for k, v in input.items() if v is not None}
import script_helpers

script_helpers.get_parameters_from_yaml(input)
# exit()
print(input)
FOLDER = [input["folder"]]
ROOT = [input["root"]]
NAMEOFSIMSAVE = [input["name_sim_save"]]
NAMEOFSIM = [input["name_sim"]]
positionsVertical = input["vertical"]
Xmin = input["xmin"][0]
Xmax = input["xmax"][0]
Ymin = input["ymin"][0]
Ymax = input["ymax"][0]
k_factor = input["k_factor"]
ignoreTwoPi = input["ignore_two_pi"][0]
print(f"ignoreTwoPi: {ignoreTwoPi}")
ICS = input["ics"] if "ics" in input.keys() else False
ICS = [Fig.makeIterable(ICS, len(FOLDER[0]))]
n = input["n"][0]
L = input["l"][0]
resDepth = input["resdepth"][0]
setup = input["setup"]
if type(setup) != list:
    setup = [setup]
print(setup)
alpha_folder = input["alpha_folder"][0]
single_folder_plots = input["single_folder_plots"][0]
print(f"single_folder_plots: {single_folder_plots}")
Numbers = input["numbers"]
numbers_min = input["numbers_min"][0]
numbers_max = input["numbers_max"][0]
numbers_n = input["numbers_n"][0]
print(numbers_min, numbers_max)
print(bool(numbers_min), bool(numbers_max))
print(Numbers)
if (numbers_min or numbers_min == 0) and numbers_max != False:
    Numbers = np.linspace(numbers_min, numbers_max, numbers_n)


onlyVariableNumFromAll = None
ignoreVariableForFolder = [[], [0, 1], []]
ignoreVariableForFolder = None
FIELDTYPE = "Grid"
alphaError = 0.2

Xrange = [None, None]  # [8,400]

s = np.sqrt(3.0)
kL = 1 / s  # 1/s
kinj = 40 * 0.67  # 270#40#15 #8
kcrit = 80 * 0.67  # 675#80 #20
a = 0.0
b = -11.0 / 3.0
c = 0.0
fieldDim = 1

loadPlotData = savePlotData = True
# -----------------
delta = L / n
n_pad = 0
sigma_to_be = 3e-7  # ((4.15e-7)**2)#1.5e4
realFT = False
INFO["constMomFluxRhVToBRatio"] = 1.0

plotTheorySnap = False
plotTheoryTurb = False
plotTheoryProf = False
plotTheoryTurbRescaled = False

useSigmaToNormalize = False
if plotTheorySnap or plotTheoryTurb or plotTheoryProf or plotTheoryTurbRescaled:
    useSigmaToNormalize = True
powerspec = True
binTypeCalc = "median"

NBins = 50
scatterData = False
scatterShowN = 100
legendFolderExternal = False
legendTimeExternal = False
legendFolderNcol = 1

INFO["percentilesLowerBoundary"] = 0.3173
INFO["percentilesUpperBoundary"] = 0.6827

# ------


print(ICS)
print(FOLDER)
print(NAMEOFSIM)
print(NAMEOFSIMSAVE)
print(ROOT)
if single_folder_plots:
    FOLDER = [[x] for x in FOLDER[0]]
    NAMEOFSIM = [[x] for x in NAMEOFSIM[0]]
    NAMEOFSIMSAVE = [[x] for x in NAMEOFSIMSAVE[0]]
    ROOT = [[x] for x in ROOT[0]]
    ICS = [[x] for x in ICS[0]]
print(ICS)
print(FOLDER)
print(NAMEOFSIM)
print(NAMEOFSIMSAVE)
print(ROOT)
for indexFolder, (NAMSIM, NAMSAVE, FOLD, RO, IC) in enumerate(
    zip(NAMEOFSIM, NAMEOFSIMSAVE, FOLDER, ROOT, ICS)
):

    for s in setup:
        legendExternal = legendFolderExternal or legendTimeExternal
        fig, ax = Fig.getFigure(
            legendExternal=legendExternal,
            secondXaxis=secondXaxis,
            nolegendExtAxSize=[0.12, 0.12, 0.86, 0.87],
        )
        LABELFolder = []
        LABELRest = []
        LABELTIME = []
        PLOTSFolder = []
        PLOTSRest = []
        PLOTSTime = []

        ignoreVariableForFolder = None
        if s == "rm":
            Xrange = [8, 1e3]
            Yrange = [1e8, 1e14]
            axis = [2, 0]
            depth = 1 / 0.67
            ylabel = r"$\mathrm{P}_\mathrm{RM}$"
            FIELDTYPE = "RMmap"
            VARIABLES = ["AbsoluteConstantMagneticField", "MagneticFieldY"]
            VARIABLESnames = [r"RM smooth profile", r"RM"]
        elif s == "mixed":
            VARIABLES = [
                # "SqrtAbsoluteConstantMagneticFieldEnergyDensity",
                # "SqrtMagneticFieldEnergyDensity",
                "SqrtAbsoluteMomentumFlux",
                # "AbsoluteConstantMagneticField",
                # "MagneticFieldY",
            ]
            VARIABLESnames = [
                # r"smooth profile",
                # r"magnetic",
                r"kinetic",
                # r"RM smooth profile",
                # r"RM",
            ]
            FIELDTYPE = [
                # "Grid",
                # "Grid",
                "Grid",
                # "RMmap",
                # "RMmap"
            ]
        elif s == "velocities":
            VARIABLES = [
                "VelocityX",
                "VelocityY",
                "VelocityZ",
            ]
            VARIABLESnames = [
                "VelocityX",
                "VelocityY",
                "VelocityZ",
            ]
            FIELDTYPE = "Grid"
        elif s == "magneticfields":
            VARIABLES = [
                "MagneticFieldX",
                "MagneticFieldY",
                "MagneticFieldZ",
            ]
            VARIABLESnames = VARIABLES
            FIELDTYPE = "Grid"
        elif s == "magneticfield":
            VARIABLES = [
                "MagneticFieldX",
            ]
            VARIABLESnames = VARIABLES
            FIELDTYPE = "Grid"
        elif s == "magneticfields":
            VARIABLES = [
                "MagneticFieldX",
                "MagneticFieldY",
                "MagneticFieldZ",
            ]
            VARIABLESnames = VARIABLES
            FIELDTYPE = "Grid"
        elif s == "velocity":
            VARIABLES = [
                "VelocityX",
            ]
            VARIABLESnames = [
                "VelocityX",
            ]
            FIELDTYPE = "Grid"
        elif s == "xrayfluct":
            ylabel = r"$\mathrm{P}_\mathrm{X}$"
            INFO["XrayTemperatureCutInKeV"] = 15
            VARIABLES = ["XrayEmissivity"]
            VARIABLESnames = [r"x-ray"]
            FIELDTYPE = "Projection"

        elif s == "bvturb":
            # Yrange = [1e-14, 1e-4]
            # Yrange = [1e-9, 1e-5]
            # LineCaptionPositions = [[12, 1e-5], [80, 6e-6]]
            # LineYstart = [2e-6, 1e-5]

            # Xrange = [8, 2e3]
            ShadedRegionXrange = [[1e3, 2e3]]
            LineYstart = [8e-7, 4e-6]
            LineXrange = [[8, 25], [50, 1e3]]
            LineCaptionPositions = [[12, 4e-6], [300, 2.5e-7]]
            ylabel = r"${P}_\mathrm{kin},\ {P}_B$"
            VARIABLES = [
                "SqrtMagneticFieldEnergyDensity",
                "SqrtAbsoluteMomentumFlux",
                "SqrtAbsoluteConstantMagneticFieldEnergyDensity",
            ]
            VARIABLESnames = [
                r"magnetic",
                r"kinetic",
                r"smooth profile",
            ]
            legendLocTime = "upper right"
            legendLocFolder = "lower left"
            LegendLocRest = "upper right"
        else:
            raise Exception(f"setup {s} unknown!")
        addName = ""
        if plotTheoryTurb:
            addName += "_TT"
        if plotTheoryProf:
            addName += "_TP"
        if plotTheorySnap:
            addName += "_TS"
        if plotTheoryTurbRescaled:
            addName += "_TR"
        if any([x in ["RMmap", 2] for x in FIELDTYPE]):
            addName += "_Dep%s" % aux.convert_params_to_string(depth, Round=3)
            addName += "_ResDep%s" % aux.convert_params_to_string(resDepth)
        if useSigmaToNormalize:
            addName += "_SigmaNormalized"

        if s == "rm":
            prefix += "RM_"
        filename = (
            "../figures/FourierSimple/AfterPS%s%s_%s_My%s_N%i_Real%i_Calc%s_L%s_PS%i%s.pdf"
            % (
                prefix,
                aux.convert_params_to_string(VARIABLES),
                aux.convert_params_to_string(NAMSAVE),
                aux.convert_params_to_string(Numbers),
                n,
                realFT,
                binTypeCalc,
                aux.convert_params_to_string(L),
                powerspec,
                addName,
            )
        )
        print(".. working on %s" % filename)
        if len(os.path.basename(filename)) > 255:
            raise Exception(
                "filename too long %i>255!" % len(os.path.basename(filename))
            )
        VARIABLES = Fig.makeIterable(VARIABLES, 1)
        VARIABLESnames = Fig.makeIterable(VARIABLESnames, len(VARIABLES))
        FIELDTYPE = Fig.makeIterable(FIELDTYPE, len(VARIABLES))
        for indexFolder, (nameSim, nameSimSave, folder, root, ics) in enumerate(
            zip(NAMSIM, NAMSAVE, FOLD, RO, IC)
        ):
            folder = root + folder
            for indexTime, numSim in enumerate(Numbers):
                for indexRest, (variable, fieldType, name) in enumerate(
                    zip(VARIABLES, FIELDTYPE, VARIABLESnames)
                ):
                    spaceDim = 3
                    if fieldType == "RMmap":
                        spaceDim = 2
                    print(f"delta: {delta}")
                    ft = FC.FTObj(
                        spaceDim=spaceDim,
                        fieldDim=fieldDim,
                        n=n,
                        delta=delta,
                        ignoreTwoPi=ignoreTwoPi,
                        k_factor=k_factor,
                    )
                    # check if loading plot possible

                    INFOInitial = copy.deepcopy(INFO)
                    if fieldType == "RMmap":
                        INFO["RMmap_resDepth"] = resDepth
                        INFO["RMmap_depth"] = depth
                    if fieldType != "Grid":
                        INFO[f"{fieldType}_axis"] = axis
                    if ignoreVariableForFolder is not None:
                        if indexRest in ignoreVariableForFolder[indexFolder]:
                            continue
                    if onlyVariableNumFromAll is not None:
                        if indexTime != 0 and indexRest not in onlyVariableNumFromAll:
                            continue
                    colorIndex, tickIndex, alphaIndex = Fig.getPlotIndex(
                        useTimeIndexAsColor,
                        useFolderIndexAsColor,
                        useTimeIndexAsTickStyle,
                        useFolderIndexAsTickStyle,
                        useTimeIndexAsAlpha,
                        useFolderIndexAsAlpha,
                        indexTime,
                        indexFolder,
                        indexRest,
                        verbose=verbose,
                    )
                    snap = (numSim, folder, useTime)

                    linestyle = "-"
                    marker = None
                    if Markers[tickIndex] in ["-", "--", ":", "-."]:
                        linestyle = Markers[tickIndex]
                    else:
                        marker = Markers[tickIndex]

                    plot = ft.plotFT(
                        typ="snap",
                        coordinate="kAbs",
                        comp=0,
                        absTrafo=False,
                        plotVariableInKspace=True,
                        absolute=True,
                        sqrt=False,
                        factor=None,
                        binData=True,
                        NBins=NBins,
                        binLog=xlog,
                        powerspec=powerspec,
                        scatter=False,
                        scatterData=scatterData,
                        scatterShowN=scatterShowN,
                        binTypeCalc=binTypeCalc,
                        binSameNumberPoints=False,
                        errbar="percentiles",
                        savePlotData=savePlotData,
                        loadPlotData=loadPlotData,
                        ax=ax,
                        errbarStyle="shaded",
                        alphaError=alphaError,
                        color=Colors[colorIndex],
                        alpha=Alphas[alphaIndex],
                        marker=marker,
                        linestyle=linestyle,
                        xlog=True,
                        ylog=True,
                        xlabel=None,
                        ylabel=None,
                        Xrange=Xrange,
                        Yrange=Yrange,
                        savePlot=False,
                        snap=snap,
                        snapName=nameSimSave,
                        variable=variable,
                        initializeVariableInKspace=False,
                        initializeProfileInKspace=True,
                        fieldType=fieldType,
                        variableProfile=None,
                        INFO=INFOInitial,
                        showlegend=False,
                        ics=ics,
                    )

                    # plot = magsna.plotFT(marker=marker, linestyle=linestyle, color=Colors[colorIndex], alpha=Alphas[alphaIndex], linewidth=linewidth, markersize=markersize, ax=ax, label=label, alphaError=alphaError)
                    # if plotTheorySnap or plotTheoryTurb or plotTheoryProf or plotTheoryTurbRescaled:
                    # 	A = magsna.getAValue()
                    # if plotTheorySnap:
                    # 	label = r'$%s:\ \mathrm{theory}$'%name
                    # 	magsna.plotTheoPS(marker=theoryMarker[0], color=theoryColor[0], alpha=theoryAlpha, linewidth=linewidth, markersize=markersize, ax=ax, powerspec=powerspec, label=label, binTypeCalc=binTypeCalc, NBins=NBins, scatterData=scatterData, scatterShowN=scatterShowN, errbar=errbar)
                    if indexRest == 0 and indexTime == 0:
                        LABELFolder.append(nameSim)
                        PLOTSFolder.append(plot)
                    if indexFolder == 0 and indexTime == 0:
                        LABELRest.append(name)
                        PLOTSRest.append(plot)
                    if indexFolder == 0 and indexRest == 0:
                        if numSim is not None:
                            LABELTIME.append(r"$%i\ \mathrm{Myr}$" % numSim)
                        else:
                            LABELTIME.append(r"ics")
                        PLOTSTime.append(plot)

        MaxN = np.max([np.size(x) for x in [FOLDER, Numbers, VARIABLES]])
        Colors = Fig.makeIterable(Colors, MaxN, List=False)
        Markers = Fig.makeIterable(Markers, MaxN, List=False)
        Alphas = Fig.makeIterable(Alphas, MaxN, List=False)
        FIELDTYPE = Fig.makeIterable(FIELDTYPE, len(VARIABLES), List=False)

        if LineCaptions is not None:
            for Lcap, LcapPos, Lpow, Lxrang, LYst, LLineSty in zip(
                LineCaptions,
                LineCaptionPositions,
                LinePower,
                LineXrange,
                LineYstart,
                LineLineStyles,
            ):
                xvals = np.linspace(Lxrang[0], Lxrang[1], 10)
                ax.plot(
                    xvals,
                    LYst * (xvals / Lxrang[0]) ** Lpow,
                    color="black",
                    linestyle=LLineSty,
                )
                ax.text(*LcapPos, Lcap, color="black")
        if len(ShadedRegionXrange[0]) > 0:
            for Sxrang in ShadedRegionXrange:
                ax.axvspan(Sxrang[0], Sxrang[1], color="black", alpha=0.3)
        if ignoreTwoPi:
            xlabel = r"$k\ [\mathrm{kpc}^{-1}]$"
        else:
            xlabel = r"$k\ [2\pi/x]$"
        Fig.modifyPlot(
            ax,
            xlog=xlog,
            ylog=1,
            xlabel=xlabel,
            ylabel=ylabel,
            Xrange=Xrange,
            Yrange=Yrange,
        )

        # looks at time
        if plotLegendTime:
            if indexTime > 0:
                if legendTimeExternal:
                    legend1 = ax.legend(
                        [x[0] for x in PLOTSTime],
                        LABELTIME,
                        loc="center left",
                        framealpha=framealpha,
                        fancybox=True,
                        bbox_to_anchor=(1, 0.5),
                    )
                else:
                    legend1 = ax.legend(
                        [x[0] for x in PLOTSTime],
                        LABELTIME,
                        loc=legendLocTime,
                        framealpha=framealpha,
                        fancybox=True,
                    )
                ax.add_artist(legend1)

        if plotLegendFolder:
            if indexFolder > 0 and len(LABELFolder) > 1:
                if legendFolderExternal:
                    legend2 = ax.legend(
                        [x[0] for x in PLOTSFolder],
                        LABELFolder,
                        loc="center left",
                        framealpha=framealpha,
                        fancybox=True,
                        bbox_to_anchor=(1, 0.5),
                        ncol=legendFolderNcol,
                    )
                else:
                    legend2 = ax.legend(
                        [x[0] for x in PLOTSFolder],
                        LABELFolder,
                        loc=legendLocFolder,
                        framealpha=framealpha,
                        fancybox=True,
                        ncol=legendFolderNcol,
                    )
                ax.add_artist(legend2)
        if plotLegendRest:
            if indexRest > 0:
                legend3 = ax.legend(
                    [x[0] for x in PLOTSRest],
                    LABELRest,
                    loc=LegendLocRest,
                    framealpha=framealpha,
                    fancybox=True,
                    frameon=True,
                )
                legend3.get_frame().set_edgecolor("black")
                ax.add_artist(legend3)

        if secondXaxis:
            ax2 = ax.twiny()
            numberMaxLogTicks = numberMaxLinTicks = len(ax.get_xticks()) * 2
            Xmin, Xmax = ax.get_xlim()
            NewXmin = 1 / Xmax
            NewXmax = 1 / Xmin
            if xlog:
                ticks, minorTicks = Fig.setLogTicks(
                    NewXmin,
                    NewXmax,
                    numberMax=numberMaxLogTicks,
                    minormajorOverlap=True,
                )
                ax2.set_xscale("log")
            else:
                ticks = Fig.setLinTicksAlt(
                    NewXmin, NewXmax, numberMax=numberMaxLinTicks
                )

            def tickConvert(X):
                return 1 / (X)

            ax2.set_xlim(ax.get_xlim())
            # ax2.tick_params(axis='x', which='minor', top=False)
            ax2.set_xticks(tickConvert(ticks), minor=False)
            ax2.set_xticklabels(["%g" % x for x in ticks], minor=False)
            print(f"ticks: {ticks}")
            print(f"minorTicks {minorTicks}")
            print(f"tickConvert(ticks): {tickConvert(ticks)}")
            if xlog:
                pass
                minorTicks = np.array([x for x in minorTicks if x not in ticks])
                ax2.set_xticks(tickConvert(minorTicks), minor=True)
                ax2.set_xticklabels(["" % x for x in minorTicks], minor=True)
                # ax2.set_xticklabels(["%g" % x for x in minorTicks], minor=True)
            ax2.set_xlabel(r"$r\ [\mathrm{kpc}]$")

        aux.create_dirs_for_file(filename)
        aux.checkExistence_delete_file(filename)
        print(".. saving %s" % filename)
        fig.savefig(filename, dpi=400, bbox_inches="tight")
print(f"finished plots at {datetime.datetime.now()}")
print(
    f"plotting took: {'%i'%((time.time()-start_time)//60)} min  {'%.2f'%((time.time()-start_time)%60)} s"
)
