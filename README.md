# Install guide:
```shell
python -m pip install -r requirements.txt
```
Latex is required for most plots, can be installed on linux via
```shell
sudo apt install texlive-latex-base dvipng texlive-latex-extra texlive-fonts-recommended cm-super
```
# Tutorial:
## multi_show_main.py :
Used to generate images from your snapshots based on combining your details for plotting in _macros_, which can be chained together for new plots.
### First example:
Start by adding a macro specifying the folder of your snapshots to folder.yaml, where you need to specify 
```yaml
- mysim: # macro name
    - root: # root folder
        - /ll1701/kristian/CoolRuns/M15Fixed/R6/
    - folder: # second part of folder name
        - 11_cooling_jet_bondi_RQEff1M4_BHhsml2kpc_highfreq/output/
    - name_sim: # name of simulation when labeled in plot
        - M14R6BoJEfM4Bs2kpcTf2IP
    - name_sim_save: #name of simulation in filename of figure
        - M14R6BoJEfM4Bs2kpcTf2IP
```
Then you are ready for your first plot. Run the following command to generate a plot of your folder
of the density, temperature and thermal pressure of your snapshot at 10 Myr. The underscores in front of your parameters
are required when calling macros.
```shell
python multi_show_main.py --folder _mysim --variable _ElectronNumberDensity _TemperatureInkV _ThermalPressure --snapshot_number 10
```
The resulting figure should the specified variables from left to right of the specified snapshot. Many details of these 
specific variable plots are again detailed in the definition of their macro in the respective file variable.yaml . Macros are
always defined in a .yaml file called after the argument. Let's have a look at the definition of 
```yaml
- ElectronNumberDensity: # macro name
    - variable: # physical quantity as specified in the function get_value in auxiliary_functions.py
        - ElectronNumberDensity
    - weight: # weight 
        - None
    - variable_name: # name in figure assumes latex mathmode by default
        - n_\\mathrm{e}
    - variable_name_units: # name of units in figure assumes latex mathmode by default 
        - \\mathrm{cm}^{-3}
    - colormap: # color map as defined by matplotlib
        - Spectral
    - log: # logarithmic colorbar with possible values: True,False,symlog
        - 1
    - plot_type: # plot_type with possible values: slice, projection, projection_depth, integral, pixel_integral, isolate_features, rotation_measure, projection_twice, sz_map, sz_effect, sum_projection, external
        - projection
    - vmin: # minimum value in colorbar
        - 1e-3
    - vmax: # maximum value in colorbar
        - 1e-1
```
List of possible colormaps can be found [here](https://matplotlib.org/stable/tutorials/colors/colormaps.html).
### Specifying resolution and dimensions of the plot:
The amount of pixels (resolution) of your resulting image in each dimension are specified as **res**. **height** and **width** 
are used to specify height and width of the plot respectively. The orientation of the plot is defined as **axis** which is a list of two integers, where the first integer gives the axis of the simulation (can think of it as: x-axis=0, y-axis=1 and z-axis=2) plotted along the x-axis of the plot and the second integer is the axis of the simulation plotted along the y-axis of the plot. The depth for a projection is controlled via changing the argument **depth**. The plot is by default centered on the center of the snapshot, which can be changed by setting **center** to the desired coordinates. Let's plot the first figure with a resolution of 512 
in an image with dimension 100 kpc x 100 kpc x 4 kpc. By default, this is done in units of kpc. 
```shell
python multi_show_main.py --folder _mysim --variable _ElectronNumberDensity _TemperatureInkV _ThermalPressure --snapshot_number 10 --res 512 --height 100 --width 100 --depth 4
```
Other units are specified with argument 
**box_units** . Alternatively, one could define the dimensions of the plot as a list in conjunction with **box_3d** (defined **axis** is crucial here). The same plot as above could then alternatively be obtained via
```shell
python multi_show_main.py --folder _mysim --variable _ElectronNumberDensity _TemperatureInkV _ThermalPressure --snapshot_number 10 --res 512 --box_3d [100,4,100]
```
Warning, this will overwrite **depth_max** as well!

The choice of dimension and resolution becomes especially crucial when looking at integrated properties, e.g. xray emissivity, surface mass density, ... .
One hand generating large grids in 3D that are summed up later to obtain the desired quantity are computationally expensive and
 may overwhelm the memory of your system. However, missing crucial data points may undermine your results. Checking for a 
_resolved_ plot is therefore crucial.

To vary the depth of the integral, we vary the argument **depth_max**. The resolution in this dimension is controlled via
**res_depth**. By default the code will set **depth_max** to the box size and **res_depth** will be set such that the minimal
cell radius found gives the resolution requirement, i.e. **res_depth**=**depth_max**/min(cell volume ** (1/3)). This is a rather
conservative value and may be too expensive to compute. In addition, the library used to generate 3D grids, i.e. arepo-snap-util,
will crash if insufficient cells are when generating a grid. Setting these two values by hand may therefore be inevitable.

In addition to **plot_type** integral, the class variable plots_deep contains **plot_type**s that check
for **res_depth** and **depth_max**. As of now, this includes the following **plot_type**s: "integral", "pixel_integral", "isolate_features", "rotation_measure", "sz_map" and "sz_effect" .

As an example, we plot the cooling emissivity of the simulation with a depth of integration of 4 Mpc at a resolution of 4096 via
```shell
python multi_show_main.py --folder _mysim --variable _CoolingEmissivityChandraNone --snapshot_number 10 --depth_max 4000 --res_depth 4096
```
### (Meta) macros:

_Macros_ are generally collections of parameter choices. In the following we go through the location of different macros and
details of defined parameters. If individual parameters specified in macros should be overwritten/replaced the first argument of the respective keyword argument should be augmented with REP_ , e.g.
```shell
python multi_show_main.py --folder _mysim --variable _ElectronNumberDensity _TemperatureInkV --snapshot_number 10 --colormap REP_Blues
```
This command will plot the macros as defined in variable.yaml but overwrite their prescribed colormaps with "Blues".
#### folder.yaml 
Parameters concerning location of folder where snapshots are saved.
```yaml
- mysim: # macro name
    - root: # root folder
        - /ll1701/kristian/CoolRuns/M15Fixed/R6/
    - folder: # second part of folder name
        - 11_cooling_jet_bondi_RQEff1M4_BHhsml2kpc_highfreq/output/
    - name_sim: # name of simulation when labeled in plot
        - M14R6BoJEfM4Bs2kpcTf2IP
    - name_sim_save: #name of simulation in filename of figure
        - M14R6BoJEfM4Bs2kpcTf2IP
```
#### variable.yaml
Parameters defining physical quantities plotted, labels, label units, colormap choice and its range.
```yaml
- ElectronNumberDensity: # macro name
    - variable: # physical quantity as specified in the function get_value in auxiliary_functions.py
        - ElectronNumberDensity
    - weight: # weight 
        - None
    - variable_name: # name in figure assumes latex mathmode by default
        - n_\\mathrm{e}
    - variable_name_units: # name of units in figure assumes latex mathmode by default 
        - \\mathrm{cm}^{-3}
    - colormap: # color map as defined by matplotlib
        - Spectral
    - log: # logarithmic colorbar with possible values: True,False,symlog
        - 1
    - plot_type: # plot_type with possible values: slice, projection, projection_depth, integral, pixel_integral, isolate_features, rotation_measure, projection_twice, sz_map, sz_effect, sum_projection, external
        - projection
    - vmin: # minimum value in colorbar
        - 1e-3
    - vmax: # maximum value in colorbar
        - 1e-1
```
#### quiver_variable.yaml 
Same as variable.yaml but for quiver data.
```yaml
- MagneticField:
    - quiver_variable:
        - MagneticField # see variable
    - quiver_variable_name:
        - B # same as variable_name appears in colorbar on top of image if --quiver_colorbar True
    - quiver_variable_name_units:
        - \\mathrm{Gauss} # same as variable_name_units appears in colorbar on top of image if --quiver_colorbar True
    - quiver_plot_type:
        - slice # see plot_type
    - quiver_weight:
        - None # see weight
    - quiver_color:
        - "#004220" # single color for quiver plot in conflict with quiver_colormap
    - quiver_res:
        - 128 # see res
    - quiver_skip_n:
        - 4 # skips 4 resolution elements, used to limit amount of arrows appearing on plot while drawing from smoother distribution
    - quiver_colormap:
        - None # colormap for quiver plot in conflict with quiver_color
    - quiver_alpha:
        - 0.82 # alpha of arrows
    - quiver_vmax:
        - 1e-5 # see vmax
    - quiver_vmin:
        - 1e-7 # see vmin
    - quiver_log:
        - 0 # see log
```

#### contour_variable.yaml 

Same as variable.yaml but for contour data.
```yaml
- XrayEmissivityChandraIsolated:
    - contour_variable:
        - XrayEmissivityABOVE0p2keVBELOW10keV # see variable
    - contour_plot_type:
        - isolate_features # see plot_type
    - contour_weight: 
        - None # see weight
    - contour_levels: 
        - [-1.5e-1, -1e-1] # values where contours drawn 
    - contour_res:
        - 512 # see res
    - contour_filter:
        - filter2_filternum4 # variety of filters (filter) available with parameter choices (filternum) defined in auxiliary_functions.get_contour_data
    - contour_colors:
          - ["#fff2cc","#bf9000", "#7ba769", "#000033", "#bbffff", "#ff3030"] # color values corresponding to levels
    - contour_linewidth:
          - 0.5 # linewidth of contours
```
#### multi_show_meta_macros.yaml 
Collection of macros and any additional parameter intended for easy access to frequently plotted figures.
```yaml
- overviewccvelocities:
    - variable: # calling macros now including all parameters defined for them
        - _AbsoluteVelocityHitomi
        - _DispersionLOSPixel
        - _ColdGasDensitySF
        - _ColdGasDensityTCool30
        - _Entropy
        - _XrayEmissivityChandra
    - quiver: # location of figure where quiver plotted
        - [1, 0, 0, 0, 0, 0, 'x']
    - quiver_variable: # calling macro for quiver
        - _Velocity
    - contour: # location of figure where contour plotted
        - [0, 1, 0, 0, 0, 0, 'x']
    - contour_variable: # calling macro for contour
        - _JettracerSlice
    - quiver_colorbar: # bool to decide if colorbar on top of figure for quiver desired 
        - 0
    - show_time: # rule for displaying time on images in figure
        - "once"
    - res: # see res
          - 510
    - res_depth: # see res_depth and chapter 'Specifying resolution and dimensions of the plot'
          - 4096
    - width: # see res_depth
          - 132
    - height: # see res_depth
          - 132
```
Macros are called by the argument corresponding to their filename (multi_show_meta_macros called via --meta). For example, 
we plot the meta macro overviewccvelocities at 10 Myr of our folder via
```shell
python multi_show_main.py --folder _mysim --meta _overviewccvelocities --snapshot_number 10 --depth_max 4000 --res_depth 4096
```
Note; we specify **depth_max** and **res_depth** explicitly as default usually unnecessarily expansive (see Chapter 'Specifying resolution and dimensions of the plot' for details). 
This is required for macros _DispersionLOSPixel, _ColdGasDensitySF, _ColdGasDensityTCool30 and
_XrayEmissivityChandra as they all have **plot_type**s belonging to so called _plots_deep_ defined in class variable **plots_deep** (see mentioned chapter).

Note; due to implementation details, if more than one macro of the same type, e.g. --variable _AbsoluteVelocityHitomi _DispersionLOSPixel, are used the same parameters
have to be defined in all of them. That's why the parameters defined in the outlined .yaml files should
be consistent throughout the same file. 

### Indexes to specify what is varied along axis of plot
In general, either snapshot number, variable or folder are varied from left to right (x-axis) or top to bottom (y-axis) in a typical application of the script. By default, variables are varied along the x-axis and snapshot numbers along the y-axis.
The corresponding _index_ changes this behavior. They are:

* index_variable: controls the axis along which **variable** is varied
* index_snapshot_number: controls the axis along which **snapshot_number** is varied
* index_folder: controls the axis along which **folder** is varied

Possible values are:

* x: varies quantity along x-axis of figure, i.e. from left to right
* y: varies quantity along y-axis of figure, i.e. from top to bottom
* all: first value stays constant throughout figure
* xy: varies quantity from left to right top to bottom where **lengthX** defined number of plots along x

In addition, the previous values can be added after any parameter to signify that it should be varied along the defined axis, e.g.
```shell
p multi_show_main.py --folder _mysim --snapshot_number 10 10 --variable _ElectronNumberDensity --colormap REP_Blues Spectral y
```
which plots __ElectronNumberDensity_ at 10 Myr and varies **colormap** along the y-axis.
### Plot types:
A wide variety of choices for **plot_type** are available that are briefly introduced:

1. "slice" : Corresponds to values on a plane through the simulation box. Will usually show detailed cell structure of code. Defined in Fig.do_Slice.
2. "projection" : Average values taken along the line of sight from a usually thin cuboid, i.e. small **depth**. If **weight** is specified, a weighted average is taken along the line of sight, i.e. sum(variable values * weight values) / sum(weight values). Resolution is assumed to have same distance in all directions, i.e. a projection with **height** x **width** x **depth** = 100 x 100 x 5 and **res**=400 will be based on cube with resolution 400x400x20. Defined in Fig.do_Projection.
3. "projection_depth" : See "projection", however drops assumption of keeping resolution element equidistant in all dimension and uses **res_depth** instead. Useful if want to vary resolution along line of sight for a projection plot. Defined in Fig.do_Projection.
4. "integral" : Integrates the variable along the line of sight, i.e. sum(variable, axis=line of sight) * length of cuboid along line of sight. Defined in FigureMove.do_Projection with sumAlongAxis=True.
5. "pixel_integral" : Reduces number of data cubes to single value, i.e. pixelates the image. Parameter _group_ controls the number of image pixel that will be reduced with function function defined as _PixIntRetu_ . Defined in Fig.do_PixelIntegral while Fig.getGaussianFit contains the defined methods of reducing the data.
6. "isolate_features" : Give relative difference between image and smoothed image. Used to automatically detect contours of x-ray cavities in images. Defined in feature_extraction.do_IsolateFeatures .
7. "rotation_measure" : Compute Faraday rotation measure integral, i.e. electron number density * magnetic field along line of sight * length along line of sight. **variable** should correspond to the magnetic field component along line of sight. Internally will automatically use electron number density. If ism model of SpringelHernquist2003 is active will default to electron number density of warm phase, which should dominate RM signal. Defined in Fig.do_RMmap.
8. "projection_twice" : Will compute two projections, where first one uses specified variable, weight and snapshot_number while second one uses variable, weight and snapshot_number as specified in parameter "ProjTwiceVariable" "ProjTwiceWeight" and "ProjTwiceSnapNum", respectively. Defined in Fig.do_ProjectionTwice.
9. "sz_map" : Computes maps based on vanilla Sunyaev–Zeldovich signal, where **variable** corresponds to the used variable and temperature aux.get_Temperature_inkV is used by default. Defined in Fig.do_SZmap.
10. "sz_effect" : Computes maps of Sunyaev–Zeldovich signal assuming different plasma compositions including relativistic, thermal and kinetic effects. Needs to be tested before use! Defined in SunyaevZeldichSigal.get_SZEffect.
11. "sum_projection" : Computes projection individually for dic of arrays and sums them afterwards. Very special use cases only! Defined in multi_show_data directly.
12. "external" : Allows to direcly load in pre-computed 2D data points, where the filename needs to be located in the folder of the simulation and **variable** corresponds to the filename (without extension) of the file with extension .txt. Defined in multi_show_data directly.

### Quiver plots:
To add a quiver plot on top of the variable plot, one needs to set **quiver**=True, other parameters setting **plot_type**, resolution, etc. have the same name with the additional __quiver__ prefix.
For example,
```shell
python multi_show_main.py --folder _mysim --variable _ElectronNumberDensity --quiver 1 --quiver_variable _Velocity
```
would plot the electron number density as defined in the macro "_ElectronNumberDensity" (defined in file variable.yaml) overplotted with a quiver plot of the velocity via the "_Velocity" macro (defined in file quiver_variable.yaml).

The analogous parameters for quiver plots and their counterparts in the vanilla image plot include

*  quiver_info  &rarr; info
*  quiver_variable  &rarr; variable
*  quiver_variable_name  &rarr; variable_name (only relevant when **quiver_colorbar**=True, with colorbar shown on top)
*  quiver_variable_name_units  &rarr; variable_name_units (only relevant when **quiver_colorbar**=True, with colorbar shown on top)
*  quiver_weight  &rarr; weight 
*  quiver_box_3d  &rarr; box_3d 
*  quiver_res  &rarr; res 
*  quiver_res_depth  &rarr; res_depth 
*  quiver_vmin  &rarr; vmin 
*  quiver_vmax  &rarr; vmax 
*  quiver_plot_type  &rarr; plot_type 
*  quiver_depth  &rarr; depth 
*  quiver_depth_max  &rarr; depth_max 
*  quiver_log  &rarr; log 
*  quiver_colormap  &rarr; colormap (only relevant when **quiver_color**=None otherwise defined color in **quiver_color** used) 

Additional quiver specific parameters are
 
* quiver_type: "quiver" (=arrows), "streamplot" (=stream lines), "lic" (=line integral convolution, see [lic](https://lic.readthedocs.io/en/latest/)
* quiver_normalization: "None" (no normalization applied), "max"/"average" deprecated as comparing values across different simulations becomes impossible
* quiver_scale: deprecated
* quiver_alpha: sets transparency; 0 &rarr; fully transparent, 1 &rarr; fully opaque
* quiver_color: sets color of quiver plot, overwrites setting of **quiver_colormap**
* quiver_skip_n: integer value, skips plotting n-values from grid with resolution defined in **quiver_res**/**quiver_res_depth**
### Contour plots:
To add a contour plot on top of the variable plot, one needs to set **contour**=True, other parameters setting **plot_type**, resolution, etc. have the same name with the additional __contour__ prefix.
For example,
```shell
python multi_show_main.py --folder _mysim --variable _ElectronNumberDensity --contour 1 --contour_variable _XrayEmissivityChandraIsolated
```
would plot the electron number density as defined in the macro "_ElectronNumberDensity" (defined in file variable.yaml) overplotted with a quiver plot of the xray emissivity via the "_XrayEmissivityChandraIsolated" macro (defined in file contour_variable.yaml).

The analogous parameters for contour plots and their counterparts in the vanilla image plot include

*  contour_info  &rarr; info
*  contour_variable  &rarr; variable
*  contour_weight  &rarr; weight 
*  contour_box_3d  &rarr; box_3d 
*  contour_res  &rarr; res 
*  contour_res_depth  &rarr; res_depth 
*  contour_plot_type  &rarr; plot_type 
*  contour_depth  &rarr; depth 
*  contour_depth_max  &rarr; depth_max 

Additional contour specific parameters are
 
* contour_levels: list of floats (in ascending order) that specify contour levels to plotted (must match number of colors unless only single **contour_colors** specified)
* contour_colors: list of colors corresponding to levels defined in **contour_levels** (must match number of colors unless only single **contour_colors** specified)
* contour_linewidth: line width of contour lines
* contour_filter: 1&rarr;"median", 2&rarr;"gaussian", 3&rarr;"zoom" where corresponding integer parameter value of ndimage functions is specified as filternum in the form; filterFILTER_filternumFILTERNUM, e.g. filter2_filternum2 
* contour_ellipse: outlines fitted ellipses should only be used in conjunction with **contour_plot_type**=isolate_features

### Iterator to efficiently vary (multiple) parameters across plots
To vary a parameter across figures, one can use **iterator** to first specify the parameter and its desired values.  

### Parallelization and **threads**
The code is parallelized in two distinct ways. First, costly functions like computing slices or grids are parallelized and threads assigned to this process are defined via **threads**. On the other hand, plotting of individual images can be done in parallel. **threads_main** defines the number of figures computed in parallel. Note, it does not make sense for **threads_main** * **threads** to exceed the total threads count of the system. 
When plotting a large quantity of figures with a single command, e.g. movies or using **iterate**, usually one should set **threads_main** to the number of available threads and **threads**=1. However, creating large grids, e.g. for plots of the x-ray emissivity at high resolution may overwhelm memory capacity so a more balanced approach with higher values of **threads** and lower values for **threads_main** may be necessary.

### INFO or variable specific parameters of physical quantities
To allow for a certain flexibility in free parameters which do not necessarily warrant new keyword arguments, a so-called INFO (or "info") dictionary is used throughout the code. If the parameter relevant for a function exists in INFO it will be used otherwise the default parameters specified in the individual function is used. Let's take a look at auxiliary_functions.get_VelocityBelowXkeV:
```python
def get_VelocityBelowXkeV(snap, considering_h=False, INFO=None, no_conversion=True):
    # calculates velocity in x direction in cm/s
    Velocity = np.zeros(np.shape(snap.data["vel"][snap.data["type"] == 0][:, :]))
    temperature = get_Temperature_inkV(snap)
    thresh = checkIfInDictionary("ThreshVelKeV", INFO, 20)
    mask = temperature < thresh  # in keV
    for i in range(3):
        Velocity[:, i][mask] = snap.data["vel"][snap.data["type"] == 0][:, i][mask]
    if no_conversion:
        Unit_Vel = 1.0
    else:
        Unit_Vel = get_UnitsVelocity(snap, INFO)
    return Velocity * Unit_Vel
```
Here, _ThreshVelKeV_ is a possible parameter defined in INFO, its default value is 20. For multi_show_main.py, these info parameters can be defined in a global dictionary as **info**. It is however recommended to specify these parameters after the parameters is corresponds to. In the case of **weight** or **variable** parameters can then be attributed to the specific call of the variable instead of global parameters in the INFO dictionary. This is for example implemented in the variable macro _RMICMJet: 
```yaml
- RMICMJet:
    - variable:
           - MagneticFieldY_INDIVIDUAL::locationGrid:BJI::axisOfInt:1::locationGridneedHitJet:1
    - weight:
          - None
    - variable_name:
          - \\mathrm{RM}_{\\mathrm{ICM}+\\mathrm{jet}}
    ...
```
The parameters are list after the underscore where INDIVIDUAL signifies that they should only be added to the INFO dict of the current image (Note, in general the INFO dictionary is valid for all images in the plot.). Different parameters are separated by two colons and parameters from parameter value by a single colon.
### Rotations:
Rotations are implemented with respect to an axis in the simulation box in units of degrees. Three rotations around three different axis are possible. In order of execution these are: **rotation_early** around axis **rotation_axis_early**, **rotation** around axis **rotation_axis** and **rotation_late** around axis **rotation_axis_late**.
To plot _ElectronNumberDensity at 10 Myr of our default simulation which is rotated around the y-axis by 90 degrees can be achieved via
```shell
python multi_show_main.py --folder _mysim --snapshot_number 10 --variable _ElectronNumberDensity --rotation_axis [0,1,0] --rotation 90
```
One may want to show the same simulation at the same time from different angles. As there is no index defined for rotation, one should just specify the same **snapshot_number**, **folder**, or **variable** depending on the situation. 
To plot _ElectronNumberDensity at 10 Myr of our default simulation which is rotated around the y-axis by (30, 60, 90) degrees can be achieved via
```shell
python multi_show_main.py --folder _testl _testl _testl --snapshot_number 10 --variable _ElectronNumberDensity --rotation_axis [0,1,0] --rotation 30 60 90 x --index_snapshot_number y --index_folder x --index_variable all
```
Here we just called the same folder multiple times and varied rotation along the same axis as the folder (x-axis). See "Indexes to specify what is varied along axis of plot
" for more details.
### Shortcuts and conventions:
#### Negative numbers
In general, negative numbers are not directly recognized by the script and will throw an error. Use "n","N","m" or "M" in front of numbers instead of minus sign to denote negative numbers, e.g. -1 &\arr;
#### --**snapshot_number**
A few shortcuts exist to simplify specifying times/numbers of snapshots. Those are currently:

* IT: use
### Tips for implementing new features:
### Additional  parameters:
Additional general parameters and their function are listed below:

* variable_factor: finalized plot data will be multiplied by this factor, use with caution ...
* data_manipulation: None or "smooth", optional postprocessing of variable plot
* ic: should be set to True if snapshots are initial conditions where density=mass, warning: if no volume specified usually not possible to get meaningful plots with this script
* use_weight_thresholds: Sets the minimum value of the **weight** quantity to the predefined values in FigureMove.getWeights, which sets a background floor and prevents values of infinity in for example projection plots.
* res_critical: maximum resolution value in any dimension used to compute 3D grids via snap.mapOnCartGrid (from arepo-snap-util) to prevent crashes. If desired grids cannot be computed with single call, maximum sized grids (as prescribed by **res_critical**) are stitched together.
* variable_info: not in use currently, should be used to define parameters specific to **plot_type** while currently **info** is used for this
* quiver_info: used for parameters specifying details of quiver plot as defined via **quiver_plot_type**
* contour_info: used for parameters specifying details of quiver plot as defined via **contour_plot_type**
* vmin_mapping: "default", "min": defines behavior when **vmin** unspecified, "default": vmin = three orders magnitude below vmax, "min": min value of data
* model_vmin_vmax: allows for dynamical update of **vmin** and **vmax**
* fields_loaded: specify which fields of snapshot to load, usually unnecessary as default lazy load will load on demand anyway
* use_time: default is True, calls individual snapshots via **snapshot_number** by their time in Myr, if False use number in name of snapshot 
* difference_time: when **use_time**=True gives maximum difference in time of shown snapshot to desired time value (as defined in **snapshot_number**) 
* follow: dynamical update of **center**, partly deprecated, currently only "focus_upper" fully implemented which centers image on upper half of image to _focus_ on bubble/jet 
* data_plot: deprecated, supposed to allow for different visualization styles other than matplotlib.pyplot.pcolormesh 
* save_folder: general path to folder where plots are saved (file will be saved in save_folder+save_subfolder)
* save_subfolder: more specific sub path to folder where plots are saved (file will be saved in save_folder+save_subfolder)
* save_file_type: "pdf", file type of figure
* common_vmin_vmax_axis: defines axis (xy, x, all) for which same vmin/vmax are used. Automatically picks correct vmin/vmax taking into consideration data from all plots along these axis. Desired **common_vmin_vmax_axis** is picked automatically mostly based on **index_variable** (see PlotObjects.find_common_vmin_vmax_axis for details).

Parameters mostly governing display of figure:

* scale_bar: length in units specified in **box_units** of scale bar, set to None to remove
* scale_bar_position: "default" (="upperright"), "upperright", "lowerleft", "lowerright", "centerleft": position in plot of scale bar
* scale_bar_color: None (=**text_color**) or color value: specifies color of scale bar and its text
* scale_bar_show_label: boolean, allows hiding of scale bar label
* draw_object: ""ellipse", allows drawing of arbitrary shapes with (see PlotElement._draw_objects for details)
* show_colorbar: default True, usually only turned off for outreach figures
* rasterized: determined if pcolormesh is plotted with parameter rasterized on
* show_data_setup: specify setups in gridplot_setups.py with values displayed on image
* show_data: "left", position of values from setups defined in **show_data_setup**
* show_time: "never" (time never shown), "once" (show once for any different time), "always" (show on every image); frequency of time displayed per figure
* show_folder: "never" (folder never shown), "once" (show once for any different folder), "always" (show on every image); frequency of folder displayed per figure
* time_format: "integer"/"int" or "integer10"/"int10" (round(value, -1)); format of time value
* xlim: list with minimum and maximum position along x-axis (positions are between [0,inf] not relative to **center**)
* ylim: list with minimum and maximum position along y-axis (positions are between [0,inf] not relative to **center**)
* text_color: color of text in figure of **variable_name**, **variable_name_units**, time, **name_sim**, ...
* text_style: "border"; allows to add borders to the text to make it pop more
* text_top: list of strings added on top of image rows
* fontsize: fontsize of text
* dpi: dots-per-inch of saved figure
* length_colorbar_percent: length of colorbar with relative to total length of image
* width_colorbar: float between [0,1]; relative colorbar with with respective to size of figure
* colorbar_offset: float between [0,1]; relative offset between image and colorbar with respective to size of figure
* text_position: "top"/"bottom", "left"/"right" and superpositions thereof, e.g. "topleft"; position of text of folder
* text_position_time: "top"/"bottom", "left"/"right" and superpositions thereof, e.g. "topleft"; position of text of time
* verbose: how verbose prints when plotting -> verbose=0 should give minimal output
* show_plot: shows plot after saving if backend supports this
* parameter_as_index: supposed to allow arbitray index based on any parameter; not fully implemented though
* identifier_process: plot will be saved with unique id, for debugging only
* check_file_exists: will check if file with same name currently exists, will only plot if it doesn't, useful when plotting movies
## Radprof.py :

The script plots radials profiles of arepo snaphots. Multiple axes per figure distinguished by variable, folder are possible. 

### First example:
The path to simulations have to included as a macro in folder and will be called analogously as for running multi_show_main.py . We again will use the macro mysim saved in folder.yaml (see above for details on macros).
Run the following command to plot a radial profile of the number density of the simulation specified in _mysim at 10 Myr:
```shell
python Radprof.py --folder _mysim --setup ne --numbers 10
```
The details of the physical quantity being plotted are defined in setups, which can be called by name, e.g. "ne", or number, e.g. "1" (for "ne"). They are specified in the file Radprof.py itself. Different snapshots of the simulation are differentiated by the run time of the respective snapshot and specified as a list after the argparse argument **numbers**.
# Matplotlib backend:
Matplotlib can use different backends that draw figures. Some are also able to visualize an interactive the plot from the terminal. To display currently used backend type;
```python
import matplotlib as mpl
mpl.get_backend()
```
On the cluster, used backends should usually not be able to display plots especially in screen sessions. That is why the backend _Agg_ is used by default. Check for 
```python
import matplotlib as mpl
mpl.use("Agg")
```
to change this setting. Backends based on qt seem to cause issues. However 
```python
import matplotlib as mpl
mpl.use("TkAgg")
```
works with recommanded library versions to interactively display plots. For debugging purposes it may be useful to use this to check the current state of the figure by calling
```python
import matplotlib.pyplot as plt
plt.show()
```