import multiprocessing

import matplotlib as mpl

mpl.use("Agg")
HITOMIPAPER = False
PRESENTATION = True
if PRESENTATION:
    time_formatter = lambda t: round(t, -1)
else:
    time_formatter = lambda t: round(t)
import auxiliary_functions as aux
import FigureMove as Fig
import numpy as np
import matplotlib.pyplot as plt
import Param as param
import os.path
import matplotlib.colors as colors
import matplotlib.cm as cmx
import time as TimeModule
import argparse
import argparse_helpers
import datetime
import script_helpers
import Multithread as mt
import AxesGeneration
import dictionary_modulation
import warnings
from matplotlib.ticker import (
    AutoMinorLocator,
    LogLocator,
    LogFormatterSciNotation,
    NullFormatter,
    LogFormatter
)


def is_none(t):
    return t == [None] or t is None


def get_index_list(_index, _list):
    mask = [True if _index == x else False for x in _list]
    if sum(mask) != 1:
        raise Exception(f"expected single hit: {mask}, for index: {_index} in _list")
    print(f"np.arange(0, len(mask))[np.array(mask)] {np.arange(0, len(mask))[np.array(mask)]}")
    return (np.arange(0, len(mask))[np.array(mask)])[0]


def get_from_input_list(input_list, key, keep_shape=True):
    if keep_shape:
        return [x[key] for x in input_list]
    else:
        return [y[key] for x in input_list for y in x]


def get_colors(
    number_plots,
    single_color_gradient=False,
    folder_color_gradient=False,
    number_folders=None,
    numbers=None,
    sample_colormap=False,
):
    scalarMap = None
    if single_color_gradient:
        color = aux.linear_gradient("#010f16", "#9dd5f1", n=number_plots)
    elif folder_color_gradient:
        print(f"number folders {number_folders}")
        if number_folders is None:
            raise Exception(
                "need to specify number folders to determine  colors for 'folder_color_gradient'! "
            )
        color_beg = ["#1e3607", "#4d0101", "#172e46", "#55492d", "#2c0a2b", "#3e3130"]
        colo_end = ["#c9dbb8", "#cc9a9a", "#a8b8c7", "#d4c9ac", "#b29fb1", "#cbc0bf"]
        color = []
        for i in range(number_folders):
            color.append(aux.linear_gradient(color_beg[i], colo_end[i], n=number_plots))
    elif sample_colormap:
        cm = plt.get_cmap("plasma")  # magma # gist_rainbow # plasma
        cNorm = colors.Normalize(vmin=min(numbers), vmax=max(numbers))
        scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)
        color = [scalarMap.to_rgba(x) for x in numbers]
    else:
        color = [
            "#5b9bd5",
            "#c74634",
            "#7aa998",
            "#ffc000",
            "#bb596d",
            "#199967",
            "#2565ca",
            "#E4A41A",
            "#ff3232",
            "#201104",
            "#664200",
            "#0e2850",
            "#664200",
            "#54784c",
            "#0b3f00",
            "#031200",
        ]  # 'black',
    if folder_color_gradient:
        if len(color[0]) < number_plots:
            raise Exception(f"need to define more (!{number_plots}) colors: {color}!")
    elif len(color) < number_plots:
        raise Exception(f"need to define more (!{number_plots}) colors: {color}!")
    return color, scalarMap


FieldsToLoad = None
INFO = {}

Test = False
default_folder = "_testt"
default_numbers = [0, 400, 800]
if os.path.exists("/home/kristian/Analysis/") or os.path.exists(
    "/home/kristian/analysis/"
):
    print("using test!")
    Test = True

start_time = TimeModule.time()
parser = argparse.ArgumentParser(
    description="grid plots of different variables!",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    f"--folder", type=argparse_helpers.fancy_eval, nargs="+", default=[default_folder]
)
parser.add_argument(
    f"--setup",
    type=argparse_helpers.fancy_eval,
    nargs="+",
    # default=[0.2, 1.1, 0.43, 0.23, 0.6, 0.3, 0.44, 0.1, 1, 0, 0.22, 9.1,],
    default=[
        "coolingtime",
        "density",
        "XrayCoolingFreeFallTimeRatioxray",
        "coolingtimexray",
        "starformation",
        "freefalltime",
        "mincoolingtime",
        "entropy",
        "ne",
        "temperature",
        "coolingtimemass",
        "velocity",
    ],
)
parser.add_argument(
    f"--numbers", type=argparse_helpers.fancy_eval, nargs="+", default=default_numbers,
)
parser.add_argument(f"--xmin", type=argparse_helpers.fancy_eval, default=[None])
parser.add_argument(f"--xmax", type=argparse_helpers.fancy_eval, default=[None])
parser.add_argument(f"--ymin", type=argparse_helpers.fancy_eval, default=[None])
parser.add_argument(f"--ymax", type=argparse_helpers.fancy_eval, default=[None])
parser.add_argument(f"--xmin_show", type=argparse_helpers.fancy_eval, default=[None])
parser.add_argument(f"--xmax_show", type=argparse_helpers.fancy_eval, default=[None])
parser.add_argument(
    f"--nbins", type=argparse_helpers.fancy_eval, nargs="+", default=[80]
)
parser.add_argument(f"--numbers_min", type=argparse_helpers.fancy_eval, default=[0])
parser.add_argument(f"--numbers_max", type=argparse_helpers.fancy_eval, default=False)
parser.add_argument(f"--numbers_n", type=argparse_helpers.fancy_eval, default=8)
parser.add_argument(f"--numbers_step", type=argparse_helpers.fancy_eval, default=[None])
parser.add_argument(
    f"--time_average_range", type=argparse_helpers.fancy_eval, default=[None]
)
parser.add_argument(f"--fontsize", type=argparse_helpers.fancy_eval, default=[None])
parser.add_argument(
    f"--locFolderLegend", type=argparse_helpers.fancy_eval, default="lower left"
)
parser.add_argument(f"--threads", type=argparse_helpers.fancy_eval, nargs=1, default=8)
parser.add_argument(
    f"--differencetime", type=argparse_helpers.fancy_eval, nargs=1, default=4
)
parser.add_argument(f"--multi_folder_plots", action="store_true", default=False)
parser.add_argument(
    f"--split_setups_in_panels",
    type=argparse_helpers.fancy_eval,
    default=["foldersetup"],
)
parser.add_argument(
    f"--single_ylabel", type=argparse_helpers.fancy_eval, default="default"
)
parser.add_argument(
    f"--single_xlabel", type=argparse_helpers.fancy_eval, default="default"
)

parser.add_argument(
    f"--index_folder", type=argparse_helpers.fancy_eval, default="x"
)
parser.add_argument(
    f"--index_setup", type=argparse_helpers.fancy_eval, default="y"
)
parser.add_argument(f"--logx", type=argparse_helpers.fancy_eval, default="default")
parser.add_argument(f"--logy", type=argparse_helpers.fancy_eval, default="default")
parser.add_argument(
    f"--show_single_folder", type=argparse_helpers.fancy_eval, default=False
)
parser.add_argument(f"--force_xn", type=argparse_helpers.fancy_eval, default=False)
parser.add_argument(
    f"--colorbar_horizontal_offset", type=argparse_helpers.fancy_eval, default=[0.08]
)
parser.add_argument(
    f"--width_colorbar_horizontal", type=argparse_helpers.fancy_eval, default=[0.04]
)
parser.add_argument(
    f"--length_colorbar_horizontal_percent",
    type=argparse_helpers.fancy_eval,
    default=[0.8],
)
parser.add_argument(
    f"--sample_colors_from_colorbar", type=argparse_helpers.fancy_eval, default=[True]
)
parser.add_argument(
    f"--colors_to_sample_from_colorbar",
    type=argparse_helpers.fancy_eval,
    nargs="+",
    default=[None],
)
parser.add_argument(
    f"--axes_text", type=argparse_helpers.fancy_eval, nargs="+", default=[None],
)
parser.add_argument(
    f"--axes_text_position", type=argparse_helpers.fancy_eval, default="upper left"
)
parser.add_argument(
    f"--overplot_folder", type=argparse_helpers.fancy_eval, default=[None]
)
parser.add_argument(
    f"--overplot_plot_color", type=argparse_helpers.fancy_eval, default=[None]
)
parser.add_argument(
    f"--overplot_plot_tickstyle", type=argparse_helpers.fancy_eval, default=[None]
)
parser.add_argument(
    f"--overplot_plot_alpha", type=argparse_helpers.fancy_eval, default=[0.7]
)
parser.add_argument(
    f"--overplot_folder_location_folder", type=argparse_helpers.fancy_eval, default=[None]
)
parser.add_argument(
    f"--overplot_folder_include_times", type=argparse_helpers.fancy_eval, default=[["all"]]
)
parser.add_argument(
    f"--number_max_y_ticks", type=argparse_helpers.fancy_eval, default=[4]
)
parser.add_argument(
    f"--number_max_x_ticks", type=argparse_helpers.fancy_eval, default=[2]
)
parser.add_argument(
    f"--use_own_ticks_x", type=argparse_helpers.fancy_eval, default=[False]
)
parser.add_argument(
    f"--use_own_ticks_y", type=argparse_helpers.fancy_eval, default=[False]
)
input = parser.parse_args().__dict__.copy()
print(input)
input = {k: v for k, v in input.items() if v is not None}
script_helpers.get_parameters_from_yaml(input)
print(input)
Folders = input["folder"]
Roots = input["root"] if "root" in input.keys() else ""
NameOfSimSave = input["name_sim_save"] if "name_sim_save" in input.keys() else "test"
NameOfSim = input["name_sim"] if "name_sim" in input.keys() else "test"
Ymin = input["ymin"][0]
Ymax = input["ymax"][0]
# index_folder = input["index_folder"][0]
# index_setup = input["index_setup"][0]
colorbar_horizontal_offset = input["colorbar_horizontal_offset"][0]
length_colorbar_horizontal_percent = input["length_colorbar_horizontal_percent"][0]
width_colorbar_horizontal = input["width_colorbar_horizontal"][0]
single_ylabel = input["single_ylabel"][0]
single_xlabel = input["single_xlabel"][0]
logX = input["logx"][0]
logY = input["logy"][0]
show_single_folder = input["show_single_folder"][0]
force_xn = input["force_xn"][0]
XminShow = input["xmin_show"][0] if input["xmin_show"][0] is not None else 8e-1
XmaxShow = input["xmax_show"][0] if input["xmax_show"][0] is not None else 8e2
Xmin = input["xmin"][0] if input["xmin"][0] is not None else XminShow * 0.9
Xmax = input["xmax"][0] if input["xmax"][0] is not None else XmaxShow * 1.1
NBins = input["nbins"][:][0]
SETUPS = input["setup"]
threads = input["threads"][0]
fontsize = input["fontsize"][0]
differenceTime = input["differencetime"][0]
Numbers = input["numbers"]
locFolderLegend = input["locFolderLegend"][0]
sample_colors_from_colorbar = input["sample_colors_from_colorbar"][0]
colors_to_sample_from_colorbar = input["colors_to_sample_from_colorbar"]
if colors_to_sample_from_colorbar == [None]:
    colors_to_sample_from_colorbar = None
split_setups_in_panels = input["split_setups_in_panels"][0]
if split_setups_in_panels == False:
    split_setups_in_panels = None
multi_folder_plots = input["multi_folder_plots"][0]
if split_setups_in_panels is not None and "folder" in split_setups_in_panels:
    multi_folder_plots = True
if single_xlabel == "default":
    single_xlabel = True
    if split_setups_in_panels == "setup":
        single_xlabel = False
if single_ylabel == "default":
    single_ylabel = True
    if split_setups_in_panels == "setup":
        single_ylabel = False
logX_reset = False
if logX == "default":
    logX_reset = True
logY_reset = False
if logY == "default":
    logY_reset = True
single_folder_plots = not multi_folder_plots
numbers_min = input["numbers_min"][0]
numbers_max = input["numbers_max"][0]
numbers_n = input["numbers_n"][0]
numbers_step = input["numbers_step"][0]
time_average_range = input["time_average_range"][0]
axes_text = input["axes_text"]
axes_text_position = input["axes_text_position"][0]
overplot_folder = input["overplot_folder"]
overplot_folder_location_folder = input["overplot_folder_location_folder"]
overplot_plot_color = input["overplot_plot_color"][0]
overplot_plot_tickstyle = input["overplot_plot_tickstyle"][0]
overplot_plot_alpha = input["overplot_plot_alpha"][0]
overplot_folder_include_times = input["overplot_folder_include_times"]
xignoreLinTicks = not input["use_own_ticks_x"][0]
use_own_x_ticks = not xignoreLinTicks
yignoreLinTicks = not input["use_own_ticks_y"][0]
numberMaxLinXTicks = input["number_max_x_ticks"][0]
numberMaxLinYTicks = input["number_max_y_ticks"][0]

if not isinstance(overplot_folder_include_times[0], list):
    overplot_folder_include_times = [overplot_folder_include_times]
assert len(overplot_folder) == len(overplot_folder_location_folder) == len(overplot_folder_include_times)
del input
if (numbers_min or numbers_min == 0) and numbers_max != False:
    if numbers_step is None:
        Numbers = np.linspace(numbers_min, numbers_max, numbers_n)
    else:
        Numbers = np.arange(numbers_min, numbers_max, numbers_step)
radprof_style = "default"
time_average_style_error = "percentiles"
time_average_style_error = "notnanpercentiles"
time_average_style_error = "zerospercentiles1sigma"
time_average_style = "mean"
time_average_style = "zerosmedian"
time_average_style = "zerosmean"
if time_average_range is not None:
    radprof_style = "time_average"
ymax_reset = False
if Ymax is None:
    ymax_reset = True
ymin_reset = False
if Ymin is None:
    ymin_reset = True
print(f"numbers_min: {numbers_min}")
# exit()
otherAlpha = 0.5
plotLegend = True
plotLegendFolderExternal = False
plotLegendExternal = False
showLegendTime = True
showLegendTimeOnlyFirstLast = False
showAllLabels = False

showTime = True
showVariable = False
showWeight = False
showAverageStyle = False
variableLabel = None

alphaErr = 0.1
MiniPictures = False
vertical_pictures = False
square_pictures = True
shorter_vertical_pictures = False
if PRESENTATION:
    vertical_pictures = True
    square_pictures = False

Smooth = False
SmoothN = 200
SameNumberPointsPerBin = False
SmoothLimit = None


framealpha = 0.6
# what to plot

plotTexts = False
Texts = ["Vol change", "Vol change", "Mesh chg", "Mesh chg", "Mesh chg"]
factxt = 0.67 * 100
Height = 0.06
Positions = [
    [5.83 / factxt, Height * 0.9],
    [25.67 / factxt, Height * 0.9],
    [4.93 / factxt, Height * 1.1],
    [13.37 / factxt, Height],
    [34.74 / factxt, Height],
]
colorTxt1 = "#2565ca"
colorTxt2 = "#0e2850"
ColorsText = [colorTxt1, colorTxt1, colorTxt2, colorTxt2, colorTxt2]
set_color_black_only = False

hlines = None
vlines = None

TickStyleTheory = [">", "<", "x"]
colorTheory = Fig.get_colors_grouped([6])
alphaTheory = 1
extraSpecificInfo = ""
if not type(Folders[0]) == list:
    if single_folder_plots:
        Folders = [[x] for x in Folders]
        NameOfSim = [[x] for x in NameOfSim]
        NameOfSimSave = [[x] for x in NameOfSimSave]
        Roots = [[x] for x in Roots]
    else:
        Folders = [Folders]
        NameOfSim = [NameOfSim]
        NameOfSimSave = [NameOfSimSave]
        Roots = [Roots]
input_list = []
index_folder = 0


def get_filename(reduce_length=False, minimize_length=False):
    addInfo = ""
    if numberSetup == 8:
        addInfo += "xFreq%s_" % aux.ReplaceDotInName(INFO["xFreq"], "%.2g")
    else:
        addInfo += "Var-%s_" % aux.convert_params_to_string(Variables, string=True)
        if postprocess is not None:
            addInfo += "%s_" % aux.convert_params_to_string(
                postprocess, string=True
            )
    addInfo += "Wei-%s_" % aux.convert_params_to_string(Weights, string=True)
    if reduce_length:
        lis = aux.strings_reduce_by_matches(NameOfSimSave_single_plot, min_similarity=0.8)
        print(f"lis: {lis}")
        for l in lis[0] + lis[1]:
            addInfo += l
    elif minimize_length:
        addInfo += "TOOMANYFILENAMESTOSHOW"
    else:
        print(f"NameOfSimSave_single_plot: {NameOfSimSave_single_plot}")
        addInfo += "%s_" % aux.convert_params_to_string(
            NameOfSimSave_single_plot, string=True
        )
    if (numbers_min or numbers_min == 0) and numbers_max != False:
        if numbers_step is None:
            addInfo += f"NU{numbers_min}-{numbers_max}-{numbers_n}_"
        else:
            addInfo += f"NU{numbers_min}-{numbers_max}-{numbers_step}_"
    else:
        addInfo += "%s_" % aux.convert_params_to_string(Numbers, string=False)
    addInfo += "Av%s_" % aux.convert_params_to_string(AverageStyle, string=True)
    addInfo += "Er%s_" % aux.convert_params_to_string(Errorbars, string=True)
    if radprof_style == "time_average":
        addInfo += "TA%iV%sE%s_" % (
            time_average_range,
            time_average_style,
            time_average_style_error,
        )
    if Smooth:
        addInfo += "Smooth%i_" % (SmoothN)
    if SameNumberPointsPerBin:
        addInfo += "BinSamNumPoi_"
    if aux.checkExistenceKey(INFO, "useTelBandInt", False):
        if not INFO["useTelBandInt"]:
            addInfo += "NOTBandInt_"
    if aux.checkExistenceKey(INFO, "AbsoluteValue", False):
        addInfo += "ABS_"
    if not plotLegend:
        addInfo += "noLeg_"
    addInfo += "%s" % extraSpecificInfo
    name = "../figures/Radprof/%s/Radprof_%sNbins%i.pdf" % (
        str(numberSetup),
        addInfo,
        NBins,
    )
    return name


for index_folder_outer, (
    Roots_single_plot,
    Folders_single_plot,
    NameOfSim_single_plot,
    NameOfSimSave_single_plot,
) in enumerate(zip(Roots, Folders, NameOfSim, NameOfSimSave)):
    Folders_single_plot = [
        x + y for x, y in zip(Roots_single_plot, Folders_single_plot)
    ]
    for index_setup, numberSetup in enumerate(SETUPS):
        Selects = None
        TickStyle = ["-", ":", "-.", "--", "o", "x", "^", "v", "h", "h", "+", "h", "D"]
        single_color_gradient = False
        folder_color_gradient = False
        if True:
            folder_color_gradient = True
        if ymin_reset:
            Ymin = None
        if ymax_reset:
            Ymax = None
        if logX_reset:
            logX = True
        if logY_reset:
            logY = True
        alpha = 1
        plotICsInsteadZero = False
        # Xmin = 1e0
        locTimeLegend = "upper right"
        locRestLegend = "best"
        ICsFolder = None
        noXtickLabels = False
        showLegendTime = True
        showLegendFolder = True
        showVariable = False
        Xlabel = r"$r\ [\mathrm{kpc}]$"
        postprocess = None
        twinXfactor = None
        twinXlabel = None
        TwinX = False
        twinYfactor = None
        twinYlabel = None
        TwinY = False

        minorXTicks = True
        minorYTicks = False
        spacingMinorYTicks = spacingMinorXTicks = 10
        reduce_minor_x_ticks = False
        reduce_minor_y_ticks = False
        legendFolderNcol = 1

        if split_setups_in_panels is not None and "folder" in split_setups_in_panels:
            showLegendFolder = True
        IndexColorRest = False
        IndexColorTime = True
        IndexAlphaRest = False
        IndexAlphaTime = True
        IndexTickStyleRest = True
        IndexTickStyleSubRest = True
        IndexTickStyleTime = False
        error_bar_style = "shaded"
        variableLabel = None
        plotLegendExternal = False
        if HITOMIPAPER:
            plotLegendExternal = True
        plotLegend = False
        AverageStyle = "mean"
        Errorbars = "percentiles"  # None
        showFolder = True
        VariablesTheory = []
        hlines = None
        vlines = None
        if numberSetup == 0 or numberSetup == "temperature":
            Ylabel = "T\\ [\\mathrm{keV}]"
            Variables = "TemperatureInkV"
            Weights = "Mass"
            TwinY = True
            # twinYlabel = r"$T\ [\mathrm{K}]$"
            # twinYfactor = 1 / 8.6173303e-08
            # Ymin = None  # 1e-2
            # Ymax = None  # 9e0
            Ymin = 0.2
            Ymax = 9
            # VariablesTheory = ['TemperatureInKevVantyghemMS0735']
        elif numberSetup == 99 or numberSetup == "count":
            Ylabel = "Counts"
            Variables = "One"
            Weights = None
            AverageStyle = "sum"
            postprocess = "CUMSUM"
        elif numberSetup == 0.01 or numberSetup == "temperaturexray":
            Ylabel = "T_\\mathrm{X}\\ [\\mathrm{keV}]"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = "TemperatureInkV"
            Weights = "Mass"
            logY = False
            Ymin = 0.2
            Ymax = 9
        elif numberSetup == 0.02 or numberSetup == "temperaturexraywei":
            Ylabel = "T_\\mathrm{X}\\ [\\mathrm{keV}]"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = "TemperatureInkV"
            Weights = "CoolingLuminosity"
            logY = False
            Ymin = 0.01
            Ymax = 9
        elif numberSetup == 0.1 or numberSetup == "entropy":
            Ylabel = "K\\ [\\mathrm{keV}\\ \\mathrm{cm}^2]"
            Variables = "Entropy"
            Weights = "Volume"
            Ymin = 1e-2 #7e-1
            Ymax = 1e3 #8e2
            hlines = [30]
            if not logX:
                Ymin = 0
                Ymax = 10
        elif numberSetup == 0.11 or numberSetup == "entropytcut":
            Ylabel = "K_\\mathrm{X}\\ [\\mathrm{keV}\\ \\mathrm{cm}^2]"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = "Entropy"
            Weights = "Volume"
            Ymin = 7e-1
            Ymax = 8e2
            hlines = [30]
            if not logX:
                Ymin = 0
                Ymax = 10
        elif numberSetup == 0.111 or numberSetup == "entropyxraywei":
            Ylabel = "K_\\mathrm{X}\\ [\\mathrm{keV}\\ \\mathrm{cm}^2]"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = "Entropy"
            Weights = "CoolingLuminosity"
            Ymin = 2e-1
            Ymax = 8e2
            hlines = [30]
            if not logX:
                Ymin = 0
                Ymax = 10
        elif numberSetup == 0.111 or numberSetup == "entropyxrayweidebug":
            Ylabel = "K_\\mathrm{X}\\ [\\mathrm{keV}\\ \\mathrm{cm}^2]"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = "Entropy"
            Weights = "CoolingLuminosity"
            Ymin = 8e-2
            Ymax = 8e2
            hlines = [30]
            if not logX:
                Ymin = 0
                Ymax = 10
        elif numberSetup == 0.111 or numberSetup == "entropyxrayweidivide":
            Ylabel = "K_\\mathrm{X,divide}\\ [\\mathrm{keV}\\ \\mathrm{cm}^2]"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = [["EntropyTop", "EntropyBottom"]]
            Weights = "CoolingLuminosity"
            postprocess = "DIVIDEBY"
            Ymin = 2e-1
            Ymax = 8e2
            hlines = [30]
            if not logX:
                Ymin = 0
                Ymax = 10
        elif numberSetup == 0.111 or numberSetup == "entropyxrayweidividedebug":
            Ylabel = "K_\\mathrm{X,divide}\\ [\\mathrm{keV}\\ \\mathrm{cm}^2]"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = [["EntropyTop", "EntropyBottom"]]
            Weights = "CoolingLuminosity"
            postprocess = "DIVIDEBY"
            Ymin = 8e-2
            Ymax = 8e2
            hlines = [30]
            if not logX:
                Ymin = 0
                Ymax = 10
        elif numberSetup == 0.13:
            Ylabel = "K_\\mathrm{nosfr}\\ [\\mathrm{keV}\\ \\mathrm{cm}^2]"
            Variables = "Entropy"
            Weights = "Volume"
            Selects = "StarFormationRate==0"
        elif numberSetup == 0.15:
            Ylabel = "K_\\mathrm{X,nosfr}\\ [\\mathrm{keV}\\ \\mathrm{cm}^2]"
            Variables = "Entropy"
            Weights = "CoolingLuminosity"
            Selects = "StarFormationRate==0"
        elif numberSetup == 0.19 or numberSetup == "xray":
            Ylabel = "\\epsilon_\\mathrm{X}\\ [cgs]"
            Variables = "XrayEmissivity"
            Weights = "vol"
        elif numberSetup == 0.2 or numberSetup == "timescales":
            Ylabel = "t\\ [\\mathrm{Myr}]"
            variableLabel = [
                "t_\\mathrm{ff}",
                "t_\\mathrm{cool}",
            ]
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = [
                "FreeFallTimeInMyr",
                "CoolingTimescaleInMyr",
            ]
            locRestLegend = "upper right"
            Weights = ["CoolingLuminosity", "CoolingLuminosity"]  # 'Volume'
            locTimeLegend = "upper left"
            AverageStyle = ["mean", "mean"]
            plotLegend = True
            hlines = [1e2, 1e3]
            vlines = [40]
            Ymin = 1e1
            Ymax = 1e4
        elif numberSetup == 0.201 or numberSetup == "timescalesextended":
            folder_color_gradient = False
            Ylabel = "t\\ [\\mathrm{Myr}]"
            variableLabel = [
                "t_{\\mathrm{ff},\\,\\mathrm{X}}",
                "t_{\\mathrm{cool},\\,\\mathrm{X}}",
                # "t_\\mathrm{cool,min}",
            ]
            set_color_black_only = True
            MiniPictures = True
            vertical_pictures = False
            shorter_vertical_pictures = True
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = [
                "FreeFallTimeInMyr",
                "CoolingTimescaleInMyr",
                # "CoolingTimescaleInMyr",
            ]
            Weights = ["CoolingLuminosity", "CoolingLuminosity"]  # , None]  # 'Volume'
            AverageStyle = ["mean", "mean"]  # , "min"]
            plotLegend = True
            # INFO["percentilesLowerBoundary"] = 1 - 0.9773
            # INFO["percentilesUpperBoundary"] = 0.9773
            showLegendFolder = False
            Errorbars = ["percentilesweighted", "percentilesweighted"]  # , None]
            hlines = [1e2, 1e3]
            Ymin = 8e0
            Ymax = 1e4

            # Errorbars = ["percentiles", "percentiles"]  # , None]
            # Weights = [None, None]  # , None]  # 'Volume'
            # Selects = None
        elif numberSetup == 0.21 or numberSetup == "coolingtime":
            Ylabel = "t_\\mathrm{cool}\\ [\\mathrm{Myr}]"
            Variables = "CoolingTimescaleInMyr"
            Weights = "Mass"  # 'Volume'
            hlines = [100, 1e3]
            Ymin = 1
            Ymax = 1e4
        elif numberSetup == 0.211 or numberSetup == "coolingtimenosfr":
            Selects = "StarFormationRate==0"
            Ylabel = "t_\\mathrm{cool}\\ [\\mathrm{Myr}]"
            Variables = "CoolingTimescaleInMyr"
            Weights = "Mass"  # 'Volume'
            hlines = [100, 1e3]
            Ymin = 2e1
            Ymax = 12e3
        elif numberSetup == 0.22 or numberSetup == "tcoolxray":
            # Xmin = XminShow = 10
            Ylabel = "t_{\\mathrm{cool},\\mathrm{Chandra}}\\ [\\mathrm{Myr}]"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = "CoolingTimescaleInMyr"
            Weights = "Mass"  # 'Volume'
            Ymin = 1.1e1
            Ymax = 2e4
            hlines = [1e3, 12e3]
        elif numberSetup == 0.221 or numberSetup == "tcoolxraywei":
            Ylabel = "t_{\\mathrm{cool},\\,\\mathrm{X}}\\ [\\mathrm{Myr}]"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = "CoolingTimescaleInMyr"
            Weights = "CoolingLuminosity"  # 'Volume'
            Ymin = 1.1e1
            Ymax = 2e4
            hlines = [1e3]  # , 12e3]
        elif numberSetup == 0.23 or numberSetup == "coolingtimemass":
            # Xmin = XminShow = 10
            Ylabel = "t_\\mathrm{cool}\\ [\\mathrm{Myr}]"
            Variables = "XrayCoolingTimescaleInMyr"
            Weights = "Mass"  # 'Volume'
            hlines = [100, 1e3]
        elif numberSetup == 0.221 or numberSetup == "mincoolingtimenosfr":
            Variables = "CoolingTimescaleInMyr"
            Ylabel = "t_{\\mathrm{cool},\\mathrm{min}}\\ [\\mathrm{Myr}]"
            Selects = "StarFormationRate==0"
            Weights = None  # "Mass"
            AverageStyle = "min"
            Errorbars = None  # "percentiles"
            Ylabel = "t_{\\mathrm{cool},\\mathrm{min}}"
            hlines = [100, 1e3]
        elif numberSetup == 0.222 or numberSetup == "coolingtimenosfr":
            Variables = "CoolingTimescaleInMyr"
            Ylabel = "t_{\\mathrm{cool}}\\ [\\mathrm{Myr}]"
            Selects = "StarFormationRate==0"
            Weights = "Mass"  # "Mass"
            Ylabel = "t_{\\mathrm{cool}}"
            hlines = [100, 1e3]
        elif numberSetup == 0.23 or numberSetup == "coolingtimexray":
            # Xmin = XminShow = 10
            Ylabel = "t_\\mathrm{cool}\\ [\\mathrm{Myr}]"
            Variables = "XrayCoolingTimescaleInMyr"
            Weights = "Mass"  # 'Volume'
            AverageStyle = "meanABOVE0.2keVBELOW10keV"
            hlines = [100, 1e3]
        elif numberSetup == 0.3 or numberSetup == "freefalltime":
            Variables = "FreeFallTimeInMyr"
            Weights = "Mass"
            Ylabel = "t_\\mathrm{ff}\\ [\\mathrm{Myr}]"
        elif numberSetup == 0.4 or numberSetup == "XrayCoolingFreeFallTimeRatio":
            Variables = "XrayCoolingFreeFallTimeRatio"
            Weights = "Mass"
            Ylabel = "t_\\mathrm{X}/t_\\mathrm{ff}"
        elif numberSetup == 0.43 or numberSetup == "XrayCoolingFreeFallTimeRatioxray":
            Variables = "XrayCoolingFreeFallTimeRatio"
            Weights = None  # "Mass"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Errorbars = None  # "percentiles"
            Ylabel = "t_\\mathrm{X}/t_{\\mathrm{ff},T>0.3\\,\\mathrm{keV}}"
        elif numberSetup == 0.44 or numberSetup == "minxraycoolingtime":
            Variables = "XrayCoolingFreeFallTimeRatio"
            Weights = None  # "Mass"
            AverageStyle = "min"
            Errorbars = None  # "percentiles"
            Ylabel = "t_\\mathrm{X}/t_\\mathrm{ff}"
        elif numberSetup == 0.5 or numberSetup == "mincoolingfreefalltimerationosfr":
            Variables = "CoolingFreeFallTimeRatio"
            Selects = "StarFormationRate==0"
            Weights = None  # "Mass"
            AverageStyle = "min"
            Errorbars = None  # "percentiles"
            hlines = [10]
            Ylabel = "\\mathrm{min}(t_\\mathrm{cool}/t_\\mathrm{ff})"
            Ymin = 2e-1
            Ymax = 2e2
        elif numberSetup == 0.5 or numberSetup == "mincoolingfreefalltimeratioxray":
            Variables = "CoolingFreeFallTimeRatio"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Weights = None  # "Mass"
            AverageStyle = "min"
            Errorbars = None  # "percentiles"
            hlines = [10]
            Ylabel = "\\mathrm{min}(t_\\mathrm{cool}/t_\\mathrm{ff})"
            Ymin = 2e-1
            Ymax = 2e2
        elif numberSetup == 0.501 or numberSetup == "coolingfreefalltimeratioxraywei":
            Variables = "CoolingFreeFallTimeRatio"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Weights = "CoolingLuminosity"  # "Mass"
            hlines = [10]
            Ylabel = "[t_\\mathrm{cool}/t_\\mathrm{ff}]_\\mathrm{X}"
            Ymin = 2e-1
            Ymax = 2e2
        elif (
            numberSetup == 0.51 or numberSetup == "mincoolingfreefalltimerationosfrtest"
        ):
            Variables = "CoolingFreeFallTimeRatio"
            Selects = "StarFormationRate==0ANDCoolingTimescaleInMyr>1"
            Weights = None  # "Mass"
            AverageStyle = "min"
            Errorbars = None  # "percentiles"
            Ymin = 1e-2
            Ymax = 1e3
            Ylabel = "t_\\mathrm{cool}/t_\\mathrm{ff}"
        elif numberSetup == 0.6 or numberSetup == "starformationsum":
            Variables = "StarFormationRate"
            Weights = None  # "Mass"
            Errorbars = None
            AverageStyle = "sum"
            Ylabel = "\\mathrm{SFR}"
        elif numberSetup == 0.61 or numberSetup == "starformation":
            Variables = "StarFormationRate"
            Weights = None  # "Mass"
            Errorbars = "percentiles"
            AverageStyle = "median"
            Ylabel = "\\mathrm{SFR}"
        elif numberSetup == 1 or numberSetup == "ne":
            Variables = "ElectronNumberDensity"
            Weights = "vol"
            Ylabel = "n_{\\mathrm{e},\\mathrm{V}}\\ [\\mathrm{cm}^{-3}]"
            # Xlabel = r''
            plotLegend = True
            locTimeLegend = "upper right"
            Ymin = 2e-3
            Ymax = 2e3
            hlines = [1e-2]
            if not logX:
                Ymin = 5e-3
                Ymax = 14e-1
        elif numberSetup == 1.01 or numberSetup == "nexray":
            Variables = "ElectronNumberDensity"
            Weights = "vol"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Ylabel = "n_{\\mathrm{e},\\,\\mathrm{V,X}}\\ [\\mathrm{cm}^{-3}]"
            Ymin = 2e-3
            Ymax = 2e0
            if not logX:
                Ymin = 5e-3
                Ymax = 14e-2
            hlines = [1e-2]
        elif numberSetup == 1.02 or numberSetup == "nexraywei":
            Variables = "ElectronNumberDensity"
            Weights = "CoolingLuminosity"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Ylabel = "n_{\\mathrm{e},\\,\\mathrm{X}}\\ [\\mathrm{cm}^{-3}]"
            # Ymin = 2e-3
            Ymin = 4e-4
            Ymax = 4e0
            hlines = [1e-2]
            if np.all(Numbers == [10]):
                Ymax = 2e-1
            if not logX:
                Ymin = 5e-3
                Ymax = 14e-2
        elif numberSetup == 1.03 or numberSetup == "nexrayweims":
            Variables = "ElectronNumberDensity"
            Weights = "CoolingLuminosity"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Ylabel = "n_{\\mathrm{e},\\,\\mathrm{X}}\\ [\\mathrm{cm}^{-3}]"
            # Ymin = 2e-3
            Ymin = 4e-4
            Ymax = 4e0
            hlines = [1e-2]
            plotLegend = True
            VariablesTheory = [
                "NumberDensityVantyghemMS0735",
                "ElectronNumberDensityProfile",
                "MS0735fit",
            ]
            if np.all(Numbers == [10]):
                Ymax = 2e-1
            if not logX:
                Ymin = 5e-3
                Ymax = 14e-2
        elif numberSetup == 1.1 or numberSetup == "density":
            # noXtickLabels = True
            Variables = "Density"
            Weights = "vol"
            TwinY = True
            twinYlabel = r"$n_\mathrm{e}\ [\mathrm{cm}^{-3}]$"
            twinYfactor = 1.0 / param.PROTONMASS_cgs * param.ElectronProtonFraction
            Ylabel = "\\rho\\ [\\mathrm{g}\\,\\mathrm{cm}^{-3}]"
            # Xlabel = r''
            locTimeLegend = "upper right"
            # VariablesTheory = ['NumberDensityVantyghemMS0735']
        elif numberSetup == 1.111 or numberSetup == "nems":
            # noXtickLabels = True
            Variables = [["ElectronNumberDensity"], ["ElectronNumberDensity"]]
            postprocess = ["INTERPOLATE-NumberDensityVantyghemMS0735", None]
            Weights = "vol"
            Ylabel = "n_\\mathrm{e}\\ [\\mathrm{cm}^{-3}]"
            # Xlabel = r''
            plotLegend = True
            locTimeLegend = "lower right"
            VariablesTheory = [
                "NumberDensityVantyghemMS0735",
                "ElectronNumberDensityProfile",
                "MS0735fit",
            ]
        elif numberSetup == 1.1111 or numberSetup == "nemsclean":
            # noXtickLabels = True
            Variables = "ElectronNumberDensity"
            postprocess = None
            Weights = "vol"
            Ylabel = "n_\\mathrm{e}\\ [\\mathrm{cm}^{-3}]"
            # Xlabel = r''
            plotLegend = True
            locTimeLegend = "upper right"
            VariablesTheory = [
                "NumberDensityVantyghemMS0735",
                "ElectronNumberDensityProfile",
                "MS0735fit",
            ]
        elif numberSetup == 1.112 or numberSetup == "nemsfrac":
            # noXtickLabels = True
            Variables = [["ElectronNumberDensity", "NumberDensityVantyghemMS0735"]]
            Weights = "vol"
            Ylabel = "\\eta_{n_\\mathrm{e}}"
            # Xlabel = r''
            # postprocess = "DIVIDEBY"
            postprocess = "INTERPOLATE-NumberDensityVantyghemMS0735_RELATIVEERROR_ABS"
            plotLegend = True
            logY = False
            locTimeLegend = "upper right"
            Errorbars = None
        elif numberSetup == 1.112 or numberSetup == "nemsfitfrac":
            # noXtickLabels = True
            Variables = [["ElectronNumberDensity", "MS0735fit"]]
            Weights = "vol"
            Ylabel = "\\eta_{n_\\mathrm{e,fit}}"
            # Xlabel = r''
            # postprocess = "DIVIDEBY"
            postprocess = "INTERPOLATE-MS0735fit_RELATIVEERROR_ABS"
            plotLegend = True
            logY = False
            locTimeLegend = "upper right"
            Errorbars = None
        elif numberSetup == 1.112 or numberSetup == "nemsfitobs":
            # noXtickLabels = True
            Variables = [["MS0735fit", "NumberDensityVantyghemMS0735"]]
            Weights = "vol"
            Ylabel = "\\eta_{n_\\mathrm{e,fit-obs}}"
            # Xlabel = r''
            # postprocess = "DIVIDEBY"
            postprocess = "INTERPOLATE-NumberDensityVantyghemMS0735_RELATIVEERROR_ABS"
            plotLegend = True
            logY = False
            locTimeLegend = "upper right"
            Errorbars = None
        elif numberSetup == 1.2 or numberSetup == "densitymassweighted":
            # noXtickLabels = True
            Variables = "Density"
            Weights = "Mass"
            TwinY = True
            twinYlabel = r"$n_\mathrm{e,m}\ [\mathrm{cm}^{-3}]$"
            twinYfactor = 1.0 / param.PROTONMASS_cgs * param.ElectronProtonFraction
            Ylabel = "\\rho_\\mathrm{m}\\ [\\mathrm{g}\\,\\mathrm{cm}^{-3}]"
            # Xlabel = r''
            locTimeLegend = "upper right"
            # VariablesTheory = ['NumberDensityVantyghemMS0735']
        elif numberSetup == 2:
            # noXtickLabels = True
            Variables = ["ThermalPressure", "CosmicRayPressure"]
            Weights = "vol"
            Ylabel = "P\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            Ymin = 1e-13  # 1e-28
            Ymax = 1e-9  # 1e-24
            locTimeLegend = "lower right"
            locRestLegend = "upper right"
            showVariable = True
            variableLabel = ["P_\\mathrm{th}", "P_\\mathrm{cr}"]
            showLegendTime = False
            plotLegend = True
            VariablesTheory = ["ThermalPressureVantyghemMS0735"]
        elif numberSetup == 2.1 or numberSetup == "thermalpressure":
            Ylabel = "P_{\\mathrm{th}}\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            Variables = "ThermalPressure"
            Weights = "Volume"
            Ymin = 1e-11
            Ymax = 3e-8
        elif numberSetup == 2.11 or numberSetup == "thermalpressurexray":
            Ylabel = (
                "P_{\\mathrm{th},\\,\\mathrm{X}}\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            )
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = "ThermalPressure"
            Weights = "Volume"
            Ymin = 1e-11
            Ymax = 8e-9
        elif numberSetup == 2.111 or numberSetup == "thermalpressurexraywei":
            Ylabel = (
                "P_{\\mathrm{th},\\,\\mathrm{X}}\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            )
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = "ThermalPressure"
            Weights = "CoolingLuminosity"
            Ymin = 1e-11
            Ymax = 5e-9
        elif numberSetup == 2.2 or numberSetup == "thermalpressure":
            Variables = ["ThermalPressure"]
            Weights = "vol"
            Ylabel = "P\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            Ymin = Ymax = None
            locTimeLegend = "lower right"
            locRestLegend = "upper right"
            showVariable = True
            variableLabel = ["P_\\mathrm{th}"]
            plotLegend = True
        elif numberSetup == 2.3 or numberSetup == "pthcold":
            Variables = ["ThermalPressure", "MagneticPressure"]
            Weights = "vol"
            Ylabel = "P_{<10\\,\\mathrm{Myr},\\mathrm{No}\\,\\mathrm{SF}}\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            # Ymin = Ymax = None
            Selects = "CoolingTimescaleInMyr<10ANDStarFormationRate==0"
            locTimeLegend = "upper right"
            locRestLegend = "lower right"
            showVariable = True
            variableLabel = ["P_\\mathrm{th}", "P_B"]
            plotLegend = True
        elif numberSetup == 2.31 or numberSetup == "pthsfr":
            Variables = ["ThermalPressure", "MagneticPressure"]
            Weights = "vol"
            Ylabel = "P_{\\mathrm{SF}>0}\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            # Ymin = Ymax = None
            Selects = "StarFormationRate>0"
            locTimeLegend = "upper right"
            locRestLegend = "lower right"
            showVariable = True
            variableLabel = ["P_\\mathrm{th}", "P_B"]
            plotLegend = True
        elif numberSetup == 2.32 or numberSetup == "pth10":
            Variables = ["ThermalPressure", "MagneticPressure"]
            Weights = "vol"
            Ylabel = "P_{10\\,\\mathrm{Myr}<t_\\mathrm{cool}<100\\,\\mathrm{Myr}}\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            # Ymin = Ymax = None
            Selects = "CoolingTimescaleInMyr>10" "AND" "CoolingTimescaleInMyr<100"
            locTimeLegend = "lower right"
            locRestLegend = "upper right"
            showVariable = True
            variableLabel = ["P_\\mathrm{th}", "P_B"]
            plotLegend = True
        elif numberSetup == 2.33 or numberSetup == "pthB10":
            Variables = ["ThermalPressure", "MagneticPressure"]
            Weights = "vol"
            Ylabel = "P\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            # Ymin = Ymax = None
            Selects = "StarFormationRate==0" "AND" "CoolingTimescaleInMyr<10"
            locTimeLegend = "lower right"
            locRestLegend = "upper right"
            showVariable = True
            variableLabel = ["P_\\mathrm{th}", "P_B"]
            plotLegend = True
        elif numberSetup == 2.4 or numberSetup == "pthhot":
            Variables = ["ThermalPressure", "MagneticPressure"]
            Weights = "vol"
            Ylabel = "P_{>100\\,\\mathrm{Myr}}\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            # Ymin = Ymax = None
            Selects = "CoolingTimescaleInMyr>100"
            locTimeLegend = "lower left"
            locRestLegend = "upper right"
            locFolderLegend = "center left"
            showVariable = True
            variableLabel = ["P_\\mathrm{th}", "P_B"]
            plotLegend = True
        elif numberSetup == 2.5 or numberSetup == "totalpressure":
            Ylabel = (
                "P_{\\mathrm{tot}}\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            )
            Selects = None
            Variables = "Pressure"
            Weights = "Volume"
            Ymin = 1e-11
            Ymax = 8e-9
        elif numberSetup == 2.1 or numberSetup == "totalpressurestrongmagnetized":
            Ylabel = (
                "P_{\\mathrm{tot},\\,\\mathrm{XB}>0.1}\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            )
            Selects = "XB>0.1"
            Variables = "Pressure"
            Weights = "Volume"
            Ymin = 1e-11
            Ymax = 8e-9
        elif numberSetup == 2.1 or numberSetup == "totalpressuremagnetized":
            Ylabel = (
                "P_{\\mathrm{tot},\\,0.1>\\mathrm{XB}>0.01}\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            )
            Selects = "XB<0.1ANDXB>0.01"
            Variables = "Pressure"
            Weights = "Volume"
            Ymin = 1e-11
            Ymax = 8e-9
        elif numberSetup == 2.1 or numberSetup == "totalpressureweakmagnetized":
            Ylabel = (
                "P_{\\mathrm{tot},\\,\\mathrm{XB}<0.01}\\ [\\mathrm{dyn}\\ \\mathrm{cm}^{-2}]"
            )
            Selects = "XB<0.01"
            Variables = "Pressure"
            Weights = "Volume"
            Ymin = 1e-11
            Ymax = 8e-9
        elif numberSetup == 3:
            Variables = [["CosmicRayPressure", "ThermalPressure"]]
            Weights = "vol"
            Ylabel = "X_\\mathrm{cr}"
            Ymin = 1e-4  # 1e-28
            Ymax = 1e0  # 1e-24
            locTimeLegend = "lower right"
            locRestLegend = "upper right"
            variableLabel = ["X_\\mathrm{cr}"]
            showLegendTime = False
        elif numberSetup == 3.1:
            Variables = [["ThermalPressure", "TotalEnergyDensity"]]
            Weights = "vol"
            Ylabel = "X_\\mathrm{cr}"
            Ymin = 1e-4  # 1e-28
            Ymax = 1e0  # 1e-24
            locTimeLegend = "lower right"
            locRestLegend = "upper right"
            variableLabel = ["X_\\mathrm{th}"]
            showLegendTime = False
        elif numberSetup == 4:
            Variables = ["XCR"]
            Weights = "vol"
            Ylabel = "X_\\mathrm{cr}"
            Ymin = 1e-4  # 1e-28
            Ymax = 1e0  # 1e-24
            locTimeLegend = "lower right"
            locRestLegend = "upper right"
            variableLabel = ["X_\\mathrm{cr}"]
        elif numberSetup == 5 or numberSetup == "b":
            plotICsInsteadZero = False
            plotLegend = False
            showLegendTime = False
            showFolder = True
            Variables = ["SQDAbsoluteMagneticField"]
            Weights = "vol"
            postprocess = "SQRT"
            Ylabel = "|\\boldsymbol{B}|\\ [\\mathrm{G}]"
            locTimeLegend = "lower right"
            locRestLegend = "upper right"
            variableLabel = ["B"]
            alpha = 1.0
        elif numberSetup == 5.01 or numberSetup == "bold":
            plotICsInsteadZero = False
            plotLegend = False
            showLegendTime = False
            showFolder = True
            Variables = ["AbsoluteMagneticField"]
            Weights = "vol"
            Ylabel = "|\\boldsymbol{B}|\\ [\\mathrm{G}]"
            locTimeLegend = "lower right"
            locRestLegend = "upper right"
            variableLabel = ["B"]
            alpha = 1.0  # [1,0.5]
            if HITOMIPAPER:
                Ymin = 7e-7
                Ymax = 4e-5
        elif numberSetup == 5.1:
            plotICsInsteadZero = False
            plotLegend = False
            showFolder = True
            Variables = ["AbsoluteMagneticField"]
            Weights = "SQTVolume"
            Ylabel = "|\\boldsymbol{B}|\\ [\\mathrm{G}]"
            locTimeLegend = "lower right"
            locRestLegend = "upper right"
            variableLabel = ["B"]
            alpha = 1.0  # [1,0.5]
            if HITOMIPAPER:
                Ymin = 7e-7
                Ymax = 4e-5
                showLegendTime = False
                showLegendFolder = False
        elif numberSetup == 5.2 or numberSetup == "xb":
            plotICsInsteadZero = False
            plotLegend = False
            showFolder = True
            Variables = [["MagneticEnergyDensity", "ThermalEnergyDensity"]]
            Weights = "Volume"
            postprocess = "DIVIDEBY"
            Ylabel = "X_{B}"
            locTimeLegend = "lower right"
            locRestLegend = "upper right"
            variableLabel = ["X_{B}"]
            hlines = [0.01, 0.0125]
            alpha = 1.0  # [1,0.5]
        elif numberSetup == 5.2 or numberSetup == "xbold":
            hlines = [0.01, 0.0125]
            plotICsInsteadZero = False
            plotLegend = False
            showFolder = True
            Variables = ["XB"]
            Weights = "Volume"
            Ylabel = "X_{B}"
            locTimeLegend = "lower right"
            locRestLegend = "upper right"
            variableLabel = ["X_{B}"]
            alpha = 1.0  # [1,0.5]
        elif numberSetup == 6:
            noXtickLabels = False
            Variables = ["AlfvenCoolingTimescaleInMyr", "CoolingTimescaleInMyr"]
            Weights = "vol"
            Ylabel = "t\\ [\\mathrm{Myr}]"
            variableLabel = ["t_\\mathrm{A}", "t_\\mathrm{cool}"]
            plotLegendExternal = False
            showVariable = plotLegend = True
            IndexTickStyleRest = True
            Ymin = None  # 1e-13#1e-28
            Ymax = None  # 1e-9#1e-24
        elif numberSetup == 6.1:
            Variables = ["CosmicRayHeatingRate"]
            Weights = "vol"
            Ylabel = "\\mathcal{H}_\\mathrm{cr}\\ [\\mathrm{erg}\\,\\mathrm{cm}^{-3}\\,\\mathrm{s}^{-1}]"
            Errorbars = "percentiles"
        elif numberSetup == 6.2:
            Variables = ["CosmicRayHeatingRate", "CoolingRate"]
            Weights = None
            Ylabel = "\\mathcal{H},\\,\\mathcal{C}\\ [\\mathrm{erg}\\,\\mathrm{cm}^{-3}\\,\\mathrm{s}^{-1}]"
            variableLabel = ["\\mathcal{H}_\\mathrm{cr}", "\\mathcal{C}"]
            Errorbars = "percentiles"
            plotLegendExternal = False
            showVariable = plotLegend = True
            IndexTickStyleRest = True
            Ymin = 1e-28
            Ymax = 1e-24
        elif numberSetup == 6.3:
            Variables = ["CosmicRayHeatingRateCoolingRateRatio"]
            Weights = None
            Ylabel = "\\mathcal{H}_\\mathrm{cr}/\\mathcal{C}"
            variableLabel = ["\\mathcal{H}_\\mathrm{cr}/\\mathcal{C}"]
            Errorbars = "percentiles"
            plotLegendExternal = False
            showVariable = plotLegend = True
            IndexTickStyleRest = True
            Ymin = 1e-3
            Ymax = 1e3
        elif numberSetup == 7:
            noXtickLabels = False
            Variables = ["ElectronNumberDensity", "ElectronNumberDensityPerseus"]
            Weights = "vol"
            Ylabel = "n_\\mathrm{e}\\ [\\mathrm{cm}^{-3}]"
            showVariable = True
            variableLabel = ["n_\\mathrm{e,sim}", "t_\\mathrm{e,per}"]
            Xlabel = r""
            locTimeLegend = "upper right"
        elif numberSetup == 8.1 or numberSetup == "bondimdot":
            noXtickLabels = False
            Variables = ["BondiMdot"]
            Weights = ["Mass"]
            Ylabel = "\\dot{M}_\\mathrm{Bondi}\\ [M_\\odot\\,\\mathrm{yr}^{-1}]"
            showVariable = True
            variableLabel = ["\\cdot{M}_\\mathrm{Bondi}"]
            locTimeLegend = "upper right"
        elif numberSetup == 9:
            INFO["unitsV"] = "kms"
            noXtickLabels = False
            Variables = ["AbsoluteVelocity"]
            Weights = "vol"
            Ylabel = "v [\\mathrm{km}\\,\\mathrm{s}^{-1}]"
            locTimeLegend = "upper right"
        elif numberSetup == 9.1 or numberSetup == "velocity":
            INFO["unitsV"] = "kms"
            noXtickLabels = False
            Variables = ["AbsoluteVelocity"]
            Weights = "SQTMass"
            Ylabel = "v [\\mathrm{km}\\,\\mathrm{s}^{-1}]"
            locTimeLegend = "upper right"
            if HITOMIPAPER:
                Ymin = 0
                Ymax = 300
                logY = False

                showLegendFolder = True
        elif numberSetup == 9.12 or numberSetup == "velocitydispersion":
            AverageStyle = "WEIGHTEDSTANDARDDEVIATIONDATAPLOTReferenceManual"
            INFO["unitsV"] = "kms"
            noXtickLabels = False
            Variables = ["AbsoluteVelocity"]
            Weights = "SQTMass"
            Ylabel = "\\sigma [\\mathrm{km}\\,\\mathrm{s}^{-1}]"
            locTimeLegend = "upper right"
            Errorbars = None
            hlines = [50, 75]
        elif numberSetup == 9.121 or numberSetup == "velocitydispersionstdweighted":
            AverageStyle = "stdweighted"
            INFO["unitsV"] = "kms"
            noXtickLabels = False
            Variables = ["AbsoluteVelocity"]
            Weights = "SQTMass"
            Ylabel = "\\sigma [\\mathrm{km}\\,\\mathrm{s}^{-1}]"
            locTimeLegend = "upper right"
            Errorbars = None
            hlines = [50, 75]
        elif numberSetup == 9.122 or numberSetup == "velocitydispersionWP2":
            AverageStyle = "WP2"
            INFO["unitsV"] = "kms"
            noXtickLabels = False
            Variables = ["AbsoluteVelocity"]
            Weights = "SQTMass"
            Ylabel = "\\sigma [\\mathrm{km}\\,\\mathrm{s}^{-1}]"
            locTimeLegend = "upper right"
            Errorbars = None
            hlines = [50, 75]
        elif numberSetup == 9.122 or numberSetup == "velocityXdispersion":
            AverageStyle = "WEIGHTEDSTANDARDDEVIATIONDATAPLOTReferenceManual"
            INFO["unitsV"] = "kms"
            noXtickLabels = False
            Variables = ["VelocityX"]
            Weights = "SQTMass"
            Ylabel = "\\sigma [\\mathrm{km}\\,\\mathrm{s}^{-1}]"
            locTimeLegend = "upper right"
            Errorbars = None
            hlines = [50, 75]
        elif numberSetup == 9.2 or numberSetup == "soundspeed":
            INFO["unitsV"] = "kms"
            noXtickLabels = False
            Variables = ["SoundSpeed"]
            Weights = ["Mass"]
            Ylabel = "c_\\mathrm{s}\\ [\\mathrm{km}\\,\\mathrm{s}^{-1}]"
            showVariable = True
            variableLabel = ["c_\\mathrm{s}"]
            locTimeLegend = "upper right"
        elif numberSetup == 9.3 or numberSetup == "CoolingLuminosityCumulative":
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Variables = ["CoolingLuminosity", "XrayLuminosity"]
            Weights = None
            postprocess = "CUMSUM"
            Ylabel = "P_\\mathrm{cum}\\,[\\mathrm{erg}\\,\\mathrm{s}^{-1}]"
            variableLabel = [
                "P_{\\mathrm{ICM},\\,\\mathrm{cum}}",
                "P_{\\mathrm{ICM,X},\\,\\mathrm{cum}}",
            ]
            locRestLegend = "upper right"
            AverageStyle = "sum"
            plotLegend = True
            Errorbars = None
            vlines = [40]
            Ymin = 2e42
            Ymax = 1e46
        elif numberSetup == 10.11 or numberSetup == "energy":
            Variables = [["MagneticFieldEnergy"], ["ThermalEnergy"], ["KineticEnergy"]]
            Weights = None
            postprocess = "CUMSUM"
            Ylabel = "E_\\mathrm{cum}\\,[\\mathrm{erg}]"
            variableLabel = ["E_{B}", "E_\\mathrm{th}", "E_\\mathrm{kin}"]
            AverageStyle = "sum"
            plotLegend = True
            Errorbars = None
            Ymin = 1e52
            Ymax = 1e64
        elif numberSetup == 11.01 or numberSetup == "cumcoldgasoverview":
            Variables = [["MassInSolar"], ["MassInSolar"], ["MassInSolar"]]
            Selects = [
                "Temperature<1e6",
                "StarFormationRate>0",
                "CoolingTimescaleInMyr<100",
            ]
            Weights = None
            postprocess = "CUMSUM"
            Ylabel = "M_\\mathrm{col}(<r)\\,[{M_\\odot}]"
            variableLabel = [
                "M_\\mathrm{col}(<r)",
                "M_\\mathrm{sf}(<r)",
                "M_\\mathrm{tcool<100}(<r)",
            ]
            AverageStyle = "sum"
            Errorbars = None
            logX = True
        elif numberSetup == 11.1 or numberSetup == "cumcoldgas":
            Variables = [
                ["MassInSolar", "MassInSolar"],
            ]
            Selects = [["Temperature<1e6ANDStarFormationRate==0", None]]
            Weights = None
            postprocess = "CUMSUM_DIVIDEBY"
            Ylabel = "M_\\mathrm{col}(<r)\\,[{M_\\odot}]"
            variableLabel = ["M_\\mathrm{col}(<r)"]
            AverageStyle = "sum"
            Errorbars = None
            logX = False
        elif numberSetup == 11.2 or numberSetup == "cumsfgasnorm":
            Variables = [
                ["MassInSolar", "MassInSolar"],
            ]
            Selects = [["StarFormationRate>0", None]]
            Weights = None
            postprocess = "CUMSUM_DIVIDEBY"
            Ylabel = "M_\\mathrm{sf}(<r)\\,[{M_\\odot}]"
            variableLabel = ["M_\\mathrm{sf}(<r)"]
            AverageStyle = "sum"
            Errorbars = None
            logX = False
        elif numberSetup == 11.21 or numberSetup == "sfrcumsum":
            Variables = "StarFormationRate"
            Selects = None
            Weights = None
            Ymin = 1e0
            Ymax = 2e3
            Ylabel = "\\Sigma\\,\\mathrm{SFR}\\ [\\mathrm{M}_\\odot\\,\\mathrm{yr}^{-1}]"
            variableLabel = "\\Sigma\\,\\mathrm{SFR}\\ [\\mathrm{M}_\\odot\\,\\mathrm{yr}^{-1}]"
            AverageStyle = "sum"
            postprocess = "CUMSUM"
            Errorbars = None
        elif numberSetup == 11.3 or numberSetup == "cumtcoolgas":
            Variables = [
                ["MassInSolar", "MassInSolar"],
            ]
            Selects = [["CoolingTimescaleInMyr<100>0", None]]
            Weights = None
            postprocess = "CUMSUM_DIVIDEBY"
            Ylabel = "M_\\mathrm{tcool<100}(<r)\\,[{M_\\odot}]"
            variableLabel = ["M_\\mathrm{tcool<100}(<r)"]
            AverageStyle = "sum"
            Errorbars = None
            logX = False
        elif numberSetup == 11.4 or numberSetup == "cumhotgas":
            Variables = [
                ["MassInSolar", "MassInSolar"],
            ]
            Selects = [["CoolingTimescaleInMyr>100", None]]
            Weights = None
            postprocess = "CUMSUM_DIVIDEBY"
            Ylabel = "M_\\mathrm{tcool>100}(<r)\\,[{M_\\odot}]"
            variableLabel = ["M_\\mathrm{tcool>100}(<r)"]
            AverageStyle = "sum"
            Errorbars = None
            logX = False
        elif numberSetup == 11.5 or numberSetup == "starforminggasmasssum":
            Variables = "MassInSolar"
            Weights = None  # "Mass"
            Errorbars = None
            AverageStyle = "sum"
            Selects = "StarFormationRate>0"
            Ylabel = "M_\\mathrm{SF}\\ [\\mathrm{M}_\\odot]"
        elif numberSetup == 11.51 or numberSetup == "starforminggasmasssumnorm":
            Variables = "MassInSolar"
            Weights = None  # "Mass"
            Errorbars = None
            Ymin = 4e7
            Ymax = 3e11
            postprocess = "BINNORM"
            AverageStyle = "sum"
            Selects = "StarFormationRate>0"
            Ylabel = "\\mathrm{d}M_\\mathrm{SF}/\\mathrm{d}\\,\\mathrm{log}(r)"
        elif numberSetup == 11.6 or numberSetup == "coldnonstarforminggasmasssum":
            Variables = "MassInSolar"
            Weights = None  # "Mass"
            Errorbars = None
            AverageStyle = "sum"
            Selects = "StarFormationRate==0ANDCoolingTimescaleInMyr<10"
            Ylabel = "M_{<10\\,\\mathrm{Myr},\\mathrm{No}\\,\\mathrm{SF}}\\ [\\mathrm{M}_\\odot]"
        elif numberSetup == 11.61 or numberSetup == "coldnonstarforminggasmasssumnorm":
            Variables = "MassInSolar"
            Weights = None  # "Mass"
            Errorbars = None
            Ymin = 4e7
            Ymax = 3e11
            postprocess = "BINNORM"
            AverageStyle = "sum"
            Selects = "StarFormationRate==0ANDCoolingTimescaleInMyr<10"
            Ylabel = "\\frac{\\mathrm{d}\\,\\mathrm{log}M_{<10\\,\\mathrm{Myr},\\mathrm{No}\\,\\mathrm{SF}}}{\\mathrm{d}\\,\\mathrm{log}(r)}"
            Ylabel = "\\left[\\frac{\\mathrm{d}M}{\\mathrm{d}\\,\\mathrm{log}(r)}\\right]_{<10\\,\\mathrm{Myr},\\mathrm{No}\\,\\mathrm{SF}}"
        elif numberSetup == 11.7 or numberSetup == "colgasmassctA10B100sum":
            Variables = "MassInSolar"
            Weights = None  # "Mass"
            Errorbars = None
            AverageStyle = "sum"
            Selects = "CoolingTimescaleInMyr>10ANDCoolingTimescaleInMyr<100"
            Ylabel = (
                "M_{>10\\,\\mathrm{Myr},<100\\,\\mathrm{Myr}}\\ [\\mathrm{M}_\\odot]"
            )
        elif numberSetup == 11.71 or numberSetup == "colgasmassctA10B100sumnorm":
            Variables = "MassInSolar"
            postprocess = "BINNORM"
            Weights = None  # "Mass"
            Ymin = 4e7
            Ymax = 3e11
            Errorbars = None
            AverageStyle = "sum"
            Selects = "CoolingTimescaleInMyr>10ANDCoolingTimescaleInMyr<100"
            Ylabel = "\\frac{\\mathrm{d}\\,\\mathrm{log}M_{>10\\,\\mathrm{Myr},<100\\,\\mathrm{Myr}}}{\\mathrm{d}\\,\\mathrm{log}(r)}"
            Ylabel = "\\left[\\frac{\\mathrm{d}M}{\\mathrm{d}\\,\\mathrm{log}(r)}\\right]_{10\\,\\mathrm{Myr}<t_\\mathrm{cool}<100\\,\\mathrm{Myr}}"
        elif numberSetup == 11.72 or numberSetup == "colgasmassctB100":
            Variables = "MassInSolar"
            postprocess = "BINNORM"
            Weights = None  # "Mass"
            Ymin = 4e7
            Ymax = 3e11
            Errorbars = None
            AverageStyle = "sum"
            Selects = "CoolingTimescaleInMyr<100ANDStarFormationRate==0"
            Ylabel = "\\frac{\\mathrm{d}\\,\\mathrm{log}M_{>10\\,\\mathrm{Myr},<100\\,\\mathrm{Myr}}}{\\mathrm{d}\\,\\mathrm{log}(r)}"
            Ylabel = "\\left[\\frac{\\mathrm{d}\\,M}{\\mathrm{d}\\,\\mathrm{log}(r)}\\right]_{\\mathrm{cold},\\,\\mathrm{No}\\,\\mathrm{SF}}\\,[\\mathrm{M}_\\odot]"
        elif numberSetup == 11.51 or numberSetup == "starforminggasmasscumsum":
            postprocess = "CUMSUM"
            Variables = "MassInSolar"
            Weights = None  # "Mass"
            Errorbars = None
            AverageStyle = "sum"
            Selects = "StarFormationRate>0"
            Ylabel = "M_\\mathrm{SF}\\ [\\mathrm{M}_\\odot]"
        elif numberSetup == 11.61 or numberSetup == "coldnonstarforminggasmasscumsum":
            postprocess = "CUMSUM"
            Variables = "MassInSolar"
            Weights = None  # "Mass"
            Errorbars = None
            AverageStyle = "sum"
            Selects = "StarFormationRate==0ANDCoolingTimescaleInMyr<10"
            Ylabel = "M_{<10\\,\\mathrm{Myr},\\mathrm{No}\\,\\mathrm{SF}}\\ [\\mathrm{M}_\\odot]"
        elif numberSetup == 11.71 or numberSetup == "colgasmassctA10B100cumsum":
            postprocess = "CUMSUM"
            Variables = "MassInSolar"
            Weights = None  # "Mass"
            Errorbars = None
            AverageStyle = "sum"
            Selects = "CoolingTimescaleInMyr>10ANDCoolingTimescaleInMyr<100"
            Ylabel = "M_{10\\,\\mathrm{Myr}<t_\\mathrm{cool}<100\\,\\mathrm{Myr}}\\ [\\mathrm{M}_\\odot]"
        elif numberSetup == 11.8 or numberSetup == "warmgasmasstcA100sum":
            Variables = "MassInSolar"
            Weights = None  # "Mass"
            Errorbars = None
            AverageStyle = "sum"
            Selects = "CoolingTimescaleInMyr>100"
            Ylabel = "M_{>100\\,\\mathrm{Myr}}\\ [\\mathrm{M}_\\odot]"
        elif numberSetup == 11.9 or numberSetup == "xraygasmasssum":
            Variables = "MassInSolar"
            Weights = None  # "Mass"
            Errorbars = None
            AverageStyle = "sum"
            Selects = "TemperatureInkV>0.2ANDTemperatureInkV<10"
            Ylabel = "M_{\\mathrm{Chandra}}\\ [\\mathrm{M}_\\odot]"
        elif numberSetup == 11.91 or numberSetup == "superxraygasmasssum":
            Variables = "MassInSolar"
            Weights = None  # "Mass"
            Errorbars = None
            AverageStyle = "sum"
            Selects = "TemperatureInkV>10"
            Ylabel = "M_{T>\\mathrm{Chandra}}\\ [\\mathrm{M}_\\odot]"
        elif numberSetup == 11.99 or numberSetup == "gasmassum":
            Variables = "MassInSolar"
            Weights = None  # "Mass"
            Errorbars = None
            AverageStyle = "sum"
            # Selects = "TemperatureInkV>10"
            Ylabel = "M_{\\mathrm{tot}}\\ [\\mathrm{M}_\\odot]"
        elif numberSetup == 12.1 or numberSetup == "2corr":
            Variables = [
                [["Velocity", "Velocity"]],
            ]
            Selects = [
                [
                    [
                        "DistanceToCenterInKpc<100ANDTemperatureInkV>0.2ANDTemperatureInkV<10ANDJetTracer<1e-3",
                        # "DistanceToCenterInKpc<100ANDTemperatureInkV<10",
                        "DistanceToCenterInKpc<100ANDStarFormationRate>0",
                    ]
                ]
            ]
            Weights = [
                [["Mass", "Mass"]],
            ]
            postprocess = None
            Ylabel = "2corr"
            variableLabel = ["2corr"]
            AverageStyle = "2corr_samples:10000_Nbootstrap:10"
            Errorbars = "bootstrap"
            error_bar_style = "errorbars"
            logX = True
            logY = False
            Ymin = -1
            Ymax = 1
        elif numberSetup == 12.2 or numberSetup == "2corrr":
            Variables = [
                [["Velocity", "Velocity"]],
                [["Velocity", "Velocity"]],
            ]
            Selects = [
                [
                    [
                        "DistanceToCenterInKpc<100ANDTemperatureInkV>0.2ANDTemperatureInkV<10ANDJetTracer<1e-3",
                        "DistanceToCenterInKpc<100ANDStarFormationRate>0",
                    ]
                ],
                [
                    [
                        "DistanceToCenterInKpc<100ANDTemperatureInkV>0.2ANDTemperatureInkV<10ANDJetTracer<1e-3",
                        "DistanceToCenterInKpc<100ANDStarFormationRate>0",
                    ]
                ],
            ]
            Weights = [
                [["Volume", "Volume"]],
                [["Mass", "Mass"]],
            ]
            postprocess = None
            Ylabel = "2corr"
            variableLabel = ["2corr"]
            AverageStyle = "2corr_samples:1000_Nbootstrap:200"
            Errorbars = "bootstrap"
            error_bar_style = "errorbars"
            logX = True
            logY = False
            Ymin = None  # -1
            Ymax = None  # 1
        else:
            raise Exception(f"setup {numberSetup} not found!")

        if folder_color_gradient or single_color_gradient:
            number_plots = len(Numbers)
        else:
            number_plots = (
                len(Numbers)
                if IndexColorTime
                else np.size(Variables)
                if IndexColorRest
                else np.size(Folders)
            )
        sample_colormap = False
        if sample_colors_from_colorbar:
            sample_colormap = True
            single_color_gradient = False
            folder_color_gradient = False
        numbers = Numbers
        if colors_to_sample_from_colorbar is not None:
            if not all([x in colors_to_sample_from_colorbar for x in Numbers]):
                print(
                    f"{[type(x) for x in Numbers]} {[type(x) for x in colors_to_sample_from_colorbar]}"
                )
                raise Exception(
                    f"cant find plotted number {Numbers} in number to sample {colors_to_sample_from_colorbar}! {[x is colors_to_sample_from_colorbar for x in Numbers]}"
                )
            numbers = colors_to_sample_from_colorbar
        showLegendTimeColormap = False
        if len(Numbers) > 3:  # len(Folders[0]) <= 3 and
            folder_color_gradient = False
            single_color_gradient = False
            showLegendTimeColormap = True
            sample_colormap = True
            if not PRESENTATION:
                pass
                # showLegendTimeColormap = True
                # sample_colormap = True
        print(f"sample_colormap: {sample_colormap}")
        # exit()
        color, scalarMap = get_colors(
            number_plots,
            single_color_gradient=single_color_gradient,
            folder_color_gradient=folder_color_gradient,
            number_folders=len(Folders[0]),
            numbers=numbers,
            sample_colormap=sample_colormap,
        )
        print(f"color: {color}")
        if colors_to_sample_from_colorbar is not None:
            color = [
                c for c, x in zip(color, colors_to_sample_from_colorbar) if x in Numbers
            ]
        if IndexColorTime and showLegendTime:
            if folder_color_gradient:
                if len(color[0]) > 7:
                    showLegendTimeOnlyFirstLast = True
                    print(
                        "Too many times being plotted -> showing first and last time!!"
                    )
            elif len(color) > 7:
                showLegendTimeOnlyFirstLast = True
                print("Too many times being plotted -> showing first and last time!!")
        if numberSetup != 8:
            Ylabel = r"$%s$" % Ylabel
        print(f"showLegendTime: {showLegendTime}")
        print(f"showLegendTimeOnlyFirstLast: {showLegendTimeOnlyFirstLast}")
        numberDiffplots = np.max(
            [np.size(x) for x in [Variables, Weights, AverageStyle, Errorbars]]
        )
        Variables = Fig.makeIterable(Variables, numberDiffplots)
        Weights = Fig.makeIterable(Weights, np.size(Variables))
        Postprocess = Fig.makeIterable(postprocess, np.size(Variables))
        Errorbars = Fig.makeIterable(Errorbars, numberDiffplots)
        AverageStyle = Fig.makeIterable(AverageStyle, numberDiffplots)
        Selects = Fig.makeIterable(Selects, numberDiffplots)
        input_sub_list = []
        INFOArray = aux.INFOSorter(INFO, FinalDimension=[np.size(Variables)])
        print(f"INFOArray: {INFOArray}")
        name = get_filename()
        if len(os.path.basename(name)) > 255:
            warnings.warn(f"filename too long ({len(os.path.basename(name))} > 255)!")
            print("trying to shorten.....")
            name = get_filename(reduce_length=True)
            if len(os.path.basename(name)) > 255:
                warnings.warn(f"filename still too long ({len(os.path.basename(name))} > 255)! removing filenames!")
                print("removing folder names from filename.....")
                name = get_filename(minimize_length=True)
                if len(os.path.basename(name)) > 255:
                    raise Exception(f"filename too long ({len(os.path.basename(name))} > 255)!")
        print("working on %s" % name)
        print(f"NameOfSim_single_plot: {NameOfSim_single_plot}")
        for index_folder_debug, (folder, nameOfSim, nameOfSimSave) in enumerate(
            zip(Folders_single_plot, NameOfSim_single_plot, NameOfSimSave_single_plot)
        ):
            X = []
            Y = []
            Err = []
            Labels = []
            times = []
            for indexNumber, number in enumerate(Numbers):
                for (
                    indexvariable,
                    (variable, weight, avgStyle, errBrs, select, postprocess),
                ) in enumerate(
                    zip(
                        Variables,
                        Weights,
                        AverageStyle,
                        Errorbars,
                        Selects,
                        Postprocess,
                    )
                ):
                    _index_folder = index_folder_outer * len(Folders) + index_folder_debug
                    if not is_none(overplot_folder) and _index_folder in overplot_folder:
                        if not np.array(overplot_folder_include_times)[get_index_list(_index_folder, overplot_folder)] == ["all"]:
                            if number not in overplot_folder_include_times[get_index_list(_index_folder, overplot_folder)]:
                                continue
                    INFO = INFOArray[indexvariable]
                    print(f"INFO: {INFO}")
                    input = {}
                    input["fontsize"] = fontsize
                    input["index_number"] = indexNumber
                    input["is_overplot"] = False
                    if not is_none(overplot_folder) and _index_folder in overplot_folder:
                        _index_folder = overplot_folder_location_folder[get_index_list(_index_folder, overplot_folder)]
                        input["is_overplot"] = True
                    input["index_folder"] = _index_folder
                    input["indexvariable"] = indexvariable
                    input["index_setup"] = index_setup
                    input["folder"] = folder
                    input["number"] = number
                    input["nameOfSim"] = nameOfSim
                    input["nameOfSimSave"] = nameOfSimSave
                    input["FieldsToLoad"] = FieldsToLoad
                    input["variable"] = variable
                    input["weight"] = weight
                    input["AverageStyle"] = avgStyle
                    input["Errorbars"] = errBrs
                    input["select"] = select
                    input["info"] = INFO
                    input["showFolder"] = showFolder
                    input["showVariable"] = showVariable
                    input["showWeight"] = showWeight
                    input["showAverageStyle"] = showAverageStyle
                    input["showTime"] = showTime
                    input["Range"] = [Xmin, Xmax]
                    input["NBins"] = NBins
                    input["Smooth"] = Smooth
                    input["SmoothN"] = SmoothN
                    input["logX"] = logX
                    input["logY"] = logY
                    input["Ymin"] = Ymin
                    input["Ymax"] = Ymax
                    input["SmoothLimit"] = SmoothLimit
                    input["SameNumberPointsPerBin"] = SameNumberPointsPerBin
                    input["variableLabel"] = (
                        variableLabel[indexvariable]
                        if variableLabel is not None
                        else None
                    )
                    input["ICsFolder"] = ICsFolder
                    input["plotICsInsteadZero"] = plotICsInsteadZero
                    input["name"] = name
                    input["Ylabel"] = Ylabel
                    input["hlines"] = hlines
                    input["vlines"] = vlines
                    input["twinYlabel"] = twinYlabel
                    input["twinYfactor"] = twinYfactor
                    input["single_color_gradient"] = single_color_gradient
                    input["folder_color_gradient"] = folder_color_gradient
                    input["sample_colormap"] = sample_colormap
                    input["showLegendTimeColormap"] = showLegendTimeColormap
                    input["showLegendTime"] = showLegendTime
                    input["VariablesTheory"] = VariablesTheory
                    input["plotLegend"] = plotLegend
                    input["showLegendTimeOnlyFirstLast"] = showLegendTimeOnlyFirstLast
                    input["postprocess"] = postprocess
                    input["differenceTime"] = differenceTime
                    input["split_setups_in_panels"] = split_setups_in_panels
                    input["colorbar_horizontal_offset"] = colorbar_horizontal_offset
                    input["width_colorbar_horizontal"] = width_colorbar_horizontal
                    input[
                        "length_colorbar_horizontal_percent"
                    ] = length_colorbar_horizontal_percent
                    input["error_bar_style"] = error_bar_style
                    input["radprof_style"] = radprof_style
                    input["time_average_range"] = time_average_range
                    input["time_average_style_error"] = time_average_style_error
                    input["time_average_style"] = time_average_style
                    input["radprof_style"] = radprof_style
                    input["overplot_plot_color"] = overplot_plot_color
                    input["overplot_plot_tickstyle"] = overplot_plot_tickstyle
                    input["overplot_plot_alpha"] = overplot_plot_alpha
                    # input["index_folder"] = index_folder
                    # input["index_setup"] = index_setup
                    input_sub_list.append(input)
        input_list.append(input_sub_list)


def main_loop_get_data(input):
    input = input[0]
    print(f'input["info"]: {input["info"]}')
    get_radprof(input)


def get_radprof(input):
    if input["radprof_style"] == "time_average":
        print(f"radprof_style: {radprof_style}: doing time_average")
        (
            input["x"],
            input["y"],
            input["err"],
            input["label"],
        ) = Fig.do_radprof_snap_time_average(
            variable=input["variable"],
            folder=input["folder"],
            numbers=None,
            weight=input["weight"],
            select=input["select"],
            average_style=input["time_average_style"],
            average_style_error=input["time_average_style_error"],
            name_of_sim=input["nameOfSim"],
            name_of_sim_save=input["nameOfSimSave"],
            ics=input["ICsFolder"],
            info=input["info"],
            fields_to_load=input["FieldsToLoad"],
            nbins=input["NBins"],
            range=input["Range"],
            same_number_points_bin=input["SameNumberPointsPerBin"],
            method=input["AverageStyle"],
            postprocess=input["postprocess"],
            method_error=input["Errorbars"],
            error_bar_style=input["error_bar_style"],
            log=input["logX"],
            differenceTime=input["differenceTime"],
            time_averaging_range=input["time_average_range"],
            central_time=input["number"],
        )
    else:
        input["x"], input["y"], input["err"], input["label"] = Fig.do_radprof_snap(
            number=input["number"],
            variable=input["variable"],
            weight=input["weight"],
            select=input["select"],
            mask=None,
            folder=input["folder"],
            name_of_sim=input["nameOfSim"],
            name_of_sim_save=input["nameOfSimSave"],
            ics=input["ICsFolder"],
            info=input["info"],
            fields_to_load=input["FieldsToLoad"],
            nbins=input["NBins"],
            range=input["Range"],
            same_number_points_bin=input["SameNumberPointsPerBin"],
            method=input["AverageStyle"],
            method_error=input["Errorbars"],
            error_bar_style=input["error_bar_style"],
            log=input["logX"],
            postprocess=input["postprocess"],
            differenceTime=input["differenceTime"],
        )


def main_loop_plot_data(input_list):
    for input in input_list:
        get_radprof(input)
    plotLegend = input["plotLegend"]
    split_setups_in_panels = input["split_setups_in_panels"]
    showLegendTimeColormap = input["showLegendTimeColormap"]
    colorbar_horizontal_offset = input["colorbar_horizontal_offset"]
    width_colorbar_horizontal = input["width_colorbar_horizontal"]
    length_colorbar_horizontal_percent = input["length_colorbar_horizontal_percent"]
    VariablesTheory = input["VariablesTheory"]
    fontsize = input["fontsize"]
    # index_folder = input["index_folder"]
    # index_setup = input["index_setup"]
    showLegendTimeOnlyFirstLast = input["showLegendTimeOnlyFirstLast"]
    numberNumbers = max([x["index_number"] for x in input_list]) + 1
    numberFolders = max([x["index_folder"] for x in input_list]) + 1
    print(f"numberFolders: {numberFolders}")
    numberVariables = max([x["index_setup"] for x in input_list]) + 1
    BIG = False
    BIGGER = False
    if HITOMIPAPER:
        BIG = False
        BIGGER = True
    if fontsize is None:
        if BIG:
            fontsize = 30
        elif BIGGER:
            fontsize = 22
        else:
            fontsize = 18
        if split_setups_in_panels:
            fontsize = 12
    if split_setups_in_panels:
        tick_length_major = 5 * fontsize / 16
        tick_length_minor = 3 * fontsize / 16
    else:
        tick_length_major = 12
        tick_length_minor = 7
    markersize = 4 * fontsize / 22.0
    linewidth = 0.6 * markersize
    font = {"family": "serif", "serif": ["Arial"], "size": fontsize}
    ticks_font = {"family": "sans-serif", "serif": ["Helvetica"]}  # ,
    mpl.rc("text", usetex=True)
    # mpl.rcParams["text.latex.preamble"] = [
    #     r"\usepackage{amsmath,bm}\newcommand{\sbf}[1]{\textsf{\textbf{#1}}}"
    # ]
    mpl.rc("font", **font)
    PLOTS = []
    TIME = []
    PLOTSFOLDER = []
    FOLDERS = []
    YLABELS = []
    HLINES = []
    VLINES = []
    left = 0.18
    lower = 0.13
    axes_dimensions = [left, lower, 1 - left - 0.03, 1 - lower - 0.03]
    figure_dimensions = [8, 6]
    if MiniPictures:
        figure_dimensions = [7.5, 10]
        axes_dimensions[0] = 0.11
        axes_dimensions[2] = 0.85
    elif TwinY:
        axes_dimensions[0] = 0.17
        axes_dimensions[2] = 0.66
    if vertical_pictures:
        figure_dimensions = [6, 8]
    if shorter_vertical_pictures:
        figure_dimensions = [6, 7]
    if square_pictures:
        figure_dimensions = [6, 6]
    print(f"split_setups_in_panels: {split_setups_in_panels}")
    if split_setups_in_panels is None:
        fig = plt.figure(figsize=np.array(figure_dimensions))
        ax = plt.axes(axes_dimensions)
        double_split_panels = False
        number_axes = 1
    else:
        x_n = y_n = None
        double_split_panels = False
        if "folder" in split_setups_in_panels:
            number_folders = (
                max(
                    dictionary_modulation.get_unique_dic_value_from_list(
                        "index_folder", input_list
                    )
                )
                + 1
            )
            number_unique_setups = number_folders
            if "setup" in split_setups_in_panels:
                double_split_panels = True
                number_setups = len(
                    dictionary_modulation.get_unique_dic_value_from_list(
                        "index_setup", input_list
                    )
                )
                number_unique_setups *= number_setups
                x_n, y_n = AxesGeneration.determine_xy_index(
                    split_setups_in_panels.startswith("setup"),
                    number_setups,
                    number_folders,
                )
                print(f"number_unique_setups: {number_unique_setups}")
        elif "setup" in split_setups_in_panels:
            number_setups = len(
                dictionary_modulation.get_unique_dic_value_from_list(
                    "index_setup", input_list
                )
            )
            number_unique_setups = number_setups
            print(f"number_unique_setups: {number_unique_setups}")
        else:
            raise Exception(
                f"split_setups_in_panels: {split_setups_in_panels} not implemented!"
            )

        if (x_n is None and y_n is None) or force_xn:
            numbers_x_min = 3
            if force_xn:
                numbers_x_min = force_xn
            x_n, y_n = AxesGeneration.get_xy_shape(
                number_unique_setups, numbers_x_min=numbers_x_min
            )
        print(f"number_unique_setups: {number_unique_setups}")
        print(f"x_n: {x_n}")
        print(f"y_n: {y_n}")
        if number_unique_setups == 1:
            x_n = y_n = 1
        x_size = 7
        y_size = 8
        shape_list = [[[x_size, y_size] for i in range(y_n)] for j in range(x_n)]
        space = 0.1
        top = 0.05
        space_below = 0.07
        if single_xlabel:
            space_below = 0
        if single_ylabel:
            space = 0
        if y_n == 1:
            space = 0.077
        number_axes = number_unique_setups
        fig, Axes, AxesColorbar, axes_colorbar_above = AxesGeneration.getAxesList(
            shape_list,
            show_colorbar_above=False,
            show_colorbar=showLegendTimeColormap,
            SingleColorbar=True,
            left=space,
            right=0.02,
            top=top,
            hspace=space_below,
            wspace=space,
            width_colorbar_horizontal=width_colorbar_horizontal,
            bottom=space_below,
            ShowAxisTicks=True,
            length_colorbar_horizontal_percent=length_colorbar_horizontal_percent,
            colorbar_horizontal_offset=colorbar_horizontal_offset,
        )
        # if single_ylabel:
        #     for y in range(y_n):
        #         for x in range(1, x_n):
        #             Axes[0][y].get_shared_y_axes().join(Axes[0][y], Axes[x][y])
            # for a in Axes:

    numMax = np.max([numberFolders, numberNumbers, numberVariables])
    single_color_gradient = input["single_color_gradient"]
    folder_color_gradient = input["folder_color_gradient"]
    sample_colormap = input["sample_colormap"]
    showLegendTimeColormap = input["showLegendTimeColormap"]
    showLegendTime = input["showLegendTime"]
    hlines = input["hlines"]
    vlines = input["vlines"]
    logY = input["logY"]
    logX = input["logX"]
    overplot_plot_color = input["overplot_plot_color"]
    overplot_plot_tickstyle = input["overplot_plot_tickstyle"]
    overplot_plot_alpha = input["overplot_plot_alpha"]
    color, scalarMap = get_colors(
        number_plots,
        single_color_gradient=single_color_gradient,
        folder_color_gradient=folder_color_gradient,
        number_folders=numberFolders,
        numbers=Numbers,
        sample_colormap=sample_colormap,
    )
    if set_color_black_only:
        scalarMap = None
        color = "black"
    TickStyle = ["-", "--", "-.", "D", "x", ",", ".", "X", "|"]
    if double_split_panels:
        TickStyle = ["-", "--", "-."]
        # aux.CHECK(split_setups_in_panels.startswith("setup"))
    alpha = 1.0
    if IndexColorTime and showLegendTime:
        if folder_color_gradient:
            if len(color[0]) > 7:
                showLegendTimeOnlyFirstLast = True
                print("Too many times being plotted -> showing first and last time!!")
        elif len(color) > 7:
            showLegendTimeOnlyFirstLast = True
            print("Too many times being plotted -> showing first and last time!!")
    showLegendTimeOnlyFirstLast = False
    color = Fig.makeIterable(color, numMax, List=False)
    TickStyle = Fig.makeIterable(TickStyle, numMax, List=False)
    alpha = Fig.makeIterable(alpha, numMax, List=False)
    LabelPlot = []
    PlotForLabel = []
    LabelPlotDic = {}
    PlotForLabelDic = {}
    TWINYLABELS = []
    TWINYFACTOR = []
    INDICES_SETUP = []
    INDICES_FOLDER = []
    LOGY = []
    YMIN = []
    YMAX = []
    for input in input_list:
        x = input["x"]
        y = input["y"]
        err = input["err"]
        label = input["label"]
        indexTime = input["index_number"]
        indexFolder = input["index_folder"]
        indexRest = input["index_setup"]
        indexvariable = input["indexvariable"]
        error_bar_style = input["error_bar_style"]
        variableLabel = input["variableLabel"]
        is_overplot = input["is_overplot"]
        if split_setups_in_panels is not None:
            if split_setups_in_panels == "folder":
                IndexPlotType = indexFolder
            if double_split_panels:
                index_x, index_y = AxesGeneration.determine_xy_index(
                    split_setups_in_panels.startswith("setup"), indexRest, indexFolder
                )
            elif split_setups_in_panels.startswith("setup"):
                index_x = indexRest
                index_y = indexFolder % y_n
            else:
                IndexPlotType = indexRest
                index_x = IndexPlotType % x_n
                if y_n == 1:
                    index_y = 0
                else:
                    index_y = IndexPlotType // x_n
            ax = Axes[index_x][index_y]
        if IndexColorRest:
            indexColor = indexRest
        elif IndexColorTime:
            indexColor = indexTime
        else:
            indexColor = indexFolder
        if IndexAlphaRest:
            indexAlpha = indexRest
        elif IndexAlphaTime:
            indexAlpha = indexTime
        else:
            indexAlpha = indexFolder
        if IndexTickStyleRest:
            indexTickStyle = indexRest
            if IndexTickStyleSubRest:
                indexTickStyle = indexvariable
        elif IndexTickStyleTime:
            indexTickStyle = indexTime
        else:
            indexTickStyle = indexFolder
        if folder_color_gradient:
            color_cur = color[indexFolder][indexTime]
        else:
            color_cur = color[indexColor]
        if is_overplot and not is_none(overplot_plot_color):
            color_cur = overplot_plot_color
        _tickstyle = TickStyle[indexTickStyle]
        if is_overplot and not is_none(overplot_plot_tickstyle):
            _tickstyle = overplot_plot_tickstyle
        if _tickstyle == "dashed":
            _tickstyle = "--"
        _alpha = alpha[indexAlpha]
        if is_overplot and not is_none(overplot_plot_alpha):
            _alpha = overplot_plot_alpha
        if error_bar_style != "errorbars" or err is None:
            plot = ax.plot(
                x,
                y,
                _tickstyle,
                color=color_cur,
                alpha=_alpha,
                markersize=markersize,
                linewidth=linewidth,
            )
        else:
            plot = ax.errorbar(
                x,
                y,
                yerr=err[0],
                ls=_tickstyle,
                color=color_cur,
                alpha=_alpha,
                markersize=markersize,
                linewidth=linewidth,
            )
        if (indexTime == 0 and indexFolder == 0) or showAllLabels:
            if variableLabel is None:
                l = f'${label["variable"]}$'
            else:
                l = f"${variableLabel}$"
            LabelPlot.append(l)
            PlotForLabel.append(plot)
            if indexRest in LabelPlotDic.keys():
                LabelPlotDic[indexRest].append(l)
            else:
                LabelPlotDic[indexRest] = [l]
            if indexRest in PlotForLabelDic.keys():
                PlotForLabelDic[indexRest].append(plot)
            else:
                PlotForLabelDic[indexRest] = [plot]
        if error_bar_style == "shaded" and err is not None:
            ax.fill_between(
                x, err[0], err[1], color=color_cur, label=None, alpha=alphaErr,
            )
        if indexFolder == 0 and indexRest == 0 and indexvariable == 0:
            TIME.append(label["time_in_myr"])
            PLOTS.append(plot)
        if indexTime == 0 and indexRest == 0:
            PLOTSFOLDER.append(plot)
            FOLDERS.append(input["nameOfSim"])
            INDICES_FOLDER.append(indexFolder)
        if indexTime == 0 and indexFolder == 0:
            YLABELS.append(input["Ylabel"])
            TWINYLABELS.append(input["twinYlabel"])
            TWINYFACTOR.append(input["twinYfactor"])
            LOGY.append(input["logY"])
            YMIN.append(input["Ymin"])
            YMAX.append(input["Ymax"])
            HLINES.append(input["hlines"])
            VLINES.append(input["vlines"])
            INDICES_SETUP.append(indexRest)
    if not len(FOLDERS):
        raise Exception(
            f"not sufficient folders; "
            f"indexTime: {get_from_input_list(input_list, 'index_number')}"
            f"indexRest: {get_from_input_list(input_list, 'index_setup')}"
        )

    def where_index_setup(INDICES_SETUP, index_setup):
        return np.where(np.array(INDICES_SETUP) == index_setup)[0][0]

    legend_variable_show = []
    for index_axes in range(number_axes):
        noYtickLabels = False
        index_setup = None
        index_not_folder = None
        index_not_setup = index_axes
        if number_axes == 1:
            index_folder = indexFolder
            index_x = index_y = 0
            x_n = y_n = 0
        if split_setups_in_panels is not None:
            if number_axes == 1:
                index_x = index_y = 0
                x_n = y_n = 1
            else:
                index_x = index_axes % x_n
                if y_n == 1:
                    index_y = 0
                else:
                    index_y = index_axes // x_n
            ax = Axes[index_x][index_y]
            index_folder = index_axes
            index_setup = index_axes
            if double_split_panels:
                # x_n, y_n = AxesGeneration.determine_xy_index(split_setups_in_panels.startswith("setup"), index_x, index_y)
                index_folder = [index_x, index_y][
                    aux.CHECK(split_setups_in_panels.startswith("folder"), 0, 1)
                ]
                index_setup = [index_x, index_y][
                    aux.CHECK(split_setups_in_panels.startswith("setup"), 0, 1)
                ]
                index_not_setup = index_folder
                index_not_folder = index_setup
                indexFolder = index_folder
            elif split_setups_in_panels.startswith("setup"):
                index_setup = index_x
                indexFolder = index_axes % x_n
                index_not_setup = index_folder
                print(f"index_setup: {index_setup}")
        if split_setups_in_panels is not None:
            if HLINES[where_index_setup(INDICES_SETUP, index_setup)] is not None:
                for hl in HLINES[where_index_setup(INDICES_SETUP, index_setup)]:
                    ax.axhline(hl, ls="--", c="grey", alpha=0.5, linewidth=linewidth)
        else:
            for index_setup in INDICES_SETUP:
                if HLINES[index_setup] is not None:
                    for hl in HLINES[index_setup]:
                        ax.axhline(hl, ls="--", c="grey", alpha=0.5, linewidth=linewidth)
        if split_setups_in_panels is not None:
            if VLINES[where_index_setup(INDICES_SETUP, index_setup)] is not None:
                for vl in VLINES[where_index_setup(INDICES_SETUP, index_setup)]:
                    ax.axvline(vl, ls="--", c="grey", alpha=0.5, linewidth=linewidth)
        else:
            for index_setup in INDICES_SETUP:
                if VLINES[index_setup] is not None:
                    for vl in VLINES[index_setup]:
                        ax.axvline(vl, ls="--", c="grey", alpha=0.5, linewidth=linewidth)
        tick_style_theory = aux.makeNIterableList(TickStyleTheory, len(VariablesTheory))
        color_theory = aux.makeNIterableList(colorTheory, len(VariablesTheory))
        if len(VariablesTheory) > 0:
            for indexTheory, variableTheory in enumerate(VariablesTheory):
                x_theo, y_theo, label = Fig.getTheoryRadprof(variableTheory, x)
                plot = ax.plot(
                    x_theo,
                    y_theo,
                    tick_style_theory[indexTheory],
                    color=color_theory[indexTheory],
                    alpha=alphaTheory,
                    markersize=markersize,
                    linewidth=linewidth,
                )
                # change here
                LabelPlot.append(label)
                PlotForLabel.append(plot)
                PlotForLabelDic[index_setup].append(plot)
                LabelPlotDic[index_setup].append(label)

        ax.tick_params(
            labelsize=fontsize, direction="in", which="major", length=tick_length_major
        )
        ax.tick_params(
            labelsize=fontsize, direction="in", which="minor", length=tick_length_minor
        )

        if plotTexts:
            print("should be printing text now!!")
            Fig.plotText(ax, Positions, Texts, fontsize=fontsize, color=ColorsText)
            Fig.drawCompleteVerticalLines(
                ax, [x[0] for x in Positions], color=ColorsText, alpha=0.5
            )
        if logX:
            ax.set_xscale("log")
            # ax.ticklabel_format(style="sci", scilimits=(-3, 4), axis="x")
        if LOGY[index_setup]:
            ax.set_yscale("log")
            # ax.ticklabel_format(style="sci", scilimits=(-3, 4), axis="y")

        if Xlabel is not None:
            ax.set_xlabel(Xlabel, fontsize=fontsize)
        print("---------------")
        print(f"YLABELS: {YLABELS}")
        print(f"index_setup: {index_setup}")
        print(f"INDICES_SETUP: {INDICES_SETUP}")
        print(
            f"np.where(np.array(INDICES_SETUP)==index_setup): {np.where(np.array(INDICES_SETUP)==index_setup)}"
        )
        print(f"np.where(np.array(INDICES_SETUP) == index_setup)[0][0]: {np.where(np.array(INDICES_SETUP) == index_setup)[0][0]}")
        if YLABELS[np.where(np.array(INDICES_SETUP) == index_setup)[0][0]] is not None:
            if single_ylabel and index_not_setup > 0:
                noYtickLabels = True
            else:
                ax.set_ylabel(
                    YLABELS[np.where(np.array(INDICES_SETUP) == index_setup)[0][0]],
                    fontsize=fontsize,
                )
        if index_x == x_n - 1:
            Fig.set_tick_positions(
                ax,
                y_ticks_both=True,
                major_tick_length=tick_length_major,
                minor_tick_length=tick_length_minor,
                fontsize=fontsize,
                labelleft=True,
                labelbottom=True,
            )
        # elif index_x == 0:
        #     Fig.set_tick_positions(
        #         ax,
        #         y_ticks_both=False,
        #         major_tick_length=tick_length_major,
        #         minor_tick_length=tick_length_minor,
        #         fontsize=fontsize,
        #         labelleft=True,
        #         labelbottom=True,
        #     )
            # if YLABELS[index_setup] is not None:
        #     if single_ylabel and index_not_setup > 0:
        #         noYtickLabels = True
        #     else:
        #         ax.set_ylabel(YLABELS[index_setup], fontsize=fontsize)
        if axes_text != [None]:
            Fig.add_text_plot(
                axes_text[index_axes], position=axes_text_position, axes=ax
            )
        print(f"indexTime: {indexTime}")
        print(f"showLegendTime: {showLegendTime}")
        print(f"index_axes: {index_axes}")
        print(f"split_setups_in_panels: {split_setups_in_panels}")
        print(f"showLegendTimeOnlyFirstLast: {showLegendTimeOnlyFirstLast}")
        if indexTime > 0 and showLegendTime and index_axes == 0:
            if showLegendTimeColormap and scalarMap is not None:
                if split_setups_in_panels is not None and AxesColorbar is not None:
                    cbar = plt.colorbar(
                        scalarMap,
                        cax=AxesColorbar[0],
                        orientation="horizontal",
                        ticks=Numbers,
                    )
                else:
                    cbar = plt.colorbar(scalarMap, ax=ax, location="top", aspect=40)
                cbar.ax.set_xlabel(r"$t\,[\mathrm{Myr}]$")
            elif split_setups_in_panels is not None:
                legend1 = ax.legend(
                    [x[0] for x in PLOTS],
                    ["$%i\\,\\mathrm{Myr}$" % (x) for x in Numbers],
                    loc=locTimeLegend,
                    fontsize=fontsize,
                    framealpha=framealpha,
                )
            elif not showLegendTimeOnlyFirstLast:
                legend1 = plt.legend(
                    [x[0] for x in PLOTS],
                    [f"{time_formatter(t)} Myr" for t in TIME],
                    loc=locTimeLegend,
                    fontsize=fontsize,
                    framealpha=framealpha,
                )
            else:
                legend1 = plt.legend(
                    [x[0] for x in [PLOTS[0], PLOTS[-1]]],
                    [TIME[0], TIME[-1]],
                    loc=locTimeLegend,
                    fontsize=fontsize,
                    framealpha=framealpha,
                )
        print(f"plotLegend: {plotLegend}")
        print(f"split_setups_in_panels: {split_setups_in_panels}")
        print(f"number_axes: {number_axes}")
        print(f"index_not_setup: {index_not_setup}")
        print(f"plotLegendExternal: {plotLegendExternal}")
        print(f"INDICES_FOLDER: {INDICES_FOLDER}")
        if plotLegend and len(PlotForLabel) > 1:
            if number_axes == 1 and split_setups_in_panels is None:
                if plotLegendExternal:
                    legend3 = plt.legend(
                        [x[0] for x in PlotForLabel],
                        LabelPlot,
                        loc=locRestLegend,
                        fontsize=fontsize,
                        framealpha=framealpha,
                        fancybox=True,
                        bbox_to_anchor=(1, 0.5),
                    )
                else:
                    print(f"PlotForLabel: {PlotForLabel}")
                    print(f"LabelPlot: {LabelPlot}")
                    legend3 = plt.legend(
                        [x[0] for x in PlotForLabel],
                        LabelPlot,
                        loc=locRestLegend,
                        fontsize=fontsize,
                        scatterpoints=1,
                        framealpha=framealpha,
                    )
                ax.add_artist(legend3)
            else:
                print(f"index_not_setup: {index_not_setup}")
                print(f"index_setup: {index_setup}")
                print(f"PlotForLabelDic[index_setup]: {PlotForLabelDic[index_setup]}")
                print(f"legend_variable_show: {legend_variable_show}")
                if len(np.unique(PlotForLabelDic[index_setup])) > 1 and index_setup not in legend_variable_show:
                    ax.legend(
                        [x[0] for x in PlotForLabelDic[index_setup]],
                        LabelPlotDic[index_setup],
                        loc=locRestLegend,
                    )
                    legend_variable_show.append(index_setup)
        print(f"--------")
        print(f"showLegendFolder: {showLegendFolder}")
        print(f"FOLDERS: {FOLDERS}")
        print(f"indexFolder: {indexFolder}")
        print(f"PLOTSFOLDER: {PLOTSFOLDER}")
        print(f"split_setups_in_panels: {split_setups_in_panels}")
        # if split_setups_in_panels is not None and "folder" not in split_setups_in_panels and "setup" in split_setups_in_panels:
        #     if index_axes > 0:
        #         showLegendFolder = False
        no_legend_folder_shown = False
        if showLegendFolder and (not show_single_folder or index_axes == 0):
            if indexFolder == 0 and "folder" not in split_setups_in_panels:
                no_legend_folder_shown = True
                Fig.add_text_plot(
                    FOLDERS[indexFolder], position=locFolderLegend, axes=ax
                )
            elif (
                split_setups_in_panels is not None
                and "folder" in split_setups_in_panels
            ):
                # if split by folder don't need legend just label of folder in top row where index_setup==0
                if index_setup == 0:
                    no_legend_folder_shown = True
                    Fig.add_text_plot(
                        FOLDERS[
                            np.where(np.array(INDICES_FOLDER) == indexFolder)[0][0]
                        ],
                        position=locFolderLegend,
                        axes=ax,
                    )
            else:
                if not plotLegendFolderExternal and len(PLOTSFOLDER) > 1:
                    legend4 = plt.legend(
                        [x[0] for x in PLOTSFOLDER],
                        FOLDERS,
                        loc=locFolderLegend,
                        fontsize=fontsize,
                        scatterpoints=1,
                        framealpha=framealpha,
                    )
                    ax.add_artist(legend4)
                elif len(PLOTSFOLDER) == 1:
                    no_legend_folder_shown = True
                    Fig.add_text_plot(FOLDERS[0], position=locFolderLegend, axes=ax)
                else:
                    legend4 = plt.legend(
                        [x[0] for x in PLOTSFOLDER],
                        FOLDERS,
                        loc="center left",
                        fontsize=fontsize,
                        scatterpoints=1,
                        framealpha=framealpha,
                        fancybox=True,
                        bbox_to_anchor=(1, 0.5),
                    )
                    ax.add_artist(legend4)
        if (
            indexTime > 0
            and showLegendTime
            and not (showLegendTimeColormap and scalarMap is not None)
            and index_setup == 0
            and index_axes == 0
            # and not (split_setups_in_panels)
        ):
            if not no_legend_folder_shown:
                ax2 = ax.twinx()
                lines = [x[0] for x in PLOTS]
                # times = ["%i Myr" %(time_formatter(t)) for t in TIME]
                m = np.unique(TIME, return_index=True)[1]
                m_sorted = np.argsort(np.array(TIME)[m])
                lines = (np.array(lines)[m])[m_sorted]
                TIME = (np.array(TIME)[m])[m_sorted]
                times = ["%i Myr" % (time_formatter(t)) for t in TIME]
                styles = [l.get_linestyle() for l in lines]
                colors = [l.get_color() for l in lines]
                for i, (sty, c, t) in enumerate(zip(styles, colors, times)):
                    ax2.plot(np.NaN, np.NaN, ls=sty, label=t, c=c)
                ax2.get_yaxis().set_visible(False)
                ax2.legend(loc=1)
            else:
                ax.add_artist(legend1)
        ax.set_xlim([XminShow, XmaxShow])
        # if not single_ylabel or index_x == 0:
        ax.set_ylim([YMIN[where_index_setup(INDICES_SETUP, index_setup)], YMAX[where_index_setup(INDICES_SETUP, index_setup)]])
        # ax.set_xlim([np.min(Time),np.max(Time)])
        print(f"YMIN: {YMIN}")
        print(f"index_setup: {index_setup}")
        ymin = YMIN[where_index_setup(INDICES_SETUP, index_setup)]
        ymax = YMAX[where_index_setup(INDICES_SETUP, index_setup)]
        xmin = XminShow
        xmax = XmaxShow
        logY = LOGY[index_setup]
        if noXtickLabels or (single_xlabel and index_y != y_n - 1):
            labels = [item.get_text() for item in ax.get_xticklabels()]
            empty_string_labels = [""] * len(labels)
            ax.set_xticklabels(empty_string_labels)
        print(f"noYtickLabels: {noYtickLabels}")
        if noYtickLabels or (single_ylabel and index_x > 0):
            ax.set_yticklabels([])
        if not yignoreLinTicks and ymin is not None and ymax is not None and logY:
            print("setting own yticks!")
            if not logY:
                yticks = Fig.setLinTicksAlt(
                    ymin, ymax, numberMax=numberMaxLinYTicks
                )
            else:
                yticks = Fig.setLogTicks(
                    ymin,
                    ymax,
                    numberMax=numberMaxLinYTicks,
                    returnOnlyMajorTicks=True,
                )
            print(f" index_x: {index_x};  index_y: {index_y}; yticks: {yticks}")
            # for axis in [ax.yaxis]:
            #     # axis.set_major_formatter(LogFormatterSciNotation())
            #     axis.set_major_formatter(LogFormatterSciNotation())
            #     axis.set_minor_formatter(NullFormatter())
            #     plt.draw()
            ax.set_yticks(list(yticks))
            # ax.set_yticklabels(list(yticks))
            # _, labels = plt.yticks(yTicks, xyLabels)
            # plt.setp(labels, rotation=0)
            # ax.xaxis.get_major_locator().set_params(numticks=15, subs=(1.0,))
            # locs = np.append(np.arange(0.1, 1, 0.1), np.arange(1, 10, 0.2))
            # ax.minorticks_off()
            # ax.yaxis.set_major_formatter(StrMethodFormatter('{x:.1f}'))
            # ax.yaxis.set_minor_formatter(NullFormatter())
            # ax.yaxis.set_major_locator(LogLocator(numticks=15))  # (1)
            # ax.yaxis.set_minor_locator(LogLocator(numticks=15, subs=np.arange(2, 10)))  # (2)
            # print(f"ax.yaxis.get_ticklabels(): {[x for x in ax.yaxis.get_ticklabels()]}")
            # for label in ax.yaxis.get_ticklabels()[:]:
            #     label.set_visible(True)  # (3)
            # ax.set_yticks(list(yticks), minor=False)
            # import matplotlib.ticker as mticker
            # # ax.yaxis.set_major_locator(mticker.FixedLocator(yticks))
            # ax.set_yticklabels(list(yticks), minor=False)
            # # ax.ticklabel_format(axis="y", style="sci")
            # print(f"ax.get_yticks(): {[x for x in ax.get_yticks()]}")
        elif minorYTicks and logY:
            minorLocator = AutoMinorLocator()
            if logY:
                if reduce_minor_y_ticks:
                    minorLocator = LogLocator(
                        base=10.0, subs=(0.2, 0.4, 0.6, 0.8), numticks=12
                    )
                else:
                    minorLocator = LogLocator(
                        base=10.0, subs=np.arange(1, 10) / 10  # , numticks=12
                    )
                # minorLocator = MultipleLocator(spacingMinorYTicks)
                # minorLocator = LogLocator(base=10)
            ax.yaxis.set_minor_locator(minorLocator)
        if not xignoreLinTicks and xmin is not None and xmax is not None:
            # if xticks is None:
            print("setting own xticks!")
            if not logX and use_own_x_ticks:
                xticks = Fig.setLinTicksAlt(
                    xmin, xmax, numberMax=numberMaxLinXTicks
                )
            else:
                xticks = Fig.setLogTicks(xmin, xmax, numberMax=numberMaxLinXTicks)[0]
            print(f"xticks: {xticks}")
            ax.xaxis.set_ticks(xticks)
        if minorXTicks:
            # minorLocator = MultipleLocator(spacingMinorXTicks)
            minorLocator = AutoMinorLocator()
            if logX:
                # minorLocator = LogLocator(base=10)
                if reduce_minor_x_ticks:
                    minorLocator = LogLocator(
                        base=10.0, subs=(0.2, 0.4, 0.6, 0.8), numticks=12
                    )
                else:
                    minorLocator = LogLocator(
                        base=10.0, subs=np.arange(1, 10) / 10  # , numticks=12
                    )
            ax.xaxis.set_minor_locator(minorLocator)

        print(f"single_ylabel: {single_ylabel}")

        if TwinX:
            ax = Fig.getTwinX(ax, factor=twinXfactor, xlog=logX, xlabel=twinXlabel)
        if TWINYLABELS[index_setup] is not None:
            if (
                single_ylabel
                and index_not_setup
                != [x_n, y_n][
                    aux.CHECK(split_setups_in_panels.startswith("setup"), 1, 0)
                ]
                - 1
            ):
                pass
            else:
                ax = Fig.getTwinY(
                    ax,
                    factor=TWINYFACTOR[index_setup],
                    ylog=LOGY[index_setup],
                    ylabel=TWINYLABELS[index_setup],
                )
    if single_ylabel or noYtickLabels:
        for y in range(y_n):
            for x in range(1, x_n):
                Axes[x][y].set_yticklabels([])
    savename = input["name"]
    filename = aux.shortenFileName(os.path.split(savename)[1])
    savename = os.path.join(os.path.split(savename)[0], filename)
    aux.create_dirs_for_file(savename)
    aux.checkExistence_delete_file(savename)
    print(".. saving %s" % savename)
    if not HITOMIPAPER:
        fig.savefig(savename, dpi=150, bbox_inches="tight")  # , rasterized=True)
    else:
        fig.savefig(savename, dpi=450)  # , rasterized=True)
    plt.close(fig)


# print([x for sub in input_list for x in sub])
# for index, i in enumerate(x for sub in input_list for x in sub):
#     print(f"{index}: {i}")
# exit()
number_data_calcs = len([x for sub in input_list for x in sub])
if threads >= multiprocessing.cpu_count():
    threads = multiprocessing.cpu_count() - 1
if threads > 1 and number_data_calcs > 1:
    number_data_calcs = sum([len(x) for x in input_list])
    if number_data_calcs < threads:
        threads = number_data_calcs
    bd = mt.BigData(
        main_loop_get_data,
        [x for x in np.arange(number_data_calcs)],
        [[[x] for sub in input_list for x in sub]],
        1,
        None,
        "numpy",
        trivial_distribution=False,
    )
    bd.do_multi(threads)
if split_setups_in_panels is not None and "setup" in split_setups_in_panels:
    main_loop_plot_data([x for sub in input_list for x in sub])
else:
    for input in input_list:
        main_loop_plot_data(input)
# else:
#     for input in input_list:
#         main_loop_get_data(input)
print(f"finished plots at {datetime.datetime.now()}")
print(
    f"plotting took: {'%i'%((TimeModule.time()-start_time)//60)} min  {'%.2f'%((TimeModule.time()-start_time)%60)} s"
)
