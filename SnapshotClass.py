from gadget_snap import gadget_snapshot
from SuperClasses import checkLoadNoMemory
import auxiliary_functions as aux
import os
import re
import numpy as np

(
    GRID_FOLDER,
    LOADPLOTDATA,
    REMOVE_GRID_FOLDER_AFTER_PLOT,
    SAVE_GRID_WITH_PROCESSOR_ID,
) = aux.env_variables_snapshot()


class snapshot(gadget_snapshot):
    def __init__(
        self,
        filename,
        verbose=False,
        chunk=False,
        loadonly=False,
        loadonlytype=False,
        applytransformationfacs=True,
        onlyHeader=False,
        nommap=False,
        tracer=False,
        hdf5=True,
        species=None,
        forcesingleprec=False,
        cosmological=None,
        loadonlyhalo=-1,
        subfind=None,
        lazy_load=False,
        forcedoubleprec=False,
        convert_posvel_to_double=False,
        quiet=False,
        convAddit=False,
        loadParameterFile=False,
        loadConfigFile=False,
    ):

        super(snapshot, self).__init__(
            filename,
            verbose=verbose,
            chunk=chunk,
            loadonly=loadonly,
            loadonlytype=loadonlytype,
            applytransformationfacs=applytransformationfacs,
            onlyHeader=onlyHeader,
            nommap=nommap,
            tracer=tracer,
            hdf5=hdf5,
            species=species,
            forcesingleprec=forcesingleprec,
            cosmological=cosmological,
            loadonlyhalo=loadonlyhalo,
            subfind=subfind,
            lazy_load=lazy_load,
            forcedoubleprec=forcedoubleprec,
            convert_posvel_to_double=convert_posvel_to_double,
            quiet=quiet,
            convAddit=convAddit,
            loadParameterFile=loadParameterFile,
            loadConfigFile=loadConfigFile,
        )
        self.rotation_angle = []
        self.rotation_axis = []
        self.rotation_quiver_angle = []
        self.rotation_quiver_axis = []
        self.file_name_abs = os.path.abspath(self.file_name)
        self.folder = os.path.split(self.file_name_abs)[0]
        self.file_name_formatted = aux.getSimName(self, includeFileName=True)
        number_search = re.search(
            "_([0-9]+).hdf5", os.path.split(self.file_name_abs)[1]
        )
        self.file_number = int(number_search.group(1)) if number_search else None
        self.time_in_myr = aux.get_Time(self)
        if (
            "UnitLength_in_cm" not in self.__dict__.keys()
            and "parameterfile" in self.__dict__.keys()
        ):
            self.UnitMass_in_g = self.parameterfile["UnitMass_in_g"]
            self.UnitLength_in_cm = self.parameterfile["UnitLength_in_cm"]
            self.UnitVelocity_in_cm_per_s = self.parameterfile[
                "UnitVelocity_in_cm_per_s"
            ]
        if "bhmd" not in self.data.keys() and "rho" in self.data.keys():
            # adding fields for black hole even if simulation initially had no blackholes to have same analysis
            # if len(self.pos[self.type == 5]) == 0:
            # self.data["type"] = np.concatenate((self.data["type"], np.array([5])))
            self.data["jetr"] = np.full_like(self.rho, 0.0, dtype=np.float64)
            self.data["bhmd"] = np.array([[0]], dtype=np.float64)
            self.data["bhed"] = np.array([[0]], dtype=np.float64)
            self.data["bcmq"] = np.array([[0]], dtype=np.float64)
            self.data["bcmr"] = np.array([[0]], dtype=np.float64)
            self.data["bhhs"] = np.array([[0]], dtype=np.float64)
            self.data["bhjk"] = np.array([[0]], dtype=np.float64)
            self.data["bhjt"] = np.array([[0]], dtype=np.float64)
            self.data["bhbk"] = np.array([[0]], dtype=np.float64)
            self.data["bhbt"] = np.array([[0]], dtype=np.float64)
            self.data["bhjm"] = np.array([[0]], dtype=np.float64)
            self.data["bhjc"] = np.array([[0]], dtype=np.float64)
            self.data["bhbe"] = np.array([[0]], dtype=np.float64)
            self.data["bcer"] = np.array([[0]], dtype=np.float64)
            self.data["bceq"] = np.array([[0]], dtype=np.float64)

    def __repr__(self):
        rep = ""
        params_show = [
            "filecount",
            "files",
            "file_name",
            "center",
            "boxsize",
            "hubbleparam",
            "rotation_angle",
            "rotation_axis",
            "rotation_quiver_angle",
            "rotation_quiver_axis",
            "file_name_abs",
            "file_name_formatted",
        ]
        rep += "\n --------HEADER-----------\n \n"
        for param in params_show:
            if param in self.__dict__:
                rep += f"{param} => {self.__dict__[param]} \n"
        rep += " \n --------FIELDS-----------\n \n"
        for key, value in self.data.items():
            # force lazy load to portray all fields
            try:
                rep += f"{key} => {self.data[key]} \n"
            except:
                print(f"key: {key} couldnt be printed")
        return rep

    def mapOnCartGrid(
        self,
        value,
        center=False,
        box=False,
        res=512,
        saveas=False,
        use_only_cells=None,
        numthreads=1,
        verbose=0,
    ):
        self.data["unique_ids"] = np.arange(
            np.size(self.data[value]), dtype="int32"
        ).reshape(self.data[value].shape)
        if np.size(np.unique(self.data["unique_ids"])) != np.size(self.data[value]):
            raise Exception("overflow error with int mask, unique_ids?")
        if use_only_cells == "gas":
            raise Exception("is default anyways but different implementation")
            use_only_cells = self.data["type"] == 0
        if use_only_cells is not None:
            raise Exception(
                "if use_only_cells None: will only look at gas cells, what you want instead?"
            )
        mask = self._mapOnCartGrid(
            value="unique_ids",
            center=center,
            box=box,
            res=res,
            saveas=saveas,
            use_only_cells=use_only_cells,
            numthreads=numthreads,
            verbose=verbose,
        )
        return self.data[value][mask.astype(np.int64)]

    @checkLoadNoMemory(
        namesreturnvariables=["grid"],
        dtypes=["int"],
        loadPlotData=LOADPLOTDATA,
        savePlotDataFolder=GRID_FOLDER,
        savePlotDataNameInclude=[
            "self.file_name_abs",
            "res",
            "self.rotation_angle",
            "self.rotation_axis",
        ],
        Ignore=["verbose", "saveas", "numthreads"],
        checkadditional=[
            "self.rotation_angle",
            "self.rotation_axis",
            "self.rotation_quiver_angle",
            "self.rotation_quiver_axis",
            "self.file_name_abs",
        ],
        verbose=0,
        save_folder_id_process=SAVE_GRID_WITH_PROCESSOR_ID,
    )
    def _mapOnCartGrid(
        self,
        value="unique_ids",
        center=False,
        box=False,
        res=512,
        saveas=False,
        use_only_cells=None,
        numthreads=1,
        verbose=1,
    ):
        return super(snapshot, self).mapOnCartGrid(
            value,
            center=center,
            box=box,
            res=res,
            saveas=saveas,
            use_only_cells=use_only_cells,
            numthreads=numthreads,
            verbose=verbose,
        )

    @checkLoadNoMemory(
        namesreturnvariables="all",
        return_variable_type=dict,
        loadPlotData=LOADPLOTDATA,
        savePlotDataFolder=GRID_FOLDER,
        savePlotDataNameInclude=[
            "self.file_name_abs",
            "res",
            "self.rotation_angle",
            "self.rotation_axis",
        ],
        Ignore=["verbose", "numthreads"],
        checkadditional=[
            "self.rotation_angle",
            "self.rotation_axis",
            "self.rotation_quiver_angle",
            "self.rotation_quiver_axis",
            "self.file_name_abs",
        ],
        verbose=0,
        save_folder_id_process=SAVE_GRID_WITH_PROCESSOR_ID,
    )
    def _get_Aslice_wrapper(
        self,
        value,
        grad=False,
        res=1024,
        center=False,
        axes=[0, 1],
        proj_fact=0.5,
        box=False,
        proj=False,
        polar=False,
        nx=None,
        ny=None,
        nz=None,
        boxz=None,
        allsky=False,
        allsky_phi_offset=0,
        numthreads=1,
        verbose=1,
    ):
        data = super().get_Aslice(
            value,
            grad,
            res,
            center,
            axes,
            proj_fact,
            box,
            proj,
            polar,
            nx,
            ny,
            nz,
            boxz,
            allsky,
            allsky_phi_offset,
            numthreads,
            verbose,
        )
        print(f'np.max(data["grid"]): {np.max(data["grid"])}')
        print(f"np.shape(value): {np.shape(self.data[value])}")
        if np.max(data["grid"]) >= np.sum(np.shape(self.data[value])):
            raise Exception(
                f' max(data["grid"]) {max(data["grid"])} >= np.sum(value.shape) {np.sum(value.shape)}'
            )
        return data

    def get_Aslice(
        self,
        value,
        grad=False,
        res=1024,
        center=False,
        axes=[0, 1],
        proj_fact=0.5,
        box=False,
        proj=False,
        polar=False,
        nx=None,
        ny=None,
        nz=None,
        boxz=None,
        allsky=False,
        allsky_phi_offset=0,
        numthreads=1,
        verbose=0,
    ):
        self.data["unique_ids"] = np.arange(np.size(self.data[value])).reshape(
            self.data[value].shape
        )
        dictionary = self._get_Aslice_wrapper(
            "unique_ids",
            grad=grad,
            res=res,
            center=center,
            axes=axes,
            proj_fact=proj_fact,
            box=box,
            proj=proj,
            polar=polar,
            nx=nx,
            ny=ny,
            nz=nz,
            boxz=boxz,
            allsky=allsky,
            allsky_phi_offset=allsky_phi_offset,
            numthreads=numthreads,
            verbose=verbose,
        )
        dictionary["grid"] = self.data[value][dictionary["grid"].astype(np.int64)]
        # setting contours to None as not used!
        dictionary["contours"] = None
        return dictionary

    def rotate_around_axis(
        self, rotationangle=None, plotquiver=True, center=None, rotationaxis=[1, 0, 0]
    ):
        """

        Parameters
        ----------
        rotationangle : float
        plotquiver : bool
        center : list
        rotationaxis : list

        Returns
        -------

        """
        if center is None:
            center = [self.boxsize / 2.0 for x in range(3)]
        if rotationangle is not None and rotationangle != 0:
            aux.RotatePositions(
                self, center, rotationangle, rotationaxis, input_units="degrees"
            )
            if "rotation_angle" not in self.__dict__:
                self.__dict__["rotation_angle"] = []
            if "rotation_axis" not in self.__dict__:
                self.__dict__["rotation_axis"] = []
            self.__dict__["rotation_angle"] += [rotationangle]
            self.__dict__["rotation_axis"] += [rotationaxis]
            if plotquiver:
                if "rotation_quiver_angle" not in self.__dict__:
                    self.__dict__["rotation_quiver_angle"] = []
                if "rotation_quiver_axis" not in self.__dict__:
                    self.__dict__["rotation_quiver_axis"] = []
                self.__dict__["rotation_quiver_angle"] += [rotationangle]
                self.__dict__["rotation_quiver_axis"] += [rotationaxis]
                aux.RotateForQuiver(self, rotationangle, rotationaxis)

    def plot_Aslice(
        self,
        value,
        grad=False,
        logplot=False,
        colorbar=True,
        colorbar_label="default",
        contour=False,
        res=1024,
        center=False,
        axes=[2, 0],
        proj_fact=0.5,
        minimum=-np.inf,
        newfig=True,
        newlabels=False,
        cmap=False,
        vrange=False,
        cblabel=False,
        rasterized=False,
        box=False,
        proj=False,
        dextoshow=False,
        neglog=False,
        cmap2=False,
        levels=False,
        numthreads=8,
        verbose=1,
        INFO=None,
    ):
        if value not in self.data.keys():
            print(f"{value} not yet available for snapshot, calculating ...")
            self.data[value] = aux.get_value(
                variable_to_get=value, snap=self, INFO=INFO
            )
        return super().plot_Aslice(
            value,
            grad=grad,
            logplot=logplot,
            colorbar=colorbar,
            contour=contour,
            res=res,
            center=center,
            axes=axes,
            proj_fact=proj_fact,
            minimum=minimum,
            newfig=newfig,
            newlabels=newlabels,
            cmap=cmap,
            vrange=vrange,
            cblabel=cblabel,
            rasterized=rasterized,
            box=box,
            proj=proj,
            dextoshow=dextoshow,
            neglog=neglog,
            cmap2=cmap2,
            levels=levels,
            numthreads=numthreads,
            verbose=verbose,
        )


if __name__ == "__main__":
    import FigureMove as Fig

    snap = Fig.quickImport(10, folder="../output/")
    snap.get_Aslice("u", res=2031)
