import matplotlib as mpl

# import gadget
import os
import matplotlib.pyplot as plt

# import gadget
import numpy as np
import pickle
import csv
from collections import namedtuple
import time
import scipy.ndimage as ndimage
from scipy.special import beta
from scipy.special import betainc
from scipy.special import gamma
import math
import scipy.stats
import copy
import Param as param
import FileWorks as FW
from numpy import savetxt
import mpmath as mp
import auxiliary_functions as aux
import FigureMove as Fig


def ArrAy(thing):
    if type(thing) == list:
        thing = np.array(thing)
    return thing


def AlignSnapWithJet(snap, up=True, BH_pos=None, center=None, INFO=None):
    calculate = not aux.checkExistenceKey(INFO, "rotatebyhand", False)
    if not up:
        rotateXCenter = [0.5 / snap.hubbleparam for x in range(3)]
        snap = aux.RotatePositions(
            snap, center=rotateXCenter, theta=np.pi, axis=[0, 0, 1]
        )
    if center is None:
        center = [float(snap.boxsize) / 2.0 for x in range(3)]
    if BH_pos is None:
        BH_pos = [float(snap.boxsize) / 2.0 for x in range(3)]
    if calculate:
        tip_position = Fig.get_MostDistantJetCells(snap, TracerThresh=1e-3, up=True)
        diffVect = ArrAy(tip_position) - ArrAy(BH_pos)
        RotCenter = ArrAy(center)  # + 1./2. * diffVect
        RotAxis = aux.normalVectorPlane(diffVect, np.array([1, 0, 0]))
        theta = aux.AngleBetweenVectors(np.array([1, 0, 0]), diffVect)
        DistanceJetHeadBH = aux.DistanceVectors(BH_pos, tip_position)
    else:
        RotCenter = INFO["RotCenter"]
        theta = INFO["theta"]
        RotAxis = INFO["RotAxis"]
        DistanceJetHeadBH = INFO["DistanceJetHeadBH"]
    print("values for rot")
    print(RotCenter)
    print(theta)
    print(RotAxis)
    print(DistanceJetHeadBH)
    aux.RotatePositions(snap, RotCenter, theta, RotAxis)
    return snap, DistanceJetHeadBH


def ReduceGrids(
    GObjects,
    MinNcells,
    reductionType="average",
    includeTopPart=True,
    sliceAxis=1,
    INFO=None,
):
    averageFirst = aux.checkExistenceKey(INFO, "averageFirst", False)
    for object in GObjects:
        object.setIncludeTopPart(includeTopPart)
        object.LimitLength(MinNcells)
        if averageFirst:
            for i in range(object.dim):
                object.grids[i] = Fig.reduceArray(
                    object.grids[i], reductionType, 3 - sliceAxis
                )
    return GObjects


def getAlignedSlices(
    folder,
    number,
    Variables,
    Nwidth=4,
    dx=0.01,
    up=True,
    BH_pos=None,
    includeTopPart=True,
    sliceAxis=1,
    reductionType="average",
    plotPerp=False,
    plotProjection=True,
    projectionNameExt="",
    relativeToCenter=True,
    factorIncreaseSearchArea=4,
    numthreads=12,
    INFO=None,
    FieldsToLoad=["Coordinates", "Masses", "Volume", "Density", "u", "jetr"],
    useTime=True,
    depthSlice=1.0,
    snap=None,
):
    # have to modify to not cut central cells
    # slice axis shouldnt be 0... otherwise rotation along line of sight wouldnt work correctly ..
    MinNcells = None
    GObjects = []
    Nperps = 0
    labelAdd = ""
    if plotPerp:
        Nperps = np.size(AnglesPerp)
        GObjectsPerp = [[] for i in range(Nperps)]
        AnglesPerp = makeIterable(AnglesPerp, 1, List=False)
        print(
            "still %i more snaps to go for average radprof!"
            % (np.size(Numbers) - index)
        )
    if snap is None:
        snap = Fig.readSnap(folder, number, FieldsToLoad=FieldsToLoad, useTime=useTime)
        snap, time = Fig.getGeneralInformation(
            snap,
            convertedkpc=False,
            considering_h=False,
            StringTime=True,
            Precision=5,
            INFO=INFO,
        )
    if plotProjection:
        snapOriginal = copy.deepcopy(snap)
    # rotate to align with jet axis
    if aux.checkExistenceKey(INFO, "AlignSnapWithJet", True):
        snap, DistanceJetHeadBH = AlignSnapWithJet(
            snap, up=up, BH_pos=BH_pos, INFO=INFO
        )
    else:
        DistanceJetHeadBH = 1.0
    LengthOfShownJet = aux.checkExistenceKey(
        INFO, "LengthOfShownJet", DistanceJetHeadBH
    )
    # rotate along line of sight
    rotateAlongLineOfSight = aux.checkExistenceKey(
        INFO, "rotateAlongLineOfSight", False
    )
    if aux.checkExistenceKey(INFO, "OffsetRealJet", False):
        snap = Fig.do_AxisShift(snap, INFO, overallMask=snap.data["jetr"] > 1e-3)
    if rotateAlongLineOfSight:
        print("rotateAlongLineOfSight")
        rotateAlongLineOfSightAngle = aux.checkExistenceKey(
            INFO, "rotateAlongLineOfSightAngle", None
        )
        if rotateAlongLineOfSightAngle is not None:
            print(
                "rotateAlongLineOfSightAngle: %g*pi"
                % (rotateAlongLineOfSightAngle / np.pi)
            )
            snap = aux.RotatePositions(
                snap,
                [snap.boxsize / 2.0 for i in range(3)],
                rotateAlongLineOfSightAngle,
                [1 if i == 3 - sliceAxis else 0 for i in range(3)],
            )
    if plotPerp:
        SnapsPerp = [copy.deepcopy(snap) for i in range(Nperps)]
    if plotProjection:
        Masks = []
        center = [0.5 / snap.hubbleparam for i in range(3)]
        Masks.append(
            np.logical_and.reduce(
                (
                    snap.data["pos"][snap.data["type"] == 0][:, 0]
                    < LengthOfShownJet + center[0],
                    abs(snap.data["pos"][snap.data["type"] == 0][:, 1] - center[1])
                    < dx * Nwidth * (1 + (sliceAxis - 2) * 3),
                    abs(snap.data["pos"][snap.data["type"] == 0][:, 2] - center[2])
                    < dx * Nwidth * (1 + (sliceAxis - 1) * 3),
                )
            )
        )

    #     if plotPerp:
    #         centerperp = np.copy(center)
    #         centerperp[0] += DistanceJetHeadBH/2.
    #         for indexAng,angle in enumerate(AnglesPerp):
    #             aux.RotatePositions(SnapsPerp[indexAng],center,angle,RotAxis)
    #             if plotProjection:
    #                 Masks.append(np.logical_and.reduce((SnapsPerp[indexAng].part0.pos[:,0]<DistanceJetHeadBH+center[0],abs(SnapsPerp[indexAng].part0.pos[:,1]-center[1])<dx*Nwidth,abs(SnapsPerp[indexAng].part0.pos[:,2]-center[2])<dx*Nwidth)))

    for variable in Variables:

        gObj = aux.GetCentralCellsGeneral(
            snap,
            variable,
            LengthOfShownJet,
            Nwidth=Nwidth,
            dx=dx,
            INFO=INFO,
            numthreads=numthreads,
            includeTopPart=includeTopPart,
            factorIncreaseSearchArea=factorIncreaseSearchArea,
            center=None,
            returnSlice=True,
            sliceAxis=sliceAxis,
            depthSlice=depthSlice,
        )
        GObjects.append(gObj)

        #         if plotPerp:
        #             for indexAng,angle in enumerate(AnglesPerp):
        #                 for indexosa in range(3):
        #                     print SnapsPerp[indexAng].part0.pos[:,indexosa]
        #                 #exit()
        #                 res = 128
        #                 if False:
        #                     plot_Projection(SnapsPerp[indexAng],'Density', WEIGHT='JetMassFraction',ProjBox=[0.2,0.2,0.2],axis=[2,0],numthreads=8,vmin=None,vmax=None,ext=None,center=center,resolution=res,
        #                         colormap='jet',saveFig=True,filename='../figures/AverageWake/Projectiontest/%s_%s_%g_%03d.pdf'%(name,'Angle%s_' %aux.convert_params_to_string(angle*180./np.pi,string=False),res,number),
        #                         log=True,fontsize=32,textColor='white',labelsizeTicks=24,volumeIntegral=False, inputGrids=False)
        #                 gObj, label = aux.GetCentralCells(SnapsPerp[indexAng],variable,DistanceJetHeadBH,Nwidth=Nwidth,dx=dx,INFO=INFO,numthreads=numthreads,includeTopPart=includeTopPart,factorIncreaseSearchArea=factorIncreaseSearchArea)
        #                 GObjectsPerp[indexAng].append(gObj)
        #             [x.close() for x in SnapsPerp]
        if plotProjection:
            if variable == "PositionX_kpc1":
                continue
            print("working on plot_ProjectionContour")
            sideLengthLong = DistanceJetHeadBH * 2.2
            sideLengthShort = DistanceJetHeadBH * 1.6  # /6.
            # if not up:
            #    exit('down for mask not implemented for projection plot')
            axis = [2, 0]
            ProjBox = [sideLengthLong, sideLengthLong, sideLengthLong]
            ProjBox[int(np.sum(axis))] = sideLengthShort
            ProjBox[int(3 - np.sum(axis))] = 0.004
            weightsPlot = "JetMassFraction"
            weightsPlot = "Volume"
            resolution = [int(x / dx) for x in ProjBox]
            Testresolution = [int(x / np.max(ProjBox) * 512) for x in ProjBox]
            colorsContour = [
                "#bb596d",
                "#199967",
                "#2565ca",
                "#4f0917",
                "#FFA500",
                "#664200",
                "#0e2850",
                "#664200",
                "#54784c",
                "#0b3f00",
                "#031200",
            ]
            addName = ""
            name = "%s_%s_Nwid%i_Dx%s_Up%i_%s" % (
                variable,
                addName,
                Nwidth,
                aux.ReplaceDotInName(dx, "%s"),
                int(up),
                projectionNameExt,
            )
            figname = "../figures/AverageWake/Projection/%s_%s.pdf" % (
                name,
                os.path.basename(snap.filename)[5:-5],
            )
            # ElectronProtonFractioncopia = copy.deepcopy(snapOriginal)
            deltaAngleInitial = None
            if rotateAlongLineOfSight:
                deltaAngleInitial = rotateAlongLineOfSightAngle
            Fig.plot_ProjectionContour(
                copy.deepcopy(snapOriginal),
                deltaAngle=np.pi / 4.0,
                axis=axis,
                figname=figname,
                showTime=True,
                vmin=param.getGlobalVminVmax(variable)[0],
                vmax=param.getGlobalVminVmax(variable)[1],
                cbarLabel=r"$colorbar$",
                plotContour=True,
                mask=Masks,
                INFO=INFO,
                variable=variable,
                box=[sideLengthShort, sideLengthLong],
                center=center,
                numthreads=8,
                noTicks=False,
                ProjBox=ProjBox,
                res=Testresolution,
                ContourWeights="JetMassFraction",
                ContourProjection=False,
                useContourWeights=False,
                slice=False,
                weights_variable=weightsPlot,
                colorsContour=colorsContour,
                howmanyX=5,
                howmanyY=9,
                initialTilt=rotateAlongLineOfSight,
                deltaAngleInitial=deltaAngleInitial,
                RotAxisInitial=[1 if i == 3 - sliceAxis else 0 for i in range(3)],
            )
            print("finished working on plot_ProjectionContour")
            # copia.close()
    spatialSpacingArrays_in_cm = dx * snap.UnitLength_in_cm / snap.hubbleparam
    snap.close()
    Ncells = int(LengthOfShownJet / dx)
    # exit()
    if MinNcells is None or MinNcells > Ncells:
        MinNcells = Ncells
    if includeTopPart:
        MinNcells *= 2
    GObjects = ReduceGrids(
        GObjects,
        MinNcells,
        reductionType=reductionType,
        includeTopPart=includeTopPart,
        sliceAxis=sliceAxis,
        INFO=INFO,
    )
    x = np.linspace(0, MinNcells * dx, MinNcells)
    if relativeToCenter:
        x = np.array(x) - DistanceJetHeadBH  # (tip_position[0]-center[0])

    return x, [g.grids for g in GObjects], spatialSpacingArrays_in_cm, labelAdd


def ifnotAlreadyInclude(List, Objects):
    for object in Objects:
        if object not in List:
            List.append(object)
    return List


def getDeltaIRadprof(
    folder,
    number,
    sliceAxis=1,
    snap=None,
    dx=0.001,
    Nwidth=4,
    up=True,
    factorIncreaseSearchArea=4,
    numthreads=12,
    depthSlice=1.0,
    INFO=None,
    projectionNameExt="",
):
    averageFirst = aux.checkExistenceKey(INFO, "averageFirst", False)
    Variables = ["PositionX_kpc1"]
    include = aux.checkExistenceKey(
        INFO, "include", ["thermal", "kinetic", "relativistic"]
    )
    if "thermal" in include:
        temperatureVariable = checkExistenceKey(
            INFO, "TemperatureVariableThSZ", "Temperature"
        )
        densityVariable = checkExistenceKey(
            INFO, "ElectronDensityThSZ", "ElectronNumberDensity"
        )
        Variables = ifnotAlreadyInclude(
            Variables, [temperatureVariable, densityVariable]
        )
    if "kinetic" in include:
        velocityVariable = checkExistenceKey(INFO, "VelocityVariableKinSZ", "VelocityY")
        densityVariable = checkExistenceKey(
            INFO, "ElectronDensityKinSZ", "ElectronNumberDensity"
        )
        Variables = ifnotAlreadyInclude(Variables, [velocityVariable, densityVariable])
    if "relativistic" in include:
        pressureVariable = checkExistenceKey(
            INFO, "PressureContributionCReLob", "PressureLob"
        )
        Variables = ifnotAlreadyInclude(Variables, [pressureVariable])
    x, Values, spatialSpacingArrays_in_cm, label = getAlignedSlices(
        folder,
        number,
        Variables,
        Nwidth=Nwidth,
        dx=dx,
        up=up,
        BH_pos=None,
        includeTopPart=True,
        sliceAxis=sliceAxis,
        reductionType="average",
        plotPerp=False,
        plotProjection=False,
        relativeToCenter=True,
        factorIncreaseSearchArea=factorIncreaseSearchArea,
        numthreads=numthreads,
        INFO=INFO,
        FieldsToLoad=["Coordinates", "Masses", "Volume", "Density", "u", "jetr"],
        useTime=True,
        depthSlice=depthSlice,
        snap=snap,
        projectionNameExt=projectionNameExt,
    )
    VariableValues = {}
    for i, variableName in enumerate(Variables):
        VariableValues[variableName] = Values[i][0]
    # temperature = Values[0][0]
    # elecnumberdensity = Values[1][0]
    # print np.shape(Values[2][0])
    distance = np.average(VariableValues["PositionX_kpc1"], axis=1) - 0.5 / 0.67 * 1e3
    print("shapes of deltaI rapdorf")
    print(np.shape(distance))
    # print np.shape(temperature)
    if averageFirst:
        integratingAxis = 1
    else:
        integratingAxis = sliceAxis
    deltaI = DeltaI(
        VariableValues,
        spatialSpacingArrays_in_cm=spatialSpacingArrays_in_cm,
        integratingAxis=integratingAxis,
        include=include,
        INFO=INFO,
    )
    if not averageFirst:
        deltaI = np.average(deltaI, axis=1)
    print(np.shape(deltaI))
    return distance, deltaI, label  # x*1000, deltaI


def y_gas(
    elecnumberdensity,
    temperature,
    spatialSpacingArrays_in_cm,
    integratingAxis,
    INFO=None,
):
    cutTempKeV = aux.checkExistenceKey(INFO, "SZThermalTemperatureCutInKeV", None)

    if cutTempKeV is not None:
        tempCutInK = cutTempKeV / param.BOLTZMANN_in_eV_per_K * 1e3
        print("tempCutInK in %g" % tempCutInK)
        ArrAy(temperature)[ArrAy(temperature) > tempCutInK] = 0
    if integratingAxis is None:
        int = (
            ArrAy(elecnumberdensity)
            * ArrAy(temperature)
            * (1 + relCorrSZ(temperature, INFO))
        )
    else:
        int = np.sum(
            ArrAy(elecnumberdensity)
            * ArrAy(temperature)
            * (1 + relCorrSZ(temperature, INFO))
            * spatialSpacingArrays_in_cm,
            axis=integratingAxis,
        )
    int *= (
        param.BOLTZMANN_cgs
        * param.THOMSONCROSSSECTION_cgs
        / (param.ElECTRONMASS_cgs * param.SPEEDOFLIGHT_cgs ** 2)
    )
    return int


def w_gas(
    elecnumberdensity, velocity, spatialSpacingArrays_in_cm, integratingAxis, INFO=None
):
    cutVelcms = aux.checkExistenceKey(INFO, "SZKineticVelocityCutInCmPs", None)
    if cutVelcms is not None:
        print("cutVelcms in %g" % cutVelcms)
        ArrAy(velocity)[ArrAy(velocity) > cutVelcms] = 0
    if integratingAxis is None:
        int = ArrAy(elecnumberdensity) * ArrAy(velocity)
    else:
        int = np.sum(
            ArrAy(elecnumberdensity) * ArrAy(velocity) * spatialSpacingArrays_in_cm,
            axis=integratingAxis,
        )
    int *= param.THOMSONCROSSSECTION_cgs / param.SPEEDOFLIGHT_cgs
    if aux.checkExistenceKey(INFO, "InvKineticSZSignal", False):
        int *= -1.0
    return int


def wToyRatio_gas(
    elecnumberdensity,
    velocity,
    temperature,
    spatialSpacingArrays_in_cm,
    integratingAxis,
    INFO=None,
):
    w = w_gas(
        elecnumberdensity,
        velocity,
        spatialSpacingArrays_in_cm,
        integratingAxis,
        INFO=INFO,
    )
    y = y_gas(
        elecnumberdensity,
        temperature,
        spatialSpacingArrays_in_cm,
        integratingAxis,
        INFO=INFO,
    )
    return np.abs(w / y)


def tau_rel(
    pressureToReplaceWithCRe, spatialSpacingArrays_in_cm, integratingAxis, INFO
):
    # findValueList = checkExistenceKey(INFO, 'findValueList', True)
    n_relE = calcN_relE(pressureToReplaceWithCRe, INFO)
    if integratingAxis is None:
        int = n_relE
    else:
        int = np.sum((n_relE) * spatialSpacingArrays_in_cm, axis=integratingAxis)
    int *= param.THOMSONCROSSSECTION_cgs
    return int


def calcN_relE(pressureToReplaceWithCRe, INFO):
    p1, p2, alpha, betaTh, Jassumption = getConsts(INFO)
    function = pressureRelE
    functionParameters = p1, p2, alpha, betaTh, Jassumption
    addInfo = ""
    addInfo += "_%s" % Jassumption
    if Jassumption == "PcrPowerLaw" or Jassumption == "fEPowerLaw":
        relevantElements = [p1, p2, alpha]
        header = tuple(("p1", "p2", "alpha"))
    elif Jassumption == "fEThermal" or Jassumption == "jZero":
        relevantElements = [betaTh]
        header = ("betaTh",)
    filename = "Data/SZ_PressRelE%s.csv" % (addInfo)
    relevantElementsNumbers = list(np.arange(np.size(relevantElements)))
    header += ("PressureRelE",)
    value = FW.checkRetrieveValueCSV(
        filename,
        header,
        function,
        functionParameters,
        relevantElementsNumbers=relevantElementsNumbers,
        relevantElements=relevantElements,
    )  # [0]
    return pressureToReplaceWithCRe / value


def pressureRelE(p1, p2, alpha, betaTh, Jassumption):
    print("solving pressure RelE integral nummerically")
    print(p1, p2, alpha, betaTh, Jassumption)
    if Jassumption == "PcrPowerLaw" or Jassumption == "fEPowerLaw":
        function = (
            lambda p: param.ElECTRONMASS_cgs
            * param.SPEEDOFLIGHT_cgs ** 2
            / 3.0
            * fepPL(p, p1, p2, alpha)
            * p ** 2
            / np.sqrt(1 + p ** 2)
        )
        result = Integral(function, p1, p2)  # p1, p2)
        # 5.70560428825794e-7#for p1=0.5, p2=1e3, alpha=2.2; pressureRelE(INFO)
    elif Jassumption == "fEThermal" or Jassumption == "jZero":
        function = (
            lambda p: param.ElECTRONMASS_cgs
            * param.SPEEDOFLIGHT_cgs ** 2
            / 3.0
            * fepTH(p, betaTh)
            * p ** 2
            / np.sqrt(1 + p ** 2)
        )
        result = Integral(function, 0.0, np.inf)
    # else: only valid for power law cr distribution
    #     result = param.ElECTRONMASS_cgs*param.SPEEDOFLIGHT_cgs**2*(alpha-1.) / (6.*(p1**(1.-alpha)-p2**(1.-alpha)))* \
    #     (mp.betainc((alpha-2.)/2.,(3.-alpha)/2., x2=1./(1.+p1**2))-mp.betainc((alpha-2.)/2.,(3.-alpha)/2., x2=1./(1.+p2**2)))
    return float(result)


def fepPL(p, p1, p2, alpha=2.0, INFO=None):
    # power law of highly relativistic electron population
    # print 'nomralize fepl %g' %Integral(lambda p: (alpha-1.)*p**(-alpha) / (p1**(1.-alpha)-p2**(1.-alpha)), p1, p2)
    return (alpha - 1.0) * p ** (-alpha) / (p1 ** (1.0 - alpha) - p2 ** (1.0 - alpha))


def fepTH(p, betaTh, INFO=None):
    # thermal distribution of mildly relativistic electron population
    # print Integral(lambda p: betaTh/mp.besselk(2, betaTh) * p**2 * mp.exp(-betaTh*mp.sqrt(1+p**2)),0,np.inf)
    return (
        betaTh / mp.besselk(2, betaTh) * p ** 2 * mp.exp(-betaTh * mp.sqrt(1 + p ** 2))
    )


def Ptp(t, p):
    return -3.0 * abs(1.0 - t) / (32.0 * p ** 6 * t) * (
        1.0 + (10.0 + 8.0 * p ** 2 + 4.0 * p ** 4) * t + t ** 2
    ) + 3.0 * (1.0 + t) / (8.0 * p ** 5) * (
        (3.0 + 3.0 * p ** 2 + p ** 4) / np.sqrt(1.0 + p ** 2)
        - (3.0 + 2.0 * p ** 2) / (2.0 * p) * (2.0 * mp.asinh(p) - abs(mp.log(t)))
    )


def i_x(x):
    return x ** 3 / (mp.exp(x) - 1.0)


def jx_intfct(t, p, x, INFO=None):
    return fep(p, INFO=INFO) * Ptp(t, p) * ix(x / t)


def jx_intfctLog(
    variable, x, p1, p2, alpha, betaTh, Jassumption, INFO=None
):  # ,x=1.,INFO=None):
    # p1 = checkExistenceKey(INFO, 'RelSZp1', 1.)
    # p2 = checkExistenceKey(INFO, 'RelSZp2', 1e3)
    # alpha = checkExistenceKey(INFO, 'RelSZalpha', 2.)
    if Jassumption == "fEPowerLaw":
        return (
            PcrGeneral(alpha, mp.exp(variable), p1, p2, betaTh, PowerLawCRs=True)
            * i_x(x * mp.exp(-variable))
            * mp.exp(variable)
        )
    elif Jassumption == "PcrPowerLaw":
        return (
            PcrPowLaw(alpha, mp.exp(variable), p1, p2)
            * i_x(x * mp.exp(-variable))
            * mp.exp(variable)
        )  # fep(p,INFO=INFO)*Ptp(t,p)*
    elif Jassumption == "fEThermal":
        return (
            PcrGeneral(alpha, mp.exp(variable), p1, p2, betaTh, PowerLawCRs=False)
            * i_x(x * mp.exp(-variable))
            * mp.exp(variable)
        )
    else:
        print(Jassumption)
        exit("assumption for j(x) factor not included jet!")


def checkExistenceKey(dic, key, y):
    if dic is not None:
        if key in list(dic.keys()):
            return dic[key]
    return y


def findIndexValue(value, Array, NumberOfOccurence=1):
    N = np.size(Array)
    diffDataOld = abs(value - 0)
    number = 0
    for i in range(1, N):
        diffDataNew = abs(Array[i] - value)
        if diffDataNew <= diffDataOld:
            number = i
            diffDataOld = diffDataNew
        else:
            if NumberOfOccurence == 1:
                break
            else:
                NumberOfOccurence -= 1
    return number


def jx(x, INFO=None):
    arguments = (x, INFO)
    lowerP = 1.0
    upperP = 1e3
    lowerT = 0
    import mpmath as mp

    upperT = mp.exp(2.0 * np.arcsinh(upperP))  # np.inf
    print("upper limit T")
    print(np.exp(2.0 * np.arcsinh(upperP)))
    print("normalization for fe")
    # from scipy import integrate
    # print integrate.quad(fep,lowerP,upperP)[0]
    return IntDbl(
        jx_intfct, lowerP, upperP, lowerT, upperT, arguments
    )  # /integrate.quad(fep,0,np.inf)[0]


def jxLog(x, p1, p2, alpha, betaTh, Jassumption, INFO=None):
    # arguments = (x,INFO)
    # p1 = lowerP = checkExistenceKey(INFO, 'RelSZp1', 1.)
    # p2 = upperP = checkExistenceKey(INFO, 'RelSZp2', 1e3)
    lowerT = -2.0 * np.arcsinh(p2)  # lambda t: 0
    upperT = 2.0 * np.arcsinh(p2)  # lambda t: mp.exp(2.*np.arcsinh(upperP))#np.inf
    fct = lambda t: jx_intfctLog(t, x, p1, p2, alpha, betaTh, Jassumption, INFO)
    if Jassumption == "jZero":
        int = 0.0
    else:
        int = Integral(
            fct, lowerT, upperT
        )  # +Integral(fct, 0., upperT)#(jx_intfctLog, lowerP, upperP, lowerT, upperT, arguments)#/integrate.quad(fep,0,np.inf)[0]
    return int


def PcrPowLaw(alpha, t, p1, p2):
    # scipy.special.betainc(a, b, x) w/ integral gamma(a+b) / (gamma(a)*gamma(b)) * integral(t**(a-1) (1-t)**(b-1), t=0..x).
    p2Tild = p2  # np.max([p2, np.sqrt(t)/2.])#np.max([p2, np.sqrt(t)/2.])#p2#
    p1Tild = np.min(
        [p2Tild, np.max([p1, abs(np.sqrt(t) - 1 / np.sqrt(t)) / 2.0])]
    )  # np.max([p1, np.sqrt(t)/2.])#np.min([p2, np.max([p1, abs(np.sqrt(t)-1/np.sqrt(t))/2.])])#
    # print p1Tild
    # print p2Tild
    return (
        (alpha - 1.0)
        / (p1 ** (1.0 - alpha) - p2 ** (1.0 - alpha))
        * 3.0
        / 16.0
        * (1 + t)
        * (Betas(p2Tild, t, alpha) - Betas(p1Tild, t, alpha))
    )


def PcrGeneral(alpha, t, p1, p2, betaTh, PowerLawCRs=True):
    # scipy.special.betainc(a, b, x) w/ integral gamma(a+b) / (gamma(a)*gamma(b)) * integral(t**(a-1) (1-t)**(b-1), t=0..x).
    p1Tild = np.min(
        [p2, np.max([p1, abs(np.sqrt(t) - 1 / np.sqrt(t)) / 2.0])]
    )  # np.max([p1, np.sqrt(t)/2.])#np.min([p2, np.max([p1, abs(np.sqrt(mp.exp(t))-1/np.sqrt(mp.exp(t)))/2.])])#np.max([p1, np.sqrt(np.exp(s))/2.])##
    p2Tild = p2  # np.max([p2, np.sqrt(t)/2.])#p2#np.max([p2, np.sqrt(np.exp(s))/2.])#
    # print p1Tild
    # print p2Tild
    if PowerLawCRs:
        ElectronDistribution = lambda p: fepPL(p, p1, p2, alpha, INFO=None)
    else:
        ElectronDistribution = lambda p: fepTH(p, betaTh, INFO=None)
    function = lambda p: ElectronDistribution(p) * Ptp(t, p)  # *t
    return Integral(function, p1Tild, p2Tild)


def Betas(p, t, alpha):
    import scipy.special  # as betainc

    return (
        -mp.betainc((1.0 + alpha) / 2.0, -alpha / 2.0, x2=1.0 / (1.0 + p ** 2))
        - mp.betainc((3.0 + alpha) / 2.0, -(2.0 + alpha) / 2.0, x2=1.0 / (1.0 + p ** 2))
        * (7.0 + 3 * alpha)
        / (3.0 + alpha)
        - mp.betainc((5.0 + alpha) / 2.0, -(4.0 + alpha) / 2.0, x2=1.0 / (1.0 + p ** 2))
        * (12.0 + 3 * alpha)
        / (5.0 + alpha)
        + p ** (-5.0 - alpha)
        * (
            (3.0 / (5.0 + alpha) + 2.0 * p ** 2 / (3.0 + alpha))
            * (2 * mp.asinh(p) - abs(mp.log(t)))
            + abs((1.0 - t) / (1.0 + t))
            * (
                (1.0 + t ** 2) / (2.0 * (5.0 + alpha) * t)
                + 5.0 / (5.0 + alpha)
                + 4.0 * p ** 2 / (3.0 + alpha)
                + 2.0 * p ** 4 / (1.0 + alpha)
            )
        )
    )


#    return  - scipy.special.betainc((1.+alpha)/2., -alpha/2., 1./(1.+p**2)) \
#        - scipy.special.betainc((3.+alpha)/2.,-(2.+alpha)/2.,1./(1.+p**2))*(7.+3*alpha)/(3.+alpha) \
#        - scipy.special.betainc((5.+alpha)/2.,-(4.+alpha)/2.,1./(1.+p**2))*(12.+3*alpha)/(5.+alpha) \
#        + p**(-5.-alpha) * ((3./(5.+alpha)+2.*p**2/(3.+alpha))*(2*np.arcsinh(p)-abs(mp.log(t))) + abs((1.-t)/(1.+t))*((1.+t**2)/(2.*(5.+alpha)*t) + 5./(5.+alpha) + 4.*p**2/(3.+alpha) + 2.*p**4/(1.+alpha))# )


def Integral(function, lower, upper, arguments=None, points=None):
    import scipy.integrate as integrate

    if lower < 0.0 and upper > 0:
        boundaries = [lower, 0, upper]
    else:
        boundaries = [lower, upper]
    # return integrate.quad(function, lower, upper)[0]#, args=arguments, points=points)[0]
    return mp.quad(
        function, boundaries, method="tanh-sinh"
    )  # , verbose=True)#'gauss-legendre', )#, args=arguments)


def IntDbl(function, lowerX, upperX, lowerY, upperY, arguments):
    # integraten f(y,x) give boundaries as named
    from scipy import integrate

    return custom_dblquad(function, lowerX, upperX, lowerY, upperY, args=arguments)[0]


def _infunc(x, func, gfun, hfun, more_args):
    from scipy import integrate

    a = gfun(x)
    b = hfun(x)
    myargs = (x,) + more_args
    return integrate.quad(func, a, b, args=myargs, limit=400)[0]


def custom_dblquad(
    func, a, b, gfun, hfun, args=(), epsabs=1.49e-8, epsrel=1.49e-8, maxp1=50, limit=100
):
    from scipy import integrate

    return integrate.quad(
        _infunc,
        a,
        b,
        (func, gfun, hfun, args),
        epsabs=epsabs,
        epsrel=epsrel,
        maxp1=maxp1,
        limit=limit,
    )


def get_PrefacINTS(xFreq, deltaX, prefac, p1, p2, alpha, betaTh, Jassumption, INFO):
    lower = xFreq - deltaX / 2.0
    upper = xFreq + deltaX / 2.0
    if prefac == "g":
        function = lambda x: g(x)
    elif prefac == "h":
        function = lambda x: h(x)
    elif prefac == "jmini":
        function = lambda x: jxLog(
            x, p1, p2, alpha, betaTh, Jassumption, INFO=INFO
        ) - i_x(x)
    value = Integral(function, lower, upper) / deltaX
    return float(value)


def get_Prefac(xFreq, prefac, p1, p2, alpha, betaTh, Jassumption, INFO):
    if prefac == "g":
        value = g(xFreq)
    elif prefac == "h":
        value = h(xFreq)
    elif prefac == "jmini":
        value = jxLog(xFreq, p1, p2, alpha, betaTh, Jassumption, INFO=INFO) - i_x(xFreq)
    return float(value)


def getConsts(INFO):
    p1 = aux.checkExistenceKey(INFO, "RelSZp1", 1.0)
    p2 = aux.checkExistenceKey(INFO, "RelSZp2", 1e3)
    alpha = aux.checkExistenceKey(INFO, "RelSZalpha", 2.0)
    betaTh = aux.checkExistenceKey(INFO, "RelTherSZBeta", 50.0)
    # normalized thermal beta paramter beta=m_e c^2/ k T_e
    Jassumption = aux.checkExistenceKey(INFO, "PtpAssumptions", "PcrPowerLaw")
    # possibilities:
    # 1: use analytical P_tp for highly relativistic distribution 'PcrPowerLaw';
    # 2: use P_tp for highly relativistic distribution 'fEPowerLaw';
    # 3: use P_tp for trans-relativistic distribution 'fEThermal';
    if (Jassumption == "fEPowerLaw" or Jassumption == "PcrPowerLaw") and (
        p1 is None or p2 is None or alpha is None
    ):
        print(Jassumption)
        print(p1, p2, alpha)
        exit(
            "need to specify p1, p2, alpha if want to look at power law for relativistic CRe distribution!"
        )
    if Jassumption == "fEThermal" and betaTh is None:
        exit(
            "Need to specify betaTh if want to look at thermal distribution of trans-relativistic electrons!"
        )
    print("making assumption for j(x) that: %s" % Jassumption)
    print("using p1: %g, p2: %g, alpha: %g, betaTh: %g" % (p1, p2, alpha, betaTh))
    return p1, p2, alpha, betaTh, Jassumption


def get_Prefacs(xFreq, prefac, useTelBandInt=False, INFO=None):
    ignorePrefac = aux.checkExistenceKey(INFO, "ignorePrefacSZ", False)
    if ignorePrefac:
        print("ignoring prefactors SZ!")
        return 1.0
    p1, p2, alpha, betaTh, Jassumption = getConsts(INFO)
    if useTelBandInt:
        function = get_PrefacINTS
        telescope = aux.checkExistenceKey(INFO, "Telescope", None)
        deltaFrequency = aux.checkExistenceKey(INFO, "DeltaFrequency", None)
        if telescope is None or deltaFrequency is None:
            print(telescope, deltaFrequency)
            exit("have to specify delta frequency as well as telescope")
        relevantElements = [telescope, xFreq, deltaFrequency]
        header = tuple(("Telescope", "Frequency", "DeltaFrequency"))
        functionParameters = (
            xFreq,
            deltaFrequency,
            prefac,
            p1,
            p2,
            alpha,
            betaTh,
            Jassumption,
            INFO,
        )
        addInfo = ""
        if prefac == "jmini":
            addInfo += "_%s" % Jassumption
            if Jassumption == "PcrPowerLaw" or Jassumption == "fEPowerLaw":
                relevantElements += [p1, p2, alpha]
                header += tuple(("p1", "p2", "alpha"))
            elif Jassumption == "fEThermal":
                relevantElements += [betaTh]
                header += ("betaTh",)
        filename = "Data/SZ_%sX_TelBandInt%s.csv" % (prefac, addInfo)
        relevantElementsNumbers = list(np.arange(np.size(relevantElements)))
        header += ("GXINT",)
        value = FW.checkRetrieveValueCSV(
            filename,
            header,
            function,
            functionParameters,
            relevantElementsNumbers=relevantElementsNumbers,
            relevantElements=relevantElements,
        )  # [0]
    else:

        relevantElements = [xFreq]
        function = get_Prefac
        functionParameters = xFreq, prefac, p1, p2, alpha, betaTh, Jassumption, INFO
        header = ("Frequency",)
        addInfo = ""
        if prefac == "jmini":
            addInfo += "_%s" % Jassumption
            if Jassumption == "PcrPowerLaw" or Jassumption == "fEPowerLaw":
                relevantElements += [p1, p2, alpha]
                header += tuple(("p1", "p2", "alpha"))
            elif Jassumption == "fEThermal":
                relevantElements += [betaTh]
                header += ("betaTh",)
        filename = "Data/SZ_%sX%s.csv" % (prefac, addInfo)
        relevantElementsNumbers = list(np.arange(np.size(relevantElements)))
        header += ("Value",)
        value = FW.checkRetrieveValueCSV(
            filename,
            header,
            function,
            functionParameters,
            relevantElementsNumbers=relevantElementsNumbers,
            relevantElements=relevantElements,
        )  # [0]
    if aux.checkExistenceKey(INFO, "mJyParcmin2", False):
        print("converting to mJyParcmin2")
        print(value)
        value = (
            float(value)
            * 2
            * (param.BOLTZMANN_cgs * param.TEMPERATURECMB_K) ** 3
            / (param.PLANCKSCONSTANT_cgs * param.SPEEDOFLIGHT_cgs) ** 2
            / 1e-26
            * (np.pi / (180 * 60)) ** 2
        )
        print(value)
    return value


def g(x):
    # return x**4*np.exp(x)/(np.exp(x)-1.)**2 * (x*(np.exp(x)+1.)/(np.exp(x)-1.)-4.)
    return (
        x ** 4
        * mp.exp(x)
        / (mp.exp(x) - 1.0) ** 2
        * (x * (mp.exp(x) + 1.0) / (mp.exp(x) - 1.0) - 4.0)
    )


def h(x):
    # return x**4*np.exp(x)/(np.exp(x)-1.)**2
    return x ** 4 * mp.exp(x) / (mp.exp(x) - 1.0) ** 2


def coth(x):
    return np.cosh(x) / np.sinh(x)


def constsItoh1998(x):
    Xtild = x * coth(x / 2.0)
    Stild = x / np.sinh(x / 2.0)
    Y0 = -4 + Xtild
    Y1 = (
        -10.0
        + 47 / 2.0 * Xtild
        - 42 / 5.0 * Xtild ** 2
        + 7 / 10.0 * Xtild ** 3
        + Stild ** 2 * (-21 / 5.0 + 7 / 5.0 * Xtild)
    )
    Y2 = (
        -15 / 2.0
        + 1023 / 8.0 * Xtild
        - 868 / 5.0 * Xtild ** 2
        + 329 / 5.0 * Xtild ** 3
        - 44 / 5.0 * Xtild ** 5
        + Stild ** 2
        * (
            -434 / 5.0
            + 658 / 5.0 * Xtild
            - 242 / 5.0 * Xtild ** 2
            + 143 / 30.0 * Xtild ** 3
        )
        + Stild ** 4 * (-44 / 5.0 + 187 / 60.0 * Xtild)
    )
    Y3 = (
        15 / 2.0
        + 2505 / 8.0 * Xtild
        - 7098 / 5.0 * Xtild ** 2
        + 14253 / 10.0 * Xtild ** 3
        - 18594 / 35.0 * Xtild ** 4
        + 12059 / 140.0 * Xtild ** 5
        - 128 / 21.0 * Xtild ** 6
        + 16 / 105.0 * Xtild ** 7
        + Stild ** 2
        * (
            -7098 / 10.0
            + 14253 / 5.0 * Xtild
            - 102267 / 35.0 * Xtild ** 2
            + 156767 / 140.0 * Xtild ** 3
            - 1216 / 7.0 * Xtild ** 4
            + 64 / 7.0 * Xtild ** 5
        )
        + Stild ** 4
        * (
            -18594 / 35.0
            + 205003 / 280.0 * Xtild
            - 1920 / 7.0 * Xtild ** 2
            + 1024 / 35.0 * Xtild ** 3
        )
        + Stild ** 6 * (-544 / 21.0 + 992 / 105.0 * Xtild)
    )
    Y4 = (
        -135 / 32.0
        + 30375 / 128.0 * Xtild
        - 62391 / 10 * Xtild ** 2
        + 614727 / 40.0 * Xtild ** 3
        - 124389 / 10.0 * Xtild ** 4
        + 355703 / 80.0 * Xtild ** 5
        - 16568 / 21.0 * Xtild ** 6
        + 7516 / 105.0 * Xtild ** 7
        - 22 / 7.0 * Xtild ** 8
        + 11 / 210.0 * Xtild ** 9
        + Stild ** 2
        * (
            -62391 / 20.0
            + 614727 / 20.0 * Xtild
            - 1368279 / 20.0 * Xtild ** 2
            + 4624139 / 80.0 * Xtild ** 3
            - 157396 / 7.0 * Xtild ** 4
            + 30064 / 7.0 * Xtild ** 5
            - 2717 / 7.0 * Xtild ** 6
            + 2761 / 210.0 * Xtild ** 7
        )
        + Stild ** 4
        * (
            -124389 / 10.0
            + 6046951 / 160.0 * Xtild
            - 248520 / 7.0 * Xtild ** 2
            + 481024 / 35.0 * Xtild ** 3
            - 15972 / 7.0 * Xtild ** 4
            + 18689 / 140.0 * Xtild ** 5
        )
        + Stild ** 6
        * (
            -70414 / 21.0
            + 465992 / 105.0 * Xtild
            - 11792 / 7.0 * Xtild ** 2
            + 19778 / 105.0 * Xtild ** 3
        )
        + Stild ** 8 * (-682 / 7.0 + 7601 / 210.0 * Xtild)
    )
    return Y0, Y1, Y2, Y3, Y4


def relCorrSZ(temperature, INFO):
    # assumes temperature in K
    if not aux.checkExistenceKey(INFO, "useThSZRelCor", False):
        return 0.0
    x = aux.checkExistenceKey(INFO, "xFreq", None)
    if x is None:
        exit("specify xFreq of SZ signal for relativistic corrections!")
    deltaE = (
        temperature * param.BOLTZMANN_in_eV_per_K / 1e3 / 510.9989
    )  # convert to keV and then normalize with m_e*c^2
    mask = np.logical_and(0.001 <= deltaE, deltaE <= 0.1)
    # only valid in range 0.05 <= deltaE <= 0.1
    # very accurate until 15 keV (deltaE=0.03) in rayleigh-jeans regime (X<=1)
    print("xFreq for rel corr %f" % x)
    print(
        "temperature of %i cell(s) too high to calculate relativistic corrections (frac: %.4f)"
        % (
            np.size(deltaE[deltaE > 0.1]),
            float(np.size(deltaE[deltaE > 0.1])) / np.size(deltaE),
        )
    )
    print(
        "max temperature detected %.2f keV"
        % (np.max(temperature) * param.BOLTZMANN_in_eV_per_K / 1e3)
    )
    result = np.zeros(np.shape(deltaE))
    deltaE = deltaE[mask]

    if 0 <= x and x <= 1.2:
        Y0, Y1, Y2, Y3, Y4 = constsItoh1998(x)
        result[mask] = (
            Y1 * deltaE + Y2 * deltaE ** 2 + Y3 * deltaE ** 3 + Y4 * deltaE ** 4
        ) / Y0
    elif 1.2 < x and x <= 17:
        exit("untested regime for relCorrSZ!")
        a = getAIto2004()
        X0 = getX0AIto2004(deltaE)
        prefac = deltaE * x ** 2 * (x - X0)
        result[mask] = np.sum(
            prefac * a[i, j] * (10.0 * deltaE) ** i * (x / 20.0) ** j
            for i in range(13)
            for j in range(13)
        )
    else:
        print(x)
        exit("x not included!")

    return result


def getAIto2004():
    return np.loadtxt("CleanedIto2004IJ.txt")


def getX0AIto2004(deltaE):
    return 3.830 * (1.0 + 1.1674 * deltaE - 0.8533 * deltaE ** 2)


def get_SZEffect(
    variable="ThermalSz",
    Variables=None,
    spatialSpacingArrays_in_cm=None,
    integratingAxis=1,
    include=["thermal", "kinetic", "relativistic"],
    INFO=None,
):
    xFreq = aux.checkExistenceKey(INFO, "xFreq", 1.0)
    if variable == "ThermalSz":
        result = y_gas(
            Variables["elecnumberdensity"],
            Variables["temperature"],
            spatialSpacingArrays_in_cm,
            integratingAxis,
            INFO=INFO,
        )
    elif variable == "KineticSz":
        result = w_gas(
            Variables["elecnumberdensity"],
            Variables["velocity"],
            spatialSpacingArrays_in_cm,
            integratingAxis,
            INFO=INFO,
        )
    elif variable == "KineticToThermalSZRatio":
        result = wToyRatio_gas(
            Variables["elecnumberdensity"],
            Variables["velocity"],
            Variables["temperature"],
            spatialSpacingArrays_in_cm,
            integratingAxis,
            INFO=INFO,
        )
    elif variable == "RelativisticSz":
        # result = jitau(xFreq, Variables['pressure'], spatialSpacingArrays_in_cm, integratingAxis, INFO=INFO)
        result = tau_rel(
            Variables["pressure"],
            spatialSpacingArrays_in_cm,
            integratingAxis,
            INFO=INFO,
        )
    else:
        print(variable)
        exit("variable not yet defined in SZ effect!")
    return result


# relative change in flux density as function of dimensionles frequency x=h nu/ k_B T_CMB for line of sight through galaxy cluster
def DeltaI(
    VariableValues,
    spatialSpacingArrays_in_cm=None,
    integratingAxis=1,
    include=["thermal", "kinetic", "relativistic"],
    INFO=None,
    singleCompDic=False,
):
    include = aux.checkExistenceKey(INFO, "include", include)
    xFreq = aux.checkExistenceKey(INFO, "xFreq", None)
    if xFreq is None:
        exit("specify xFreq of SZ signal!")
    useTelBandInt = aux.checkExistenceKey(INFO, "useTelBandInt", True)
    """
        delta corresponds to relativistic effect if thermal gas too hot, relevant here?, set to 0 for now
    """
    delta = 0
    deltaI = 0
    print("shape in deltaI")
    print(list(VariableValues.keys()))
    print(integratingAxis)
    DICSingleComps = {}
    if "thermal" in include:
        temperatureVariable = checkExistenceKey(
            INFO, "TemperatureVariableThSZ", "Temperature"
        )
        densityVariable = checkExistenceKey(
            INFO, "ElectronDensityThSZ", "ElectronNumberDensity"
        )
        if not singleCompDic:
            deltaI += get_Prefacs(
                xFreq, "g", useTelBandInt=useTelBandInt, INFO=INFO
            ) * y_gas(
                VariableValues[densityVariable],
                VariableValues[temperatureVariable],
                spatialSpacingArrays_in_cm,
                integratingAxis,
                INFO=INFO,
            )  # * (1. + relCorrSZ(xFreq,temperature)) * g(xFreq)
        else:
            DICSingleComps["thermal"] = get_Prefacs(
                xFreq, "g", useTelBandInt=useTelBandInt, INFO=INFO
            ) * y_gas(
                VariableValues[densityVariable],
                VariableValues[temperatureVariable],
                spatialSpacingArrays_in_cm,
                integratingAxis,
                INFO=INFO,
            )
    if "kinetic" in include:
        velocityVariable = checkExistenceKey(INFO, "VelocityVariableKinSZ", "VelocityY")
        densityVariable = checkExistenceKey(
            INFO, "ElectronDensityKinSZ", "ElectronNumberDensity"
        )
        if not singleCompDic:
            deltaI += get_Prefacs(
                xFreq, "h", useTelBandInt=useTelBandInt, INFO=INFO
            ) * w_gas(
                VariableValues[densityVariable],
                VariableValues[velocityVariable],
                spatialSpacingArrays_in_cm,
                integratingAxis,
                INFO=INFO,
            )
        else:
            DICSingleComps["kinetic"] = get_Prefacs(
                xFreq, "h", useTelBandInt=useTelBandInt, INFO=INFO
            ) * w_gas(
                VariableValues[densityVariable],
                VariableValues[velocityVariable],
                spatialSpacingArrays_in_cm,
                integratingAxis,
                INFO=INFO,
            )
    if "relativistic" in include:
        pressureVariable = checkExistenceKey(
            INFO, "PressureContributionCReLob", "PressureLob"
        )
        if not singleCompDic:
            deltaI += get_Prefacs(
                xFreq, "jmini", useTelBandInt=useTelBandInt, INFO=INFO
            ) * tau_rel(
                VariableValues[pressureVariable],
                spatialSpacingArrays_in_cm,
                integratingAxis,
                INFO=INFO,
            )
        else:
            DICSingleComps["relativistic"] = get_Prefacs(
                xFreq, "jmini", useTelBandInt=useTelBandInt, INFO=INFO
            ) * tau_rel(
                VariableValues[pressureVariable],
                spatialSpacingArrays_in_cm,
                integratingAxis,
                INFO=INFO,
            )
    print(INFO)
    np.shape(deltaI)
    # deltaI *= 2.*(param.BOLTZMANN_cgs*2.725)**3/(param.PLANCKSCONSTANT_cgs*param.SPEEDOFLIGHT_cgs)**2
    if not singleCompDic:
        return deltaI
    else:
        return DICSingleComps


def getEstimates():
    dz_Mpc = 1.0
    ne_cm3 = 1e-2
    T_keV = 10.0
    v_kms = 300
    y_gas = 4e-4 * dz_Mpc * ne_cm3 / 1e-2 * T_keV / 10.0
    w_gas = 1e-6 * dz_Mpc / 5e-2 * ne_cm3 / 1e-2 * v_kms / 300
    dz_Mpc = 2e-1
    tau_re = 1e-4 * dz_Mpc / 2e-1 * ne_cm3 / 1e-2 * T_keV / 10.0
    return y_gas, w_gas, tau_re


def get_ExamplePlots():
    if False:
        xFreq = np.linspace(0.001, 20, 100)
        i = [float(i_x(x)) for x in xFreq]
        g = [float(g(x)) for x in xFreq]
        h = [float(h(x)) for x in xFreq]
        j = [
            float(
                jxLog(
                    x, 1e1, 1e3, 2.0, betaTh=None, Jassumption="PcrPowerLaw", INFO=None
                )
            )
            for x in xFreq
        ]
        VALUES = np.array([xFreq, i, g, h, j]).T
        np.savetxt("ExampleValuesSZ.txt", VALUES, delimiter=",")
        print(" saved")

    for PlotIndexs in [[0], [0, 1], [0, 1, 2, 3]]:
        fig = plt.figure(figsize=np.array([8, 6]))
        ax = plt.axes([0.13, 0.13, 0.84, 0.74])
        ax2 = ax.twiny()
        xFreq, i, g, h, j = data = np.loadtxt("ExampleValuesSZ.txt", delimiter=",").T
        y_gas, w_gas, tau_re = getEstimates()
        linewidth = 2.7
        ax.plot(
            xFreq, [0.0] * np.size(xFreq), color="black", linewidth=linewidth
        )  # ,label='thermal')
        if 0 in PlotIndexs:
            ax.plot(xFreq, g * y_gas / 1e-3, label="thermal", linewidth=linewidth)
        if 1 in PlotIndexs:
            ax.plot(
                xFreq, -h * w_gas * 1e1 / 1e-3, label="kinetic x10", linewidth=linewidth
            )
        if 2 in PlotIndexs:
            ax.plot(
                xFreq,
                (j - i) * tau_re * 1e1 / 1e-3,
                label="relativ. x10",
                linewidth=linewidth,
            )
        if 3 in PlotIndexs:
            ax.plot(
                xFreq, -i * tau_re * 1e1 / 1e-3, label="-i x10", linewidth=linewidth
            )
        fontsize = 22
        ax.legend(fontsize=fontsize)
        ax.set_xlabel(r"$x\ [h\nu/kT_\mathrm{CMB}]$", fontsize=fontsize)
        ax.tick_params(labelsize=fontsize)
        ax.set_xlim([0, 20])
        # ax.set_ylim([-0.003,0.003])
        ax.set_ylim([-3, 3])
        ax.ticklabel_format(style="sci", scilimits=(-3, 4), axis="both")
        # ax.yaxis.set_offset_position('left')
        ax.yaxis.get_offset_text().set(size=fontsize)
        # offset = ax.yaxis.get_major_formatter().get_offset()
        # ax.yaxis.offsetText.set_visible(False)
        # print offset
        ax.set_ylabel(
            r"$\mathrm{spectral}\ \mathrm{distortion}\ \delta i\ \times10^{-3}$",
            fontsize=fontsize,
        )
        # ax.yaxis.set_label_text("original label" )
        new_tick_labels = [200, 400, 600, 800]
        newToOld = lambda x: 6.6260755e-27 * x / (1.380658e-16 * 2.726) * 1.0e9
        new_tick_position = [newToOld(x) for x in new_tick_labels]
        ax2.set_xlim(ax.get_xlim())
        ax2.set_xticks(new_tick_position)
        ax2.set_xticklabels(new_tick_labels)
        ax2.set_xlabel(r"$\nu\ [\mathrm{GHz}]$", fontsize=fontsize)
        ax2.tick_params(labelsize=fontsize)
        fig.savefig(
            "ExampleSZ%s.pdf" % aux.convert_params_to_string(PlotIndexs),
            dpi=400,
            rasterized=True,
        )
    plt.show()


def betaTh(Temp_keV):
    return param.ElECTRONMASS_in_keV / Temp_keV


def simplifiedThermalToRelativisticRatio():
    return


def convertKTokeV(Temp_in_K):
    return Temp_in_K * param.BOLTZMANN_in_eV_per_K / 1e3


if __name__ == "__main__":
    # print jxLog(x,INFO) - i_x(x)
    """
    s = 2.
    p1 = 1.
    p2 = 10.
    alpha = 2.
    #print PcrGeneral(alpha, np.exp(s), p1, p2)
    data=np.loadtxt('Data/EnsslinKaiser2000/fig3.txt', delimiter=',').T
    plt.plot(data[0],data[1])
    S = np.linspace(-6,6,100)
    plt.plot(S[1::2],[PcrPowLaw(alpha, np.exp(s), p1, p2)*np.exp(s) for s in S][1::2],'x')
    plt.plot(S[::2],[PcrGeneral(alpha, np.exp(s), p1, p2)*np.exp(s) for s in S][::2],'x')
    plt.show()
    
    INFO = {}
    print pressureRelE(INFO,calcSelf=True)
    INFO['RelSZp1']= 0.5
    INFO['RelSZp2']= 1e3
    INFO['RelSZalpha']= 2.2
    print pressureRelE(INFO,calcSelf=True)
    x=np.linspace(1e-3,15,200)
    #plt.plot(x,[jxLog(y,usePCRgeneral=True)-i_x(y) for y in x],'o',label='general calc')
    #dataEns = [jxLog(y,INFO=INFO,usePCRgeneral=False)-i_x(y) for y in x]
    dataEns = [jxLog(y,INFO=INFO,usePCRgeneral=False)-i_x(y) for y in x]
    VALUES = np.array([x,dataEns]).T
    np.savetxt('ValuesSZRelP10p5P21e3Alpha2p2.txt', VALUES, delimiter=',')
    plt.plot(x,dataEns,'x', label='solution Ensslin2000')
    
    folder = 'Data/EnsslinKaiser2000/'
    data=np.loadtxt(folder+'fig1upper.txt', delimiter=',').T
    plt.plot(data[0],data[1], label='p1=1')
    plt.plot(data[0],data[1]*np.max(dataEns)/np.max(data[1]), label='p1=1, rescaled')
    data=np.loadtxt('Data/Pfrommer2005/Fig3gTildUCRe.txt', delimiter=',').T
    plt.plot(data[0],data[1], label='p1=1 Pfrommer')
    plt.plot(data[0],data[1]*np.max(dataEns)/np.max(data[1]), label='p1=1 Pfrommer rescaled')
    data=np.loadtxt(folder+'fig1lower.txt', delimiter=',').T
    plt.plot(data[0],data[1], label='p1=3')
    data=np.loadtxt(folder+'minusI.txt', delimiter=',').T
    plt.plot(data[0],data[1],label='minus I')
    plt.legend()
    plt.show()
    """
    print(betaTh(500.0))
    # exit()
    # exit()
    xFreq = 0.528  # 3.822#8.278
    prefac = "jmini"  # jmini
    INFO = {}
    INFO["Telescope"] = "ALMA"
    INFO["DeltaFrequency"] = 0.705
    INFO["RelSZp1"] = 1.0
    INFO["RelSZp2"] = 1e3
    INFO["RelSZalpha"] = 2.0
    INFO["RelTherSZBeta"] = 10.22  # corresponds to 50keV
    INFO["p1"] = 1.0
    INFO["p2"] = 1e3
    INFO["alpha"] = 2.0
    INFO["xFreq"] = xFreq
    INFO["useThSZRelCor"] = True
    # normalized thermal beta paramter beta=m_e c^2/ k T_e
    # INFO['PtpAssumptions'] = 'fEPowerLaw'#'fEThermal'#'PcrPowerLaw'
    pressureToReplaceWithCRe = 1.0
    # INFO['DeltaFrequency'] = deltaX
    # useTelBandInt = False
    # print get_PrefacINTS(xFreq, deltaX, prefac, INFO)
    temps = np.array([1e7, 1.7e8, 4e8])
    print((1 + relCorrSZ(xFreq, temps, INFO)))  #     * g(xFreq)
    temps = np.linspace(0, 5.8e8, 100)  # [1.7e8]
    filename = "../figuresTEST/SZRelSZGFactorX%s.pdf" % (
        aux.ReplaceDotInName(INFO["xFreq"], "%.2f")
    )
    fig = plt.figure()
    ax = plt.axes([0.13, 0.1, 0.85, 0.85])
    for INFO["xFreq"] in [0.1, 0.2, 0.3, 0.4, 0.5]:
        gValue = g(INFO["xFreq"])
        gValueRel = (1 + relCorrSZ(INFO["xFreq"], temps, INFO)) * g(INFO["xFreq"])
        # ax.plot(convertKTokeV(temps),[gValue]*np.size(temps),'-',label=r'$g(x), x=%.f$'%INFO['xFreq'])
        ax.plot(
            convertKTokeV(temps),
            (gValueRel - [gValue] * np.size(temps)) / gValueRel,
            "--",
            label=r"$g(x)_\mathrm{rel}, x=%.f$" % INFO["xFreq"],
        )
        ax.set_xlabel(r"$T\ [\mathrm{keV}]$")
        ax.set_ylabel(r"$g(x)/g(x)_\mathrm{rel}$")
        # ax.set_xscale('log')
        # ax.set_yscale('log')
        print(("saving %s ..." % filename))
    # ax.set_ylim([0.7,1.05])
    ax.legend()
    aux.create_dirs_for_file(filename)
    aux.checkExistence_delete_file(filename)
    fig.savefig(filename, dpi=param.dpi)
    plt.close(fig)

    # exit()
    # for INFO['PtpAssumptions'] in ['fEThermal']:#'fEThermal']:#'fEPowerLaw','PcrPowerLaw'
    # for prefac in ['jmini']:#'g','h', #0.528
    # for INFO['RelTherSZBeta'] in [betaTh(x) for x in [10.22,1.022]]:
    # print 'working on %g' %INFO['RelTherSZBeta']
    # print '---result---'
    # print calcN_relE(pressureToReplaceWithCRe, INFO)
    # for xFreq, INFO['DeltaFrequency'] in zip([8.8], [0.0352]):#2.536, 8.278, 3.822, ##0.669, 0.705, 0.705,
    # print calcN_relE(pressureToReplaceWithCRe, INFO)

    # get_ExamplePlots()
    # print g,h,j,i
    # p1 = 1.0
    # p2 = 1e3
    # alpha = 2.
    # betaTh = None
    # Jassumption = 'PcrPowerLaw'
    # print pressureRelE(p1, p2, alpha, betaTh, Jassumption)
