import FileWorks as Fw
import FigureMove as Fig
import numpy as np
import os.path
import re
import wrapt
import cProfile
import pstats
from functools import wraps
import multiprocessing as mp
import time as TimeModule


def profile(
    output_file=None, sort_by="cumulative", lines_to_print=None, strip_dirs=False
):
    """A time profiler decorator.
    Inspired by and modified the profile decorator of Giampaolo Rodola:
    http://code.activestate.com/recipes/577817-profile-decorator/
    Args:
        output_file: str or None. Default is None
            Path of the output file. If only name of the file is given, it's
            saved in the current directory.
            If it's None, the name of the decorated function is used.
        sort_by: str or SortKey enum or tuple/list of str/SortKey enum
            Sorting criteria for the Stats object.
            For a list of valid string and SortKey refer to:
            https://docs.python.org/3/library/profile.html#pstats.Stats.sort_stats
        lines_to_print: int or None
            Number of lines to print. Default (None) is for all the lines.
            This is useful in reducing the size of the printout, especially
            that sorting by 'cumulative', the time consuming operations
            are printed toward the top of the file.
        strip_dirs: bool
            Whether to remove the leading path info from file names.
            This is also useful in reducing the size of the printout
    Returns:
        Profile of the decorated function
    """

    def inner(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            _output_file = output_file or func.__name__ + ".prof"
            pr = cProfile.Profile()
            pr.enable()
            retval = func(*args, **kwargs)
            pr.disable()
            pr.dump_stats(_output_file)

            with open(_output_file, "w") as f:
                ps = pstats.Stats(pr, stream=f)
                if strip_dirs:
                    ps.strip_dirs()
                if isinstance(sort_by, (tuple, list)):
                    ps.sort_stats(*sort_by)
                else:
                    ps.sort_stats(sort_by)
                ps.print_stats(lines_to_print)
            return retval

        return wrapper

    return inner


def wraps(f):
    def decorator_hide(g):
        def helper(*args, **kwargs):
            return g(*args, **kwargs)

        attributes = (
            "__module__",
            "__name__",
            "__qualname__",
            "__doc__",
            "__annotations__",
        )
        for attr in attributes:
            try:
                value = getattr(f, attr)
            except AttributeError:
                pass
            else:
                setattr(helper, attr, value)
        return helper

    return decorator_hide


def checkRerunLoad(
    checkLoadFromFile=False,
    Ignore=[],
    methodSpecficVariables=False,
    otherMethodsVariables=[],
    verbose=1,
):
    assert type(otherMethodsVariables) != str

    def insicheckRerun(func):
        # @wraps#functools.wraps(func)
        def wrapper(self, *args, **kwargs):

            if methodSpecficVariables:
                self.__dict__ = Fw.mergeDictionariesOverwrite(
                    self.__dict__, locals()["kwargs"]
                )
                self.updateIgnores(func.__name__, Ignore)
                self.addCheckParameters(func.__name__, True, **kwargs)
            for otherMethod in otherMethodsVariables:
                self.updateIgnores(otherMethod, Ignore)
                self.addCheckParameters(otherMethod, True, **kwargs)
            if self.dicCheckParameters[func.__name__]["redo"]:
                self.__dict__ = Fw.mergeDictionariesOverwrite(
                    self.__dict__, locals()["kwargs"]
                )
                if (
                    checkLoadFromFile
                    and self.dicCheckParameters[func.__name__]["loadPlotData"]
                ):
                    filename = self.dicCheckParameters[func.__name__][
                        "fnamePlotDataSavedLoad"
                    ]
                    if filename is None:
                        ignoreList, checkingDict = self.getDicForComparingWFile(
                            func.__name__
                        )
                        _, filename = Fw.checkRedo(
                            pathSaveFiles=[
                                self.dicCheckParameters[func.__name__][
                                    "savePlotDataFolder"
                                ]
                            ]
                            + self.dicCheckParameters[func.__name__][
                                "loadPlotDataCheckFolders"
                            ],
                            dicCheckParameters=checkingDict,
                            fileString="",
                            ignore=ignoreList,
                            verbose=verbose,
                        )
                    if filename is not None:
                        result = Fw.loadFieldsFromFile(
                            filename,
                            self.dicCheckParameters[func.__name__][
                                "saveloadPlotDataKeys"
                            ],
                        )
                        self.__dict__ = Fw.mergeDictionariesOverwrite(
                            self.__dict__, result
                        )
                    else:
                        result = func(self, *args, **kwargs)
                else:
                    result = func(self, *args, **kwargs)
            else:
                if self.verbose:
                    print("no need to redo {} ".format(func.__name__))
                result = None
            self.dicCheckParameters[func.__name__]["redo"] = False
            return result

        return wrapper

    return insicheckRerun


class checkLoadNoMemory(object):
    """
    need to define all variables as kwargs when using function want to wrap!
    """

    def __init__(
        self,
        namesreturnvariables,
        loadPlotData=True,
        savePlotDataFolder=None,
        savePlotDataNameInclude=None,
        loadPlotDataAdditFolders=None,
        Ignore=None,
        checkadditional=None,
        return_variable_type=None,
        dtypes=None,
        filename_add_random_number=True,
        save_folder_id_process=False,
        check_same_filename=True,
        verbose=1,
    ):
        self.loadPlotData = loadPlotData
        self.savePlotDataFolder = savePlotDataFolder
        if self.savePlotDataFolder is None:
            self.savePlotDataFolder = "./"
        if loadPlotData and save_folder_id_process:
            self.savePlotDataFolder = os.path.join(
                self.savePlotDataFolder, str(id(mp.current_process()))
            )
        print(f"self.savePlotDataFolder {self.savePlotDataFolder}")
        self.savePlotDataNameInclude = savePlotDataNameInclude
        self.Ignore = Ignore
        if self.Ignore is None:
            self.Ignore = []
        self.loadPlotDataAdditFolders = loadPlotDataAdditFolders
        if self.loadPlotDataAdditFolders is None:
            self.loadPlotDataAdditFolders = []
        self.save_folder_id_process = save_folder_id_process
        self.namesFuncReturns = namesreturnvariables
        self.checkadditional = checkadditional
        if self.checkadditional is None:
            self.checkadditional = []
        self.return_variable_type = return_variable_type
        self.dtypes = dtypes
        self.filename_add_random_number = filename_add_random_number
        self.check_same_filename = check_same_filename
        self.verbose = verbose
        # ??check for folder of snapshot!!

    @wrapt.decorator
    def __call__(self, wrapped, instance, args, kwargs):
        if not self.loadPlotData:  # TODO: check problem if do here?
            result = wrapped(*args, **kwargs)
            return result
        dic_check = Fw.mergeDictionaries(
            Fw.get_default_args(wrapped), kwargs, allowOverwrittenKeywords=True
        )
        dic_check = Fw.mergeDictionaries(
            self._convert_nontrivial_args_to_dictionary(
                instance, self.checkadditional, dic_check
            ),
            dic_check,
        )
        if self.check_same_filename:
            if self.savePlotDataNameInclude is None:
                filename_dic = dic_check.copy()
            else:
                filename_dic = self._convert_nontrivial_args_to_dictionary(
                    instance, self.savePlotDataNameInclude, dic_check.copy()
                )
            file_string = Fw.getNameFile(
                fType="", prefix=f"{wrapped.__name__}", **filename_dic
            )

        save_attributes = Fw.reduceDictByKeysNotInDic(dic_check.copy(), self.Ignore)
        save_attributes = Fw.convertSubDicsToKeywords(
            save_attributes, addAdditional="DICT__", inplace=True
        )
        _, filename = Fw.checkRedo(
            pathSaveFiles=[self.savePlotDataFolder] + self.loadPlotDataAdditFolders,
            dicCheckParameters=save_attributes,
            fileString=file_string,
            ignore=self.Ignore,
            verbose=self.verbose,
        )
        # exit()
        if not self.loadPlotData:
            result = wrapped(*args, **kwargs)
        elif filename is not None:
            result = Fw.loadFieldsFromFile(filename, self.namesFuncReturns)
            result = Fw.convertKeywordsToSubDics(
                result, addAdditional="DICT__", inplace=False
            )
            if self.return_variable_type == dict:
                pass
            elif len(self.namesFuncReturns) == 1 and type(self.namesFuncReturns) != str:
                result = tuple(result.values())[0]
            else:
                result = tuple(result.values())
        else:
            if self.verbose:
                print("--> need to redo plot!")
            result = wrapped(*args, **kwargs)
            result = self.check_number_return_values(result)
            if self.savePlotDataNameInclude is None:
                filename_dic = dic_check.copy()
            else:
                filename_dic = self._convert_nontrivial_args_to_dictionary(
                    instance, self.savePlotDataNameInclude, dic_check.copy()
                )
            filename = self._get_filename(wrapped, filename_dic)
            if self.return_variable_type == dict:
                results_dic = result
            elif len(self.namesFuncReturns) == 1 and type(self.namesFuncReturns) != str:
                results_dic = {self.namesFuncReturns[0]: result}
            else:
                results_dic = {k: v for k, v in zip(self.namesFuncReturns, result)}
            for k, v in results_dic.items():
                if k in self.Ignore:
                    results_dic[k] = None
            save_attributes = Fw.reduceDictByKeysNotInDic(dic_check.copy(), self.Ignore)
            save_attributes, results_dic = Fw.decide_attribute_field_hdf5(
                save_attributes, results_dic
            )
            save_attributes = Fw.convertSubDicsToKeywords(
                save_attributes, addAdditional="DICT__", inplace=True
            )
            Fw.saveFieldsToFile(
                filename,
                results_dic,
                save_attributes,
                dtypes=self.dtypes,
                v=self.verbose,
            )
        return result

    def check_number_return_values(self, result):
        if self.namesFuncReturns == "all":
            return result
        if len(self.namesFuncReturns) == 1 and type(self.namesFuncReturns) != str:
            pass
        else:
            assert len(result) == len(
                self.namesFuncReturns
            ), f"need name for everything returned by function {len(result)} != {len(self.namesFuncReturns)}"
        return result

    def _get_filename(self, wrapped, filename_dic):
        filename = Fw.getNameFile(
            fType="", prefix=f"{wrapped.__name__}", **filename_dic
        )
        fileNumber = (
            Fw.getFileMaxNum(
                Fig.findAllFilesWithEnding(self.savePlotDataFolder, ".hdf5")
            )
            + 1
        )
        add = ""
        if self.filename_add_random_number:
            add = "_%i" % (np.random.randint(1e6))
        filename = "%s%s_%i.hdf5" % (filename, add, fileNumber,)
        filename = os.path.join(self.savePlotDataFolder, filename)
        if self.verbose:
            print(f"filename hdf5: {filename}")
        return filename

    def _convert_nontrivial_args_to_dictionary(self, instance, args, dic):
        """

        Parameters
        ----------
        instance: needed for "." style args
        args: add these keywords
        dic: if not "."-style-keywords add from dic

        Returns
        -------

        """
        new_dic = {}
        for a in args:
            if "." in a:
                comps = re.split(r"\.", a)
                # print(instance.__dict__["snap"].__dict__["rotation_angle"])
                # if len(comps) > 2:
                #     raise Exception(
                #         f"{a}: more than one dot not supported in args: {args}"
                #     )
                if a in dic.keys():
                    if dic[a] != self._get_nontricial_arg_value(comps, dic, instance):
                        raise Exception(
                            f"shouldn't overwrite existing key {a} in dic with different value! "
                            f"{dic[a]}!={self._get_nontricial_arg_value(comps, dic, instance)}"
                        )
                new_dic[a] = self._get_nontricial_arg_value(comps, dic, instance)
            else:
                comps = a
                new_dic[a] = self._get_nontricial_arg_value(comps, dic, instance)
        return new_dic

    def _get_nontricial_arg_value(self, comps, dic, instance):
        concat_string_calling_property = f"_{instance.__class__.__name__}__{comps[1]}"
        if type(comps) == str:
            if comps in dic:
                value = dic[comps]
            elif comps in instance.__dict__:
                value = instance.__dict__[comps]
        else:
            if comps[0] == "self" and comps[1] in instance.__dict__:
                if len(comps) == 2:
                    value = instance.__dict__[comps[1]]
                elif len(comps) == 3:
                    value = instance.__dict__[comps[1]].__dict__[comps[2]]
                else:
                    raise Exception(
                        f"looking in class object with more than 2 subobjects, case not implemented yet!: {comps}"
                    )
            elif comps[0] in instance.__dict__:
                value = instance.__dict__[comps[0]].__dict__[comps[1]]
            elif concat_string_calling_property in instance.__dict__:
                value = instance.__dict__[concat_string_calling_property]
            elif comps[0] in dic:
                if hasattr(dic[comps[0]], "__dict__"):
                    if comps[1] in dic[comps[0]].__dict__:
                        value = dic[comps[0]].__dict__[comps[1]]
                else:
                    value = dic[comps[0]]
        return value


# def checkRerunLoadNoMemoryOld(
#         wrapped=None,
#         loadPlotData=False,
#         fnamePlotDataSavedLoad="",
#         savePlotDataFolder="./",
#         Ignore=[],
# ):
#     # def insicheckRerun(func):
#     # @wraps(func)#functools.wraps(func)
#     @wrapt.decorator
#     def wrapper(wrapped, instance, args, kwargs):
#         dic_check = {}
#         dic_check["args"] = locals()["args"]
#         dic_check = FW.mergeDictionaries(locals()["kwargs"], dic_check)
#         print(dic_check)
#         print(inspect.signature(wrapped))
#         for otherMethod in otherMethodsVariables:
#             self.updateIgnores(otherMethod, Ignore)
#             self.addCheckParameters(otherMethod, True, **kwargs)
#         if self.dicCheckParameters[wrapped.__name__]["redo"]:
#             self.__dict__ = FW.mergeDictionariesOverwrite(
#                 self.__dict__, locals()["kwargs"]
#             )
#             if checkLoadFromFile and loadPlotData:
#                 filename = fnamePlotDataSavedLoad
#                 if filename is None:
#                     ignoreList, checkingDict = self.getDicForComparingWFile(
#                         wrapped.__name__
#                     )
#                     _, filename = FW.checkRedo(
#                         pathSaveFiles=os.path.join(
#                             savePlotDataFolder + loadPlotDataCheckFolders
#                         ),
#                         dicCheckParameters=checkingDict,
#                         fileString="",
#                         ignore=ignoreList,
#                     )
#                 if filename is not None:
#                     result = FW.loadFieldsFromFile(
#                         filename,
#                         self.dicCheckParameters[wrapped.__name__][
#                             "saveloadPlotDataKeys"
#                         ],
#                     )
#                     self.__dict__ = FW.mergeDictionariesOverwrite(self.__dict__, result)
#                 else:
#                     result = wrapped(*args, **kwargs)
#             else:
#                 result = wrapped(*args, **kwargs)
#         else:
#             print("no need to redo {} ".format(func.__name__))
#             result = None
#         self.dicCheckParameters[func.__name__]["redo"] = False
#         return result
#         # return wrapper
#
#     return wrapper(result)


class SAVER:
    def __init__(self):
        self.dicCheckParameters = {}
        self.oldDefinedParameters = {}

    def getDicForComparingWFile(self, MethodName):
        checkingDict = self.getDicForComparing(
            self.dicCheckParameters[MethodName], MethodName
        )
        checkingDict = Fw.convertSubDicsToKeywords(
            checkingDict, addAdditional="DICT__", inplace=False
        )
        checkingDict = Fw.reduceDictByKeysNotInDic(
            checkingDict, self.dicCheckParameters[MethodName]["ignore"]
        )
        # print(checkingDict)
        return self.dicCheckParameters[MethodName]["ignore"], checkingDict

    def getDicForComparing(self, dic, MethodName, removeNone=True):
        # ignore all None values in parameters
        checkingDict = {k: v for k, v in dic.items() if v is not None}
        return Fw.reduceDictByKeysNotInDic(
            checkingDict, self.dicCheckParameters[MethodName]["ignore"]
        )

    def updateIgnores(self, MethodName, Ignore):
        # update ignore list not to consider when comparing parameters for saving/loading
        self.dicCheckParameters[MethodName]["ignore"] = (
            self.dicCheckParameters[MethodName]["ignore"] + Ignore
        )
        self.dicCheckParameters[MethodName]["ignore"] = np.unique(
            self.dicCheckParameters[MethodName]["ignore"]
        ).tolist()

    def updateParas(self, **kwrgs):
        dic = Fw.kwrgsToDic(**kwrgs)
        for methodName in self.dicCheckParameters.keys():
            methodDic = Fw.reduceDictByKeys(
                dic, self.dicCheckParameters[methodName].keys()
            )
            self.addCheckParameters(methodName, True, **methodDic)
            # overwrite class attributes that changed
            self.__dict__ = Fw.mergeDictionariesOverwrite(self.__dict__, methodDic)
            dicParas = Fw.reduceDictBySubStr(dic, "%s_" % methodName)
            self.addCheckParameters(methodName, True, **dicParas)
            dicParas = Fw.reduceDictBySubStr(
                dic, "%s_" % methodName, removePhrase=False
            )
            self.__dict__ = Fw.mergeDictionariesOverwrite(self.__dict__, dicParas)
        return

    def addCheckParameters(self, METHODNAMES, conditions, **conditionSaves):
        # initialize if not already present
        if type(METHODNAMES) == str:
            METHODNAMES = [METHODNAMES]
        for MethodName in METHODNAMES:
            if MethodName not in self.dicCheckParameters.keys():
                self.initializeDicCheckParameters(MethodName)

            DicInitially = self.dicCheckParameters[MethodName].copy()
            self.addCheckConditParameters(
                MethodName, conditions, overwriteParameters=True, **conditionSaves
            )
            # if anything changed need to redo feature next time we check for it!
            if self.getDicForComparing(
                DicInitially, MethodName
            ) != self.getDicForComparing(
                self.dicCheckParameters[MethodName], MethodName
            ):
                self.dicCheckParameters[MethodName]["redo"] = True
        return self.dicCheckParameters

    def initializeDicCheckParameters(self, MethodName):
        self.dicCheckParameters[MethodName] = {}
        self.dicCheckParameters[MethodName]["ignore"] = ["ignore", "redo"]
        self.dicCheckParameters[MethodName]["fnamePlotSavedLoad"] = None
        self.dicCheckParameters[MethodName]["redo"] = True
        return self.dicCheckParameters[MethodName]

    def resetRedoCheckParameters(self, MethodName):
        self.dicCheckParameters[MethodName]["redo"] = False
        return self.dicCheckParameters

    def checkRedoNecessary(self, MethodName):
        return self.dicCheckParameters[MethodName]["redo"]

    def addCheckAlwaysParameters(self, MethodName, dicParas, overwriteParameters=True):
        # dictionary in dic will be overwritten accordingly
        self.dicCheckParameters[MethodName] = Fw.mergeDictionaries(
            self.dicCheckParameters[MethodName],
            dicParas,
            allowOverwrittenKeywords=overwriteParameters,
        )
        return self.dicCheckParameters

    def addCheckConditParameters(
        self, MethodName, conditions, overwriteParameters=True, **conditionSaves
    ):
        # conditions can be True
        # use as [variable is not None]*3+[bool(other)]*2, variable=variable, variableTyp=variableTyp, variableCol=variableCol, other=other, otherTyp=otherTyp
        dicParasToChek = locals()["conditionSaves"]
        if type(conditions) == bool:
            if conditions:
                self.addCheckAlwaysParameters(
                    MethodName,
                    dicParas=dicParasToChek,
                    overwriteParameters=overwriteParameters,
                )
                return self.dicCheckParameters
            else:
                print("addCheckConditParameters: no parameters changed/added!")
                return self.dicCheckParameters

        conditions = np.array(conditions)
        DIC = {}
        for cond, keys in zip(conditions, dicParasToChek.keys()):
            if cond:
                DIC[keys] = dicParasToChek[keys]
        self.dicCheckParameters[MethodName] = Fw.mergeDictionaries(
            self.dicCheckParameters[MethodName],
            DIC,
            allowOverwrittenKeywords=overwriteParameters,
        )
        return self.dicCheckParameters

    def resetCheckDictionary(self):
        self.dicCheckParameters = {}

    def loadPlot(self, fnamePlotSavedLoad=None, **dicWhatFieldToLoad):
        # dicWhatFieldToLoad in format: x=True, y=True, err=(self.errbar != None), scatterDataX=self.scatterData, scatterDataY=self.scatterData
        if fnamePlotSavedLoad is None:
            fnamePlotSavedLoad = self.fnamePlotSavedLoad
        self.dicLoadSaveFields = Fw.createDictionary(**dicWhatFieldToLoad)
        return Fw.loadFieldsFromFile(fnamePlotSavedLoad, self.dicLoadSaveFields)
