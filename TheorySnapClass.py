import auxiliary_functions as aux
import FileWorks as FW
import numpy as np
import Param as param
import gadget
import os
# import importlib.util
# spec = importlib.util.spec_from_file_location("gadget_snapshot", "/home/kristian/analysis/arepo-snap-util/gadget_snap.py")
# foo = importlib.util.module_from_spec(spec)
# spec.loader.exec_module(foo)
import h5py
from gadget_format3 import format3
import shutil
import debug


class TheorySnapshot(format3):
    def __init__(
            self,
            numTypes=6,
            BoxSize=1.0,
            hubble_parameter=1.0,
            FloatType=np.float64,
            IntType=np.int32,
            UnitLength_in_cm=1.,
            UnitMass_in_g=1.,
            UnitVelocity_in_cm_per_s=1.,
    ):
        super(TheorySnapshot, self).__init__(
            numTypes=numTypes,
            BoxSize=BoxSize,
            hubble_parameter=hubble_parameter,
            FloatType=FloatType,
            IntType=IntType,
            UnitLength_in_cm=UnitLength_in_cm,
            UnitMass_in_g=UnitMass_in_g,
            UnitVelocity_in_cm_per_s=UnitVelocity_in_cm_per_s,
        )


def convert_spherical_to_cartesian_coordinates(r, phi, theta, x=None):
    if x is not None:
        x = x
    else:
        x = r * np.cos(phi) * np.sin(theta)
    y = r * np.sin(phi) * np.sin(theta)
    z = r * np.cos(theta)
    return x, y, z


def convert_cartesian_to_spherical_coordinates(x, y, z):
    xy = (x)**2+(y)**2
    r = np.sqrt(xy+(z)**2)
    theta = np.arctan2(np.sqrt(xy), (z))
    phi = np.arctan2((y), (x))
    return r, phi, theta


def random_centered(shape, centering=-0.5):
    return rng.random(shape) + centering


def convert_spherical_velocities_to_cartesian(vr, vphi, vtheta, x, y, z):
    # https://en.wikipedia.org/wiki/Spherical_coordinate_system#Kinematics
    r, phi, theta = convert_cartesian_to_spherical_coordinates(x, y, z)
    # vx = vr*np.sin(theta)*np.cos(phi) + r*vtheta*np.cos(theta)*np.cos(phi) + r*vphi*np.sin(theta)*(-np.sin(phi))
    vx = vr*np.sin(theta)*np.cos(phi) + vtheta*np.cos(theta)*np.cos(phi) + vphi*(-np.sin(phi))
    # vy = vr*np.sin(theta)*np.sin(phi) + r*vtheta*np.cos(theta)*np.sin(phi) + r*vphi*np.sin(theta)*np.cos(phi)
    vy = vr*np.sin(theta)*np.sin(phi) + vtheta*np.cos(theta)*np.sin(phi) + vphi*np.cos(phi)
    # vz = vr*np.cos(theta)             + r*vtheta*(-np.sin(theta))          + 0
    vz = vr*np.cos(theta)             + vtheta*(-np.sin(theta))          + 0
    return vx, vy, vz


if __name__ == "__main__":
    style = "radial"
    new_ics = False
    n_samples = 4096
    box_size = 3977.8838577357856
    center = box_size / 2.
    UnitLength_in_cm = 3.085678e+21
    UnitMass_in_g = 1.989e+43
    UnitVelocity_in_cm_per_s = 100000.0
    rng = np.random.default_rng()
    if new_ics:
        folder = "../figures/histogram_plots/theory/"
        name = f"{style}SN{n_samples}"
        snap = TheorySnapshot(
            numTypes=6,
            BoxSize=box_size,
            hubble_parameter=0.67,
            FloatType=np.float64,
            IntType=np.int32,
            UnitLength_in_cm=UnitLength_in_cm,
            UnitMass_in_g=UnitMass_in_g,
            UnitVelocity_in_cm_per_s=UnitVelocity_in_cm_per_s,
        )
        filename = os.path.join(folder, name)
        aux.create_dirs_for_file(filename)
    else:
        folder = "../figures/histogram_plots/theory_repaint/"
        name = f"{style}"
        filename = os.path.join(folder, name)
        aux.create_dirs_for_file(filename)
        # shutil.copyfile("/home/kristian/analysis/cluster_ics/testR7/R7_test.hdf5", filename + ".hdf5")
        shutil.copyfile("/home/kristian/Desktop/snap_000.hdf5", filename + ".hdf5")
        f = h5py.File(filename + ".hdf5", "r+")
        print(f"loading {filename + '.hdf5'}")
        dist = np.sqrt(np.sum((f["PartType0/Coordinates"][:]-center)**2, axis=1))
        # f["PartType0"].create_dataset(
        #     "StarFormationRate", data=np.zeros_like(f["PartType0/Masses"][:])
        # )
        # header = f["Header/"]
        # header.attrs["UnitLength_in_cm"] = UnitLength_in_cm
        # header.attrs["UnitMass_in_g"] = UnitMass_in_g
        # header.attrs["UnitVelocity_in_cm_per_s"] = UnitVelocity_in_cm_per_s
        sfr = f["PartType0/StarFormationRate"]
        x, y, z = f["PartType0/Coordinates"][:, 0] - center, f["PartType0/Coordinates"][:, 1] - center, f["PartType0/Coordinates"][:, 2] - center
        r, phi, theta = convert_cartesian_to_spherical_coordinates(x, y, z)
        # phi angle between axis 1 and 0 values between -pi...pi
        # theta angle between axis 1,0 and 2 values between 0...pi
        # thin disk
        mask = np.logical_and.reduce((r < 80, r > 4, np.abs(z) < 4))
        mask = np.logical_and.reduce((r < 80, r > 4))
        phi_delta = theta_delta = np.pi/40.
        # theta_delta = np.pi/40.
        # ray setup:
        mask = np.zeros_like(r)
        for phi_val in [-0.7, 2.2]:#rng.random(2)*np.pi-np.pi:
            theta_val = abs(phi_val)
            # mask = np.logical_or(np.logical_and.reduce((r < 80, r > 4, phi < phi_val + phi_delta, phi > phi_val - phi_delta)), mask)
            mask = np.logical_or(np.logical_and.reduce((r < 80, r > 4, phi < phi_val + phi_delta, phi > phi_val - phi_delta, theta < theta_val + theta_delta, theta > theta_val - theta_delta)), mask)
        # mask = np.logical_and(r < 50, theta < np.pi/4.)
        n_samples = np.sum(mask)
        debug.printDebug(theta[mask], "theta[mask]")
        debug.printDebug(phi[mask], "phi[mask]")
        print(f"np.sum(mask): {np.sum(mask)}")
        Velocities = f["PartType0/Velocities"]
    if style == "disk":
        x, y, z = convert_spherical_to_cartesian_coordinates(r, phi, theta)
        pos = np.vstack((center + x, center + y, center + z)).T
        vr = 0 * np.ones_like(x)
        vphi = 1 * np.ones_like(x)
        vtheta = 0 * np.ones_like(x)
        vx, vy, vz = convert_spherical_velocities_to_cartesian(vr, vphi, vtheta, x, y, z)
        vx, vy, vz = vx[mask], vy[mask], vz[mask]
        vel = np.vstack((vx, vy, vz)).T
    elif style == "random":
        x, y, z = convert_spherical_to_cartesian_coordinates(r, phi, theta)
        pos = np.vstack((center + x, center + y, center + z)).T
        vr = 0 * np.ones_like(x)
        vr = random_centered(np.shape(x))
        # vphi = 0 * np.ones_like(x)
        vphi = random_centered(np.shape(x))
        # vtheta = 0 * np.ones_like(x)
        vtheta = random_centered(np.shape(x))
        vx, vy, vz = convert_spherical_velocities_to_cartesian(vr, vphi, vtheta, x, y, z)
        vx, vy, vz = vx[mask], vy[mask], vz[mask]
        vel = np.vstack((vx, vy, vz)).T
    elif style == "radial":
        x, y, z = convert_spherical_to_cartesian_coordinates(r, phi, theta)
        pos = np.vstack((center + x, center + y, center + z)).T
        vr = 0 * np.ones_like(x)
        # vr = random_centered(np.shape(x))
        vr = rng.random(np.shape(x))
        vphi = random_centered(np.shape(x))  * 1e-4
        vtheta = np.ones_like(vr) * 1e-4
        vx, vy, vz = convert_spherical_velocities_to_cartesian(vr, vphi, vtheta, x, y, z)
        vx, vy, vz = vx[mask], vy[mask], vz[mask]
        vel = np.vstack((vx, vy, vz)).T
    if new_ics:
        mass = np.ones_like(x) * 3.855538e-06
        utherm = np.ones_like(x) * 1643445.6
        snap.set_gas(pos, vel, mass, utherm, bfld=None, start_id=1)
        snap.addStarFormationRate(np.ones_like(mass))
        snap.addVolume(np.ones_like(mass))
        snap.write_files(filename + ".hdf5")
    else:
        SFR = np.zeros_like(sfr[:])
        SFR[mask] = 1
        print(f"(np.shape(sfr[:])[0],3): {(np.shape(sfr[:])[0],3)}")
        print(f"mask.shape: {mask.shape}")
        print(f"mask: {mask}")
        VELOCITIES = np.zeros((np.shape(sfr[:])[0], 3))
        print(f"vel: {vel}")
        VELOCITIES[mask] = vel[:]
        sfr[:] = SFR[:]
        Velocities[:, :] = VELOCITIES[:, :]
        print(f"writing: {filename + '.hdf5'}")
        f.close()

class TheorySnapShotFourier:
    def __init__(
        self, n=32, boxsize=1.0, spaceDim=3, hubbleparam=0.67, positionAddNoise=None
    ):
        self.data = {}

        def x1(n, delta):
            return (-(n) / 2 + 1) * delta

        self.spaceDim = spaceDim
        self.boxsize = (
            boxsize  # aux.makeIterable(boxsize, self.spaceDim, enforceType=float)
        )
        self.center = aux.makeIterable(
            boxsize / 2, self.spaceDim, enforceType=float
        )  # [x/2. for x in self.boxsize]
        self.n = aux.makeIterable(n, self.spaceDim, enforceType=int)
        self.delta = [self.boxsize / (y - 1) for y in self.n]
        # self.data['pos'] = np.array(np.meshgrid(*(np.arange(x1(self.n[i] ,self.delta[i]), x1(self.n[i] , self.delta[i])+(self.n[i])*self.delta[i], self.delta[i])+((self.n[i])/2 - 1)*self.delta[i] for i in range(self.spaceDim)))).reshape(3,-1).swapaxes(0,1)
        self.data["pos"] = (
            np.array(
                np.meshgrid(
                    *(
                        np.linspace(0, self.boxsize, self.n[i])
                        for i in range(self.spaceDim)
                    )
                )
            )
            .reshape(3, -1)
            .swapaxes(0, 1)
        )
        if positionAddNoise is not None:
            self.data["pos"] += (
                np.random.random(np.shape(self.data["pos"])) - 0.5
            ) * positionAddNoise
        self.positionAddNoise = positionAddNoise
        # self.pos = self.data['pos']
        # print(self.data['pos'].swapaxes(1,0).reshape(3,n,n,n))
        self.data["type"] = np.zeros(n ** self.spaceDim)
        print(
            f"shapes type: {np.shape(self.data['type'])} shapes pos: {np.shape(self.data['pos'])} n**3: {n**3}"
        )
        self.hubbleparam = hubbleparam
        self.nparticlesall = np.array([np.prod(n), 0, 0, 0, 0, 0])
        self.npart = np.sum(self.nparticlesall)
        # self.da = foo.gadget_snapshot.mapOnCartGrid(self, 'type')
        # self.updateDataDic()
        self.UnitMass_in_g = param.UnitMass_in_g
        self.UnitVelocity_in_cm_per_s = param.UnitVelocity_in_cm_per_s
        self.UnitLength_in_cm = param.UnitLength_in_cm

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, data):
        for k, v in data.items():
            self.__dict__[k] = v
        self.__data = data

    # def updateDataDic(self):
    # 	print('update')
    # 	self.__dict__ = FW.mergeDictionariesOverwrite(self.__dict__, self.data)
    # 	print(self.__dict__)


