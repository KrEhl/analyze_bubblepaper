import ast


def fancy_eval(obj):
    if type(obj) == int or type(obj) == float:
        return obj
    if len(obj) > 0:
        if obj[0] in ["m", "M", "n", "N"]:
            try:
                return ast.literal_eval(obj[1:]) * -1
            except:
                pass
    try:
        obj = ast.literal_eval(obj)
        if type(obj) == list:
            obj = [fancy_eval(str(v)) for v in obj]
            if type(obj[0]) == list:
                obj = [[fancy_eval(str(v)) for v in x] for x in obj]
                if type(obj[0][0]) == list:
                    obj = [[[fancy_eval(str(v)) for v in x] for x in y] for y in obj]
        return obj
    except:
        return obj
