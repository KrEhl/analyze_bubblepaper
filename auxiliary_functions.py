import matplotlib as mpl
#will print values of variables in traceback independent of file causing exception!
# pip install traceback-with-variables

# from traceback_with_variables import activate_by_import
# from debug import debug.printDebug
import debug
mpl.use("Agg")
## ##mpl.use('Agg')
## ##mpl.use(' Agg')
# import gadget
import os
from contextlib import contextmanager

# import os
# import matplotlib as mpl
# font = {'family' : 'serif',
#        'serif': ['Computer Modern']}#,
#        'size'   : fontsize}
# mpl.rc('text', usetex=True)
# mpl.rcParams['text.latex.unicode'] = True
# mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
# mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath,bm}\newcommand{\sbf}[1]{\textsf{\textbf{#1}}}"]
# mpl.rc('font', **font)
# from aifc import data  ## what is this?
import collections
import difflib
import matplotlib.pyplot as plt
import inspect

try:
    import gadget
except:
    print("couldnt import gadget!")
import numpy as np
import time
import scipy.ndimage as ndimage
from scipy.special import beta
from scipy.special import betainc
from scipy.special import gamma
import math
import scipy.stats
import copy
import Param as param
import re
import ast
import itertools
from itertools import chain
import warnings
import string_manipulation
import Multithread as mt
from functools import wraps
from statsmodels.stats.weightstats import DescrStatsW


def timing(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time.time()
        result = f(*args, **kw)
        te = time.time()
        print("func:%r args:[%r, %r] took: %2.4f sec" % (f.__name__, args, kw, te - ts))
        return result

    return wrap


class PlotProperties:
    def __init__(self, times):
        self.times


def create_dir_for_file(filename):
    # checks if directory exists, if not creates directory
    dir = os.path.dirname(filename)
    try:
        os.stat(dir)
    except:
        os.mkdir(dir)


def create_dirs_for_file(filename, tryexcept=False):
    # checks if directory exists, if not creates directory
    if tryexcept:
        try:
            dir = os.path.dirname(filename)
            if os.path.isdir(dir) or len(dir) == 0:
                pass
            else:
                os.makedirs(dir)
        except:
            print("couldnt create folder: %s" % filename)
    else:
        dir = os.path.dirname(filename)
        if os.path.isdir(dir) or len(dir) == 0:
            pass
        else:
            os.makedirs(dir)


def get_AbsoluteMagneticField2(
    snap, considering_h=False, no_conversion=False, highprecision=False
):
    # units in gauss
    if highprecision:
        mag = np.sqrt(
            snap.data["bfld"][:, 0].astype(np.float128).copy() ** 2
            + snap.data["bfld"][:, 1].astype(np.float128).copy() ** 2
            + snap.data["bfld"][:, 2].astype(np.float128).copy() ** 2
        )
    else:
        mag = np.sqrt(
            snap.data["bfld"][:, 0].copy() ** 2
            + snap.data["bfld"][:, 1].copy() ** 2
            + snap.data["bfld"][:, 2].copy() ** 2
        )
    if no_conversion:
        mag *= 1.0
    elif considering_h:
        try:
            mag *= (
                (snap.UnitLength_in_cm / snap.hubbleparam) ** (-0.5)
                * (snap.UnitMass_in_g / snap.hubbleparam) ** (0.5)
                * snap.UnitVelocity_in_cm_per_s
                / (snap.UnitLength_in_cm / snap.hubbleparam)
            )
        except:
            import Param

            mag *= (
                (Param.UnitLength_in_cm / Param.HubbleParam) ** (-0.5)
                * (Param.UnitMass_in_g / Param.HubbleParam) ** 0.5
                * Param.UnitVelocity_in_cm_per_s
                / (Param.UnitLength_in_cm / Param.HubbleParam)
            )
    else:
        mag *= (
            snap.UnitLength_in_cm ** (-0.5)
            * snap.UnitMass_in_g ** (0.5)
            * snap.UnitVelocity_in_cm_per_s
            / snap.UnitLength_in_cm
        )
    return mag ** 2


def get_AbsoluteMagneticField(
    snap, considering_h=False, no_conversion=False, highprecision=False
):
    # units in gauss
    if highprecision:
        mag = np.sqrt(
            snap.data["bfld"][:, 0].astype(np.float128).copy() ** 2
            + snap.data["bfld"][:, 1].astype(np.float128).copy() ** 2
            + snap.data["bfld"][:, 2].astype(np.float128).copy() ** 2
        )
    else:
        mag = np.sqrt(
            snap.data["bfld"][:, 0].copy() ** 2
            + snap.data["bfld"][:, 1].copy() ** 2
            + snap.data["bfld"][:, 2].copy() ** 2
        )
    if no_conversion:
        mag *= 1.0
    elif considering_h:
        try:
            mag *= (
                (snap.UnitLength_in_cm / snap.hubbleparam) ** (-0.5)
                * (snap.UnitMass_in_g / snap.hubbleparam) ** (0.5)
                * snap.UnitVelocity_in_cm_per_s
                / (snap.UnitLength_in_cm / snap.hubbleparam)
            )
        except:
            import Param

            mag *= (
                (Param.UnitLength_in_cm / Param.HubbleParam) ** (-0.5)
                * (Param.UnitMass_in_g / Param.HubbleParam) ** 0.5
                * Param.UnitVelocity_in_cm_per_s
                / (Param.UnitLength_in_cm / Param.HubbleParam)
            )
    else:
        mag *= (
            snap.UnitLength_in_cm ** (-0.5)
            * snap.UnitMass_in_g ** (0.5)
            * snap.UnitVelocity_in_cm_per_s
            / snap.UnitLength_in_cm
        )
    return mag


def get_AbsoluteMagneticFieldinmuG(
    snap, considering_h=False, no_conversion=False, highprecision=False
):
    # units in gauss
    if highprecision:
        mag = np.sqrt(
            snap.data["bfld"][:, 0].astype(np.float128).copy() ** 2
            + snap.data["bfld"][:, 1].astype(np.float128).copy() ** 2
            + snap.data["bfld"][:, 2].astype(np.float128).copy() ** 2
        )
    else:
        mag = np.sqrt(
            snap.data["bfld"][:, 0].copy() ** 2
            + snap.data["bfld"][:, 1].copy() ** 2
            + snap.data["bfld"][:, 2].copy() ** 2
        )
    if no_conversion:
        mag *= 1.0
    elif considering_h:
        mag *= (
            (snap.UnitLength_in_cm / Param.HubbleParam) ** (-0.5)
            * (snap.UnitMass_in_g / snap.hubbleparam) ** (0.5)
            * snap.UnitVelocity_in_cm_per_s
            / (snap.UnitLength_in_cm / snap.hubbleparam)
            / 1.0e-6
        )
    else:
        mag *= (
            snap.UnitLength_in_cm ** (-0.5)
            * snap.UnitMass_in_g ** (0.5)
            * snap.UnitVelocity_in_cm_per_s
            / snap.UnitLength_in_cm
            / 1.0e-6
        )
    return mag


def get_MagneticFieldX(snap, considering_h=False, convert_units=True):
    mag = snap.data["bfld"][:, 0].copy()
    if not convert_units:
        pass
    elif considering_h:
        try:
            mag *= (
                (snap.UnitLength_in_cm / snap.hubbleparam) ** (-0.5)
                * (snap.UnitMass_in_g / snap.hubbleparam) ** 0.5
                * snap.UnitVelocity_in_cm_per_s
                / (snap.UnitLength_in_cm / snap.hubbleparam)
            )
        except:
            import Param

            mag *= (
                (Param.UnitLength_in_cm / Param.HubbleParam) ** (-0.5)
                * (Param.UnitMass_in_g / Param.HubbleParam) ** 0.5
                * Param.UnitVelocity_in_cm_per_s
                / (Param.UnitLength_in_cm / Param.HubbleParam)
            )
    else:
        mag *= (
            snap.UnitLength_in_cm ** (-0.5)
            * snap.UnitMass_in_g ** (0.5)
            * snap.UnitVelocity_in_cm_per_s
            / snap.UnitLength_in_cm
        )
    return mag


def get_MagneticFieldY(snap, considering_h=False, convert_units=True):
    mag = snap.data["bfld"][:, 1].copy()
    if not convert_units:
        pass
    elif considering_h:
        try:
            mag *= (
                (snap.UnitLength_in_cm / snap.hubbleparam) ** (-0.5)
                * (snap.UnitMass_in_g / snap.hubbleparam) ** 0.5
                * snap.UnitVelocity_in_cm_per_s
                / (snap.UnitLength_in_cm / snap.hubbleparam)
            )
        except:
            import Param

            mag *= (
                (Param.UnitLength_in_cm / Param.HubbleParam) ** (-0.5)
                * (Param.UnitMass_in_g / Param.HubbleParam) ** 0.5
                * Param.UnitVelocity_in_cm_per_s
                / (Param.UnitLength_in_cm / Param.HubbleParam)
            )
    else:
        mag *= (
            snap.UnitLength_in_cm ** (-0.5)
            * snap.UnitMass_in_g ** (0.5)
            * snap.UnitVelocity_in_cm_per_s
            / snap.UnitLength_in_cm
        )
    return mag


def get_MagneticFieldZ(snap, considering_h=False, convert_units=True):
    mag = snap.data["bfld"][:, 2].copy()
    if not convert_units:
        pass
    elif considering_h:
        try:
            mag *= (
                (snap.UnitLength_in_cm / snap.hubbleparam) ** (-0.5)
                * (snap.UnitMass_in_g / snap.hubbleparam) ** 0.5
                * snap.UnitVelocity_in_cm_per_s
                / (snap.UnitLength_in_cm / snap.hubbleparam)
            )
        except:
            import Param

            mag *= (
                (Param.UnitLength_in_cm / Param.HubbleParam) ** (-0.5)
                * (Param.UnitMass_in_g / Param.HubbleParam) ** 0.5
                * Param.UnitVelocity_in_cm_per_s
                / (Param.UnitLength_in_cm / Param.HubbleParam)
            )
    else:
        mag *= (
            snap.UnitLength_in_cm ** (-0.5)
            * snap.UnitMass_in_g ** (0.5)
            * snap.UnitVelocity_in_cm_per_s
            / snap.UnitLength_in_cm
        )
    return mag


def get_ConstantMagneticField(snap, INFO=None, convert_units=True):
    field = np.ones(np.shape(snap.data["pos"][snap.data["type"] == 0]))
    bStrength = checkExistenceKey(INFO, "constBinGauss", 10e-6)
    Xb = checkExistenceKey(INFO, "constBXB", 0.05)
    if Xb is not None:
        print("const B field: X_B=%g" % Xb)
    if Xb is not None:
        fileICs = checkExistenceKey(
            INFO,
            "constFileICs",
            os.path.dirname(os.path.realpath(__file__))
            + "/Data/ICProfile/IC_profile_Perseus_R_rho_u_codeunits.txt",
        )
        print(
            "considering constant magnetic field with x_B=%g taking density/temperature from %s"
            % (Xb, fileICs)
        )
        try:
            radiusTxt, rhoTxt, uTxt = np.loadtxt(fileICs).T
        except:
            radiusTxt, rhoTxt, uTxt = np.loadtxt(fileICs)
        dist = (
            get_DistanceToCenter(snap, no_conversion=True, INFO=INFO) * snap.hubbleparam
        )
        rho = np.interp(dist, radiusTxt, rhoTxt)
        # rho =  get_ICFileValues(dist, rhoTxt, radiusTxt)
        u = np.interp(dist, radiusTxt, uTxt) / (Xb + 1.0)
        # u =  get_ICFileValues(dist, uTxt, radiusTxt) / (Xb+1.)
        for i in range(3):
            field[:, i] = np.sqrt(rho * u * 2.0 / 3.0 * Xb * 8 * np.pi / 3.0)
        if convert_units:
            field *= np.sqrt(
                snap.UnitMass_in_g
                / snap.UnitLength_in_cm ** 3
                * snap.hubbleparam ** 2
                * (snap.UnitVelocity_in_cm_per_s) ** 2
            )
    else:
        field = field * bStrength
    return field


def get_AbsoluteConstantMagneticField(snap, INFO=None, convert_units=True):
    field = get_ConstantMagneticField(snap, INFO=INFO, convert_units=convert_units)
    field = np.sqrt(np.sum(field ** 2, axis=1))
    return field


def get_AbsoluteConstantMagneticFieldEnergyDensity(snap, INFO=None, convert_units=True):
    field = get_ConstantMagneticField(snap, INFO=INFO, convert_units=convert_units)
    field = np.sum(field ** 2, axis=1)
    return field / (8.0 * np.pi)


def get_SqrtAbsoluteConstantMagneticFieldEnergyDensity(
    snap, INFO=None, convert_units=True
):
    field = get_ConstantMagneticField(snap, INFO=INFO, convert_units=convert_units)
    field = np.sqrt(np.sum(field ** 2, axis=1))
    return field / np.sqrt(8.0 * np.pi)


def get_SqrtAbsoluteConstantMomentumFlux(snap, INFO=None, convert_units=True):
    field = get_SqrtAbsoluteConstantMagneticFieldEnergyDensity(
        snap, INFO=INFO, convert_units=convert_units
    )
    constMomFluxRhVToBRatio = checkExistenceKey(INFO, "constMomFluxRhVToBRatio", None)
    if constMomFluxRhVToBRatio is None:
        raise Exception("need to define constMomFluxRhVToBRatio!")
    field *= constMomFluxRhVToBRatio
    return field


def get_MagneticField(snap, considering_h=False, no_conversion=False):
    mag = snap.data["bfld"].copy()
    if no_conversion:
        UnitMagneticField_in_cgs = 1.0
    elif considering_h:
        UnitMagneticField_in_cgs = (
            (snap.UnitLength_in_cm / snap.hubbleparam) ** (-0.5)
            * (snap.UnitMass_in_g / snap.hubbleparam) ** (0.5)
            * snap.UnitVelocity_in_cm_per_s
            / (snap.UnitLength_in_cm / snap.hubbleparam)
        )
    else:
        UnitMagneticField_in_cgs = (
            snap.UnitLength_in_cm ** (-0.5)
            * snap.UnitMass_in_g ** (0.5)
            * snap.UnitVelocity_in_cm_per_s
            / snap.UnitLength_in_cm
        )
    return mag * UnitMagneticField_in_cgs


def get_MagneticFieldEnergyDensity(snap, considering_h=False, no_conversion=False):
    magD = get_MagneticField(
        snap, considering_h=considering_h, no_conversion=no_conversion
    )
    magD = np.sum(magD ** 2, axis=1)
    magD /= 8.0 * np.pi
    return magD


def get_SqrtMagneticFieldEnergyDensity(snap, considering_h=False, no_conversion=False):
    magD = get_MagneticField(
        snap, considering_h=considering_h, no_conversion=no_conversion
    )
    magD = np.sum(magD ** 2, axis=1)
    magD /= 8.0 * np.pi
    return np.sqrt(magD)


def get_Temperature(snap, considering_h=False, no_conversion=False):
    # calculate temperature in units of Kelvin
    if no_conversion:
        UnitTemp_in_cgs = 1.0
    else:
        UnitTemp_in_cgs = snap.UnitVelocity_in_cm_per_s ** 2
    temperature = (
        snap.u  # data["u"]
        * param.GAMMA_MINUS1
        / param.BOLTZMANN_cgs
        * param.PROTONMASS_cgs
        * UnitTemp_in_cgs
        * param.meanmolecularweight
    )
    return temperature


def get_Temperature_inkV(snap):
    # calculate temperature in units of keV
    UnitTemp_in_cgs = snap.UnitVelocity_in_cm_per_s ** 2
    temperature = (
        snap.data["u"]
        * UnitTemp_in_cgs
        * param.GAMMA_MINUS1
        * param.PROTONMASS_cgs
        * param.meanmolecularweight
        / param.BOLTZMANN_cgs
        * param.BOLTZMANN_eV
        / 1.0e3
    )  # *  param.BOLTZMANN_cgs**2 /1.e3 #/ 1.16e7
    return temperature


def get_EntropyTop(snap, considering_h=False):
    # only use with get_EntropyBottom calculate entropy (K=kT/n_e^(gamma-1) in units of keV cm^2
    temperature = get_Temperature(snap)
    entropicFunction = param.BOLTZMANN_in_eV_per_K * temperature * 1e-3
    return entropicFunction


def get_EntropyBottom(snap, considering_h=False):
    # only use with get_EntropyBottom calculate entropy (K=kT/n_e^(gamma-1) in units of keV cm^2
    if considering_h:
        UnitDens_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            * snap.hubbleparam ** 3
        )
    else:
        UnitDens_in_cgs = (
            snap.UnitMass_in_g
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
        )
    entropicFunction = (
        snap.data["rho"].astype(np.float64)
        * UnitDens_in_cgs
        / (param.mu * param.PROTONMASS_cgs)
    ) ** (param.GAMMA_MINUS1)

    return entropicFunction


def get_Entropy(snap, considering_h=False):
    # calculate entropy (K=kT/n_e^(gamma-1) in units of keV cm^2
    if considering_h:
        UnitDens_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            * snap.hubbleparam ** 3
        )
    else:
        UnitDens_in_cgs = (
            snap.UnitMass_in_g
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
        )
    temperature = get_Temperature(snap)
    entropicFunction = param.BOLTZMANN_in_eV_per_K * temperature * 1e-3
    entropicFunction /= (
        snap.data["rho"].astype(np.float64)
        * UnitDens_in_cgs
        / (param.mu * param.PROTONMASS_cgs)
    ) ** (param.GAMMA_MINUS1)

    return entropicFunction


def get_Density(snap, considering_h=False, no_conversion=False, returnLabel=False):
    # in units of g cm^-3
    if no_conversion:
        UnitDens_in_cgs = 1.0
    elif considering_h:
        UnitDens_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            * snap.hubbleparam ** 3
        )
    else:
        UnitDens_in_cgs = (
            snap.UnitMass_in_g
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
        )
    density = snap.data["rho"] * UnitDens_in_cgs
    if not returnLabel:
        return density
    else:
        return density, r"$\rho$"


def useLocationMask(field, INFO):
    mask = None
    if INFO["Location"] is not None:
        mask = INFO["LocationMask"]
        mask = np.logical_not(mask)
    else:
        return field
    if len(field.shape) == 2:
        debug.printDebug(field, "field")
        debug.printDebug(mask, "mask")
        field[:, None][mask] = 0
    else:
        field[mask] = 0
    return field


def get_ThermalSZ(snap, considering_h=False, no_conversion=False, INFO=None):
    Variables = {}
    mask = None
    if INFO["Location"] is not None:
        mask = INFO["LocationMask"]
        mask = np.logical_not(mask)
    Variables = {}
    Variables["elecnumberdensity"] = get_ElectronNumberDensity(
        snap, considering_h=considering_h, no_conversion=no_conversion
    )
    Variables["temperature"] = get_Temperature(
        snap, considering_h=considering_h, no_conversion=no_conversion
    )
    if mask is not None:
        Variables["elecnumberdensity"][mask] = 0
        Variables["temperature"][mask] = 0
    return Variables


def get_KineticSZ(snap, considering_h=False, no_conversion=False, INFO=None):
    Variables = {}
    mask = None
    if INFO["Location"] is not None:
        mask = INFO["LocationMask"]
        mask = np.logical_not(mask)
    Variables["elecnumberdensity"] = get_ElectronNumberDensity(
        snap, considering_h=considering_h, no_conversion=no_conversion
    )
    Variables["velocity"] = get_Velocity(snap, considering_h=False)
    if mask is not None:
        Variables["elecnumberdensity"][mask] = 0
        Variables["velocity"][mask] = 0
    return Variables


def get_KineticToThermalSZRatio(
    snap, considering_h=False, no_conversion=False, INFO=None
):
    Variables = {}
    mask = None
    if INFO["Location"] is not None:
        mask = INFO["LocationMask"]
        mask = np.logical_not(mask)
    Variables["elecnumberdensity"] = get_ElectronNumberDensity(
        snap, considering_h=considering_h, no_conversion=no_conversion
    )
    Variables["temperature"] = get_Temperature(
        snap, considering_h=considering_h, no_conversion=no_conversion
    )
    Variables["velocity"] = get_Velocity(snap, considering_h=False)
    if mask is not None:
        Variables["elecnumberdensity"][mask] = 0
        Variables["velocity"][mask] = 0
        Variables["temperature"][mask] = 0
    return Variables


def get_RelativisticSZ(snap, considering_h=False, no_conversion=False, INFO=None):
    Variables = {}
    pressureVariable = checkExistenceKey(INFO, "PressureContributionCRe", "Pressure")
    mask = None
    if INFO["Location"] is not None:
        mask = INFO["LocationMask"]
        mask = np.logical_not(mask)
    Variables["pressure"] = get_value(
        pressureVariable, snap, considering_h=False, convert_units=True, INFO=INFO
    )
    # Variables['elecnumberdensity'] = get_ElectronNumberDensity(snap, considering_h = considering_h,no_conversion = no_conversion)
    print("mask")
    print(mask)
    if mask is not None:
        # Variables['elecnumberdensity'][mask] = 0
        Variables["pressure"][mask] = 0
    return Variables


def get_SZSignal(snap, considering_h=False, no_conversion=False, INFO=None):
    """
	calculate SZ signal in data cube:
	use INFO dictionary to specify following options:
		xFreq: sets unitless frequency of SZ signal
		useTelBandInt: specify if want to integrate over band as done in Pfrommer 2007 (can take much longer with similar result)
			if integrate: specify bandwidth:
				DeltaFrequency
		include: which SZ effect should be included:
			thermal: can specify:
				TemperatureVariableThSZ, ElectronDensityThSZ
			kinetic: can specify:
				VelocityVariableKinSZ, ElectronDensityKinSZ
			kinetic: can specify:
				PressureContributionCReLob
				different models (PtpAssumptions):
					PcrPowerLaw, fEPowerLaw:
						RelSZp1, RelSZp2, RelSZalpha
					fEThermal:
						RelTherSZBeta
	"""
    import SZTESTING as SZ

    include = checkExistenceKey(INFO, "include", ["thermal", "kinetic", "relativistic"])
    Variables = []
    if "thermal" in include:
        temperatureVariable = checkExistenceKey(
            INFO, "TemperatureVariableThSZ", "Temperature"
        )
        densityVariable = checkExistenceKey(
            INFO, "ElectronDensityThSZ", "ElectronNumberDensity"
        )
        Variables = SZ.ifnotAlreadyInclude(
            Variables, [temperatureVariable, densityVariable]
        )
    if "kinetic" in include:
        velocityVariable = checkExistenceKey(INFO, "VelocityVariableKinSZ", "VelocityY")
        densityVariable = checkExistenceKey(
            INFO, "ElectronDensityKinSZ", "ElectronNumberDensity"
        )
        Variables = SZ.ifnotAlreadyInclude(
            Variables, [velocityVariable, densityVariable]
        )
    if "relativistic" in include:
        pressureVariable = checkExistenceKey(
            INFO, "PressureContributionCReLob", "PressureLob"
        )
        Variables = SZ.ifnotAlreadyInclude(Variables, [pressureVariable])
    VariableValues = {}
    for var in Variables:
        VariableValues[var] = get_value(
            var,
            snap,
            considering_h=considering_h,
            convert_units=not no_conversion,
            INFO=INFO,
        )
    sz = SZ.DeltaI(
        VariableValues,
        spatialSpacingArrays_in_cm=None,
        integratingAxis=None,
        include=include,
        INFO=INFO,
        singleCompDic=True,
    )
    return sz


def get_DeltaI(snap, considering_h=False, no_conversion=False, INFO=None):
    include = check_alternative(INFO["include"], ["thermal"])
    Variables = {}
    Location = checkIfInDictionary("Location", INFO, None)
    if "thermal" in include:
        Variables["elecnumberdensity"] = get_ElectronNumberDensity(
            snap, considering_h=considering_h, no_conversion=no_conversion
        )
        Variables["temperature"] = get_Temperature(
            snap, considering_h=considering_h, no_conversion=no_conversion
        )
    if "kinetic" in include:
        if "elecnumberdensity" not in list(Variables.keys()):
            print("in get deltaI, different implementation")
            exit()
    deltaI = DeltaI(
        VariableValues,
        spatialSpacingArrays_in_cm=None,
        integratingAxis=1,
        include=["thermal", "kinetic", "relativistic"],
        INFO=None,
    )
    return deltaI


def get_ElectronNumberDensityFullyIonized(snap, considering_h=False, no_conversion=False):
    # computing electron number density based on arepo electron-to-proton ratio and model for springel hernquist regime
    print("considering_h in ElectronNumberDensity", considering_h)
    Ndensity = (
        snap.data["rho"]
        * snap.UnitMass_in_g
        / param.PROTONMASS_cgs
        * param.ElectronProtonFraction
    )
    if no_conversion:
        UnitNDens_in_cgs = 1.0
    elif considering_h:
        UnitNDens_in_cgs = (
            1.0
            / snap.hubbleparam
            / 1.0
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            * snap.hubbleparam ** 3
        )
    else:
        UnitNDens_in_cgs = (
            1.0 / snap.UnitLength_in_cm / snap.UnitLength_in_cm / snap.UnitLength_in_cm
        )
    return Ndensity * UnitNDens_in_cgs


def get_ElectronNumberDensity(snap, considering_h=False, no_conversion=False):
    # computing electron number density based on arepo electron-to-proton ratio 
    if "gmet" in snap.data:
        # not used in our sims so far
        x_h = snap.gmet[snap.type == 0, 0] 
    else:
        x_h = 0.76
    rho = snap.data['rho'] * snap.UnitMass_in_g / snap.UnitLength_in_cm**3
    if "ne" in snap.data:
        ne = rho * snap.data["ne"].astype('f8') * x_h / param.PROTONMASS_cgs
    else:
        warnings.warn("get_ElectronNumberDensity: ElectronAbundance not found, assuming fully ionized!")
        ne = rho * param.ElectronProtonFraction / param.PROTONMASS_cgs
    return ne


def get_ElectronNumberDensityWarmPhase(snap, considering_h=False, no_conversion=False):
    # computing electron number density based on arepo electron-to-proton ratio and model for warm phase for springel hernquist regime
    if "gmet" in snap.data:
        # not used in our sims so far
        x_h = snap.gmet[snap.type == 0, 0] 
    else:
        x_h = 0.76
    rho = snap.data['rho'] * snap.UnitMass_in_g / snap.UnitLength_in_cm**3
    print(snap.data.keys())
    print(f"self._loaded_keys: {snap.data._loaded_keys}")
    ne = rho * snap.data["ne"].astype('f8') * x_h / param.PROTONMASS_cgs
    print(np.shape(x_h), np.shape(ne), np.shape(snap.rho))
    i, = np.where(snap.data["sfr"] > 0.)
    ne[i] = density_on_equation_of_state(snap)[i]
    print("NE:", ne.min(), ne.max())
    return ne


def get_HydrogenNumberDensity(snap):
    rho = get_Density(snap)
    if "gmet" in snap.data:
        # not used in our sims so far
        x_h = snap.gmet[snap.type == 0, 0]
    else:
        x_h = 0.76
    return rho / param.HYDROGENMASS_cgs * x_h


def density_on_equation_of_state(snap):
    rho = snap.data['rho'] * snap.UnitMass_in_g / snap.UnitLength_in_cm**3
    # convert threshold to g/cm^3
    # equivalent to around 0.13 particles/cm^3
    PhysDensThresh = 754320. * 1e10 * param.SOLARMASS_in_g / (1e6 * param.PC_in_cm)**3 / (1. / snap.hubbleparam ** 2)
    # PhysDensThresh = 754320. / (snap.time ** 3 / snap.hubbleparam ** 2)
    FactorEVP = 573.0
    facEVP = (rho / PhysDensThresh) ** -0.8 * FactorEVP
    meanweight = 4. / (1. + 3. * 0.76)
    Tfac = 1. / meanweight * (1.0 / (5. / 3. - 1.)) * param.BOLTZMANN_cgs / param.PROTONMASS_cgs / 1e10
    print(f"Tfac: {Tfac}")
    # TempClouds=1000.0  # T_cloud in K
    EgySpecCold = Tfac * snap.parameterfile["TempClouds"]
    # TempSupernova=5.73e7  # T_SN in K
    EgySpecSN = Tfac * snap.parameterfile["TempSupernova"]
    EgyHot = EgySpecSN / (1. + facEVP) + EgySpecCold
    xCloud = (snap.u - EgyHot) / (EgySpecCold - EgyHot)
    debug.printDebug(xCloud, "xCloud")
    ne = (1. - xCloud) * (0.76 + 0.23 / 4. * 2.) * rho / param.PROTONMASS_cgs
    return ne


def get_ElectronNumberDensityFullyIonized(snap, considering_h=False, no_conversion=False):
    # approximate electron number density assuming fully ionized gas for calculating constant factor for electron to proton fraction
    print("considering_h in ElectronNumberDensity", considering_h)
    Ndensity = (
        snap.data["rho"]
        * snap.UnitMass_in_g
        / param.PROTONMASS_cgs
        * param.ElectronProtonFraction
    )
    if no_conversion:
        UnitNDens_in_cgs = 1.0
    elif considering_h:
        UnitNDens_in_cgs = (
            1.0
            / snap.hubbleparam
            / 1.0
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            * snap.hubbleparam ** 3
        )
    else:
        UnitNDens_in_cgs = (
            1.0 / snap.UnitLength_in_cm / snap.UnitLength_in_cm / snap.UnitLength_in_cm
        )
    Ndensity = Ndensity * UnitNDens_in_cgs
    return Ndensity


def get_ElectronNumberDensityPerseus(
    snap, considering_h=False, no_conversion=False, INFO=None
):
    dist = get_DistanceToCenterInKpc(
        snap, considering_h=False, no_conversion=False, INFO=INFO
    )
    Ndensity = 26.9e-3 * (1.0 + (dist / 57.0) ** 2) ** (-1.8) + 2.8e-3 * (
        1.0 + (dist / 200.0) ** 2
    ) ** (-0.87)
    print((np.max(Ndensity)))
    return Ndensity


def get_ICFileValues(distance_to_center, array, radius):
    import warnings

    radius_min = radius[0]
    d_radius = radius[1] - radius[0]
    value = np.zeros((np.shape(distance_to_center)))
    index = ((distance_to_center - radius_min) / d_radius).astype(np.int64)
    dx = distance_to_center - radius[index]
    mask = np.logical_and(dx < 0, distance_to_center < radius_min)
    if np.max(abs(dx)) > d_radius:
        print((np.max(abs(dx)), d_radius))
        warnings.warn("dx cannot be larger than d_radius")
        # exit('dx cannot be larger than d_radius')
    value[mask] = array[0]
    inverted_mask = np.invert(mask)
    value[inverted_mask] = array[index][inverted_mask] + dx[
        inverted_mask
    ] / d_radius * (array[index + 1][inverted_mask] - array[index][inverted_mask])
    return value


def get_TemperatureMS0735Bubble(snap, INFO=None):
    valueTemp = checkIfInDictionary("BubbleTempInKeV", INFO, 20.0)
    realBubble = checkIfInDictionary("RealBubble", INFO, True)
    temp = get_TemperatureMS0735(snap, INFO=INFO)
    if realBubble:
        print("looking at real bubble in temp MS0735")
        thresh = checkIfInDictionary("BubbleThresh", INFO, 1e-3)
        mask = snap.data["jetr"] > thresh
        if valueTemp is None:
            temp[mask] = get_Temperature(
                snap, considering_h=False, no_conversion=False
            )[mask]
        else:
            valueTemp *= 1.0e3 / param.BOLTZMANN_eV
            temp[mask] = valueTemp
            print(("bubble temp %g K" % valueTemp))
    else:
        print("looking at fake bubble in temp MS0735")
        mask = getFakeBubble(snap, INFO=INFO)
        if valueTemp is None:
            exit("if using fake bubble need to specify temperature of cavity!")
        valueTemp *= 1.0e3 / param.BOLTZMANN_eV
        temp[mask] = valueTemp
        print(("bubble temp %g keV" % valueTemp))
    return temp


def get_ElectronNumberDensityMS0735Bubble(snap, INFO=None):
    realBubble = checkIfInDictionary("RealBubble", INFO, True)
    nDensity = get_ElectronNumberDensityMS0735(snap, INFO=INFO)
    if realBubble:
        thresh = checkIfInDictionary("BubbleThresh", INFO, 1e-3)
        mask = snap.data["jetr"] > thresh
        nDensity[mask] = (
            get_Pressure(snap, considering_h=False, no_conversion=False)[mask]
            * param.meanmolecularweight
            / (
                get_TemperatureMS0735Bubble(snap, INFO=INFO)[mask]
                * param.BOLTZMANN_cgs
                * param.ElectronProtonFraction
            )
        )
    else:
        mask = getFakeBubble(snap, INFO=INFO)
        nDensity[mask] = (
            get_PressureMS0735(snap, INFO=INFO)[mask]
            * param.meanmolecularweight
            / (
                get_TemperatureMS0735Bubble(snap, INFO=INFO)[mask]
                * param.BOLTZMANN_cgs
                * param.ElectronProtonFraction
            )
        )
    return nDensity


def get_PressureMS0735(snap, INFO=None):
    pres = (
        get_ElectronNumberDensityMS0735(snap, INFO=INFO)
        * param.ElectronProtonFraction
        * param.BOLTZMANN_cgs
        * get_TemperatureMS0735(snap, INFO=INFO)
        / param.meanmolecularweight
    )
    return pres


def get_TracerFakeBubble(snap, INFO=None):
    print("in tracer fake bubble")
    print(snap.data.keys())
    print(snap.data["pos"], snap.data["type"])
    try:
        print(snap.data["pos"], snap.data["type"])
        tracer = np.zeros(np.shape(snap.data["pos"][snap.data["type"] == 0][:, 0]))
    except:
        raise Exception("couldnt get zeros for tracers!")
    print("trying toget mask")
    mask = getFakeBubble(snap, INFO=INFO)
    print("got mask tracer fake bubble")
    tracer[mask] = 1.0
    return tracer


def get_ProfileFit(snap, variable, INFO, weight="Volume", noJet=False, NBins=10000):
    import FigureMove as Fig

    if INFO is None:
        INFO = {}
    values = get_value(variable, snap, considering_h=False, convert_units=True)
    weight = Fig.getWeights(snap, weight, True, None)
    INFO["center"] = [(snap.boxsize) / 2.0, (snap.boxsize) / 2.0, (snap.boxsize) / 2.0]
    dist = get_DistanceToCenterInKpc(snap, INFO=INFO, no_conversion=False)
    if noJet:
        mask = snap.data["jetr"] < 1e-6
        dist = dist[mask]
        values = values[mask]
        weight = weight[mask]
    x, y, err = Fig.do_Radprof(
        dist,
        values,
        weight=weight,
        range=[0, 1600],
        log=False,
        NBins=NBins,
        type="average",
        errbar=None,
        SameNumberPointsPerBin=False,
    )
    return x, y


def get_ElectronNumberDensityMS0735(snap, INFO=None):
    dist = get_DistanceToCenterInKpc(
        snap, considering_h=False, no_conversion=False, INFO=INFO
    )
    if checkIfInDictionary("useTriaxialFit", INFO, False):
        Ndensity = PressureTriaxial(
            snap.data["pos"][snap.data["type"] == 0][:, :] - 0.5 / snap.hubbleparam,
            2.68240818e-02,
            3.83396832e-02,
            3.91789499e-02,
            2.90294584e-01,
            5.80016053e-10,
        ) / (
            Param.BOLTZMANN_cgs / Param.ElectronProtonFraction
        )  # 1.79584857e-16
        print("using triax fit for pressure (dens = pressure)!!!")
    elif checkIfInDictionary("useCurrentFitProfile", INFO, True):
        x, y = get_ProfileFit(snap, "ElectronNumberDensity", INFO, weight="Volume")
        Ndensity = get_ICFileValues(dist, y, x)
    elif checkIfInDictionary("useICsProfile", INFO, True):
        data = np.loadtxt(
            "MS0735DistInKpcNeTempInKFitR7Sn160MyNoJet.txt", delimiter=","
        ).T
        Ndensity = get_ICFileValues(dist, data[1], data[0])
    else:
        print("use file for ICM profile initally used for ICs (density)")
        ICFile = checkIfInDictionary(
            "ICFile", INFO, "MS0735Profile.txt"
        )  # check_alternative('ICFile', 'MS0735Profile.txt')
        print(ICFile)
        data = np.loadtxt(ICFile)
        radius = data[0] * 1e3 / snap.hubbleparam
        Ndensity = (
            get_ICFileValues(dist, data[1], radius)
            * 1.0
            / snap.hubbleparam
            / 1.0
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            * snap.hubbleparam ** 3
            * snap.UnitMass_in_g
            / param.PROTONMASS_cgs
            / param.ElectronProtonFraction
        )
    # 0.05 * (1.0+(dist/100.)**2)**(-1.8) + 0.01 * (1.0+(dist/400.)**2)**(-0.7)
    print((np.max(Ndensity)))
    return Ndensity


def PressureTriaxial(XMinCenter, r1, r2, r3, beta, P0):
    x1 = XMinCenter[:, 0]
    x2 = XMinCenter[:, 1]
    x3 = XMinCenter[:, 2]
    return P0 * (1 + (x1 / r1) ** 2 + (x2 / r2) ** 2 + (x3 / r3) ** 2) ** (
        -3 * beta / 2.0
    )


def get_TemperatureMS0735(snap, INFO=None):
    dist = get_DistanceToCenterInKpc(
        snap, considering_h=False, no_conversion=False, INFO=INFO
    )
    if checkIfInDictionary("useTriaxialFit", INFO, False):
        temp = np.ones(np.shape(snap.data["pos"][snap.data["type"] == 0][:, 0]))
        print("using triax fit for pressure (temp = 1)!!!")
    elif checkIfInDictionary("useCurrentFitProfile", INFO, True):
        x, y = get_ProfileFit(snap, "Temperature", INFO, weight="Mass")
        temp = get_ICFileValues(dist, y, x)
    elif checkIfInDictionary("useICsProfile", INFO, True):
        data = np.loadtxt(
            "MS0735DistInKpcNeTempInKFitR7Sn160MyNoJet.txt", delimiter=","
        ).T  # MS0735DistInKpcNeTempInKFitR7Sn0.txt
        temp = get_ICFileValues(dist, data[2], data[0])
    else:
        print("use file for ICM profile initally used for ICs (temperature)")
        ICFile = checkIfInDictionary("ICFile", INFO, "MS0735Profile.txt")
        data = np.loadtxt(ICFile)
        radius = data[0] * 1e3 / snap.hubbleparam
        temp = (
            get_ICFileValues(dist, data[2], radius)
            * snap.UnitVelocity_in_cm_per_s ** 2
            * param.GAMMA_MINUS1
            / param.BOLTZMANN_cgs
            * param.PROTONMASS_cgs
            * param.meanmolecularweight
        )
    print((np.max(temp)))
    return temp


def get_NFWPotentialPerseus(snap, INFO=None):
    dist = get_DistanceToCenter(
        snap, considering_h=False, no_conversion=False, INFO=INFO
    )
    GravConst = param.GRAVITATIONALCONSTANT_cgs
    R_s = 0.424 * param.MegaParsec_in_cm
    rho_0 = 8.06864e-26  # g cm^-3
    pot = -4.0 * np.pi * GravConst * R_s ** 3 * rho_0 / dist * np.log(1 + dist / R_s)
    return pot


def get_r200_nfw(m200, hubbleparam=0.674):
    critical_density = 1.8788e-26 * hubbleparam ** 2 * 1e3 / 1e2 ** 3
    return (m200 / (4 * np.pi / 3.0 * 200 * critical_density)) ** (1 / 3.0)


def get_TotalPressurePerseus(snap, INFO=None):
    sigma_c = -1.36415e16  # cm^2 s^-2
    rho = (
        get_ElectronNumberDensity(snap, considering_h=False, no_conversion=False)
        * param.PROTONMASS_cgs
        * param.ElectronProtonFraction
    )
    tot = rho * (sigma_c - get_NFWPotentialPerseus(snap, INFO=INFO))
    return tot


def get_TotalInternalEnergyDensityPerseus(snap, INFO=None):
    sigma_c = -1.36415e16  # cm^2 s^-2
    rho1 = (
        get_ElectronNumberDensity(snap, considering_h=False, no_conversion=False)
        * param.PROTONMASS_cgs
        * param.ElectronProtonFraction
    )
    rho = get_Density(snap, considering_h=False, no_conversion=False, returnLabel=False)
    print("rho1-rho")
    print((rho1 - rho))
    tot = rho * (sigma_c - get_NFWPotentialPerseus(snap, INFO=INFO))
    print("tot")
    print(tot)
    print("rho")
    print(rho)
    print("get_NFWPotentialPerseus(snap,INFO=INFO)")
    print((get_NFWPotentialPerseus(snap, INFO=INFO)))
    return tot * 2.0 / 3.0


def get_Pressure(snap, considering_h=False, no_conversion=False, noCRs=False):
    if no_conversion:
        UnitPressure_in_cgs = 1.0
    elif considering_h:
        UnitPressure_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            / (snap.UnitLength_in_cm / snap.hubbleparam)
            * (
                snap.UnitVelocity_in_cm_per_s
                / (snap.UnitLength_in_cm / snap.hubbleparam)
            )
            ** 2
        )
    else:
        UnitPressure_in_cgs = (
            snap.UnitMass_in_g
            / snap.UnitLength_in_cm
            * (snap.UnitVelocity_in_cm_per_s / snap.UnitLength_in_cm) ** 2
        )
    # # thermal pressure in cgs
    TotalPressure = 2.0 / 3.0 * snap.data["rho"] * snap.data["u"]
    # # magnetic pressure in cgs
    if hasattr(snap, "bfld"):
        print("adding magnetic pressure to total pressure!")
        TotalPressure[:] += np.sum(snap.data["bfld"][:] ** 2, axis=1) / 8.0 / param.M_PI
    # # cosmic ray pressure in cgs ... not 100% sure about units
    if hasattr(snap, "cren") and not noCRs:
        print("adding CR pressure to total pressure!")
        TotalPressure[:] += 1.0 / 3.0 * snap.data["rho"] * snap.data["cren"][:]

    return TotalPressure * UnitPressure_in_cgs


def get_TemperatureBubble(snap, considering_h=False, no_conversion=False, INFO=None):
    valueTemp = checkIfInDictionary("BubbleTempInKeV", INFO, 20.0)
    realBubble = checkIfInDictionary("RealBubble", INFO, True)
    temp = get_Temperature(
        snap, considering_h=considering_h, no_conversion=no_conversion
    )
    if realBubble:
        thresh = checkIfInDictionary("BubbleThresh", INFO, 1e-3)
        mask = snap.data["jetr"] > thresh
        if valueTemp is not None:
            valueTemp *= 1.0e3 / param.BOLTZMANN_eV
            temp[mask] = valueTemp
            print(("bubble temp %g keV" % valueTemp))
    else:
        print("looking at fake bubble in temp MS0735")
        mask = getFakeBubble(snap, INFO=INFO)
        if valueTemp is None:
            exit("if using fake bubble need to specify temperature of cavity!")
        valueTemp *= 1.0e3 / param.BOLTZMANN_eV
        temp[mask] = valueTemp
        print(("bubble temp %g K" % valueTemp))
    return temp


def get_ElectronNumberDensityBubble(
    snap, considering_h=False, no_conversion=False, INFO=None
):
    realBubble = checkIfInDictionary("RealBubble", INFO, True)
    nDensity = get_ElectronNumberDensity(
        snap, considering_h=considering_h, no_conversion=no_conversion
    )
    if realBubble:
        # replaces number density in bubbles with value given for constant (user-defined) temperature in bubble to satisfy total pressure given by bubble in bubble
        thresh = checkIfInDictionary("BubbleThresh", INFO, 1e-3)
        mask = snap.data["jetr"] > thresh
        # following should be in cgs w/ Kelvin and dyn
        nDensity[mask] = (
            get_Pressure(
                snap, considering_h=considering_h, no_conversion=no_conversion
            )[mask]
            * param.meanmolecularweight
            / (
                get_TemperatureBubble(
                    snap,
                    considering_h=considering_h,
                    no_conversion=no_conversion,
                    INFO=INFO,
                )[mask]
                * param.BOLTZMANN_cgs
                * param.ElectronProtonFraction
            )
        )
    else:
        mask = getFakeBubble(snap, INFO=INFO)
        nDensity[mask] = (
            get_Pressure(
                snap, considering_h=considering_h, no_conversion=no_conversion
            )[mask]
            * param.meanmolecularweight
            / (
                get_TemperatureBubble(
                    snap,
                    considering_h=considering_h,
                    no_conversion=no_conversion,
                    INFO=INFO,
                )[mask]
                * param.BOLTZMANN_cgs
                * param.ElectronProtonFraction
            )
        )
    return nDensity


def get_CRPressure(snap, considering_h=False):
    if considering_h:
        UnitPressure_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            / (snap.UnitLength_in_cm / snap.hubbleparam)
            * (
                snap.UnitVelocity_in_cm_per_s
                / (snap.UnitLength_in_cm / snap.hubbleparam)
            )
            ** 2
        )
    else:
        UnitPressure_in_cgs = (
            snap.UnitMass_in_g
            / snap.UnitLength_in_cm
            * (snap.UnitVelocity_in_cm_per_s / snap.UnitLength_in_cm) ** 2
        )
    return (
        UnitPressure_in_cgs
        * 1.0
        / 3.0
        * snap.data["rho"][:]
        * snap.data["CosmicRaySpecificEnergy"][:]
    )


def get_ThermalPressure(
    snap, considering_h=False, no_conversion=False, returnLabel=False
):
    if no_conversion:
        UnitPressure_in_cgs = 1.0
    elif considering_h:
        UnitPressure_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            / (snap.UnitLength_in_cm / snap.hubbleparam)
            * (
                snap.UnitVelocity_in_cm_per_s
                / (snap.UnitLength_in_cm / snap.hubbleparam)
            )
            ** 2
        )
    else:
        UnitPressure_in_cgs = (
            snap.UnitMass_in_g
            / snap.UnitLength_in_cm
            * (snap.UnitVelocity_in_cm_per_s / snap.UnitLength_in_cm) ** 2
        )
    if returnLabel:
        return (
            UnitPressure_in_cgs
            * param.GAMMA_MINUS1
            * snap.data["rho"]
            * snap.data["u,"],
            r"$P_\mathrm{th}$",
        )
    return UnitPressure_in_cgs * param.GAMMA_MINUS1 * snap.data["rho"] * snap.data["u"]


def get_Time(
    snap, considering_h=False,
):
    if considering_h:
        raise Exception("reading in snapshot takes care of h now!")
    # calculates time in units of Myr
    if considering_h:
        try:
            UnitTime_in_cgs = (
                snap.UnitLength_in_cm / snap.hubbleparam
            ) / snap.UnitVelocity_in_cm_per_s
        except:
            import Param

            UnitTime_in_cgs = (
                Param.UnitLength_in_cm / Param.HubbleParam
            ) / Param.UnitVelocity_in_cm_per_s

    else:
        UnitTime_in_cgs = snap.UnitLength_in_cm / snap.UnitVelocity_in_cm_per_s
    time = snap.time * UnitTime_in_cgs

    time /= 31536000000000.0
    return time


def get_delta_time_snapshot_in_myr(snap, info=None, use_previous_if_last_snap=True):
    import FigureMove as Fig

    numbers, times = Fig.load_time_file(
        snap.folder,
        filename="TimeFile.txt",
        check_completion=True,
        maxFiles=1000,
        snapbase="snap_",
        snapdirbase="snapdir_",
        check_criteria="max",
        verbose=False,
    ).T
    numbers = np.array(numbers, dtype=int)
    m = np.argsort(numbers)
    numbers = numbers[m]
    times = times[m]
    use_previous_if_last_snap = checkExistenceKey(
        info, "usePreviousIfLastSnap", use_previous_if_last_snap
    )
    print(f"snap.file_number: {snap.file_number}")
    m = np.where(numbers == snap.file_number)[0][0]
    if m + 1 < len(times):
        time = times[m + 1] - times[m]
    else:
        if use_previous_if_last_snap:
            time = times[m] - times[m - 1]
        else:
            time = 0
    # if time < 0:
    #     raise Exception(f"delta time is <0, time={time}")
    # if time > 50:
    #     raise Exception(f"delta time probably large: {time}! for {snap.folder}")
    return time


def get_cum_variable(snap, info=None):
    import FigureMove as Fig

    print(f"info: {info}")
    function = checkExistenceKey(info, "cumFunction", get_BHAgnPower)
    times_function = checkExistenceKey(
        info, "cumTimesFunction", get_delta_time_snapshot_in_myr
    )
    cumFactor = checkExistenceKey(info, "cumFactor", 1)
    numbers, times = Fig.load_time_file(
        snap.folder,
        filename="TimeFile.txt",
        check_completion=True,
        maxFiles=1000,
        snapbase="snap_",
        snapdirbase="snapdir_",
        check_criteria="max",
        verbose=False,
    ).T
    numbers = np.array(numbers, dtype=int)
    m = np.argsort(numbers)
    numbers = numbers[m]
    index_snap = np.where(numbers == snap.file_number)[0][0]
    variable = np.zeros(index_snap + 1)
    multiply = np.ones(index_snap + 1)
    for i in range(index_snap + 1):
        snap = Fig.quickImport(numbers[i], folder=snap.folder)
        variable[i] = function(snap, info=info)
        if times_function is not None:
            multiply[i] = times_function(snap, info=info)
    print(f"multiply: {multiply}")
    print(f"variable: {variable}")
    print(f"cumFactor: {cumFactor}")
    return np.sum(variable * multiply * cumFactor)


def get_XrayLuminosity(snap, considering_h=False, no_conversion=False, INFO=None):
    # calculate Xray luminosity density in erg s^-1
    emissivity = get_XrayEmissivity(
        snap, considering_h=considering_h, no_conversion=no_conversion, INFO=INFO
    )
    vol = snap.vol
    if not no_conversion:
        vol = vol[:] * snap.UnitLength_in_cm ** 3
    return emissivity * vol


def get_XrayEmissivity(snap, considering_h=False, no_conversion=False, INFO=None):
    # calculate Xray Emissivity density in erg s^-1 cm^-3 from 1st paper of Jacob 2017 in erg /s
    # equivalent to get_XrayCoolingRate
    XrayTemperatureCutInKeV = checkExistenceKey(INFO, "XrayTemperatureCutInKeV", None)
    XrayEmissivityDebug = checkExistenceKey(INFO, "XrayEmissivityDebug", False)
    temp = get_Temperature(
        snap, considering_h=considering_h, no_conversion=no_conversion
    )
    dens = get_ElectronNumberDensity(
        snap, considering_h=considering_h, no_conversion=no_conversion
    )
    print(INFO)
    if XrayTemperatureCutInKeV is not None:
        mask = get_Temperature_inkV(snap) > XrayTemperatureCutInKeV
        print((np.size(temp[mask]) / np.size(temp)))
        dens[mask] = 0.0
        print(("fraction of cells hotter than %.2g" % XrayTemperatureCutInKeV))
        print((np.size(temp[mask]), np.size(temp)))
        print((float(np.size(temp[mask])) / np.size(temp)))
    Lambda0 = 1.2e-23  # erg cm^3/s
    Lambda1 = 1.8e-27  # erg cm^3/s /K^{1/2}
    emissivity = dens ** 2 * (Lambda0 + Lambda1 * temp ** (1.0 / 2.0))
    if XrayEmissivityDebug:
        emissivity = dens ** 2 * (Lambda1 * temp ** (1.0 / 2.0))
    return emissivity


def get_FakeEnergy(snap, considering_h=False):
    # calculate fake energy = specific internal energy * mass
    if considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g / snap.hubbleparam * snap.UnitVelocity_in_cm_per_s ** 2
        )
    else:
        UnitEng_in_cgs = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    return snap.data["mass"][snap.data["type"] == 0] * snap.data["u"] * UnitEng_in_cgs


def get_One(snap):
    # every cell set to one
    return np.ones_like(snap.rho)


def get_UnitsVelocity(snap, INFO):
    unitsV = checkIfInDictionary("unitsV", INFO, "cgs")
    print("units velocity: ", unitsV)
    if unitsV == "cgs":
        Unit_Vel = snap.UnitVelocity_in_cm_per_s
    elif unitsV == "kms":
        Unit_Vel = snap.UnitVelocity_in_cm_per_s / 1e5
    else:
        print(unitsV)
        exit("units not yet implemented for velocities!")
    return Unit_Vel


def get_UnitsAcceleration(snap, INFO, considering_h=False, convert_from_cgs_to=False):
    unitsAcc = checkIfInDictionary("unitsAcc", INFO, "cgs")
    print("units acceleration: ", unitsAcc)
    units_cgs = snap.UnitVelocity_in_cm_per_s / (
        snap.UnitLength_in_cm / snap.UnitVelocity_in_cm_per_s
    )
    if considering_h:
        units_cgs = snap.UnitVelocity_in_cm_per_s / (
            snap.UnitLength_in_cm / snap.hubbleparam / snap.UnitVelocity_in_cm_per_s
        )
    if convert_from_cgs_to:
        units_cgs = 1.0
    if unitsAcc == "cgs":
        unit_acc = units_cgs
    elif unitsAcc == "kpcpermyr":
        unit_acc = units_cgs * param.SEC_PER_MEGAYEAR ** 2 / param.KiloParsec_in_cm
    else:
        raise Exception(f"units: {unitsAcc} not yet implemented for velocities!")
    return unit_acc


def get_AbsoluteVelocitySpeedOfLightRatio(snap):
    # calculates abolsute velocity / speed of light
    AbsoluteVelocity = (
        np.sqrt(
            snap.data["vel"][snap.data["type"] == 0][:, 0] ** 2
            + snap.data["vel"][snap.data["type"] == 0][:, 1] ** 2
            + snap.data["vel"][snap.data["type"] == 0][:, 2] ** 2
        )
        * snap.UnitVelocity_in_cm_per_s
    )
    return AbsoluteVelocity / param.SPEEDOFLIGHT_cgs


def get_AbsoluteVelocity(snap, considering_h=False, INFO=None, no_conversion=True):
    # calculates absolute velocity in cm/s
    AbsoluteVelocity = np.sqrt(
        snap.data["vel"][snap.data["type"] == 0][:, 0] ** 2
        + snap.data["vel"][snap.data["type"] == 0][:, 1] ** 2
        + snap.data["vel"][snap.data["type"] == 0][:, 2] ** 2
    )
    if no_conversion:
        Unit_Vel = 1.0
    else:
        Unit_Vel = get_UnitsVelocity(snap, INFO)
    return AbsoluteVelocity * Unit_Vel


def get_AbsoluteVelocityinkms(snap, considering_h=False, no_conversion=False):
    # calculates absolute velocity in units of km/s
    if no_conversion:
        UnitVelocity = 1.0
    else:
        UnitVelocity = snap.UnitVelocity_in_cm_per_s / 1.0e5
    AbsoluteVelocity = (
        np.sqrt(np.sum(snap.data["vel"][snap.data["type"] == 0][:, :] ** 2, axis=1))
        * UnitVelocity
    )
    return AbsoluteVelocity


def get_VelocityRadial(snap, INFO=None, no_conversion=False):
    center = checkIfInDictionary("center", INFO, snap.center)
    r = snap.pos[snap.type == 0] - np.array(center)
    r_normed = r[:, :] / np.sqrt(np.sum(r ** 2, axis=1))[:, None]
    vel = snap.vel[snap.type == 0]
    if no_conversion:
        units_vel = 1.0
    else:
        units_vel = get_UnitsVelocity(snap, INFO)
    return np.sum(vel * r_normed, axis=1)


def get_VelocityX(snap, considering_h=False, INFO=None, no_conversion=True):
    """

    Parameters
    ----------
    snap : arepo snapshot
    considering_h : only useful for cosmological runs and nfw
    INFO : parameters input
    no_conversion : just code units

    Returns velocity in x direction
    -------

    """
    # calculates velocity in x direction in cm/s
    VelocityX = snap.data["vel"][snap.data["type"] == 0][:, 0]
    if no_conversion:
        Unit_Vel = 1.0
    else:
        Unit_Vel = get_UnitsVelocity(snap, INFO)
    return VelocityX * Unit_Vel


def get_VelocityY(snap, considering_h=False, INFO=None, no_conversion=True):
    """
    
    Parameters
    ----------
    snap : arepo snapshot
    considering_h : only useful for cosmological runs and nfw
    INFO : parameters input
    no_conversion : just code units

    Returns velocity in y direction
    -------

    """
    # calculates velocity in x direction in cm/s
    VelocityY = snap.data["vel"][snap.data["type"] == 0][:, 1]
    if no_conversion:
        Unit_Vel = 1.0
    else:
        Unit_Vel = get_UnitsVelocity(snap, INFO)
    return VelocityY * Unit_Vel


def get_VelocityBelowXkeV(snap, considering_h=False, INFO=None, no_conversion=True):
    # calculates velocity in x direction in cm/s
    Velocity = np.zeros(np.shape(snap.data["vel"][snap.data["type"] == 0][:, :]))
    temperature = get_Temperature_inkV(snap)
    thresh = checkIfInDictionary("ThreshVelKeV", INFO, 20)
    mask = temperature < thresh  # in keV
    print(
        (
            "get_VelocityBelowXkeV: Setting cell fraction %g to 0 for temperature cut at %g keV!"
            % (
                1 - float(np.size(Velocity[:, 0][mask])) / np.size(Velocity[:, 0]),
                thresh,
            )
        )
    )
    for i in range(3):
        Velocity[:, i][mask] = snap.data["vel"][snap.data["type"] == 0][:, i][mask]
    if no_conversion:
        Unit_Vel = 1.0
    else:
        Unit_Vel = get_UnitsVelocity(snap, INFO)
    return Velocity * Unit_Vel


def get_SoundSpeedBelowXkeV(snap, considering_h=False, INFO=None, no_conversion=True):
    # calculates sound speed in cm/s
    sound_speed = get_SoundSpeed(snap, convert_units=not no_conversion, INFO=INFO)
    temperature = get_Temperature_inkV(snap)
    thresh = checkIfInDictionary("ThreshVelKeV", INFO, 20)
    mask = temperature > thresh  # in keV
    sound_speed[mask] = 0
    print(
        (
            "get_VelocityBelowXkeV: Setting cell fraction %g to 0 for temperature cut at %g keV!"
            % (np.size(sound_speed[mask]) / np.size(sound_speed), thresh,)
        )
    )
    if no_conversion:
        Unit_Vel = 1.0
    else:
        Unit_Vel = get_UnitsVelocity(snap, INFO)
    return sound_speed * Unit_Vel


def get_VelocityYAbove20keV(snap, considering_h=False, INFO=None, no_conversion=True):
    # calculates velocity in x direction in cm/s
    VelocityY = np.zeros(np.shape(snap.data["vel"][snap.data["type"] == 0][:, 1]))
    temperature = get_Temperature_inkV(snap)
    mask = temperature < 20.0  # in keV
    print(
        (
            "get_VelocityYAbove20keV: Setting cell fraction %g to 0 for temperature cut!"
            % (1 - float(np.size(VelocityY[mask])) / np.size(VelocityY))
        )
    )
    VelocityY[mask] = snap.data["vel"][snap.data["type"] == 0][:, 1][mask]
    if no_conversion:
        Unit_Vel = 1.0
    else:
        Unit_Vel = get_UnitsVelocity(snap, INFO)
    return VelocityY * Unit_Vel


def get_VelocityZ(snap, considering_h=False, INFO=None, no_conversion=True):
    # calculates velocity in x direction in cm/s
    VelocityZ = snap.data["vel"][snap.data["type"] == 0][:, 2]
    if no_conversion:
        Unit_Vel = 1.0
    else:
        Unit_Vel = get_UnitsVelocity(snap, INFO)
    return VelocityZ * Unit_Vel


def get_Velocity(snap, considering_h=False, no_conversion=True, INFO=None):
    # calculates velocity in cm/s
    if no_conversion:
        Unit_Vel = 1.0
    else:
        Unit_Vel = get_UnitsVelocity(snap, INFO)
    Velocity = snap.data["vel"][snap.data["type"] == 0].copy() * Unit_Vel
    return Velocity


def get_CurrentDensity(snap, convert_units=False, considering_h=False):
    # current density = density * velocity
    if not convert_units:
        UnitCurrentDensity_in_cgs = 1.0
    elif considering_h:
        UnitCurrentDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            / snap.UnitLength_in_cm ** 3
            * snap.hubbleparam ** 3
            * snap.UnitVelocity_in_cm_per_s
        )
    else:
        UnitCurrentDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            / snap.UnitLength_in_cm ** 3
            * snap.hubbleparam ** 3
            * snap.UnitVelocity_in_cm_per_s
        )
    CurrentDensity = np.zeros(np.shape(snap.data["vel"][snap.data["type"] == 0]))
    for i in range(3):
        CurrentDensity[:, i] = (
            snap.data["rho"]
            * snap.data["vel"][snap.data["type"] == 0][:, i].copy()
            * UnitCurrentDensity_in_cgs
        )
    return CurrentDensity


def get_Mass(snap, considering_h=False, convert_units=True, returnLabel=False):
    # calculates mass in g of type 0
    if not convert_units:
        UnitMass_in_cgs = 1.0
    elif considering_h:
        UnitMass_in_cgs = snap.UnitMass_in_g / snap.hubbleparam
    else:
        UnitMass_in_cgs = snap.UnitMass_in_g

    Mass = snap.data["mass"][snap.data["type"] == 0][:] * UnitMass_in_cgs
    if not returnLabel:
        return Mass
    else:
        return Mass, r"$m$"


def get_MassInSolar(snap):
    return snap.mass[snap.data["type"] == 0] * snap.UnitMass_in_g / param.SOLARMASS_in_g


def get_Volume(
    snap, considering_h=False, convert_units=False, highprecision=False, INFO=None
):
    # calculates volume in code units
    if not highprecision:
        if not hasattr(snap, "vol"):
            vol = snap.data["mass"][snap.data["type"] == 0] / snap.data["rho"]
        else:
            vol = snap.data["vol"]
    else:
        if not hasattr(snap, "vol"):
            vol = snap.data["mass"][snap.data["type"] == 0].astype(
                np.float128
            ) / snap.data["rho"].astype(np.float128)
        else:
            vol = snap.data["vol"]
    if considering_h:
        Volume = (
            vol / snap.hubbleparam ** 3
        )  # get_Mass(snap, considering_h = considering_h)/get_Density(snap, considering_h = considering_h)
        print("volume in Mpc^3")
    else:
        Volume = vol
    try:
        UnitsVolume = INFO["UnitsVolume"]
    except:
        UnitsVolume = "code"
        if convert_units:
            UnitsVolume = "code"
    if UnitsVolume == "kpc3":
        print(("using following units for volume: %s" % UnitsVolume))
        Volume = Volume * (snap.UnitLength_in_cm / param.MegaParsec_in_cm * 1e3) ** 3
    elif UnitsVolume == "cm3":
        print(("using following units for volume: %s" % UnitsVolume))
        Volume = Volume * (snap.UnitLength_in_cm) ** 3
    else:
        print(("using following units for volume: code"))

    return Volume


def get_VolumeInKpc3(snap, considering_h=False):
    vol = snap.vol * (snap.UnitLength_in_cm / param.KiloParsec_in_cm) ** 3
    if considering_h:
        vol *= 1 / snap.hubble_param ** 3
    return vol


def get_Machnumber(snap, considering_h=False):
    # calculate Machnumber
    return snap.data["Machnumber"]


def get_MachnumberICM(snap, considering_h=False):
    mask = snap.data["jetr"] < 1e-3
    machs = np.zeros(np.shape(snap.data["Machnumber"]))
    machs[mask] = snap.data["Machnumber"][mask]
    return machs


def get_DissipationEnergy(snap, considering_h=False, convert_units=False):
    diss = snap.data["EnergyDissipation"] * snap.data["TimeStep"]
    if not convert_units:
        UnitEng_in_cgs = 1.0
    elif considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g / snap.hubbleparam * snap.UnitVelocity_in_cm_per_s ** 2
        )
    else:
        UnitEng_in_cgs = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2

    return diss * UnitEng_in_cgs


def get_VorticityperMyr2(snap, considering_h=False, convert_units=False):
    # vorticity =  | nabla x v | ** 2 in [Myr^-2]
    vorti = (
        snap.data["vort"][:, 0] ** 2
        + snap.data["vort"][:, 1] ** 2
        + snap.data["vort"][:, 2] ** 2
    )
    if not convert_units:
        Unit_InvVelo2 = 1.0
    elif considering_h:
        Unit_InvVelo2 = 1 / (snap.UnitLength_in_cm / snap.UnitVelocity_in_cm_per_s / param.SEC_PER_MEGAYEAR) ** 2 * (snap.hubbleparam) ** 2
    else:
        Unit_InvVelo2 = 1 / (snap.UnitLength_in_cm / snap.UnitVelocity_in_cm_per_s / param.SEC_PER_MEGAYEAR) ** 2
    return vorti * Unit_InvVelo2


def get_DissipationEnergyDensity(
    snap, considering_h=False, convert_units=False, returnLabel=False
):
    dissD = snap.data["EnergyDissipation"] / snap.data["vol"] * snap.data["TimeStep"]
    if not convert_units:
        UnitEngD_in_cgs = 1.0
    elif considering_h:
        UnitEngD_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitEngD_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm) ** 3
        )
    if not returnLabel:
        return dissD * UnitEngD_in_cgs
    else:
        return dissD * UnitEngD_in_cgs, r"$\epsilon_\mathrm{diss}$"


def get_SoundSpeed(snap, convert_units=True, units=None, INFO=None):
    speed = np.sqrt(param.GAMMA * (param.GAMMA - 1.0) * snap.data["u"])
    if not convert_units:
        Unit_Vel = 1.0
    else:
        if units is not None and not checkIfInDictionary("unitsV", INFO, False):
            if INFO is None:
                INFO = {}
            INFO["unitsV"] = units
        Unit_Vel = get_UnitsVelocity(snap, INFO)
    return speed * Unit_Vel


def get_VelocitySoundSpeedRatio(snap):
    GAMMA = 5.0 / 3.0
    Velocity = np.sqrt(
        (snap.data["vel"][snap.data["type"] == 0][:, :] ** 2.0).sum(axis=1)
    )
    SoundSpeed = np.sqrt(GAMMA * (GAMMA - 1.0) * snap.data["u"])
    return Velocity / SoundSpeed


def get_VelocitySignalSpeedRatio(snap):
    GAMMA = 5.0 / 3.0
    GAMMACR = 4.0 / 3.0
    Velocity = np.sqrt(
        (snap.data["vel"][snap.data["type"] == 0][:, :] ** 2.0).sum(axis=1)
    )
    SoundSpeed = np.sqrt(GAMMA * (GAMMA - 1.0) * snap.data["u"])
    SoundSpeedCR = np.sqrt(GAMMACR * (GAMMACR - 1.0) * snap.data["cren"])
    AlfvenSpeed = np.sqrt(
        snap.data["bfld"][:, 0] ** 2
        + snap.data["bfld"][:, 1] ** 2
        + snap.data["bfld"][:, 2] ** 2
    ) / np.sqrt(4.0 * np.pi * snap.data["rho"])
    print(
        (
            "average Sound Speed %g, Sound Speed CR %g, Alfven Speed %g"
            % (
                np.average(SoundSpeed),
                np.average(SoundSpeedCR),
                np.average(AlfvenSpeed),
            )
        )
    )
    return Velocity / np.sqrt(SoundSpeed ** 2 + SoundSpeedCR ** 2 + AlfvenSpeed ** 2)


def get_Cren(snap, considering_h=False, change=True):
    # calculates cray specific energy
    Cren = snap.data["cren"].copy()
    if change:
        Cren = introduce_nonzero_floor(Cren)
    return Cren * snap.UnitVelocity_in_cm_per_s ** 2


def get_JetTracer(snap, considering_h=False):
    tracer = snap.data["jetr"].copy()
    #    tracer[tracer<=1e-3] = 1e-3
    return tracer


def get_JetVolumeFraction(snap, considering_h=False, no_conversion=True):
    tracerVolume = abs(snap.data["jetr"] * snap.data["vol"][:])
    UnitVolume_in_cgs = 1.0
    if no_conversion:
        UnitVolume_in_cgs = 1.0
    elif considering_h:
        print("cant convert jetvolume to actual units")
        #     UnitVolume_in_cgs = (snap.UnitLength_in_cm/snap.hubbleparam) **3
    else:
        print("cant convert jetvolume to actual units")
        # UnitVolume_in_cgs = snap.UnitLength_in_cm **3
    tracerVolume *= UnitVolume_in_cgs
    print("tracerVolume")
    print((np.min(tracerVolume), np.max(tracerVolume)))
    # print tracerVolume
    return tracerVolume


def get_JetDensityFraction(snap, considering_h=False, no_conversion=True):
    snap.jetr
    jet_tracer = np.zeros_like(snap.jetr)
    m = snap.jetr > 0
    jet_tracer[m] = snap.jetr[m]
    Trho = abs(jet_tracer * snap.data["rho"][:])
    if no_conversion:
        UnitDens_in_cgs = 1.0
    elif considering_h:
        UnitDens_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            * snap.hubbleparam ** 3
        )
    else:
        UnitDens_in_cgs = (
            snap.UnitMass_in_g
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
        )
    Trho *= UnitDens_in_cgs
    print("tracerVolume")
    print((np.min(Trho), np.max(Trho)))
    # print Trho
    return Trho


def get_PassiveScalarCRAccelerationEnergyDensity(
    snap, considering_h=False, no_conversion=True
):
    snap.data["jeac"][snap.data["jeac"] < 0] = 0
    Trho = abs(snap.data["jeac"] * snap.data["rho"])
    if no_conversion:
        UnitEngDens_in_cgs = 1.0
    elif considering_h:
        UnitEngDens_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            * snap.hubbleparam ** 3
            * snap.UnitVelocity_in_cm_per_s ** 2
        )
    else:
        UnitEngDens_in_cgs = (
            snap.UnitMass_in_g
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            * snap.UnitVelocity_in_cm_per_s ** 2
        )
    Trho *= UnitEngDens_in_cgs
    print("AccTracer energydensity")
    print((np.min(Trho), np.max(Trho)))
    # print Trho
    return Trho


def get_PassiveScalarCRAccelerationEnergy(
    snap, considering_h=False, no_conversion=False,
):
    energy = snap.data["jeac"] * snap.data["mass"][snap.data["type"] == 0]
    if no_conversion:
        UnitEng_in_cgs = 1.0
    elif considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g / snap.hubbleparam * snap.UnitVelocity_in_cm_per_s ** 2
        )
    else:
        UnitEng_in_cgs = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    energy = energy * UnitEng_in_cgs
    return energy


def get_JetMassFraction(snap, considering_h=False, no_conversion=True):
    tracermass = abs(snap.data["jetr"] * snap.data["mass"][snap.data["type"] == 0][:])
    UnitMass_in_cgs = 1.0
    if no_conversion:
        UnitMass_in_cgs = 1.0
    elif considering_h:
        print("cant convert jetmass fraction")
        # UnitMass_in_cgs = snap.UnitMass_in_g / snap.hubbleparam
    else:
        print("cant convert jetmass fraction")
        # UnitMass_in_cgs = snap.UnitMass_in_g

        # UnitVolume_in_cgs = snap.UnitLength_in_cm **3
    tracermass *= UnitMass_in_cgs
    print("tracermass")
    # print tracermass
    return tracermass


def get_InternalCosmicRatio(snap, considering_h=False):
    ratio = snap.data["cren"] / snap.data["u"]
    return ratio


def get_InternalEnergyDensity(
    snap,
    considering_h=False,
    where=None,
    highprecision=False,
    returnLabel=False,
    no_conversion=False,
):
    # units in gauss
    if no_conversion:
        UnitEnergyDensity_in_cgs = 1.0
    elif considering_h:
        UnitEnergyDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitEnergyDensity_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 3
        )

    if (where == None) and highprecision:
        internal = snap.data["rho"].astype(np.float128) * snap.data["u"].astype(
            np.float128
        )
    elif (where == None) and not highprecision:
        internal = snap.data["rho"] * snap.data["u"]
    elif (where != None) and not highprecision:
        internal = snap.data["rho"][where] * snap.data["u"][where]
    else:
        internal = snap.data["rho"][where].astype(np.float128) * snap.data["u"][
            where
        ].astype(np.float128)
    internal *= UnitEnergyDensity_in_cgs
    if not returnLabel:
        return internal
    else:
        return internal, r"$\epsilon_u$"


def get_CosmicRayEnergyDensity(
    snap,
    considering_h=False,
    where=None,
    highprecision=False,
    no_conversion=False,
    returnLabel=False,
):
    # units in gauss
    if no_conversion:
        UnitEnergyDensity_in_cgs = 1.0
    elif considering_h:
        UnitEnergyDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitEnergyDensity_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 3
        )

    if (where == None) and highprecision:
        cosmic = snap.data["rho"].astype(np.float128) * snap.data["cren"].astype(
            np.float128
        )
    elif (where == None) and not highprecision:
        cosmic = snap.data["rho"] * snap.data["cren"]
    elif (where != None) and not highprecision:
        cosmic = snap.data["rho"][where] * snap.data["cren"][where]
    else:
        cosmic = snap.data["rho"][where].astype(np.float128) * snap.data["cren"][
            where
        ].astype(np.float128)
    cosmic *= UnitEnergyDensity_in_cgs
    if not returnLabel:
        return cosmic
    else:
        return cosmic, r"$\epsilon_\mathrm{cr}$"


def get_MagneticEnergyDensity(
    snap,
    considering_h=False,
    where=None,
    highprecision=False,
    returnLabel=False,
    no_conversion=False,
):
    # units in gauss
    if no_conversion:
        UnitEnergyDensity_in_cgs = 1.0
    elif considering_h:
        UnitEnergyDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitEnergyDensity_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 3
        )

    if (where == None) and highprecision:
        mag = (
            snap.data["bfld"][:, 0].astype(np.float128) ** 2
            + snap.data["bfld"][:, 1].astype(np.float128) ** 2
            + snap.data["bfld"][:, 2].astype(np.float128) ** 2
        ) / (8.0 * np.pi)
    elif (where == None) and not highprecision:
        mag = (
            snap.data["bfld"][:, 0] ** 2
            + snap.data["bfld"][:, 1] ** 2
            + snap.data["bfld"][:, 2] ** 2
        ) / (8.0 * np.pi)
    elif (where != None) and not highprecision:
        mag = (
            snap.data["bfld"][:, 0][where] ** 2
            + snap.data["bfld"][:, 1][where] ** 2
            + snap.data["bfld"][:, 2][where] ** 2
        ) / (8.0 * np.pi)
    else:
        mag = (
            snap.data["bfld"][:, 0][where].astype(np.float128) ** 2
            + snap.data["bfld"][:, 1][where].astype(np.float128) ** 2
            + snap.data["bfld"][:, 2][where].astype(np.float128) ** 2
        ) / (8.0 * np.pi)
    mag *= UnitEnergyDensity_in_cgs
    if not returnLabel:
        return mag
    else:
        return mag, r"$\epsilon_{\mathbf{B}}$"


def get_RamPressure(snap, considering_h=False, where=None, highprecision=False):
    # units in gauss
    if considering_h:
        UnitEnergyDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitEnergyDensity_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 3
        )

    if (where == None) and highprecision:
        ram = (
            snap.data["rho"].astype(np.float128)
            * (
                snap.data["vel"][snap.data["type"] == 0][:, 0].astype(np.float128) ** 2
                + snap.data["vel"][snap.data["type"] == 0][:, 1].astype(np.float128)
                ** 2
                + snap.data["vel"][snap.data["type"] == 0][:, 2].astype(np.float128)
                ** 2
            )
            / 2.0
        )
    elif (where == None) and not highprecision:
        ram = (
            snap.data["rho"]
            * (
                snap.data["vel"][snap.data["type"] == 0][:, 0] ** 2
                + snap.data["vel"][snap.data["type"] == 0][:, 1] ** 2
                + snap.data["vel"][snap.data["type"] == 0][:, 2] ** 2
            )
            / 2.0
        )
    elif (where != None) and not highprecision:
        ram = (
            snap.data["rho"][where]
            * (
                snap.data["vel"][snap.data["type"] == 0][:, 0][where] ** 2
                + snap.data["vel"][snap.data["type"] == 0][:, 1][where] ** 2
                + snap.data["vel"][snap.data["type"] == 0][:, 2][where] ** 2
            )
            / 2.0
        )
    else:
        ram = (
            snap.data["rho"][where].astype(np.float128)
            * (
                snap.data["vel"][snap.data["type"] == 0][:, 0][where].astype(
                    np.float128
                )
                ** 2
                + snap.data["vel"][snap.data["type"] == 0][:, 1][where].astype(
                    np.float128
                )
                ** 2
                + snap.data["vel"][snap.data["type"] == 0][:, 2][where].astype(
                    np.float128
                )
                ** 2
            )
            / 2.0
        )
    ram *= UnitEnergyDensity_in_cgs
    return ram


def get_TotalEnergyDensity(
    snap, considering_h=False, no_conversion=False, returnLabel=False
):
    energy_density = (
        snap.data["rho"]
        * np.sum(snap.data["vel"][snap.data["type"] == 0][:, :] ** 2, axis=1)
        / 2.0
        + snap.data["rho"] * snap.data["u"]
    )
    if hasattr(snap, "bfld"):
        energy_density += np.sum(snap.data["bfld"][:, :] ** 2, axis=1) / (8.0 * np.pi)
    if hasattr(snap, "cren"):
        energy_density += snap.data["rho"] * snap.data["cren"]
    if no_conversion:
        UnitEnergyDensity_in_cgs = 1.0
    elif considering_h:
        UnitEnergyDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitEnergyDensity_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 3
        )
    if not returnLabel:
        return energy_density * UnitEnergyDensity_in_cgs
    else:
        return energy_density * UnitEnergyDensity_in_cgs, r"$\epsilon_{\mathrm{tot}}$"


def get_CREnergyDensityFraction(snap, returnLabel=False):
    total = get_TotalEnergyDensity(snap, considering_h=False, no_conversion=True)
    if hasattr(snap, "cren"):
        CR = snap.data["rho"] * snap.data["cren"]
    else:
        CR = np.zeros(np.shape(total))

    if not returnLabel:
        return CR / total
    else:
        return CR / total, r"$\epsilon_\mathrm{cr}/\epsilon_\mathrm{tot}$"


def get_KineticEnergyDensityFraction(snap, returnLabel=False):
    total = get_TotalEnergyDensity(snap, considering_h=False, no_conversion=True)
    kinetic = (
        snap.data["rho"]
        * (
            snap.data["vel"][snap.data["type"] == 0][:, 0] ** 2
            + snap.data["vel"][snap.data["type"] == 0][:, 1] ** 2
            + snap.data["vel"][snap.data["type"] == 0][:, 2] ** 2
        )
        / 2.0
    )
    if not returnLabel:
        return kinetic / total
    else:
        return kinetic / total, r"$\epsilon_\mathrm{kin}/\epsilon_\mathrm{tot}$"


def get_MagneticEnergyDensityFraction(snap, returnLabel=False):
    total = get_TotalEnergyDensity(snap, considering_h=False, no_conversion=True)
    magnetic = (
        snap.data["bfld"][:, 0] ** 2
        + snap.data["bfld"][:, 1] ** 2
        + snap.data["bfld"][:, 2] ** 2
    ) / (8.0 * np.pi)
    if not returnLabel:
        return magnetic / total
    else:
        return magnetic / total, r"$\epsilon_{\mathbf{B}}/\epsilon_\mathrm{tot}$"


def get_ThermalEnergyDensityFraction(snap, returnLabel=False):
    total = get_TotalEnergyDensity(snap, considering_h=False, no_conversion=True)
    thermal = snap.data["rho"] * snap.data["u"]
    if not returnLabel:
        return thermal / total
    else:
        return thermal / total, r"$\epsilon_u/\epsilon_\mathrm{tot}$"


def checkIfInDictionary(key, dic, alternative):
    if type(dic) == dict:
        if key in list(dic.keys()):
            return dic[key]
    return alternative


def getVelocityUnits(snap, INFO):
    unitsV = checkIfInDictionary("unitsV", INFO, "cgs")
    if unitsV == "cgs":
        Unit_Vel = snap.UnitVelocity_in_cm_per_s
    elif unitsV == "kms":
        Unit_Vel = 1.0
    else:
        print(unitsV)
        exit("units not yet implemented for velocities!")
    return Unit_Vel


def get_BulkVelocity(snap, no_conversion=True, INFO=None):
    JetDirection = checkIfInDictionary("JetDirection", INFO, 0)
    threshTracer = checkIfInDictionary("bulkvelothresh", INFO, 1e-3)
    center = checkIfInDictionary("center", INFO, [snap.boxsize / 2.0 for i in range(3)])
    avgBulkVelUp, avgBulkVelDown = get_BulkVelocityValue(snap, both=True, INFO=INFO)

    bulk = np.zeros(np.shape(snap.data["vel"][snap.data["type"] == 0]))
    mask = np.logical_and(
        snap.data["pos"][snap.data["type"] == 0][:, JetDirection]
        >= center[JetDirection],
        snap.data["jetr"] >= threshTracer,
    )
    for i in range(3):
        bulk[mask, i] = avgBulkVelUp[i]
    mask = np.logical_and(
        snap.data["pos"][snap.data["type"] == 0][:, JetDirection]
        < center[JetDirection],
        snap.data["jetr"] >= threshTracer,
    )
    for i in range(3):
        bulk[mask, i] = avgBulkVelDown[i]

    if no_conversion:
        Unit_Vel = 1.0
    else:
        Unit_Vel = getVelocityUnits(snap, INFO)
    return bulk * Unit_Vel


def get_AbsoluteBulkVelocity(snap, no_conversion=True, INFO=None):
    bulk = get_BulkVelocity(snap, no_conversion=True, INFO=INFO)
    bulk = np.sqrt(np.sum(bulk ** 2, axis=1))
    if no_conversion:
        Unit_Vel = 1.0
    else:
        Unit_Vel = getVelocityUnits(snap, INFO)
    return bulk * Unit_Vel


def get_TurbulentVelocity(snap, no_conversion=True, INFO=None):
    bulk = get_BulkVelocity(snap, no_conversion=True, INFO=INFO)
    turb = snap.data["vel"][snap.data["type"] == 0] - bulk
    if no_conversion:
        Unit_Vel = 1.0
    else:
        Unit_Vel = getVelocityUnits(snap, INFO)
    return turb * Unit_Vel


def get_AboluteTurbulentVelocity(snap, no_conversion=True, INFO=None):
    turb = get_TurbulentVelocity(snap, no_conversion=True, INFO=INFO)
    turb = np.sqrt(np.sum(turb ** 2, axis=1))
    if no_conversion:
        Unit_Vel = 1.0
    else:
        Unit_Vel = getVelocityUnits(snap, INFO)
    return turb * Unit_Vel


def get_BulkVelocityValue(snap, up=True, both=False, INFO=None):
    weightsVar = checkIfInDictionary("WeightsBulkVel", INFO, "Mass")
    print(("using %s to calculate the bulk velocity in the lobes!" % weightsVar))
    weights = get_value(weightsVar, snap, convert_units=False, INFO=INFO)
    JetDirection = checkIfInDictionary("JetDirection", INFO, 0)
    threshTracer = checkIfInDictionary("bulkvelothresh", INFO, 1e-3)
    center = checkIfInDictionary("center", INFO, [snap.boxsize / 2.0 for i in range(3)])

    if both:
        print("calculatuing upper and lower bulk velocity at same time!")
    maskAdd = snap.data["jetr"] >= threshTracer
    if not both:
        if up:
            mask = (
                snap.data["pos"][snap.data["type"] == 0][:, JetDirection]
                >= center[JetDirection]
            )
        else:
            mask = (
                snap.data["pos"][snap.data["type"] == 0][:, JetDirection]
                < center[JetDirection]
            )
        avgBulkVel = np.zeros(3)
        mask = np.logical_and(mask, maskAdd)
        avgBulkVel[0] = np.average(
            snap.data["vel"][snap.data["type"] == 0][mask, 0], weights=weights[mask]
        )
        avgBulkVel[1] = np.average(
            snap.data["vel"][snap.data["type"] == 0][mask, 1], weights=weights[mask]
        )
        avgBulkVel[2] = np.average(
            snap.data["vel"][snap.data["type"] == 0][mask, 2], weights=weights[mask]
        )
    else:
        maskUp = (
            snap.data["pos"][snap.data["type"] == 0][:, JetDirection]
            >= center[JetDirection]
        )
        maskUp = np.logical_and(maskUp, maskAdd)
        maskDown = (
            snap.data["pos"][snap.data["type"] == 0][:, JetDirection]
            < center[JetDirection]
        )
        maskDown = np.logical_and(maskDown, maskAdd)
        avgBulkVelUp = np.zeros(3)
        avgBulkVelUp[0] = np.average(
            snap.data["vel"][snap.data["type"] == 0][maskUp, 0], weights=weights[maskUp]
        )
        avgBulkVelUp[1] = np.average(
            snap.data["vel"][snap.data["type"] == 0][maskUp, 1], weights=weights[maskUp]
        )
        avgBulkVelUp[2] = np.average(
            snap.data["vel"][snap.data["type"] == 0][maskUp, 2], weights=weights[maskUp]
        )
        print("avgBulkVelUp")
        print(avgBulkVelUp)
        avgBulkVelDown = np.zeros(3)
        avgBulkVelDown[0] = np.average(
            snap.data["vel"][snap.data["type"] == 0][maskDown, 0],
            weights=weights[maskDown],
        )
        avgBulkVelDown[1] = np.average(
            snap.data["vel"][snap.data["type"] == 0][maskDown, 1],
            weights=weights[maskDown],
        )
        avgBulkVelDown[2] = np.average(
            snap.data["vel"][snap.data["type"] == 0][maskDown, 2],
            weights=weights[maskDown],
        )
        print("avgBulkVelDown")
        print(avgBulkVelDown)
        return avgBulkVelUp, avgBulkVelDown
    print(("average bulk velocity up %i equal to:" % up))
    print(avgBulkVel)
    return avgBulkVel


def get_BulkKineticEnergyDensity(
    snap, considering_h=False, no_conversion=False, returnLabel=False, INFO=None
):
    bulkVelo = get_AbsoluteBulkVelocity(snap, no_conversion=True, INFO=INFO)
    energy = 1.0 / 2.0 * snap.data["rho"] * bulkVelo ** 2
    if no_conversion:
        UnitEng_in_cgs = 1.0
    elif considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 3
        )
    energy = energy * UnitEng_in_cgs
    if not returnLabel:
        return energy
    else:
        return energy, r"$\epsilon_\mathrm{bulk}$"


def get_TurbulentEnergyDensity(
    snap, considering_h=False, no_conversion=False, returnLabel=False, INFO=None
):
    turbVelo = get_AboluteTurbulentVelocity(snap, no_conversion=True, INFO=INFO)
    energy = 1.0 / 2.0 * snap.data["rho"] * turbVelo ** 2
    if no_conversion:
        UnitEng_in_cgs = 1.0
    elif considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 3
        )
    energy = energy * UnitEng_in_cgs
    if not returnLabel:
        return energy
    else:
        return energy, r"$\epsilon_\mathrm{turb}$"


def get_TurbulentEnergyDensityResid(
    snap, considering_h=False, no_conversion=False, returnLabel=False, INFO=None
):
    kin = get_KineticEnergyDensity(
        snap, considering_h=False, no_conversion=True, returnLabel=False
    )
    bulk = get_BulkKineticEnergyDensity(
        snap, considering_h=False, no_conversion=True, returnLabel=False, INFO=INFO
    )
    turb = kin - bulk
    if no_conversion:
        UnitEng_in_cgs = 1.0
    elif considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 3
        )
    turb = turb * UnitEng_in_cgs
    if not returnLabel:
        return turb
    else:
        return turb, r"$\epsilon_\mathrm{turb}$"


def get_TotalEnergyDensityInc(
    snap, considering_h=False, no_conversion=False, returnLabel=False, INFO=None
):
    try:
        included = INFO["InclTotErg"]
    except:
        included = [
            "BulkKineticEnergyDensity",
            "TurbulentEnergyDensity",
            "InternalEnergyDensity",
            "MagneticEnergyDensity",
            "CosmicRayEnergyDensity",
        ]
    TIME_INI = time.time()
    total = np.zeros(np.shape(snap.data["pos"][snap.data["type"] == 0][:, 0]))
    for index, variable in enumerate(included):
        total += get_value(
            variable, snap, considering_h=False, convert_units=False, INFO=INFO
        )

        print(variable)
        print((np.max(total), np.min(total)))
        print(
            (
                get_value(
                    variable, snap, considering_h=False, convert_units=False, INFO=INFO
                )[snap.data["jetr"] > 1e-3]
            )
        )
        print((total[snap.data["jetr"] > 1e-3]))
    print(("looked at %i variables to calculate total energy density!" % (index + 1)))
    if no_conversion:
        UnitEng_in_cgs = 1.0
    elif considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 3
        )

    total = total * UnitEng_in_cgs

    print(("took %g to calculate TotalEnergyDensityInc" % (time.time() - TIME_INI)))
    if not returnLabel:
        return total
    else:
        return total, r"$\epsilon_\mathrm{tot}$"


def get_TurbEnergyDensityResidFrInc(snap, INFO=None):
    total = get_TotalEnergyDensityInc(
        snap, considering_h=False, no_conversion=True, INFO=INFO
    )
    turb = get_TurbulentEnergyDensityResid(
        snap, considering_h=False, returnLabel=False, no_conversion=True, INFO=INFO
    )
    return turb / total


def get_KineticEnergyDensityFrInc(snap, INFO=None):
    total = get_TotalEnergyDensityInc(
        snap, considering_h=False, no_conversion=True, INFO=INFO
    )
    kin = get_KineticEnergyDensity(
        snap, considering_h=False, no_conversion=True, returnLabel=False
    )
    return kin / total


def get_ThermalEnergyDensityFrInc(snap, INFO=None):
    total = get_TotalEnergyDensityInc(
        snap, considering_h=False, no_conversion=True, INFO=INFO
    )
    thermal = get_InternalEnergyDensity(
        snap,
        considering_h=False,
        where=None,
        highprecision=False,
        returnLabel=False,
        no_conversion=True,
    )
    return thermal / total


def get_MagneticEnergyDensityFrInc(snap, INFO=None):
    total = get_TotalEnergyDensityInc(
        snap, considering_h=False, no_conversion=True, INFO=INFO
    )
    magnetic = get_MagneticEnergyDensity(
        snap,
        considering_h=False,
        where=None,
        highprecision=False,
        returnLabel=False,
        no_conversion=True,
    )
    return magnetic / total


def get_TurbEnergyDensityFrInc(snap, INFO=None):
    total = get_TotalEnergyDensityInc(
        snap, considering_h=False, no_conversion=True, INFO=INFO
    )
    turb = get_TurbulentEnergyDensity(
        snap, considering_h=False, no_conversion=True, returnLabel=False, INFO=INFO
    )
    return turb / total


def get_BulkEnergyDensityFrInc(snap, INFO=None):
    total = get_TotalEnergyDensityInc(
        snap, considering_h=False, no_conversion=True, INFO=INFO
    )
    bulk = get_BulkKineticEnergyDensity(
        snap, considering_h=False, no_conversion=True, returnLabel=False, INFO=INFO
    )
    return bulk / total


def get_CREnergyDensityFrInc(snap, INFO=None):
    total = get_TotalEnergyDensityInc(
        snap, considering_h=False, no_conversion=True, INFO=INFO
    )
    cosmicray = get_CosmicRayEnergyDensity(
        snap,
        considering_h=False,
        where=None,
        highprecision=False,
        no_conversion=True,
        returnLabel=False,
    )
    return cosmicray / total


def get_KineticEnergyDensity(
    snap, considering_h=False, no_conversion=False, returnLabel=False
):
    energydensity = (
        snap.data["rho"]
        * (
            snap.data["vel"][snap.data["type"] == 0][:, 0] ** 2
            + snap.data["vel"][snap.data["type"] == 0][:, 1] ** 2
            + snap.data["vel"][snap.data["type"] == 0][:, 2] ** 2
        )
        / 2.0
    )
    if no_conversion:
        UnitEnergyDensity_in_cgs = 1.0
    elif considering_h:
        UnitEnergyDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitEnergyDensity_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 3
        )
    if not returnLabel:
        return energydensity * UnitEnergyDensity_in_cgs
    else:
        return energydensity * UnitEnergyDensity_in_cgs, r"$\epsilon_\mathrm{kin}$"


def get_KineticEnergy(
    snap, considering_h=False, no_conversion=False, returnLabel=False
):
    energydensity = (
        snap.data["rho"]
        * np.sum((snap.data["vel"][snap.type == 0]) ** 2, axis=1)
        / 2.0
        * snap.data["vol"]
    )
    if no_conversion:
        UnitEnergyDensity_in_cgs = 1.0
    elif considering_h:
        UnitEnergy_in_cgs = (
            snap.UnitMass_in_g / snap.hubbleparam * snap.UnitVelocity_in_cm_per_s ** 2
        )
    else:
        UnitEnergy_in_cgs = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    if not returnLabel:
        return energydensity * UnitEnergy_in_cgs
    else:
        return energydensity * UnitEnergy_in_cgs, r"$E_\mathrm{kin}$"


def get_U(snap, convert_units=True, considering_h=False):
    # calculates specfic internal energy in (cm/s)**2
    internal = snap.data["u"]
    if convert_units:
        internal = internal * snap.UnitVelocity_in_cm_per_s ** 2
    return internal


def get_XKin(snap, considering_h=False, returnunits=False):
    ratio = (
        1.0
        / 2.0
        * get_AbsoluteVelocity(snap, no_conversion=True) ** 2
        / (get_U(snap, convert_units=False) * 2.0 / 3.0)
    )
    if not returnunits:
        return ratio
    else:
        return ratio, " "


def get_XCR(snap, considering_h=False, returnunits=False):
    ratio = 1.0 / 3.0 * get_Cren(snap, change=False) / (get_U(snap) * 2.0 / 3.0)
    if not returnunits:
        return ratio
    else:
        return ratio, " "


def get_CosmicRayMagneticPressureRatio(snap):
    ratio = (
        snap.data["cren"]
        * snap.data["rho"]
        * 1.0
        / 3.0
        / (
            (
                snap.data["bfld"][:, 0] ** 2
                + snap.data["bfld"][:, 1] ** 2
                + snap.data["bfld"][:, 2] ** 2
            )
            / (8.0 * np.pi)
        )
    )
    return ratio


def get_XB(snap, considering_h=False, returnunits=False):
    ratio = (
        get_AbsoluteMagneticField(snap, no_conversion=True) ** 2
        / (8.0 * np.pi)
        / (
            get_Density(snap, no_conversion=True)
            * get_U(snap, convert_units=False)
            * 2.0
            / 3.0
        )
    )
    if not returnunits:
        return ratio
    else:
        return ratio, " "


def get_DivBRatio(snap, considering_h=False):
    bfield = np.sqrt(snap.bfld[:, 0] ** 2 + snap.bfld[:, 1] ** 2 + snap.bfld[:, 2] ** 2)
    mask = [bfield != 0]
    ratio = np.ones(np.shape(bfield))
    # ratio[mask] = snap.MagneticFieldDivergence[mask] / (
    #     bfield[mask] / snap.vol[mask] ** (1.0 / 3.0)
    # )
    ratio[mask] = snap.divb[mask] / (bfield[mask] / snap.vol[mask] ** (1.0 / 3.0))
    return abs(ratio)


def get_MagneticRamPressureRatio(snap, considering_h=False):
    if any(
        (
            snap.data["bfld"][:, 0] ** 2
            + snap.data["bfld"][:, 1] ** 2
            + snap.data["bfld"][:, 2] ** 2
        )
        == 0
    ):
        exit("have fields with zero magnetic field, would divide by 0")
    ratio = (
        (
            snap.data["bfld"][:, 0] ** 2
            + snap.data["bfld"][:, 1] ** 2
            + snap.data["bfld"][:, 2] ** 2
        )
        / (8.0 * np.pi)
    ) / (
        snap.data["rho"]
        * (
            snap.data["vel"][snap.data["type"] == 0][:, 0] ** 2
            + snap.data["vel"][snap.data["type"] == 0][:, 1] ** 2
            + snap.data["vel"][snap.data["type"] == 0][:, 2] ** 2
        )
    )
    return ratio


def get_CosmicRayPressure(
    snap, considering_h=False, no_conversion=False, returnLabel=False
):
    pressure = (
        1.0
        / 3.0
        * get_CosmicRayEnergyDensity(
            snap,
            where=None,
            highprecision=False,
            considering_h=considering_h,
            no_conversion=no_conversion,
        )
    )
    if returnLabel:
        return pressure, r"$P_\mathrm{cr}$"
    return pressure


def get_AlfvenVelocity(snap, considering_h=False, no_conversion=False, INFO=None):
    alfven = get_AbsoluteMagneticField(
        snap, considering_h=considering_h, no_conversion=True
    ) / np.sqrt(
        4.0 * np.pi * get_Density(snap, considering_h=considering_h, no_conversion=True)
    )
    if no_conversion:
        Unit_Velocity = 1.0
    else:
        Unit_Velocity = get_UnitsVelocity(snap, INFO)
    alfven *= Unit_Velocity
    return alfven


def get_VelocityToAlfvenVelocityRatio(snap):
    ratio = np.zeros(np.shape(snap.data["rho"]))
    alfven = get_AlfvenVelocity(snap, considering_h=False, no_conversion=True)
    velocity = get_AbsoluteVelocity(
        snap, considering_h=False, INFO=None, no_conversion=True
    )
    mask = [alfven != 0]
    ratio[mask] = (velocity / alfven)[mask]
    return ratio


def get_AlfvenVelocityinkms(snap, considering_h=False, no_conversion=False):
    alfven = get_AbsoluteMagneticField(
        snap, considering_h=considering_h, no_conversion=True
    ) / np.sqrt(
        4.0 * np.pi * get_Density(snap, considering_h=considering_h, no_conversion=True)
    )
    if no_conversion:
        Unit_Velocity = 1.0
    else:
        Unit_Velocity = snap.UnitVelocity_in_cm_per_s / 1.0e5
    alfven *= Unit_Velocity
    return alfven


def get_AlfvenMachnumber(snap):
    ratio = np.sqrt(
        (snap.data["vel"][snap.data["type"] == 0][:, :] ** 2).sum(axis=1)
    ) / (
        np.sqrt((snap.data["bfld"][:, :] ** 2).sum(axis=1))
        / (4.0 * np.pi * snap.data["rho"])
    )
    return ratio


def get_AbsoluteCosmicRayPressureGradient(
    snap, considering_h=False, no_conversion=False
):
    if no_conversion:
        UnitPressureGradient_in_cgs = 1.0
    elif considering_h:
        UnitPressureGradient_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 4.0
            * snap.hubbleparam ** 4
        )
    else:
        UnitPressureGradient_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 4
        )
    gradient = (
        np.sqrt(
            snap.data["crpg"].astype(np.float64)[:, 0] ** 2
            + snap.data["crpg"].astype(np.float64)[:, 1] ** 2
            + snap.data["crpg"].astype(np.float64)[:, 2] ** 2
        )
        * UnitPressureGradient_in_cgs
    )
    return gradient


def get_CosmicRayPressureGradient(snap, considering_h=False, no_conversion=False):
    if no_conversion:
        UnitPressureGradient_in_cgs = 1.0
    elif considering_h:
        UnitPressureGradient_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 4.0
            * snap.hubbleparam ** 4
        )
    else:
        UnitPressureGradient_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 4
        )
    gradient = snap.data["crpg"].copy() * UnitPressureGradient_in_cgs
    return gradient


def get_AbsolutePressureGradient(snap, considering_h=False, no_conversion=False):
    if no_conversion:
        UnitPressureGradient_in_cgs = 1.0
    elif considering_h:
        UnitPressureGradient_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 4.0
            * snap.hubbleparam ** 4
        )
    else:
        UnitPressureGradient_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 4
        )
    gradient = (
        np.sqrt(
            np.sum(snap.data["PressureGradient"][:, :].astype(np.float64) ** 2, axis=1)
        )
        * UnitPressureGradient_in_cgs
    )
    return gradient


def get_PressureGradient(snap, considering_h=False, no_conversion=False):
    if no_conversion:
        UnitPressureGradient_in_cgs = 1.0
    elif considering_h:
        UnitPressureGradient_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 4.0
            * snap.hubbleparam ** 4
        )
    else:
        UnitPressureGradient_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 4
        )
    gradient = snap.data["PressureGradient"].copy() * UnitPressureGradient_in_cgs
    return gradient


def get_CosmicRayLengthScale(snap, considering_h=False):
    gradient = get_AbsoluteCosmicRayPressureGradient(
        snap, considering_h=considering_h, no_conversion=True
    )
    pressure = get_CosmicRayPressure(
        snap, considering_h=considering_h, no_conversion=True
    )
    if considering_h:
        Unit_Length = snap.UnitLength_in_cm / snap.hubbleparam
    else:
        Unit_Length = snap.UnitLength_in_cm
    length = np.zeros(np.shape(gradient))
    mask = gradient != 0
    length[mask] = pressure[mask] / gradient[mask]
    print((np.max(length)))
    # exit()
    return length * Unit_Length


def get_VelocityGradient(snap, considering_h=False, no_conversion=False):
    gradient = (
        snap.data["VelocityGradient"][:, 0]
        + snap.data["VelocityGradient"][:, 4]
        + snap.data["VelocityGradient"][:, 8]
    )
    if no_conversion:
        UnitVeloGradient_in_cgs = 1.0
    elif considering_h:
        UnitVeloGradient_in_cgs = (
            snap.UnitVelocity_in_cm_per_s / snap.UnitLength_in_cm * snap.hubbleparam
        )
    else:
        UnitVeloGradient_in_cgs = snap.UnitVelocity_in_cm_per_s / snap.UnitLength_in_cm
    return gradient * UnitVeloGradient_in_cgs


def get_VelocityLengthScale(snap, no_conversion=False, considering_h=False):
    gradient = get_VelocityGradient(snap, considering_h=False, no_conversion=True)
    velocity = get_AbsoluteVelocity(
        snap, considering_h=False, INFO=None, no_conversion=True
    )
    if no_conversion:
        Unit_Length = 1.0
    elif considering_h:
        Unit_Length = snap.UnitLength_in_cm / snap.hubbleparam
    else:
        Unit_Length = snap.UnitLength_in_cm
    length = np.zeros(np.shape(gradient))
    mask = gradient != 0
    length[mask] = velocity[mask] / gradient[mask]
    print((np.max(length)))
    # exit()
    return length * Unit_Length ** 2


def get_JeansLengthInKpc(snap, no_conversion=False, considering_h=False):
    if no_conversion:
        Unit_Length = 1.0
    elif considering_h:
        Unit_Length = snap.UnitLength_in_cm / snap.hubbleparam
    else:
        Unit_Length = snap.UnitLength_in_cm
    length = get_SoundSpeed(snap, convert_units=True) / np.sqrt(
        get_Density(snap, no_conversion=False) * param.GRAVITATIONALCONSTANT_cgs
    )
    return length / param.KiloParsec_in_cm


def get_CellRadiusInKpc(snap):
    radius = snap.vol ** (1 / 3)
    return radius * snap.UnitLength_in_cm / param.KiloParsec_in_cm


def get_VorticityperMyr2Approx(snap, considering_h=False, convert_units=False):
    veloLengthScale2 = (
        get_VelocityLengthScale(snap, no_conversion=True, considering_h=False) ** 2
    )
    mask = veloLengthScale2 != 0
    vorti = np.zeros(np.shape(veloLengthScale2))
    vorti[mask] = (
        get_AbsoluteVelocity(snap, considering_h=False, INFO=None, no_conversion=True)[
            mask
        ]
        ** 2
        / veloLengthScale2[mask]
    )
    if not convert_units:
        Unit_InvVelo2 = 1.0
    elif considering_h:
        Unit_InvVelo2 = (1.022e-3 * 1e-3 * snap.hubbleparam) ** 2
    else:
        Unit_InvVelo2 = (1.022e-3 * 1e-3) ** 2
    return vorti * Unit_InvVelo2


def get_CosmicRayLengthScaleMask(snap, considering_h=False):
    gradient = get_AbsoluteCosmicRayPressureGradient(
        snap, considering_h=considering_h, no_conversion=True
    )
    pressure = get_CosmicRayPressure(
        snap, considering_h=considering_h, no_conversion=True
    )
    if considering_h:
        Unit_Length = snap.UnitLength_in_cm / snap.hubbleparam
    else:
        Unit_Length = snap.UnitLength_in_cm
    length = np.zeros(np.shape(gradient))
    mask2 = gradient != 0
    # length[mask] =  pressure[mask]/ gradient[mask]
    mask1 = (
        snap.data["rho"]
        * snap.data["cren"]
        * snap.UnitMass_in_g
        / snap.hubbleparam
        * snap.UnitVelocity_in_cm_per_s ** 2
        / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        > 1e-14
    )
    mask = np.logical_and(mask1, mask2)
    length[mask] = pressure[mask] / gradient[mask]
    print((np.max(length)))
    # exit()
    return length * Unit_Length


def get_CRDiffusionCoefficient(snap, considering_h=False):
    coefficient = get_AlfvenVelocity(snap, considering_h=considering_h) * (
        get_CosmicRayLengthScale(snap, considering_h=considering_h)
    )
    # use mask to insure that CR density in counted cells is significant and not solemly due to nummerical diffusion
    Coefficient = np.zeros(np.shape(snap.data["vol"]))
    mask = (
        snap.data["rho"]
        * snap.data["cren"]
        * snap.UnitMass_in_g
        / snap.hubbleparam
        * snap.UnitVelocity_in_cm_per_s ** 2
        / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        > 1e-14
    )
    Coefficient[mask] = coefficient[mask]
    return Coefficient


def get_AlfvenCoolingTimescale_inMyr(snap, considering_h=False):
    cooling = get_CosmicRayLengthScale(
        snap, considering_h=considering_h
    ) / get_AlfvenVelocity(snap, considering_h=considering_h)
    cooling /= 3600.0 * 24.0 * 365.0 * 1.0e6
    # use mask to insure that CR density in counted cells is significant and not solemly due to nummerical diffusion
    Cooling = np.zeros(np.shape(snap.data["vol"]))
    mask = (
        snap.data["rho"]
        * snap.data["cren"]
        * snap.UnitMass_in_g
        / snap.hubbleparam
        * snap.UnitVelocity_in_cm_per_s ** 2
        / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        > 1e-14
    )
    Cooling[mask] = cooling[mask]
    return Cooling


def get_XrayCoolingRate(snap, considering_h=False):
    # taken from 1st paper of Jacob 2017
    # equivalent to get_XrayEmissivity
    # Lambda0 = 1.2e-23  # erg cm^3/s
    # Lambda1 = 1.8e-27  # erg cm^3/s /K^{1/2}
    # rate = get_ElectronNumberDensity(
    #     snap, considering_h=considering_h, no_conversion=False
    # ) ** 2 * (
    #     Lambda0
    #     + Lambda1
    #     * get_Temperature(snap, considering_h=considering_h, no_conversion=False)
    #     ** (1.0 / 2.0)
    # )
    rate = get_XrayEmissivity(snap, considering_h=considering_h, no_conversion=False, INFO=None)
    return rate  # erg /s /cm^3


def get_XrayCoolingRateWithinKpc(snap, INFO=None, considering_h=False):
    # Sum ( (luminosity_xray) [ r < ColdGasRadiusInKpc] ) in erg s^-1
    cold_gas_radius_in_kpc = checkIfInDictionary("ColdGasRadiusInKpc", INFO, None)
    if cold_gas_radius_in_kpc is None:
        raise Exception(
            f"need to define radius: cold_gas_radius_in_kpc:{cold_gas_radius_in_kpc}"
            " to calculate total xray luminosity within!"
        )
    mask = get_DistanceToCenterInKpc(snap) < cold_gas_radius_in_kpc
    luminosity = get_XrayLuminosity(
        snap, considering_h=considering_h, no_conversion=False
    )
    luminosity = luminosity[mask]
    return np.sum(luminosity)  # erg /s


def get_CoolingLuminosityWithinKpc(snap, info=None, considering_h=False, verbose=1):
    # cooling rate taken from simulation in erg s^-1
    cold_gas_radius_in_kpc = checkIfInDictionary("ColdGasRadiusInKpc", info, None)
    if cold_gas_radius_in_kpc is None:
        raise Exception(
            f"need to define radius: cold_gas_radius_in_kpc:{cold_gas_radius_in_kpc}"
            " to calculate total xray luminosity within!"
        )
    rate = get_CoolingLuminosity(snap, verbose=verbose, INFO=info)
    mask = get_DistanceToCenterInKpc(snap) < cold_gas_radius_in_kpc
    rate = rate[mask]
    return np.sum(rate)  # erg /s


def get_XrayCoolingTimescaleInMyr(snap, considering_h=False):
    cooling = get_InternalEnergyDensity(
        snap, considering_h=considering_h, no_conversion=False
    ) / get_XrayCoolingRate(snap)
    cooling /= 3600.0 * 24.0 * 365.0 * 1.0e6  # convert s to Myr
    return cooling


def get_CoolingRate(snap, verbose=1, INFO=None):
    # emissivity like cooling rate computed from cooling rate used in simulation if available
    # in erg / cm^3 / s
    try:
        if verbose:
            print("get_CoolingRate: taking cooling rate from simulation")
        # snap.gcol given in cm^3 erg/s
        gcol = snap.gcol
        rate = gcol * -1 * get_HydrogenNumberDensity(snap) ** 2
        if len(rate[rate <= 0]):
            warnings.warn(
                f"found {len(rate[rate<=0])} cells with negative cooling rate, "
                f"setting to non-negative value ({np.min(rate[rate>0])})!"
            )
            rate[rate <= 0] = np.min(rate[rate > 0])
        # [gcol] = erg cm^3 / s
    except AttributeError:
        if verbose:
            print("get_CoolingRate: using approximate X-ray cooling rate")
        rate = get_XrayCoolingRate(snap)
    return rate  # [rate] = erg / cm^3 / s


def get_CoolingEmissivity(snap, verbose=1, INFO=None):
    # cooling emissivity taken from snapshot if available erg / cm^3 / s
    # equivalent to get_CoolingRate
    return get_CoolingRate(snap, verbose=verbose, INFO=INFO)


def get_CoolingLuminosity(snap, verbose=1, INFO=None):
    # cooling taken from snapshot if available in erg /s
    unit_vol = snap.UnitLength_in_cm ** 3
    emissivity = get_CoolingEmissivity(snap, verbose=verbose, INFO=INFO)
    return emissivity * snap.vol * unit_vol


def get_CoolingEnergyRate(
    snap, verbose=1, INFO=None
):
    # equivalent to get_CoolingLuminosity
    unit_volume = snap.UnitLength_in_cm ** 3
    c = (
        get_CoolingRate(snap, verbose=verbose, INFO=INFO)
        * get_Volume(snap, convert_units=False)
        * unit_volume
    )
    return c  # [erg/s]


def get_CosmicRayHeatingRateCoolingRateRatio(snap, INFO=None):
    cooling = get_CoolingRate(snap, INFO=INFO)
    heating = get_CosmicRayHeatingRate(snap, no_conversion=False)
    ratio = heating / cooling
    return ratio


def get_PressureEquilbriumCondensationCriteriumCellRadiusInKpc(snap):
    # criterion taken from Fielding+2020:Multiphase Gas and the Fractal Nature of Radiative Turbulent Mixing Layers
    # delta x < c_s * t_cool
    c_s = get_SoundSpeed(snap, convert_units=True)
    t_cool = get_CoolingTimeInMyr(snap) * param.SEC_PER_MEGAYEAR
    delta_x = c_s * t_cool / param.KiloParsec_in_cm
    return delta_x


def get_CoolingTimeInMyr(snap, INFO=None):
    time = get_InternalEnergyDensity(snap) / get_CoolingRate(snap, INFO=INFO)
    time /= param.SEC_PER_MEGAYEAR
    return time


def get_CoolingFreeFallTimeRatio(snap, considering_h=False, INFO=None, verbose=1):
    tcool = get_CoolingTimeInMyr(snap, INFO=INFO,)
    tff = get_FreeFallTimeInMyr(snap, considering_h=considering_h)
    return tcool / tff


def get_CosmicRayEnergy(
    snap,
    considering_h=False,
    no_conversion=False,
    returnLabel=False,
    highprecision=False,
):
    if not highprecision:
        energy = snap.data["cren"] * snap.data["mass"][snap.data["type"] == 0]
    else:
        energy = snap.data["cren"].astype(np.float128) * snap.data["mass"][
            snap.data["type"] == 0
        ].astype(np.float128)
    if no_conversion:
        UnitEng_in_cgs = 1.0
    elif considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g / snap.hubbleparam * snap.UnitVelocity_in_cm_per_s ** 2
        )
    else:
        UnitEng_in_cgs = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    energy = energy * UnitEng_in_cgs

    # exit()
    if not returnLabel:
        return energy
    else:
        return energy, r"$E_\mathrm{cr}$"


def get_TotalCosmicRayHeatingRate(snap, considering_h=False, no_conversion=False):
    print("considering jet mass fraction")
    heating = (
        get_AlfvenVelocity(snap, considering_h=considering_h, no_conversion=True)
        * get_AbsoluteCosmicRayPressureGradient(
            snap, considering_h=considering_h, no_conversion=True
        )
        * snap.data["vol"][:]
    )
    heating = np.zeros(np.shape(snap.data["vol"]))
    heating[snap.data["jetr"] < 1e-3] = (
        get_AlfvenVelocity(snap, considering_h=considering_h, no_conversion=True)[
            snap.data["jetr"] < 1e-3
        ]
        * get_AbsoluteCosmicRayPressureGradient(
            snap, considering_h=considering_h, no_conversion=True
        )[snap.data["jetr"] < 1e-3]
        * snap.data["vol"][:][snap.data["jetr"] < 1e-3]
    )
    if no_conversion:
        UnitPowerDensity_in_cgs = 1.0
    elif considering_h:
        UnitPowerDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 1.0
            * snap.hubbleparam ** 1
        )
    else:
        UnitPowerDensity_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 1
        )
    return heating * UnitPowerDensity_in_cgs


def get_MagneticFieldEnergy(
    snap,
    considering_h=False,
    no_conversion=False,
    returnLabel=False,
    highprecision=False,
):
    if not highprecision:
        energy = (
            get_AbsoluteMagneticField(snap, no_conversion=True) ** 2
            / (8.0 * np.pi)
            * get_Volume(snap, convert_units=False)
        )
    else:
        energy = (
            get_AbsoluteMagneticField(
                snap, no_conversion=True, highprecision=highprecision
            )
            ** 2
            / (8.0 * np.pi)
            * get_Volume(snap, convert_units=False, highprecision=highprecision)
        )
    if no_conversion:
        UnitEng_in_cgs = 1.0
    elif considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g / snap.hubbleparam * snap.UnitVelocity_in_cm_per_s ** 2
        )
    else:
        UnitEng_in_cgs = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    energy = energy * UnitEng_in_cgs

    if not returnLabel:
        return energy
    else:
        return energy, r"$E_{B}$"


def get_KineticEnergy(
    snap, considering_h=False, no_conversion=False, returnLabel=False
):

    energy = (
        1.0
        / 2.0
        * snap.data["mass"][snap.data["type"] == 0]
        * np.sum(snap.data["vel"][snap.data["type"] == 0][:, :] ** 2, axis=1)
    )

    if no_conversion:
        UnitEng_in_cgs = 1.0
    elif considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g / snap.hubbleparam * snap.UnitVelocity_in_cm_per_s ** 2
        )
    else:
        UnitEng_in_cgs = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    energy = energy * UnitEng_in_cgs

    if not returnLabel:
        return energy
    else:
        return energy, r"$E_\mathrm{kin}$"


def get_MagneticEnergy(
    snap, considering_h=False, no_conversion=False, returnLabel=False
):
    energy = (
        1.0
        / (8.0 * np.pi)
        * snap.data["vol"]
        * np.sum(snap.data["bfld"][:, :] ** 2, axis=1)
    )
    if no_conversion:
        UnitEng_in_cgs = 1.0
    elif considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g / snap.hubbleparam * snap.UnitVelocity_in_cm_per_s ** 2
        )
    else:
        UnitEng_in_cgs = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    energy = energy * UnitEng_in_cgs
    if not returnLabel:
        return energy
    else:
        return energy, r"$E_{B}$"


def get_TurbulentEnergyResid(
    snap, considering_h=False, no_conversion=False, returnLabel=False, INFO=None
):
    kin = get_KineticEnergy(
        snap, considering_h=False, no_conversion=True, returnLabel=False
    )
    bulk = (
        get_BulkKineticEnergyDensity(
            snap, considering_h=False, no_conversion=True, returnLabel=False, INFO=INFO
        )
        * snap.data["vol"][:]
    )
    turb = kin - bulk
    if no_conversion:
        UnitEng_in_cgs = 1.0
    elif considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g / snap.hubbleparam * snap.UnitVelocity_in_cm_per_s ** 2
        )
    else:
        UnitEng_in_cgs = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    turb = turb * UnitEng_in_cgs
    if not returnLabel:
        return turb
    else:
        return turb, r"$E_\mathrm{turb}$"


def get_TotalEnergy(snap, considering_h=False, no_conversion=False, returnLabel=False):
    # magnetic, kinetic, thermal, CR energy
    energy = (
        (snap.data["bfld"][:, :] ** 2).sum(axis=1) / (8.0 * np.pi) * snap.data["vol"]
        + snap.data["mass"][snap.data["type"] == 0]
        * (snap.data["vel"][snap.data["type"] == 0] ** 2).sum(axis=1)
        / 2.0
        + snap.data["mass"][snap.data["type"] == 0] * snap.data["u"]
        + snap.data["cren"] * snap.data["mass"][snap.data["type"] == 0]
    )

    if no_conversion:
        UnitEng_in_cgs = 1.0
    elif considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g / snap.hubbleparam * snap.UnitVelocity_in_cm_per_s ** 2
        )
    else:
        UnitEng_in_cgs = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    energy = energy * UnitEng_in_cgs

    if not returnLabel:
        return energy
    else:
        return energy, r"$E_\mathrm{tot}$"


def get_CosmicRayHeatingRate(snap, considering_h=False, no_conversion=False):
    # units: g (cm/2)**2 1/s 1/cm**3 = erg/s/cm**3
    heating = np.abs(
        snap.data["bfld"][:, 0] * snap.data["crpg"][:, 0]
        + snap.data["bfld"][:, 1] * snap.data["crpg"][:, 1]
        + snap.data["bfld"][:, 2] * snap.data["crpg"][:, 2]
    ) / np.sqrt(4.0 * np.pi * snap.data["rho"])
    if no_conversion:
        UnitPowerDensity_in_cgs = 1.0
    elif considering_h:
        UnitPowerDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 4.0
            * snap.hubbleparam ** 4
        )
    else:
        UnitPowerDensity_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 4
        )
    return heating * UnitPowerDensity_in_cgs


def get_CosmicRayHeatingRateActive(
    snap, considering_h=False, no_conversion=False, INFO=None
):
    heating = np.zeros(np.shape(snap.data["vol"]))
    try:
        thresholdTracer = INFO["TracerThresholdBuble"]
    except:
        thresholdTracer = 1e-3
    mask = snap.data["jetr"] < thresholdTracer
    heating[mask] = get_CosmicRayHeatingRate(
        snap, considering_h=False, no_conversion=True
    )[mask]
    if no_conversion:
        UnitPowerDensity_in_cgs = 1.0
    elif considering_h:
        UnitPowerDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 4.0
            * snap.hubbleparam ** 4
        )
    else:
        UnitPowerDensity_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 4
        )
    return heating * UnitPowerDensity_in_cgs


def get_CosmicRayHeatingRateActiveJFr0p05(
    snap, considering_h=False, no_conversion=False
):
    heating = np.zeros(np.shape(snap.data["vol"]))
    mask = snap.data["jetr"] < 0.05
    heating[mask] = get_CosmicRayHeatingRate(
        snap, considering_h=False, no_conversion=True
    )[mask]
    if no_conversion:
        UnitPowerDensity_in_cgs = 1.0
    elif considering_h:
        UnitPowerDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 4.0
            * snap.hubbleparam ** 4
        )
    else:
        UnitPowerDensity_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 4
        )
    return heating * UnitPowerDensity_in_cgs


def get_CosmicRayHeatingRateActiveJFr0p01(
    snap, considering_h=False, no_conversion=False
):
    heating = np.zeros(np.shape(snap.data["vol"]))
    mask = snap.data["jetr"] < 0.01
    heating[mask] = get_CosmicRayHeatingRate(
        snap, considering_h=False, no_conversion=True
    )[mask]
    if no_conversion:
        UnitPowerDensity_in_cgs = 1.0
    elif considering_h:
        UnitPowerDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 4.0
            * snap.hubbleparam ** 4
        )
    else:
        UnitPowerDensity_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 4
        )
    return heating * UnitPowerDensity_in_cgs


def get_CosmicRayHeatingRateActiveJFr0p5(
    snap, considering_h=False, no_conversion=False
):
    heating = np.zeros(np.shape(snap.data["vol"]))
    mask = snap.data["jetr"] < 0.5
    heating[mask] = get_CosmicRayHeatingRate(
        snap, considering_h=False, no_conversion=True
    )[mask]
    if no_conversion:
        UnitPowerDensity_in_cgs = 1.0
    elif considering_h:
        UnitPowerDensity_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 4.0
            * snap.hubbleparam ** 4
        )
    else:
        UnitPowerDensity_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 4
        )
    return heating * UnitPowerDensity_in_cgs


def get_ThermalEnergy(
    snap,
    considering_h=False,
    no_conversion=False,
    returnLabel=False,
    highprecision=False,
):
    if not highprecision:
        energy = snap.data["u"] * snap.data["mass"][snap.data["type"] == 0]
    else:
        energy = snap.data["u"].astype(np.float128) * snap.data["mass"][
            snap.data["type"] == 0
        ].astype(np.float128)
    if no_conversion:
        UnitEng_in_cgs = 1.0
    elif considering_h:
        UnitEng_in_cgs = (
            snap.UnitMass_in_g / snap.hubbleparam * snap.UnitVelocity_in_cm_per_s ** 2
        )
    else:
        UnitEng_in_cgs = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    energy = energy * UnitEng_in_cgs
    if not returnLabel:
        return energy
    else:
        return energy, r"$E_u$"


def get_AbsoluteMomentumFlux(snap, considering_h=False, no_conversion=False):
    flux = (
        snap.data["rho"]
        * np.sum(snap.vel[:, :][snap.data["type"] == 0] ** 2, axis=1)
        / 2.0
    )
    if no_conversion:
        UnitFlux_in_cgs = 1.0
    elif considering_h:
        UnitFlux_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitFlux_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 3
        )
    flux *= UnitFlux_in_cgs
    return flux


def get_SqrtAbsoluteMomentumFlux(snap, considering_h=False, no_conversion=False):
    flux = (
        snap.data["rho"]
        * np.sum(snap.vel[:, :][snap.data["type"] == 0] ** 2, axis=1)
        / 2.0
    )
    if no_conversion:
        UnitFlux_in_cgs = 1.0
    elif considering_h:
        UnitFlux_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitFlux_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 3
        )
    flux *= UnitFlux_in_cgs
    return np.sqrt(flux)


def get_AbsoluteKineticEnergyFlux(snap, considering_h=False, no_conversion=False):
    flux = snap.data["rho"] * (
        snap.vel[:, 0] ** 2 + snap.vel[:, 1] ** 2 + snap.vel[:, 2] ** 2
    ) ** (3.0 / 2.0)
    if no_conversion:
        UnitFlux_in_cgs = 1.0
    elif considering_h:
        UnitFlux_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 3
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitFlux_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 3
        )
    flux *= UnitFlux_in_cgs
    return flux


def get_MomentumFlux(snap, considering_h=False, no_conversion=False):
    flux = [
        snap.data["rho"]
        * np.sqrt(
            snap.data["vel"][snap.data["type"] == 0][:, 0] ** 2
            + snap.data["vel"][snap.data["type"] == 0][:, 1] ** 2
            + snap.data["vel"][snap.data["type"] == 0][:, 2] ** 2
        )
        * x
        for x in [
            snap.data["vel"][snap.data["type"] == 0].copy()[:, 0],
            snap.data["vel"][snap.data["type"] == 0].copy()[:, 1],
            snap.data["vel"][snap.data["type"] == 0].copy()[:, 2],
        ]
    ]
    flux = np.array(flux).T
    if no_conversion:
        UnitFlux_in_cgs = 1.0
    elif considering_h:
        UnitFlux_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 2
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitFlux_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 2
            / snap.UnitLength_in_cm ** 3
        )
    flux *= UnitFlux_in_cgs
    return flux


def get_KineticEnergyFlux(snap, considering_h=False, no_conversion=False):
    flux = [
        snap.data["rho"]
        * (
            snap.data["vel"][snap.data["type"] == 0][:, 0] ** 2
            + snap.data["vel"][snap.data["type"] == 0][:, 1] ** 2
            + snap.data["vel"][snap.data["type"] == 0][:, 2] ** 2
        )
        * x
        for x in [
            snap.data["vel"][snap.data["type"] == 0].copy()[:, 0],
            snap.data["vel"][snap.data["type"] == 0].copy()[:, 1],
            snap.data["vel"][snap.data["type"] == 0].copy()[:, 2],
        ]
    ]
    flux = np.array(flux).T
    if no_conversion:
        UnitFlux_in_cgs = 1.0
    elif considering_h:
        UnitFlux_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s ** 3
            / (snap.UnitLength_in_cm / snap.hubbleparam) ** 3
        )
    else:
        UnitFlux_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s ** 3
            / snap.UnitLength_in_cm ** 3
        )
    flux *= UnitFlux_in_cgs
    return flux


def get_AbsoluteMomentum(snap, considering_h=False, no_conversion=True):
    momentum = snap.data["mass"][snap.data["type"] == 0] * np.sqrt(
        snap.vel[:, 0] ** 2 + snap.vel[:, 1] ** 2 + snap.vel[:, 2] ** 2
    )
    if no_conversion:
        UnitMomentum_in_cgs = 1.0
    elif considering_h:
        UnitMomentum_in_cgs = (
            snap.UnitMass_in_g / snap.hubbleparam * snap.UnitVelocity_in_cm_per_s
        )
    else:
        UnitMomentum_in_cgs = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s
    momentum *= UnitMomentum_in_cgs
    return momentum


def get_NormalizedDrapingLayerThickness(snap):
    # draping layer thickness as given in Dursi+CP 2008, Eq.2 normalized to bubble radius
    thick = 1.0 / (
        6.0
        * get_MagneticRamPressureRatio(snap, considering_h=False)
        * get_AlfvenMachnumber(snap) ** 2
    )
    return thick


def betai(a, b, x):
    return betainc(a, b, x) * beta(a, b)


def delta(alpha):
    return 0.14 * alpha ** (-1.6) + 0.44


def sigmapp(alpha, mbarn):
    return 32.0 * (0.96 + np.exp(4.4 - 2.4 * alpha)) * mbarn


def Cp(eps, alpha, q, mp, c):
    bracket = 0.5 * betai(
        (alpha - 2.0) / 2.0, (3.0 - alpha) / 2.0, 1.0 / (1 + q ** 2)
    ) + q ** (1.0 - alpha) * (np.sqrt(1.0 + q ** 2) - 1.0)
    return eps * (alpha - 1) / (mp * c ** 2 * bracket)


def get_SynchrotronRadiation(snap, INFO):
    UnitLength_in_cm = snap.UnitLength_in_cm / snap.hubbleparam
    UnitMass_in_g = snap.UnitMass_in_g / snap.hubbleparam
    UnitVelocity_in_cm_per_s = snap.UnitVelocity_in_cm_per_s
    UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s
    UnitDensity_in_cgs = UnitMass_in_g / UnitLength_in_cm ** 3
    UnitPressure_in_cgs = UnitMass_in_g / UnitLength_in_cm / UnitTime_in_s ** 2
    UnitEnergy_in_cgs = UnitMass_in_g * UnitLength_in_cm ** 2 / UnitTime_in_s ** 2

    parsec = 3.086e18
    eV = 1.60217733e-12
    MeV = 1e6 * eV
    GeV = 1e9 * eV
    barn = 1e-24
    mbarn = 1e-27
    sigmaT = 0.66524616 * barn
    erg_per_cm2_in_mJy = 1e26
    sterad_in_arcmin2 = (2.0 * np.pi / (360.0 * 60.0)) ** 2.0

    c = 3.0e10
    KB = 1.38064852e-16

    me = 9.10939e-28
    mp = 1.67262e-24
    mpi0 = 134.9734 * MeV / c ** 2
    Xh = 0.24
    qe = 4.80320425e-10  # electron charge
    H = 6.6260755e-27  # planck constant
    xe = 1.157  # nx/nH electron to hydrogen abundance fraction

    freq = 1e9  # in Hz
    wavelength = c / freq  # in cm
    w = 2.0 * np.pi * freq
    alpha = 2.05
    q = 0.5

    projAxis = INFO["ProjAxis"]
    #    proj = INFO['proj']

    snap.centerat(snap.center)  # ??? dnagerous

    if projAxis == 1:
        bxz = np.sqrt((snap.data["bfld"][:, ::2] ** 2.0).sum(axis=projAxis)) * np.sqrt(
            UnitPressure_in_cgs
        )
    else:
        exit(
            "synchrotron radiation for other axis not implemented, have to change magnetic field first"
        )
    Bsquared = (snap.data["bfld"][:, :] ** 2.0).sum(axis=projAxis)
    epsB = Bsquared * UnitPressure_in_cgs / (8.0 * np.pi)
    epsCMB = (
        (np.pi) ** 2 / 15.0 * (KB * 2.725) ** 4 / (H / 2.0 / np.pi * c) ** 3
    )  # * (1+z)^4 for cosmological scaling
    # assuming secondaries responsible for synchrotron radiation:
    nN = snap.data["rho"].astype("float64") / mp * UnitDensity_in_cgs
    ecr = (
        snap.data["cren"].astype("float64") * snap.data["rho"] * UnitPressure_in_cgs
    )  # ecr in erg/pc^3

    alphae = alpha + 1

    Ce = snap.data["rho"].astype("float64") / snap.data["rho"]
    Ce *= Cp(ecr, alpha, q, mp, c) * nN
    Ce *= (
        16.0 ** (2.0 - alphae)
        / (alphae - 2.0)
        * sigmapp(alpha, mbarn)
        / sigmaT
        * me
        * c ** 2
    )
    Ce *= (mp / me) ** (alphae - 2.0) / (epsB + epsCMB)
    # factors from Waelkens et al. 2009 equation 3,4 (emissivity) and following for intensities
    c1 = (
        1.0
        / (4.0 * np.pi)
        * np.sqrt(3.0)
        * qe ** 3.0
        / (8.0 * np.pi * me * c ** 2)
        * (2.0 * me * c / (3.0 * qe) * w) ** (0.5 * (1.0 - alphae))
        * gamma(alphae / 4.0 - 1.0 / 12.0)
    )
    c2 = (
        2.0 ** (0.5 * (alphae + 1.0))
        / (alphae + 1.0)
        * gamma(alphae / 4.0 + 19.0 / 12.0)
    )
    c3 = 2.0 ** (0.5 * (alphae - 3.0)) * gamma(alphae / 4.0 + 7.0 / 12.0)
    c4 = Ce * bxz ** (0.5 * (alphae + 1.0))

    c5 = erg_per_cm2_in_mJy * sterad_in_arcmin2

    # j_perpendicular = c1 * c4 * (c2 + c3)
    # j_parallel = c1 * c4 * (c2 - c3)
    j_intensity = 2.0 * c1 * c4 * c2
    # j_polarization = 2. * c1 * c4 * c3
    snap.data["S_intensity"] = (
        j_intensity * c5 * 4.0 * np.pi * 2 * np.pi
    )  # * 1e3 * parsec # (numbers just if using no projection) in mJy/arcmin^2 x (L/kpc) times pi factors for conversion to herz and integral over surface of sphere

    return snap.data["S_intensity"]  # units: mJy/arcmin^2


def calculate_AbsLogValue(slice):

    for l in range(np.shape(slice)[0]):
        for k in range(np.shape(slice)[1]):
            if slice[l, k] != 0:
                if abs(slice[l, k]) < 1:
                    # print 'before: %f' %slice[l,k]
                    slice[l, k] = 0
                    # print 'after: %f ' %slice[l,k]
                elif (slice[l, k]) >= 1:
                    slice[l, k] = np.log10((slice[l, k]))
                elif (slice[l, k]) <= -1:
                    slice[l, k] = -1.0 * np.log10(abs(slice[l, k]))
                else:
                    print(("rest slice[l,k]: %f" % slice[l, k]))
    return slice


def get_RamThermalPressureRatio(snap, considering_h=False, no_conversion=True):
    ratio = (
        snap.data["vel"][snap.data["type"] == 0][:, 0] ** 2
        + snap.data["vel"][snap.data["type"] == 0][:, 1] ** 2
        + snap.data["vel"][snap.data["type"] == 0][:, 2] ** 2
    ) / snap.data["u"]
    return ratio


def get_NonThermalToThermalPressureRatio(snap, considering_h=False, no_conversion=True):
    # including #rampressure, magneticpressure, cosmicraypressure
    ratio = (
        snap.data["bfld"][:, 0] ** 2
        + snap.data["bfld"][:, 1] ** 2
        + snap.data["bfld"][:, 0] ** 2
    ) / (8.0 * np.pi) + snap.data["cren"] * snap.data["rho"] * 1.0 / 3.0
    ratio /= (
        snap.data["u"] * snap.data["rho"] * 2.0 / 3.0
    )  # snap.data['rho']*(snap.data['vel'][snap.data['type']==0][:,0]**2+snap.data['vel'][snap.data['type']==0][:,1]**2+snap.data['vel'][snap.data['type']==0][:,2]**2)/2. +
    return ratio


def get_ThermalPressureAcceleration(
    snap, no_conversion=False, INFO=None, considering_h=False
):
    acc = np.sqrt(np.sum(snap.grap ** 2, axis=1)) / snap.rho
    if no_conversion:
        units_acc = 1.0
    else:
        units_acc = get_UnitsAcceleration(snap, INFO=INFO, considering_h=considering_h)
    return acc * units_acc


def get_MagneticPressureAcceleration(
    snap, no_conversion=False, INFO=None, considering_h=False
):
    acc = np.zeros_like(snap.bfld)
    acc[:, 0] -= (
        snap.bfld[:, 0] * snap.bfgr[:, 0]
        + snap.bfld[:, 1] * snap.bfgr[:, 3]
        + snap.bfld[:, 2] * snap.bfgr[:, 6]
    )
    acc[:, 1] -= (
        snap.bfld[:, 0] * snap.bfgr[:, 1]
        + snap.bfld[:, 1] * snap.bfgr[:, 4]
        + snap.bfld[:, 2] * snap.bfgr[:, 7]
    )
    acc[:, 2] -= (
        snap.bfld[:, 0] * snap.bfgr[:, 2]
        + snap.bfld[:, 1] * snap.bfgr[:, 5]
        + snap.bfld[:, 2] * snap.bfgr[:, 8]
    )
    acc /= 4 * np.pi * snap.rho[:, None]
    acc = np.sqrt(np.sum(acc ** 2, axis=1))
    if no_conversion:
        units_acc = 1.0
    else:
        units_acc = get_UnitsAcceleration(snap, INFO=INFO, considering_h=considering_h)
    return acc * units_acc


def get_GravitationalAcceleration(snap, INFO=None, considering_h=False):
    acc = (
        param.GRAVITATIONALCONSTANT_cgs
        * get_EnclosedMass(snap)(get_DistanceToCenter(snap))
        / get_DistanceToCenter(snap, no_conversion=True) ** 2
    )
    acc /= snap.UnitLength_in_cm ** 2
    units_acc = get_UnitsAcceleration(
        snap, INFO=INFO, considering_h=considering_h, convert_from_cgs_to=True
    )
    return acc * units_acc


def absolute_value_array(a):
    return np.sqrt(np.sum(a ** 2, axis=1))


def absolute_value_vector(a):
    return np.sqrt(np.sum(a ** 2, axis=0))


def get_NonRadialVelocityComponent(snap, INFO=None):
    center = checkExistenceKey(INFO, "center", snap.center)
    r = snap.pos - center[None, :]
    l_ang = np.sqrt(np.sum(np.cross(snap.vel, r) ** 2, axis=1))
    return (l_ang / (absolute_value_array(snap.vel) * absolute_value_array(r)))[
        snap.type == 0
    ]


def get_CircularVelocity(snap, INFO=None, considering_h=False):
    vel = np.sqrt(
        param.GRAVITATIONALCONSTANT_cgs
        * get_EnclosedMass(snap)(get_DistanceToCenter(snap))
        / get_DistanceToCenter(snap, no_conversion=True)
    )
    vel /= np.sqrt(snap.UnitLength_in_cm)
    units_vel = get_UnitsVelocity(snap, INFO) / snap.UnitVelocity_in_cm_per_s
    return vel * units_vel


def get_TotalSpecificAngularMomentum(
    snap, no_conversion=False, considering_h=False, info=None
):
    # r x v in cgs
    center = checkExistenceKey(info, "center", snap.center)
    l_ang = np.sqrt(np.sum(np.cross(snap.vel, snap.pos - center[None, :]) ** 2, axis=1))
    if no_conversion:
        ang_units = 1.0
    elif considering_h:
        ang_units = (
            snap.UnitVelocity_in_cm_per_s * snap.UnitLength_in_cm / snap.hubbleparam
        )
    else:
        ang_units = snap.UnitVelocity_in_cm_per_s * snap.UnitLength_in_cm
    return np.array(l_ang)[snap.type == 0] * ang_units


def get_TotalSpecificAngularMomentumKpcKms(
    snap, no_conversion=False, considering_h=False, info=None
):
    # r x v in cgs
    l_ang = get_TotalSpecificAngularMomentum(snap, no_conversion=True, info=info)
    if no_conversion:
        ang_units = 1.0
    elif considering_h:
        ang_units = (
            snap.UnitVelocity_in_cm_per_s
            * snap.UnitLength_in_cm
            / snap.hubbleparam
            / param.KiloParsec_in_cm
            / 1e5
        )
    else:
        ang_units = (
            snap.UnitVelocity_in_cm_per_s
            * snap.UnitLength_in_cm
            / param.KiloParsec_in_cm
            / 1e5
        )
    return np.array(l_ang) * ang_units


def get_AngularMomentumEpsilon(snap, INFO=None):
    v_circ = get_CircularVelocity(snap)
    r = get_DistanceToCenter(snap, no_conversion=False)
    l_ang = get_TotalSpecificAngularMomentum(snap, no_conversion=False)
    debug.printDebug(l_ang, "l_ang")
    debug.printDebug(v_circ, "v_circ")
    debug.printDebug(r, "r")
    return l_ang / (r * v_circ)


def get_AngularMomentumGlobalNormalized(snap, INFO=None):
    center = checkExistenceKey(INFO, "center", snap.center)
    gas_type = snap.type == 0
    l_ang = np.cross(snap.vel, snap.pos - center[None, :])[gas_type]
    mass = snap.mass[gas_type]
    # mass weighted total angular momentum
    l_ang_total = np.sum(mass[:, None] * l_ang / np.sum(mass), axis=0)
    debug.printDebug(l_ang_total, name="l_ang_total")
    debug.printDebug(l_ang, name="l_ang")
    print(l_ang_total)
    print(f"absolute_value_vector(l_ang_total): {absolute_value_vector(l_ang_total)}")
    print(f"absolute_value_array(l_ang): {absolute_value_array(l_ang)}")
    debug.printDebug(np.dot(
        (l_ang_total / absolute_value_vector(l_ang_total)).T,
        (l_ang / absolute_value_array(l_ang)[:, None]).T,
    ), "np.dot(l_ang_total / absolute_value_vector(l_ang_total)).T,(l_ang / absolute_value_array(l_ang)[:, None]).T")
    angular_momentum_global = np.zeros_like(snap.mass[gas_type])
    # mask_non_zero = absolute_value_array(l_ang).T != 0
    # angular_momentum_global[mask_non_zero] = np.dot(
    #     (l_ang_total / absolute_value_vector(l_ang_total)).T,
    #     (l_ang / absolute_value_array(l_ang)[:, None]).T,
    # )[mask_non_zero]
    angular_momentum_global = np.dot(
        (l_ang_total / absolute_value_vector(l_ang_total)).T,
        (l_ang / absolute_value_array(l_ang)[:, None]).T,
    )
    return angular_momentum_global


def get_MagneticTensionAbsoluteForceRatio(snap):
    AbsTensionForce = get_MagneticTensionForce(
        snap, considering_h=False, no_conversion=True
    )

    PressureForce = np.zeros_like(snap.rho, dtype=np.float32)
    ThermalPressureForce = np.zeros_like(snap.rho, dtype=np.float32)
    MagneticPressureForce = np.zeros_like(snap.rho, dtype=np.float32)
    CRPressureForce = np.zeros_like(snap.rho, dtype=np.float32)
    ## thermal
    ThermalPressureForce[...] -= snap.grap[...]
    ## magnetic
    MagneticPressureForce[:, 0] -= (
        1.0
        / (4.0 * np.pi)
        * (
            snap.bfld[:, 0] * snap.bfgr[:, 0]
            + snap.bfld[:, 1] * snap.bfgr[:, 3]
            + snap.bfld[:, 0] * snap.bfgr[:, 6]
        )
    )
    MagneticPressureForce[:, 1] -= (
        1.0
        / (4.0 * np.pi)
        * (
            snap.bfld[:, 0] * snap.bfgr[:, 1]
            + snap.bfld[:, 1] * snap.bfgr[:, 4]
            + snap.bfld[:, 0] * snap.bfgr[:, 7]
        )
    )
    MagneticPressureForce[:, 2] -= (
        1.0
        / (4.0 * np.pi)
        * (
            snap.bfld[:, 0] * snap.bfgr[:, 2]
            + snap.bfld[:, 1] * snap.bfgr[:, 5]
            + snap.bfld[:, 0] * snap.bfgr[:, 8]
        )
    )

    ## cosmic ray
    print("max CR pressure gradient")
    print((np.max(snap.crpg[...])))
    CRPressureForce[...] -= snap.crpg[...]

    for k in np.arange(3):
        ThermalPressureForce[:, k] /= snap.rho[:]
        MagneticPressureForce[:, k] /= snap.rho[:]
        CRPressureForce[:, k] /= snap.rho[:]

    ## compensate for gravitational acceleration

    ThermalPressureForce[...] += snap.acce[...]

    PressureForce[...] = ThermalPressureForce + MagneticPressureForce + CRPressureForce

    AbsPressureForce = np.sqrt(
        PressureForce[:, 0] ** 2 + PressureForce[:, 1] ** 2 + PressureForce[:, 2] ** 2
    )
    # AbsThermalPressureForce = np.sqrt( ThermalPressureForce[:,0]**2 + ThermalPressureForce[:,1]**2 + ThermalPressureForce[:,2]**2 )
    # AbsMagneticPressureForce = np.sqrt( MagneticPressureForce[:,0]**2 + MagneticPressureForce[:,1]**2 + MagneticPressureForce[:,2]**2 )
    # AbsCRPressureForce = np.sqrt( CRPressureForce[:,0]**2 + CRPressureForce[:,1]**2 + CRPressureForce[:,2]**2 )
    return AbsTensionForce / AbsPressureForce


def get_MagneticTensionAcceleration(
    snap, considering_h=False, no_conversion=False, INFO=None
):
    if no_conversion:
        units_acc = 1.0
    else:
        units_acc = get_UnitsAcceleration(snap, INFO=INFO, considering_h=considering_h)
    TensionForce = np.zeros_like(snap.bfgr[:, 0:3], dtype=np.float128)
    ## gradient output in arepo:
    TensionForce[:, 0] = (
        snap.bfld[:, 0] * snap.bfgr[:, 0]
        + snap.bfld[:, 1] * snap.bfgr[:, 1]
        + snap.bfld[:, 2] * snap.bfgr[:, 2]
    )
    TensionForce[:, 1] = (
        snap.bfld[:, 0] * snap.bfgr[:, 3]
        + snap.bfld[:, 1] * snap.bfgr[:, 4]
        + snap.bfld[:, 2] * snap.bfgr[:, 5]
    )
    TensionForce[:, 2] = (
        snap.bfld[:, 0] * snap.bfgr[:, 6]
        + snap.bfld[:, 1] * snap.bfgr[:, 7]
        + snap.bfld[:, 2] * snap.bfgr[:, 8]
    )
    TensionForce[...] /= 4.0 * np.pi

    TensionForce[:, :] /= snap.rho[:, None]

    AbsTensionForce = np.sqrt(
        TensionForce[:, 0] ** 2 + TensionForce[:, 1] ** 2 + TensionForce[:, 2] ** 2
    )

    return AbsTensionForce * units_acc


def get_MagneticTensionForce(snap, considering_h=False, no_conversion=False):
    if no_conversion:
        UnitsTensionForce_in_cgs = 1.0
    elif considering_h:
        UnitsTensionForce_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            * snap.UnitVelocity_in_cm_per_s
            / (snap.UnitLength_in_cm / snap.hubbleparam / snap.UnitVelocity_in_cm_per_s)
        )
    else:
        UnitsTensionForce_in_cgs = (
            snap.UnitMass_in_g
            * snap.UnitVelocity_in_cm_per_s
            / (snap.UnitLength_in_cm / snap.UnitVelocity_in_cm_per_s)
        )
    TensionForce = np.zeros_like(
        snap.bfgr[:, 0:3] * UnitsTensionForce_in_cgs, dtype=np.float128
    )
    ## gradient output in arepo:
    TensionForce[:, 0] = (
        snap.bfld[:, 0] * snap.bfgr[:, 0]
        + snap.bfld[:, 1] * snap.bfgr[:, 1]
        + snap.bfld[:, 2] * snap.bfgr[:, 2]
    )
    TensionForce[:, 1] = (
        snap.bfld[:, 0] * snap.bfgr[:, 3]
        + snap.bfld[:, 1] * snap.bfgr[:, 4]
        + snap.bfld[:, 2] * snap.bfgr[:, 5]
    )
    TensionForce[:, 2] = (
        snap.bfld[:, 0] * snap.bfgr[:, 6]
        + snap.bfld[:, 1] * snap.bfgr[:, 7]
        + snap.bfld[:, 2] * snap.bfgr[:, 8]
    )
    TensionForce[...] /= 4.0 * np.pi

    AbsTensionForce = np.sqrt(
        TensionForce[:, 0] ** 2 + TensionForce[:, 1] ** 2 + TensionForce[:, 2] ** 2
    )

    return AbsTensionForce * UnitsTensionForce_in_cgs


def get_DistanceToCenterInKpc(
    snap, considering_h=False, no_conversion=False, INFO=None, verbose=True
):
    # POS = copy.deepcopy(snap.data["pos"][snap.data["type"] == 0][:])
    POS = snap.data["pos"][snap.data["type"] == 0]
    try:
        center = INFO["center"]
    except:
        center = snap.center
        if considering_h:
            # POS /= snap.hubbleparam
            center = [x / snap.hubbleparam for x in center]
    if no_conversion:
        distance = np.sqrt(
            (POS[:, 0] - center[0]) ** 2
            + (POS[:, 1] - center[1]) ** 2
            + (POS[:, 2] - center[2]) ** 2
        )
        if verbose:
            print("using following center to calculate distance to center:")
            print(center)
        return distance
    else:
        POS *= snap.UnitLength_in_cm / param.KiloParsec_in_cm
        center = [x * snap.UnitLength_in_cm / param.KiloParsec_in_cm for x in center]
    if verbose:
        print("using following center to calculate distance to center:")
        print(center)
        print(("pos max %g min %g" % (np.max(POS), np.min(POS))))

    distance = np.sqrt(
        (POS[:, 0] - center[0]) ** 2
        + (POS[:, 1] - center[1]) ** 2
        + (POS[:, 2] - center[2]) ** 2
    )
    return distance


def find_Vmaxmin_fromScratch(
    variable_to_look_at, snap, box=None, center=None, axis=None
):
    """
		calculates vmin, vmax of current snap

		Parameters
		----------

		Vmin, Vmax: optional
			If set, vmin/vmax of snap will be compared to these values
		Output_as_Dictionary: bool, optional
			If set to True, vmin/vmax/dictionary with entries: str(variable_to_look_at) + '_Vmin', str(variable_to_look_at) + '_Vmax' will be given
		Dictionary_to_Output: dictionary, optional
			If set, specified dictionary will be appended to include vmin,vmax specified as in Output_as_Dictionary
		box, center, axis: either or all have to be specified, optional
			If set vmin, vmax of slice with these parameters will be calculated
	"""
    print(("have to find Vmin Vmax from scratch for %s" % variable_to_look_at))
    value_variable = get_value(variable_to_look_at, snap)
    # if box, center, axis = None
    # otherwise all three have to be set so that vmin/vmax will be found in the specified area
    if (box != None) and (axis != None) and (center != None):
        if not (hasattr(snap, "vol")):
            snap.addField("vol", [1, 0, 0, 0, 0, 0])
            snap.data["vol"][:] = (
                snap.data["mass"][snap.data["type"] == 0] / snap.data["rho"]
            )
        SLICE = snap.get_Aslice(value_variable, box=box, center=center, axis=axis)
        value_variable = SLICE["grid"]
        if (
            variable_to_look_at == "VelocityX"
            or variable_to_look_at == "VelocityY"
            or variable_to_look_at == "VelocityZ"
        ) and param.useAbsLogValue:
            value_variable = calculate_AbsLogValue(value_variable)
        Vmin = value_variable.min()
        Vmax = value_variable.max()
        print(("vmin%.123f" % np.float128(np.min(value_variable))))
        print(("vmax%.123f" % np.float128(np.max(value_variable))))
    return Vmin, Vmax


def introduce_nonzero_floor(to_be_returned):
    print("introducing floor")
    start_time = time.time()
    minValue = np.min(to_be_returned)
    if minValue != 0:
        pass
    else:
        try:
            all_0 = all([x == 0 for x in y] for y in to_be_returned)
        except:
            all_0 = all([x == 0 for x in to_be_returned])
        print(all_0)
        if all_0:
            to_be_returned[:] = 1.0
            print("all values 0, thus setting them to 1")
        else:
            nonzero_min = np.min(to_be_returned[to_be_returned != 0])
            to_be_returned[to_be_returned == 0] = nonzero_min
            print(("Set zero values to min value %g" % nonzero_min))
    print(("introducing floor took %f" % (time.time() - start_time)))
    return to_be_returned


def get_DistanceToCenter(snap, INFO=None, no_conversion=False, considering_h=False):
    try:
        center = INFO["center"]
    except:
        print("using center of snapshot")
        center = snap.center
    print(f"get_DistanceToCenter: center: {center}")
    dist = np.sqrt(
        (snap.data["pos"][snap.data["type"] == 0][:, 0] - center[0]) ** 2
        + (snap.data["pos"][snap.data["type"] == 0][:, 1] - center[1]) ** 2
        + (snap.data["pos"][snap.data["type"] == 0][:, 2] - center[2]) ** 2
    )
    if no_conversion:
        UnitsLenght_in_cgs = 1.0
    elif considering_h:
        UnitsLenght_in_cgs = snap.UnitLength_in_cm / snap.hubbleparam
    else:
        UnitsLenght_in_cgs = snap.UnitLength_in_cm
    return dist * UnitsLenght_in_cgs


def getOutputFolderSnap(snap):
    path = os.path.dirname(snap._path) + "/"
    return path


def getPlotInfoFilename(snap, filename):
    path = getOutputFolderSnap(snap) + "AnalysisData/" + filename
    create_dirs_for_file(path)
    return path


def get_ReconstructedDensity(snap, INFO=None, returnINFO=False):
    import MixingEvolution as mix

    try:
        rho_average = INFO["rho_average"]
        rhoJ_0 = INFO["rhoJ_0"]
    except:
        try:
            folder = INFO["folder"]
        except:
            exit("in order to get rho average need to know the folder!")
        rho_average, rhoJ_0 = mix.get_InitialRhoAverage(folder)
        INFO["rho_average"] = rho_average

    try:
        threshold = INFO["threshold"]
    except:
        threshold = 1e-3
    print(
        (
            "using threshold value of %g of tracer fluids for reconstructed density"
            % threshold
        )
    )
    mask = snap.data["jetr"] > threshold
    Newrho = snap.data["rho"]
    volume = snap.data["vol"]
    scalar = snap.data["PassiveScalar"]
    tracer = snap.data["jetr"]

    Newrho[mask] = 0  # intialising new rho fields
    Newrho[mask] += tracer[mask] * rhoJ_0
    for i in range(np.shape(scalar)):
        Newrho[mask] += (rho_average[i] * volume[mask]) * scalar[:, i][mask]
    if not returnINFO:
        return Newrho
    else:
        return Newrho, INFO


def get_PassiveScalar(snap, INFO=None):
    if INFO is None:
        raise Exception("need to specify NScalar to get value!")
    number = INFO["NScalar"]
    print(("getting scalar number %i" % number))
    if "pass01" in snap.data.keys():
        scalar = snap.data["pass%02d" % number]
    else:
        warnings.warn("only single passive scalar found!")
        scalar = snap.data["pass"]
    if checkExistenceKey(INFO, "enforceNonNegScalar", True):
        scalar[scalar < 0] = 0
    if checkExistenceKey(INFO, "MinConsidTracer", None) is not None:
        print(
            "using tracer lower bound of %e!"
            % checkExistenceKey(INFO, "MinConsidTracer", None)
        )
        scalar[scalar < checkExistenceKey(INFO, "MinConsidTracer", None)] = 0
    if checkExistenceKey(INFO, "useStepFctTracer", False):
        scalar[scalar < 0] = 0
        scalar[scalar > 0] = 1
    scalar = useLocationMask(scalar, INFO)
    return scalar


def get_DensityPassiveScalar(snap, no_conversion=False, considering_h=False, INFO=None):
    scalarN = checkExistenceKey(INFO, "NScalar", None)
    if scalarN is None:
        raise Exception("need to specify NScalar to get value!")
    scalar = get_PassiveScalar(snap, INFO=INFO)
    print(("getting scalar number %i" % scalarN))
    # factorUnits = 1.
    # if checkExistenceKey(INFO, 'MsolPcM2', False):
    # 	factorUnits = param.PC_in_cm**2 / param.SOLARMASS_in_g
    if no_conversion:
        UnitDens_in_cgs = 1.0
    elif considering_h:
        UnitDens_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            * snap.hubbleparam ** 3
        )
    else:
        UnitDens_in_cgs = (
            snap.UnitMass_in_g
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
            / snap.UnitLength_in_cm
        )

    scalar = scalar * snap.data["rho"] * UnitDens_in_cgs  # * factorUnits
    return scalar


def get_MassPassiveScalar(snap, no_conversion=False, considering_h=False, INFO=None):
    scalarN = checkExistenceKey(INFO, "NScalar", None)
    if scalarN is None:
        raise Exception("need to specify NScalar to get value!")
    scalar = get_PassiveScalar(snap, INFO=INFO)
    print(("getting scalar number %i" % scalarN))
    if no_conversion:
        UnitMass_in_cgs = 1.0
    elif considering_h:
        UnitMass_in_cgs = snap.UnitMass_in_g / snap.hubbleparam
    else:
        UnitMass_in_cgs = snap.UnitMass_in_g

    scalar = scalar * snap.data["mass"][snap.data["type"] == 0] * UnitMass_in_cgs
    return scalar


def get_PassiveScalarCRAcceleration(snap):
    return snap.data["jeac"]


def get_PassiveScalar0(snap):
    return snap.data["pass00"]


def get_PassiveScalar1(snap):
    return snap.data["pass01"]


def get_PassiveScalar2(snap):
    return snap.data["pass02"]


def get_DistanceToObjectInKpc(
    snap, considering_h=False, convert_units=True, INFO=None, returnLabel=False
):
    try:
        CoordObject = INFO["CoordObject"]
    except:
        exit(
            "have to give coordinates of object (CoordObject) to calcualate distance to!"
        )
    distance = np.sqrt(
        (snap.data["pos"][snap.data["type"] == 0][:, 0] - CoordObject[0]) ** 2
        + (snap.data["pos"][snap.data["type"] == 0][:, 1] - CoordObject[1]) ** 2
        + (snap.data["pos"][snap.data["type"] == 0][:, 2] - CoordObject[2]) ** 2
    )
    if not convert_units:
        UnitLength_in_Kpc = 1.0
    elif considering_h:
        UnitLength_in_Kpc = snap.UnitLength_in_cm / snap.hubbleparam / 3.086e21
    else:
        UnitLength_in_Kpc = snap.UnitLength_in_cm / 3.086e21

    if returnLabel:
        return distance * UnitLength_in_Kpc
    else:
        return distance * UnitLength_in_Kpc, r"$d$"


def get_Position(snap, INFO=None):
    kpc = checkExistenceKey(INFO, "kpc", False)
    pos = copy.deepcopy(snap.data["pos"][snap.data["type"] == 0])
    if kpc:
        pos *= 1000
    return pos


def checkExistenceKey(dic, key, y, duplicateList=0, duplicateAlt=None):
    if duplicateAlt is None:
        duplicateAlt = duplicateList
    if dic is not None:
        if key in list(dic.keys()):
            return makeNIterable(dic[key], duplicateList)
    return makeNIterable(y, duplicateList)


def seperateStringFloat(combined):
    split = re.split(r"(\d+)", combined)[:-1]
    if not split:
        return None
    string = split[0]
    value = float(("".join(split[1:])).replace("p", "."))
    return string, value


def seperateStringString(combined, delimiter):
    split = combined.split(delimiter)
    if len(split) == 1:
        return None
    if len(split) > 2:
        raise Exception("seperater not unique!")
    string0, string1 = split
    try:
        string1 = ast.literal_eval(string1)
    except:
        pass
    return string0, string1


def getDistanceBHJetTip(snap, up=True, INFO=None, center=None, TracerThresh=1e-3):
    if center is None:
        center = [snap.boxsize / 0.5 for i in range(3)]
    if up:
        dist = (
            np.max(
                snap.data["pos"][snap.data["type"] == 0][:, 0][
                    snap.data["jetr"] > TracerThresh
                ]
            )
            - center[0]
        )
    else:
        dist = (
            np.min(
                snap.data["pos"][snap.data["type"] == 0][:, 0][
                    snap.data["jetr"] > TracerThresh
                ]
            )
            - center[0]
        )
    print(center)
    print(
        (
            np.max(
                snap.data["pos"][snap.data["type"] == 0][:, 0][
                    snap.data["jetr"] > TracerThresh
                ]
            ),
            np.min(
                snap.data["pos"][snap.data["type"] == 0][:, 0][
                    snap.data["jetr"] > TracerThresh
                ]
            ),
        )
    )
    return np.abs(dist)


def getThreeDimDistanceMask(pos, center, radii):
    print(center)
    print(radii)
    if len(radii) == 1:
        radii *= 3
    print(center)
    print(radii)
    print((center[0]))
    print((radii[0]))

    d = np.sqrt(
        (pos[:, 0] - center[0]) ** 2 / radii[0] ** 2
        + (pos[:, 1] - center[1]) ** 2 / radii[1] ** 2
        + (pos[:, 2] - center[2]) ** 2 / radii[2] ** 2
    )
    mask = d < 1.0
    return mask


def checkNumberOfIterations(Object):
    check = [i for i, x in enumerate(Object)]
    return int(np.max(check)) + 1


def makeIterableArbitrary(Object, numbers=[1], dim=2):
    import collections
    import copy

    if not isinstance(numbers, collections.Iterable):
        numbers = [numbers]
    if len(numbers) != dim - 1:
        print((numbers, dim))
        raise Exception(
            "incorrect numbers for specified dimension in makeIterableArbitrary!"
        )
    dimPresent = 0
    objectCop = copy.deepcopy(Object)
    numberIt = np.zeros(dim, dtype=int)
    for i in range(dim):
        if isinstance(objectCop, collections.Iterable):
            dimPresent += 1
            numberIt[i] = checkNumberOfIterations(objectCop)
            objectCop = objectCop[0]
    print(numberIt, dimPresent)
    for i in range(dim - 1):
        print(i)
        print(numberIt[i], numbers[i])
        if numberIt[i] != numbers[i]:
            print(numberIt[i], numbers[i])
            Object = [Object for x in range(numbers[i])]
    return Object


if __name__ == "__main__":
    Object = [[2, 1], [1, 2, 3], [3, 2]]
    thing = makeIterableArbitrary(Object, numbers=[5, 4, 3], dim=4)
    print(thing)
    print(np.shape(thing))


def makeIterable(
    obj,
    number,
    name="name",
    verbose=1,
    enforceType=False,
    list_of_list=False,
    strings_iterable=True,
):
    if (not isinstance(obj, collections.Iterable) and strings_iterable) or (
        not isIterable(obj) and not strings_iterable
    ):
        if enforceType:
            if type(obj) != enforceType:
                print(type(obj), enforceType)
                raise Exception("wrong type")
        obj = [obj for i in range(number)]
    else:
        if enforceType:
            if not all(type(x) == enforceType for x in obj):
                print(type(obj), enforceType)
                raise Exception("wrong type")
    if np.size(obj) < number:
        if verbose > 1:
            print(name, obj)
        raise Exception("need more information for %sx, %sy, ..." % (name, name))
    return obj


def convert_to_list(obj):
    if not isIterable(obj):
        return [obj]
    if type(obj) == np.ndarray or type(obj) == list:
        if type(obj) == np.ndarray:
            obj = obj.tolist()
        try:
            obj[0][0]
            if type(obj[0][0]) != str:
                obj = [convert_to_list(x) for x in obj]
        except:
            return obj
    return obj


def makeNIterableList(object, N):
    if not isIterable(N):
        N = [N]
    length = len(N)
    if [0] == N or len(N) == 0:
        return object
    if not isIterable(object):
        object = [
            makeNIterableList(object, N[1:]) if length > 1 else object
            for i in range(N[0])
        ]
    else:
        object = convert_to_list(object)
        if N[0] > len(object):
            object = object + [object[0]] * (N[0] - len(object))
        elif N[0] < len(object):
            object = object[: N[0]]
        object = [makeNIterableList(o, N[1:]) for o in object]
    return object


def calcBubbleRadiusCenter(
    up, snap, center, TracerThresh, INFO, CumTracThresh=[0.46, 0.1, 0.1]
):
    DistanceBHJetTip = getDistanceBHJetTip(
        snap, up=up, INFO=INFO, center=center, TracerThresh=TracerThresh
    )
    BubbleCenter, BubbleRadYZ = ScreenJetTracer(
        snap,
        bins=100,
        up=up,
        TracerThresh=TracerThresh,
        CumTracThresh=CumTracThresh,
        AreaToCheck=[DistanceBHJetTip * 2, DistanceBHJetTip * 2, DistanceBHJetTip * 2],
        ensureOnRespectiveSide=True,
        center=center,
        returnBubbleRadiiYZ=True,
    )
    radiusBubbleX = np.abs(DistanceBHJetTip - np.abs(BubbleCenter[0] - center[0]))
    radiusBubble = [float(radiusBubbleX), BubbleRadYZ[0], BubbleRadYZ[1]]
    return BubbleCenter, radiusBubble


def get_double_beta_profile(r_in_kpc, a_cm3, a_in_kpc, a, b_cm3, b_in_kpc, b):
    # profile of typical density in cool core galaxy cluster
    # r in kpc
    return a_cm3 * (1 + (r_in_kpc / a_in_kpc) ** 2) ** (a) + b_cm3 * (
        1 + (r_in_kpc / b_in_kpc) ** 2
    ) ** (b)


def getBubbleRadiusCenter(snap, whichBubble="up", INFO=None):
    """
	whichBubble = up, down, both
	"""
    down = True
    up = True
    if whichBubble == "up":
        down = False
    elif whichBubble == "down":
        up = False
    elif whichBubble == "both":
        pass
    else:
        exit("bubble config not available in calc radius/center of bubbles!")
    TracerThresh = checkIfInDictionary("CenterLobeThresh", INFO, 1e-6)
    center = checkIfInDictionary("center", INFO, [snap.boxsize / 2.0 for i in range(3)])
    CumTracThresh = checkIfInDictionary(
        "CumTracThreshFakeBubble", INFO, [0.46, 0.1, 0.1]
    )
    print("CumTracThresh fake bubble")
    print(CumTracThresh)
    if whichBubble in ["up", "down"]:
        Bcenter, Bradius = calcBubbleRadiusCenter(
            up, snap, center, TracerThresh, INFO, CumTracThresh=CumTracThresh
        )
        return Bcenter, Bradius
    else:
        BcenterU, BradiusU = calcBubbleRadiusCenter(
            True, snap, center, TracerThresh, INFO, CumTracThresh=CumTracThresh
        )
        BcenterD, BradiusD = calcBubbleRadiusCenter(
            False, snap, center, TracerThresh, INFO, CumTracThresh=CumTracThresh
        )
        return BcenterU, BradiusU, BcenterD, BradiusD


def getFakeBubble(snap, INFO=None):
    print("in fake bubble")
    import FileWorks as FW

    print("imported FW")
    import FigureMove as Fig

    print("import fig")
    snapNum = checkIfInDictionary("SnTime", INFO, None)
    print("snapNum", snapNum)
    # raise Exception('stop!')
    if snapNum is not None:
        # exit('need to specify time of bubble')
        print(("looking at fake bubble at snap time %g" % snapNum))
        if snapNum == "current":
            snapNum = get_Time(snap, considering_h=False)
        else:
            snapNum = Fig.getSnapNumber(
                getOutputFolderSnap(snap), snapNum, differenceTime=2
            )
        if snapNum == get_Time(snap, considering_h=False):
            snapBub = copy.deepcopy(snap)
        else:
            snapBub = Fig.readSnap(
                getOutputFolderSnap(snap),
                snapNum,
                FieldsToLoad=["pos", "jetr", "vol"],
                useTime=False,
                differenceTime=2,
            )
            snapBub, time = Fig.getGeneralInformation(snapBub, considering_h=False)
        header = (
            "SnNum",
            "BcenterU",
            "BradiusU",
            "BcenterD",
            "BradiusD",
            "CumTracThresh",
        )
        CumTracThresh = checkIfInDictionary(
            "CumTracThreshFakeBubble", INFO, [0.46, 0.1, 0.1]
        )
        function = getBubbleRadiusCenter
        functionParameters = snapBub, "both", INFO
        print(header)
        BcenterU, BradiusU, BcenterD, BradiusD = FW.checkRetrieveValueCSV(
            getPlotInfoFilename(snap, "BubbleCenterRadius.txt"),
            header,
            function,
            functionParameters,
            relevantElementsNumbers=[0, 5],
            relevantElements=[snapNum, CumTracThresh],
        )
    else:
        BcenterU = checkIfInDictionary("FaBubCenterU", INFO, None)
        BradiusU = checkIfInDictionary("FaBubRadiusU", INFO, None)
        BcenterD = checkIfInDictionary("FaBubCenterD", INFO, None)
        BradiusD = checkIfInDictionary("FaBubRadiusD", INFO, None)
        if any([x is None for x in [BcenterD, BcenterU, BradiusD, BradiusU]]):
            print(BcenterU)
            print(BradiusU)
            print(BcenterD)
            print(BradiusD)
            exit(
                "need to specify FaBubCenterU, FaBubRadiusU, FaBubCenterD, FaBubRadiusD"
            )
    if snapNum != get_Time(snap, considering_h=False) and snapNum is not None:
        del snapBub
    print("BubbleCenter, radiusBubble")
    print((BcenterU, BradiusU, BcenterD, BradiusD))
    print((np.max(snap.data["pos"][snap.data["type"] == 0])))
    print((np.min(snap.data["pos"][snap.data["type"] == 0])))
    maskU = getThreeDimDistanceMask(
        snap.data["pos"][snap.data["type"] == 0], BcenterU, BradiusU
    )
    maskD = getThreeDimDistanceMask(
        snap.data["pos"][snap.data["type"] == 0], BcenterD, BradiusD
    )
    mask = np.logical_or(maskU, maskD)
    return mask


def getCumThresh(grid, INFO=None, invert=None):
    print(("shape grid cum thresh", np.shape(grid)))
    if invert is None:
        invert = checkIfInDictionary("CTInv", INFO, False)
    threshBeginningJet = checkIfInDictionary("threshTracBegJet", INFO, 1e-1)
    axisOfInt = checkIfInDictionary("axisOfInt", INFO, None)
    if axisOfInt is None or np.size(axisOfInt) != 1:
        print(axisOfInt)
        exit(
            "specify axis of integration for location mask of beginning of jet correctly!!"
        )
    if not invert:
        tracer = np.cumsum(grid, axis=axisOfInt)
    else:
        SLICER = [slice(None) for i in range(3)]
        SLICER[axisOfInt] = slice(None, None, -1)
        tracer = np.cumsum(grid[SLICER], axis=axisOfInt)[SLICER]
    mask = np.zeros(np.shape(tracer))
    condition = tracer < threshBeginningJet
    mask[condition] = 1.0
    return mask


def getCumSumAverage(grid, axisOfInt=None):
    if axisOfInt is None:
        raise Exception(
            "need to specify axis of integration when calculating cumsum average!"
        )
    CumSum = np.cumsum(grid, axis=axisOfInt)
    SLICER = [slice(None) for i in range(3)]
    if axisOfInt == 0:
        inversion_slice = slice(-1, None, None)
    elif axisOfInt == 1:
        inversion_slice = slice(None, -1, None)
    elif axisOfInt == 2:
        inversion_slice = slice(None, None, -1)
    SLICER[axisOfInt] = slice(None, None, -1)
    CumSumInv = (np.cumsum(grid[SLICER], axis=axisOfInt))[SLICER]
    return (CumSum + CumSumInv) / 2.0


def getLocationMaskGrid(snap, INFO, res, ProjBox, center, numthreads):
    location = checkIfInDictionary("locationGrid", INFO, None)
    print(("working on location", location))
    needHitJet = checkIfInDictionary("locationGridneedHitJet", INFO, False)
    print(f"getLocationMaskGrid: needHitJet:{needHitJet}")
    if location is None:
        return None
    import FigureMove as Fig

    grid = Fig.getAgrid(
        snap, "jetr", res=res, ProjBox=ProjBox, center=center, numthreads=numthreads
    )
    if location == "UJB":
        # until begining of jet
        mask = getCumThresh(grid, INFO=INFO, invert=False)
    elif location == "BJI":
        # from Begining of Jet Including jet
        mask1 = getCumThresh(grid, INFO=INFO, invert=False)
        mask2 = getCumThresh(grid, INFO=INFO, invert=True)
        mask = np.logical_not(np.logical_or(mask1, mask2))
        mask = np.logical_or(mask, mask1)
    elif location == "UJE":
        # until begining of jet from behind
        mask = getCumThresh(grid, INFO=INFO, invert=True)
    elif location == "NJE":
        # ignore jet region
        mask1 = getCumThresh(grid, INFO=INFO, invert=False)
        mask2 = getCumThresh(grid, INFO=INFO, invert=True)
        mask = np.logical_or(mask1, mask2)
    elif location == "OJE":
        # only jet region
        mask1 = getCumThresh(grid, INFO=INFO, invert=False)
        mask2 = getCumThresh(grid, INFO=INFO, invert=True)
        mask = np.logical_not(np.logical_or(mask1, mask2))
    elif location == "OJEAlt":
        # only jet region
        threshOJEAlt = checkIfInDictionary("locationGridTrshOJEAlt", INFO, 1e-3)
        print("using %e for thresh of OJEAlt" % threshOJEAlt)
        mask = grid > threshOJEAlt
    else:
        print(location)
        raise Exception("location not yet specified in location mask grid!")

    if needHitJet:
        tracerType = checkIfInDictionary("tracerHitType", INFO, "JetTracer")
        print(f"tracerType: {tracerType}")
        if tracerType == "JetTracer":
            pass
        elif tracerType == "PassiveScalars":
            grid = snap.get_Agrid(
                get_PassiveScalar(snap, INFO=INFO),
                res=res,
                box=ProjBox,
                center=center,
                numthreads=numthreads,
            )["grid"]
        else:
            raise Exception(
                "haven implemented %s type of tracer to hit yet!" % tracerType
            )
        axisOfInt = checkIfInDictionary("axisOfInt", INFO, None)
        thresh = checkIfInDictionary("locationGridneedHitJetThresh", INFO, 1e-3)
        jetcells = np.zeros(np.shape(grid))
        jetcells[grid > thresh] = 1.0
        mask1 = getCumSumAverage(jetcells, axisOfInt=axisOfInt)
        mask1 = mask1 >= 1.0
        mask = np.logical_and(mask1, mask)
        print(f"needHitJet: {needHitJet}")
        print(f"axisOfInt: {axisOfInt}")
        print(f"thresh: {thresh}")
        print(f"location: {location}")
    if checkIfInDictionary("maskPlotTracer", INFO, False):
        import FigureMove as Fig

        # assumes axis=[2,0]
        ids = Fig.getAgrid(
            snap, "id", res=res, ProjBox=ProjBox, center=center, numthreads=numthreads
        )
        sumed = np.sum(mask, axis=1)
        indices = np.where(sumed > 0)
        uniqueN = [
            len(np.unique(ids[i, :, j] * mask[i, :, j]))
            for i, j in zip(indices[0], indices[1])
        ]
        uniqueNArray = np.zeros(np.shape(sumed))
        uniqueNArray[indices] = uniqueN
        Fig.plotGrid(
            uniqueNArray,
            log=True,
            axis=None,
            vmin=None,
            vmax=None,
            labelsizeTicks=14,
            fontsize=20,
            prefixFile="Uniq",
            addFile="%s" % location,
            colormap="tab20c_r",
            pixelToPlot="half",
            ProjBox=ProjBox,
            center=center,
            saveFold="../figures/Masks/",
        )
        Fig.plotGrid(
            sumed,
            log=True,
            axis=None,
            vmin=None,
            vmax=None,
            labelsizeTicks=14,
            fontsize=20,
            prefixFile="Sumd",
            addFile="%s" % location,
            colormap="tab20c_r",
            pixelToPlot="half",
            ProjBox=ProjBox,
            center=center,
            saveFold="../figures/Masks/",
        )
    return mask


def getLocationMask(snap, INFO):
    location = INFO["Location"]
    tracerThresh = checkIfInDictionary("LocationLobeThresh", INFO, 1e-3)
    print(
        (
            "using threshold of %f for tracer threshold when defining location mask!"
            % tracerThresh
        )
    )
    if location == "Lob":
        mask = snap.data["jetr"] > tracerThresh
        print(("looking at lob with threshold %g" % tracerThresh))
    elif location == "ICM":
        mask = snap.data["jetr"] < tracerThresh
        print(("looking at ICM with threshold %g" % tracerThresh))
    elif location == "All":
        mask = np.ones(np.shape(Jet_Tracer), dtype=bool)
    elif location == "FBu":  # Lob of fake bubble
        mask = getFakeBubble(snap, INFO=INFO)
    elif location == "FBN":  # ICM of fake bubble
        mask = getFakeBubble(snap, INFO=INFO)
        mask = np.logical_not(mask)
    elif location == "ACC":  # ICM of fake bubble
        mask = get_AccretableGas(snap, INFO=INFO)
    else:
        print(location)
        print(INFO)
        exit("location not implemented yet!")
    return mask


def get_MagneticFieldTurbulent1D(
    kabs,
    INFO=None,
    kL=1 / np.sqrt(3),
    kinj=40,
    kcrit=80,
    A=None,
    a=0,
    b=-11 / 3.0,
    c=0,
    useAasUnity=False,
    sigma_to_be=1,
    v=0,
):
    kL = checkExistenceKey(INFO, "MagneticFieldTurbulentkL", kL)
    kinj = checkExistenceKey(INFO, "MagneticFieldTurbulentkinj", kinj)
    kcrit = checkExistenceKey(INFO, "MagneticFieldTurbulentkcrit", kcrit)
    A = checkExistenceKey(INFO, "MagneticFieldTurbulentA", A)
    a = checkExistenceKey(INFO, "MagneticFieldTurbulenta", a)
    b = checkExistenceKey(INFO, "MagneticFieldTurbulentb", b)
    c = checkExistenceKey(INFO, "MagneticFieldTurbulentc", c)
    useAasUnity = checkExistenceKey(
        INFO, "MagneticFieldTurbulentuseAasUnity", useAasUnity
    )
    sigma_to_be = checkExistenceKey(
        INFO, "MagneticFieldTurbulentsigma_to_be", sigma_to_be
    )

    if v > 1:
        if useAasUnity:
            print("initializeField1D: using A=1")
        elif A is not None:
            print("initializeField1D: use predefined A to initialize magnetic field!")
        else:
            print("WARNING: initializeField1D: calculate A based on sigma_to_be")
    rand0 = np.zeros(np.shape(kabs), dtype=np.complex)
    if A is None or useAasUnity:
        A = 1.0
    sigma = np.sqrt(PowerSpec(kabs, kL, kinj, kcrit, A, a, b, c)) / np.sqrt(2.0)
    rand0.real, rand0.imag = random_gauss(sigma)
    if A == 1.0 and not useAasUnity:
        if v > 1:
            print("---------------using sigma_to_be: ", sigma_to_be)
        A = (
            sigma_to_be ** 2 / np.sum(np.abs(rand0) ** 2) * np.prod(np.shape(kabs))
        )  # **self.spaceDim
        A = np.sqrt(A)
        rand0 = rand0 * A
    return rand0


def get_Powerspec(
    kabs,
    INFO=None,
    kL=1 / np.sqrt(3),
    kinj=40,
    kcrit=80,
    A=None,
    a=0,
    b=-11 / 3.0,
    c=0,
    useAasUnity=False,
    sigma_to_be=1,
    v=0,
):
    kL = checkExistenceKey(INFO, "MagneticFieldTurbulentkL", kL)
    kinj = checkExistenceKey(INFO, "MagneticFieldTurbulentkinj", kinj)
    kcrit = checkExistenceKey(INFO, "MagneticFieldTurbulentkcrit", kcrit)
    A = checkExistenceKey(INFO, "MagneticFieldTurbulentA", A)
    a = checkExistenceKey(INFO, "MagneticFieldTurbulenta", a)
    b = checkExistenceKey(INFO, "MagneticFieldTurbulentb", b)
    c = checkExistenceKey(INFO, "MagneticFieldTurbulentc", c)
    useAasUnity = checkExistenceKey(
        INFO, "MagneticFieldTurbulentuseAasUnity", useAasUnity
    )
    sigma_to_be = checkExistenceKey(
        INFO, "MagneticFieldTurbulentsigma_to_be", sigma_to_be
    )

    if v > 1:
        if useAasUnity:
            print("initializeField1D: using A=1")
        elif A is not None:
            print("initializeField1D: use predefined A to initialize magnetic field!")
        else:
            print("WARNING: initializeField1D: calculate A based on sigma_to_be")
    if A is None or useAasUnity:
        A = 1.0
    power = np.sqrt(PowerSpec(kabs, kL, kinj, kcrit, A, a, b, c))
    if A == 1.0 and not useAasUnity:
        if v > 1:
            print("---------------using sigma_to_be: ", sigma_to_be)
        A = (
            sigma_to_be ** 2 / np.sum(np.abs(power) ** 2) * np.prod(np.shape(kabs))
        )  # **self.spaceDim
        A = np.sqrt(A)
        power = power * A
    return power


def get_MagneticFieldTurbulent3D(kabs, INFO=None, v=0):
    return np.array(
        get_MagneticFieldTurbulent1D(kabs, INFO=INFO, v=v) for i in range(3)
    )


def random_gauss(sigma):
    shape = np.shape(sigma)
    X1 = np.random.rand(*shape)
    X2 = np.random.rand(*shape)
    phi = 2.0 * np.pi * X1
    r = np.sqrt(-2 * sigma ** 2 * np.log(X2))
    return r * np.cos(phi), r * np.sin(phi)


def get_SingleBetaFunction(x, a=1, b=1, c=1, INFO=None, v=0):
    a, b, c = checkExistenceKey(INFO, "SingleBetaABC", (a, b, c))
    return a * (1.0 + (x / b) ** 2) ** (-c)


def get_SingleBetaFT(ks, a=1, b=1, c=1, INFO=None, v=0):
    a, b, c = checkExistenceKey(INFO, "SingleBetaFT", (a, b, c))
    import scipy.special

    # assumes profile in real space: a*(1+(x/b)^2)^(-c)
    return (
        2 ** (1.0 - c)
        * a
        / (b ** 2) ** (-1.0 / 4.0 - c / 2.0)
        * abs(ks) ** (-1.0 / 2.0 + c)
        * scipy.special.kv(1.0 / 2.0 - c, np.array(abs(ks) * abs(b), dtype=np.complex))
        / scipy.special.gamma(np.complex(c))
    )


def get_SingleBetaFT2D(kabs, a=1, b=1, c=1, INFO=None, v=0):
    a, b, c = checkExistenceKey(INFO, "SingleBetaFT2D", (a, b, c))
    import scipy.special

    # assumes profile in real space: a*(1+(x/b)^2)^(-c)
    return (
        2 ** (-c)
        * a
        * np.pi
        * kabs ** (-1 + c)
        * abs(b) ** (1 + c)
        * (
            -1 * scipy.special.iv(1 - c, np.array(abs(kabs) * abs(b), dtype=np.complex))
            + scipy.special.iv(-1 + c, np.array(abs(kabs) * abs(b), dtype=np.complex))
        )
        / np.sin(c * np.pi)
        / scipy.special.gamma(np.complex(c))
    )


def get_valueTheoretical(variable_to_get, x, INFO=None, returnINFO=False, v=0):
    print(variable_to_get)

    if variable_to_get == "MagneticFieldTurbulent1D":
        to_be_returned = get_MagneticFieldTurbulent1D(x, INFO=INFO, v=v)
    elif variable_to_get == "MagneticFieldTurbulent3D":
        to_be_returned = get_MagneticFieldTurbulent3D(x, INFO=INFO, v=v)
    elif variable_to_get == "Powerspec":
        to_be_returned = get_Powerspec(x, INFO=INFO, v=v)
    elif variable_to_get == "SingleBetaFunction":
        to_be_returned = get_SingleBetaFunction(x, INFO=INFO, v=v)
    elif variable_to_get == "SingleBetaFT":
        to_be_returned = get_SingleBetaFT(x, INFO=INFO, v=v)
    elif variable_to_get == "SingleBetaFT2D":
        to_be_returned = get_SingleBetaFT2D(x, INFO=INFO, v=v)
    else:
        raise Exception("%s not implemented in get_valueTheoretical!" % variable_to_get)

    if returnINFO:
        to_be_returned, INFO
    return to_be_returned


def get_value(
    variable_to_get,
    snap,
    where=None,
    check_for_0=False,
    considering_h=False,
    convert_units=True,
    INFO=None,
    returnINFO=False,
    returnLabel=False,
    highprecision=False,
    return_INFO_unchanged=True,
    verbose=1,
):
    if return_INFO_unchanged:
        if INFO is not None:
            INFO = INFO.copy()
    if verbose:
        print(f"variable_to_get: {variable_to_get}")
    if considering_h:
        raise Exception("old remnant; importing snapshot takes care of this!")
    # INFO in name overwrites if key in INFO present!
    # dont use strings X,Y,Z,'Lob','ICM','All','FBu','FBN' unintententionally at end of new variable names
    if INFO is None:
        INFO = {}
    split = variable_to_get.split("_")
    variable_to_get = split[0]
    if np.size(split) > 1:
        for part in split[1:]:
            print(f"part: {part}")
            if ":" not in part and seperateStringFloat(part):
                key, value = seperateStringFloat(part)
            else:
                key, value = seperateStringString(part, ":")
            INFO["%s" % key] = value
    FieldDirection = None
    if re.findall(r"(ABOVE)(\d+[p.e]?(?:\d+))?((?:keV|K))", variable_to_get):
        above_string = re.findall(
            r"(ABOVE)(\d+[p.e]?(?:\d+))?((?:keV|K))", variable_to_get
        )[0]
        INFO["TcutAboveTemp"], INFO["TcutAboveUnits"] = (
            float(above_string[1].replace("p", ".")),
            above_string[2],
        )
        variable_to_get = variable_to_get.replace("".join(above_string), "")
    if re.findall(r"(BELOW)(\d+[p.e]?(?:\d+))?((?:keV|K))", variable_to_get):
        below_string = re.findall(
            r"(BELOW)(\d+[p.e]?(?:\d+))?((?:keV|K))", variable_to_get
        )[0]
        INFO["TcutBelowTemp"], INFO["TcutBelowUnits"] = (
            float(below_string[1].replace("p", ".")),
            below_string[2],
        )
        variable_to_get = variable_to_get.replace("".join(below_string), "")
    if variable_to_get[:3] in ["ABS", "SQT", "SQD"]:
        addInfo = variable_to_get[:3]
        variable_to_get = variable_to_get[3:]
        INFO[addInfo] = True
    if variable_to_get[-1] in ["X", "Y", "Z", "0", "1", "2"]:
        FieldDirection = variable_to_get[-1]
        variable_to_get = variable_to_get[:-1]
    Logable = False
    if variable_to_get[-7:] in ["Logable"]:
        variable_to_get = variable_to_get[:-7]
        Logable = True
    if variable_to_get[-3:] in ["Lob", "ICM", "All", "FBu", "FBN", "ACC"]:
        if "SELECT" in INFO:
            raise Exception(
                f"cannot have both {variable_to_get[-3:]} and INFO[Select]: {INFO['SELECT']}!"
            )
        INFO["Location"] = variable_to_get[-3:]
        variable_to_get = variable_to_get[:-3]
        INFO["LocationMask"] = getLocationMask(snap, INFO)
    elif "SELECT" in INFO:
        _select = INFO["SELECT"]
        INFO["Location"] = _select
        INFO["LocationMask"] = get_selection(_select, snap)
    else:
        INFO["Location"] = None

    if variable_to_get == "Time":
        to_be_returned = get_Time(snap, considering_h=considering_h)
    elif variable_to_get == "Position":
        to_be_returned = get_Position(
            snap, INFO=INFO
        )  # considering h already taken care of presumably when loading snapshot
    elif (variable_to_get == "Density") or (variable_to_get == "rho"):
        to_be_returned = get_Density(snap, considering_h=considering_h)
    elif variable_to_get == "ElectronNumberDensityFullyIonized":
        to_be_returned = get_ElectronNumberDensityFullyIonized(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "ElectronNumberDensity":
        to_be_returned = get_ElectronNumberDensity(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "ElectronNumberDensityWarmPhase":
        to_be_returned = get_ElectronNumberDensityWarmPhase(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "ElectronNumberDensityPerseus":
        to_be_returned = get_ElectronNumberDensityPerseus(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "NFWPotentialPerseus":
        to_be_returned = get_NFWPotentialPerseus(snap, INFO=INFO)
    elif variable_to_get == "TotalPressurePerseus":
        to_be_returned = get_TotalPressurePerseus(snapm, INFO=INFO)
    elif variable_to_get == "TotalInternalEnergyDensityPerseus":
        to_be_returned = get_TotalInternalEnergyDensityPerseus(snap, INFO=INFO)
    elif variable_to_get == "Entropy":
        to_be_returned = get_Entropy(snap, considering_h=considering_h)
    elif variable_to_get == "EntropyTop":
        to_be_returned = get_EntropyTop(snap, considering_h=considering_h)
    elif variable_to_get == "EntropyBottom":
        to_be_returned = get_EntropyBottom(snap, considering_h=considering_h)
    elif (variable_to_get == "Temperature") or (variable_to_get == "temp"):
        to_be_returned = get_Temperature(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "TemperatureInkV":
        to_be_returned = get_Temperature_inkV(snap)
    elif variable_to_get == "XrayLuminosity":
        to_be_returned = get_XrayLuminosity(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "XrayEmissivity":
        to_be_returned = get_XrayEmissivity(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "Pressure":
        to_be_returned = get_Pressure(snap, considering_h=considering_h)
    elif variable_to_get == "FakeEnergy":
        to_be_returned = get_FakeEnergy(snap, considering_h=considering_h)
    elif variable_to_get == "One":
        to_be_returned = get_One(snap)
    elif variable_to_get == "AbsoluteVelocitySpeedOfLightRatio":
        to_be_returned = get_AbsoluteVelocitySpeedOfLightRatio(snap)
    elif variable_to_get == "AbsoluteVelocity":
        to_be_returned = get_AbsoluteVelocity(
            snap,
            considering_h=considering_h,
            INFO=INFO,
            no_conversion=not convert_units,
        )
    elif variable_to_get == "AbsoluteVelocityinkms":
        to_be_returned = get_AbsoluteVelocityinkms(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "VelocityRadial":
        to_be_returned = get_VelocityRadial(
            snap, INFO=INFO, no_conversion=not convert_units,
        )
    elif variable_to_get == "VelocityX":
        to_be_returned = get_VelocityX(
            snap,
            considering_h=considering_h,
            INFO=INFO,
            no_conversion=not convert_units,
        )
    elif variable_to_get == "VelocityY":
        to_be_returned = get_VelocityY(
            snap,
            considering_h=considering_h,
            INFO=INFO,
            no_conversion=not convert_units,
        )
    elif variable_to_get == "VelocityYAbove20keV":
        to_be_returned = get_VelocityYAbove20keV(
            snap,
            considering_h=considering_h,
            INFO=INFO,
            no_conversion=not convert_units,
        )
    elif variable_to_get == "VelocityBelowXkeV":
        to_be_returned = get_VelocityBelowXkeV(
            snap,
            considering_h=considering_h,
            INFO=INFO,
            no_conversion=not convert_units,
        )
    elif variable_to_get == "SoundSpeedBelowXkeV":
        to_be_returned = get_SoundSpeedBelowXkeV(
            snap,
            considering_h=considering_h,
            INFO=INFO,
            no_conversion=not convert_units,
        )
    elif variable_to_get == "VelocityZ":
        to_be_returned = get_VelocityZ(
            snap,
            considering_h=considering_h,
            INFO=INFO,
            no_conversion=not convert_units,
        )
    elif variable_to_get == "Velocity":
        to_be_returned = get_Velocity(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "CurrentDensity":
        to_be_returned = get_CurrentDensity(
            snap, convert_units=convert_units, considering_h=considering_h
        )
    elif variable_to_get == "MagneticFieldX":
        to_be_returned = get_MagneticFieldX(
            snap, considering_h=considering_h, convert_units=convert_units
        )
    elif variable_to_get == "MagneticFieldY":
        to_be_returned = get_MagneticFieldY(
            snap, considering_h=considering_h, convert_units=convert_units
        )
    elif variable_to_get == "MagneticFieldZ":
        to_be_returned = get_MagneticFieldZ(
            snap, considering_h=considering_h, convert_units=convert_units
        )
    elif (
        variable_to_get == "ConstantMagneticField" or variable_to_get == "ConsMagField"
    ):
        to_be_returned = get_ConstantMagneticField(
            snap, INFO=INFO, convert_units=convert_units
        )
    elif variable_to_get == "AbsoluteConstantMagneticField":
        to_be_returned = get_AbsoluteConstantMagneticField(
            snap, INFO=INFO, convert_units=convert_units
        )
    elif variable_to_get == "AbsoluteConstantMagneticFieldEnergyDensity":
        to_be_returned = get_AbsoluteConstantMagneticFieldEnergyDensity(
            snap, INFO=INFO, convert_units=convert_units
        )
    elif variable_to_get == "SqrtAbsoluteConstantMagneticFieldEnergyDensity":
        to_be_returned = get_SqrtAbsoluteConstantMagneticFieldEnergyDensity(
            snap, INFO=INFO, convert_units=convert_units
        )
    elif variable_to_get == "SqrtAbsoluteConstantMomentumFlux":
        to_be_returned = get_SqrtAbsoluteConstantMomentumFlux(
            snap, INFO=INFO, convert_units=convert_units
        )
    elif (variable_to_get == "vol") or (variable_to_get == "Volume"):
        to_be_returned = get_Volume(
            snap, considering_h=considering_h, convert_units=convert_units, INFO=INFO
        )
    elif variable_to_get == "VolumeInKpc3":
        to_be_returned = get_VolumeInKpc3(snap, considering_h=considering_h)
    elif variable_to_get == "Mass":
        to_be_returned = get_Mass(
            snap,
            considering_h=considering_h,
            convert_units=convert_units,
            returnLabel=returnLabel,
        )
    elif variable_to_get == "MassInSolar":
        to_be_returned = get_MassInSolar(snap)
    elif variable_to_get == "ColdGasMassInSM":
        to_be_returned = get_ColdGasMassInSM(snap, INFO=INFO)
    elif variable_to_get == "ColdGasDensity":
        to_be_returned = get_ColdGasDensity(
            snap, INFO=INFO, convert_units=convert_units, considering_h=considering_h
        )
    elif variable_to_get == "StarFormationRate":
        to_be_returned = get_StarFormationRate(snap)
    elif variable_to_get == "cren":
        to_be_returned = get_Cren(snap, considering_h=considering_h)
    elif variable_to_get == "u":
        to_be_returned = get_U(snap, considering_h=considering_h)
    elif variable_to_get == "Machnumber":
        to_be_returned = get_Machnumber(snap, considering_h=considering_h)
    elif variable_to_get == "MachnumberICM":
        to_be_returned = get_MachnumberICM(snap, considering_h=considering_h)
    elif (variable_to_get == "jetr") or (variable_to_get == "JetTracer"):
        to_be_returned = get_JetTracer(snap, considering_h=considering_h)
    elif variable_to_get == "PassiveScalarCRAcceleration":
        to_be_returned = get_PassiveScalarCRAcceleration(snap)
    elif variable_to_get == "AbsoluteMagneticField2":
        to_be_returned = get_AbsoluteMagneticField2(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            highprecision=highprecision,
        )
    elif variable_to_get == "AbsoluteMagneticField":
        to_be_returned = get_AbsoluteMagneticField(snap, considering_h=considering_h)
    elif variable_to_get == "AbsoluteMagneticFieldinmuG":
        to_be_returned = get_AbsoluteMagneticFieldinmuG(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            highprecision=highprecision,
        )
    elif variable_to_get == "InternalCosmicRatio":
        to_be_returned = get_InternalCosmicRatio(snap, considering_h=considering_h)
    elif variable_to_get == "RamPressure":
        to_be_returned = get_RamPressure(
            snap, considering_h=considering_h, where=None, highprecision=highprecision
        )
    elif (variable_to_get == "InternalEnergyDensity") or (
        variable_to_get == "ThermalEnergyDensity"
    ):
        to_be_returned = get_InternalEnergyDensity(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            where=None,
            highprecision=highprecision,
            returnLabel=returnLabel,
        )
    elif (variable_to_get == "MagneticEnergyDensity") or (
        variable_to_get == "MagneticPressure"
    ):
        to_be_returned = get_MagneticEnergyDensity(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            where=None,
            highprecision=highprecision,
            returnLabel=returnLabel,
        )
    elif variable_to_get == "MagneticFieldEnergy":
        to_be_returned = get_MagneticFieldEnergy(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
            highprecision=highprecision,
        )
    elif variable_to_get == "ThermalEnergy":
        to_be_returned = get_ThermalEnergy(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
            highprecision=highprecision,
        )
    elif variable_to_get == "CosmicRayEnergyDensity":
        to_be_returned = get_CosmicRayEnergyDensity(
            snap,
            considering_h=considering_h,
            where=None,
            highprecision=highprecision,
            returnLabel=returnLabel,
            no_conversion=not convert_units,
        )
    elif variable_to_get == "KineticEnergy":
        to_be_returned = get_KineticEnergy(
            snap,
            considering_h=considering_h,
            returnLabel=returnLabel,
            no_conversion=not convert_units,
        )
    elif variable_to_get == "XKin":
        to_be_returned = get_XKin(snap, considering_h=considering_h)
    elif variable_to_get == "XCR":
        to_be_returned = get_XCR(snap, considering_h=considering_h)
    elif variable_to_get == "XB":
        to_be_returned = get_XB(snap, considering_h=considering_h)
    elif variable_to_get == "DivBRatio":
        to_be_returned = get_DivBRatio(snap, considering_h=considering_h)
    elif (variable_to_get == "ThermalPressure") or (
        variable_to_get == "InternalPressure"
    ):
        to_be_returned = get_ThermalPressure(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
        )
    elif variable_to_get == "CRPressure":
        to_be_returned = get_CRPressure(snap, considering_h=considering_h)
    elif variable_to_get == "MagneticField":
        to_be_returned = get_MagneticField(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "MagneticFieldEnergyDensity":
        to_be_returned = get_MagneticFieldEnergyDensity(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "SqrtMagneticFieldEnergyDensity":
        to_be_returned = get_SqrtMagneticFieldEnergyDensity(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "MagneticRamPressureRatio":
        to_be_returned = get_MagneticRamPressureRatio(snap, considering_h=considering_h)
    elif variable_to_get == "CosmicRayPressure":
        to_be_returned = get_CosmicRayPressure(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
        )
    elif variable_to_get == "AlfvenVelocity":
        to_be_returned = get_AlfvenVelocity(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "AlfvenVelocityinkms":
        to_be_returned = get_AlfvenVelocityinkms(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "VelocityToAlfvenVelocityRatio":
        to_be_returned = get_VelocityToAlfvenVelocityRatio(snap)
    elif variable_to_get == "AbsoluteCosmicRayPressureGradient":
        to_be_returned = get_AbsoluteCosmicRayPressureGradient(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "AbsolutePressureGradient":
        to_be_returned = get_AbsolutePressureGradient(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "CosmicRayPressureGradient":
        to_be_returned = get_CosmicRayPressureGradient(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "PressureGradient":
        to_be_returned = get_PressureGradient(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "CosmicRayLengthScale":
        to_be_returned = get_CosmicRayLengthScale(snap, considering_h=considering_h)
    elif variable_to_get == "CosmicRayLengthScaleMask":
        to_be_returned = get_CosmicRayLengthScaleMask(snap, considering_h=considering_h)
    elif variable_to_get == "CRDiffusionCoefficient":
        to_be_returned = get_CRDiffusionCoefficient(snap, considering_h=considering_h)
    elif variable_to_get == "AlfvenCoolingTimescaleInMyr":
        to_be_returned = get_AlfvenCoolingTimescale_inMyr(
            snap, considering_h=considering_h
        )
    elif variable_to_get == "CosmicRayEnergy":
        to_be_returned = get_CosmicRayEnergy(
            snap,
            considering_h=considering_h,
            returnLabel=returnLabel,
            highprecision=highprecision,
            no_conversion=not convert_units,
        )
    elif variable_to_get == "AbsoluteMomentum":
        to_be_returned = get_AbsoluteMomentum(snap, considering_h=considering_h)
    elif variable_to_get == "AbsoluteMomentumFlux":
        to_be_returned = get_AbsoluteMomentumFlux(snap, considering_h=considering_h)
    elif variable_to_get == "SqrtAbsoluteMomentumFlux":
        to_be_returned = get_SqrtAbsoluteMomentumFlux(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "MomentumFlux":
        to_be_returned = get_MomentumFlux(snap, considering_h=considering_h)
    elif variable_to_get == "AbsoluteKineticEnergyFlux":
        to_be_returned = get_AbsoluteKineticEnergyFlux(
            snap, considering_h=considering_h
        )
    elif variable_to_get == "KineticEnergyFlux":
        to_be_returned = get_KineticEnergyFlux(snap, considering_h=considering_h)
    elif variable_to_get == "RamThermalPressureRatio":
        to_be_returned = get_RamThermalPressureRatio(snap, considering_h=considering_h)
    elif variable_to_get == "NonThermalToThermalPressureRatio":
        to_be_returned = get_NonThermalToThermalPressureRatio(
            snap, considering_h=considering_h
        )
    elif variable_to_get == "DistanceToCenterInKpc":
        to_be_returned = get_DistanceToCenterInKpc(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "CosmicRayHeatingRate":
        to_be_returned = get_CosmicRayHeatingRate(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "TotalCosmicRayHeatingRate":
        to_be_returned = get_TotalCosmicRayHeatingRate(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "CosmicRayHeatingRateActive":
        to_be_returned = get_CosmicRayHeatingRateActive(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "CosmicRayHeatingRateActiveJFr0p05":
        to_be_returned = get_CosmicRayHeatingRateActiveJFr0p05(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "CosmicRayHeatingRateActiveJFr0p01":
        to_be_returned = get_CosmicRayHeatingRateActiveJFr0p01(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "CosmicRayHeatingRateActiveJFr0p5":
        to_be_returned = get_CosmicRayHeatingRateActiveJFr0p5(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "SynchrotronRadiation":
        to_be_returned = get_SynchrotronRadiation(snap, INFO)
    elif variable_to_get == "SoundSpeed":
        to_be_returned = get_SoundSpeed(
            snap, convert_units=convert_units, units=None, INFO=INFO
        )
    elif variable_to_get == "VelocitySoundSpeedRatio":
        to_be_returned = get_VelocitySoundSpeedRatio(snap)
    elif variable_to_get == "CosmicRayMagneticPressureRatio":
        to_be_returned = get_CosmicRayMagneticPressureRatio(snap)
    elif variable_to_get == "AlfvenMachnumber":
        to_be_returned = get_AlfvenMachnumber(snap)
    elif variable_to_get == "MagneticTensionAbsoluteForceRatio":
        to_be_returned = get_MagneticTensionAbsoluteForceRatio(snap)
    elif variable_to_get == "MagneticTensionForce":
        to_be_returned = get_MagneticTensionForce(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "ThermalPressureAcceleration":
        to_be_returned = get_ThermalPressureAcceleration(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "MagneticPressureAcceleration":
        to_be_returned = get_MagneticPressureAcceleration(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "GravitationalAcceleration":
        to_be_returned = get_GravitationalAcceleration(
            snap, considering_h=considering_h, INFO=INFO,
        )
    elif variable_to_get == "CircularVelocity":
        to_be_returned = get_CircularVelocity(
            snap, considering_h=considering_h, INFO=INFO,
        )
    elif variable_to_get == "TotalSpecificAngularMomentum":
        to_be_returned = get_TotalSpecificAngularMomentum(
            snap, no_conversion=not convert_units, considering_h=considering_h,
        )
    elif variable_to_get == "TotalSpecificAngularMomentumKpcKms":
        to_be_returned = get_TotalSpecificAngularMomentumKpcKms(
            snap, no_conversion=not convert_units, considering_h=considering_h,
        )
    elif variable_to_get == "AngularMomentumEpsilon":
        to_be_returned = get_AngularMomentumEpsilon(snap, INFO=INFO,)
    elif variable_to_get == "AngularMomentumGlobalNormalized":
        to_be_returned = get_AngularMomentumGlobalNormalized(snap, INFO=INFO,)
    elif variable_to_get == "NonRadialVelocityComponent":
        to_be_returned = get_NonRadialVelocityComponent(snap, INFO=INFO,)
    elif variable_to_get == "MagneticTensionAcceleration":
        to_be_returned = get_MagneticTensionAcceleration(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "JetVolumeFraction":
        to_be_returned = get_JetVolumeFraction(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "KineticEnergyDensity":
        to_be_returned = get_KineticEnergyDensity(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
        )
    elif variable_to_get == "JetMassFraction":
        to_be_returned = get_JetMassFraction(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "JetDensityFraction":
        to_be_returned = get_JetDensityFraction(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "PassiveScalarCRAccelerationEnergyDensity":
        to_be_returned = get_PassiveScalarCRAccelerationEnergyDensity(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "PassiveScalarCRAccelerationEnergy":
        to_be_returned = get_PassiveScalarCRAccelerationEnergy(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "VelocitySignalSpeedRatio":
        to_be_returned = get_VelocitySignalSpeedRatio(snap)
    elif variable_to_get == "TotalEnergyDensity":
        to_be_returned = get_TotalEnergyDensity(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
        )
    elif variable_to_get == "CREnergyDensityFraction":
        to_be_returned = get_CREnergyDensityFraction(snap, returnLabel=returnLabel)
    elif variable_to_get == "KineticEnergyDensityFraction":
        to_be_returned = get_KineticEnergyDensityFraction(snap, returnLabel=returnLabel)
    elif variable_to_get == "MagneticEnergyDensityFraction":
        to_be_returned = get_MagneticEnergyDensityFraction(
            snap, returnLabel=returnLabel
        )
    elif variable_to_get == "ThermalEnergyDensityFraction":
        to_be_returned = get_ThermalEnergyDensityFraction(snap, returnLabel=returnLabel)
    elif variable_to_get == "ReconstructedDensity":
        if returnINFO:
            to_be_returned, INFO = get_ReconstructedDensity(
                snap, INFO=INFO, returnINFO=returnINFO
            )
        else:
            to_be_returned = get_ReconstructedDensity(
                snap, INFO=INFO, returnINFO=returnINFO
            )
    elif variable_to_get == "PassiveScalars":
        to_be_returned = get_PassiveScalar(snap, INFO=INFO)
    elif variable_to_get == "PassiveScalar0" or variable_to_get == "PassiveScalarFirst":
        to_be_returned = get_PassiveScalar0(snap)
    elif variable_to_get == "PassiveScalar1":
        to_be_returned = get_PassiveScalar1(snap)
    elif variable_to_get == "PassiveScalar2":
        to_be_returned = get_PassiveScalar2(snap)
    elif variable_to_get == "DensityPassiveScalar":
        to_be_returned = get_DensityPassiveScalar(
            snap,
            no_conversion=not convert_units,
            considering_h=considering_h,
            INFO=INFO,
        )
    elif variable_to_get == "MassPassiveScalar":
        to_be_returned = get_MassPassiveScalar(
            snap,
            no_conversion=not convert_units,
            considering_h=considering_h,
            INFO=INFO,
        )
    elif variable_to_get == "DistanceToCenter":
        to_be_returned = get_DistanceToCenter(
            snap,
            INFO=INFO,
            no_conversion=not convert_units,
            considering_h=considering_h,
        )
    elif variable_to_get == "DissipationEnergy":
        to_be_returned = get_DissipationEnergy(
            snap, considering_h=considering_h, convert_units=convert_units
        )
    elif variable_to_get == "DissipationEnergyDensity":
        to_be_returned = get_DissipationEnergyDensity(
            snap,
            considering_h=considering_h,
            convert_units=convert_units,
            returnLabel=returnLabel,
        )
    elif variable_to_get == "VorticityperMyr":
        to_be_returned = get_VorticityperMyr2(
            snap, considering_h=considering_h, convert_units=convert_units
        )
    elif variable_to_get == "DistanceToObjectInKpc":
        to_be_returned = get_DistanceToObjectInKpc(
            snap,
            considering_h=considering_h,
            convert_units=convert_units,
            INFO=INFO,
            returnLabel=returnLabel,
        )
    elif variable_to_get == "BulkKineticEnergyDensity":
        to_be_returned = get_BulkKineticEnergyDensity(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
            INFO=INFO,
        )
    elif variable_to_get == "TurbulentEnergyDensity":
        to_be_returned = get_TurbulentEnergyDensity(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
            INFO=INFO,
        )
    elif variable_to_get == "TurbulentEnergyDensityResid":
        to_be_returned = get_TurbulentEnergyDensityResid(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
            INFO=INFO,
        )
    elif variable_to_get == "KineticEnergy":
        to_be_returned = get_KineticEnergy(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
        )
    elif variable_to_get == "MagneticEnergy":
        to_be_returned = get_MagneticEnergy(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
        )
    elif variable_to_get == "TurbulentEnergyResid":
        to_be_returned = get_TurbulentEnergyResid(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
            INFO=INFO,
        )
    elif variable_to_get == "TotalEnergy":
        to_be_returned = get_TotalEnergy(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
        )
    elif variable_to_get == "TotalEnergyDensityInc":
        to_be_returned = get_TotalEnergyDensityInc(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            returnLabel=returnLabel,
            INFO=INFO,
        )
    elif variable_to_get == "ThermalEnergyDensityFrInc":
        to_be_returned = get_ThermalEnergyDensityFrInc(snap, INFO=INFO)
    elif variable_to_get == "MagneticEnergyDensityFrInc":
        to_be_returned = get_MagneticEnergyDensityFrInc(snap, INFO=INFO)
    elif variable_to_get == "TurbEnergyDensityFrInc":
        to_be_returned = get_TurbEnergyDensityFrInc(snap, INFO=INFO)
    elif variable_to_get == "BulkEnergyDensityFrInc":
        to_be_returned = get_BulkEnergyDensityFrInc(snap, INFO=INFO)
    elif variable_to_get == "CREnergyDensityFrInc":
        to_be_returned = get_CREnergyDensityFrInc(snap, INFO=INFO)
    elif variable_to_get == "TurbEnergyDensityResidFrInc":
        to_be_returned = get_TurbEnergyDensityResidFrInc(snap, INFO=INFO)
    elif variable_to_get == "KineticEnergyDensityFrInc":
        to_be_returned = get_KineticEnergyDensityFrInc(snap, INFO=INFO)
    elif variable_to_get == "VelocityGradient":
        to_be_returned = get_VelocityGradient(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "VelocityLengthScale":
        to_be_returned = get_VelocityLengthScale(
            snap, no_conversion=not convert_units, considering_h=considering_h
        )
    elif variable_to_get == "CellRadiusInKpc":
        to_be_returned = get_CellRadiusInKpc(snap)
    elif variable_to_get == "JeansLengthInKpc":
        to_be_returned = get_JeansLengthInKpc(
            snap, considering_h=considering_h, no_conversion=not convert_units
        )
    elif variable_to_get == "VorticityperMyr2Approx":
        to_be_returned = get_VorticityperMyr2Approx(
            snap, considering_h=considering_h, convert_units=convert_units
        )
    elif variable_to_get == "JetTotalEnergyAvailable":
        to_be_returned = get_JetTotalEnergyAvailable(snap, returnLabel=returnLabel)
    elif variable_to_get == "JetEnergy":
        to_be_returned = get_JetEnergy(snap, returnLabel=returnLabel)
    elif variable_to_get == "CumAccretedMassColdAccretionInSolarMass":
        to_be_returned = get_CumAccretedMassColdAccretionInSolarMass(snap)
    elif variable_to_get == "BondiMdot":
        to_be_returned = get_BondiMdot(snap, INFO=INFO)
    elif variable_to_get == "BHCumThEgyInjectedInJet":
        to_be_returned = get_BHCumThEgyInjectedInJet(snap)
    elif variable_to_get == "BHCumThEgyInjectedInJetNegative":
        to_be_returned = get_BHCumThEgyInjectedInJetNegative(snap)
    elif variable_to_get == "JetBufferEnergyNegative":
        to_be_returned = get_JetBufferEnergyNegative(snap)
    elif variable_to_get == "JetBufferEnergy":
        to_be_returned = get_JetBufferEnergy(snap)
    elif variable_to_get == "BHCumEgyInjectedRM":
        to_be_returned = get_BHCumEgyInjectedRM(snap)
    elif variable_to_get == "BHCumEgyInjectedQM":
        to_be_returned = get_BHCumEgyInjectedQM(snap)
    elif variable_to_get == "BHCumKinEgyInjectedInJet":
        to_be_returned = get_BHCumKinEgyInjectedInJet(snap, returnLabel=returnLabel)
    elif variable_to_get == "BHCumMagnEgyInjectedInJet":
        to_be_returned = get_BHCumMagnEgyInjectedInJet(snap, returnLabel=returnLabel)
    elif variable_to_get == "BHCumCREgyInjectedInJet":
        to_be_returned = get_BHCumCREgyInjectedInJet(snap, returnLabel=returnLabel)
    elif variable_to_get == "JetDistanceHalfHalf":
        to_be_returned = get_JetDistanceHalfHalf(
            snap, INFO=INFO, both=True, up=False, center=None, returnLabel=returnLabel
        )
    elif variable_to_get == "BulkVelocity":
        to_be_returned = get_BulkVelocity(
            snap, no_conversion=not convert_units, INFO=INFO
        )
    elif variable_to_get == "AbsoluteBulkVelocity":
        to_be_returned = get_AbsoluteBulkVelocity(
            snap, no_conversion=not convert_units, INFO=INFO
        )
    elif variable_to_get == "TurbulentVelocity":
        to_be_returned = get_TurbulentVelocity(
            snap, no_conversion=not convert_units, INFO=INFO
        )
    elif variable_to_get == "AboluteTurbulentVelocity":
        to_be_returned = get_AboluteTurbulentVelocity(
            snap, no_conversion=not convert_units, INFO=INFO
        )
    elif variable_to_get == "JetActivity":
        to_be_returned = get_JetActivity(snap, returnLabel=returnLabel, folder=None)
    elif variable_to_get == "NormalizedDrapingLayerThickness":
        to_be_returned = get_NormalizedDrapingLayerThickness(snap)
    elif variable_to_get == "XrayCoolingRate":
        to_be_returned = get_XrayCoolingRate(snap, considering_h=considering_h)
    elif variable_to_get == "XrayCoolingTimescaleInMyr":
        to_be_returned = get_XrayCoolingTimescaleInMyr(
            snap, considering_h=considering_h
        )
    elif variable_to_get == "CoolingRate":
        to_be_returned = get_CoolingRate(snap, INFO=INFO)
    elif variable_to_get == "CoolingEmissivity":
        to_be_returned = get_CoolingEmissivity(snap, INFO=INFO)
    elif variable_to_get == "CoolingLuminosity":
        to_be_returned = get_CoolingLuminosity(snap, INFO=INFO)
    elif variable_to_get == "CoolingEnergyRate":
        to_be_returned = get_CoolingEnergyRate(
            snap,
            verbose=verbose,
            INFO=INFO,
        )
    elif variable_to_get == "PressureEquilbriumCondensationCriteriumCellRadiusInKpc":
        to_be_returned = get_PressureEquilbriumCondensationCriteriumCellRadiusInKpc(
            snap
        )
    elif variable_to_get == "CosmicRayHeatingRateCoolingRateRatio":
        to_be_returned = get_CosmicRayHeatingRateCoolingRateRatio(snap, INFO=INFO)
    elif (
        variable_to_get == "CoolingTimescaleInMyr"
        or variable_to_get == "CoolingTimeInMyr"
    ):
        to_be_returned = get_CoolingTimeInMyr(snap, INFO=INFO)
    elif variable_to_get == "FreeFallTimeInMyr":
        to_be_returned = get_FreeFallTimeInMyr(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "XrayCoolingFreeFallTimeRatio":
        to_be_returned = get_XrayCoolingFreeFallTimeRatio(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "CoolingFreeFallTimeRatio":
        to_be_returned = get_CoolingFreeFallTimeRatio(
            snap, considering_h=considering_h, INFO=INFO, verbose=verbose
        )
    elif variable_to_get == "DeltaI":
        to_be_returned = get_DeltaI(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "ThermalSz":
        to_be_returned = get_ThermalSZ(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "KineticSz":
        to_be_returned = get_KineticSZ(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "KineticToThermalSZRatio":
        to_be_returned = get_KineticToThermalSZRatio(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "RelativisticSz":
        to_be_returned = get_RelativisticSZ(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "SZSignal":
        to_be_returned = get_SZSignal(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "TemperatureBubble":
        to_be_returned = get_TemperatureBubble(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "ElectronNumberDensityBubble":
        to_be_returned = get_ElectronNumberDensityBubble(
            snap,
            considering_h=considering_h,
            no_conversion=not convert_units,
            INFO=INFO,
        )
    elif variable_to_get == "TemperatureMS0735":
        to_be_returned = get_TemperatureMS0735(snap, INFO=INFO)
    elif variable_to_get == "ElectronNumberDensityMS0735":
        to_be_returned = get_ElectronNumberDensityMS0735(snap, INFO=INFO)
    elif variable_to_get == "TemperatureMS0735Bubble":
        to_be_returned = get_TemperatureMS0735Bubble(snap, INFO=INFO)
    elif variable_to_get == "ElectronNumberDensityMS0735Bubble":
        to_be_returned = get_ElectronNumberDensityMS0735Bubble(snap, INFO=INFO)
    elif variable_to_get == "TracerFakeBubble":
        to_be_returned = get_TracerFakeBubble(snap, INFO=INFO)
    # single values below ....
    elif variable_to_get == "BHhsml":
        to_be_returned = get_BHhsml(snap, convert_units=convert_units, INFO=INFO)
    else:
        errorTXT = (
            "ERROR: The variable: %s is not available yet in function get_value!"
            % variable_to_get
        )
        exit(errorTXT)
    # INFO["variable_to_get"] = variable_to_get
    if check_for_0:
        to_be_returned = introduce_nonzero_floor(to_be_returned)
    if type(to_be_returned) != dict:
        to_be_returned = useLocationMask(to_be_returned, INFO)
    if FieldDirection is not None:
        if FieldDirection in ["X", "0"]:
            FieldDirection = 0
        elif FieldDirection in ["Y", "1"]:
            FieldDirection = 1
        elif FieldDirection in ["Z", "2"]:
            FieldDirection = 2
        else:
            raise Exception(f"FieldDirection {FieldDirection} not understood!")
        to_be_returned = to_be_returned[:, FieldDirection]
    if checkIfInDictionary("ABS", INFO, False):
        try:
            to_be_returned = np.sqrt(np.sum(to_be_returned ** 2, axis=1))
        except:
            to_be_returned = np.abs(to_be_returned)
    if Logable:
        to_be_returned[to_be_returned <= 0] = np.min(to_be_returned[to_be_returned > 0])
    if checkIfInDictionary("SQT", INFO, False):
        print("taking sqrt of %s" % variable_to_get)
        to_be_returned = np.sqrt(to_be_returned)
    if checkIfInDictionary("SQD", INFO, False):
        print("squaring of %s" % variable_to_get)
        to_be_returned = to_be_returned ** 2
    if returnLabel and np.size(to_be_returned) != 2:
        to_be_returned = [to_be_returned, "NA"]
    if checkIfInDictionary("TcutAboveTemp", INFO, False):
        mask = get_mask_temperature_cut(
            snap, above=True, cut=INFO["TcutAboveTemp"], units=INFO["TcutAboveUnits"]
        )
        to_be_returned = mask * to_be_returned
    if checkIfInDictionary("TcutBelowTemp", INFO, False):
        mask = get_mask_temperature_cut(
            snap, above=False, cut=INFO["TcutBelowTemp"], units=INFO["TcutBelowUnits"]
        )
        to_be_returned = mask * to_be_returned
    if not returnINFO:
        return to_be_returned
    else:
        return to_be_returned, INFO


def get_value_single(snap, variable_to_get, operation, INFO=None, select=None):
    if select and checkIfInDictionary("select", INFO, False):
        if select != INFO["select"]:
            raise Exception(
                f"select need to be identical if specified in select {select} and INFO {INFO['select']}"
            )
    elif select is None and checkIfInDictionary("select", INFO, False):
        select = checkIfInDictionary("select", INFO, None)
    if select is not None:
        mask = get_selection(select, snap)
    else:
        mask = None
    values = get_value(
        variable_to_get=variable_to_get, snap=snap, convert_units=True, INFO=INFO
    )
    values = values[mask]
    if operation == "sum":
        values = np.sum(values)
    elif operation == "sumNonNegative":
        values = np.sum(values[values > 0])
    elif operation == "sumNegative":
        values = np.sum(values[values < 0])
    elif operation == "sumNegativeAbs":
        values = np.abs(np.sum(values[values < 0]))
    elif operation == "max":
        values = np.max(values)
    elif operation == "mean":
        values = np.mean(values)
    else:
        raise Exception(f"operation {operation} not understood")
    return values


def get_value_single_weighted(snap, variable_to_get, weight, operation, INFO=None, select=None):
    if select and checkIfInDictionary("select", INFO, False):
        if select != INFO["select"]:
            raise Exception(
                f"select need to be identical if specified in select {select} and INFO {INFO['select']}"
            )
    elif select is None and checkIfInDictionary("select", INFO, False):
        select = checkIfInDictionary("select", INFO, None)
    if select is not None:
        mask = get_selection(select, snap)
    else:
        mask = None
    values = get_value(
        variable_to_get=variable_to_get, snap=snap, convert_units=True, INFO=INFO
    )
    values = values[mask]

    weights = get_value(
        variable_to_get=weight, snap=snap, convert_units=True, INFO=INFO
    )
    weights = weights[mask]

    if operation == "averageweighted":
        values = np.average(values, weights=weights)
    else:
        raise Exception(f"operation {operation} not understood")
    return values


def get_mask_temperature_cut(snap, above, cut, units):
    if units == "K":
        temp = get_Temperature(snap, no_conversion=False)
    elif units == "keV":
        temp = get_Temperature_inkV(snap)
    else:
        raise Exception(f"temperature units {units} not understood")
    if above:
        return temp > cut
    else:
        return temp < cut


def get_occurence_in_list(lis, obj):
    return sum([1 if x == obj else 0 for x in lis])


def get_selection(string, snap):
    print(f"string: {string}")
    if string is None:
        print(f"selectin ({string}) is None")
        return None
    combinations_chaining = "(AND|OR)"
    chaining_split = re.split(combinations_chaining, string)
    for index, operation in enumerate(chaining_split):
        if re.search(operation, combinations_chaining) is not None:
            continue
        chaining_split[index] = get_single_selection(operation, snap)
    if len(chaining_split) > 1:
        if get_occurence_in_list(chaining_split, "(") != 0:
            raise Exception("not implemtned")

        def reduce_masks(chaining_split):
            if len(chaining_split) == 1:
                return chaining_split
            method = chaining_split[1]
            if method == "AND":
                reduction = np.logical_and(chaining_split[0], chaining_split[2])
            elif method == "OR":
                reduction = np.logical_or(chaining_split[0], chaining_split[2])
            return reduce_masks([reduction] + chaining_split[3:])

        mask = reduce_masks(chaining_split)
    else:
        mask = chaining_split[0]
    print(f"returning")
    if type(mask) != np.ndarray:
        mask = mask[0]
        if type(mask) != np.ndarray:
            raise Exception(f"mask still not array! mask: {mask}")
    return mask


def get_single_selection(string, snap):
    combinations_operations = "([><]|==)"
    operation_split = re.split(combinations_operations, string)

    def get_selection_arg(string):
        from argparse_helpers import fancy_eval

        arg = fancy_eval(string)
        if isinstance(arg, str):
            arg = get_value(arg, snap)
        debug.printDebug(arg, string)
        return arg

    arg1 = get_selection_arg(operation_split[0])
    arg2 = get_selection_arg(operation_split[2])
    operation = operation_split[1]
    if operation == "<":
        mask = arg1 < arg2
    elif operation == "==":
        mask = arg1 == arg2
    elif operation == ">":
        mask = arg1 > arg2
    else:
        raise Exception(f"operation not understood: {operation}")
    return mask


def get_CRConversionLog(directory):
    data = np.loadtxt(directory + "blackhole_jet_cr_acceleration.txt")
    return data


def get_ConvertedCosmicRayEnergy(
    time, convert_units=True, considering_h=False, DataCRconv=None, returnLabel=True
):
    if DataCRconv is None:
        exit("need blackhole_jet_cr_acceleration.txt file")
    UnitLength_in_cm = param.UnitLength_in_cm
    UnitVelocity_in_cm_per_s = param.UnitVelocity_in_cm_per_s
    HubbleParam = param.HubbleParam
    UnitMass_in_g = param.UnitMass_in_g
    if not convert_units:
        unitEnergyinErg = 1.0
        unitTimeinMyr = 1.0
    elif not considering_h:
        unitEnergyinErg = UnitMass_in_g * UnitVelocity_in_cm_per_s ** 2
        unitTimeinMyr = UnitLength_in_cm / UnitVelocity_in_cm_per_s / 3.1536e13
    else:
        unitEnergyinErg = UnitMass_in_g * UnitVelocity_in_cm_per_s ** 2 / HubbleParam
        unitTimeinMyr = (
            UnitLength_in_cm / UnitVelocity_in_cm_per_s / 3.1536e13 / HubbleParam
        )
    index = findIndexValue(time, DataCRconv[:, 0] * unitTimeinMyr)
    print(index)
    print(
        (
            "found time in CR conversion file at %f Myr"
            % (DataCRconv[index, 0] * unitTimeinMyr)
        )
    )
    if returnLabel:
        return DataCRconv[index, 1] * unitEnergyinErg, r"$E_\mathrm{cr,conv}$"
    return DataCRconv[index, 1] * unitEnergyinErg


def get_JetTotalEnergyAvailable(snap, returnLabel=False):
    TimeSpan = (
        snap.configfile["BH_CONSTANT_POWER_RADIOMODE_TIMELIMIT_IN_MYR"]
        * 3.1536e13
        / snap.hubbleparam
    )
    TimeStart = (
        snap.configfile["BH_CONSTANT_POWER_RADIOMODE_START_TIME_IN_MYR"]
        * 3.1536e13
        / snap.hubbleparam
    )
    timeRun = (
        snap.Time
        / snap.hubbleparam
        * snap.UnitLength_in_cm
        / snap.UnitVelocity_in_cm_per_s
    )
    if timeRun > (TimeSpan + TimeStart):
        activeTimeSpan = TimeSpan
    else:
        activeTimeSpan = timeRun - TimeStart
        if activeTimeSpan < 0:
            activeTimeSpan = 0
    lum = activeTimeSpan * snap.configfile["BH_CONSTANT_POWER_RADIOMODE_IN_ERG_PER_S"]
    if returnLabel:
        return lum, r"$E_\mathrm{tot,jet}\ [\mathrm{erg}]$"
    return lum


def get_JetEnergy(snap, returnLabel=False):
    TimeSpan = (
        snap.configfile["BH_CONSTANT_POWER_RADIOMODE_TIMELIMIT_IN_MYR"]
        * 3.1536e13
        / snap.hubbleparam
    )
    TimeStart = (
        snap.configfile["BH_CONSTANT_POWER_RADIOMODE_START_TIME_IN_MYR"]
        * 3.1536e13
        / snap.hubbleparam
    )
    activeTimeSpan = TimeSpan
    erg = activeTimeSpan * snap.configfile["BH_CONSTANT_POWER_RADIOMODE_IN_ERG_PER_S"]
    if returnLabel:
        return erg, r"$E_\mathrm{jet}\ [\mathrm{erg}]$"
    return erg


def get_JetBufferEnergy(snap, returnLabel=False):
    erg = snap.bhbe[0] * snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    if returnLabel:
        return erg, r"$E_\mathrm{jet,buff}\ [\mathrm{erg}]$"
    return erg


def get_JetBufferMassInSolar(snap, returnLabel=False):
    mass = snap.bhbm[0] * snap.UnitMass_in_g / param.SOLARMASS_in_g
    if returnLabel:
        return mass, r"$M_\mathrm{jet,buff}\ [M\odot]$"
    return mass


def get_JetBufferEnergyNegative(snap, returnLabel=False):
    erg = -1 * snap.bhbe[0] * snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    if returnLabel:
        return erg, r"$E_\mathrm{jet,buff}\ [\mathrm{erg}]$"
    return erg


def get_BHCumEgyInjectedRM(snap, returnLabel=False):
    erg = snap.bcer[0] * snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    if returnLabel:
        return erg, r"$E_\mathrm{jet,RM}\ [\mathrm{erg}]$"
    return erg


def get_BHCumEgyInjectedQM(snap, returnLabel=False):
    erg = snap.bceq[0] * snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    if returnLabel:
        return erg, r"$E_\mathrm{jet,QM}\ [\mathrm{erg}]$"
    return erg


def get_BHCumThEgyInjectedInJet(snap, returnLabel=False):
    """
	gives cumulative kinetic energy injected by jet of first BH in list
	"bhjt", "BH_CumThermEgyInjectedInJet"
	"""
    erg = snap.bhjt[0] * snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    if returnLabel:
        return erg, r"$E_\mathrm{jet,th}\ [\mathrm{erg}]$"
    return erg


def get_BHCumThEgyInjectedInJetNegative(snap, returnLabel=False):
    """
	gives cumulative kinetic energy injected by jet of first BH in list
	"bhjt", "BH_CumThermEgyInjectedInJet"
	"""
    erg = -1.0 * snap.bhjt[0] * snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    if returnLabel:
        return erg, r"$E_\mathrm{jet,th}\ [\mathrm{erg}]$"
    return erg


def get_BHCumKinEgyInjectedInJet(snap, returnLabel=False):
    """
	gives cumulative kinetic energy injected by jet of first BH in list
	"bhjk", "BH_CumKinEgyInjectedInJet"
	"""
    erg = snap.bhjk[0] * snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    if returnLabel:
        return erg, r"$E_\mathrm{jet,kin}\ [\mathrm{erg}]$"
    return erg


def get_BHCumKinEgyInjectedInJetNegative(snap, returnLabel=False):
    """
	gives cumulative kinetic energy injected by jet of first BH in list
	"bhjk", "BH_CumKinEgyInjectedInJet"
	"""
    erg = -1 * snap.bhjk[0] * snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    if returnLabel:
        return erg, r"$E_\mathrm{jet,kin}\ [\mathrm{erg}]$"
    return erg


def get_BHCumMagnEgyInjectedInJet(snap, returnLabel=False):
    """
	gives cumulative kinetic energy injected by jet of first BH in list
	"bhjk", "BH_CumKinEgyInjectedInJet"
	"""
    erg = snap.bhjm[0] * snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    if returnLabel:
        return erg, r"$E_{\mathrm{jet},B}\ [\mathrm{erg}]$"
    return erg


def get_BHCumCREgyInjectedInJet(snap, returnLabel=False):
    """
	gives cumulative kinetic energy injected by jet of first BH in list
	"bhjk", "BH_CumKinEgyInjectedInJet"
	"""
    erg = snap.bhjc[0] * snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    if returnLabel:
        return erg, r"$E_\mathrm{jet,cr}\ [\mathrm{erg}]$"
    return erg


def get_CumMassGrowthBHInSU(snap):
    """
	gives cumulative mass growth of blackhole in quasar and radio mode in solar masses (of first blackhole)
        'BH_CumMassGrowth_QM' : 'bcmq',
        'BH_CumMassGrowth_RM' : 'bcmr',
	"""
    mass = snap.bcmq[0] + snap.bcmr[0]
    mass *= snap.UnitMass_in_g / param.SOLARMASS_in_g
    return mass


def get_BlackholeMassInSU(snap):
    # mass of first black hole in solar units
    if len(snap.mass[snap.type == 5]):
        mass = (snap.mass[snap.type == 5])[0]
    else:
        warnings.warn("nop black hole; setting mass to 0!")
        mass = 0
    mass *= snap.UnitMass_in_g / param.SOLARMASS_in_g
    return mass


def get_BlackholeMassSubgridInSU(snap):
    # subgrid mass of first black hole in solar units
    if "bhma" in snap.data:
        mass = snap.bhma[0]
    else:
        warnings.warn("nop black hole; setting mass to 0!")
        mass = 0
    mass *= snap.UnitMass_in_g / param.SOLARMASS_in_g
    return mass


def get_SFRFile(snap):
    # file contents from https://arepo-code.org/wp-content/userguide/diagnosticfiles.html

    # time (code units or scale factor)
    # total stellar mass to be formed in timestepo prior to stochastic sampling (code units),
    # instantaneous star formation rate of all cells (Msun/yr),
    # instantaneous star formation rate of active cells (Msun/yr),
    # total mass in stars formed in this timestep (after sampling) (code units),
    # cumulative stellar mass formed (code units).
    fileLoc = getFoldersOfSnap(snap, splitPath=False)[0] + "/" + "sfrFIXED.txt"
    if not os.path.isfile(fileLoc):
        fileLoc = getFoldersOfSnap(snap, splitPath=False)[0] + "/" + "sfr.txt"
    file = np.loadtxt(fileLoc).T
    return file


def get_CumStellarMassFormedSnap(snap, no_conversion=False):
    # get cumulative sum of mass in stars in solar masses
    mass = np.sum(snap.mass[snap.type == 4])
    if not no_conversion:
        mass *= snap.UnitMass_in_g / 1.98847e33
    return mass


def get_ColdGasMassCC(snap, no_conversion=False):
    # get cold gas mass as defined by CC accretion routine in solar masses
    if "bccm" in snap.data.keys():
        mass = np.sum(snap.bccm)
    else:
        mass = 0.0
    if not no_conversion:
        mass *= snap.UnitMass_in_g / 1.98847e33
    return mass


def get_ColdGasMassInSM(snap, INFO=None):
    # gives field with mass (in solar masses) in cells that have temperature smaller or equal than ColdGasThresholdInKelvin
    tempThreshold = checkIfInDictionary(
        "ColdGasThresholdInKelvin", INFO, "ChaoticAccretionThreshU"
    )
    if tempThreshold == "ChaoticAccretionThreshU":
        tempThreshold = snap.parameterfile["ChaoticAccretionThreshU"]
        tempThreshold *= (
            snap.UnitVelocity_in_cm_per_s ** 2
            * param.GAMMA_MINUS1
            / param.BOLTZMANN_cgs
            * param.PROTONMASS_cgs
            * param.meanmolecularweight
        )
    print(f"threshold temperature: {tempThreshold} K")
    coldM = np.zeros_like(snap.mass[snap.type == 0])
    mask = get_Temperature(snap) <= tempThreshold
    coldM[mask] = snap.mass[snap.type == 0][mask]
    coldM *= snap.UnitMass_in_g / 1.98847e33
    return coldM


def get_ColdGasDensity(snap, INFO=None, convert_units=True, considering_h=False):
    # gives field with mass in cells that have temperature smaller or equal than ColdGasThresholdInKelvin
    tempThreshold = checkIfInDictionary(
        "ColdGasThresholdInKelvin", INFO, "ChaoticAccretionThreshU"
    )
    print(f"INFO: {INFO}")
    print(f"tempThreshold: {tempThreshold}")
    if type(tempThreshold) == float:
        temp_value = tempThreshold
        mask = get_Temperature(snap) <= temp_value
    elif type(tempThreshold) == str and tempThreshold != "SF" and "tcoolmyr" not in tempThreshold:
        if type(tempThreshold) == str and "ChaoticAccretionThreshU" in tempThreshold:
            temp_value = snap.parameterfile["ChaoticAccretionThreshU"]
            temp_value *= (
                snap.UnitVelocity_in_cm_per_s ** 2
                * param.GAMMA_MINUS1
                / param.BOLTZMANN_cgs
                * param.PROTONMASS_cgs
                * param.meanmolecularweight
            )
        print(f"threshold temperature: {temp_value} K")
        mask = get_Temperature(snap) <= temp_value
        if type(tempThreshold) == str and "SF" in tempThreshold:
            mask = np.logical_or(mask, snap.sfr > 0)
            print(f"including sfr!")
    elif tempThreshold == "SF":
        mask = snap.sfr > 0
    elif "tcoolmyr" in tempThreshold:
        t_below = re.findall(r"(BELOW)(\d+[p.eE]?(?:\d+))", tempThreshold)
        t_above = re.findall(r"(ABOVE)(\d+[p.eE]?(?:\d+))", tempThreshold)
        t_cool = get_CoolingTimeInMyr(snap, INFO=INFO)
        debug.printDebug(t_cool, name="t_cool")
        mask = None
        if t_below:
            t_below = float(t_below[0][1].replace("p", "."))
            mask = t_cool < t_below
        if t_above:
            t_above = float(t_above[0][1].replace("p", "."))
            if mask is not None:
                mask = np.logical_and(mask, t_cool > t_above)
            else:
                mask = t_cool > t_above
        if "SF" not in tempThreshold:
            mask = np.logical_and(mask, snap.sfr == 0)
        print(
            f"tcoolmyr {tempThreshold} in get_ColdGasDensity: t_below: {t_below} t_above: {t_above}"
        )
    else:
        raise Exception(f"ColdGasThresholdInKelvin {tempThreshold} not understood!")
    cold_mass_density = np.zeros_like(snap.rho)
    cold_mass_density[mask] = snap.rho[mask]
    unit_density_in_cgs = 1.0
    if convert_units:
        unit_density_in_cgs *= snap.UnitMass_in_g / snap.UnitLength_in_cm ** 3
    elif considering_h:
        unit_density_in_cgs = (
            snap.UnitMass_in_g
            / snap.hubbleparam
            / snap.UnitLength_in_cm ** 3
            * snap.hubbleparam ** 3
        )
    return cold_mass_density * unit_density_in_cgs


def get_ColdGasMassWithingRadiusInSM(snap, INFO=None):
    threshold_temperature_in_kelvin = checkIfInDictionary(
        "ThresholdTemperatureInKelvin", INFO, None
    )
    cold_gas_radius_in_kpc = checkIfInDictionary("ColdGasRadiusInKpc", INFO, None)

    if threshold_temperature_in_kelvin is None or cold_gas_radius_in_kpc is None:
        raise Exception(
            f"need to define threshold_temperature_in_kelvin: "
            f"{threshold_temperature_in_kelvin} and cold_gas_radius_in_kpc: "
            f"{cold_gas_radius_in_kpc}"
        )
    print(f"threshold temperature: {threshold_temperature_in_kelvin} K")
    print(f"search radius: {cold_gas_radius_in_kpc} kpc")

    mask_radius = get_DistanceToCenterInKpc(snap) < cold_gas_radius_in_kpc
    mask_temp = get_Temperature(snap) < threshold_temperature_in_kelvin
    mask = np.logical_and(mask_temp, mask_radius)
    mass = snap.mass[snap.type == 0][mask]
    mass *= snap.UnitMass_in_g / 1.98847e33
    return np.sum(mass)


def get_AccretableGas(snap, INFO=None):
    tempThreshold = checkIfInDictionary(
        "AccGasColdGasThresholdInKelvin", INFO, "ChaoticAccretionThreshU"
    )
    if tempThreshold == "ChaoticAccretionThreshU":
        tempThreshold = snap.parameterfile["ChaoticAccretionThreshU"]
        tempThreshold *= (
            snap.UnitVelocity_in_cm_per_s ** 2
            * param.GAMMA_MINUS1
            / param.BOLTZMANN_cgs
            * param.PROTONMASS_cgs
            * param.meanmolecularweight
        )
    print(f"threshold temperature: {tempThreshold} K")
    use_sfr_criterion = checkIfInDictionary("AccGasUseSfrCrit", INFO, True)
    print(f"use_sfr_criterion: {use_sfr_criterion}")
    if tempThreshold and use_sfr_criterion:
        mask = np.logical_or(get_Temperature(snap) < tempThreshold, snap.sfr > 0)
    elif tempThreshold:
        mask = get_Temperature(snap) < tempThreshold
    elif use_sfr_criterion:
        mask = snap.sfr > 0
    else:
        raise Exception(
            f"need to define either temperature threshold {tempThreshold} "
            f"or star formation {use_sfr_criterion}!"
        )
    return mask


def get_unit_conversion(
    snap, solar=True, parsec=True, u_in_Kelvin=True, time_in_myr=True
):
    mass = snap.UnitMass_in_g
    if solar:
        mass = snap.UnitMass_in_g / param.SOLARMASS_in_g
    length = snap.UnitLength_in_cm
    if solar:
        length = snap.UnitLength_in_cm / param.KiloParsec_in_cm
    pressure = (
        snap.UnitMass_in_g
        * snap.UnitVelocity_in_cm_per_s ** 2
        / snap.UnitLength_in_cm ** 3
    )
    u = snap.UnitVelocity_in_cm_per_s ** 2
    density = snap.UnitMass_in_g / snap.UnitLength_in_cm ** 3
    masspertime = (
        snap.UnitMass_in_g
        / param.SOLARMASS_in_g
        / (snap.UnitLength_in_cm / snap.UnitVelocity_in_cm_per_s / param.SEC_PER_YEAR)
    )
    if u_in_Kelvin:
        u = (
            snap.UnitVelocity_in_cm_per_s ** 2
            * param.GAMMA_MINUS1
            / param.BOLTZMANN_cgs
            * param.PROTONMASS_cgs
            * param.meanmolecularweight
        )
    energy = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2
    momentum = snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s
    time = snap.UnitLength_in_cm / snap.UnitVelocity_in_cm_per_s
    if time_in_myr:
        time = (
            snap.UnitLength_in_cm
            / snap.UnitVelocity_in_cm_per_s
            / param.SEC_PER_MEGAYEAR
        )
    dic = {
        "mass": mass,
        "length": length,
        "pressure": pressure,
        "u": u,
        "density": density,
        "masspertime": masspertime,
        "energy": energy,
        "momentum": momentum,
        "time": time,
        "none": 1,
    }
    return dic


def get_CumAccretedMassColdAccretionInSolarMass(snap):
    mass = snap.bcam * snap.UnitMass_in_g / param.SOLARMASS_in_g
    return mass


def get_ColdCentralGasMass(snap, INFO):
    tempThreshold = checkIfInDictionary(
        "ColdGasThresholdInKelvin", INFO, "ChaoticAccretionThreshU"
    )
    radius = checkIfInDictionary("BlackholeRadius", INFO, "Nngb")
    maxSearchRadiusInKpc = checkIfInDictionary("NGBMaxSearchRadiusInKpc", INFO, None)
    if tempThreshold == "ChaoticAccretionThreshU":
        tempThreshold = snap.parameterfile["ChaoticAccretionThreshU"]
        tempThreshold *= (
            snap.UnitVelocity_in_cm_per_s ** 2
            * param.GAMMA_MINUS1
            / param.BOLTZMANN_cgs
            * param.PROTONMASS_cgs
            * param.meanmolecularweight
        )
    print(f"threshold temperature: {tempThreshold} K")
    if not re.findall(r"N(\d+)", radius):
        Nngb = checkIfInDictionary("DesNumNgbBlackHole", snap.parameterfile, radius)
    else:
        Nngb = int(re.findall(r"N(\d+)", radius)[0])
    if type(Nngb) != int:
        raise Exception(f"ngbs: {Nngb} should be int!")
    print(f"Nngb {Nngb}, maxSearchRadiusInKpc {maxSearchRadiusInKpc}")
    ids, maskNGB = findBHNGB(snap, Nngb, maxSearchRadiusInKpc)
    mass = snap.mass[snap.type == 0][maskNGB][ids]
    mask = get_Temperature(snap)[maskNGB][ids] < tempThreshold
    mass = mass[mask]
    mass *= snap.UnitMass_in_g / 1.98847e33
    return np.sum(mass)


def get_BHMdot(snap, no_conversion=False):
    """obtain [Mdot]=M_sun/year for first black hole in list"""
    mdot = snap.data["bhmd"][0]
    if not no_conversion:
        mdot *= (
            snap.UnitMass_in_g
            / (snap.UnitLength_in_cm / snap.UnitVelocity_in_cm_per_s / 365 / 3600 / 24)
            / 1.98847e33
        )
    return mdot


def get_BHMdotChaoticAccMonitor(snap, no_conversion=False):
    """obtain [BH_ChaoticAccMdotMonitor]=M_sun/year for first black hole in list"""
    mdot = snap.bcmm[0]
    if not no_conversion:
        mdot *= (
            snap.UnitMass_in_g
            / (snap.UnitLength_in_cm / snap.UnitVelocity_in_cm_per_s / 365 / 3600 / 24)
            / 1.98847e33
        )
    return mdot


def get_BHEddingtonCalculated(snap):
    """obtain [Mdot]=M_sun/year for first black hole in list"""
    mdot = (
        (snap.mass[snap.type == 5])[0]
        * 4
        * np.pi
        * param.GRAVITATIONALCONSTANT_cgs
        * param.PROTONMASS_cgs
        * param.SPEEDOFLIGHT_cgs
        / (
            param.THOMSONCROSSSECTION_cgs
            * param.SPEEDOFLIGHT_cgs ** 2
            * snap.parameterfile["BlackHoleRadiativeEfficiency"]
        )
    )
    mdot *= snap.UnitMass_in_g / 1.98847e33 * 365 * 3600 * 24
    return mdot


def get_BHEddington(snap, no_conversion=False):
    """obtain [M_eddington]=M_sun/year for first black hole in list"""
    mdot = snap.bhed[0]
    if not no_conversion:
        mdot *= (
            snap.UnitMass_in_g
            / (snap.UnitLength_in_cm / snap.UnitVelocity_in_cm_per_s / 365 / 3600 / 24)
            / 1.98847e33
        )
    return mdot


def get_BHAgnPower(snap, info=None):
    """obtain P_agn=e_rad*e_feed*m_dot*c^2 in erg/s for first black hole in list"""
    mdot = get_BHMdot(snap, no_conversion=True)
    if "BH_JET" in snap.configfile:
        factor = snap.parameterfile["BH_JET_FeedbackFactor"]
        if "BH_ACCRETION_DEVELOP" in snap.configfile:
            if "ChaoticAccretionEta" in snap.parameterfile:
                factor *= snap.parameterfile["ChaoticAccretionEta"]
    elif "BH_ADIOS_WIND" in snap.configfile:
        factor = (
            snap.parameterfile["BlackHoleRadiativeEfficiency"]
            * snap.parameterfile["RadioFeedbackFactor"]
        )
        if (
            "BH_ACCRETION_DEVELOP" in snap.configfile
            and "ChaoticAccretionEta" in snap.parameterfile
        ):
            factor = snap.parameterfile["ChaoticAccretionEta"]
    else:
        print("no feedback factor found -> setting to 1")
        factor = 1
    unit_mass_per_time = (
        snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s / snap.UnitLength_in_cm
    )
    return factor * mdot * unit_mass_per_time * param.SPEEDOFLIGHT_cgs ** 2


def get_BHAgnPowerLogFile(folder):
    import FigureMove as Fig

    """obtain P_agn=e_rad*e_feed*m_dot*c^2 in erg/s for total mdot in log file"""
    snap_num = Fig.get_number_snapshots(
        folder, snapbase="snap_", snapdirbase="snapdir_",
    )[0]
    snap = Fig.quickImport(snap_num, folder=folder, onlyHeader=True)

    mdot = get_blackholes_logfile(
        folder, variables=["total_mdot_in_sun_year"], return_dictionary=False, info=None
    )
    mdot = np.array(mdot) * param.SOLARMASS_in_g / param.SEC_PER_YEAR
    if "BH_JET" in snap.configfile:
        factor = snap.parameterfile["BH_JET_FeedbackFactor"]
        if "BH_ACCRETION_DEVELOP" in snap.configfile:
            if "ChaoticAccretionEta" in snap.parameterfile:
                factor *= snap.parameterfile["ChaoticAccretionEta"]
    elif "BH_ADIOS_WIND" in snap.configfile:
        factor = (
            snap.parameterfile["BlackHoleRadiativeEfficiency"]
            * snap.parameterfile["RadioFeedbackFactor"]
        )
        if (
            "BH_ACCRETION_DEVELOP" in snap.configfile
            and "ChaoticAccretionEta" in snap.parameterfile
        ):
            factor = snap.parameterfile["ChaoticAccretionEta"]
    else:
        print("no feedback factor found -> setting to 1")
        factor = 1
    return factor * mdot * param.SPEEDOFLIGHT_cgs ** 2


def get_StarFormationRate(snap):
    """obtain [sfr]=M_sun/year for all stars"""
    sfr = snap.sfr
    return sfr


def get_StarFormationRateTotal(snap, info=None):
    """obtain [sfr]=M_sun/year for all stars"""
    sfr = np.sum(snap.sfr)
    return sfr


def get_TreeNGB(snap, partType=0):
    tree = scipy.spatial.KDTree(
        list(
            zip(
                snap.pos[:, 0][snap.type == 0].ravel(),
                snap.pos[:, 1][snap.type == 0].ravel(),
                snap.pos[:, 2][snap.type == 0].ravel(),
            )
        )
    )
    return tree


def get_TreeNGB_FAST(snap, partType=0, maxSearchRadiusInKpc=10):
    # code_to_kpc = snap.UnitLength_in_cm/param.KiloParsec_in_cm
    # radius = maxSearchRadiusInKpc/code_to_kpc
    mask = get_DistanceToCenterInKpc(snap) < maxSearchRadiusInKpc
    print(f"radius {maxSearchRadiusInKpc}")
    tree = scipy.spatial.KDTree(
        list(
            zip(
                snap.pos[:, 0][snap.type == 0][mask].ravel(),
                snap.pos[:, 1][snap.type == 0][mask].ravel(),
                snap.pos[:, 2][snap.type == 0][mask].ravel(),
            )
        )
    )
    return tree, mask


def findNGBTree(pos, tree, Nngb=1):
    dists, ids = tree.query(pos, k=Nngb)
    return dists, ids


def findBHNGB(snap, Nngb, maxSearchRadiusInKpc):
    # assuming single BH
    tree, mask = get_TreeNGB_FAST(
        snap, partType=0, maxSearchRadiusInKpc=maxSearchRadiusInKpc
    )
    bh_pos = snap.pos[snap.type == 5]
    if len(snap.pos[snap.type == 5]) == 0:
        bh_pos = np.array([snap.center])
    dists, ids = findNGBTree(bh_pos, tree, Nngb=Nngb)
    if len(np.unique(ids)) != Nngb:
        raise Exception(
            f"increase search radius!, coulnt find {Nngb} ngbs within {maxSearchRadiusInKpc} kpc!"
        )
    return ids[0], mask


def get_EnclosedMass(snap, include=["NFW", "ISO", "BH"], verbose=1):
    # calculates enclosed mass as function radius r for potential active in simulation and if chosen to be 'included' in calculation
    nfw = lambda r: r * 0
    if "STATICNFW" in snap.configfile.keys() and "NFW" in include:
        NFW_C = snap.configfile["NFW_C"]
        NFW_M200 = (
            snap.configfile["NFW_M200"] * snap.UnitMass_in_g / snap.hubbleparam
        )  # correcting for hubble factor
        rho0Rs3 = NFW_M200 / (
            4.0 * np.pi * (np.log(1 + NFW_C) - NFW_C / (1 + NFW_C))
        )  # corresponds to term rho_s*r_s**3
        # scaling radius corresponds to r200/c; r200=(NFW_M200*G/(100 H**2))**(1/3)
        r200 = (
            NFW_M200
            * param.GRAVITATIONALCONSTANT_cgs
            / (100 * (snap.hubbleparam * param.HUBBLECONSTANT100_cgs) ** 2)
        ) ** (1.0 / 3)
        r_s = r200 / NFW_C
        nfw = (
            lambda r: 4.0 * np.pi * rho0Rs3 * (np.log((r_s + r) / r_s) - r / (r_s + r))
        )
        if verbose > 0:
            print(
                "nfw assuming M200 %f 10^10 Msun, R200 %f kpc, concentration %f"
                % (
                    NFW_M200 / param.SOLARMASS_in_g / 1e10,
                    r200 / param.KiloParsec_in_cm,
                    NFW_C,
                )
            )
    iso = lambda r: r * 0
    if "STATICISO" in snap.configfile.keys() and "ISO" in include:
        ISO_R200 = (
            snap.configfile["ISO_R200"] * snap.UnitLength_in_cm / snap.hubbleparam
        )
        ISO_M200 = (
            snap.configfile["ISO_M200"] * snap.UnitMass_in_g / snap.hubbleparam
        )  # correcting for hubble factor
        # sigma2DIVgravtitioanlconst = ISO_M200/ISO_R200
        # sigma**2=kT/m with mean mass of partiles constituting the sphere
        # clip values if exceed r200
        iso = lambda r: 2 * ISO_M200 * np.clip(r / ISO_R200, 0, 1)
        # T_iso = sigma**2*m/k = G*M200/ (2*r200)*m/k
        sigma2 = param.GRAVITATIONALCONSTANT_cgs * ISO_M200 / (2.0 * ISO_R200)
        tempISO = (
            sigma2
            * param.meanmolecularweight
            * param.PROTONMASS_cgs
            / param.BOLTZMANN_cgs
        )
        if verbose > 0:
            print(
                "iso assuming M200 %f 10^10 Msun, R200 %f kpc, corresponding temperature %f K (%f keV), sigma %f km/s"
                % (
                    ISO_M200 / param.SOLARMASS_in_g / 1e10,
                    ISO_R200 / param.KiloParsec_in_cm,
                    tempISO,
                    tempISO * param.BOLTZMANN_keV,
                    np.sqrt(sigma2) / 1e5,
                )
            )
    bh = lambda r: r * 0
    if np.size(snap.mass[snap.type == 5]) > 0 and "BH" in include:
        bhmass = snap.mass[snap.type == 5][0] * snap.UnitMass_in_g
        bh = lambda r: bhmass + r * 0  # h should be converted already!
        if verbose > 0:
            print(
                "First BH in simulation with mass %f 10^10 Msun"
                % (bhmass / param.SOLARMASS_in_g / 1e10)
            )
    potential = lambda r: nfw(r) + iso(r) + bh(r)
    return lambda r: potential(r)


def _get_FreeFallTime(snap, r, include=["NFW", "ISO", "BH"], verbose=1):
    # calculates free fall time in s
    # assumes r in cgs (cm)
    # t_ff=sqrt(2r/g)
    # g = GM(<r)/r**2
    # t_ff[Myr] =
    r = np.array(r, dtype=np.float64)
    Menc = get_EnclosedMass(snap, include=include, verbose=verbose)
    g = param.GRAVITATIONALCONSTANT_cgs * Menc(r) / r ** 2
    t_ff = np.sqrt(2 * r / g)
    return t_ff


def get_FreeFallTimeInMyr(
    snap, considering_h=False, no_conversion=False, INFO=None, verbose=1
):
    radius = get_DistanceToCenter(
        snap, considering_h=considering_h, no_conversion=no_conversion, INFO=INFO
    )
    t_ff = _get_FreeFallTime(
        snap, radius, include=["NFW", "ISO", "BH"], verbose=verbose
    )
    t_ff /= param.SEC_PER_MEGAYEAR
    return t_ff


def get_XrayCoolingFreeFallTimeRatio(
    snap, considering_h=False, no_conversion=False, INFO=None, verbose=1
):
    tff = get_FreeFallTimeInMyr(
        snap,
        considering_h=considering_h,
        no_conversion=no_conversion,
        INFO=INFO,
        verbose=verbose,
    )
    tX = get_XrayCoolingTimescaleInMyr(snap, considering_h=considering_h)
    return tX / tff


def get_BondiMdot(snap, INFO=None):
    # retuns sum_i  \dot{M}=2*np.pi*G**2*M_BH**2*rho_inf/c_inf**3 within radius
    # units M_sun/year
    boosted = checkIfInDictionary(
        "BondiBlackHoleAccretionFactor", INFO, "parameterfile"
    )
    if boosted == "parameterfile":
        boosted = checkIfInDictionary(
            "BlackHoleAccretionFactor", snap.parameterfile, boosted
        )
    assert type(boosted) != str, f"boosted cant be ste string {boosted}"
    radiativeEfficiency = checkIfInDictionary(
        "BlackHoleRadiativeEfficiency", INFO, "parameterfile"
    )
    if radiativeEfficiency == "parameterfile":
        radiativeEfficiency = checkIfInDictionary(
            "BlackHoleRadiativeEfficiency", snap.parameterfile, radiativeEfficiency
        )
    print(f"Bondi: boosted (BondiBlackHoleAccretionFactor) by {boosted}")
    print(f"Bondi: reduced (1-BlackHoleRadiativeEfficiency) by {1-radiativeEfficiency}")
    print("BONDI: assuming first BH gives mass")
    mdot = (
        (1 - radiativeEfficiency)
        * boosted
        * 4
        * np.pi
        * param.GRAVITATIONALCONSTANT_cgs ** 2
        * (snap.mass[snap.type == 5][0] * snap.UnitMass_in_g) ** 2
        * get_Density(snap)
        / get_SoundSpeed(snap) ** 3
    )
    s = np.log10(mdot)  # bug test

    return mdot / param.SOLARMASS_in_g * 365 * 3600 * 24


def get_BHhsml(snap, convert_units=True, INFO=None):
    # looks at first black hole and returns "radius" of sphere of influence of black hole BH_hsml
    radius = snap.bhhs[0]
    units = checkIfInDictionary("UnitsBHhsml", INFO, "kpc")
    print(f"units of BHhsml: {units}")
    if convert_units:
        if units == "Mpc":
            radius *= snap.UnitLength_in_cm / param.MegaParsec_in_cm
        elif units == "kpc":
            radius *= snap.UnitLength_in_cm / param.KiloParsec_in_cm
        else:
            raise Exception(f"units {units} not implemented")
    return radius


def get_Nngb_within_BHhsml(snap):
    # looks at first black hole and returns "radius" of sphere of influence of black hole BH_hsml
    dic = {}
    dic["UnitsBHhsml"] = "kpc"
    radius = get_BHhsml(snap, convert_units=True, INFO=dic)
    dist = get_DistanceToCenterInKpc(snap)
    Nngb = len(dist[dist < radius])
    return Nngb


def get_BHNgbRadius(snap, INFO=None):
    max_radius_ngb_search_in_kpc = checkIfInDictionary(
        "MaxRadiusNgbSearchInKpc", INFO, None
    )
    number_ngb = checkIfInDictionary("NumberNgb", INFO, "DesNumNgbBlackHole")
    if number_ngb == "DesNumNgbBlackHole":
        number_ngb = checkIfInDictionary(
            "DesNumNgbBlackHole", snap.parameterfile, 64
        )  # snap.parameterfile["DesNumNgbBlackHole"]
    if type(number_ngb) != int:
        raise Exception(f"ngbs: {number_ngb} should be int!")
    ids, mask_ngb = findBHNGB(snap, number_ngb, max_radius_ngb_search_in_kpc)
    radius = get_DistanceToCenterInKpc(snap)[mask_ngb][ids]
    return np.max(radius)


def get_BondiMdotAroundBH(snap, INFO=None):
    # returns \dot{M}=2*np.pi*G**2*M_BH**2*rho_inf/c_inf**3
    # units M_sun/s
    radius = checkIfInDictionary(
        "BondiAccretionRadius", INFO, "BH_Hsml"
    )  # BH_Hsml Nngb
    maxSearchRadiusInKpc = checkIfInDictionary("NGBMaxSearchRadiusInKpc", INFO, None)
    if "Nngb" in radius:
        if not re.findall(r"N(\d+)", radius):
            Nngb = checkIfInDictionary("DesNumNgbBlackHole", snap.parameterfile, radius)
        else:
            Nngb = int(re.findall(r"N(\d+)", radius)[0])
        if type(Nngb) != int:
            raise Exception(f"ngbs: {Nngb} should be int!")
        ids, maskNGB = findBHNGB(snap, Nngb, maxSearchRadiusInKpc)
        mdot = get_BondiMdot(snap, INFO)[maskNGB][ids]
    elif radius == "BH_Hsml":
        radius = snap.bhhs[0] * snap.UnitLength_in_cm / param.KiloParsec_in_cm
        mask = get_DistanceToCenterInKpc(snap) < radius
        mdot = get_BondiMdot(snap, INFO)[mask]
    else:
        raise Exception(f"search radius {radius} not understood!")
    print(f"mdot bondi: {mdot}, radius: {radius}")
    s = np.log10(mdot)  # debug test
    return np.average(mdot)


def get_BondiEddingtonRatio(snap, INFO):
    bondi = get_BondiMdotAroundBH(snap, INFO=INFO)
    eddington = get_BHEddington(snap, no_conversion=False)
    return bondi / eddington


def get_LiCCMDot(snap, INFO=None):
    # looking at first blackhole!
    temp = get_Temperature(snap)  # [snap.type==0]
    mass = get_Mass(snap)  # [snap.type==0]
    threshTemp = checkIfInDictionary("LiCCMDotThreshTempInK", INFO, 2e4)
    useConstantRadius = checkIfInDictionary("LiCCMDotUseConstRad", INFO, "BH_Hsml")
    accTime = checkIfInDictionary("LiCCMDotAccTimeInMyr", INFO, 5)
    useConstantTime = checkIfInDictionary("LiCCMDotUseConstAccTime", INFO, False)
    UseSpecificNNgb = checkIfInDictionary("LiCCMDotUseSpecificNNgb", INFO, False)
    use_sfr_criterion = checkIfInDictionary("LiCCMDotUseSfrCrit", INFO, True)
    maxSearchRadiusInKpc = checkIfInDictionary("NGBMaxSearchRadiusInKpc", INFO, 20)
    print("cooling below %g K" % threshTemp)
    accRadius = checkIfInDictionary("LiCCMDotAccRadiusInKpc", INFO, 0.5)
    if useConstantRadius == "BH_Hsml":
        accRadius = snap.bhhs[0] * snap.UnitLength_in_cm / param.KiloParsec_in_cm
    if not useConstantTime or useConstantRadius:
        distInKpc = get_DistanceToCenterInKpc(snap)
    if UseSpecificNNgb:
        Nngb = UseSpecificNNgb
    else:
        Nngb = snap.parameterfile["DesNumNgbBlackHole"]
    if not useConstantRadius:
        ids, maskNGB = findBHNGB(snap, Nngb, maxSearchRadiusInKpc)
        print("find %i nearst ngb" % Nngb)
        if use_sfr_criterion:
            mask = np.logical_or(
                temp[maskNGB][ids] < threshTemp, snap.sfr[maskNGB][ids] > 0
            )
        else:
            mask = temp[maskNGB][ids] < threshTemp

    if not useConstantTime:
        if not useConstantRadius:
            accTime = _get_FreeFallTime(
                snap, distInKpc[maskNGB][ids][mask] * param.KiloParsec_in_cm
            )
            accTime /= 365 * 24 * 3600
        else:
            print(f"looking at inidividual times!")
            if use_sfr_criterion:
                mask = np.logical_or(temp < threshTemp, snap.sfr > 0)
            else:
                mask = temp < threshTemp
            mask = np.logical_and(distInKpc < accRadius, mask)
            accTime = _get_FreeFallTime(snap, distInKpc[mask] * param.KiloParsec_in_cm)
            accTime /= 365 * 24 * 3600
    else:
        accTime = accTime * param.SEC_PER_MEGAYEAR / (365 * 24 * 3600)

    if not useConstantRadius:
        mass = mass[maskNGB][ids][mask] / param.SOLARMASS_in_g
    else:
        print("assuming const radius for get_LiCCMDot")
        if use_sfr_criterion:
            mask = np.logical_or(temp < threshTemp, snap.sfr > 0)
        else:
            mask = temp < threshTemp
        mask = np.logical_and(distInKpc < accRadius, mask)
        mass = mass[mask] / param.SOLARMASS_in_g

    # if len(mass) != len(accTime):
    # print(f"accTime: {accTime}")
    if type(accTime) == float:
        mdot = np.sum(mass) / np.average(accTime)
    else:
        print(f"mass and time per cell")
        mdot = np.sum(mass / accTime)
    print(f"Li Mdot {mdot}")
    return mdot


def numberElementsArray(array_to_convert):
    LenA = len(array_to_convert)
    if LenA <= 1:
        return array_to_convert
    elementPrev = array_to_convert[0]
    count = 1
    new = []
    for index, element in enumerate(array_to_convert[1:]):
        if element == elementPrev:
            count += 1
        else:
            if count == 1:
                new.append(elementPrev)
            else:
                new.append("%ix" % count)
                new.append(elementPrev)
            count = 1
            elementPrev = element
    if count == 1:
        new.append(element)
    else:
        new.append("%ix" % count)
        new.append(elementPrev)

    return np.array(new)


def cps(
    array_to_convert,
    string=False,
    convertInt=False,
    Round=None,
    replaceDot=True,
    joinSymbol="-",
    checkUnique=False,
    efficientNumber=True,
    replace=["."],
    replace_with="p",
):
    return convert_params_to_string(
        array_to_convert,
        string=string,
        convertInt=convertInt,
        Round=Round,
        replaceDot=replaceDot,
        joinSymbol=joinSymbol,
        checkUnique=checkUnique,
        efficientNumber=efficientNumber,
        replace=replace,
        replace_with=replace_with,
    )


def convert_params_to_string(
    array_to_convert,
    string=False,
    convertInt=False,
    Round=None,
    replaceDot=True,
    joinSymbol="-",
    checkUnique=False,
    efficientNumber=True,
    replace=["."],
    replace_with="p",
    reduce_same_starting_string=False,
):
    # Round: only show #Round many number of signifcant digits
    # print(f"array_to_convert: {array_to_convert}")
    if not isIterable(array_to_convert):
        array_to_convert = [array_to_convert]
    if array_to_convert is None or (
        len(array_to_convert) == 1 and array_to_convert[0] is None
    ):
        return "None"
    elif np.size(array_to_convert) == 0:
        return ""
    array_to_convert = np.array(array_to_convert).flatten()
    if any([type(x) == np.str_ for x in array_to_convert]):
        string = True
    if efficientNumber:
        checkUnique = True
    if not string:
        if Round is not None:
            array_to_convert = [
                (round(x, -int((np.log10(abs(x)))) + Round))
                if (x != 0 and type(x) != np.bool_ and x is not None)
                else x
                for x in np.array(array_to_convert)
            ]
        if not convertInt:
            if replaceDot:
                array_to_convert = [
                    ReplaceDotInName(
                        x, "%g", replace=replace, replace_with=replace_with
                    )
                    for x in array_to_convert
                    if x is not None
                ]
                array_to_convert = [x.replace(" ", "") for x in array_to_convert]
                # resulting_string = joinSymbol.join(map(str,tuple(array_to_convert)))
            else:
                resulting_string = []
                for i in range(np.size(array_to_convert)):
                    resulting_string.append(str(array_to_convert[i]))
                resulting_string = tuple(resulting_string)
                # resulting_string = joinSymbol.join(resulting_string)
        else:
            resulting_string = []
            for i in range(np.size(array_to_convert)):
                resulting_string.append(str(int(array_to_convert[i])))
            resulting_string = tuple(resulting_string)
            # resulting_string = joinSymbol.join(resulting_string)

    else:
        array_to_convert = [
            x.replace(" ", "") if x is not None else str(x) for x in array_to_convert
        ]
        if replaceDot:
            array_to_convert = [
                ReplaceDotInName(x, "%s", replace=replace, replace_with=replace_with)
                for x in array_to_convert
            ]

        # resulting_string = joinSymbol.join(map(str,tuple(array_to_convert)))
    if checkUnique:
        if np.size(np.unique(array_to_convert)) == 1:
            array_to_convert = [array_to_convert[0]]
            efficientNumber = False
    if efficientNumber:
        array_to_convert = numberElementsArray(array_to_convert)
    if string or (not string and not convertInt):
        resulting_string = joinSymbol.join(map(str, tuple(array_to_convert)))
    else:
        resulting_string = joinSymbol.join(resulting_string)
    if reduce_same_starting_string:
        if not string:
            raise Exception(f"string={string}!=True which is required necessary!")
        reduced_name = string_manipulation.reduce_same_starting_string(
            resulting_string.split(joinSymbol)
        )
        if reduced_name:
            resulting_string = reduced_name
    if all([s == resulting_string[0] for s in resulting_string]) and isIterable(
        resulting_string
    ):
        return resulting_string[0]
    return resulting_string


def INFOAddKey(INFO, keyword, value):
    for dic in INFO.flatten():
        dic[keyword] = value
    return INFO


def INFOSorter(INFO, FinalDimension=[1, 1]):
    # specify FinalDimension as list even if 1D required
    # give index of INFO as initial numbers sepeterated by '-' and followed up by '_' before actual keyword '12-23_KEYWhatever'
    D1 = False
    if np.size(FinalDimension) == 1:
        D1 = True
        FinalDimension.append(1)
    INFOArray = np.array(
        [
            np.array([{} for i in range(FinalDimension[1])])
            for i in range(FinalDimension[0])
        ],
        dtype=object,
    )
    for key in list(INFO.keys()):
        oldKey = "%s" % (key)
        if "-" in key:
            nims = []  # np.zeros(2, dtype=object)
            numbers = key.split("_")[0].split("-")
            for index, num in enumerate(numbers):
                if num == "":
                    # nims
                    nims.append(
                        slice(0, FinalDimension[index])
                    )  # [index] = slice(0,FinalDimension[index])
                else:
                    nims.append(slice(int(num), int(num) + 1))  # nims[index] = int(num)
            key = "".join(key.split("_")[1:])
        else:
            nims = (slice(0, FinalDimension[0]), slice(0, FinalDimension[1]))
        for dics in INFOArray[tuple(nims)]:
            for dic in dics:
                dic[key] = INFO[oldKey]
    if D1:
        INFOArray = INFOArray.flatten()
    return INFOArray


#####
##### class definitions
#####
##### this class should contain all information that is needed for the plots
##### ideally, the param file is not needed in ploting routine


class SimulationProperties:
    def __init__(self, path, times):
        self.path = path
        self.snappath = ""
        self.NumberOfFiles = 0
        self.times = times
        self.i = 0

        ## path to logfiles
        self.energy_txt_path = self.path + "energy.txt"

        ## slice and projection properties
        self.res = param.res
        self.ProjBox = param.ProjBox
        self.ProjCenter = param.ProjCenter
        self.ProjAxis = 3 - np.sum(param.axis)
        self.axis = param.axis
        self.Center = param.center

        if hasattr(param, "plotDirectory"):
            self.plotDirectory = param.plotDirectory
        else:
            self.plotDirectory = "../figures/"

    ## execute this function to update the snappath
    def updateIndex(self, i):
        self.i = i
        ##
        if len(self.times) >= i:
            i_time = self.times[i]
        else:
            print("self.times not long enough")
            raise
        snappath = self.path + "snap_%03d.hdf5" % i_time
        try:
            sn = arepo.Simulation(snappath, onlyHeader=True)
            self.NumberOfFiles = 1
            self.snappath = snappath  ## path to snapshot data
            sn.close()
        except:
            snappath = (
                param.FileDirectory
                + "snapdir_%03d/" % i_time
                + "snap_%03d.0.hdf5" % i_time
            )
            try:
                sn = arepo.Simulation(snappath, onlyHeader=True)
                self.NumberOfFiles = np.int(sn.header.NumFilesPerSnapshot)
                self.snappath = snappath  ## path to snapshot data
                sn.close()
            except:
                print(
                    (
                        "SimulationProperties: error: could neither open \n "
                        + param.FileDirectory
                        + "snap_%03d.hdf5" % i_time
                        + " nor \n"
                        + param.FileDirectory
                        + "snapdir_%03d/" % i_time
                        + "snap_%03d.0.hdf5" % i_time
                    )
                )
                raise

    def snapIndex(self):
        return self.times[self.i]


def resolution_calculator_projections(res, ProjBox):
    if np.size(res) == 1:
        pass
    else:
        return np.array(res)
    res = [int(res * (x) / np.max(ProjBox)) for x in ProjBox]
    if any([True if x == 0 else False for x in res]):
        print(res)
        print(ProjBox)
        raise Exception(
            f"resolution {res} for projection has zero in one dimension, "
            f"increase resolution or box {ProjBox} in respective dimension, "
            f"need minimum res of {math.ceil(np.max(ProjBox)/np.min(ProjBox))}"
        )
    return res


def plot_contour(
    axes,
    snap,
    variable,
    axis,
    box,
    center,
    levels=None,
    numthreads=1,
    colors="k",
    projection=False,
    projBox=None,
    projAxis=None,
    linewidths=1,
    useweights=False,
    weights=None,
    considering_h=False,
    res=1024,
    INFO=None,
    Filter=None,
    filternum=None,
    ReturnValues=False,
    alpha=None,
    capAt=None,
    sumAlongAxis=True,
):
    print(snap)
    print(variable)
    print(axis)
    print(box)
    print(center)
    print(levels)
    print(projection)
    print(projBox)
    print(projAxis)
    print(useweights)
    print(weights)
    print(considering_h)
    print(INFO)
    print(Filter)
    print(filternum)
    print(alpha)
    print(ReturnValues)
    # exit()
    print("starting contour plot")
    StartTime = time.time()
    if projection:
        if projBox is None:
            exit("need to specifiy box of projection for contours if using projections")
        if projAxis is None:
            projAxis = 3 - np.sum(axis)
        if useweights:
            if weights is None:
                exit(
                    "need to specifiy weight of projection for contours if using weights for projections"
                )
    if isinstance(variable, str):
        VALUES = get_value(variable, snap, considering_h, INFO=INFO)
    else:
        VALUES = variable
    VALUES = VALUES.copy()
    if projection:
        import FigureMove as Fig

        if type(res) != list:
            resProj = resolution_calculator_projections(res, projBox)
        else:
            resProj = res
        print(resProj)
        print(box)
        print(projBox)

        # exit()
        if useweights:
            if isinstance(weights, str):
                import FigureMove as Fig

                WEIGHTS = Fig.getWeights(
                    snap,
                    weights,
                    considering_h=False,
                    INFO=INFO,
                    useWeightsThresholds=False,
                )
            else:
                WEIGHTS = weights
            print(("proj box contour", projBox))

            SLICE2 = Fig.getAgrid(
                snap,
                WEIGHTS,
                res=resProj,
                ProjBox=projBox,
                center=center,
                numthreads=numthreads,
            )
            grid2 = np.sum(SLICE2, axis=projAxis)
            SLICE = Fig.getAgrid(
                snap,
                VALUES * WEIGHTS,
                res=resProj,
                ProjBox=projBox,
                center=center,
                numthreads=numthreads,
            )
            Z = np.sum(SLICE, axis=projAxis)
            Z /= grid2
            if axis[1] > axis[0]:
                Z = Z.T
        else:
            if sumAlongAxis:
                print("contour: sum along axis")
                SLICE = Fig.getAgrid(
                    snap,
                    VALUES,
                    res=resProj,
                    ProjBox=projBox,
                    center=center,
                    numthreads=numthreads,
                )
                Z = np.sum(SLICE, axis=projAxis)
                spacing = resolution_calculator_projections(res, projBox)[projAxis]
                if checkExistenceKey(INFO, "ignoreSpacingProj", False):
                    spacing = 1.0
                ProjLength = projBox[projAxis] / spacing
                if checkExistenceKey(INFO, "ConvertProjCM", True):
                    print("converting projected distance to cm!")
                    ProjLength *= snap.UnitLength_in_cm
                Z = np.array(Z) * float(ProjLength)
            else:
                print("contour: calculate mean")
                Z = np.mean(SLICE, axis=projAxis)
            if capAt is not None:
                Z[Z > capAt] = capAt
            if axis[1] > axis[0]:
                Z = Z.T

        if Filter == "median":
            Z = ndimage.median_filter(Z, filternum)
        elif Filter == "gaussian":
            Z = ndimage.gaussian_filter(Z, sigma=filternum)
        elif Filter == "zoom":
            Z = ndimage.zoom(Z, filternum)
        if Filter is not None:
            print("setting all values <=0 to >0!")
            Z[Z <= 0] = np.min(Z[Z > 0])
        print(axis)
        print(resProj)
        print(projBox)
        resx = resProj[axis[0]]
        resy = resProj[axis[1]]

        dx = 1.0 * box[0] / resx
        dy = 1.0 * box[1] / resy
        centersX = np.linspace(
            center[axis[0]] - 0.5 * box[0] + 0.5 * dx,
            center[axis[0]] + 0.5 * box[0] - 0.5 * dx,
            resx,
        )
        centersY = np.linspace(
            center[axis[1]] - 0.5 * box[1] + 0.5 * dy,
            center[axis[1]] + 0.5 * box[1] - 0.5 * dy,
            resy,
        )
        X, Y = np.meshgrid(centersX, centersY)
    else:
        SLICE = snap.get_Aslice(
            VALUES, axis=axis, box=box, center=center, numthreads=numthreads
        )
        Z = SLICE["grid"].T
        if Filter == "median":
            Z = ndimage.median_filter(Z, filternum)
        elif Filter == "gaussian":
            Z = ndimage.gaussian_filter(Z, sigma=filternum)
        elif Filter == "zoom":
            Z = ndimage.zoom(Z, filternum)
        if Filter is not None:
            Z[Z <= 0] = np.min(Z[Z > 0])
        X, Y = np.meshgrid(SLICE["x2"], SLICE["y2"])
    if np.shape(Z) != np.shape(X):
        print(axis)
        print(box)
        print(projAxis)
        print(projBox)
        print(box)
        print(resx)
        print(resy)
        exit("shapes of Z and X dont match")
    print(("contour plotting took %g" % (StartTime - time.time())))
    if ReturnValues:
        return X, Y, Z, levels, colors, linewidths
    CS = axes.contour(
        X, Y, Z, levels, colors=colors, linewidths=linewidths, alpha=alpha
    )


def get_contour_data(
    data,
    axis,
    box,
    center=None,
    filter=None,
    filter_num=None,
    max_value=None,
    info=None,
    return_dic=False,
):
    from string_manipulation import dictionary_from_split

    if filter is not None:
        parameter_dic = dictionary_from_split(filter)
        filter_num = parameter_dic["filternum"]
        filter = parameter_dic["filter"]

    # filter_num = checkExistenceKey(info, "filter_num", filter_num)
    # max_value = checkExistenceKey(info, "max_value", max_value)

    if max_value is not None:
        data[data > max_value] = max_value
    if filter is not None:
        if filter == "median" or filter == 1:
            data = ndimage.median_filter(data, filter_num)
        elif filter == "gaussian" or filter == 2:
            data = ndimage.gaussian_filter(data, sigma=filter_num)
        elif filter == "zoom" or filter == 3:
            data = ndimage.zoom(data, filter_num)
        # if "non_zero" in filternum:
        #     print("setting all values <=0 to >0!")
        #     data[data <= 0] = np.min(data[data > 0])
    if len(box) == 3:
        box_x = box[axis[0]]
        box_y = box[axis[1]]
    elif len(box) == 2:
        box_x = box[0]
        box_y = box[1]
    else:
        raise Exception(f"box: {box} should have 2 or 3 entries!")

    resy, resx = np.shape(data)

    dx = 1.0 * box_x / resx
    dy = 1.0 * box_y / resy

    centersX = np.linspace(
        center[axis[0]] - 0.5 * box_x + 0.5 * dx,
        center[axis[0]] + 0.5 * box_x - 0.5 * dx,
        resx,
    )
    centersY = np.linspace(
        center[axis[1]] - 0.5 * box_y + 0.5 * dy,
        center[axis[1]] + 0.5 * box_y - 0.5 * dy,
        resy,
    )

    X, Y = np.meshgrid(centersX, centersY)

    if np.shape(data) != np.shape(X):
        raise Exception(
            f"shapes of ({np.shape(data)}) data and X ({np.shape(X)}) don't match"
        )
    if return_dic:
        return {"X": X, "Y": Y, "Z": data}
    return X, Y, data


def get_quiver_stream_data(
    U,
    V,
    box,
    center,
    axis,
    skipN=0,
    info=None,
    normalize=None,
    normalize_length=False,
    return_dic=False,
):
    if np.shape(U) != np.shape(V):
        raise Exception("U and V need to have same shape!")

    colorbar_vmax = checkIfInDictionary("colorbar_vmax", info, None)

    resy, resx = np.shape(U)
    if len(box) == 3:
        box_x = box[axis[0]]
        box_y = box[axis[1]]
    elif len(box) == 2:
        box_x = box[0]
        box_y = box[1]
    else:
        raise Exception(f"box: {box} should have 2 or 3 entries!")
    dx = 1.0 * box_x / resx
    dy = 1.0 * box_y / resy

    centers1 = np.linspace(
        center[axis[0]] - 0.5 * box_x + 0.5 * dx,
        center[axis[0]] + 0.5 * box_x - 0.5 * dx,
        resx,
    )
    centers2 = np.linspace(
        center[axis[1]] - 0.5 * box_y + 0.5 * dy,
        center[axis[1]] + 0.5 * box_y - 0.5 * dy,
        resy,
    )
    X, Y = np.meshgrid(centers1, centers2)
    if skipN == 0:
        pass
    elif isinstance(skipN, (int, np.int64)) and skipN > 0:
        X = X[::skipN, ::skipN]
        Y = Y[::skipN, ::skipN]
        U = U[::skipN, ::skipN]
        V = V[::skipN, ::skipN]
    else:
        print(type(skipN), skipN > 0)
        raise Exception("if want to use QuiverSkipN, need integer greater than 0!")
    cc = np.sqrt(U ** 2 + V ** 2)
    if normalize is None:
        cc_average = 1.0
    elif normalize == "average":
        cc_average = np.average(cc)
    elif normalize == "max":
        cc_average = np.max(cc) / colorbar_vmax  # 6.
    else:
        raise Exception(f"normalize: {normalize} not understood")

    if cc_average == 0:
        cc_average = 1.0
        cc[:] = 1.0
        U[:] = 1.0
        V[:] = 1.0
        print(
            "QUIVER PLOT: changing average and total value to 1 as otherwise divide by inf"
        )
    if False:
        print(f"we are normalizing quiver")
        norm_fac = np.sqrt(U ** 2 + V ** 2)
        U /= norm_fac
        V /= norm_fac
        scale = (resx + resy) / 2.0 / 2

    if return_dic:
        return {"U": U, "V": V, "X": X, "Y": Y, "cc_average": cc_average}
    return U, V, X, Y, cc_average


def plot_quiver_stream(
    snap,
    box,
    axis,
    center,
    res,
    variable,
    axes,
    figure,
    AlternativeBfieldFlag=False,
    numthreads=4,
    considering_h=False,
    colorbarMin=None,
    colorbarMax=None,
    projection=True,
    use_weights=False,
    weights=None,
    projBox=None,
    projAxis=None,
    use_normalization=False,
    normalization_value=None,
    scale=None,
    cmap="bone",
    alpha=1.0,
    use_max=False,
    lengthnormalise=False,
    axoffset=0.005,
    plotyourself=False,
    useWeightsThresholds=True,
    INFO=None,
    QuiverSkipN=0,
    sumAlongAxis=True,
):
    print(("using projections for quiver stream %i" % int(projection)))
    if box != [projBox[axis[0]], projBox[axis[1]]]:
        exit(
            "projection box and box of resulting image have to have same size otherwise image will be distorted"
        )
    data = get_value(variable, snap)
    if projection:
        if weights is None and use_weights:
            exit("need to specify weights to use")
        if projBox is None:
            exit("need to specify projection box of quiver plot")
        if projAxis is None:
            exit("need to specify projection axis of quiver plot")
        if use_weights:
            import FigureMove as Figure

            WEIGHTS = Figure.getWeights(
                snap,
                weights,
                considering_h,
                INFO=INFO,
                useWeightsThresholds=useWeightsThresholds,
            )

    if use_normalization:
        if normalization_value is None:
            exit("need value to normalize quiver values to...")
    if np.shape(data)[1] == 3:
        if not projection:
            myslice1 = snap.get_Aslice(
                data[:, axis[0]],
                axis=axis,
                res=res,
                box=box,
                center=np.array(center),
                numthreads=numthreads,
            )
            myslice2 = snap.get_Aslice(
                data[:, axis[1]],
                axis=axis,
                res=res,
                box=box,
                center=np.array(center),
                numthreads=numthreads,
            )
            b1 = myslice1["grid"].T
            b2 = myslice2["grid"].T
            if np.size(res) == 1:
                resx = res
                resy = res
            else:
                resx = res[axis[0]]
                resy = res[axis[1]]

        else:
            import FigureMove as Fig

            VARIABLE = data
            resProj = resolution_calculator_projections(res, projBox)
            if use_weights:
                data2 = Fig.getAgrid(
                    snap,
                    WEIGHTS,
                    res=resProj,
                    ProjBox=projBox,
                    center=center,
                    numthreads=numthreads,
                )
                grid2 = np.sum(data2, axis=projAxis)
                data = Fig.getAgrid(
                    snap,
                    VARIABLE[:, axis[0]] * WEIGHTS,
                    res=resProj,
                    ProjBox=projBox,
                    center=center,
                    numthreads=numthreads,
                )
                b1 = np.sum(data, axis=projAxis)
                b1 /= grid2
                data = Fig.getAgrid(
                    snap,
                    VARIABLE[:, axis[1]] * WEIGHTS,
                    res=resProj,
                    ProjBox=projBox,
                    center=center,
                    numthreads=numthreads,
                )
                b2 = np.sum(data, axis=projAxis)
                b2 /= grid2
                if axis[0] < axis[1]:
                    b1 = b1.T
                    b2 = b2.T
            elif sumAlongAxis:
                data = Fig.getAgrid(
                    snap,
                    VARIABLE[:, axis[0]],
                    res=resProj,
                    ProjBox=projBox,
                    center=center,
                    numthreads=numthreads,
                )
                b1 = np.sum(data, axis=projAxis)
                data = Fig.getAgrid(
                    snap,
                    VARIABLE[:, axis[1]],
                    res=resProj,
                    ProjBox=projBox,
                    center=center,
                    numthreads=numthreads,
                )
                b2 = np.sum(data, axis=projAxis)
                if axis[0] < axis[1]:
                    b1 = b1.T
                    b2 = b2.T
            else:
                data = Fig.getAgrid(
                    snap,
                    VARIABLE[:, axis[0]],
                    res=resProj,
                    ProjBox=projBox,
                    center=center,
                    numthreads=numthreads,
                )
                b1 = np.mean(data, axis=projAxis)
                data = Fig.getAgrid(
                    snap,
                    VARIABLE[:, axis[1]],
                    res=resProj,
                    ProjBox=projBox,
                    center=center,
                    numthreads=numthreads,
                )
                b2 = np.mean(data, axis=projAxis)
                if axis[0] < axis[1]:
                    b1 = b1.T
                    b2 = b2.T
    else:
        exit("needs to be vector field")
    # print res,projBox
    # print res,projBox
    if not projection:
        xx, yy = myslice1["x2"], myslice1["y2"]
        xx, yy = np.meshgrid(xx, yy)
    else:
        res = resolution_calculator_projections(res, projBox)
        resx = res[axis[0]]
        resy = res[axis[1]]
        dx = 1.0 * box[0] / resx
        dy = 1.0 * box[1] / resy
        print(dx, dy, "dx,dy")
        centers1 = np.linspace(
            center[axis[0]] - 0.5 * box[0] + 0.5 * dx,
            center[axis[0]] + 0.5 * box[0] - 0.5 * dx,
            resx,
        )
        centers2 = np.linspace(
            center[axis[1]] - 0.5 * box[1] + 0.5 * dy,
            center[axis[1]] + 0.5 * box[1] - 0.5 * dy,
            resy,
        )
        xx, yy = np.meshgrid(centers1, centers2)
    if QuiverSkipN == 0:
        pass
    elif isinstance(QuiverSkipN, (int, np.int64)) and QuiverSkipN > 0:
        xx = xx[::QuiverSkipN, ::QuiverSkipN]
        yy = yy[::QuiverSkipN, ::QuiverSkipN]
        b1 = b1[::QuiverSkipN, ::QuiverSkipN]
        b2 = b2[::QuiverSkipN, ::QuiverSkipN]
    else:
        print(QuiverSkipN)
        print((type(QuiverSkipN), QuiverSkipN > 0))
        exit("if want to use QuiverSkipN, need integer greater than 0!")
    cc = np.sqrt(b1 ** 2 + b2 ** 2)
    # print(center,'center')
    # print(box,'box')
    # print(res,'res')
    # debug.printDebug(xx, name='xx')
    # debug.printDebug(yy, name='yy')
    if use_normalization:
        cc_average = normalization_value
    elif use_max:
        if colorbarMax is None:
            colorbarMax = 6.0
        cc_average = np.max(cc) / colorbarMax  # 6.
        print(("using max of cc %g" % cc_average))
    else:
        cc_average = np.average(cc)
        print(("average of quiver variable %s equal to %g" % (variable, cc_average)))
    if cc_average == 0:
        cc_average = 1.0
        cc[:] = 1.0
        b1[:] = 1.0
        b2[:] = 1.0
        print(
            "QUIVER PLOT: changing average and total value to 1 as otherwise divide by inf"
        )
    if lengthnormalise:
        norm_fac = np.sqrt(b1 ** 2 + b2 ** 2)
        b1 /= norm_fac
        b2 /= norm_fac
        scale = (resx + resy) / 2.0 / 2
    if AlternativeBfieldFlag:
        if plotyourself:
            return (
                xx,
                yy,
                b1,
                b2,
                cc,
                [colorbarMin, colorbarMax],
                scale,
                cmap,
                alpha,
                cc_average,
            )
        quiver_stream = axes.quiver(
            xx,
            yy,
            b1,
            b2,
            cc / cc_average,
            clim=[colorbarMin, colorbarMax],
            scale=scale,
            cmap=cmap,
            alpha=alpha,
        )  # units='x', pivot='tip', width=0.5, scale=1 / 0.15)#
    # for varying size of streams:
    else:
        if plotyourself:
            return xx, yy, b1, b2, cc, cmap, cc_average
        quiver_stream = axes.streamplot(
            xx, yy, b1, b2, color=cc / cc_average, cmap=cmap
        )

    axes.set_xlim(center[axis[0]] - 0.5 * box[0], center[axis[0]] + 0.5 * box[0])
    axes.set_ylim(center[axis[1]] - 0.5 * box[1], center[axis[1]] + 0.5 * box[1])
    # also use following three for colorbar
    if AlternativeBfieldFlag:
        pos_axCurrent = axes.get_position().get_points()
        cbaxesBfield = figure.add_axes(
            [
                pos_axCurrent[1, 0] + axoffset,
                pos_axCurrent[0, 1] + 0.005,
                0.005,
                (pos_axCurrent[1, 1] - pos_axCurrent[0, 1]) / 1.1,
            ]
        )  # [*left*, *bottom*, *width*, *height*]
        cb = plt.colorbar(
            quiver_stream, cax=cbaxesBfield, orientation="vertical"
        )  # ,norm=plt.Normalize(vmin=colorbarMin,vmax=colorbarMax))


# taken from http://bsou.io/posts/color-gradients-with-python
def hex_to_RGB(hex):
    """ "#FFFFFF" -> [255,255,255] """
    # Pass 16 to the integer function for change of base
    return [int(hex[i : i + 2], 16) for i in range(1, 6, 2)]


def color_to_hex(c):
    """ "#FFFFFF" -> [255,255,255] """
    # Pass 16 to the integer function for change of base
    return mpl.colors.to_hex(c)


def RGB_to_hex(RGB):
    """ [255,255,255] -> "#FFFFFF" """
    # Components need to be integers for hex to make sense
    RGB = [int(x) for x in RGB]
    return "#" + "".join(
        ["0{0:x}".format(v) if v < 16 else "{0:x}".format(v) for v in RGB]
    )


def color_dict(gradient):
    """ Takes in a list of RGB sub-lists and returns dictionary of
	colors in RGB and hex form for use in a graphing function
	defined later on """
    return {
        "hex": [RGB_to_hex(RGB) for RGB in gradient],
        "r": [RGB[0] for RGB in gradient],
        "g": [RGB[1] for RGB in gradient],
        "b": [RGB[2] for RGB in gradient],
    }


def linear_gradient(start_hex, finish_hex="#FFFFFF", n=10, returnKey="hex"):
    """ returns a gradient list of (n) colors between
	two hex colors. start_hex and finish_hex
	should be the full six-digit color string,
	inlcuding the number sign ("#FFFFFF") """
    # Starting and ending colors in RGB form
    s = hex_to_RGB(start_hex)
    f = hex_to_RGB(finish_hex)
    # Initilize a list of the output colors with the starting color
    RGB_list = [s]
    # Calcuate a color at each evenly spaced value of t from 1 to n
    for t in range(1, n):
        # Interpolate RGB vector for color at the current value of t
        curr_vector = [
            int(s[j] + (float(t) / (n - 1)) * (f[j] - s[j])) for j in range(3)
        ]
        # Add it to our list of output colors
        RGB_list.append(curr_vector)

    return color_dict(RGB_list)[returnKey]


def make_iterable(whatever, kind="whatever"):
    if (
        type(whatever) == np.ndarray
        or type(whatever) == list
        or type(whatever) == tuple
    ):
        if kind == "whatever":
            return whatever
        else:
            return kind(whatever)
    else:
        if kind == "whatever" or kind == list:
            return [whatever]
        else:
            raise Exception(f"unsure how to convert to {kind}")


def isIterable(whatever):
    if (
        type(whatever) == np.ndarray
        or type(whatever) == list
        or type(whatever) == tuple
    ):
        return True
    else:
        return False


def flattenList(l):
    if isIterable(l) and len(l) > 0:
        if isIterable(l[0]):
            return flattenList(list(chain.from_iterable(l)))
    return l


def convertDataType(obj, dtype=float):
    if isIterable(obj):
        return [convertDataType(x, dtype=dtype) for x in obj]
    else:
        return dtype(obj)


def checkCommonType(obj):
    if isIterable(obj):
        obj = flattenList(obj)
        typ = type(obj[0])
        ifcom = all(typ == type(x) for x in obj)
    else:
        typ = type(obj)
        ifcom = True
    return typ, ifcom


def makeNIterable(object, N):
    if type(N) != int:
        print((object, N))
        exit("need int to make object iterable!")
    if N == 0:
        return object
    else:
        if np.size(object) == N:
            return object
        elif isIterable(object):
            return [object[0]] * N
        else:
            return [object] * N


def checkArListConv(ar, typeConv=list):
    if type(ar) == list or type(ar) == np.ndarray or type(ar) == tuple:
        return typeConv(ar)
    else:
        if typeConv == list:
            return [ar]
        elif typeConv == tuple:
            return (ar,)
        elif typeConv == np.array:
            return np.array([ar])
        else:
            raise Exception("typeConv not yet implemented yet!")


def modified_binned_statistic(
    sample,
    values,
    statistics=None,
    bins=10,
    range=None,
    weights=None,
    SaveHistogram=False,
    INFO=None,
    v=2,
    flatten=True,
):
    print(statistics, "statistics")
    """
	supporting statistics: 
					statistic types: mean, median, sum, medianweighted, meanweighted, statistic (specify function as statistic)
					error types: std, stdweighted, percentiles, percentilesweighted
	parameters in INFO: 
					medianweighted: factormedianweighted, medianweightedLowerBoundary, medianweightedUpperBoundary
	"""
    # check output's
    statistics = checkArListConv(statistics, typeConv=list)
    if v > 0:
        print("statistics", statistics)
    dic = {}

    known_stats = ["mean", "median", "count", "sum", "std", "min", "max", "statistic"]
    if flatten:
        sample = np.array(sample).flatten()
        values = np.array(values).flatten()
        if weights is not None:
            weights = np.array(weights).flatten()
    # from scipy._lib.six import callable

    debug.printDebug(values, "valuesbeg")
    for i, stat in enumerate(statistics):
        if stat in known_stats or callable(stat):
            try:
                dic[str(stat)], bin_edges, binnumber = scipy.stats.binned_statistic(
                    sample, values, statistic=stat, bins=bins, range=range
                )
            except ValueError as v:
                print(f"stat: {stat}")
                print(f"bins: {bins}")
                print(f"range: {range}")
                debug.printDebug(sample, name="sample")
                debug.printDebug(values, name="values")
                print(v)
            except Exception as e:
                print(e)
            statistics = [x for x in statistics if str(x) != str(stat)]

    print(f"dic: {dic}")
    # if not other statistics necessary
    if len(statistics) == 0:
        return dic, bin_edges

    for stat in statistics:
        if v > 0:
            print(stat)
        if "weighted" in stat and weights is None:
            raise Exception("need to specify weights to calculate %s" % stat)
    sample = np.array(sample)
    values = np.array(values)
    Sa = sample.flatten()
    Va = values.flatten()
    if weights is not None:
        weights = np.array(weights)
        We = weights.flatten()
        if np.size(Sa) != np.size(We):
            exit("sample and weights should have same size")
    if np.size(Sa) != np.size(Va):
        exit("sample and values should have same size")
    N = np.size(Sa)
    # Select range for each dimension
    # Used only if number of bins is given.
    if range is None:
        smin = Sa.min()
        smax = Sa.max()
    else:
        smin = range[0]
        smax = range[1]

    # Make sure the bins have a finite width.
    if smin == smax:
        smin = smin - 0.5
        smax = smax + 0.5

    # Create edge arrays
    if np.isscalar(bins):
        nbin = bins + 2  # +2 for outlier bins
        edges = np.linspace(smin, smax, nbin - 1)
    else:
        edges = bins
        nbin = len(edges) + 1  # +1 for outlier bins
    dedges = np.diff(edges)
    # Compute the bin number each sample falls into.
    Ncount = np.digitize(Sa, edges)
    # Using digitize, values that fall on an edge are put in the right bin.
    # For the rightmost bin, we want values equal to the right
    # edge to be counted in the last bin, and not as an outlier.
    decimal = int(-np.log10(dedges.min())) + 6
    # Find which points are on the rightmost edge.
    on_edge = np.where(np.around(Sa, decimal) == np.around(edges[-1], decimal))[0]
    # Shift these points one bin to the left.
    Ncount[on_edge] -= 1

    # should just be Ncount
    xy = Ncount

    core = tuple([slice(1, -1)])

    known_stats_alternative = [
        "medianweighted",
        "percentiles",
        "mean",
        "meanweighted",
        "median",
        "P1",
        "P2",
        "WP1",
        "WP2",
    ]

    if (
        "medianweighted" in statistics
        or "percentilesweighted" in statistics
        or "percentiles" in statistics
    ):
        time_start = time.time()
        lowerBound, upperBound = (
            checkExistenceKey(INFO, "percentilesLowerBoundary", 0.1),
            checkExistenceKey(INFO, "percentilesUpperBoundary", 0.9),
        )
        threads = checkExistenceKey(INFO, "percentilesThreads", 1)
        if v > 0:
            print(
                "medianweighted and percentilesweighted: using upper percentile: %g and lower percentile: %g"
                % (lowerBound, upperBound)
            )
        if lowerBound > 1 or upperBound > 1:
            raise Exception("bounding percentiles cannot be above 1!")

        def calc(data, weight):
            if not len(data) or (weight is not None and not len(weight)):
                return np.array([np.nan, np.nan, np.nan])
            wq = DescrStatsW(data=data, weights=weight)
            return np.array(
                list(
                    wq.quantile(
                        probs=np.array([lowerBound, upperBound]), return_pandas=False
                    )
                )
                + [wq.mean]
            )

        debug.printDebug(values, "values")
        debug.printDebug(np.arange(nbin), "np.arange(nbin)")
        debug.printDebug(len(values), "len(values))")
        debug.printDebug(xy, "xy")
        if threads > 1:
            bd = mt.BigData(
                calc,
                [np.arange(len(values))[xy == i] for i in np.arange(nbin)],
                [values, We],
                2,
                None,
                "numpy",
                trivial_distribution=False,
            )
            results = np.array(bd.do_multi(threads)).T
        else:
            results = []
            print(f"nbin: {nbin}")
            for i in np.arange(nbin):
                if weights is not None:
                    results.append(calc(values[xy == i], We[xy == i]))
                else:
                    print(f"values[xy == i]: {values[xy == i]}")
                    results.append(calc(values[xy == i], None))
            results = np.array(results).T
        dic["percentilesweightedL"] = results[0][core]
        dic["percentilesweightedU"] = results[1][core]
        dic["percentilesL"] = results[0][core]
        dic["percentilesU"] = results[1][core]
        # dic["percentiles"] = np.array([results[0][core], results[1][core]])
        dic["medianweighted"] = results[2][core]
        # dic['MedW1'] = medianweighted1[core]
        if v > 0:
            print(
                ("\n calculating upper lower took %f \n" % (time.time() - time_start))
            )

    # if "percentiles" in statistics:
    #     # NOTE: different factors then weighted median
    #     lowerBound, upperBound = (
    #         checkExistenceKey(INFO, "percentilesLowerBoundary", 0.1),
    #         checkExistenceKey(INFO, "percentilesUpperBoundary", 0.9),
    #     )
    #     if v > 0:
    #         print(
    #             "percentiles: using upper percentile: %g and lower percentile: %g"
    #             % (lowerBound, upperBound)
    #         )
    #     if lowerBound > 1 or upperBound > 1:
    #         raise Exception("bounding percentiles cannot be above 1!")
    #     statistic = lambda y, weights: find_lower_upper(
    #         y, weights, lowerBound, upperBound
    #     )
    #     lower = np.empty(nbin, float)
    #     upper = np.empty(nbin, float)
    #     lower.fill(np.nan)
    #     upper.fill(np.nan)
    #     for i in np.unique(xy):
    #         lower[i], upper[i] = statistic(values[xy == i], None)
    #     lower = lower[core]
    #     upper = upper[core]
    #     dic["percentilesL"] = lower
    #     dic["percentilesU"] = upper

    # Shape into a proper matrix
    """
#    result = result.reshape(np.sort(nbin))
#    result1 = result1.reshape(np.sort(nbin))
	
	for i in np.arange(nbin.size):
		j = ni.argsort()[i]
		result = result.swapaxes(i, j)
		result1 = result1.swapaxes(i, j)
		ni[i], ni[j] = ni[j], ni[i]
	"""
    # Remove outliers (indices 0 and -1 for each dimension).
    debug.printDebug(values, "values")
    # exit()
    if "mean" in statistics:
        mean = np.empty(nbin, float)
        mean.fill(np.nan)
        flatcount = np.bincount(xy, None)
        flatsum = np.bincount(xy, values)
        a = flatcount.nonzero()
        mean[a] = flatsum[a] / flatcount[a]
        dic["mean"] = mean[core]

    if "meanweighted" in statistics:
        meanweighted = np.empty(nbin, float)
        meanweighted.fill(np.nan)
        flatcount = np.bincount(xy, None)
        flatsum_weighted = np.bincount(xy, values * weights)
        flatsum_weights = np.bincount(xy, weights)
        a = flatcount.nonzero()
        meanweighted[a] = flatsum_weighted[a] / flatsum_weights[a]
        dic["meanweighted"] = meanweighted[core]

    if "median" in statistics:
        median = np.empty(nbin, float)
        median.fill(np.nan)
        for i in np.unique(xy):
            median[i] = np.median(values[xy == i])
        dic["median"] = median[core]

    if "min" in statistics:
        min = np.empty(nbin, float)
        min.fill(np.nan)
        for i in np.unique(xy):
            min[i] = np.min(values[xy == i])
        dic["min"] = min[core]

    if "max" in statistics:
        max = np.empty(nbin, float)
        max.fill(np.nan)
        for i in np.unique(xy):
            max[i] = np.max(values[xy == i])
        dic["max"] = max[core]

    if "P1" in statistics:  # correct standard deviation
        std1 = np.empty(nbin, float)
        std1.fill(np.nan)
        flatcount = np.bincount(xy, None)
        flatsum = np.bincount(xy, values)
        flatsum2 = np.bincount(xy, values ** 2)
        a = flatcount.nonzero()
        a = np.where(flatcount > 1)
        std1[a] = np.sqrt(flatsum2[a] / flatcount[a] - (flatsum[a] / flatcount[a]) ** 2)
        dic["P1"] = std1[core]
        # dic['P1'] =  np.nan_to_num(std1)

    if "P2" in statistics:  # based on   MSE = 1/N sum_i (Y_i - \hat{Y}_i)^2
        # raise Exception('not working correctly!')
        std2 = np.empty(nbin, float)
        std2.fill(np.nan)
        flatcount = np.float64(np.bincount(xy, None))
        flatsum = np.bincount(xy, values)
        flatsum2 = np.bincount(xy, values ** 2)
        nonOne = flatcount == 1
        a = np.where(flatcount > 1)
        std2[a] = np.sqrt(
            abs(
                1.0
                / (flatcount[a])
                * (
                    (
                        flatsum2[a]
                        - 2.0 * flatsum[a] * flatsum[a] / flatcount[a]
                        + (flatsum[a] / flatcount[a]) ** 2
                    )
                )
            )
        )
        dic["P2"] = std2[core]
        # dic['P2'] =  np.nan_to_num(std2)

    if "stdweighted" in statistics:
        stdweighted = np.empty(nbin, float)
        stdweighted.fill(np.nan)
        flatcount = np.bincount(xy, None)
        flatsum_weighted = np.bincount(xy, values * weights)
        flatsum2_weighted = np.bincount(xy, values ** 2 * weights)
        flatsum_weights = np.bincount(xy, weights)
        a = np.where(flatcount > 1)  # flatcount >= 2
        stdweighted[a] = np.sqrt(
            abs(
                flatsum2_weighted[a] / flatsum_weights[a]
                - (flatsum_weighted[a] / flatsum_weights[a]) ** 2
            )
        )
        dic["stdweighted"] = stdweighted[core]
        # dic['WP1'] =  np.nan_to_num(stdweighted)

    if "WP2" in statistics:
        stdweighted = np.empty(nbin, float)
        stdweighted.fill(np.nan)
        flatcount = np.bincount(xy, None)
        a = np.where(flatcount > 1)  # flatcount >= 2
        flatsum_weighted = np.bincount(xy, values * weights)
        flatsum2_weighted = np.bincount(xy, values ** 2 * weights)
        flatsum_weights = np.bincount(xy, weights)
        np.where(flatcount > 1)
        stdweighted[a] = np.sqrt(
            abs(
                (
                    flatsum2_weighted[a]
                    - 2.0
                    * flatsum_weighted[a]
                    * flatsum_weighted[a]
                    / flatsum_weights[a]
                    + flatsum_weights[a]
                    * (flatsum_weighted[a] / flatsum_weights[a]) ** 2
                )
                / (flatsum_weights[a] * (flatcount[a] - 1) / flatcount[a])
            )
        )
        dic["WP2"] = stdweighted[core]
        # dic['WP2'] =  np.nan_to_num(stdweighted)
    if "WEIGHTEDSTANDARDDEVIATIONDATAPLOTReferenceManual" in statistics:
        stdweighted = np.empty(nbin, float)
        stdweighted.fill(np.nan)
        flatcount = np.bincount(xy, None)
        flatsum_weighted = np.bincount(xy, values * weights)
        flatsum_weights = np.bincount(xy, weights)
        a = flatcount.nonzero()
        meanweighted = np.empty(nbin, float)
        meanweighted.fill(np.nan)
        meanweighted[a] = flatsum_weighted[a] / flatsum_weights[a]
        print(f"meanweighted: {meanweighted}")
        print(f"xy: {xy}")
        meanweighted_extend = np.empty(values.shape, float)
        meanweighted_extend.fill(np.nan)
        for index, mean in enumerate(meanweighted):
            mask = xy == index
            meanweighted_extend[mask] = mean
        print(meanweighted_extend)
        numerator_std_weighted = np.bincount(xy, weights * (values - meanweighted_extend)**2)
        weights_nonzero = weights.nonzero()
        number_nonzero_weights = np.bincount(xy[weights_nonzero], None)
        stdweighted[a] = np.sqrt(
            numerator_std_weighted[a] / ((number_nonzero_weights[a]-1)/number_nonzero_weights[a] * flatsum_weights[a])
        )
        dic["WEIGHTEDSTANDARDDEVIATIONDATAPLOTReferenceManual"] = stdweighted[core]
        # dic['WP2'] =  np.nan_to_num(stdweighted)

    # check if everything indicated was calculated:
    not_calc = [
        str(x)
        for x in statistics
        if not any([x in dic.keys() for x in [str(x) + "L", str(x), str(x) + "U"]])
    ]
    if len(not_calc) > 0:
        import warnings

        warnings.warn("couldnt calculate statistics: %s" % (" ".join(not_calc)))
    if SaveHistogram:
        median_weighted = [1] + list(dic["MedW1"]) + [1]
        median_weighted1 = [1] + list(dic["MedW1"]) + [1]
        WP1 = [1] + list(dic["WP1"]) + [1]
        WP2 = [1] + list(dic["WP2"]) + [1]
        P1 = [1] + list(dic["P1"]) + [1]
        P2 = [1] + list(dic["P2"]) + [1]
        # if v > 0:
        #     print("edges", edges)
        #     print("xy", xy)
        for i in np.unique(xy):
            sample = values[xy == i]
            sampleW = weights[xy == i]
            selfcalc2 = np.sqrt(
                abs(
                    np.sum(sample ** 2)
                    - 2.0 * np.sum(sample) * np.average(sample)
                    + np.average(sample) ** 2
                )
                / (np.size(sample) - 1)
            )
            selfcalc1 = np.sqrt(
                abs(np.sum(sample ** 2) / np.size(sample) - np.average(sample) ** 2)
            )
            number_of_zero_cells = np.size(sample[sample == 0])
            if np.max(sample) == 0:
                continue
            sample[sample == 0] = np.min(sample[sample != 0])
            if len(edges) - 1 < i:
                # edge doesnt exist as 'cored' out
                e = edges[-1] + 1
            else:
                e = edges[i]
            m = np.median(sample)
            title = "%i Cells:%.3g, Edge%.3g Median%.3g MW%.3g MW %.3g" % (
                number_of_zero_cells,
                np.min(sample[sample != 0]),
                m,
                e,
                median_weighted[i],
                median_weighted1[i],
            )
            name = "../figures/histograms/hist_CRED_%i.png" % i
            create_dir_for_file(name)
            checkExistence_delete_file(name)
            save_histogram_old(
                (sample), dataname=sample, bins=20, filename=name, log=True, title=title
            )
            name = "../figures/histograms/hist_VOL_%i.png" % i
            title = "%i Cells:%.3g, Edge%.3g MW%.3g WP1%.3g WP2%.3g" % (
                number_of_zero_cells,
                np.min(sample[sample != 0]),
                e,
                median_weighted[i],
                WP1[i],
                WP2[i],
            )
            create_dir_for_file(name)
            checkExistence_delete_file(name)
            save_histogram_old(
                sample,
                weights=sampleW,
                dataname=sample,
                bins=20,
                filename=name,
                log=True,
                title=title,
            )
    return dic, edges


def checkExistence_delete_file(filename):
    if os.path.isfile(filename):
        os.remove(filename)


def minValue(array, log=True, list=False):
    if list:
        try:
            while 1:
                array = [y for sub in array for y in sub]
        except:
            Min = min(array)
        array = np.array(array)
        if log:
            if np.size(array[array > 0]) != 0:
                return np.min(array[array > 0])
            else:
                print(
                    "couldnt find any value greater than zero in array setting minimum arbitrarily to 1"
                )
                return 1.0

        return Min
    if log:
        array = np.array(array).flatten()
        if np.size(array[array > 0]) != 0:
            return np.min(array[array > 0])
        else:
            print(
                "couldnt find any value greater than zero in array setting minimum arbitrarily to 1"
            )
            return 1.0
    else:
        return np.min(array)


def maxValue(array, log=True, list=False):
    if list:
        try:
            while 1:
                array = [y for sub in array for y in sub]
        except:
            Max = np.max(array)
    else:
        Max = np.max(array)
    if log:
        if Max != 0:
            return Max
        else:
            print(
                "couldnt find any value greater than zero in array setting maximum arbitrarily to 2"
            )
            return 2.0
    else:
        return Max


def check_alternative(try1, save2):
    if try1:
        return try1
    else:
        return save2


def CHECK(tryThis, returnThis, returnOther):
    if tryThis:
        return returnThis
    return returnOther


def save_histogram_old(
    data,
    dataname="stuff",
    bins=20,
    filename="test.png",
    log=False,
    title=None,
    weights=None,
):
    # profile, xbins = np.histogram(data,bins=bins)
    fig = plt.figure()
    ax = plt.axes([0.075, 0.1, 0.8, 0.8])
    MIN = np.min(data)
    MAX = np.max(data)
    if log:
        bins = 10 ** np.linspace(
            np.log10(0.99 * MIN), np.log10(1.01 * MAX), bins, endpoint=True
        )
        ax.hist(data, bins=bins, weights=weights)
        ax.set_xscale("log")
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_yscale("log")
    else:
        bins = np.linspace((MIN), (MAX), bins, endpoint=True)
    ax.hist(data, bins=bins, weights=weights)
    ax.set_title(title)
    ax.set_xlim(
        np.min(bins) - (bins[1] - bins[0]) / 2.0,
        np.max(bins) + (bins[-1] - bins[-2]) / 2.0,
    )
    ax.set_ylim(0.5e0, None)
    print("... saving %s" % filename)
    fig.savefig(filename, dpi=param.dpi)
    plt.close(fig)


def calcRadprofStatistics(
    sample,
    values,
    bins=100,
    range=None,
    statistics=["mean"],
    weights=None,
    INFO=None,
    lower=0.1,
    upper=0.9,
    returnBinedges=False,
    v=2,
):
    if INFO is None:
        print(f"INFO is None")
        INFO = {}
        INFO["percentilesweightedLowerBoundary"] = lower
        INFO["percentilesweightedUpperBoundary"] = upper
        INFO["percentilesLowerBoundary"] = lower
        INFO["percentilesUpperBoundary"] = upper
        print("INFO is None: set; ", INFO)
    dic, bin_edges = modified_binned_statistic(
        sample,
        values,
        statistics=statistics,
        bins=bins,
        range=range,
        weights=weights,
        SaveHistogram=False,
        INFO=INFO,
        v=v,
    )
    bin_centers = bin_edges[1:] - np.diff(bin_edges) / 2.0
    if returnBinedges:
        return dic, bin_centers, bin_edges
    else:
        return dic, bin_centers


def get_SphericalCoordinates(snap, degrees=False, center=None, z=2, y=1, x=0):
    if center is None:
        exit("need to give center for spherical coordinates")
    r = np.sqrt(
        (snap.data["pos"][snap.data["type"] == 0][:, x] - center[x]) ** 2
        + (snap.data["pos"][snap.data["type"] == 0][:, y] - center[y]) ** 2
        + (snap.data["pos"][snap.data["type"] == 0][:, z] - center[z]) ** 2
    )
    theta = np.arccos((snap.data["pos"][snap.data["type"] == 0][:, z] - center[z]) / r)
    phi = np.arctan2(
        snap.data["pos"][snap.data["type"] == 0][:, y] - center[y],
        snap.data["pos"][snap.data["type"] == 0][:, x] - center[x],
    )
    if degrees:
        theta *= 180.0 / np.pi
        phi *= 180.0 / np.pi
    return r, theta, phi


def get_CylindricalCoordinates(snap, degrees=False, center=None, z=2, y=1, x=0):
    if center is None:
        exit("need to give center for spherical coordinates")
    X = snap.data["pos"][snap.data["type"] == 0].copy()[:, x] - center[x]
    Y = snap.data["pos"][snap.data["type"] == 0].copy()[:, y] - center[y]
    Z = snap.data["pos"][snap.data["type"] == 0].copy()[:, z] - center[z]
    r = np.sqrt(X ** 2 + Y ** 2)
    phi = np.zeros(np.shape(X))
    mask = np.logical_and(X == 0, Y == 0)
    phi[mask] = 0
    mask = np.logical_and(X == 0, Y != 0)
    phi[mask] = np.arcsin(Y[mask] / r[mask])
    mask = X > 0
    phi[mask] = np.arcsin(Y[mask] / r[mask])
    mask = X < 0
    phi[mask] = -np.arcsin(Y[mask] / r[mask]) + np.pi
    z = Z
    if degrees:
        phi = phi * 180.0 / np.pi
    return r, phi, z


def follow(
    snap,
    variable,
    upper=True,
    relative_threshold=1e-3,
    check_for_0=False,
    considering_h=False,
    convert_units=False,
    INFO=None,
    relativeAxis=0,
    pos=None,
    boxsize=None,
    weights="Volume",
):
    initial_time = time.time()
    if pos is None:
        pos = snap.data["pos"][snap.data["type"] == 0][:][:]
    if boxsize is None:
        boxsize = snap.boxsize

    avgPos = np.zeros(3)
    avgPos[:] = 0.5 * boxsize

    if weights is not None:
        weights = get_value(
            weights,
            snap,
            where=None,
            check_for_0=False,
            considering_h=False,
            convert_units=False,
            INFO=INFO,
        )

    tracer = np.array(
        get_value(
            variable,
            snap,
            where=None,
            check_for_0=check_for_0,
            considering_h=considering_h,
            convert_units=convert_units,
            INFO=INFO,
        )
    )
    if upper:
        (i,) = np.where(
            (tracer > relative_threshold * np.max(tracer))
            & (pos[:, relativeAxis] > 0.5 * boxsize)
        )
    else:
        (i,) = np.where(
            (tracer > relative_threshold * np.max(tracer))
            & (pos[:, relativeAxis] < 0.5 * boxsize)
        )
    print((len(i), " cells selected, jetTracer max = ", np.max(snap.data["jetr"])))
    if len(i) > 0:
        avgvel = np.average(
            snap.data["vel"][snap.data["type"] == 0][i, 0], weights=snap.data["vol"][i]
        )
        avgPos[0] = np.average(pos[i, 0], weights=weights[i])
        avgPos[1] = np.average(pos[i, 1], weights=weights[i])
        avgPos[2] = np.average(pos[i, 2], weights=weights[i])
    print(("calculating avg postion took %g" % (time.time() - initial_time)))
    return avgPos


def rotation_matrix(axis, theta):
    import math

    """
	Return the rotation matrix associated with counterclockwise rotation about
	the given axis by theta radians.
	http://stackoverflow.com/questions/6802577/python-rotation-of-3d-vector
	"""
    axis = np.asarray(axis)
    axis = axis / math.sqrt(np.dot(axis, axis))
    a = math.cos(theta / 2.0)
    b, c, d = -axis * math.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array(
        [
            [aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
            [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
            [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc],
        ]
    )


ALLOWED_INPUT_UNITS_ROTATIONS = ["radians", "degrees"]


def RotatePositions(snap, center, theta, axis, verbose=False, input_units="radians"):
    if input_units not in ALLOWED_INPUT_UNITS_ROTATIONS:
        raise Exception(
            f"units for rotation {input_units} not understood, allowed: {ALLOWED_INPUT_UNITS_ROTATIONS}"
        )
    if input_units == "degrees":
        theta *= np.pi / 180.0
    if verbose:
        print("rotatepositionsBEG")
        print(snap.data["pos"])

    RotatMatrix = rotation_matrix(axis, theta)
    snap.data["pos"][:, 0], snap.data["pos"][:, 1], snap.data["pos"][:, 2] = np.dot(
        RotatMatrix,
        [
            (snap.data["pos"])[:, 0].ravel() - center[0],
            (snap.data["pos"])[:, 1].ravel() - center[1],
            (snap.data["pos"])[:, 2].ravel() - center[2],
        ],
    )
    snap.data["pos"][:, 0] = snap.data["pos"][:, 0] + center[0]
    snap.data["pos"][:, 1] = snap.data["pos"][:, 1] + center[1]
    snap.data["pos"][:, 2] = snap.data["pos"][:, 2] + center[2]

    if verbose:
        print("rotatepositionsEND")
        print(snap.data["pos"])
    return snap


def normalizeVector(Vector):
    normalized = Vector.ravel() / np.sqrt(np.dot(Vector.ravel(), Vector.ravel()))
    return normalized


def normalVectorPlane(Vector1, Vector2, normalized=False):
    normal = np.cross(Vector1, Vector2)
    if normalized:
        normal = normalizeVector(normal)
    return normal


def AngleBetweenVectors(Vector1, Vector2):
    vec1 = normalizeVector(Vector1.ravel())
    vec2 = normalizeVector(Vector2.ravel())
    angle = np.arccos(np.dot(vec1, vec2))
    return angle


def DistanceVectors(Vector1, Vector2):
    return np.sqrt(
        (Vector1[0] - Vector2[0]) ** 2
        + (Vector1[1] - Vector2[1]) ** 2
        + (Vector1[2] - Vector2[2]) ** 2
    )


def RotateForQuiver(snap, theta, axis):
    RotatMatrix = rotation_matrix(axis, theta)
    if hasattr(snap, "bfld"):
        (
            snap.data["bfld"][:, 0],
            snap.data["bfld"][:, 1],
            snap.data["bfld"][:, 2],
        ) = np.dot(
            RotatMatrix,
            [
                snap.data["bfld"][:, 0].ravel(),
                snap.data["bfld"][:, 1].ravel(),
                snap.data["bfld"][:, 2].ravel(),
            ],
        )
    if hasattr(snap, "vel"):
        snap.data["vel"][:, 0], snap.data["vel"][:, 1], snap.data["vel"][:, 2] = np.dot(
            RotatMatrix,
            [
                snap.data["vel"][:, 0].ravel(),
                snap.data["vel"][:, 1].ravel(),
                snap.data["vel"][:, 2].ravel(),
            ],
        )
    return snap


def FindCenter(
    POS,
    JETTRACER,
    bins=100,
    fromUptoLow=False,
    CumTracThresh=0.01,
    ensureOnRespectiveSide=False,
    center=None,
):
    Tracer, bin_edges, binnumber = scipy.stats.binned_statistic(
        POS, JETTRACER, statistic="sum", bins=bins, range=[np.min(POS), np.max(POS)]
    )
    bin_centres = bin_edges[0:-1] + np.diff(bin_edges)

    if ensureOnRespectiveSide:
        if center is None:
            exit(
                "cant ensure that upper/lower bubble lies within upper/lower half of box"
            )
        mid = center[0]  # bin_centres[int(np.size(bin_centres)/2)]
        if fromUptoLow:
            Tracer = Tracer[bin_centres > mid]
            bin_centres = bin_centres[bin_centres > mid]
        else:
            Tracer = Tracer[bin_centres < mid]
            bin_centres = bin_centres[bin_centres < mid]
        CumTracThresh *= 2.0
    if fromUptoLow:
        Tracer = Tracer[::-1]
    #    exit()
    Tracer[Tracer < 0] = 0
    CumSum = np.cumsum(Tracer) / np.sum(Tracer)
    for i in range(np.size(bin_centres)):
        if CumSum[i] > CumTracThresh:
            CutID = i - 1
            if fromUptoLow:
                CutID = np.size(bin_centres) - CutID - 1
            break
    if CutID == bins:
        CutID = bins - 1
    Center = bin_centres[CutID]
    return Center


def ScreenJetTracer(
    snap,
    bins=20,
    up=True,
    TracerThresh=1e-6,
    CumTracThresh=0.01,
    AreaToCheck=[0.01, 0.01, 0.01],
    ensureOnRespectiveSide=False,
    center=None,
    returnBubbleRadiiYZ=False,
):
    if np.size(CumTracThresh) == 1:
        CumTracThresh = [CumTracThresh] * 3
    initTime = time.time()
    # looking for X center first
    POS = snap.data["pos"][snap.data["type"] == 0].copy()
    JETTRACER = snap.data["jetr"].copy()
    mask = JETTRACER > TracerThresh
    POS = POS[mask]
    JETTRACER = JETTRACER[mask]

    RangeUpp = np.max(POS[:, 0])  # [mask])
    RangeLow = np.min(POS[:, 0])  # [mask])
    mask = np.logical_and(POS[:, 0] >= RangeLow, POS[:, 0] <= RangeUpp)
    POS = POS[mask]
    JETTRACER = JETTRACER[mask]
    XCenter = FindCenter(
        POS[:, 0],
        JETTRACER,
        bins=bins,
        fromUptoLow=up,
        CumTracThresh=CumTracThresh[0],
        ensureOnRespectiveSide=ensureOnRespectiveSide,
        center=center,
    )

    # looking for Y center
    mask = np.logical_and(
        POS[:, 0] > XCenter - AreaToCheck[0] / 2.0,
        POS[:, 0] < XCenter + AreaToCheck[0] / 2.0,
    )
    POS = POS[mask]
    JETTRACER = JETTRACER[mask]
    YCenterL = FindCenter(
        POS[:, 1],
        JETTRACER,
        bins=bins,
        fromUptoLow=False,
        CumTracThresh=CumTracThresh[1],
    )
    YCenterU = FindCenter(
        POS[:, 1],
        JETTRACER,
        bins=bins,
        fromUptoLow=True,
        CumTracThresh=CumTracThresh[1],
    )
    YCenter = (YCenterU - YCenterL) / 2.0 + YCenterL

    # looking for Z center
    mask = np.logical_and(
        POS[:, 1] > YCenter - AreaToCheck[1] / 2.0,
        POS[:, 1] < YCenter + AreaToCheck[1] / 2.0,
    )
    POS = POS[mask]
    JETTRACER = JETTRACER[mask]
    ZCenterL = FindCenter(
        POS[:, 2],
        JETTRACER,
        bins=bins,
        fromUptoLow=False,
        CumTracThresh=CumTracThresh[2],
    )
    ZCenterU = FindCenter(
        POS[:, 2],
        JETTRACER,
        bins=bins,
        fromUptoLow=True,
        CumTracThresh=CumTracThresh[2],
    )
    ZCenter = (ZCenterU - ZCenterL) / 2.0 + ZCenterL
    #    print 'took %g'%(time.time()-initTime)
    if returnBubbleRadiiYZ:
        return (
            [XCenter, YCenter, ZCenter],
            [(YCenterU - YCenterL) / 2.0, (ZCenterU - ZCenterL) / 2.0],
        )
    return [XCenter, YCenter, ZCenter]


def ReplaceDotInName(number, returnFormat, replace=["."], replace_with=["p"]):
    if len(replace) != len(replace_with):
        raise Exception(
            f"need to know what to replace {replace} with replace_with {replace_with}"
        )
    number = returnFormat % number
    string = str(number)
    for rep, rep_w in zip(replace, replace_with):
        string = string.replace(rep, rep_w)
    return string


def histedges_equalN(x, nbin, getAll=False):
    sorted = np.sort(x.copy())
    npt = len(x)
    jump = int(npt / (nbin))
    bins = np.array(sorted[0::jump])
    if getAll and sorted[-1] != bins[-1]:
        bins = np.concatenate([bins, [sorted[-1]]])
    return bins


def findIndexValue(value, Array, NumberOfOccurence=1):
    N = np.size(Array)
    diffDataOld = abs(value - 0)
    number = 0
    for i in range(1, N):
        diffDataNew = abs(Array[i] - value)
        if diffDataNew <= diffDataOld:
            number = i
            diffDataOld = diffDataNew
        else:
            if NumberOfOccurence == 1:
                break
            else:
                NumberOfOccurence -= 1
    return number


def getBinsMMs(Variable=None, Min=None, Max=None, N=100, log=False):
    if Variable is None and (Min is None or Max is None):
        raise Exception(
            "if cannot give min and max value for bins should give variable!"
        )
    if Min is None:
        MIN = np.min(Variable)
    else:
        MIN = Min
    if Max is None:
        MAX = np.max(Variable)
    else:
        MAX = Max
    if log:
        BINS = 10 ** np.linspace(np.log10(MIN), np.log10(MAX), N)
    else:
        BINS = np.linspace((MIN), (MAX), N)
    return BINS, MIN, MAX


def get_PercentileValueHighAcc(
    snap,
    percentile,
    VariableX,
    VariableY,
    mask=None,
    INFO=None,
    normalise=True,
    invert=False,
    returnLabelY=False,
):
    VariableX, LabelX = copy.deepcopy(
        get_value(
            VariableX,
            snap,
            considering_h=False,
            convert_units=True,
            returnLabel=True,
            INFO=INFO,
        )
    )
    VariableY, LabelY = copy.deepcopy(
        get_value(
            VariableY,
            snap,
            considering_h=False,
            convert_units=True,
            returnLabel=True,
            INFO=INFO,
        )
    )
    if mask is not None:
        print((np.size(VariableX)))
        VariableX = VariableX[mask]
        VariableY = VariableY[mask]
    print(("looking at %i cells" % np.size(VariableX)))

    i_sorted = np.argsort(VariableX)
    VariableY = VariableY[i_sorted]
    VariableX = VariableX[i_sorted]
    if normalise:
        if np.sum(VariableY) != 0:
            normaliseTo = np.sum(VariableY)
        else:
            normaliseTo = 1.
    else:
        normaliseTo = 1.0
    if invert:
        VariableY = np.max(np.cumsum(VariableY / normaliseTo)) - np.cumsum(
            VariableY / normaliseTo
        )
    else:
        VariableY = np.cumsum(VariableY) / normaliseTo
    DiffPerc = np.abs(VariableY - percentile)
    print(f"DiffPerc: {DiffPerc}")
    value = VariableX[np.where(DiffPerc == DiffPerc.min())]
    if np.size(value) > 1:
        print(value)
        print("more than one distance hit in PercentileValueHighAcc, pick closest")
        value = np.min(value)
        print(f"value: {value}")
    label = "%s(%s)" % (LabelY, LabelX)
    if not returnLabelY:
        return value, label
    else:
        return value, label, LabelY


def get_JetDistanceHalfHalf(
    snap, INFO=None, both=True, up=False, center=None, returnLabel=False
):
    distD = None
    distU = None
    center = checkIfInDictionary("center", INFO, [snap.boxsize / 2.0 for i in range(3)])
    percentile = checkIfInDictionary("JetDistanceHalfHalfPercentile", INFO, 0.5)
    Variable = checkIfInDictionary(
        "VariableJetDistanceHalfHalf", INFO, "JetMassFraction"
    )
    VariableX = "DistanceToCenterInKpc"
    if both:
        up = True
        down = True
    if up:
        mask = snap.data["pos"][snap.data["type"] == 0][:, 0] > center[0]
        distU, NoLabel, labelVariable = get_PercentileValueHighAcc(
            snap,
            percentile,
            VariableX,
            Variable,
            mask=mask,
            INFO=INFO,
            normalise=True,
            invert=False,
            returnLabelY=True,
        )
    if down:
        mask = snap.data["pos"][snap.data["type"] == 0][:, 0] < center[0]
        distD, NoLabel, labelVariable = get_PercentileValueHighAcc(
            snap,
            percentile,
            VariableX,
            Variable,
            mask=mask,
            INFO=INFO,
            normalise=True,
            invert=False,
            returnLabelY=True,
        )
    print(f"float(distU), float(distD): {distU}{distD}")
    if returnLabel:
        return float(distU), float(distD), labelVariable
    return float(distU), float(distD)


def get_JetDistanceTracMax(snap, INFO=None, both=True, up=False):
    distD = None
    distU = None
    center = checkIfInDictionary("center", INFO, [snap.boxsize / 2.0 for i in range(3)])
    bubbleThresh = checkIfInDictionary("BubbleThresh", INFO, 1e-3)
    if both:
        up, down = True, True
    if up:
        tracerU = np.logical_and(
            snap.data["jetr"] > bubbleThresh,
            snap.pos[snap.data["type"] == 0][:, 0] > center[0],
        )
        if np.sum(tracerU):
            distU = (
                np.max(snap.data["pos"][snap.data["type"] == 0][:, 0][tracerU]) - center[0]
            )
        else:
            warnings.warn(f"get_JetDistanceTracMax: no jet tracers found defaulting distance to 0")
            distU = 0
    if down:
        tracerD = np.logical_and(
            snap.data["jetr"] > bubbleThresh,
            snap.pos[snap.data["type"] == 0][:, 0] < center[0],
        )
        if np.sum(tracerD):
            distD = center[0] - np.min(
                snap.data["pos"][snap.data["type"] == 0][:, 0][tracerD]
            )
        else:
            warnings.warn(f"get_JetDistanceTracMax: no jet tracers found defaulting distance to 0")
            distD = 0
    if distD < 0 or distU < 0:
        print((distU, distD))
        exit("Error:get_JetDistanceHalfHalf: negative distances!")
    code_to_kpc = snap.UnitLength_in_cm / param.KiloParsec_in_cm
    return float(distU) * code_to_kpc, float(distD) * code_to_kpc


def get_JetBowShockDistanceMachMax(snap, INFO=None, both=True, up=False):
    """
	make sure machnumber not inside jet
	return 0 if no machnumber found
	"""
    distD = 0
    distU = 0
    center = checkIfInDictionary("center", INFO, [snap.boxsize / 2.0 for i in range(3)])
    bubbleThresh = checkIfInDictionary("BubbleThresh", INFO, 1e-3)
    if both:
        up, down = True, True
    if up:
        machsU = np.logical_and.reduce(
            (
                snap.data["mach"] > 0,
                snap.data["jetr"] < bubbleThresh,
                snap.data["pos"][snap.data["type"] == 0][:, 0] > center[0],
            )
        )
        if np.size(snap.data["pos"][snap.data["type"] == 0][:, 0][machsU]) > 0:
            distU = (
                np.max(snap.data["pos"][snap.data["type"] == 0][:, 0][machsU])
                - center[0]
            )
    if down:
        machsD = np.logical_and.reduce(
            (
                snap.data["mach"] > 0,
                snap.data["jetr"] < bubbleThresh,
                snap.data["pos"][snap.data["type"] == 0][:, 0] < center[0],
            )
        )
        if np.size(snap.data["pos"][snap.data["type"] == 0][:, 0][machsD]) > 0:
            distD = center[0] - np.min(
                snap.data["pos"][snap.data["type"] == 0][:, 0][machsD]
            )
    if distD < 0 or distU < 0:
        print((distU, distD))
        exit("Error:get_JetBowShockDistanceMachMax: negative distances!")
    code_to_kpc = snap.UnitLength_in_cm / param.KiloParsec_in_cm
    return float(distU) * code_to_kpc, float(distD) * code_to_kpc


def checkDimensionList2D(List):
    try:
        np.shape(List[0][0])
        return True
    except:
        return False


def get_FileValue(
    variable,
    time,
    folder,
    snap,
    name,
    fields=["pos", "cren", "rho", "vol", "mass"],
    function=None,
    INFO=None,
    rewriteFile=False,
    maxFiles=512,
    onlyHeader=False,
    NumberOutputs=1,
    complexFunc=False,
    fmt="%.18e",
    returnTime=False,
    singleSnapSufficient=False,
):
    import Param

    if variable in Param.NEED_ONLY_ONE_SNAPSHOT:
        singleSnapSufficient = True
    data = get_FunctionFile(
        folder,
        maxFiles=maxFiles,
        variable=variable,
        rewriteFile=rewriteFile,
        name=name,
        fields=fields,
        function=function,
        INFO=INFO,
        onlyHeader=onlyHeader,
        complexFunc=complexFunc,
        NumberOutputs=NumberOutputs,
        fmt=fmt,
        singleSnapSufficient=singleSnapSufficient,
    )
    if np.size(data) == 0:
        exit("data not correctly recovered from file!")
    # if np.shape(data)[0]==1:
    #    print data
    #    return data[2]
    if time == "all":
        return data
    if time is None:
        if snap is not None:
            time = get_Time(snap, considering_h=False)
            index = findIndexValue(time, data[:, 1], NumberOfOccurence=1)
            print(("found time %g" % data[index, 1]))

        else:
            index = 0
            print("found no snap assuming zero")
    else:
        if checkDimensionList2D(data):
            index = findIndexValue(time, data[:, 1], NumberOfOccurence=1)
            if returnTime:
                time = data[index, 1]
        else:
            index = 0
            if returnTime:
                time = 0

    if checkDimensionList2D(data):
        NumberData = np.shape(data)[1]
        if NumberData > 3:
            value = [data[index, indexData] for indexData in np.arange(2, NumberData)]
        else:
            value = data[index, 2]
    else:
        value = data[2]
    if returnTime:
        return value, time
    else:
        return value


def get_JetActivity(snap, returnLabel=False, folder=None):
    if folder is None:
        TimeSpan = (
            snap.configfile["BH_CONSTANT_POWER_RADIOMODE_TIMELIMIT_IN_MYR"]
            / snap.hubbleparam
        )
    else:
        TimeSpan, time = get_FileValue(
            None,
            0,
            folder,
            None,
            "JetActivity",
            fields=[],
            function=lambda snap: snap.configfile.BH_CONSTANT_POWER_RADIOMODE_TIMELIMIT_IN_MYR
            / snap.hubbleparam,
            INFO=None,
            rewriteFile=False,
            maxFiles=0,
            onlyHeader=True,
            returnTime=True,
        )
    if returnLabel:
        return TimeSpan, time, r"$\tau_\mathrm{jet}\ [\mathrm{Myr}]$"
    return TimeSpan


def get_FunctionFile(
    directory,
    maxFiles=512,
    variable=None,
    rewriteFile=False,
    name="test",
    fields=["pos", "jetr", "rho", "vol", "mass"],
    function=None,
    INFO=None,
    onlyHeader=False,
    complexFunc=False,
    NumberOutputs=1,
    fmt="%.18e",
    singleSnapSufficient=False,
):
    """
	saves file of contrast as:
	Time 0, index snapshot 1, value 2
	"""
    import FigureMove as Fig

    filename = "%s/PlotInfo/%s.txt" % (directory, name)
    if os.path.isfile(filename) and not rewriteFile:
        Data = np.genfromtxt(filename)
        return Data
    Data = None
    for i in range(maxFiles + 1):
        path = "%ssnap_%03d.hdf5" % (directory, i)
        pathAlt = "%ssnapdir_%03d/snap_%03d.0.hdf5" % (directory, i, i)
        for indexPath, pathIt in enumerate([path, pathAlt]):
            if os.path.isfile(pathIt):
                snap = Fig.readSnap(
                    directory,
                    i,
                    FieldsToLoad=fields,
                    useTime=False,
                    returnSnapNumber=False,
                    rewriteTimes=False,
                    onlyHeader=onlyHeader,
                )
                snap, time = Fig.getGeneralInformation(
                    snap,
                    convertedkpc=False,
                    considering_h=False,
                    StringTime=True,
                    Precision=None,
                    Round10=False,
                    Round=1,
                    returnNotStringTime=False,
                    IC=False,
                    onlyHeader=onlyHeader,
                )
                # snap=arepo.Simulation(path, parttype=[0], fields=fields,combineFiles=True, onlyHeader=onlyHeader)
                if not complexFunc:
                    if not onlyHeader:
                        data = get_value(
                            variable,
                            snap,
                            considering_h=False,
                            convert_units=True,
                            INFO=INFO,
                        )
                        time = get_Time(snap, considering_h=False)
                    else:
                        data = get_value(
                            variable,
                            snap,
                            considering_h=False,
                            convert_units=True,
                            INFO=INFO,
                        )
                        time = 0
                    if callable(function) and onlyHeader:
                        data = function(snap)
                    elif callable(function):
                        data = function(data)
                    else:
                        if "Lobe" in name:
                            mask = snap.data["jetr"] >= INFO["LobeThreshold"]
                            data = data[mask]
                        if "NotZero" in name:
                            data = data[data != 0]
                            if np.size(data) == 0:
                                data = 0
                        if "Sum" in name:
                            data = np.sum(data)
                        elif "Max" in name:
                            data = np.max(data)
                        elif "Min" in name:
                            data = np.min(data)
                        elif "Mean" in name:
                            data = np.mean(data)
                        elif "Number" in name:
                            if not "NotZero" in name:
                                data = np.size(data)
                            else:
                                if not all([x == 0 for x in make_iterable(data)]):
                                    data = np.size(data)
                        else:
                            NumberOutputs = np.size(data)
                else:
                    data = function(snap=snap, INFO=INFO)
                    time = get_Time(snap, considering_h=False)
                print("NumberOutputs")
                print(data)
                print(NumberOutputs)
                for i in range(NumberOutputs):
                    if Data is None:
                        # create Data array here to account for variable output from get_Value function
                        Data = [[] for x in range(2 + NumberOutputs)]
                    Data[i + 2].append(make_iterable(data)[i])
                Data[0].append(i)
                Data[1].append(time)
                snap.close()
                break
            elif indexPath == 1 and i == 0:
                print(path)
                print(pathAlt)
                exit("couldnt find path")

        if np.size(Data[1]) == 0:
            print(Data)
            print(directory)
            print(("%ssnap_0%02d.hdf5" % (directory, i)))
            print(pathAlt)
            raise Exception("couldnt extract times correctly!")
        if singleSnapSufficient:
            break
    print(Data)
    Data = np.array(Data, dtype=object)
    Stuff = np.transpose(Data)
    create_dirs_for_file(filename)
    print(("... saving file %s" % filename))
    np.savetxt(filename, Stuff, fmt=fmt)  # use exponential notation
    return Stuff


class GridObject:
    def __init__(self, grids):
        self.grids = grids
        self.dim = np.shape(grids)[0]
        self.InitialLength = np.shape(grids[0])[0]
        self.includeTopPart = False

    def LimitLength(self, Nmax):
        self.Nmax = Nmax
        for i in range(self.dim):
            if not self.includeTopPart:
                self.grids[i] = self.grids[i][self.InitialLength - Nmax :, :, :]
            else:
                self.InitialLength = np.shape(self.grids[0])[0]
                print("use to reduce by this")
                cutBothSides = (self.InitialLength - Nmax) / 2
                if not cutBothSides == 0:
                    self.grids[i] = self.grids[i][
                        cutBothSides : -1 * cutBothSides, :, :
                    ]

    def reduceTo1D(self):
        for i in range(self.dim):
            self.grids[i] = self.grids[i].reshape(self.Nmax, -1)

    def setIncludeTopPart(self, includeTopPart):
        self.includeTopPart = includeTopPart


def GetCentralCellsGeneral(
    snap,
    variable,
    distanceToTopFromCenter,
    Nwidth=2,
    dx=0.01,
    INFO=None,
    numthreads=8,
    includeTopPart=True,
    factorIncreaseSearchArea=1,
    center=None,
    returnSlice=False,
    sliceAxis=1,
    depthSlice=1.0,
):
    print(variable)
    if center is None:
        center = [snap.boxsize / 2.0 for x in range(3)]
    totalN = Nwidth * factorIncreaseSearchArea
    if Nwidth % 2 and factorIncreaseSearchArea > 1:
        totalN += 1
    ProjBox = [distanceToTopFromCenter, dx * totalN, dx * totalN]
    resolution = [int(distanceToTopFromCenter / dx), totalN, totalN]
    if returnSlice:
        ProjBox[sliceAxis] = depthSlice
        resolution[sliceAxis] = int(depthSlice / dx)

    if includeTopPart:
        ProjBox[0] *= 2
        resolution[0] *= 2
        if resolution[0] % 2:
            print(resolution)
            exit("resolution should be even if including top!")
    print("projbox in get centrall cells general")
    print((ProjBox, resolution))
    if type(variable) == str:
        variable = get_value(
            variable,
            snap,
            considering_h=False,
            convert_units=True,
            INFO=INFO,
            returnLabel=False,
        )
        print((np.min(variable)))
        print((np.max(variable)))
    try:
        dimension = np.shape(variable)[1]
    except:
        dimension = 1
    Grids = []
    for i in range(dimension):
        if dimension > 1:
            AGRID = snap.get_Agrid(
                np.array(variable[:, i], dtype=np.float64),
                res=resolution,
                box=ProjBox,
                center=center,
                numthreads=numthreads,
            )
            grid = AGRID["grid"]
        else:
            AGRID = snap.get_Agrid(
                np.array(variable[:], dtype=np.float64),
                res=resolution,
                box=ProjBox,
                center=center,
                numthreads=numthreads,
            )
            grid = AGRID["grid"]
        cutN = (totalN - Nwidth) / 2
        if cutN != 0:
            print("cutN, totalN, Nwidth, variable")

            print(cutN)
            print(totalN)
            print(Nwidth)
            if returnSlice:
                if sliceAxis == 1:
                    grid = grid[:, :, cutN:-cutN]
                elif sliceAxis == 2:
                    grid = grid[:, cutN:-cutN, :]
                else:
                    exit("slice axis wrong")
            else:
                grid = grid[:, cutN:-cutN, cutN:-cutN]

        Grids.append(grid)
    gObj = GridObject(Grids)
    return gObj


def GetCentralCells(
    snap,
    variable,
    distanceToTopFromCenter,
    Nwidth=2,
    dx=0.01,
    INFO=None,
    numthreads=8,
    includeTopPart=True,
    factorIncreaseSearchArea=1,
):
    center = [snap.boxsize / 2.0 for x in range(3)]
    if includeTopPart:
        center[0] += distanceToTopFromCenter
    else:
        center[0] += distanceToTopFromCenter / 2.0
    totalN = Nwidth * factorIncreaseSearchArea
    if Nwidth % 2 and factorIncreaseSearchArea > 1:
        totalN += 1
    ProjBox = [distanceToTopFromCenter, dx * totalN, dx * totalN]
    resolution = [int(distanceToTopFromCenter / dx), totalN, totalN]

    if includeTopPart:
        ProjBox[0] *= 2
        resolution[0] *= 2
        if resolution[0] % 2:
            print(resolution)
            exit("resolution should be even if including top!")
    values, label = get_value(
        variable,
        snap,
        considering_h=False,
        convert_units=True,
        INFO=None,
        returnLabel=True,
    )
    try:
        dimension = np.shape(values)[1]
    except:
        dimension = 1
    Grids = []
    for i in range(dimension):
        if dimension > 1:
            AGRID = snap.get_Agrid(
                np.array(values[:, i], dtype=np.float64),
                res=resolution,
                box=ProjBox,
                center=center,
                numthreads=numthreads,
            )
            grid = AGRID["grid"]

        else:
            AGRID = snap.get_Agrid(
                np.array(values[:], dtype=np.float64),
                res=resolution,
                box=ProjBox,
                center=center,
                numthreads=numthreads,
            )
            grid = AGRID["grid"]
        cutN = (totalN - Nwidth) / 2
        if cutN != 0:
            grid = grid[:, cutN:-cutN, cutN:-cutN]
        Grids.append(grid)
    gObj = GridObject(Grids)
    return gObj, label


def get_2d_radial_profile(shape_field, box_x, box_y):
    # r=0 is assumed to be field central point
    x = np.linspace(-1.0 * box_x / 2.0, box_x / 2.0, shape_field[0])
    y = np.linspace(-1 * box_y / 2.0, box_y / 2.0, shape_field[1])
    x, y = np.meshgrid(x, y)
    return np.sqrt(x ** 2 + y ** 2)


def fitFunction(
    x_values, y_values, initialGuess, fitfunc, method="curve_fit", bounds=None, **kwargs
):
    """

    Parameters
    ----------
    x_values
    y_values
    initialGuess
    fitfunc
    method: leastsq, curve_fit, polyfit, polyfitlog

    Returns
    -------

    """
    # fitfunc = lambda p, x: p[0]*x
    print("optimizing")
    from scipy import optimize

    if bounds is None:
        bounds = (None, None)

    if method == "leastsq":
        errfunc = lambda p, x, y: fitfunc(*x, *p) - y  # Distance to the target function
        FitParas, success = optimize.leastsq(
            errfunc, initialGuess[:], args=(x_values, y_values)
        )
        print("optimizing fct finished")
    elif method == "curve_fit":
        FitParas, success = optimize.curve_fit(
            fitfunc, x_values, y_values, p0=initialGuess, bounds=bounds, **kwargs
        )
    elif method == "polyfit":
        FitParas, success = np.polyfit(fitfunc, x_values, y_values)
    else:
        raise Exception(f"fitting method {method} not defined!")

    return FitParas, success


def getFoldersOfSnap(snap, splitPath=True):
    path, File = os.path.split(os.path.abspath(snap.file_name))
    if splitPath:
        path = [x for x in path.split("/") if x not in [""]]
    File = os.path.splitext(File)[0]
    return path, File


def getSimName(snap, includeFileName=False):
    folders, File = getFoldersOfSnap(snap)
    folders = folders[-5:-1]
    if includeFileName:
        folders += [File]
    name = convert_params_to_string(folders, string=True)
    return name


class TimeClass:
    def __init__(self, title, timeStart):
        self.title = title
        self.timeStart = timeStart
        self.timeEnd = None

    def setTimeEnd(self, timeEnd, addPrint):
        if self.timeEnd is None:
            self.timeEnd = timeEnd
        self.printTime(self.timeEnd, addPrint)

    def printTime(self, curTime, addPrint):
        if self.timeEnd is not None:
            curTime = self.timeEnd
        print("%s took %g s, %s" % (self.title, curTime - self.timeStart, addPrint))


_dicTime = {}


def timer(
    title="",
    start=False,
    end=False,
    printAll=False,
    addPrint="",
    remove=False,
    delete=False,
):
    """
		give timing updates on unlimited processes called with unique title!
		start or end or none of them has to be chosen
		only start: initialize timer for title
		only end: finish timer, print
		neither: print current time of process
		printAll: print times of all curent titles 
		remove: remove title after printing
		delete: delete all timers saved in dic
	"""
    global _dicTime
    tm = time.time()

    if printAll:
        for t in _dicTime.keys():
            _dicTime[t].printTime(tm, addPrint)

    if title not in _dicTime.keys():
        # print('timer: %s not initialized yet, do this now!' %title)
        start = True
        end = False
    elif start and end:
        print("timer: both actions chosen: giving update!")
        start = False
        end = False
    elif start:
        start = False

    if start:
        _dicTime[title] = TimeClass(title, tm)
    if end:
        _dicTime[title].setTimeEnd(tm, addPrint)
    if (not start and not end) and not printAll:
        _dicTime[title].printTime(tm, addPrint)
    if remove:
        _dicTime.pop(title, None)
    if delete:
        _dicTime = {}
    return


def print_dic(dic):
    for k, v in dic.items():
        print(f"{k}: {v}")


def strings_reduce_by_matches(strings, min_similarity=0.8, delimeter="--"):
    if type(strings) != list:
        raise Exception("need type list!")
    if len(strings) < 2:
        return strings, []
    pairs = list(itertools.combinations(strings, 2))
    similars_dic = {}
    i = 0
    strings_unique = strings[:]
    pairs = [list(p) for p in pairs]
    for pair in pairs:
        added = False
        similarity_score = difflib.SequenceMatcher(None, *pair).ratio()
        if similarity_score > min_similarity:
            for k, d in similars_dic.items():
                if any(x in d for x in pair):
                    added = True
                    similars_dic[k] = list(np.unique(d + pair))
            if not added:
                similars_dic[i] = pair
                i += 1
    strings_combined = []
    for k, strings in similars_dic.items():
        strings_unique = [s for s in strings_unique if s not in strings]
        a = strings[0]
        bs = strings[1:]
        index_order = 0
        s = []
        for index_b, b in enumerate(bs):
            for index, (eq, a0, a1, b0, b1) in enumerate(
                difflib.SequenceMatcher(None, a, b).get_opcodes()
            ):
                if eq == "equal" and index_b == 0:
                    s.append([index_order, a[a0:a1]])
                    index_order += 1
                elif eq == "replace" or eq == "insert" or eq == "delete":
                    if index_b == 0:
                        s.append([index_order, a[a0:a1]])
                        index_order += 1
                    if eq == "replace":
                        s.append([index_order, delimeter + str(b[b0:b1])])
                    elif eq == "insert" or eq == "delete":
                        if b[b0:b1]:
                            s.append(
                                [index_order, delimeter + str(b[b0:b1]) + delimeter]
                            )
                        if a[a0:a1]:
                            s.append(
                                [index_order, delimeter + str(a[a0:a1]) + delimeter]
                            )
                    index_order += 1
        strings_combined.append("".join([x[1] for x in sorted(s, key=lambda x: x[0])]))
    strings_combined = [
        x.replace(delimeter + delimeter, delimeter) for x in strings_combined
    ]
    strings_combined = [
        x[: -len(delimeter)] if x[-len(delimeter) :] == delimeter else x
        for x in strings_combined
    ]
    return strings_unique, strings_combined


def shortenFileName(name):
    import re

    return re.sub("([A-Z]{1}[a-z]{1,1})[a-z]{1,}", r"\1", name)


def PowerSpec(k, kL, kinj, kcrit, A, a, b, c, v=0):
    if v > 0:
        print("powerspec:kL,kinj,kcrit,A,a,b,c", kL, kinj, kcrit, A, a, b, c)
    spec = np.zeros(np.shape(k))
    mask1 = k < kL
    mask2 = np.logical_and(k < kinj, k >= kL)
    mask3 = k >= kinj
    spec[mask1] = 0.0
    spec[mask2] = A * k[mask2] ** a
    B = A / (kinj ** (b - a) * np.exp(-((kinj / kcrit) ** c)))
    spec[mask3] = B * k[mask3] ** b * np.exp(-((k[mask3] / kcrit) ** c))
    return spec


def flatten(l, list_of_list=True):
    if list_of_list:
        return [item for sublist in l for item in sublist]
    else:
        return [item for item in l]


def reduceDicByKeyWords(DIC, keywords, removeKeyword=False, removeKeywordAdd="_"):
    if type(keywords) == str:
        keywords = [keywords]
    new = dict()
    matchedKeys = [key for key in DIC for kw in keywords if kw in key]
    for key in matchedKeys:
        if removeKeyword:
            newKey = [
                re.sub("^%s%s" % (keyw, removeKeywordAdd), "", key)
                for keyw in keywords
                if keyw in key
            ]
            if np.size(newKey) != 1:
                raise Exception("couldnt remove keyword correctly!")
            newKey = newKey[0]
        else:
            newKey = key
        new[newKey] = DIC[key]
    return new


def gaussianFilter(xs, spatialDimensions, sigma):
    return (
        1.0
        / (2 * np.pi * sigma ** 2) ** (spatialDimensions / 2.0)
        * np.exp(-(xs ** 2) / (2 * sigma ** 2))
    )


def convolutionGaussian(image, sigma):
    # use scipy Gaussian convolution for this # https://scipy-lectures.org/intro/scipy/auto_examples/solutions/plot_image_blur.html
    # prossible modes when filter overlaps a border: reflect, constant, nearest, mirror, wrap
    import scipy.ndimage

    return scipy.ndimage.gaussian_filter(image, sigma ** 2, mode="reflect")


# def getFourierFilterMexTopHat(ks, kr, epsilon):
# Fourier filter to circumvent errors in cases without periodic boundaries
# implementation taken from Arévalo+2012 (A mexican hat with holes: Calculating low-resolution power spectra from data with gaps)
# return 2*epsilon*(ks/kr)**2*np.exp(-(ks/kr)**2)


def applyTopHatFilter(gridRealSpace, kAbs, cellsize, spatialDimensions, epsilon=1e-3):
    # PS=|FT(f)|^2
    # Fourier filter to circumvent errors in cases without periodic boundaries
    # implementation taken from Arévalo+2012 (A mexican hat with holes: Calculating low-resolution power spectra from data with gaps)
    print(f"cellsize: {cellsize}, dx: {cellsize**(1/3.)}, epsilon {epsilon}")

    def normalization(kr):
        return (
            epsilon ** 2
            * spatialDimensions
            * (spatialDimensions / 2 + 1)
            * 2 ** (-spatialDimensions / 2 - 1)
            * np.pi ** (spatialDimensions / 2)
            * kr ** spatialDimensions
        )

    FilteredPS = np.zeros(np.shape(kAbs))
    for indexKr, kr in enumerate(kAbs.flatten()):
        if kr == 0:
            continue
        sigma = (
            1.0
            / (2 * np.pi ** 2) ** (1.0 / 2.0)
            / kr
            / cellsize ** (1.0 / spatialDimensions)
        )
        sigma1 = sigma / (1 + epsilon) ** (1.0 / 2.0)
        sigma2 = sigma * (1 + epsilon) ** (1.0 / 2.0)
        # print(f"sigma1 {sigma1}, sigma2 {sigma2}, kr {kr}, dx {cellsize**(1./spatialDimensions)}")
        # assuming uniformly spaced values
        variance = np.sum(
            np.abs(
                convolutionGaussian(gridRealSpace, sigma1)
                - convolutionGaussian(gridRealSpace, sigma2)
            )
            ** 2
            * cellsize
        )
        # print(variance, normalization(kr))
        FilteredPS.ravel()[indexKr] = variance / normalization(kr)
        # print(convolutionGaussian(gridRealSpace, sigma1))
    return kAbs, FilteredPS


def env_variables_snapshot():
    """

    Returns
    -------

    """
    GRID_FOLDER = os.getenv("GRID_FOLDER")
    if GRID_FOLDER is None:
        GRID_FOLDER = "/tmp/test/"

    LOADPLOTDATA = os.getenv("LOADPLOTDATA")
    if LOADPLOTDATA is None:
        LOADPLOTDATA = True
    else:
        LOADPLOTDATA = int(LOADPLOTDATA)

    REMOVE_GRID_FOLDER_AFTER_PLOT = os.getenv("REMOVE_GRID_FOLDER_AFTER_PLOT")
    if REMOVE_GRID_FOLDER_AFTER_PLOT is None:
        REMOVE_GRID_FOLDER_AFTER_PLOT = True
    else:
        REMOVE_GRID_FOLDER_AFTER_PLOT = int(REMOVE_GRID_FOLDER_AFTER_PLOT)

    SAVE_GRID_WITH_PROCESSOR_ID = os.getenv("SAVE_GRID_WITH_PROCESSOR_ID")
    if SAVE_GRID_WITH_PROCESSOR_ID is None:
        SAVE_GRID_WITH_PROCESSOR_ID = True
    else:
        SAVE_GRID_WITH_PROCESSOR_ID = int(SAVE_GRID_WITH_PROCESSOR_ID)

    return (
        GRID_FOLDER,
        LOADPLOTDATA,
        REMOVE_GRID_FOLDER_AFTER_PLOT,
        SAVE_GRID_WITH_PROCESSOR_ID,
    )


def get_default_args(func):
    signature = inspect.signature(func)
    return {
        k: v.default
        for k, v in signature.parameters.items()
        if v.default is not inspect.Parameter.empty
    }


def latex_float(f, force_exponents=False, avoid_base=False):
    if not force_exponents:
        float_str = "{0:.2g}".format(f)
    else:
        float_str = "{0:.2e}".format(f)
    if "e" in float_str:
        base, exponent = float_str.split("e")
        if avoid_base and float(base) == 1.0:
            return r"10^{{{0}}}".format(int(exponent))
        return r"{0}\!\times\!10^{{{1}}}".format(base, int(exponent))
    else:
        return float_str


def minimum_resolution(length, snap):
    return np.ceil(length / np.min(snap.vol) ** (1 / 3.0))


@timing
def get_energy_logfile(folder, variables=None, return_dictionary=True, info=None):
    import pandas as pd
    import FigureMove as Fig

    header = [
        "time",
        "EnergyInt",
        "EnergyPot",
        "EnergyKin",
        "EnergyIntComp0",
        "EnergyPotComp0",
        "EnergyKinComp0",
        "EnergyIntComp1",
        "EnergyPotComp1",
        "EnergyKinComp1",
        "EnergyIntComp2",
        "EnergyPotComp2",
        "EnergyKinComp2",
        "EnergyIntComp3",
        "EnergyPotComp3",
        "EnergyKinComp3",
        "EnergyIntComp4",
        "EnergyPotComp4",
        "EnergyKinComp4",
        "EnergyIntComp5",
        "EnergyPotComp5",
        "EnergyKinComp5",
        "MassComp0",
        "MassComp1",
        "MassComp2",
        "MassComp3",
        "MassComp4",
        "MassComp5",
        "egyinjtot",
    ]
    filename = os.path.join(folder, "energy.txt")
    df = pd.read_csv(filename, delimiter="\s+", names=header, header=None)
    snap_num = Fig.get_number_snapshots(
        folder, snapbase="snap_", snapdirbase="snapdir_",
    )[0]
    snap = Fig.quickImport(snap_num, folder=folder, onlyHeader=True)
    if variables is None:
        variables = header[:]
    if "time" in variables:
        unit_time = checkIfInDictionary("UnitTimeEnergyLog", info, "Myr")
        if unit_time in ["Myr", "cgs"]:
            df["time"] = (
                df["time"]
                * snap.UnitLength_in_cm
                / snap.UnitVelocity_in_cm_per_s
                / snap.hubbleparam
            )
            if unit_time == "Myr":
                df["time"] /= param.SEC_PER_MEGAYEAR
    if "egyinjtot" in variables:
        df["egyinjtot"] *= (
            snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2 / snap.hubbleparam
        )
    if return_dictionary:
        return df.to_dict()
    else:
        return [df[x].values for x in variables]


@timing
def get_jet_accelerated_cr_energy_logfile(
    folder, variables=None, return_dictionary=True, info=None
):
    import pandas as pd
    import FigureMove as Fig

    header = [
        "time",
        "CumBlackHoleJetCrEnergyAccelerated",
    ]
    filename = os.path.join(folder, "blackhole_jet_cr_acceleration.txt")
    df = pd.read_csv(filename, delimiter="\s+", names=header, header=None)
    snap_num = Fig.get_number_snapshots(
        folder, snapbase="snap_", snapdirbase="snapdir_",
    )[0]
    snap = Fig.quickImport(snap_num, folder=folder, onlyHeader=True)
    if variables is None:
        variables = header[:]
    if "time" in variables:
        unit_time = checkIfInDictionary("UnitTimeEnergyLog", info, "Myr")
        if unit_time in ["Myr", "cgs"]:
            df["time"] = (
                df["time"]
                * snap.UnitLength_in_cm
                / snap.UnitVelocity_in_cm_per_s
                / snap.hubbleparam
            )
            if unit_time == "Myr":
                df["time"] /= param.SEC_PER_MEGAYEAR
    if "CumBlackHoleJetCrEnergyAccelerated" in variables:
        df["CumBlackHoleJetCrEnergyAccelerated"] *= (
            snap.UnitMass_in_g * snap.UnitVelocity_in_cm_per_s ** 2 / snap.hubbleparam
        )
    if return_dictionary:
        return df.to_dict()
    else:
        return [df[x].values for x in variables]


@timing
def get_blackholes_logfile(folder, variables=None, return_dictionary=True, info=None):
    """

    Parameters
    ----------
    folder
    variables ["time", "tot_number_bhs", "total_mass_bhs", "total_mdot", "total_mdot_in_sun_year", "total_mass_real", "total_mdoteddington"]
    return_dictionary
    info

    Returns
    -------

    """
    import pandas as pd
    import FigureMove as Fig

    header = [
        "time",
        "tot_number_bhs",
        "total_mass_bhs",
        "total_mdot",
        "total_mdot_in_sun_year",
        "total_mass_real",
        "total_mdoteddington",
    ]
    filename = os.path.join(folder, "blackholes_cleaned.txt")
    if os.path.exists(filename):
        df = pd.read_csv(filename, delimiter=",", names=header, header=None)
    else:
        filename = os.path.join(folder, "blackholes.txt")
        df = pd.read_csv(filename, delimiter="\s+", names=header, header=None)
    snap_num = Fig.get_number_snapshots(
        folder, snapbase="snap_", snapdirbase="snapdir_",
    )[0]
    snap = Fig.quickImport(snap_num, folder=folder, onlyHeader=True)
    # print(f"f: {f}")
    if not df["time"].is_monotonic_increasing:
        df = clean_blackholes_txt(df)
        filename = os.path.join(folder, "blackholes_cleaned.txt")
        df.to_csv(filename, header=None, index=False)
        # raise Exception(f"time in {filename} is not monotonic increasing!")
    if variables is None:
        variables = header[:]
    if "time" in variables:
        unit_time = checkIfInDictionary("unit_time_blackholes_log", info, "Myr")
        if unit_time in ["Myr", "cgs"]:
            df["time"] = (
                df["time"]
                * snap.UnitLength_in_cm
                / snap.UnitVelocity_in_cm_per_s
                / snap.hubbleparam
            )
            if unit_time == "Myr":
                df["time"] /= param.SEC_PER_MEGAYEAR
    # print(f"variables: {variables}")
    if return_dictionary:
        return df.to_dict()
    else:
        return [df[x].values for x in variables]


def clean_blackholes_txt(df):
    """
    Logfile blackholes.txt appended during arepo run.
    if times recalculated due to crashes and restarts from snapshots,
    duplicates may appear which are removed here.

    Parameters
    ----------
    df : expects blackholes.txt logfile as pandas dataframe

    Returns df : cleaned dataframe
    -------

    """
    all_start_indices = np.where(np.diff(df["time"]) < 0)[0]
    print(f"number cuts necessary: {len(all_start_indices)}")
    iteration = 0
    while len(np.where(np.diff(df["time"]) < 0)[0]):
        iteration += 1
        if iteration > 100:
            raise Exception(f"iteration = {iteration}")
        problematic_indices = np.where(np.diff(df["time"]) < 0)[0]
        current_index = problematic_indices[0]
        problematic_time = df["time"][current_index]
        next_index = (
            np.where(df["time"][current_index + 1 :] > problematic_time)[0][0]
            + current_index
        )
        print(
            f"dropping: from {current_index + 1} to {next_index + 1}, dropped {next_index - current_index - 1} indices"
        )
        print(f"time: {problematic_time}, next time {df['time'][current_index+1]}")
        print(
            f"time last cut: {df['time'][next_index]}, time last {df['time'][next_index+ 1]}"
        )
        df.drop(list(np.arange(current_index + 1, next_index + 1)), inplace=True)
        df.reset_index(inplace=True, drop=True)
    if not df["time"].is_monotonic_increasing:
        raise Exception(f"time still not monotonic increasing!")
    return df


def convert_list_from_kpc_to_code(snap, lis):
    # ignoring h
    return [x * param.KiloParsec_in_cm / snap.UnitLength_in_cm for x in lis]


@contextmanager
def print_array_on_one_line():
    oldoptions = np.get_printoptions()
    np.set_printoptions(linewidth=np.inf)
    yield
    np.set_printoptions(**oldoptions)


def get_dummy_value(value=1, info=None):
    value = checkIfInDictionary("value", info, value)
    return value


def get_dummy_value_log(folder, value=1, info=None):
    v = get_blackholes_logfile(
        folder, variables=["time"], return_dictionary=False, info=info
    )
    value = checkIfInDictionary("value", info, value)
    return np.ones_like(v) * value


def get_delta_time_logfile(folder, info=None):
    t = get_blackholes_logfile(
        folder, variables=["time"], return_dictionary=False, info=info
    )
    diff = np.diff(t)
    t = list(np.append(diff, diff[0][-1]).reshape(1, -1))
    return t


def get_classical_cooling_rate(snap, info=None):
    import feature_extraction

    tcool = checkIfInDictionary("coolingTimeInGyr", info, 3) * 1e3
    dist = get_DistanceToCenterInKpc(snap)
    radius = feature_extraction.get_position_in_kpc_of_mean_variable(
        tcool, snap, variable="CoolingTimescaleInMyr", weight="XrayLuminosity"
    )
    print(f"classical_cooling_rate: radius: {radius}")
    mass = get_MassInSolar(snap)
    mdot = np.sum(mass[dist < radius]) / (tcool * 1e6)
    return mdot
