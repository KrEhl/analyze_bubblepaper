# import auxiliary_functions as aux
import numpy as np

import auxiliary_functions as aux


def debug_jet(snap, conv_units=True):
    dic = {
        "BH_CumMassGrowth_QM": "bcmq",
        "BH_CumMassGrowth_RM": "bcmr",
        "BH_CumEgyInjection_QM": "bceq",
        "BH_CumEgyInjection_RM": "bcer",
        "BH_Hsml": "bhhs",
        "BH_Pressure": "bhpr",
        "BH_U": "bhu",
        "BH_Density": "bhro",
        "BH_Mdot": "bhmd",
        "BH_Mdot_Radio": "bhmr",
        "BH_Mdot_Quasar": "bhmq",
        "BH_Mass": "bhma",
        "BH_MdotEddington": "bhed",
        "BH_ChaoticAccColdMass": "bccm",
        "BH_ChaoticAccMdot": "bcmd",
        "BH_ChaoticAccMdotMonitor": "bcmm",
        "BH_ChaoticAccMdotOverMedd": "bcmo",
        "BH_ChaoticAcc_CumMass": "bcam",
        "BH_CumKinEgyInjectedInJet": "bhjk",
        "BH_CumThermEgyInjectedInJet": "bhjt",
        "BH_CumKinEgyInjectedInBuffer": "bhbk",
        "BH_CumThermEgyInjectedInBuffer": "bhbt",
        "BH_CumMagnEgyInjectedInJet": "bhjm",
        "BH_CumCREgyInjectedInJet": "bhjc",
        "BH_JetBufferMass": "bhbm",
        "BH_JetBufferMomentum": "bhbp",
        "BH_JetBufferEnergy": "bhbe",
        "BH_JetBufferScalarConserved": "bhbs",
        "BH_TimeStep": "bhts",
    }
    unit_dic = {
        "BH_CumMassGrowth_QM": "mass",
        "BH_CumMassGrowth_RM": "mass",
        "BH_CumEgyInjection_QM": "energy",
        "BH_CumEgyInjection_RM": "energy",
        "BH_Hsml": "length",
        "BH_Pressure": "pressure",
        "BH_U": "u",
        "BH_Density": "density",
        "BH_Mdot": "masspertime",
        "BH_Mdot_Radio": "masspertime",
        "BH_Mdot_Quasar": "masspertime",
        "BH_Mass": "mass",
        "BH_MdotEddington": "masspertime",
        "BH_ChaoticAccColdMass": "mass",
        "BH_ChaoticAccMdot": "masspertime",
        "BH_ChaoticAccMdotMonitor": "masspertime",
        "BH_ChaoticAccMdotOverMedd": "none",
        "BH_ChaoticAcc_CumMass": "mass",
        "BH_CumKinEgyInjectedInJet": "energy",
        "BH_CumThermEgyInjectedInJet": "energy",
        "BH_CumKinEgyInjectedInBuffer": "energy",
        "BH_CumThermEgyInjectedInBuffer": "energy",
        "BH_CumMagnEgyInjectedInJet": "energy",
        "BH_CumCREgyInjectedInJet": "energy",
        "BH_JetBufferMass": "mass",
        "BH_JetBufferMomentum": "momentum",
        "BH_JetBufferEnergy": "energy",
        "BH_JetBufferScalarConserved": "none",
        "BH_TimeStep": "time",
    }
    variable = []
    variable_name = 1
    conversion_dic = aux.get_unit_conversion(snap)
    for k, v in dic.items():
        if v in snap.data.keys():
            conv = 1
            if conv_units:
                conv = conversion_dic[unit_dic[k]]
            print(f"{k}: {snap.data[v][0]*conv}")
        else:
            print(f"{k} not found!")
    return


def getJetLogDataJetDebug(directory):
    """
	if BH_JET_DEBUG active:
	0 All.Time ,
	1 P[idx].ID,
	2 BPP(idx).BH_JetEnergy,
	3 BPP(idx).RedistributedEnergy,
	4 EgyMagnIsolation,
	5 BPP(idx).RedistributedMass,
	6 BPP(idx).RedistributedMomentum[0],\
	7 BPP(idx).RedistributedMomentum[1],
	8 BPP(idx).RedistributedMomentum[2],
	9 BPP(idx).JetDir[0],
	10 BPP(idx).JetDir[1],
	11 BPP(idx).JetDir[2],\
	12 BPP(idx).InjectedThermalEnergyInJetRegion,
	13 BPP(idx).InjectedCREnergyInJetRegion,
	14 BPP(idx).InjectedKineticEnergyInJetRegion,
	15 BPP(idx).InjectedMagneticEnergyInJetRegion, \
	16 BPP(idx).DebugJetMass[0],
	17 BPP(idx).DebugJetMass[1],
	18 BPP(idx).DebugJetMomentum[0],
	19 BPP(idx).DebugJetMomentum[1],
	20 BPP(idx).DebugJetVolume[0],
	21 BPP(idx).DebugJetVolume[1], \
	22 BPP(idx).BH_RedistThermEgyFromJetRegion,
	23 BPP(idx).BH_RedistCREgyFromJetRegion,
	24 BPP(idx).BH_RedistKinEgyFromJetRegion,
	25 BPP(i).BH_RedistThermEgy ,
	26 BPP(i).BH_RedistKinEgy, \
	27 BPP(idx).BH_NonadiabaticThermalEgyBuffer );
	"""
    SimplifiedPath = "blackhole_jet/blackhole_jet_AllSorted.txt"
    if os.path.isfile(directory + SimplifiedPath):
        Data = np.loadtxt(directory + SimplifiedPath)
        return Data
    print(
        (
            "couldnt find %s, had to open inidividual files and sort them!"
            % (directory + SimplifiedPath)
        )
    )
    n_cores = 0
    while True:
        try:
            file = open(directory + "blackhole_jet/blackhole_jet_%d.txt" % n_cores)
            file.close()
            n_cores += 1
        except:
            break

    print(("number of files = ", n_cores))
    DATA = []
    firstgone = False
    for i in np.arange(n_cores):
        data = np.loadtxt(directory + "blackhole_jet/blackhole_jet_%d.txt" % i)
        if np.shape(data)[0] != 0 and not firstgone:
            DATA = data
            firstgone = True
        elif np.shape(data)[0] != 0:
            DATA = np.vstack((DATA, data))
    shape = np.shape(DATA)
    i_sorted = np.argsort(DATA[:, 0])
    DATA = DATA[i_sorted, :]
    np.savetxt(directory + SimplifiedPath, DATA)  # , fmt='%i %1.4e')
    return DATA


def print_big(x):
    print("//////////////||||||||||||||||||||||||||")
    print(x)
    print("//////////////||||||||||||||||||||||||||")


def print_kwargs(**kwargs):
    for k, v in kwargs.items():
        print(f"{k}: {v}")


def visualizeH5pyAttr(file):
    print("Header:")
    for k in file.attrs.keys():
        print("{} => {}".format(k, file.attrs[k]))


def visualizeH5pyFields(file):
    print("Fields:")
    np.set_printoptions(threshold=6)
    for k in list(file.keys()):
        print("{} => {}".format(k, np.array(file[k])))
    np.set_printoptions(threshold=1000)


def visualizeH5py(file, show_fields=True, show_attr=True):
    if show_attr:
        visualizeH5pyAttr(file)
    if show_fields:
        visualizeH5pyFields(file)


def visualizeH5pyFieldsSnap(file, types=False, header_only=False):
    # np.set_printoptions(threshold=6)
    print("--------------")
    print("Header:")
    print("--------------")
    for k in file["/Header"].attrs.keys():
        print("{} => {}".format(k, file["/Header"].attrs[k]))
        if types:
            print("type: {}".format(type(file["/Header"].attrs[k])))
    if header_only:
        return
    for k in list([x for x in file.keys() if x != "Header"]):
        print("--------------")
        print(f"{k}:")
        print("--------------")
        for field in file[k].keys():
            print("{} => \n\t {}".format(field, np.array(file[f"{k}/{field}"])))
            if types:
                print(
                    "\t type: {}".format(type(np.array(file[f"{k}/{field}"]).take(0)))
                )
    # np.set_printoptions(threshold=1000)


def printDebug(
    data, name="data", e=False, zero=False, dontShow=["data", "median", "sametype"]
):
    if not aux.isIterable(data):
        pass
    elif len(data) == 0:
        print(f"{name}: EMPTY ARRAY")
        return
    if data is None:
        print(f"data is None: {data}")
        return
    if not "data" in dontShow:
        print((name, data))
    if not "min" in dontShow:
        print((name, ":min", np.min(data)))
    print((name, ":max", np.max(data)))
    if "median" not in dontShow:
        print((name, ":median", np.median(data)))
    print((name, ":mean", np.mean(data)))
    print((name, ":shape", np.shape(data)))
    print((name, ":number of nans", np.count_nonzero(np.isnan(data))))
    if "type" not in dontShow:
        if type(data) == list:
            t = type(next(iter(data or []), None))
        elif not aux.isIterable(data):
            t = type(data)
        else:
            t = type(data.item(0))
        print(f"type: {type(data)}")
        print(f"type first element: {t}")
    if "sametype" not in dontShow:
        print(f"element type:, all same type: {aux.checkCommonType(data)}")
    if zero:
        print((name, ":non-zero", np.size(data[data != 0])))
        print((name, ":zero", np.size(data[data == 0])))
    if e:
        raise Exception("Debug!")