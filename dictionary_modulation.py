import auxiliary_functions as aux
import numpy as np


def convert_dic_to_list(dic):
    l = []
    for k, i in dic.items():
        l.extend([k, i])
    return l


def merge_list_dic_to_single_dic(macro_dic_or_list):
    new_dic = {}
    import copy

    # make sure only dics in list, not sublist with dictionaries
    dic_list = []
    for element in macro_dic_or_list:
        if type(element) == dict:
            dic_list.append(element)
        elif aux.isIterable(element):
            dic_list.extend(element)
        else:
            raise Exception(
                f"element: {element} with this type: {type(element)} "
                f"shouldn be in macro_dic_or_list: {macro_dic_or_list}"
            )

    for dic in dic_list:
        for k, v in dic.copy().items():
            v = copy.copy(v)
            if k in new_dic.keys():
                if type(k) == list:
                    new_dic[k].append(v)
                else:
                    new_dic[k].extend(v)
            else:
                new_dic[k] = v
    return new_dic


def get_dic_value_from_list(value, list_of_dic):
    v = []
    for l in list_of_dic:
        v.append(l[value])
    return v


def get_unique_dic_value_from_list(value, list_of_dic):
    values = get_dic_value_from_list(value, list_of_dic)
    return np.unique(values)
