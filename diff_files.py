import time
import re
import argparse_helpers
import argparse
import numpy as np

PARAMETER_EXCEPTIONS = "ResubmitCommand"
MAX_PARAMETER_NAME = 30
MAX_PARAMETER_LENGTH = 25


def convert_value_entries(x, setup="config"):
    k, v = None, None
    initial_value = x
    if setup == "config":
        x = re.sub("=.*", "", x)
    elif setup == "param":
        x = x.split()[0]
    elif setup == "clusterparam":
        x, y = x.split(":")
        v = y
        k = x
    if x != initial_value:
        # print(f"x != initial_value: {x}, {initial_value}")
        if setup == "config":
            k, v = initial_value.split("=")
        elif setup == "param":
            temp = initial_value.split()
            if len(temp) > 2:
                temp = re.sub(";.*", "", initial_value).split()
                if temp[0] in PARAMETER_EXCEPTIONS:
                    temp = (temp[0], temp[1:])
                elif len(temp) != 2:
                    raise Exception(
                        f"initial_value: {initial_value} still too large;;; {temp}"
                    )
            elif len(temp) < 2:
                raise Exception(f"parameter split failed! no float given? {temp}")
            k, v = temp
        v = argparse_helpers.fancy_eval(v)
    return x, k, v


def load_files(filename, setup):
    if setup == "config":
        f = np.loadtxt(filename, dtype=str)
    elif setup == "param" or setup == "clusterparam":
        f = np.loadtxt(filename, comments=("%", "#"), dtype="str", delimiter="/l")
        # skip empty lines:
        f = [x for x in f if x.strip()]
    return f


def diff_files(f1_filename, f2_filename, setup, options=None):
    if options is None:
        options = ""
    file_f1 = load_files(f1_filename, setup)
    file_f2 = load_files(f2_filename, setup)

    f1 = {}
    for x in file_f1:
        x, k, v = convert_value_entries(x, setup=setup)
        f1[x] = v
    f2 = {}
    for x in file_f2:
        x, k, v = convert_value_entries(x, setup=setup)
        f2[x] = v
    print("-------")
    print(f"{f2_filename} is missing:")
    value_entry_f1 = {}
    for k, v in f1.items():
        if k not in f2:
            if v is None:
                print(k)
            else:
                print(f"{k}: {v}")
        elif k is not None:
            value_entry_f1[k] = v
    print("-------")
    print(f"{f1_filename} is missing:")
    value_entry_f2 = {}
    for k, v in f2.items():
        if k not in f1:
            if v is None:
                print(k)
            else:
                print(f"{k}: {v}")
        elif k is not None:
            value_entry_f2[k] = v
    print("-------")
    print(f"{f1_filename} different values than {f2_filename}")
    if len(value_entry_f1) != len(value_entry_f2):
        raise Exception(
            f"value_entry_f1:{value_entry_f1} should have same size as value_entry_f2:{value_entry_f2}"
        )
    for k, v1 in value_entry_f1.items():
        v2 = value_entry_f2[k]
        if isinstance(v2, str):
            v2 = v2.strip()
        if isinstance(v1, str):
            v1 = v1.strip()
        if v1 != v2:
            fill = (MAX_PARAMETER_NAME - len(k)) * " "
            fill1 = (MAX_PARAMETER_LENGTH - len(str(v1))) * " "
            print(f"k:{k}{fill}\t{v1}{fill1} !=\t\t{v2}")
    return


if __name__ == "__main__":
    start_time = time.time()
    parser = argparse.ArgumentParser(
        description="diff between different files",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("f1", nargs=None, help="file 1")
    parser.add_argument("f2", nargs=None, help="file 1")
    parser.add_argument(
        f"--setup", type=argparse_helpers.fancy_eval, nargs=None, default="NONE",
    )
    input = parser.parse_args().__dict__.copy()
    input = {k: v for k, v in input.items() if v is not None}
    f1, f2 = input["f1"], input["f2"]
    setup = input["setup"]
    if setup == "NONE":
        if "parameters" in f1 or "parameters" in f2:
            setup = "clusterparam"
        elif "param" in f1 or "param" in f2:
            setup = "param"
        elif "config" in f1.lower() or "config" in f2.lower():
            setup = "config"
        else:
            raise Exception(f"couldnt be determined automatically!")
    diff_files(f1, f2, setup, options=None)
    print(f1, f2)
