import numpy as np
import math
from matplotlib.patches import Ellipse
import matplotlib.pyplot as plt

import Param
import auxiliary_functions as aux
from FigureMove import (
    getAgrid,
    do_Radprof,
    interpolate_nans,
    save_1PlotPlain,
    do_Projection,
    save_pcolormesh,
    SmoothFunction,
    get_mesh_data,
    do_radprof_snap,
    do_radprof_snap_time_average,
    get_times_in_range,
    plot_radprof,
)


def do_IsolateFeatures(
    snap,
    variable,
    ProjBox,
    method="2dsubtract",  # radialsubtract 2d_subtract fourier_smooth
    weight=None,
    resolution=300,
    center=None,
    numthreads=4,
    axis=[2, 0],
    INFO=None,
    save_extra_plots=False,
    verbose=False,
    smooth=True,
    smoothing_bins=30,
):
    print(f"smoothing_bins: {smoothing_bins}")
    method = aux.checkIfInDictionary("method", INFO, method)
    center = aux.checkIfInDictionary("center", INFO, center)
    gradient_filter = False
    smooth_final_image = False
    smooth_pixel_number = 5 // max(
        x / y
        for x, y in zip(
            [ProjBox[axis[0]], ProjBox[axis[1]]],
            [resolution[axis[0]], resolution[axis[1]]],
        )
    )
    if smooth_pixel_number == 0:
        smooth_pixel_number = 1
    if center is None:
        center = snap.center
    if method == "radialsubtract":
        data = get_3d_feature_extraction_radialprofsmooth(
            INFO,
            ProjBox,
            axis,
            center,
            numthreads,
            resolution,
            save_extra_plots,
            smooth,
            snap,
            variable,
            weight,
        )
    elif method == "fouriersmooth":
        data = get_feature_extraction_fouriersmooth(
            INFO,
            ProjBox,
            axis,
            center,
            numthreads,
            resolution,
            smooth_pixel_number,
            snap,
            variable,
            weight,
        )
    elif method == "2dsubtract":
        data = get_2d_feature_extraction(
            INFO,
            ProjBox,
            axis,
            center,
            numthreads,
            resolution,
            save_extra_plots,
            snap,
            variable,
            weight,
            smoothing_bins,
            relative=True,
        )
    elif method == "2dsubtractnotrelative":
        # exit("das")
        data = get_2d_feature_extraction(
            INFO,
            ProjBox,
            axis,
            center,
            numthreads,
            resolution,
            save_extra_plots,
            snap,
            variable,
            weight,
            smoothing_bins,
            relative=False,
        )
    else:
        raise Exception(f"method: {method} not implemented yet in do_IsolateFeatures!")
    if gradient_filter:
        from scipy import ndimage, misc

        data = ndimage.gaussian_gradient_magnitude(data, sigma=5)
    if smooth_final_image:
        from scipy.ndimage import gaussian_filter

        data = gaussian_filter(data, sigma=smooth_pixel_number)
    return data


def get_pdV_energy_in_erg(snap, info=None):
    (
        contour_fit_params,
        contour_fit_coords,
        contour_fit_volume,
        axis,
        center,
    ) = get_isolated_ellipses(snap, info=info)
    PDV = []
    for params, v in zip(contour_fit_params, contour_fit_volume):
        volume = v * snap.UnitLength_in_cm ** 3
        cx, cy, a, b, angle = params
        cx = cx - center[axis[0]]
        cy = cy - center[axis[1]]
        dist = math.sqrt(cx ** 2 + cy ** 2)
        pth = get_mean_variable_at_position_in_kpc(
            dist * snap.UnitLength_in_cm / Param.KiloParsec_in_cm,
            snap,
            variable="ThermalPressure",
            weight="vol",
        )
        pdV = pth * volume
        PDV.append(pdV)
    return sum(PDV)


def get_pdV_power_in_ergpersec(snap, info=None):
    (
        contour_fit_params,
        contour_fit_coords,
        contour_fit_volume,
        axis,
        center,
    ) = get_isolated_ellipses(snap, info=info)
    PBUBBLE = []
    for params, v in zip(contour_fit_params, contour_fit_volume):
        volume = v * snap.UnitLength_in_cm ** 3
        cx, cy, a, b, angle = params
        cx = cx - center[axis[0]]
        cy = cy - center[axis[1]]
        dist = math.sqrt(cx ** 2 + cy ** 2)
        pth = get_mean_variable_at_position_in_kpc(
            dist * snap.UnitLength_in_cm / Param.KiloParsec_in_cm,
            snap,
            variable="ThermalPressureABOVE0p2keVBELOW10keV",
            weight="vol",
        )
        print(f"ThermalPressureABOVE0p2keVBELOW10keV at position: {pth}")
        timescale = _get_bubble_timescale(
            snap, volume, dist * snap.UnitLength_in_cm, info
        )
        pbubble = pth * volume / timescale
        print(f"pbubble: {pbubble}")
        print(f"timescale: {timescale}")
        print(f"volume: {volume}")
        print(f"dist in kpc: {dist * snap.UnitLength_in_cm / Param.KiloParsec_in_cm}")
        PBUBBLE.append(pbubble)
    return sum(PBUBBLE)


def get_average_timescale_in_myr(snap, info=None):
    (
        contour_fit_params,
        contour_fit_coords,
        contour_fit_volume,
        axis,
        center,
    ) = get_isolated_ellipses(snap, info=info)
    TIMESCALE = []
    for params, v in zip(contour_fit_params, contour_fit_volume):
        volume = v * snap.UnitLength_in_cm ** 3
        cx, cy, a, b, angle = params
        cx = cx - center[axis[0]]
        cy = cy - center[axis[1]]
        dist = math.sqrt(cx ** 2 + cy ** 2)
        timescale = _get_bubble_timescale(
            snap, volume, dist * snap.UnitLength_in_cm, info
        )
        timescale /= Param.SEC_PER_MEGAYEAR
        TIMESCALE.append(timescale)
    print(f"TIMESCALE: {TIMESCALE}")
    return np.mean(TIMESCALE)


def _get_bubble_timescale(snap, ellipsoid_volume_in_cm3, dist_in_cm, info):
    timescale = aux.checkIfInDictionary("timescale", info, "soundcrossingtime")
    radius = (ellipsoid_volume_in_cm3 / (4 / 3.0 * np.pi)) ** (1.0 / 3.0)
    sound_crossing_time = _get_soundcrossingtime(
        dist_in_cm / Param.KiloParsec_in_cm, radius, snap
    )
    if timescale == "soundcrossingtime":
        time = sound_crossing_time
    elif timescale == "buoyancytime":
        drag_coefficient = 0.6
        time = sound_crossing_time * np.sqrt(
            3 * drag_coefficient * Param.GAMMA / 16.0 * radius / dist_in_cm
        )
    elif timescale == "risetime":
        time = sound_crossing_time * np.sqrt(2 * Param.GAMMA * dist_in_cm / radius)
    return time


def _get_soundcrossingtime(dist, radius, snap):
    temperature = get_mean_variable_at_position_in_kpc(
        dist, snap, variable="TemperatureABOVE0p2keVBELOW10keV", weight="SQTMass"
    )
    print(f"temperature at position: {temperature}")
    time = radius * np.sqrt(
        Param.meanmolecularweight
        * Param.PROTONMASS
        / (Param.GAMMA * Param.BOLTZMANN_cgs * temperature)
    )
    return time


def get_mean_variable_at_position_in_kpc(
    dist, snap, variable="ThermalPressure", weight="vol", force_non_zero=True,
):
    select = "TemperatureInkV>0.2ANDTemperatureInkV<10"
    x, y, err, label = do_radprof_snap(
        number=snap.file_number,
        variable=variable,
        snap=snap,
        weight=weight,
        select=select,
        mask=None,
        nbins=100,
        range=[1e-1, 500],
        method="mean",
        postprocess=None,
        method_error="std",
        error_bar_style="shaded",
        log=True,
        folder=snap.file_name_abs,
    )
    m = np.logical_not(np.isnan(y))
    if force_non_zero:
        m = np.logical_and(y != 0, m)
    return np.interp(dist, x[m], y[m])


def get_position_in_kpc_of_mean_variable(
    value,
    snap,
    variable="ThermalPressure",
    weight="vol",
    force_non_zero=True,
    select="TemperatureInkV>0.2ANDTemperatureInkV<10",
    log=True,
    range=[1e-1, 500],
    option="first",
    ascending_values=True,
):
    x, y, err, label = do_radprof_snap(
        number=snap.file_number,
        variable=variable,
        snap=snap,
        weight=weight,
        select=select,
        mask=None,
        nbins=100,
        range=range,
        method="mean",
        postprocess=None,
        method_error="std",
        error_bar_style="shaded",
        log=log,
        folder=snap.file_name_abs,
    )
    m = np.logical_not(np.isnan(y))
    if force_non_zero:
        m = np.logical_and(y != 0, m)
    if option == "first":
        for i, j in zip(x[m], y[m]):
            if ascending_values:
                if j > value:
                    return i
            else:
                if j < value:
                    return i
        raise Exception("value not found!")
    else:
        raise Exception(f"option: {option} not implemented!")


def get_mean_variable_at_position_in_kpc_average_time(
    snap,
    info=None,
    dist_in_kpc=10,
    variable="ThermalPressure",
    weight="vol",
    force_non_zero=True,
    save_fig=False,
):
    print(f"info: {info}")
    range = [1e-1, 500]
    variable = aux.checkExistenceKey(info, "variableMeanVariable", variable)
    select = aux.checkExistenceKey(
        info, "variableMeanSelect", "TemperatureInkV>0.2ANDTemperatureInkV<10"
    )
    weight = aux.checkExistenceKey(info, "variableMeanWeight", weight)
    averaging_range = aux.checkExistenceKey(info, "variableMeanAveragingRangeInMyr", 10)
    save_fig = aux.checkExistenceKey(info, "variableMeanSaveFig", save_fig)
    dist_in_kpc = aux.checkExistenceKey(info, "variableMeanDistInKpc", dist_in_kpc)
    numbers = list(
        get_times_in_range(
            snap.folder,
            snap.time_in_myr - averaging_range / 2.0,
            snap.time_in_myr + averaging_range / 2.0,
        )
    )
    print(f"dist_in_kpc: {dist_in_kpc}")
    if dist_in_kpc < range[0] or dist_in_kpc > range[1]:
        raise Exception(
            f"range {range} is not big enough to get dist_in_kpc {dist_in_kpc}"
        )
    x, y, err, label = do_radprof_snap_time_average(
        variable=variable,
        folder=snap.folder,
        numbers=numbers,
        weight=weight,
        select=select,
        average_style="mean",
        average_style_error="percentiles",
        name_of_sim=None,
        name_of_sim_save=None,
        ics=False,
        info=info,
        fields_to_load=None,
        nbins=100,
        range=range,
        same_number_points_bin=False,
        method="mean",
        postprocess=None,
        method_error="percentiles",
        error_bar_style="shaded",
        log=True,
        differenceTime=4,
    )
    m = np.logical_not(np.isnan(y))
    if force_non_zero:
        m = np.logical_and(y != 0, m)
    if save_fig:
        print(f"hline at {np.interp(dist_in_kpc, x[m], y[m])}")
        plot_radprof(
            x,
            y,
            err=err,
            save_plot=True,
            filename=f"../figures/test/get_mean_variable_at_position_in_kpc_average_time/{snap.file_name_formatted}_test.pdf",
            vlines=[dist_in_kpc],
            hlines=[np.interp(dist_in_kpc, x[m], y[m])],
            label=f"({dist_in_kpc})({np.interp(dist_in_kpc, x[m], y[m])})",
        )
    return np.interp(dist_in_kpc, x[m], y[m])


def get_feature_extraction_fouriersmooth(
    INFO,
    ProjBox,
    axis,
    center,
    numthreads,
    resolution,
    smooth_pixel_number,
    snap,
    variable,
    weight,
):
    subtract_3d = False
    print(f"smooth_pixel_number: {smooth_pixel_number}")
    import FourierUtility as FU

    print(f"ProjBox: {ProjBox}")
    print(f"resolution: {resolution}")
    grid = getAgrid(
        snap,
        variable,
        res=resolution,
        ProjBox=ProjBox,
        center=center,
        numthreads=numthreads,
    )
    Fig.get_mesh_data(
        snap,
        axis,
        box_3d,
        variable,
        resolution_3d,
        center,
        plot_type="isolate_features",
        contour=False,
    )
    scales_to_remove = [[1 / 1, np.inf]]
    scales_to_remove = [[1 / 2, np.inf]]
    print(f"scales_to_remove: {scales_to_remove}")
    grid_removed_scales = FU.remove_scales(np.copy(grid), ProjBox, scales_to_remove)
    dist = aux.get_DistanceToCenterInKpc(snap, INFO=INFO)
    dist = getAgrid(
        snap,
        dist,
        res=resolution,
        ProjBox=ProjBox,
        center=center,
        numthreads=numthreads,
    )
    log = True
    x_range = [None, None]
    if log:
        x_range[0] = dist[dist > 0].min()
    else:
        x_range[0] = dist.min()
    x_range[1] = dist.max()
    x, y, err = do_Radprof(
        dist.flatten(),
        grid_removed_scales.flatten(),
        weight=weight,
        log=log,
        NBins=60,
        typeCalc="mean",
        return_errbar=True,
        range=x_range,
    )
    y = interpolate_nans(x, y)
    smooth_variable = np.interp(dist, x, y)
    save_1PlotPlain(
        [x], [y], name="../figures/IsolateFeatures/test.pdf", logX=True, alpha=0.6
    )
    if subtract_3d:
        grid = grid - smooth_variable
        data = do_Projection(
            snap,
            grid,
            None,
            resolution,
            ProjBox,
            center,
            numthreads,
            axis,
            inputGrids=True,
            sumAlongAxis=True,
            INFO=INFO,
        )
    else:
        grid = do_Projection(
            snap,
            grid,
            None,
            resolution,
            ProjBox,
            center,
            numthreads,
            axis,
            inputGrids=True,
            sumAlongAxis=True,
            INFO=INFO,
        )
        smooth_variable = do_Projection(
            snap,
            smooth_variable,
            None,
            resolution,
            ProjBox,
            center,
            numthreads,
            axis,
            inputGrids=True,
            sumAlongAxis=True,
            INFO=INFO,
        )
        for d, n in zip([grid, smooth_variable], ["grid", "smooth_variable"]):
            save_pcolormesh(
                d,
                ProjBox,
                axis,
                resolution[axis[0]],
                resolution[axis[1]],
                cmap="RdBu_r",
                log="symlog",
                vmin=None,
                vmax=None,
                arepo_positions=False,
                center=None,
                variable_name="name",
                variable_name_units="units",
                filename=f"../figures/maps/{n}.pdf",
            )
        data = grid - smooth_variable
    return data


def get_3d_feature_extraction_radialprofsmooth(
    INFO,
    ProjBox,
    axis,
    center,
    numthreads,
    resolution,
    save_extra_plots,
    smooth,
    snap,
    variable,
    weight,
):
    dist = aux.get_DistanceToCenterInKpc(snap, INFO=INFO)
    log = False
    x_range = [None, None]
    if log:
        x_range[0] = dist[dist > 0].min()
    else:
        x_range[0] = dist.min()
    x_range[1] = dist.max()
    x, y, err = do_Radprof(
        dist,
        variable,
        weight=weight,
        log=log,
        NBins=250,
        typeCalc="mean",
        return_errbar=True,
        range=x_range,
    )
    if smooth:
        y = interpolate_nans(x, y)
        x_smooth, y_smooth = SmoothFunction(
            np.copy(x),
            np.copy(y),
            Nsmooth=70,
            log=log,
            Range=None,
            Data=dist,
            Min=None,
            Max=None,
            limits=None,
            order=4,
        )
    if save_extra_plots:
        save_1PlotPlain(
            [x, x_smooth],
            [y, y_smooth],
            name="../figures/IsolateFeatures/test.pdf",
            logX=True,
            alpha=0.6,
        )
    if smooth:
        x = x_smooth
        y = y_smooth
    # nans = np.isnan(y)
    # y[nans] = np.interp(x[nans], x[~nans], y[~nans])
    y = interpolate_nans(x, y)
    smooth_variable = np.interp(dist, x, y)
    variable = do_Projection(
        snap,
        variable,
        None,
        resolution,
        ProjBox,
        center,
        numthreads,
        axis,
        sumAlongAxis=True,
        INFO=INFO,
    )
    smooth_variable = do_Projection(
        snap,
        smooth_variable,
        None,
        resolution,
        ProjBox,
        center,
        numthreads,
        axis,
        sumAlongAxis=True,
        INFO=INFO,
    )
    data = (variable - smooth_variable) / smooth_variable
    return data


def get_2d_feature_extraction(
    INFO,
    ProjBox,
    axis,
    center,
    numthreads,
    resolution,
    save_extra_plots,
    snap,
    variable,
    weight,
    NBins,
    relative=True,
):
    variable = do_Projection(
        snap,
        variable,
        None,
        resolution,
        ProjBox,
        center,
        numthreads,
        axis,
        sumAlongAxis=True,
        INFO=INFO,
    )
    dist = aux.get_2d_radial_profile(variable.shape, ProjBox[axis[0]], ProjBox[axis[1]])
    log = False
    x_range = [None, None]
    if log:
        x_range[0] = dist[dist > 0].min()
    else:
        x_range[0] = dist.min()
    x_range[0] = 1  # ignore inner region
    x_range[1] = dist.max()
    x, y, err = do_Radprof(
        dist.flatten(),
        variable.flatten(),
        weight=weight,
        log=log,
        NBins=NBins,
        typeCalc="median",
        return_errbar=True,
        range=x_range,
    )
    if save_extra_plots:
        save_1PlotPlain(
            [x],
            [y],
            Label=["data", "fit"],
            name="../figures/IsolateFeatures/fit.pdf",
            logX=True,
            alpha=0.6,
            TickStyle="-x",
        )
    y = interpolate_nans(x, y)
    smooth_variable = np.interp(dist, x, y)
    if relative:
        data = (variable - smooth_variable) / smooth_variable
    else:
        data = variable - smooth_variable
    return data


def get_single_ellipse_fit(points):
    x = points[:, 0]
    y = points[:, 1]

    x = x[:, None]
    y = y[:, None]

    D = np.hstack([x * x, x * y, y * y, x, y, np.ones(x.shape)])
    S = np.dot(D.T, D)
    C = np.zeros([6, 6])
    C[0, 2] = C[2, 0] = 2
    C[1, 1] = -1
    E, V = np.linalg.eig(np.dot(np.linalg.inv(S), C))

    # if method == 1:
    #     # does not work!
    #     n = np.argmax(np.abs(E))
    # else:
    n = np.argmax(E)
    a = V[:, n]

    # -------------------Fit ellipse-------------------
    b, c, d, f, g, a = a[1] / 2.0, a[2], a[3] / 2.0, a[4] / 2.0, a[5], a[0]
    num = b * b - a * c
    if num == 0:
        return None, None
    cx = (c * d - b * f) / num
    cy = (a * f - b * d) / num

    angle = 0.5 * np.arctan(2 * b / (a - c)) * 180 / np.pi
    up = 2 * (a * f * f + c * d * d + g * b * b - 2 * b * d * f - a * c * g)
    down1 = (b * b - a * c) * (
        (c - a) * np.sqrt(1 + 4 * b * b / ((a - c) * (a - c))) - (c + a)
    )
    down2 = (b * b - a * c) * (
        (a - c) * np.sqrt(1 + 4 * b * b / ((a - c) * (a - c))) - (c + a)
    )
    a = np.sqrt(abs(up / down1))
    b = np.sqrt(abs(up / down2))

    # ---------------------Get path---------------------
    ell = Ellipse((cx, cy), a * 2.0, b * 2.0, angle)
    ell_coord = ell.get_verts()
    params = [cx, cy, a, b, angle]

    return params, ell_coord


def get_vol_ellipsoid(a, b, c):
    return a * b * c * 4 / 3 * np.pi


def is_in_ellipse(x, y, cx, cy, a, b, angle):
    g_ell_width = a * 2  # a if a > b else b
    g_ell_height = b * 2  # a if a < b else b
    cos_angle = np.cos(np.radians(180.0 - angle))
    sin_angle = np.sin(np.radians(180.0 - angle))

    xc = x - cx
    yc = y - cy

    xct = xc * cos_angle - yc * sin_angle
    yct = xc * sin_angle + yc * cos_angle

    rad_cc = (xct ** 2 / (g_ell_width / 2.0) ** 2) + (
        yct ** 2 / (g_ell_height / 2.0) ** 2
    )
    return rad_cc <= 1.0


def get_isolated_ellipses(snap, info=None):
    print(f"info in get_isolated_ellipses: {info}")
    variable = aux.checkIfInDictionary(
        "variable", info, "XrayEmissivityABOVE0p2keVBELOW10keV"
    )
    min_vol = (
        aux.checkIfInDictionary("minvol", info, 40)
        * Param.KiloParsec_in_cm
        / snap.UnitLength_in_cm
    )
    bin_size = (
        aux.checkIfInDictionary("binsize", info, 10)
        * Param.KiloParsec_in_cm
        / snap.UnitLength_in_cm
    )
    res_depth = aux.checkIfInDictionary("resdepth", info, 300)
    res = aux.checkIfInDictionary("res", info, 300)
    depth = (
        aux.checkIfInDictionary("depth", info, 150)
        * Param.KiloParsec_in_cm
        / snap.UnitLength_in_cm
    )
    axis = aux.checkIfInDictionary("axis", info, [2, 0])
    contour_level = aux.checkIfInDictionary("contourlevel", info, -1.5e-1)
    ProjBox = (
        aux.checkIfInDictionary("ProjBox", info, 150)
        * Param.KiloParsec_in_cm
        / snap.UnitLength_in_cm
    )
    resolution = int(
        ProjBox / (150 * Param.KiloParsec_in_cm / snap.UnitLength_in_cm) * res
    )
    smoothing_bins = int(ProjBox / bin_size)
    axis_depth = 3 - sum(axis)
    box_3d = [ProjBox for x in range(3)]
    resolution_3d = [resolution for x in range(3)]
    resolution_3d[axis_depth] = res_depth
    box_3d[axis_depth] = depth
    # contour_levels = [-2.5e-1, -1.5e-1, -0.5e-1]
    # ellipse_fit_level = -2
    contour_levels = [contour_level]
    ellipse_fit_level = -1
    center = snap.center
    data, contour_data = get_mesh_data(
        snap,
        axis,
        box_3d,
        variable,
        resolution_3d,
        center,
        plot_type="isolate_features",
        contour=True,
        method="2dsubtract",
        save_extra_plots=False,
        smoothing_bins=smoothing_bins,
        smooth=True,
        numthreads=1,
    )
    contour_image = plt.contour(
        contour_data["X"],
        contour_data["Y"],
        contour_data["Z"],
        contour_levels,
        colors="black",
        linewidths=1,
        alpha=1,
    )
    contour_fit_params, contour_fit_coords, contour_fit_volume = get_all_ellipse_fit(
        contour_image.allsegs[ellipse_fit_level], min_volume=min_vol,
    )
    max_distance = aux.checkIfInDictionary("maxDistanceBubble", info, ProjBox)
    print(f"contour_fit_params initially: {contour_fit_params}")
    if max_distance is not None:
        CONT_PARAMS = contour_fit_params[:]
        CONT_COORDS = contour_fit_coords[:]
        CONT_VOLUME = contour_fit_volume[:]
        contour_fit_params = []
        contour_fit_coords = []
        contour_fit_volume = []
        for params, coords, v in zip(CONT_PARAMS, CONT_COORDS, CONT_VOLUME):
            cx, cy, a, b, angle = params
            cx = cx - center[axis[0]]
            cy = cy - center[axis[1]]
            dist = math.sqrt(cx ** 2 + cy ** 2)
            if dist > max_distance:
                continue
            contour_fit_params.append(params)
            contour_fit_coords.append(coords)
            contour_fit_volume.append(v)
    print(f"contour_fit_params after: {contour_fit_params}")
    return contour_fit_params, contour_fit_coords, contour_fit_volume, axis, center


def get_all_ellipse_fit(
    points_list, ignore_inner_ellipses=True, min_volume=None, verbose=0
):
    import numpy.ma as ma

    params, coords = [], []
    # p = c = []
    for points in points_list:
        try:
            p, c = get_single_ellipse_fit(points)
            if verbose:
                print(f"single fit: {p}")
            if p is not None and not any(np.isnan(p)):
                params.append(p)
                coords.append(c)
        except:
            if verbose:
                print(f"exception single eclipse points: {len(points)}")
    params = np.array(params)
    coords = np.array(coords)
    volume = []
    if verbose:
        print(f"final params: {params}")
    for p in params:
        cx, cy, a, b, angle = p
        V = get_vol_ellipsoid(a, a if a < b else b, b)
        volume.append(V)
    volume = np.array(volume)
    if min_volume is not None:
        mask = volume > min_volume
        volume = volume[mask]
        params = params[mask]
        coords = coords[mask]
    if ignore_inner_ellipses:
        not_nan = np.logical_not(np.isnan(volume))
        volume = volume[not_nan]
        params = params[not_nan]
        coords = coords[not_nan]
        args = np.argsort(volume)
        keep = {}
        index = 0
        for index, (p, c, v) in enumerate(
            zip(params[args], coords[args], volume[args])
        ):
            # print(f"p: {p}, c: {c}, v: {v}")
            cx, cy, a, b, angle = p
            found = False
            found_larger_1 = False
            found_larger_2 = False
            for i, (kp, kc, kv) in keep.copy().items():
                kcx, kcy, ka, kb, kangle = kp
                if verbose:
                    print(
                        f"found: kcx, kcy, ka, kb, kangle: {kcx, kcy, ka, kb, kangle}"
                    )
                    print(f"found: cx, cy, a, b, angle: {cx, cy, a, b, angle}")
                    print(
                        f" is_in_ellipse(kcx, kcy, cx, cy, a, b, angle) : {is_in_ellipse(kcx, kcy, cx, cy, a, b, angle)}"
                    )
                    print(
                        f" is_in_ellipse(cx, cy, kcx, kcy, ka, kb, kangle): {is_in_ellipse(cx, cy, kcx, kcy, ka, kb, kangle)}"
                    )
                    print(f" kv > v : {v > kv}")
                    print(f" v, {v} kv {kv}")
                    print(f"  v <= kv : {v <= kv}")
                if is_in_ellipse(kcx, kcy, cx, cy, a, b, angle):
                    if v > kv:
                        found_larger_2 = True
                        keep.pop(i)
                        keep[index] = [p, c, v]
                if is_in_ellipse(cx, cy, kcx, kcy, ka, kb, kangle) and kv > v:
                    found = True
                    break
                if (
                    is_in_ellipse(kcx, kcy, cx, cy, a, b, angle)
                    or is_in_ellipse(cx, cy, kcx, kcy, ka, kb, kangle)
                ) and v <= kv:
                    found = True
                    break
            if verbose:
                print(
                    f"found: {found}, found_larger_1: {found_larger_1}, found_larger_2: {found_larger_2}"
                )
                print(f"keep: {[x[0] for x in  keep.values()]}")
            if found_larger_1 or found_larger_2:
                pass
                # keep.pop(i)
                # keep[index] = [p, c, v]
            elif not found:
                keep[index] = [p, c, v]
        print(f"looked through {index+1} ellipse candidates")
        if keep:
            params, coords, volume = np.array(list(keep.values())).T
        else:
            params, coords, volume = [], [], []
    return params, coords, volume


if __name__ == "__main__":
    fontsize = 8
    import FigureMove as Fig
    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid1 import make_axes_locatable

    snap = Fig.quickImport(
        800,
        folder="../TestSim/CoolRuns/Perseus/R6/13_cooling_jet_CCphases_mhd_RQEff1M1_CCEff1M3_BHhsml2kpc_NgbDif4_XB0p001_Lmin30/output/",
        useTime=True,
    )
    VARIABLE = [
        "XrayEmissivityABOVE0p2keVBELOW10keV",
        "XrayEmissivityABOVE0p2keVBELOW10keV",
    ]
    plot_points = False
    CONTOUR = [True, False]
    PLOT_TYPE = ["isolate_features", "integral"]
    ProjBox = 150
    res_default = 100  # 300
    resolution = int(ProjBox / 150 * res_default)
    vmin = None
    vmax = None
    center = snap.center
    min_vol = 40
    smoothing_bins = int(ProjBox / 10)
    axis = [2, 0]
    contour_levels = [-5e-1, -4e-1, -3e-1, -2e-1, -1.5e-1, -1e-1]
    contour_levels = [-2.5e-1, -1.5e-1, -0.5e-1]
    ellipse_fit_level = -2
    box_3d = [ProjBox for x in range(3)]
    resolution_3d = [resolution for x in range(3)]
    axis_depth = 3 - sum(axis)
    resolution_3d[axis_depth] = res_default
    box_3d[axis_depth] = 150
    scale_bar = 30
    LOG = ["symlog", "log"]
    CMAP = ["bwr", "inferno"]
    DATA = []
    CONTOUR_DATA = []
    for i, (plot_type, variable, contour) in enumerate(
        zip(PLOT_TYPE, VARIABLE, CONTOUR)
    ):
        data, contour_data = Fig.get_mesh_data(
            snap,
            axis,
            box_3d,
            variable,
            resolution_3d,
            center,
            plot_type=plot_type,
            contour=contour,
            method="2dsubtract",
            save_extra_plots=False,
            smoothing_bins=smoothing_bins,
            reimportStrVar=False,
            smooth=True,
        )
        DATA.append(data)
        CONTOUR_DATA.append(contour_data)
    fig, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    CONTOUR_IMAGE = []
    for i, (axes, data, contour_data, log, cmap) in enumerate(
        zip(axs, DATA, CONTOUR_DATA, LOG, CMAP)
    ):
        divider = make_axes_locatable(axes)
        axs_cb = {}
        axs_cb[i] = divider.append_axes("right", size="5%", pad=0.05)
        #    fig = plt.figure()
        #    ax = plt.axes([0.1, 0.1, 0.74, 0.74])
        #    ax_cb = plt.axes([0.85, 0.1, 0.04, 0.74])
        # extent = Fig.get_Extent([0, 0, 0], axis, box_3d)
        contour_image, extent = Fig.do_pcolormesh_plot(
            data,
            box_3d,
            axis,
            resolution,
            center,
            axes=axes,
            axes_colorbar=axs_cb[i],
            contour_data=contour_data,
            contour_levels=contour_levels,
            scale_bar=scale_bar,
            vmin=vmin,
            vmax=vmax,
            log=log,
            fontsize=6,
            variable_name="\\mathrm{feature}",
            variable_name_units="",
            cmap=cmap,
            alpha_contour=0.7,
        )
        CONTOUR_IMAGE.append(contour_image)
        contour_image = CONTOUR_IMAGE[0]
        if contour_image is not None:
            total_vol = 0
            (
                contour_fit_params,
                contour_fit_coords,
                contour_fit_volume,
            ) = get_all_ellipse_fit(
                contour_image.allsegs[ellipse_fit_level], min_volume=min_vol,
            )
            print(f"contour_fit_volume: {contour_fit_volume}")
            colors_start, colors_finish = Fig.get_colors_examples_gradients()
            colors = aux.linear_gradient(
                colors_start[0], colors_finish[0], n=len(contour_fit_coords)
            )
            if plot_points:
                xs = np.random.uniform(extent[0], extent[1], [10000])
                ys = np.random.uniform(extent[2], extent[3], [10000])
                axes.scatter(xs, ys, c="grey", s=2, linewidths=0.3, alpha=0.4)
            for params, coords, color in zip(
                contour_fit_params, contour_fit_coords, colors_finish[2:]
            ):
                if len(coords) > 0:
                    cx, cy, a, b, angle = params
                    axes.scatter(
                        *coords.T, linewidth=1, s=0.2, color=color
                    )  # "gainsboro")
                    # axes.scatter(*(cx, cy), linewidth=1, s=0.2, color=color) #"gainsboro")
                    if plot_points:
                        mask = is_in_ellipse(xs, ys, cx, cy, a, b, angle)
                        colors_array = np.array(["black"] * len(xs))
                        colors_array[mask] = "green"
                        axes.scatter(xs[mask], ys[mask], c=color, s=2, linewidths=0.3)
                    V = round(a * b ** 2 * 4 / 3 * np.pi)
                    if V > min_vol and i == 0:
                        total_vol += V
                        text = f"a: {int(a)}, b: {int(b)}, V={V}"
                        print(f"text: {text}")
                        axes.text(cx, cy, text, color="gainsboro")
            if i == 0:
                Fig._text_time_folder(
                    axes, text=r"total vol: %i" % total_vol, text_color="gainsboro"
                )
    filename = []
    for t in [resolution_3d, box_3d]:
        filename.append("%s" % aux.convert_params_to_string(t, efficientNumber=False))
    filename = "_".join(filename)
    filename = "test"
    filename = f"../figures/ellipse_fit/{filename}.pdf"
    Fig.savePlot(fig, filename=filename, dpi=300)
