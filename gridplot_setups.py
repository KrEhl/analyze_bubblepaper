import Param
import auxiliary_functions as aux
import feature_extraction
import numpy as np
import pandas as pd


def get_grid_variable(i, setNum, SetupNam, variableLabel, INFO, MAXSEARCHRADIUS):
    setup_style = "default"
    if 0 == setNum:
        SetupNam += ["TMyr"]
        variableLabel += ["\\mathrm{t}\\ (\\mathrm{MS,no bubble})"]
        INFO["%i-_filename" % i] = "TimeInMyr"
        INFO["%i-_header" % i] = ("numberSN", "TMyr")
        INFO["%i-_function" % i] = aux.get_Time
        INFO["%i-_functionParameters" % i] = (
            "snap",
            False,
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 0.01 == setNum:
        SetupNam += ["TMyrLog"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\mathrm{t}\\ [\\mathrm{Myr}]"]
        INFO["%i-_function" % i] = aux.get_blackholes_logfile
        INFO["%i-_unit_time_blackholes_log" % i] = "Myr"
        INFO["%i-_functionParameters" % i] = (
            "folder",
            ["time"],
            False,
            "INFO",
        )
        setup_style = "logfile"
    if 0.001 == setNum:
        SetupNam += ["TimeEnergyLog"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\mathrm{t}\\ [\\mathrm{Myr}]"]
        INFO["%i-_function" % i] = aux.get_energy_logfile
        INFO["%i-_UnitTimeEnergyLog" % i] = "Myr"
        INFO["%i-_functionParameters" % i] = (
            "folder",
            ["time"],
            False,
            "INFO",
        )
        setup_style = "logfile"
    if 0.0001 == setNum:
        SetupNam += ["TimeEnergyLog"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\mathrm{t}\\ [\\mathrm{Myr}]"]
        INFO["%i-_function" % i] = aux.get_jet_accelerated_cr_energy_logfile
        INFO["%i-_UnitTimeEnergyLog" % i] = "Myr"
        INFO["%i-_functionParameters" % i] = (
            "folder",
            ["time"],
            False,
            "INFO",
        )
        setup_style = "logfile"
    if 1 == setNum:
        SetupNam += ["JDistHalHal0p99InKpc"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["d_\\mathrm{jet,half}"]
        INFO["%i-_filename" % i] = "JetDistHalHalInKpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "Percentile",
            "VariableWRT",
            "DistHalfHalfUD",
        )
        INFO["%i-_JetDistanceHalfHalfPercentile" % i] = 0.99
        INFO["%i-_VariableJetDistanceHalfHalf" % i] = "JetMassFraction"
        INFO["%i-_function" % i] = aux.get_JetDistanceHalfHalf
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:JetDistanceHalfHalfPercentile",
            "INFO:VariableJetDistanceHalfHalf",
        )
    if 2 == setNum:
        SetupNam += ["JDistTracMaxInKpc"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["d_\\mathrm{jet}"]
        INFO["%i-_filename" % i] = "JetDistTracMaxInKpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BubbleThresh",
            "DistTraxMaxUD",
        )
        INFO["%i-_BubbleThresh" % i] = 1e-3
        INFO["%i-_function" % i] = aux.get_JetDistanceTracMax
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN", "INFO:BubbleThresh")
    if 3 == setNum:
        SetupNam += ["BowDistMachMaxInKpc"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["d_\\mathrm{shock}"]
        INFO["%i-_filename" % i] = "JetDistMachMaxInKpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BubbleThresh",
            "DistTraxMaxUD",
        )
        INFO["%i-_BubbleThresh" % i] = 1e-3
        INFO["%i-_function" % i] = aux.get_JetBowShockDistanceMachMax
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN", "INFO:BubbleThresh")
    if 4 == setNum:
        SetupNam += ["JetEnergy"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_\\mathrm{jet}"]
        INFO["%i-_filename" % i] = "JetEnergyInErg"
        INFO["%i-_header" % i] = ("JetEnergy",)
        INFO["%i-_function" % i] = aux.get_JetEnergy
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = tuple()
        INFO["%i-_needReloadSN" % i] = True
        INFO["%i-_FieldsToLoad" % i] = "ONLYHEADER"
    if 4.1 == setNum:
        SetupNam += ["BHCumKinEgyInjectedInJet"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_\\mathrm{inj,kin}"]
        INFO["%i-_filename" % i] = "BHCumKinEgyInjectedInJetInErg"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHCumKinEgyInjectedInJet",
        )
        INFO["%i-_function" % i] = aux.get_BHCumKinEgyInjectedInJet
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_FieldsToLoad" % i] = ["bhjk"]
    if 4.101 == setNum:
        SetupNam += ["BHCumKinEgyInjectedInJetNegative"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["-E_\\mathrm{inj,kin}"]
        INFO["%i-_filename" % i] = "BHCumKinEgyInjectedInJetNegative"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHCumKinEgyInjectedInJetNegative",
        )
        INFO["%i-_function" % i] = aux.get_BHCumKinEgyInjectedInJetNegative
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_FieldsToLoad" % i] = ["bhjk"]
    if 4.11 == setNum:
        SetupNam += ["KineticEnergyInJet"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_\\mathrm{jet,kin}"]
        INFO["%i-_filename" % i] = "KineticEnergyInJet"
        INFO["%i-_header" % i] = (
            "numberSN",
            "LocationLobeThresh",
            "KineticEnergyInJet",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "KineticEnergyLob",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:LocationLobeThresh",
        )
        INFO["%i-_LocationLobeThresh" % i] = 1e-3
    if 4.1111 == setNum:
        SetupNam += ["ThEnergyEnergyLog"]
        variableLabel += ["E_\\mathrm{th,inj,log}"]
        INFO["%i-_function" % i] = aux.get_energy_logfile
        INFO["%i-_functionParameters" % i] = (
            "folder",
            ["egyinjtot"],
            False,
            None,
        )
        setup_style = "logfile"
    if 4.12 == setNum:
        SetupNam += ["MaxAlfvenVelocity"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["v_\\mathrm{A,max}"]
        INFO["%i-_filename" % i] = "MaxAlfvenVelocity"
        INFO["%i-_header" % i] = (
            "numberSN",
            "MaxAlfvenVelocity",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "AlfvenVelocity",
            "max",
            "INFO",
        )
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_unitsV" % i] = "kms"
    if 4.13 == setNum:
        SetupNam += ["MaxSoundSpeed"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["v_\\mathrm{s,max}"]
        INFO["%i-_filename" % i] = "MaxSoundSpeed"
        INFO["%i-_header" % i] = (
            "numberSN",
            "MaxSoundSpeed",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "SoundSpeed",
            "max",
            "INFO",
        )
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_unitsV" % i] = "kms"
    if 4.2 == setNum:
        SetupNam += ["BHCumMagnEgyInjectedInJet"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_{\\mathrm{inj},B}"]
        INFO["%i-_filename" % i] = "BHCumMagnEgyInjectedInJet"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHCumMagnEgyInjectedInJet",
        )
        INFO["%i-_function" % i] = aux.get_BHCumMagnEgyInjectedInJet
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_FieldsToLoad" % i] = ["bhjm"]
    if 4.21 == setNum:
        SetupNam += ["MagneticEnergyInJet"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_{\\mathrm{jet},B}"]
        INFO["%i-_filename" % i] = "MagneticEnergyInJet"
        INFO["%i-_header" % i] = (
            "numberSN",
            "LocationLobeThresh",
            "MagneticEnergyInJet",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MagneticEnergyLob",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:LocationLobeThresh",
        )
        INFO["%i-_LocationLobeThresh" % i] = 1e-3
    if 4.3 == setNum:
        SetupNam += ["BHCumCREgyInjectedInJet"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_\\mathrm{inj,cr}"]
        INFO["%i-_filename" % i] = "BHCumCREgyInjectedInJet"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHCumCREgyInjectedInJet",
        )
        INFO["%i-_function" % i] = aux.get_BHCumCREgyInjectedInJet
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_FieldsToLoad" % i] = ["bhjc"]
    if 4.31 == setNum:
        SetupNam += ["CosmicRayEnergyInJet"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_{\\mathrm{jet, cr}}"]
        INFO["%i-_filename" % i] = "CosmicRayEnergyInJet"
        INFO["%i-_header" % i] = (
            "numberSN",
            "LocationLobeThresh",
            "CosmicRayEnergyInJet",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "CosmicRayEnergyLob",
            "sum",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:LocationLobeThresh",
        )
        INFO["%i-_LocationLobeThresh" % i] = 1e-3
    if 4.32 == setNum:
        SetupNam += ["PotCREnergyInJet"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_{\\mathrm{jet, cr, pot}}"]
        INFO["%i-_filename" % i] = "PotCREnergyInJet"
        INFO["%i-_header" % i] = (
            "numberSN",
            "LocationLobeThresh",
            "PotCREnergyInJet",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "PassiveScalarCRAccelerationEnergyLob",
            "sumNonNegative",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:LocationLobeThresh",
        )
        INFO["%i-_LocationLobeThresh" % i] = 1e-3
    if 4.321 == setNum:
        SetupNam += ["PotCREnergyInJetNegative"]
        variableLabel += ["-E_{\\mathrm{jet, cr, pot}}"]
        INFO["%i-_filename" % i] = "PotCREnergyInJetNegative"
        INFO["%i-_header" % i] = (
            "numberSN",
            "LocationLobeThresh",
            "PotCREnergyInJetNegative",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "PassiveScalarCRAccelerationEnergyLob",
            "sumNegativeAbs",
        )
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:LocationLobeThresh",
        )
        INFO["%i-_LocationLobeThresh" % i] = 1e-3
    if 4.322 == setNum:
        SetupNam += ["PotCREnergyInICM"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_{\\mathrm{ICM, cr, pot}}"]
        INFO["%i-_filename" % i] = "PotCREnergyInICM"
        INFO["%i-_header" % i] = (
            "numberSN",
            "LocationLobeThresh",
            "PotCREnergyInICM",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "PassiveScalarCRAccelerationEnergyICM",
            "sumNonNegative",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:LocationLobeThresh",
        )
        INFO["%i-_LocationLobeThresh" % i] = 1e-3
    if 4.3221 == setNum:
        SetupNam += ["PotCREnergyInICMNegative"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["-E_{\\mathrm{ICM, cr, pot}}"]
        INFO["%i-_filename" % i] = "PotCREnergyInICMNegative"
        INFO["%i-_header" % i] = (
            "numberSN",
            "LocationLobeThresh",
            "PotCREnergyInICMNegative",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "PassiveScalarCRAccelerationEnergyICM",
            "sumNegativeAbs",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:LocationLobeThresh",
        )
        INFO["%i-_LocationLobeThresh" % i] = 1e-3
    if 4.33 == setNum:
        SetupNam += ["CRJetAccEnergy"]
        variableLabel += ["E_\\mathrm{acc,cr,log}"]
        INFO["%i-_function" % i] = aux.get_jet_accelerated_cr_energy_logfile
        INFO["%i-_functionParameters" % i] = (
            "folder",
            ["CumBlackHoleJetCrEnergyAccelerated"],
            False,
            None,
        )
        setup_style = "logfile"
    if 4.4 == setNum:
        SetupNam += ["BHCumThEgyInjectedInJet"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_\\mathrm{inj,th}"]
        INFO["%i-_filename" % i] = "BHCumThEgyInjectedInJet"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHCumThEgyInjectedInJet",
        )
        INFO["%i-_function" % i] = aux.get_BHCumThEgyInjectedInJet
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_FieldsToLoad" % i] = ["bhjt"]
    if 4.401 == setNum:
        SetupNam += ["BHCumThEgyInjectedInJetNegative"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["-E_\\mathrm{inj,th}"]
        INFO["%i-_filename" % i] = "BHCumThEgyInjectedInJetNegative"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHCumThEgyInjectedInJetNegative",
        )
        INFO["%i-_function" % i] = aux.get_BHCumThEgyInjectedInJetNegative
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_FieldsToLoad" % i] = ["bhjt"]
    if 4.41 == setNum:
        SetupNam += ["ThermalEnergyInJet"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_\\mathrm{jet,th}"]
        INFO["%i-_filename" % i] = "ThermalEnergyInJet"
        INFO["%i-_header" % i] = (
            "numberSN",
            "LocationLobeThresh",
            "ThermalEnergyInJet",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "ThermalEnergyLob",
            "sum",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:LocationLobeThresh",
        )
        INFO["%i-_LocationLobeThresh" % i] = 1e-3
    if 4.5 == setNum:
        SetupNam += ["JetBufferEnergy"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_\\mathrm{buff}"]
        INFO["%i-_filename" % i] = "JetBufferEnergy"
        INFO["%i-_header" % i] = (
            "numberSN",
            "JetBufferEnergyInErg",
        )
        INFO["%i-_function" % i] = aux.get_JetBufferEnergy
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_FieldsToLoad" % i] = ["bhbe"]
    if 4.51 == setNum:
        SetupNam += ["JetBufferEnergyNegative"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["-E_\\mathrm{buff}"]
        INFO["%i-_filename" % i] = "JetBufferEnergyNegative"
        INFO["%i-_header" % i] = (
            "numberSN",
            "JetBufferEnergyNegativeInErg",
        )
        INFO["%i-_function" % i] = aux.get_JetBufferEnergyNegative
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_FieldsToLoad" % i] = ["bhbe"]
    if 4.61 == setNum:
        SetupNam += ["BHCumEgyInjectedRM"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_\\mathrm{RM}"]
        INFO["%i-_filename" % i] = "BHCumEgyInjectedRM"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHCumEgyInjectedRMInErg",
        )
        INFO["%i-_function" % i] = aux.get_BHCumEgyInjectedRM
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_FieldsToLoad" % i] = ["bcer"]
    if 4.62 == setNum:
        SetupNam += ["BHCumEgyInjectedQM"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["E_\\mathrm{QM}"]
        INFO["%i-_filename" % i] = "BHCumEgyInjectedQM"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHCumEgyInjectedQMInErg",
        )
        INFO["%i-_function" % i] = aux.get_BHCumEgyInjectedQM
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_FieldsToLoad" % i] = ["bceq"]
    if 5.1 == setNum:
        SetupNam += ["CumStellarMassFormedSnap"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["M_\\mathrm{star,cum}"]
        INFO["%i-_filename" % i] = "CumStellarMassFormedSnap"
        INFO["%i-_header" % i] = (
            "numberSN",
            "CumStellarMassFormed",
        )
        INFO["%i-_function" % i] = aux.get_CumStellarMassFormedSnap
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 6 == setNum:
        SetupNam += ["CumStellarMassFormedSnap"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["M_\\mathrm{star,cum}"]
        INFO["%i-_filename" % i] = "CumStellarMassFormedSnap"
        INFO["%i-_header" % i] = (
            "numberSN",
            "CumStellarMassFormed",
        )
        INFO["%i-_function" % i] = aux.get_CumStellarMassFormedSnap
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 6.1 == setNum:
        SetupNam += ["ColdMassCC"]
        variableLabel += ["{M}_\\mathrm{cold}"]
        INFO["%i-_filename" % i] = "ColdMassCC"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ColdMassCC",
        )
        INFO["%i-_function" % i] = aux.get_ColdGasMassCC
        INFO["%i-_functionParameters" % i] = ("snap",)
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 6.2 == setNum:
        SetupNam += ["ColdMassRadius1e6K30kpc"]
        variableLabel += ["{M}_\\mathrm{1e6K,30kpc}"]
        INFO["%i-_filename" % i] = "ColdMassWithinRadius"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ThresholdTemperatureInKelvin",
            "ColdGasRadiusInKpc",
            "ColdCentralMass",
        )
        INFO["%i-_function" % i] = aux.get_ColdGasMassWithingRadiusInSM
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ThresholdTemperatureInKelvin",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ThresholdTemperatureInKelvin" % i] = 1e6
        INFO["%i-_ColdGasRadiusInKpc" % i] = 30
    if 6.21 == setNum:
        SetupNam += ["ColdMassRadius1e6K50kpc"]
        variableLabel += ["{M}_\\mathrm{1e6K,50kpc}"]
        INFO["%i-_filename" % i] = "ColdMassWithinRadius"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ThresholdTemperatureInKelvin",
            "ColdGasRadiusInKpc",
            "ColdCentralMass",
        )
        INFO["%i-_function" % i] = aux.get_ColdGasMassWithingRadiusInSM
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ThresholdTemperatureInKelvin",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ThresholdTemperatureInKelvin" % i] = 1e6
        INFO["%i-_ColdGasRadiusInKpc" % i] = 50
    if 6.22 == setNum:
        SetupNam += ["ColdMassRadius5e5K30kpc"]
        variableLabel += ["{M}_\\mathrm{5e5K,30kpc}"]
        INFO["%i-_filename" % i] = "ColdMassWithinRadius"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ThresholdTemperatureInKelvin",
            "ColdGasRadiusInKpc",
            "ColdCentralMass",
        )
        INFO["%i-_function" % i] = aux.get_ColdGasMassWithingRadiusInSM
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ThresholdTemperatureInKelvin",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ThresholdTemperatureInKelvin" % i] = 5e5
        INFO["%i-_ColdGasRadiusInKpc" % i] = 30
    if 6.29999 == setNum:
        # only for testing !
        INFO["%i-_filename" % i] = "ColdMassWithinRadius"
        INFO["%i-_header" % i] = (
            "numberSN",
            "numthreads",
            "ThresholdTemperatureInKelvin",
            "ColdGasRadiusInKpc",
            "ColdCentralMass",
        )
        INFO["%i-_function" % i] = aux.get_ColdGasMassWithingRadiusInSM
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:numthreads",
            "INFO:ThresholdTemperatureInKelvin",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ThresholdTemperatureInKelvin" % i] = 5.1e8
        INFO["%i-_ColdGasRadiusInKpc" % i] = 30
        SetupNam += [
            "ColdMassRadius%.1eK%ikpc"
            % (
                INFO["%i-_ThresholdTemperatureInKelvin" % i],
                INFO["%i-_ColdGasRadiusInKpc" % i],
            )
        ]
        variableLabel += [
            "{M}_\\mathrm{%.1eK,%ikpc}"
            % (
                INFO["%i-_ThresholdTemperatureInKelvin" % i],
                INFO["%i-_ColdGasRadiusInKpc" % i],
            )
        ]
        numthreads = 100
        INFO["%i-_numthreads" % i] = numthreads
    if 6.29998 == setNum:
        # only for testing !
        INFO["%i-_filename" % i] = "ColdMassWithinRadius"
        INFO["%i-_header" % i] = (
            "numberSN",
            "numthreads",
            "ThresholdTemperatureInKelvin",
            "ColdGasRadiusInKpc",
            "ColdCentralMass",
        )
        INFO["%i-_function" % i] = aux.get_ColdGasMassWithingRadiusInSM
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:numthreads",
            "INFO:ThresholdTemperatureInKelvin",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ThresholdTemperatureInKelvin" % i] = 1.5e6
        INFO["%i-_ColdGasRadiusInKpc" % i] = 30
        SetupNam += [
            "ColdMassRadius%.1eK%ikpc"
            % (
                INFO["%i-_ThresholdTemperatureInKelvin" % i],
                INFO["%i-_ColdGasRadiusInKpc" % i],
            )
        ]
        variableLabel += [
            "{M}_\\mathrm{%.1eK,%ikpc}"
            % (
                INFO["%i-_ThresholdTemperatureInKelvin" % i],
                INFO["%i-_ColdGasRadiusInKpc" % i],
            )
        ]
        # numthreads = 1
        INFO["%i-_numthreads" % i] = numthreads
    if 6.23 == setNum:
        SetupNam += ["ColdMassRadius5e5K10kpc"]
        variableLabel += ["{M}_\\mathrm{5e5K,10kpc}"]
        INFO["%i-_filename" % i] = "ColdMassWithinRadius"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ThresholdTemperatureInKelvin",
            "ColdGasRadiusInKpc",
            "ColdCentralMass",
        )
        INFO["%i-_function" % i] = aux.get_ColdGasMassWithingRadiusInSM
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ThresholdTemperatureInKelvin",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ThresholdTemperatureInKelvin" % i] = 5e5
        INFO["%i-_ColdGasRadiusInKpc" % i] = 10
    if 6.24 == setNum:
        SetupNam += ["ColdMassRadius1e5K30kpc"]
        variableLabel += ["{M}_\\mathrm{1e5K,30kpc}"]
        INFO["%i-_filename" % i] = "ColdMassWithinRadius"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ThresholdTemperatureInKelvin",
            "ColdGasRadiusInKpc",
            "ColdCentralMass",
        )
        INFO["%i-_function" % i] = aux.get_ColdGasMassWithingRadiusInSM
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ThresholdTemperatureInKelvin",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ThresholdTemperatureInKelvin" % i] = 1e5
        INFO["%i-_ColdGasRadiusInKpc" % i] = 30
    if 6.25 == setNum:
        SetupNam += ["ColdMassRadius1e4K30kpc"]
        variableLabel += ["{M}_\\mathrm{1e4K,30kpc}"]
        INFO["%i-_filename" % i] = "ColdMassWithinRadius"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ThresholdTemperatureInKelvin",
            "ColdGasRadiusInKpc",
            "ColdCentralMass",
        )
        INFO["%i-_function" % i] = aux.get_ColdGasMassWithingRadiusInSM
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ThresholdTemperatureInKelvin",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ThresholdTemperatureInKelvin" % i] = 1e4
        INFO["%i-_ColdGasRadiusInKpc" % i] = 30
    if 6.244 == setNum:
        SetupNam += ["ColdMassRadius1e5K10kpc"]
        variableLabel += ["{M}_\\mathrm{1e5K,10kpc}"]
        INFO["%i-_filename" % i] = "ColdMassWithinRadius"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ThresholdTemperatureInKelvin",
            "ColdGasRadiusInKpc",
            "ColdCentralMass",
        )
        INFO["%i-_function" % i] = aux.get_ColdGasMassWithingRadiusInSM
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ThresholdTemperatureInKelvin",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ThresholdTemperatureInKelvin" % i] = 1e5
        INFO["%i-_ColdGasRadiusInKpc" % i] = 10
    if 6.255 == setNum:
        SetupNam += ["ColdMassRadius1e4K10kpc"]
        variableLabel += ["{M}_\\mathrm{1e4K,10kpc}"]
        INFO["%i-_filename" % i] = "ColdMassWithinRadius"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ThresholdTemperatureInKelvin",
            "ColdGasRadiusInKpc",
            "ColdCentralMass",
        )
        INFO["%i-_function" % i] = aux.get_ColdGasMassWithingRadiusInSM
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ThresholdTemperatureInKelvin",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ThresholdTemperatureInKelvin" % i] = 1e4
        INFO["%i-_ColdGasRadiusInKpc" % i] = 10
    if 6.3 == setNum:
        SetupNam += ["ColdCentralMassCC"]
        variableLabel += ["{M}_\\mathrm{cold}"]
        INFO["%i-_filename" % i] = "ColdCentralMass"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ColdGasThresholdInKelvin",
            "BlackholeRadius",
            "ColdCentralMass",
        )
        INFO["%i-_function" % i] = aux.get_ColdCentralGasMass
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ColdGasThresholdInKelvin",
            "INFO:BlackholeRadius",
        )
        INFO["%i-_ColdGasThresholdInKelvin" % i] = 3e5
        INFO["%i-_BlackholeRadius" % i] = "Nngb"
        INFO["%i-_NGBMaxSearchRadiusInKpc" % i] = 25
    if 6.4 == setNum:
        SetupNam += ["AccretedMassCC"]
        variableLabel += ["{M}_\\mathrm{acc,cum,CC}"]
        INFO["%i-_filename" % i] = "AccretedMassCC"
        INFO["%i-_header" % i] = (
            "numberSN",
            "AccretedMassCCInSolar",
        )
        INFO["%i-_function" % i] = aux.get_CumAccretedMassColdAccretionInSolarMass
        INFO["%i-_functionParameters" % i] = ("snap",)
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 6.5 == setNum:
        SetupNam += ["SFGasMassTotal"]
        variableLabel += ["M_\\mathrm{SF}"]
        INFO["%i-_filename" % i] = "SFGasMassTotal"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = "StarFormationRate>0"
    if 6.501 == setNum:
        SetupNam += ["SFGasMass10kpc"]
        variableLabel += ["M_\\mathrm{SF,10kpc}"]
        INFO["%i-_filename" % i] = "SFGasMass10kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = "StarFormationRate>0" "AND" "DistanceToCenterInKpc<10"
    if 6.502 == setNum:
        SetupNam += ["SFGasMass50kpc"]
        variableLabel += ["M_\\mathrm{SF,50kpc}"]
        INFO["%i-_filename" % i] = "SFGasMass50kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = "StarFormationRate>0" "AND" "DistanceToCenterInKpc<50"
    if 6.503 == setNum:
        SetupNam += ["SFGasMassBHhsml"]
        variableLabel += ["M_{\\mathrm{SF},\\,r<r_\\mathrm{acc}}"]
        INFO["%i-_filename" % i] = "SFGasMassBHhsml"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "StarFormationRate>0" "AND" "DistanceToCenterInKpc<BHhsml"
        )
        INFO["%i-_UnitsBHhsml" % i] = "kpc"
    if 6.5031 == setNum:
        SetupNam += ["SFGasVolumeKpc3BHhsml"]
        variableLabel += ["V_{\\mathrm{SF},r<r_\\mathrm{hsml}}"]
        INFO["%i-_filename" % i] = "SFGasVolumeKpc3BHhsml"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasVolumeKpc3BHhsml",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "VolumeInKpc3",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "StarFormationRate>0" "AND" "DistanceToCenterInKpc<BHhsml"
        )
        INFO["%i-_UnitsBHhsml" % i] = "kpc"
    if 6.51 == setNum:
        SetupNam += ["ColdGasMass"]
        variableLabel += ["M_{<10\\,\\mathrm{Myr},\\mathrm{No}\\,\\mathrm{SF}}"]
        INFO["%i-_filename" % i] = "ColdGasMass"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = "StarFormationRate==0" "AND" "CoolingTimescaleInMyr<10"
    if 6.511 == setNum:
        SetupNam += ["ColdGasMass10kpc"]
        variableLabel += [
            "M_{t_\\mathrm{cool}<10\\,\\mathrm{Myr},r<10\\,\\mathrm{kpc}}"
        ]
        INFO["%i-_filename" % i] = "ColdGasMass10kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "StarFormationRate==0"
            "AND"
            "CoolingTimescaleInMyr<10"
            "AND"
            "DistanceToCenterInKpc<10"
        )
    if 6.513 == setNum:
        SetupNam += ["ColdGasMassBHhsml"]
        variableLabel += [
            "M_{<10\\,\\mathrm{Myr},\\mathrm{No}\\,\\mathrm{SF},r<r_\\mathrm{acc}}"
        ]
        INFO["%i-_filename" % i] = "ColdGasMassBHhsml"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "StarFormationRate==0"
            "AND"
            "CoolingTimescaleInMyr<10"
            "AND"
            "DistanceToCenterInKpc<BHhsml"
        )
        INFO["%i-_UnitsBHhsml" % i] = "kpc"
    if 6.52 == setNum:
        SetupNam += ["ColdGasMassTCool100"]
        variableLabel += ["M_{10\\,\\mathrm{Myr}<t_\\mathrm{cool}<100\\,\\mathrm{Myr}}"]
        INFO["%i-_filename" % i] = "ColdGasMassTCool100"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "StarFormationRate==0"
            "AND"
            "CoolingTimescaleInMyr<100"
            "AND"
            "CoolingTimescaleInMyr>10"
        )
    if 6.521 == setNum:
        SetupNam += ["ColdGasMassTCool10010kpc"]
        variableLabel += [
            "M_{10\\,\\mathrm{Myr}<t_\\mathrm{cool}<100\\,\\mathrm{Myr},r<\\mathrm{10kpc}}"
        ]
        INFO["%i-_filename" % i] = "ColdGasMassTCool10010kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "StarFormationRate==0"
            "AND"
            "CoolingTimescaleInMyr<100"
            "AND"
            "CoolingTimescaleInMyr>10"
            "AND"
            "DistanceToCenterInKpc<10"
        )
    if 6.523 == setNum:
        SetupNam += ["ColdGasMassTCool100BHhsml"]
        variableLabel += [
            "M_{10\\,\\mathrm{Myr}<t_\\mathrm{cool}<100\\,\\mathrm{Myr},r<r_\\mathrm{hsml}}"
        ]
        INFO["%i-_filename" % i] = "ColdGasMassTCool100BHhsml"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "StarFormationRate==0"
            "AND"
            "CoolingTimescaleInMyr<100"
            "AND"
            "CoolingTimescaleInMyr>10"
            "AND"
            "DistanceToCenterInKpc<BHhsml"
        )
        INFO["%i-_UnitsBHhsml" % i] = "kpc"
    if 6.53 == setNum:
        SetupNam += ["ColdGasMassTP6"]
        variableLabel += ["M_{T<10^6\\,\\mathrm{K}}"]
        INFO["%i-_filename" % i] = "ColdGasMassTP6"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = "StarFormationRate==0" "AND" "Temperature<1e6"
    if 6.533 == setNum:
        SetupNam += ["ColdGasMassTP6BHhsml"]
        variableLabel += ["M_{T<10^6\\,\\mathrm{K},r<r_\\mathrm{hsml}}"]
        INFO["%i-_filename" % i] = "ColdGasMassTP6BHhsml"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "StarFormationRate==0"
            "AND"
            "Temperature<1e6"
            "AND"
            "DistanceToCenterInKpc<BHhsml"
        )
        INFO["%i-_UnitsBHhsml" % i] = "kpc"
    if 6.54 == setNum:
        SetupNam += ["ColdGasMassTP6SF"]
        variableLabel += ["M_{T<10^6\\,\\mathrm{K}}"]
        INFO["%i-_filename" % i] = "ColdGasMassTP6SF"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = "Temperature<1e6"
    if 6.544 == setNum:
        SetupNam += ["ColdGasMassTP6SF10kpc"]
        variableLabel += ["M_{T<10^6\\,\\mathrm{K},r<10\\,\\mathrm{kpc}}"]
        INFO["%i-_filename" % i] = "ColdGasMassTP6SF10kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = "Temperature<1e6" "AND" "DistanceToCenterInKpc<10"
    if 6.545 == setNum:
        SetupNam += ["ColdGasMassTP6SF2kpc"]
        variableLabel += ["M_{T<10^6\\,\\mathrm{K},r<2\\,\\mathrm{kpc}}"]
        INFO["%i-_filename" % i] = "ColdGasMassTP6SF2kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "SFGasMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = "Temperature<1e6" "AND" "DistanceToCenterInKpc<2"
    if 6.61 == setNum:
        SetupNam += ["JetBufferMassSnapInSolar"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["M_\\mathrm{buffer}"]
        INFO["%i-_filename" % i] = "JetBufferMassSnapInSolar"
        INFO["%i-_header" % i] = (
            "numberSN",
            "JetBufferMassSnapInSolar",
        )
        INFO["%i-_function" % i] = aux.get_JetBufferMassInSolar
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 6.71 == setNum:
        SetupNam += ["JetCellMass"]
        variableLabel += ["M_{\\mathrm{jet}}"]
        INFO["%i-_filename" % i] = "JetCellMass"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "JetCellMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = "StarFormationRate==0" "AND" "JetTracer>1e-3"
    if 6.711 == setNum:
        SetupNam += ["JetCellMassBHhsml"]
        variableLabel += ["M_{\\mathrm{jet,r<r_\\mathrm{hsml}}}"]
        INFO["%i-_filename" % i] = "JetCellMassBHhsml"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "JetCellMassInSolarMass",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "MassInSolar",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "StarFormationRate==0"
            "AND"
            "JetTracer>1e-3"
            "AND"
            "DistanceToCenterInKpc<BHhsml"
        )
    if 6.7111 == setNum:
        SetupNam += ["JetCellVolumeKpc3BHhsml"]
        variableLabel += ["V_{\\mathrm{jet,r<r_\\mathrm{hsml}}}"]
        INFO["%i-_filename" % i] = "JetCellVolumeKpc3BHhsml"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "JetCellVolumeKpc3BHhsml",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "VolumeInKpc3",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "StarFormationRate==0"
            "AND"
            "JetTracer>1e-3"
            "AND"
            "DistanceToCenterInKpc<BHhsml"
        )
    if 6.81 == setNum: #total kinetic energy within 200 kpc
        SetupNam += ["TotalKineticEnergyWithin200kpc"]
        variableLabel += ["E_{\\mathrm{kin,r<200\,\mathrm{kpc}}}"]
        INFO["%i-_filename" % i] = "TotalKineticEnergyWithin200kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "TotalKineticEnergyWithin200kpc",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "KineticEnergy",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "DistanceToCenterInKpc<200"
        )
    if 6.811 == setNum: #total kinetic energy within 500 kpc
        SetupNam += ["TotalKineticEnergyWithin500kpc"]
        variableLabel += ["E_{\\mathrm{kin,r<500\,\mathrm{kpc}}}"]
        INFO["%i-_filename" % i] = "TotalKineticEnergyWithin500kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "TotalKineticEnergyWithin500kpc",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "KineticEnergy",
            "sum",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "DistanceToCenterInKpc<500"
        )
    if 6.82 == setNum: #mean velocity within 200 kpc
        SetupNam += ["MeanVelocityWithin200kpc"]
        variableLabel += ["\\bar{v}_{\\mathrm{r<200}}"]
        INFO["%i-_filename" % i] = "MeanVelocityWithin200kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "MeanVelocityWithin200kpc",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "AbsoluteVelocityinkms",
            "mean",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "DistanceToCenterInKpc<200"
        )
    if 6.821 == setNum: #mean velocity within 500 kpc
        SetupNam += ["MeanVelocityWithin500kpc"]
        variableLabel += ["\\bar{v}_{\\mathrm{r<500}}"]
        INFO["%i-_filename" % i] = "MeanVelocityWithin500kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "MeanVelocityWithin500kpc",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "AbsoluteVelocityinkms",
            "mean",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "DistanceToCenterInKpc<500"
        )
    if 6.8211 == setNum: #mean velocity within 100 kpc
        SetupNam += ["MeanVelocityWithin100kpc"]
        variableLabel += ["\\bar{v}_{\\mathrm{r<100}}"]
        INFO["%i-_filename" % i] = "MeanVelocityWithin100kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "MeanVelocityWithin100kpc",
        )
        INFO["%i-_function" % i] = aux.get_value_single
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "AbsoluteVelocityinkms",
            "mean",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "DistanceToCenterInKpc<100"
        )
    if 6.822 == setNum: #mean mass-weighted velocity within 200 kpc
        SetupNam += ["MeanWeightedVelocityWMassWithin200kpc"]
        variableLabel += ["\\bar{v}_{\\mathrm{r<200}}"]
        INFO["%i-_filename" % i] = "MeanWeightedVelocityWMassWithin200kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "MeanWeightedVelocityWMassWithin200kpc",
        )
        INFO["%i-_function" % i] = aux.get_value_single_weighted
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "AbsoluteVelocityinkms",
            "Mass",
            "averageweighted",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "DistanceToCenterInKpc<200"
        )
    if 6.8221 == setNum: #mean mass-weighted velocity within 100 kpc
        SetupNam += ["MeanWeightedVelocityWMassWithin100kpc"]
        variableLabel += ["\\bar{v}_{\\mathrm{r<100}}"]
        INFO["%i-_filename" % i] = "MeanWeightedVelocityWMassWithin100kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "INFO:select",
            "MeanWeightedVelocityWMassWithin100kpc",
        )
        INFO["%i-_function" % i] = aux.get_value_single_weighted
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "AbsoluteVelocityinkms",
            "Mass",
            "averageweighted",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:select",
        )
        INFO["%i-_select" % i] = (
            "DistanceToCenterInKpc<100"
        )
    if 7 == setNum:
        SetupNam += ["BHMdot"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\dot{M}_\\mathrm{bh}"]
        INFO["%i-_filename" % i] = "BHMdot"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHMdot",
        )
        INFO["%i-_function" % i] = aux.get_BHMdot
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 7.0001 == setNum:
        SetupNam += ["BHMdotLog"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\dot{M}_\\mathrm{bh,log}"]
        INFO["%i-_function" % i] = aux.get_blackholes_logfile
        INFO["%i-_functionParameters" % i] = (
            "folder",
            ["total_mdot_in_sun_year"],
            False,
            None,
        )
        setup_style = "logfile"
    if 7.001 == setNum:
        SetupNam += ["AGNPowerLogFile"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\dot{M}_\\mathrm{bh,log}"]
        INFO["%i-_function" % i] = aux.get_BHAgnPowerLogFile
        INFO["%i-_functionParameters" % i] = ("folder",)
        setup_style = "logfile"
    if 7.01 == setNum:
        SetupNam += ["BHMdotEddington"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\dot{M}_\\mathrm{edd}"]
        INFO["%i-_filename" % i] = "BHMdotEddington"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHMdotEddington",
        )
        INFO["%i-_function" % i] = aux.get_BHEddington
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 7.02 == setNum:
        SetupNam += ["BHMdotChaoticAccMonitor"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\dot{M}_\\mathrm{cc,monitor}"]
        INFO["%i-_filename" % i] = "BHMdotChaoticAccMonitor"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHMdotChaoticAccMonitor",
        )
        INFO["%i-_function" % i] = aux.get_BHMdotChaoticAccMonitor
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 7.1 == setNum:
        SetupNam += ["BHMdotBondi"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\dot{M}_\\mathrm{bh,bondi}"]
        INFO["%i-_filename" % i] = "BHMdotBondi"
        INFO["%i-_function" % i] = aux.get_BondiMdotAroundBH
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "INFO",
        )
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:BondiAccretionRadius",
            "INFO:BondiBlackHoleAccretionFactor",
            "INFO:BlackHoleRadiativeEfficiency",
        )
        INFO["%i-_header" % i] = (
            "numberSN",
            "BondiAccretionRadius",
            "BondiBlackHoleAccretionFactor",
            "BlackHoleRadiativeEfficiency",
            "BHMdotBondi",
        )
        INFO["%i-_BondiAccretionRadius" % i] = "BH_Hsml"
        INFO["%i-_BondiBlackHoleAccretionFactor" % i] = 1
        INFO["%i-_BlackHoleRadiativeEfficiency" % i] = "parameterfile"
        INFO["%i-_NGBMaxSearchRadiusInKpc" % i] = 25
    if 7.12 == setNum:
        SetupNam += ["BHMdotBondiEddingtonRatio"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\dot{M}_\\mathrm{bondi}/\\dot{M}_\\mathrm{edd}"]
        INFO["%i-_filename" % i] = "BHMdotBondiEddingtonRatio"
        INFO["%i-_function" % i] = aux.get_BondiEddingtonRatio
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:BondiAccretionRadius",
            "INFO:BondiBlackHoleAccretionFactor",
            "INFO:BlackHoleRadiativeEfficiency",
        )
        INFO["%i-_header" % i] = (
            "numberSN",
            "BondiAccretionRadius",
            "BondiBlackHoleAccretionFactor",
            "BlackHoleRadiativeEfficiency",
            "BHMdotBondi",
        )
        INFO["%i-_BondiAccretionRadius" % i] = "BH_Hsml"
        INFO["%i-_BondiBlackHoleAccretionFactor" % i] = 1  # 'parameterfile'
        INFO["%i-_BlackHoleRadiativeEfficiency" % i] = "parameterfile"
        INFO["%i-_NGBMaxSearchRadiusInKpc" % i] = 25
    if 7.21 == setNum:  # constant radius
        SetupNam += ["BHMdotLiCCTiCoRaCo"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\dot{M}_\\mathrm{bh,Li}"]
        INFO["%i-_filename" % i] = "BHMdotLiCC"
        INFO["%i-_function" % i] = aux.get_LiCCMDot
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:LiCCMDotThreshTempInK",
            "INFO:LiCCMDotUseConstAccTime",
            "INFO:LiCCMDotAccTimeInMyr",
            "INFO:LiCCMDotUseConstRad",
            "INFO:LiCCMDotAccRadiusInKpc",
        )
        INFO["%i-_header" % i] = (
            "numberSN",
            "LiCCMDotThreshTempInK",
            "LiCCMDotUseConstAccTime",
            "LiCCMDotAccTimeInMyr",
            "LiCCMDotUseConstRad",
            "LiCCMDotAccRadiusInKpc",
            "BHMdot",
        )
        INFO["%i-_LiCCMDotThreshTempInK" % i] = 1e5
        INFO["%i-_LiCCMDotUseConstAccTime" % i] = True
        INFO["%i-_LiCCMDotAccTimeInMyr" % i] = 5
        INFO["%i-_LiCCMDotUseConstRad" % i] = True
        INFO["%i-_LiCCMDotAccRadiusInKpc" % i] = 0.5
    if 7.22 == setNum:  # variable radius depending on Bh ngbs
        SetupNam += ["BHMdotLiCCTiCoRaNgb"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\dot{M}_\\mathrm{bh,Li,ngb}"]
        INFO["%i-_filename" % i] = "BHMdotLiCC"
        INFO["%i-_function" % i] = aux.get_LiCCMDot
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:LiCCMDotThreshTempInK",
            "INFO:LiCCMDotUseConstAccTime",
            "INFO:LiCCMDotAccTimeInMyr",
            "INFO:LiCCMDotUseConstRad",
            "INFO:LiCCMDotAccRadiusInKpc",
        )
        INFO["%i-_header" % i] = (
            "numberSN",
            "LiCCMDotThreshTempInK",
            "LiCCMDotUseConstAccTime",
            "LiCCMDotAccTimeInMyr",
            "LiCCMDotUseConstRad",
            "LiCCMDotAccRadiusInKpc",
            "BHMdot",
        )
        INFO["%i-_LiCCMDotThreshTempInK" % i] = 1e6
        INFO["%i-_LiCCMDotUseConstAccTime" % i] = True
        INFO["%i-_LiCCMDotAccTimeInMyr" % i] = 5
        INFO["%i-_LiCCMDotUseConstRad" % i] = False
        INFO["%i-_LiCCMDotAccRadiusInKpc" % i] = 0.5
        INFO["%i-_NGBMaxSearchRadiusInKpc" % i] = 10
    if 7.23 == setNum:  # variable radius depending on Bh ngbs
        SetupNam += ["BHMdotLiCCTffVarRaNgbSfr"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\dot{M}_\\mathrm{bh,Li,sfr}"]
        INFO["%i-_filename" % i] = "BHMdotLiCCSfr"
        INFO["%i-_function" % i] = aux.get_LiCCMDot
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:LiCCMDotThreshTempInK",
            "INFO:LiCCMDotUseConstAccTime",
            "INFO:LiCCMDotAccTimeInMyr",
            "INFO:LiCCMDotUseConstRad",
            "INFO:LiCCMDotAccRadiusInKpc",
            "INFO:LiCCMDotUseSfrCrit",
        )
        INFO["%i-_header" % i] = (
            "numberSN",
            "LiCCMDotThreshTempInK",
            "LiCCMDotUseConstAccTime",
            "LiCCMDotAccTimeInMyr",
            "LiCCMDotUseConstRad",
            "LiCCMDotAccRadiusInKpc",
            "LiCCMDotUseSfrCrit",
            "BHMdot",
        )
        INFO["%i-_LiCCMDotThreshTempInK" % i] = 2e4
        INFO["%i-_LiCCMDotUseConstAccTime" % i] = False
        INFO["%i-_LiCCMDotAccTimeInMyr" % i] = "No"
        INFO["%i-_LiCCMDotUseConstRad" % i] = "BH_Hsml"
        INFO["%i-_LiCCMDotAccRadiusInKpc" % i] = "No"
        INFO["%i-_LiCCMDotUseSfrCrit" % i] = True
        INFO["%i-_NGBMaxSearchRadiusInKpc" % i] = 20
    if 7.231 == setNum:  # variable radius depending on Bh ngbs
        SetupNam += ["BHMdotLiCCTffVarRaNgbNoSfr"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\dot{M}_\\mathrm{bh,Li}"]
        INFO["%i-_filename" % i] = "BHMdotLiCCSfr"
        INFO["%i-_function" % i] = aux.get_LiCCMDot
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:LiCCMDotThreshTempInK",
            "INFO:LiCCMDotUseConstAccTime",
            "INFO:LiCCMDotAccTimeInMyr",
            "INFO:LiCCMDotUseConstRad",
            "INFO:LiCCMDotAccRadiusInKpc",
            "INFO:LiCCMDotUseSfrCrit",
        )
        INFO["%i-_header" % i] = (
            "numberSN",
            "LiCCMDotThreshTempInK",
            "LiCCMDotUseConstAccTime",
            "LiCCMDotAccTimeInMyr",
            "LiCCMDotUseConstRad",
            "LiCCMDotAccRadiusInKpc",
            "LiCCMDotUseSfrCrit",
            "BHMdot",
        )
        INFO["%i-_LiCCMDotThreshTempInK" % i] = 2e4
        INFO["%i-_LiCCMDotUseConstAccTime" % i] = False
        INFO["%i-_LiCCMDotAccTimeInMyr" % i] = "No"
        INFO["%i-_LiCCMDotUseConstRad" % i] = "BH_Hsml"
        INFO["%i-_LiCCMDotAccRadiusInKpc" % i] = "No"
        INFO["%i-_LiCCMDotUseSfrCrit" % i] = False
        INFO["%i-_NGBMaxSearchRadiusInKpc" % i] = 20
    if 7.24 == setNum:  # variable radius depending on Bh ngbs
        SetupNam += ["BHMdotLiCCTffCon"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\dot{M}_\\mathrm{50Myr,Li,ngb}"]
        INFO["%i-_filename" % i] = "BHMdotLiCC"
        INFO["%i-_function" % i] = aux.get_LiCCMDot
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "INFO",
        )  # considering_h=False
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:LiCCMDotThreshTempInK",
            "INFO:LiCCMDotUseConstAccTime",
            "INFO:LiCCMDotAccTimeInMyr",
            "INFO:LiCCMDotUseConstRad",
            "INFO:LiCCMDotAccRadiusInKpc",
        )
        INFO["%i-_header" % i] = (
            "numberSN",
            "LiCCMDotThreshTempInK",
            "LiCCMDotUseConstAccTime",
            "LiCCMDotAccTimeInMyr",
            "LiCCMDotUseConstRad",
            "LiCCMDotAccRadiusInKpc",
            "BHMdot",
        )
        INFO["%i-_LiCCMDotThreshTempInK" % i] = 2e4
        INFO["%i-_LiCCMDotUseConstAccTime" % i] = True
        INFO["%i-_LiCCMDotAccTimeInMyr" % i] = 50
        INFO["%i-_LiCCMDotUseConstRad" % i] = "BH_Hsml"
        INFO["%i-_LiCCMDotAccRadiusInKpc" % i] = "No"
        INFO["%i-_NGBMaxSearchRadiusInKpc" % i] = 20
    if 7.3 == setNum:
        SetupNam += ["BHCumMassGrowthInSU"]
        variableLabel += ["{M}_\\mathrm{bh,cum}\\ [\\\]"]
        INFO["%i-_filename" % i] = "get_CumMassGrowthBHInSU"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHCumMassGrowthInSU",
        )
        INFO["%i-_function" % i] = aux.get_CumMassGrowthBHInSU
        INFO["%i-_functionParameters" % i] = ("snap",)
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 7.31 == setNum:
        SetupNam += ["BHMasssFirstBlackhole"]
        variableLabel += ["{M}_\\mathrm{bh}\\ [\\mathrm{M}_\\odot]"]
        INFO["%i-_filename" % i] = "BHMasssFirstBlackhole"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHMasssFirstBlackhole",
        )
        INFO["%i-_function" % i] = aux.get_BlackholeMassInSU
        INFO["%i-_functionParameters" % i] = ("snap",)
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 7.32 == setNum:
        SetupNam += ["BHMasssSubgridFirstBlackhole"]
        variableLabel += ["{M}_\\mathrm{bh}\\ [\\mathrm{M}_\\odot]"]
        INFO["%i-_filename" % i] = "BHMasssSubgridFirstBlackhole"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BHMasssSubgridFirstBlackhole",
        )
        INFO["%i-_function" % i] = aux.get_BlackholeMassSubgridInSU
        INFO["%i-_functionParameters" % i] = ("snap",)
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 8 == setNum:
        SetupNam += ["SFR"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["\\mathrm{SFR}"]
        INFO["%i-_filename" % i] = "SFR"
        INFO["%i-_header" % i] = (
            "numberSN",
            "sfr_Msun_yr",
        )
        INFO["%i-_function" % i] = aux.get_StarFormationRateTotal
        INFO["%i-_functionParameters" % i] = ("snap",)  # considering_h=False
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 8.11 == setNum:
        SetupNam += ["CumSFMass"]
        variableLabel += ["{M}_\\mathrm{sf,cum}"]
        INFO["%i-_filename" % i] = "CumMassInStarsFormedM_sun"
        INFO["%i-_header" % i] = (
            "numberSN",
            "CumMassInStarsFormedM_sun",
        )
        INFO["%i-_function" % i] = aux.get_cum_variable
        INFO["%i-_functionParameters" % i] = (
            "snap",
            "INFO",
        )
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_cumFunction" % i] = aux.get_StarFormationRateTotal
        INFO["%i-_cumTimesFunction" % i] = aux.get_delta_time_snapshot_in_myr
        INFO["%i-_cumFactor" % i] = 1e6
    if 9 == setNum:
        SetupNam += ["XCoolingPower30kpc"]
        variableLabel += ["{P}_\\mathrm{X,30\,kpc}"]
        INFO["%i-_filename" % i] = "XrayPowerWithinRadiusKpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ColdGasRadiusInKpc",
            "CoolingXrayPowerErgss",
        )
        INFO["%i-_function" % i] = aux.get_XrayCoolingRateWithinKpc
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ColdGasRadiusInKpc" % i] = 30
    if 9.01 == setNum:
        SetupNam += ["CoolingPower100kpc"]
        variableLabel += ["{P}_\\mathrm{cool,100\,kpc}"]
        INFO["%i-_filename" % i] = "CoolingPowerWithinRadiusKpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ColdGasRadiusInKpc",
            "CoolingPowerWithinRadiusKpc_in_erg_s",
        )
        INFO["%i-_function" % i] = aux.get_CoolingLuminosityWithinKpc
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ColdGasRadiusInKpc" % i] = 100
    if 9.0111 == setNum:
        SetupNam += ["CumCoolingPower100kpc"]
        variableLabel += ["{P}_\\mathrm{X,100\,kpc,cum}"]
        INFO["%i-_filename" % i] = "CumCoolingPowerWithinRadiusKpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ColdGasRadiusInKpc",
            "CumCoolingPower100kpc_in_erg",
        )
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ColdGasRadiusInKpc" % i] = 100

        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_function" % i] = aux.get_cum_variable
        INFO["%i-_cumFunction" % i] = aux.get_CoolingLuminosityWithinKpc
        INFO["%i-_cumTimesFunction" % i] = aux.get_delta_time_snapshot_in_myr
        INFO["%i-_cumFactor" % i] = 31557600000000.0
    if 9.012 == setNum:
        SetupNam += ["CoolingPower40kpc"]
        variableLabel += ["{P}_\\mathrm{cool,40\,kpc}"]
        INFO["%i-_filename" % i] = "CoolingPowerWithinRadiusKpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ColdGasRadiusInKpc",
            "CoolingPowerWithinRadiusKpc_in_erg_s",
        )
        INFO["%i-_function" % i] = aux.get_CoolingLuminosityWithinKpc
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ColdGasRadiusInKpc" % i] = 40
    if 9.013 == setNum:
        SetupNam += ["CoolingPower50kpc"]
        variableLabel += ["{P}_\\mathrm{cool,50\,kpc}"]
        INFO["%i-_filename" % i] = "CoolingPowerWithinRadiusKpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ColdGasRadiusInKpc",
            "CoolingPowerWithinRadiusKpc_in_erg_s",
        )
        INFO["%i-_function" % i] = aux.get_CoolingLuminosityWithinKpc
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ColdGasRadiusInKpc" % i] = 50
    if 9.02 == setNum:
        SetupNam += ["XCoolingPower100kpc"]
        variableLabel += ["{P}_\\mathrm{X-theory,100\,kpc}"]
        INFO["%i-_filename" % i] = "XrayPowerWithinRadiusKpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ColdGasRadiusInKpc",
            "CoolingXrayPowerErgss",
        )
        INFO["%i-_function" % i] = aux.get_XrayCoolingRateWithinKpc
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ColdGasRadiusInKpc",
        )
        INFO["%i-_ColdGasRadiusInKpc" % i] = 100
    if 9.021 == setNum:
        SetupNam += ["XCoolingPowerDebug100kpc"]
        variableLabel += ["{P}_\\mathrm{X-theory-debug,100\,kpc}"]
        INFO["%i-_filename" % i] = "XCoolingPowerDebug100kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "ColdGasRadiusInKpc",
            "XrayEmissivityDebug",
            "XCoolingPowerDebug100kpc",
        )
        INFO["%i-_function" % i] = aux.get_XrayCoolingRateWithinKpc
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:ColdGasRadiusInKpc",
            "INFO:XrayEmissivityDebug",
        )
        INFO["%i-_ColdGasRadiusInKpc" % i] = 100
        INFO["%i-_XrayEmissivityDebug" % i] = True
    if 9.1 == setNum:
        SetupNam += ["BhAgnPowerCgs"]
        variableLabel += ["{P}_\\mathrm{agn}"]
        INFO["%i-_filename" % i] = "BhAGNPowerCgs"
        INFO["%i-_header" % i] = (
            "numberSN",
            "BhAGNPowerInErg_s",
        )
        INFO["%i-_function" % i] = aux.get_BHAgnPower
        INFO["%i-_functionParameters" % i] = ("snap",)
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 9.11 == setNum:
        SetupNam += ["CumBhAgnPowerCgs"]
        variableLabel += ["{P}_\\mathrm{agn,cum}"]
        INFO["%i-_filename" % i] = "CumBhAgnPowerCgs"
        INFO["%i-_header" % i] = (
            "numberSN",
            "CumBhAGNPowerInErg_s",
        )
        INFO["%i-_function" % i] = aux.get_cum_variable
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = ("numberSN",)
        INFO["%i-_cumFunction" % i] = aux.get_BHAgnPower
        INFO["%i-_cumTimesFunction" % i] = aux.get_delta_time_snapshot_in_myr
        INFO["%i-_cumFactor" % i] = 31557600000000.0
    if 9.4 == setNum:
        SetupNam += ["FittedAGNPowerInErg"]
        variableLabel += ["\\mathrm{pdV}_\\mathrm{agn}"]
        INFO["%i-_filename" % i] = "FittedAGNPowerInErg"
        INFO["%i-_header" % i] = (
            "numberSN",
            "variable",
            "minvol",
            "binsize",
            "resdepth",
            "res",
            "depth",
            "axis",
            "contourlevel",
            "ProjBox",
            "maxDistanceBubble",
            "FittedAGNPowerInErg",
        )
        INFO["%i-_function" % i] = feature_extraction.get_pdV_energy_in_erg
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:variable",
            "INFO:minvol",
            "INFO:binsize",
            "INFO:resdepth",
            "INFO:res",
            "INFO:depth",
            "INFO:axis",
            "INFO:contourlevel",
            "INFO:ProjBox",
            "INFO:maxDistanceBubble",
        )
        INFO["%i-_variable" % i] = "XrayEmissivityABOVE0p2keVBELOW10keV"
        INFO["%i-_minvol" % i] = 40
        INFO["%i-_binsize" % i] = 10
        INFO["%i-_resdepth" % i] = 300
        INFO["%i-_res" % i] = 300
        INFO["%i-_depth" % i] = 150
        INFO["%i-_axis" % i] = [2, 0]
        INFO["%i-_contourlevel" % i] = -1.5e-1
        INFO["%i-_ProjBox" % i] = 150
        INFO["%i-_maxDistanceBubble" % i] = 150
    if 9.41 == setNum:
        SetupNam += ["FittedAGNPowerInErg"]
        variableLabel += ["\\mathrm{pdV}_\\mathrm{agn}"]
        INFO["%i-_filename" % i] = "FittedAGNPowerInErg"
        INFO["%i-_header" % i] = (
            "numberSN",
            "variable",
            "minvol",
            "binsize",
            "resdepth",
            "res",
            "depth",
            "axis",
            "contourlevel",
            "ProjBox",
            "maxDistanceBubble",
            "FittedAGNPowerInErg",
        )
        INFO["%i-_function" % i] = feature_extraction.get_pdV_energy_in_erg
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:variable",
            "INFO:minvol",
            "INFO:binsize",
            "INFO:resdepth",
            "INFO:res",
            "INFO:depth",
            "INFO:axis",
            "INFO:contourlevel",
            "INFO:ProjBox",
            "INFO:maxDistanceBubble",
        )
        INFO["%i-_variable" % i] = "XrayEmissivityABOVE0p2keVBELOW10keV"
        INFO["%i-_minvol" % i] = 40
        INFO["%i-_binsize" % i] = 10
        INFO["%i-_resdepth" % i] = 100
        INFO["%i-_res" % i] = 100
        INFO["%i-_depth" % i] = 150
        INFO["%i-_axis" % i] = [2, 0]
        INFO["%i-_contourlevel" % i] = -1.5e-1
        INFO["%i-_maxDistanceBubble" % i] = 150
        INFO["%i-_ProjBox" % i] = 150
    if 9.51 == setNum:
        SetupNam += ["FittedAGNPowerInErgPerS"]
        variableLabel += ["P_\\mathrm{cav}"]
        INFO["%i-_filename" % i] = "FittedAGNPowerInErgPerS"
        INFO["%i-_header" % i] = (
            "numberSN",
            "variable",
            "minvol",
            "binsize",
            "resdepth",
            "res",
            "depth",
            "axis",
            "contourlevel",
            "ProjBox",
            "timescale",
            "maxDistanceBubble",
            "FittedAGNPowerInErgPerS",
        )
        INFO["%i-_function" % i] = feature_extraction.get_pdV_power_in_ergpersec
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:variable",
            "INFO:minvol",
            "INFO:binsize",
            "INFO:resdepth",
            "INFO:res",
            "INFO:depth",
            "INFO:axis",
            "INFO:contourlevel",
            "INFO:ProjBox",
            "INFO:timescale",
            "INFO:maxDistanceBubble",
        )
        INFO["%i-_variable" % i] = "CoolingEmissivityABOVE0p2keVBELOW10keV"
        INFO["%i-_minvol" % i] = 40
        INFO["%i-_binsize" % i] = 10
        INFO["%i-_resdepth" % i] = 100
        INFO["%i-_res" % i] = 100
        INFO["%i-_depth" % i] = 150
        INFO["%i-_axis" % i] = [2, 0]
        INFO["%i-_contourlevel" % i] = -1.5e-1
        INFO["%i-_ProjBox" % i] = 150
        INFO["%i-_maxDistanceBubble" % i] = 150
        INFO["%i-_timescale" % i] = "soundcrossingtime"
    if 9.5111 == setNum:
        SetupNam += ["CumFittedAGNPowerInErg"]
        variableLabel += ["P_\\mathrm{cav,cum}"]
        INFO["%i-_filename" % i] = "CumFittedAGNPowerInErg"
        INFO["%i-_header" % i] = (
            "numberSN",
            "variable",
            "minvol",
            "binsize",
            "resdepth",
            "res",
            "depth",
            "axis",
            "contourlevel",
            "ProjBox",
            "timescale",
            "maxDistanceBubble",
            "CumFittedAGNPowerInErg",
        )
        INFO["%i-_function" % i] = aux.get_cum_variable
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:variable",
            "INFO:minvol",
            "INFO:binsize",
            "INFO:resdepth",
            "INFO:res",
            "INFO:depth",
            "INFO:axis",
            "INFO:contourlevel",
            "INFO:ProjBox",
            "INFO:timescale",
            "INFO:maxDistanceBubble",
        )
        INFO["%i-_variable" % i] = "XrayEmissivityABOVE0p2keVBELOW10keV"
        INFO["%i-_minvol" % i] = 40
        INFO["%i-_binsize" % i] = 10
        INFO["%i-_resdepth" % i] = 100
        INFO["%i-_res" % i] = 100
        INFO["%i-_depth" % i] = 150
        INFO["%i-_axis" % i] = [2, 0]
        INFO["%i-_contourlevel" % i] = -1.5e-1
        INFO["%i-_ProjBox" % i] = 150
        INFO["%i-_maxDistanceBubble" % i] = 150
        INFO["%i-_timescale" % i] = "soundcrossingtime"

        INFO["%i-_cumFunction" % i] = feature_extraction.get_pdV_power_in_ergpersec
        INFO["%i-_cumTimesFunction" % i] = aux.get_delta_time_snapshot_in_myr
        INFO["%i-_cumFactor" % i] = 31557600000000.0
    if 9.501 == setNum:
        SetupNam += ["FittedAGNPowerInErgPerSAx10"]
        variableLabel += ["P_\\mathrm{cav,ax=1,0}"]
        INFO["%i-_filename" % i] = "FittedAGNPowerInErgPerSAx10"
        INFO["%i-_header" % i] = (
            "numberSN",
            "variable",
            "minvol",
            "binsize",
            "resdepth",
            "res",
            "depth",
            "axis",
            "contourlevel",
            "ProjBox",
            "timescale",
            "maxDistanceBubble",
            "FittedAGNPowerInErgPerSAx10",
        )
        INFO["%i-_function" % i] = feature_extraction.get_pdV_power_in_ergpersec
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:variable",
            "INFO:minvol",
            "INFO:binsize",
            "INFO:resdepth",
            "INFO:res",
            "INFO:depth",
            "INFO:axis",
            "INFO:contourlevel",
            "INFO:ProjBox",
            "INFO:timescale",
            "INFO:maxDistanceBubble",
        )
        INFO["%i-_variable" % i] = "XrayEmissivityABOVE0p2keVBELOW10keV"
        INFO["%i-_minvol" % i] = 40
        INFO["%i-_binsize" % i] = 10
        INFO["%i-_resdepth" % i] = 100
        INFO["%i-_res" % i] = 100
        INFO["%i-_depth" % i] = 150
        INFO["%i-_axis" % i] = [1, 0]
        INFO["%i-_contourlevel" % i] = -1.5e-1
        INFO["%i-_ProjBox" % i] = 150
        INFO["%i-_maxDistanceBubble" % i] = 150
        INFO["%i-_timescale" % i] = "soundcrossingtime"
    if 9.502 == setNum:
        SetupNam += ["FittedAGNPowerInErgPerSRes200"]
        variableLabel += ["P_\\mathrm{cav,res=200}"]
        INFO["%i-_filename" % i] = "FittedAGNPowerInErgPerSRes200"
        INFO["%i-_header" % i] = (
            "numberSN",
            "variable",
            "minvol",
            "binsize",
            "resdepth",
            "res",
            "depth",
            "axis",
            "contourlevel",
            "ProjBox",
            "timescale",
            "maxDistanceBubble",
            "FittedAGNPowerInErgPerSAx10",
        )
        INFO["%i-_function" % i] = feature_extraction.get_pdV_power_in_ergpersec
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:variable",
            "INFO:minvol",
            "INFO:binsize",
            "INFO:resdepth",
            "INFO:res",
            "INFO:depth",
            "INFO:axis",
            "INFO:contourlevel",
            "INFO:ProjBox",
            "INFO:timescale",
            "INFO:maxDistanceBubble",
        )
        INFO["%i-_variable" % i] = "XrayEmissivityABOVE0p2keVBELOW10keV"
        INFO["%i-_minvol" % i] = 40
        INFO["%i-_binsize" % i] = 10
        INFO["%i-_resdepth" % i] = 200
        INFO["%i-_res" % i] = 200
        INFO["%i-_depth" % i] = 150
        INFO["%i-_axis" % i] = [2, 0]
        INFO["%i-_contourlevel" % i] = -1.5e-1
        INFO["%i-_ProjBox" % i] = 150
        INFO["%i-_maxDistanceBubble" % i] = 150
        INFO["%i-_timescale" % i] = "soundcrossingtime"
    elif 9.503 == setNum:
        SetupNam += ["FittedAGNPowerInErgPerSBox300"]
        variableLabel += ["P_\\mathrm{cav}"]
        INFO["%i-_filename" % i] = "FittedAGNPowerInErgPerSBox300"
        INFO["%i-_header" % i] = (
            "numberSN",
            "variable",
            "minvol",
            "binsize",
            "resdepth",
            "res",
            "depth",
            "axis",
            "contourlevel",
            "ProjBox",
            "maxDistanceBubble",
            "timescale",
            "FittedAGNPowerInErgPerS",
        )
        INFO["%i-_function" % i] = feature_extraction.get_pdV_power_in_ergpersec
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:variable",
            "INFO:minvol",
            "INFO:binsize",
            "INFO:resdepth",
            "INFO:res",
            "INFO:depth",
            "INFO:axis",
            "INFO:contourlevel",
            "INFO:ProjBox",
            "INFO:maxDistanceBubble",
            "INFO:timescale",
        )
        INFO["%i-_variable" % i] = "XrayEmissivityABOVE0p2keVBELOW10keV"
        INFO["%i-_minvol" % i] = 40
        INFO["%i-_binsize" % i] = 10
        INFO["%i-_resdepth" % i] = 200
        INFO["%i-_res" % i] = 200
        INFO["%i-_depth" % i] = 300
        INFO["%i-_axis" % i] = [2, 0]
        INFO["%i-_contourlevel" % i] = -1.5e-1
        INFO["%i-_ProjBox" % i] = 300
        INFO["%i-_maxDistanceBubble" % i] = 300
        INFO["%i-_timescale" % i] = "soundcrossingtime"
    if 9.61 == setNum:
        SetupNam += ["FittedAverageBubbleTimescaleInMyr"]
        variableLabel += ["t_\\mathrm{bubble}"]
        INFO["%i-_filename" % i] = "FittedAverageBubbleTimescaleInMyr"
        INFO["%i-_header" % i] = (
            "numberSN",
            "variable",
            "minvol",
            "binsize",
            "resdepth",
            "res",
            "depth",
            "axis",
            "contourlevel",
            "ProjBox",
            "timescale",
            "maxDistanceBubble",
            "FittedAverageBubbleTimescaleInMyr",
        )
        INFO["%i-_function" % i] = feature_extraction.get_average_timescale_in_myr
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:variable",
            "INFO:minvol",
            "INFO:binsize",
            "INFO:resdepth",
            "INFO:res",
            "INFO:depth",
            "INFO:axis",
            "INFO:contourlevel",
            "INFO:ProjBox",
            "INFO:timescale",
            "INFO:maxDistanceBubble",
        )
        INFO["%i-_variable" % i] = "XrayEmissivityABOVE0p2keVBELOW10keV"
        INFO["%i-_minvol" % i] = 40
        INFO["%i-_binsize" % i] = 10
        INFO["%i-_resdepth" % i] = 100
        INFO["%i-_res" % i] = 100
        INFO["%i-_depth" % i] = 150
        INFO["%i-_axis" % i] = [2, 0]
        INFO["%i-_contourlevel" % i] = -1.5e-1
        INFO["%i-_ProjBox" % i] = 150
        INFO["%i-_maxDistanceBubble" % i] = 150
        INFO["%i-_timescale" % i] = "soundcrossingtime"
    if 9.62 == setNum:
        SetupNam += ["FittedAverageBubbleTimescaleInMyrRes300"]
        variableLabel += ["t_\\mathrm{bubble}"]
        INFO["%i-_filename" % i] = "FittedAverageBubbleTimescaleInMyrRes300"
        INFO["%i-_header" % i] = (
            "numberSN",
            "variable",
            "minvol",
            "binsize",
            "resdepth",
            "res",
            "depth",
            "axis",
            "contourlevel",
            "ProjBox",
            "timescale",
            "maxDistanceBubble",
            "FittedAverageBubbleTimescaleInMyr",
        )
        INFO["%i-_function" % i] = feature_extraction.get_average_timescale_in_myr
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:variable",
            "INFO:minvol",
            "INFO:binsize",
            "INFO:resdepth",
            "INFO:res",
            "INFO:depth",
            "INFO:axis",
            "INFO:contourlevel",
            "INFO:ProjBox",
            "INFO:timescale",
            "INFO:maxDistanceBubble",
        )
        INFO["%i-_variable" % i] = "XrayEmissivityABOVE0p2keVBELOW10keV"
        INFO["%i-_minvol" % i] = 40
        INFO["%i-_binsize" % i] = 10
        INFO["%i-_resdepth" % i] = 300
        INFO["%i-_res" % i] = 300
        INFO["%i-_depth" % i] = 150
        INFO["%i-_axis" % i] = [2, 0]
        INFO["%i-_contourlevel" % i] = -1.5e-1
        INFO["%i-_ProjBox" % i] = 150
        INFO["%i-_maxDistanceBubble" % i] = 150
        INFO["%i-_timescale" % i] = "soundcrossingtime"
    if 10 == setNum:
        SetupNam += ["NgbRadius"]
        variableLabel += ["{r}_\\mathrm{ngb}"]
        INFO["%i-_filename" % i] = "RadiusBHNgb"
        INFO["%i-_header" % i] = (
            "numberSN",
            "NumberNgb",
            "NGBRadius",
        )
        INFO["%i-_function" % i] = aux.get_BHNgbRadius
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:NumberNgb",
        )
        INFO["%i-_MaxRadiusNgbSearchInKpc" % i] = MAXSEARCHRADIUS
        INFO["%i-_NumberNgb" % i] = "DesNumNgbBlackHole"
    if 10.1 == setNum:
        SetupNam += ["RadiusBHhsml"]
        variableLabel += ["{r}_\\mathrm{hsml}"]
        INFO["%i-_filename" % i] = "RadiusBHhsml"
        INFO["%i-_header" % i] = (
            "numberSN",
            "UnitsBHhsml",
            "RadiusBHhsml",
        )
        INFO["%i-_function" % i] = aux.get_BHhsml
        INFO["%i-_functionParameters" % i] = ("snap", True, "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:UnitsBHhsml",
        )
        INFO["%i-_UnitsBHhsml" % i] = "kpc"
    if 10.5 == setNum:
        SetupNam += ["NgbWithingBHhsml"]
        variableLabel += ["{N}_\\mathrm{ngb}"]
        INFO["%i-_filename" % i] = "NgbWithingBHhsml"
        INFO["%i-_header" % i] = (
            "numberSN",
            "NgbWithingBHhsml",
        )
        INFO["%i-_function" % i] = aux.get_Nngb_within_BHhsml
        INFO["%i-_functionParameters" % i] = ("snap",)
        INFO["%i-_relevantElements" % i] = ("numberSN",)
    if 11 == setNum:
        SetupNam += ["DraggedGasMassFraction"]
        variableLabel += ["{M}_\\mathrm{dragged}"]
        INFO["%i-_filename" % i] = "DraggedGasMassFraction"
        INFO["%i-_header" % i] = (
            "numberSN",
            "Nscalar",
            "RadiusKpc",
        )
        INFO["%i-_function" % i] = aux.get_BHNgbRadius
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:NumberNgb",
        )
        INFO["%i-_MaxRadiusNgbSearchInKpc" % i] = MAXSEARCHRADIUS
        INFO["%i-_NumberNgb" % i] = "DesNumNgbBlackHole"
    if 12.1 == setNum:  # cr acceleration scalar
        SetupNam += ["DraggedGasMassFraction"]
        variableLabel += ["{M}_\\mathrm{dragged}"]
        INFO["%i-_filename" % i] = "DraggedGasMassFraction"
        INFO["%i-_header" % i] = (
            "numberSN",
            "Nscalar",
            "RadiusKpc",
        )
        INFO["%i-_function" % i] = aux.get_BHNgbRadius
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:NumberNgb",
        )
        INFO["%i-_MaxRadiusNgbSearchInKpc" % i] = MAXSEARCHRADIUS
        INFO["%i-_NumberNgb" % i] = "DesNumNgbBlackHole"
    if 14.1 == setNum:  # cr acceleration scalar
        SetupNam += ["MeanEntropyAt10kpc"]
        variableLabel += ["\\hat{K}_{10\\,\\mathrm{kpc}}"]
        INFO["%i-_filename" % i] = "MeanEntropyAt10kpc"
        INFO["%i-_header" % i] = (
            "numberSN",
            "variableMeanVariable",
            "variableMeanSelect",
            "variableMeanWeight",
            "variableMeanAveragingRangeInMyr",
            "variableMeanDistInKpc",
            "MeanEntropyAt10kpc",
        )
        INFO[
            "%i-_function" % i
        ] = feature_extraction.get_mean_variable_at_position_in_kpc_average_time
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:variableMeanVariable",
            "INFO:variableMeanSelect",
            "INFO:variableMeanWeight",
            "INFO:variableMeanAveragingRangeInMyr",
            "INFO:variableMeanDistInKpc",
        )
        INFO["%i-_variableMeanVariable" % i] = "Entropy"
        INFO["%i-_variableMeanSelect" % i] = "TemperatureInkV>0.2ANDTemperatureInkV<10"
        INFO["%i-_variableMeanWeight" % i] = "XrayLuminosity"
        INFO["%i-_variableMeanAveragingRangeInMyr" % i] = 20
        INFO["%i-_variableMeanSaveFig" % i] = True
        INFO["%i-_variableMeanDistInKpc" % i] = 10
    if 99 == setNum:
        SetupNam += ["DummyOne"]
        variableLabel += ["dummy1"]
        INFO["%i-_filename" % i] = "Dummy11"
        INFO["%i-_header" % i] = (
            "numberSN",
            "value",
            "ValueOne",
        )
        INFO["%i-_function" % i] = aux.get_dummy_value
        INFO["%i-_functionParameters" % i] = (1, "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:value",
        )
        INFO["%i-_value" % i] = 1.0
    if 99.001 == setNum:
        SetupNam += ["DeltaTime"]
        variableLabel += ["DeltaTime"]
        INFO["%i-_filename" % i] = "DeltaTime"
        INFO["%i-_header" % i] = (
            "numberSN",
            "usePreviousIfLastSnap",
            "deltaTime",
        )
        INFO["%i-_function" % i] = aux.get_delta_time_snapshot_in_myr
        INFO["%i-_functionParameters" % i] = ("snap", "INFO")
        INFO["%i-_relevantElements" % i] = (
            "numberSN",
            "INFO:usePreviousIfLastSnap",
        )
        INFO["%i-_usePreviousIfLastSnap" % i] = True
    if 99.01 == setNum:
        SetupNam += ["DummyOneLog"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["dummy1log"]
        INFO["%i-_function" % i] = aux.get_dummy_value_log
        INFO["%i-_functionParameters" % i] = (
            "folder",
            "value",
            "INFO",
        )
        INFO["%i-_value" % i] = 1.0
        setup_style = "logfile"
    if 99.1 == setNum:
        SetupNam += ["deltatlog"]
        # variableLabel += ['\\mathrm{therm}\\ (kT_e=10\\ \\mathrm{keV})']
        variableLabel += ["deltatlog"]
        INFO["%i-_function" % i] = aux.get_delta_time_logfile
        INFO["%i-_functionParameters" % i] = ("folder",)
        setup_style = "logfile"
    return i, SetupNam, variableLabel, INFO, setup_style


def RetrivingValuesFromStrings(string, snap, INFO, numberSN, folder=None):
    """
	INFO:considering_h -> always start with INFO:

	"""
    if type(string) != str:
        return string
    if string == "snap":
        string = snap
    elif string == "folder":
        string = folder
    elif string == "INFO":
        string = INFO
    elif "INFO:" in string:
        string = INFO[string.split("INFO:")[1]]
    elif string == "numberSN":
        string = numberSN
    return string


def retrieve_parameters_from_info(INFO, pre_string_key="0-_"):
    relevantElements = INFO[f"{pre_string_key}relevantElements"]
    filename = INFO[f"{pre_string_key}filename"]
    header = INFO[f"{pre_string_key}header"]
    function = INFO[f"{pre_string_key}function"]
    functionParameters = INFO[f"{pre_string_key}functionParameters"]
    return relevantElements, filename, header, function, functionParameters


def get_filename(filename, folder_snapshot, functionParameters):
    if "snap" in functionParameters:
        filename = folder_snapshot + "AnalysisData/" + filename + ".txt"
    else:
        filename = "Data/" + filename + ".txt"
    return filename


def _box_k_smoothing(
    k,
    values,
    times,
    weights=None,
    statistics="mean",
    running_average=True,
    running_average_step=1,
):
    if running_average:
        bins = []
        for i in range(k):
            bins.append(
                np.arange(
                    min(times) - k / 2.0 + i,
                    max(times) + k / 2.0 + k + i,
                    k,
                    dtype=np.float128,
                )
            )
    else:
        bins = [
            np.arange(
                min(times) - k / 2.0, max(times) + k / 2.0 + k, k, dtype=np.float128
            )
        ]
    binned = np.zeros(np.shape(aux.flatten(bins))[0] - len(bins))
    for i, b in enumerate(bins):
        if not running_average:
            k = 0
        binned[i::k] = _single_smooth(b, statistics, times, values, weights)
    return binned


def _single_smooth(bins, statistics, times, values, weights=None):
    import scipy

    scaling = np.max(values)
    if any(np.isnan(np.float64(values / scaling))):
        print(f"values/scaling is nan: {values / scaling}")
        print(f"values: {values}")
        print(f"scaling: {scaling}")
        print(
            f"times[np.isnan(np.float64(values)]: {times[np.isnan(np.float64(values))]}"
        )
        raise Exception(
            f"np.float64(values/scaling) is nan: {np.float64(values / scaling)}"
        )
    if weights is not None:
        wv = scipy.stats.binned_statistic(
            np.float64(times),
            np.float64(weights) * np.float64(values / scaling),
            statistic="sum",
            bins=bins,
        )
        w = scipy.stats.binned_statistic(
            np.float64(times), np.float64(weights), statistic="sum", bins=bins
        )
        binned = wv.statistic / w.statistic * scaling
    else:
        ret = scipy.stats.binned_statistic(
            np.float64(times),
            np.float64(values / scaling),
            statistic=statistics,
            bins=bins,
        )
        print(f"ret.bin_edges: {ret.bin_edges}")
        binned = ret.statistic * scaling
    return binned


def get_reduction(style, x, y, x_range):
    reduction = None
    if style is None:
        pass
    elif style == "mean":
        m = np.logical_and(x > x_range[0], x < x_range[1])
        reduction = np.sum(np.diff(x[m]) * y[m][:-1]) / np.sum(np.diff(x[m]))
    else:
        raise Exception(f"reduction style: {style} not implemented!")
    return reduction


def load_data_pfrommer2012():
    # taken from arxiv id: 1106.5505
    df = pd.read_csv(
        "DataExtraction/Pfrommer2012/agn.d",
        skiprows=1,
        delim_whitespace=True,
        names=[
            "CName",
            "redshift",
            "PV",
            "negDelPV",
            "posDelPV",
            "Pcav",
            "negDelPcav",
            "posDelPcav",
            "K0",
            "DeltaK0",
            "kTX",
        ],
    )
    return df


def get_theory_grid_variable(setup):
    if setup == 1 or setup == "entropyPfrommer2012":
        data = load_data_pfrommer2012()["K0"].values
    elif setup == 2 or setup == "pcavPfrommer2012":
        # converted to erg/s
        data = load_data_pfrommer2012()["Pcav"].values * 1e44 / 1e2
    return data


def get_axes_limits(limit, index):
    if aux.isIterable(limit):
        l = limit[index]
    else:
        l = limit
    return l


def get_global_extrema(data, limit, max=True):
    if limit is None:
        if max:
            l = np.nanmax(aux.flatten(data))
        else:
            l = np.nanmin(aux.flatten(data))
    else:
        l = limit
    return l
