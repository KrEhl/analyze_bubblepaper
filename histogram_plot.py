import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import FigureMove as Fig
import numpy as np
import AxesGeneration
import auxiliary_functions as aux
import FitUtils as Fit
import matplotlib.patheffects as pe
import Param as param
import collections
import multiprocessing as mp
import warnings

import debug
import script_helpers
import time
import argparse
import argparse_helpers
import datetime


numthreads = 32
if numthreads > mp.cpu_count():
    numthreads = mp.cpu_count() - 1
text_color = "navy"
TEST = False
default_folder = "_M15mhdcrsLL"
default_numbers = [800, 1000, 1200]
if os.path.exists("/home/kristian/Analysis/") or os.path.exists(
    "/home/kristian/analysis/"
):
    print("using test!")
    TEST = True

if TEST:
    default_folder = "_test"
    default_numbers = [0, 50]

start_time = time.time()
parser = argparse.ArgumentParser(
    description="grid plots of different variables!",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    f"--folder", type=argparse_helpers.fancy_eval, nargs="+", default=[default_folder]
)
parser.add_argument(
    f"--setup",
    type=argparse_helpers.fancy_eval,
    nargs="+",
    default=["temperatures_around_bh"],
)
parser.add_argument(
    f"--numbers", type=argparse_helpers.fancy_eval, nargs="+", default=default_numbers,
)
parser.add_argument(
    f"--vary_along_x", type=argparse_helpers.fancy_eval, default="folder"
)
parser.add_argument(
    f"--vary_along_y", type=argparse_helpers.fancy_eval, default="snapshot_number"
)
parser.add_argument(f"--xmin", type=argparse_helpers.fancy_eval, default="default")
parser.add_argument(f"--xmax", type=argparse_helpers.fancy_eval, default="default")
parser.add_argument(f"--ymin", type=argparse_helpers.fancy_eval, default="default")
parser.add_argument(f"--ymax", type=argparse_helpers.fancy_eval, default="default")
parser.add_argument(
    f"--axis", type=argparse_helpers.fancy_eval, nargs="+", default=[2, 0]
)
parser.add_argument(f"--vmin", type=argparse_helpers.fancy_eval, default="default")
parser.add_argument(f"--vmax", type=argparse_helpers.fancy_eval, default="default")
parser.add_argument(f"--plot_style", type=argparse_helpers.fancy_eval, default="bar")
parser.add_argument(
    f"--averaging_range", type=argparse_helpers.fancy_eval, default=[None]
)
parser.add_argument(
    f"--histogram_average_style", type=argparse_helpers.fancy_eval, default="default"
)
parser.add_argument(f"--nbins", type=argparse_helpers.fancy_eval, default=60)
parser.add_argument(f"--numbers_min", type=argparse_helpers.fancy_eval, default=False)
parser.add_argument(f"--numbers_max", type=argparse_helpers.fancy_eval, default=False)
parser.add_argument(f"--numbers_n", type=argparse_helpers.fancy_eval, default=8)
parser.add_argument(f"--fontsize", type=argparse_helpers.fancy_eval, default=10)
parser.add_argument(f"--histogram_differential", action="store_true", default=False)
parser.add_argument(f"--plot_color_as_circle", action="store_true", default=False)
parser.add_argument(
    f"--single_colorbar", type=argparse_helpers.fancy_eval, default=[True]
)
parser.add_argument(
    f"--single_colorbar_above", type=argparse_helpers.fancy_eval, default=[True]
)
parser.add_argument(
    f"--single_xlabel", type=argparse_helpers.fancy_eval, default=[True]
)
parser.add_argument(
    f"--single_ylabel", type=argparse_helpers.fancy_eval, default=[True]
)
parser.add_argument(
    f"--plot_images", type=argparse_helpers.fancy_eval, default=[True]
)
parser.add_argument(
    f"--show_histograms", type=argparse_helpers.fancy_eval, default="bottomright"
)
parser.add_argument(f"--scale_bar_position", type=argparse_helpers.fancy_eval, default="farupperright")
parser.add_argument(f"--test", action="store_true", default=False)
parser.add_argument(f"--heightwidth", nargs="+", type=argparse_helpers.fancy_eval, default=[None])
parser.add_argument(f"--scale_bar", type=argparse_helpers.fancy_eval, default=[10])


parser.add_argument(f"--paper", action="store_true", default=False)
input = parser.parse_args().__dict__.copy()
print(input)
input = {k: v for k, v in input.items() if v is not None}
script_helpers.get_parameters_from_yaml(input)
print(input)
folders = input["folder"]
roots = input["root"]
folders_name_save = input["name_sim_save"]
folders_name_short = input["name_sim"]
xmin = input["xmin"][0]
axis = input["axis"]
if len(axis) != 2:
    raise Exception(f"axis not len 2 {axis}")
xmax = input["xmax"][0]
ymin = input["ymin"][0]
ymax = input["ymax"][0]
vvmin = input["vmin"][0]
vvmax = input["vmax"][0]
plot_style = input["plot_style"][0]
nbins = input["nbins"][0]
single_colorbar = input["single_colorbar"][0]
scale_bar_position = input["scale_bar_position"][0]
single_colorbar_above = input["single_colorbar_above"][0]
if not single_colorbar:
    single_colorbar_above = False
    print(
        f"single_colorbar set to {single_colorbar} so setting single_colorbar_above to {single_colorbar_above}"
    )
single_xlabel = input["single_xlabel"][0]
single_ylabel = input["single_ylabel"][0]
plot_images = input["plot_images"][0]
show_histograms = input["show_histograms"][0] if input["show_histograms"][0] else ""
print(f"show_histograms: {show_histograms}")
averaging_range = input["averaging_range"][0]
histogram_average_style = input["histogram_average_style"][0]
if histogram_average_style == "default":
    if averaging_range is None:
        histogram_average_style = None
    else:
        histogram_average_style = "mean"
SETUPS = input["setup"]
histogram_differential = input["histogram_differential"][0]
paper = input["paper"][0]
snap_number = input["numbers"]
vary_along_x = input["vary_along_x"][0]
vary_along_y = input["vary_along_y"][0]
numbers_min = input["numbers_min"][0]
numbers_max = input["numbers_max"][0]
numbers_n = input["numbers_n"][0]
fontsize = input["fontsize"][0]
plot_color_as_circle = input["plot_color_as_circle"][0]
test = input["test"][0]
heightwidth = input["heightwidth"] if input["heightwidth"] != [None] else None
scale_bar = input["scale_bar"][0]

if (numbers_min or numbers_min == 0) and numbers_max != False:
    snap_number = np.linspace(numbers_min, numbers_max, numbers_n)


if not vary_along_y:
    vary_along_y_objects = ""
elif vary_along_y == "snapshot_number":
    vary_along_y_objects = snap_number
elif vary_along_y == "folder":
    vary_along_y_objects = folders
else:
    raise Exception(f"can't vary_along_y {vary_along_y}!")
if not vary_along_x:
    vary_along_x_objects = ""
elif vary_along_x == "snapshot_number":
    vary_along_x_objects = snap_number
elif vary_along_x == "folder":
    vary_along_x_objects = folders
else:
    raise Exception(f"can't vary_along_x {vary_along_x}!")

for setup in SETUPS:
    dpi = 200
    colors = ["lightcoral", "royalblue", "limegreen", "sienna", "plum", "lightgray"]
    colors_1 = ["#d1ddeb", "#7599c3", "#19569b", "#14447c", "#0c2b4d", "#02080f"]
    colors_2 = ["#f9b5b5", "#f15959", "#f04747", "#902a2a", "#601c1c", "#300e0e"]
    colors = [colors, colors_1, colors_2]
    variable = "VelocityX"
    # scale_bar = 10
    res_depth = 16384
    save_individual_histograms = False
    res = 510
    alpha = 0.4
    cmap = "RdBu_r"  # "RdBu_r" plasma
    info = {}
    contour_colors = colors[:]
    max = -200  # -2e3 #6e2
    min = 200  # 2e3 #0
    vmin = min
    vmax = max
    plot_type = "slice"
    weight = (None,)
    symmetric_zero = False
    plot_mask_as_contour = False
    follow_jet = False
    histogram_show_legend = False
    histogram_ignore_zeros = False
    include_outlier_bins = False
    colorbar_vertical = False
    colormap_show_ylabel_always = True
    logX = True
    logY = True
    contour_levels = [1e0]  # thresholds
    plot_mask_as_contour = True
    fit = None
    colormap_ylim = [None, None]
    cmap_log = True
    plot_contour = True
    height = width = 40
    assert height == width, "Fig.prepare_mesh_plot expect height==width"
    depth = 4000
    select = None
    use_color_gradient_along = None
    histogram_normalize = None
    if setup == "temperatures_around_bh":
        variable = "Temperature"
        variable_latex = "\\boldsymbol{T}"
        variable_units_latex = "\\mathrm{K}"
        histogram_weight_variable = "MassInSolar"
        if not histogram_differential:
            colormap_ylim = [1e7, 2e10]
        else:
            colormap_ylim = [None, None]
        histogram_ylabel_variable = "\\Sigma\\mathrm{M}"
        histogram_ylabel_units = "\\mathrm{M_\\odot}"
        mindefault = 1e4  # 2e3 #0
        maxdefault = 1e8  # -2e3 #6e2
        vmindefault = mindefault
        vmaxdefault = maxdefault
        values_variables = "DistanceToCenterInKpc"
        values_latex = "R"  # "X_\mathrm{trc}"
        values_units_latex = "\\mathrm{kpc}"
        values_borders = [0, 10]  # , 20]  # np.linspace(10, 50, 3)
        cmap = "RdBu_r"  # "RdBu_r" plasma
        contour_color = "values"  # "mono"
        histogram_group_by = "valuessnapshot_numberfolder"  # "snapshot_number" "folder"
    elif setup == "jettracer":
        variable = "JetTracer"
        variable_latex = "{X}_\\mathrm{jet}"
        variable_units_latex = ""
        histogram_weight_variable = "MassInSolar"
        if not histogram_differential:
            colormap_ylim = [None, None]
        else:
            colormap_ylim = [None, None]
        histogram_ylabel_variable = "\\Sigma\\mathrm{M}"
        histogram_ylabel_units = "\\mathrm{M_\\odot}"
        mindefault = 1e-6  # 2e3 #0
        maxdefault = 1  # -2e3 #6e2
        vmindefault = mindefault
        vmaxdefault = maxdefault
        values_variables = "DistanceToCenterInKpc"
        values_latex = "R"  # "X_\mathrm{trc}"
        values_units_latex = "\\mathrm{kpc}"
        values_borders = [0, 10]  # , 20]  # np.linspace(10, 50, 3)
        cmap = "RdBu_r"  # "RdBu_r" plasma
        contour_color = "values"  # "mono"
        histogram_group_by = "valuessnapshot_numberfolder"  # "snapshot_number" "folder"
    elif setup == "filamentvelocityold":
        variable = "AbsoluteVelocity"
        variable_latex = "v_\\mathrm{fil}"
        variable_units_latex = ""
        select = "Temperature<1e6ORStarFormationRate>0"
        select = "Temperature<2e4ORStarFormationRate>0"
        histogram_weight_variable = "MassInSolar"
        if not histogram_differential:
            colormap_ylim = [None, None]
        else:
            colormap_ylim = [None, None]
        histogram_ylabel_variable = "\\mathrm{bincount}\\ \\mathrm{normalized}"
        histogram_ylabel_units = ""
        histogram_normalize = True
        mindefault = None  # 2e3 #0
        maxdefault = None  # -2e3 #6e2
        vmindefault = mindefault
        vmaxdefault = maxdefault
        values_variables = "DistanceToCenterInKpc"
        values_latex = "R"  # "X_\mathrm{trc}"
        values_units_latex = "\\mathrm{kpc}"
        height = width = 80
        values_borders = [0, 40]  # , 20]  # np.linspace(10, 50, 3)
        cmap = "RdBu_r"  # "RdBu_r" plasma
        contour_color = "values"  # "mono"
        histogram_group_by = "valuessnapshot_numberfolder"  # "snapshot_number" "folder"
    elif (
        setup == "filamentepsilon"
        or setup == "filamentangularmomentum"
        or setup == "filamentvelocity"
        or setup == "filamentvelocitylos"
        or setup == "filamentnonradial"
        or setup == "angularmomentumglobal"
    ):
        res = 1024  # 300
        if TEST:
            res = 100
        mindefault = None  # 2e3 #0
        maxdefault = None  # -2e3 #6e2
        weight = "Mass"
        cmap = "RdBu_r"  # "RdBu_r" plasma
        plot_type = "projection"
        if setup == "filamentepsilon":
            variable = "AngularMomentumEpsilon"
            variable_latex = "\\epsilon_\\mathrm{fil}"
            variable_units_latex = ""
            mindefault = 1e0  # 2e3 #0
            maxdefault = 1e4  # -2e3 #6e2
        elif setup == "filamentvelocity":
            variable = "AbsoluteVelocity_unitsV:kms"
            variable_latex = "|v|_\\mathrm{fil}"
            variable_units_latex = "\\mathrm{km}\\,\\mathrm{s}^{-1}"
            mindefault = 1e1  # 2e3 #0
            maxdefault = 1e3  # -2e3 #6e2
            cmap = "magma"  # "RdBu_r" plasma
        elif setup == "filamentvelocitylos":
            variable = "VelocityY_unitsV:kms"
            variable_latex = "|v|_\\mathrm{fil,los}"
            variable_units_latex = "\\mathrm{km}\\,\\mathrm{s}^{-1}"
            mindefault = -5e2  # 2e3 #0
            maxdefault = 5e2  # -2e3 #6e2
            logX = False
            histogram_ignore_zeros = True
            cmap_log = False
            cmap = "RdBu_r"  # "RdBu_r" plasma
        elif setup == "filamentnonradial":
            variable = "NonRadialVelocityComponent"
            variable_latex = "|\\hat{r}\\times\\hat{v}|_\\mathrm{fil}"
            variable_units_latex = ""
            mindefault = 0  # 2e3 #0
            maxdefault = 1  # -2e3 #6e2
            logX = cmap_log = False
            histogram_ignore_zeros = True
            cmap = "RdBu_r"  # "RdBu_r" plasma
        elif setup == "angularmomentumglobal":
            variable = "ABSAngularMomentumGlobalNormalized"
            # variable_latex = "|(\\Sigma[\\hat{r}\\times\\hat{v}])_\\mathrm{fil}\\cdot(\\hat{r}\\times\\hat{v})_\\mathrm{fil}|"
            variable_latex = "\\left|{\\bm{\\hat{L}}}_\\mathrm{fil}\\cdot \\bm{\\hat{l}}_\\mathrm{fil}\\right|"
            variable_units_latex = ""
            mindefault = 0  # 2e3 #0
            maxdefault = 1  # -2e3 #6e2
            logX = cmap_log = False
            plot_mask_as_contour = False
            histogram_ignore_zeros = True
            colormap_ylim = [0, 5.3]
            cmap = "magma_r"  # "RdBu_r" plasma
            logY = False
            use_color_gradient_along = "y"
        else:
            variable = "TotalSpecificAngularMomentumKpcKms"
            variable_latex = "l"
            variable_units_latex = "\\mathrm{kpc}\\,\\mathrm{km}\\,\\mathrm{s}^{-1}"
        select = "Temperature<2e4ORStarFormationRate>0"
        histogram_weight_variable = "MassInSolar"
        histogram_ylabel_variable = "\\mathrm{PDF}"
        histogram_ylabel_units = ""
        histogram_normalize = True
        vmindefault = mindefault
        vmaxdefault = maxdefault
        values_variables = "DistanceToCenterInKpc"
        values_latex = "R"  # "X_\mathrm{trc}"
        values_units_latex = "\\mathrm{kpc}"
        height = width = 50
        depth = height
        res_depth = 4096
        if test:
            plot_type = "slice"
            plot_type = "projection"
            height = width = depth = 80
            # plot_type = "projection"
            res = 100
            res_depth = res
            # select = "StarFormationRate>0"
            # histogram_weight_variable = None
        values_borders = [0, 50]  # , 20]  # np.linspace(10, 50, 3)
        contour_color = "values"  # "mono"
        if show_histograms:
            histogram_group_by = "valuessnapshot_numberfolder"  # "snapshot_number" "folder"
        else:
            histogram_group_by = ""
    elif setup == "accreting_gas":
        colormap_ylim = [1e5, 1e8]
        colormap_ylim = [None, None]
        if TEST:
            info["AccGasColdGasThresholdInKelvin"] = 1e7
            info["AccGasUseSfrCrit"] = False
        else:
            info["AccGasColdGasThresholdInKelvin"] = "ChaoticAccretionThreshU"
            info["AccGasUseSfrCrit"] = True
        variable = "Temperature"
        variable_latex = "\\boldsymbol{T}"
        variable_units_latex = "\\mathrm{K}"
        histogram_weight_variable = "MassInSolar"
        histogram_ylabel_variable = "\\Sigma\\mathrm{M}"
        histogram_ylabel_units = "\\mathrm{M_\\odot}"
        vmindefault = 1e4
        vmaxdefault = 1e8
        mindefault = 1e1  # 2e3 #0
        maxdefault = 1e6  # 1e5  # -2e3 #6e2
        height = width = 20
        depth = 4000
        values_variables = "TemperatureACC"
        values_latex = "T"  # "X_\mathrm{trc}"
        values_units_latex = "\\mathrm{K}"
        values_borders = [0, 1e10]  # , 20]  # np.linspace(10, 50, 3)
        cmap = "RdBu_r"  # "RdBu_r" plasma
        contour_color = "values"  # "mono"
        histogram_group_by = "valuessnapshot_numberfolder"  # "snapshot_number" "folder"
    else:
        raise Exception(f"setup {setup} not implemented")
    if heightwidth is not None:
        if len(heightwidth) == 1:
            height = width = heightwidth[0]
        elif len(heightwidth) == 2:
            height, width = heightwidth
        else:
            raise Exception(f"heightwidth not 2 or 1 arguments!: heightwidth: {heightwidth}")
    if scale_bar >= width:
        warnings.warn(f"reducing scalebar such that scale_bar > width, now {scale_bar} > {width}, new scale_bar:width/2: {width/2}")
        scale_bar = int(width/2)
    box_3d = [[] for i in range(3)]
    box_3d[axis[0]] = height
    box_3d[axis[1]] = width
    box_3d[3 - sum(axis)] = depth
    box_2d = [[] for i in range(2)]
    box_2d[0] = box_3d[axis[0]]
    box_2d[1] = box_3d[axis[1]]
    if xmin == "default":
        min = mindefault  # 2e3 #0
    else:
        min = xmin
    if xmax == "default":
        max = maxdefault  # -2e3 #6e2
    else:
        max = xmax
    if vvmin == "default":
        vmin = vmindefault
    else:
        vmin = vvmin
    if vvmax == "default":
        vmax = vmaxdefault
    else:
        vmax = vvmax
    save_folder = f"../figures/histogram_plots/{setup}/"
    filename = f"{values_variables}_{variable}_W{aux.cps(histogram_weight_variable)}_{aux.convert_params_to_string(folders_name_save, reduce_same_starting_string=True)}_Res{res}_ReDe{res_depth}_Box{aux.cps(box_3d)}_Ax{aux.cps(axis)}"
    filename += (
        f"_Tie{aux.cps(snap_number)}_Hdi{aux.cps(histogram_differential)}_Nb{nbins}"
    )
    if include_outlier_bins:
        filename += "_HiIncOutl"
    if histogram_normalize:
        filename += f"_HiNo{histogram_normalize}"
    if averaging_range is not None:
        filename += f"_AVRang{histogram_average_style}{averaging_range}"
    if not show_histograms:
        filename += f"_NoHIST" 
    filename = aux.shortenFileName(filename)
    filename += ".pdf"
    print(f"... working on {filename}")
    if len(os.path.basename(filename)) > 255:
        raise Exception(f"filename too long ({len(os.path.basename(filename))} > 255)!")
    filename = save_folder + filename
    print(f".. working on {filename}!")
    eff_length_values = len(values_borders) - 1
    font = {"family": "serif", "serif": ["Arial"]}
    mpl.rc("text", usetex=True)
    mpl.rc("text.latex", preamble=[r"\usepackage{amsmath}\usepackage{bm}"])
    mpl.rcParams["font.size"] = fontsize  # * figsizex/3
    mpl.rc("font", **font)
    wspace = 0
    hspace = 0
    length_colorbar_horizontal_percent = length_colorbar_vertical_percent = 0.8
    colorbar_horizontal_offset = colorbar_vertical_offset = 0.02
    width_colorbar_horizontal = width_colorbar_vertical = 0.04
    if (vary_along_y and vary_along_x) and not colorbar_vertical:
        wspace = 0.014
        width_colorbar_horizontal = 0.01
        hspace = 0.009  # 0.045
        length_colorbar_horizontal_percent = 0.74
        colorbar_horizontal_offset = 0.005
    elif (vary_along_y or vary_along_x) and not colorbar_vertical:
        wspace = 0.02
        width_colorbar_horizontal = 0.01
        hspace = 0.045
        length_colorbar_horizontal_percent = 0.74
        colorbar_horizontal_offset = 0.005
    elif (vary_along_y or vary_along_x) and colorbar_vertical:
        wspace = 0.14
        width_colorbar_vertical = 0.03
        hspace = 0.02
        # length_colorbar_vertical_percent = 0.74
        colorbar_vertical_offset = 0.005
    histogram_number_x = 0
    histogram_number_y = 0
    if vary_along_y and vary_along_y in histogram_group_by:
        if "values" in histogram_group_by:
            histogram_number_y += 1
        else:
            histogram_number_y += len(vary_along_y_objects) * eff_length_values
    if vary_along_x and vary_along_x in histogram_group_by:
        if "values" in histogram_group_by:
            histogram_number_x += 1
        else:
            histogram_number_x += len(vary_along_x_objects) * eff_length_values
    skip_colorbars = [
        [
            "all",
            f"{len(vary_along_y_objects)}:{len(vary_along_y_objects)+histogram_number_y}",
        ],
        [
            f"{len(vary_along_x_objects)}:{len(vary_along_x_objects)+histogram_number_x}",
            "all",
        ],
    ]
    show_colorbar = True
    show_colorbar_above = False
    colorbar_multi_row_column = True
    colorbar_above_offset = 0
    length_colorbar_above_percent = 1
    width_colorbar_above = 0.016
    top = 0.02
    if single_colorbar_above:
        width_colorbar_above = 0.012
        length_colorbar_above_percent = 1
        colorbar_above_offset = 0.01
        show_colorbar = False
        colorbar_multi_row_column = False
        show_colorbar_above = True
        if show_histograms:
            skip_colorbars = [-1]
        else:
            skip_colorbars = []
        top = 0.12
    print(f"histogram_number_x: {histogram_number_x}")
    print(f"histogram_number_y: {histogram_number_y}")
    print(f"vary_along_x_objects: {vary_along_x_objects}")
    print(f"histogram_number_x: {histogram_number_x}")
    # exit()
    skip_axes = None
    if vary_along_y and vary_along_x and show_histograms:
        skip_axes = [[-1, -1]]
    folders = [x + y for x, y in zip(roots, folders)]
    fig, axs, axes_colorbar, axes_colorbar_above = AxesGeneration.getAxesList(
        [
            [[box_2d[0], box_2d[1]]]
            * (
                len(vary_along_y_objects)
                + 1
                - 1 * bool(vary_along_y)
                + histogram_number_y
            )
        ]
        * (len(vary_along_x_objects) + 1 - 1 * bool(vary_along_x) + histogram_number_x),
        width_colorbar_horizontal=width_colorbar_horizontal,
        width_colorbar_vertical=width_colorbar_vertical,
        length_colorbar_vertical_percent=length_colorbar_vertical_percent,
        length_colorbar_horizontal_percent=length_colorbar_horizontal_percent,
        length_colorbar_above_percent=length_colorbar_above_percent,
        width_colorbar_above=width_colorbar_above,
        skip_colorbars=skip_colorbars,
        hspace=hspace,
        right=0.02,
        left=0.02,
        bottom=0.12,
        top=top,
        colorbar_vertical_offset=colorbar_vertical_offset,
        colorbar_horizontal_offset=colorbar_horizontal_offset,
        colorbar_above_offset=colorbar_above_offset,
        wspace=wspace,
        figsizex="A4",
        skip_axes=skip_axes,
        colorbarVertical=colorbar_vertical,
        SingleColorbar=False,
        ShowAxisTicks=True,
        show_colorbar_above=show_colorbar_above,
        show_colorbar=show_colorbar,
        single_colorbar_above=single_colorbar,
        dpi=dpi,
        colorbar_multi_row_column=colorbar_multi_row_column,
    )
    plt.show()

    print(axs)
    print(np.shape(axs))
    print(axes_colorbar)
    # exit()
    DATA = collections.OrderedDict()
    print(range(len(vary_along_y_objects)))
    number_plots_x = len(vary_along_x_objects) + 1 - 1 * bool(vary_along_x)
    number_plots_y = len(vary_along_y_objects) + 1 - 1 * bool(vary_along_y)
    print(f"number_plots_x: {number_plots_x}")
    print(f"number_plots_y: {number_plots_y}")
    for indexX in range(number_plots_x):
        for indexY in range(number_plots_y):
            index_snapshot_number = 0
            index_folder = 0
            if plot_images:
                plottings = [True]
            else:
                plottings = [False]
            if vary_along_x == "snapshot_number":
                index_snapshot_number = indexX
            elif vary_along_x == "folder":
                index_folder = indexX
            if vary_along_y == "snapshot_number":
                index_snapshot_number = indexY
            elif vary_along_y == "folder":
                index_folder = indexY
            numbers = [snap_number[index_snapshot_number]]
            if averaging_range is not None:
                print(
                    f"snap_number[index_snapshot_number]: {snap_number[index_snapshot_number]}"
                )
                number_to_plot = Fig.getSnapNumber(
                    folders[index_folder],
                    snap_number[index_snapshot_number],
                    returnTimeAndNumber=True,
                    differenceTime=4,
                )[1]
                numbers = list(
                    Fig.get_times_in_range(
                        folders[index_folder],
                        snap_number[index_snapshot_number] - averaging_range / 2.0,
                        snap_number[index_snapshot_number] + averaging_range / 2.0,
                    )
                )
                # if number_to_plot not in numbers:
                #     raise Exception(f"couldnt find number_to_plot ({number_to_plot}) in {numbers} to plot!")
                plottings = np.zeros_like(numbers, dtype=np.int)
                plottings[
                    np.argmin(
                        np.abs(np.array(numbers) - snap_number[index_snapshot_number])
                    )
                ] = 1
                print(f"plottings: {plottings}")
                print(f"numbers: {numbers}")
            for index_number, (plotting, number) in enumerate(zip(plottings, numbers)):
                snap = Fig.quickImport(
                    number,
                    folder=folders[index_folder],
                    useTime=True,
                    differenceTime=4,
                )
                if res_depth is None:
                    res_depth = aux.minimum_resolution(snap.boxsize, snap)
                data = aux.get_value(variable, snap, INFO=info)
                debug.printDebug(data, "data")
                print(f"variable: {variable}")
                weight_data = None
                if weight is not None:
                    weight_data = aux.get_value(
                        variable, snap, INFO=info, convert_units=False
                    )
                if select is not None:
                    print(f"select: {select}")
                    data_new = np.zeros_like(data)
                    selection = np.where(aux.get_selection(select, snap))[0]
                    debug.printDebug(selection, "selection", zero=True)
                    data_new[selection] = data[selection]
                    data = data_new[:]
                    if weight is not None:
                        weight_data_new = np.zeros_like(weight_data)
                        weight_data_new[selection] = weight_data[selection]
                        weight_data = weight_data_new[:]
                        snap.data[weight] = weight_data
                snap.data[variable] = data
                debug.printDebug(snap.data[variable], "snap.data[variable]")
                if histogram_weight_variable is not None:
                    snap.data[histogram_weight_variable] = aux.get_value(
                        histogram_weight_variable, snap, convert_units=False
                    )
                # ---------------- plot data -------------------------
                if plotting:
                    box_3d, resolution_3d = Fig.prepare_mesh_plot(
                        height,
                        resolution=res,
                        depth=depth,
                        res_depth=res_depth,
                        axis=axis,
                    )
                    data_plot, contour_data = Fig.get_mesh_data(
                        snap,
                        axis,
                        box_3d,
                        variable,
                        resolution_3d,
                        snap.center,
                        plot_type=plot_type,
                        reimportStrVar=False,
                        contour=False,
                        weight=weight,
                        numthreads=numthreads,
                    )
                    vmin, vmax = Fig._getVminVmax(
                        vmin, vmax, None, None, data_plot, "None", []
                    )
                    axes = axs[indexX][indexY]
                    print(f"axs: {axs}")
                    print(f"axes: {axes}")
                    debug.printDebug(data_plot, "data_plot")
                    image = Fig.plot_pcolormesh(
                        axes,
                        data_plot,
                        aux.convert_list_from_kpc_to_code(snap, box_3d),
                        axis,
                        res,
                        res,
                        cmap=cmap,
                        log=cmap_log,
                        vmin=vmin,
                        vmax=vmax,
                        arepo_positions=True,
                        center=snap.center,
                    )
                    if indexX == 0 and indexY == 0:
                        extent = Fig.get_Extent(
                            snap.center,
                            axis,
                            aux.convert_list_from_kpc_to_code(snap, box_3d),
                        )
                        Fig._scale_bar(
                            axes,
                            extent,
                            scale_bar=scale_bar
                            * param.KiloParsec_in_cm
                            / snap.UnitLength_in_cm,
                            scale_bar_label=r"$%.0f\,\mathrm{kpc}$" % (scale_bar),
                            scale_bar_position=scale_bar_position,
                            color=text_color,
                            scale_bar_show_label=True,
                            text_style="",
                            fontsize=fontsize,
                            linewidth=1.5,
                        )
                    if (
                        (vary_along_x == "folder" and indexY == 0)
                        or (vary_along_y == "folder" and indexX == 0)
                        or (indexX == 0 and indexY == 0)
                    ):
                        text = r"%s" % folders_name_short[index_folder]
                        Fig._text_time_folder(
                            axes,
                            text=text,
                            text_color=text_color,
                            text_position="topleft",
                        )
                    if (
                        (vary_along_x == "snapshot_number" and indexY == 0)
                        or (vary_along_y == "snapshot_number" and indexX == 0)
                        or (indexX == 0 and indexY == 0)
                    ):
                        text = "$%i\, \mathrm{Myr}$" % number
                        if paper:
                            text = "$%i\, \mathrm{Myr}$" % round(number, -1)
                        Fig._text_time_folder(
                            axes,
                            text=text,
                            text_color=text_color,
                            text_position="bottomright",
                        )
                    if single_colorbar_above:
                        axes_cb = axes_colorbar_above[0]
                        print(f"axes_colorbar_above: {axes_colorbar_above}")
                    else:
                        axes_cb = axes_colorbar[indexX][indexY]
                    if vary_along_y and indexY + 1 != len(vary_along_y_objects):
                        show_label = False
                    elif vary_along_x and vary_along_y:
                        show_label = False
                    else:
                        show_label = True
                    if single_colorbar:
                        show_label = True
                    print(f"show_label: {show_label}")
                    print(f"colorbar_vertical: {colorbar_vertical}")
                    print(f"axes_cb: {axes_cb}")
                    print(f"image: {image}")
                    Fig.plot_colorbars(
                        axes_cb,
                        image,
                        data_plot,
                        max_tick_labels=5,
                        vmin=vmin,
                        vmax=vmax,
                        log=cmap_log,
                        colorbar_vertical=colorbar_vertical,
                        variable_name=variable_latex,
                        variable_name_units=variable_units_latex,
                        show_label=show_label,
                        fontsize=fontsize,
                        ticks_above=single_colorbar_above,
                        label_above=single_colorbar_above,
                    )
                    print(f"xticks: {list(axes.xaxis.get_ticklocs())}")
                    print(f"yticks: {list(axes.yaxis.get_ticklocs())}")
                    axes.get_xaxis().set_ticks([])
                    axes.get_yaxis().set_ticks([])
                if follow_jet:
                    center = np.average(
                        (snap.pos[snap.type == 0])[
                            np.logical_and(
                                snap.jetr > 1e-3,
                                snap.pos[:, 0][snap.type == 0] > snap.center[0],
                            )
                        ],
                        axis=0,
                    )
                    info["center"] = center
                borders = values_borders[:]  # np.linspace(0, 50, 6)
                borders = [[x, y] for x, y in zip(borders[:-1], borders[1:])]
                for index_rad, bd in enumerate(borders):
                    values = aux.get_value(values_variables, snap, INFO=info)
                    mask = np.logical_and(values > bd[0], values < bd[1])
                    d = (snap.data[variable][mask]).flatten()
                    w = None
                    if histogram_weight_variable is not None:
                        w = (snap.data[histogram_weight_variable][mask]).flatten()
                    AlongColorGradient = False
                    color_type = "bright"
                    color_type = "mute"
                    if use_color_gradient_along == "x":
                        AlongColor = 0
                    if use_color_gradient_along == "y":
                        AlongColor = 1
                    else:
                        color_type = "mute"
                        AlongColor = 2
                        AlongColorGradient = "01"
                    c = Fig.getColor(
                        indexX,
                        indexY,
                        index_rad,
                        number_plots_x,
                        number_plots_y,
                        eff_length_values,
                        useGradient=True,
                        AlongColor=AlongColor,
                        AlongColorGradient=AlongColorGradient,
                        color=None,
                        colorstart=None,
                        colorfinish=None,
                        color_type=color_type,
                    )
                    if index_number == 0:
                        dic = {}
                        dic["data"] = []
                        dic["weights"] = []
                    else:
                        dic = DATA[f"{indexX}|{indexY}|{index_rad}"]
                    dic["index_folder"] = index_folder
                    dic["index_snapshot_number"] = index_snapshot_number
                    dic["border"] = bd
                    dic["data"].append(d)
                    dic["indexX"] = indexX
                    dic["indexY"] = indexY
                    dic["index_rad"] = index_rad
                    dic["weights"].append(w)
                    dic["color"] = c
                    DATA[f"{indexX}|{indexY}|{index_rad}"] = dic
                    if plot_color_as_circle and show_histograms and plotting:
                        loc = [snap.center[axis[0]], snap.center[axis[1]]]
                        radius = height * 0.03
                        # should locate dot in lower left corner
                        loc[0] -= width * 0.5 - radius - width * 0.03
                        loc[1] -= height * 0.5 - radius - height * 0.03
                        circle = plt.Circle(tuple(loc), radius, color=c)
                        axes.add_artist(circle)
                    if plotting and plot_contour and plot_mask_as_contour:
                        snap.data["contour_variable"] = np.zeros_like(mask)
                        snap.data["contour_variable"][mask] = 1
                        data_contour = snap.get_Aslice(
                            "contour_variable",
                            res=res,
                            center=False,
                            axes=axis,
                            box=aux.convert_list_from_kpc_to_code(snap, box_2d),
                            numthreads=16,
                        )["grid"].T
                        contour = aux.get_contour_data(
                            data_contour,
                            axis,
                            aux.convert_list_from_kpc_to_code(snap, box_2d),
                            center=snap.center,
                            filter=None,
                            filter_num=None,
                            max_value=None,
                            info=None,
                            return_dic=True,
                        )
                        contour_image = axes.contour(
                            contour["X"],
                            contour["Y"],
                            contour["Z"],
                            contour_levels,
                            colors=c,
                            linewidths=1,
                        )
    # assign histogram plots to corresponding axes
    hist_plots = {}
    print("DATA")
    print(DATA)
    print(DATA.keys())
    for k, item in DATA.copy().items():
        indexX, indexY, index_rad = item["indexX"], item["indexY"], item["index_rad"]
        print(f"indexX: {indexX}")
        if vary_along_y and vary_along_y in histogram_group_by:
            if "values" in histogram_group_by:
                Y = len(vary_along_y_objects)
            else:
                Y = len(vary_along_y_objects) + indexY * eff_length_values + index_rad
            X = indexX
            print(f"X,Y: {X}{Y}")
            if f"{X}|{Y}" not in hist_plots.keys():
                hist_plots[f"{X}|{Y}"] = {}
                hist_plots[f"{X}|{Y}"]["hist_dics"] = []
                hist_plots[f"{X}|{Y}"]["indexX"] = X
                hist_plots[f"{X}|{Y}"]["indexY"] = Y
            hist_plots[f"{X}|{Y}"]["hist_dics"].append(item)
        if vary_along_x and vary_along_x in histogram_group_by:
            Y = indexY
            if "values" in histogram_group_by:
                X = len(vary_along_x_objects)
            else:
                X = len(vary_along_x_objects) + indexX * eff_length_values + index_rad
            if f"{X}|{Y}" not in hist_plots.keys():
                hist_plots[f"{X}|{Y}"] = {}
                hist_plots[f"{X}|{Y}"]["hist_dics"] = []
                hist_plots[f"{X}|{Y}"]["indexX"] = X
                hist_plots[f"{X}|{Y}"]["indexY"] = Y
            hist_plots[f"{X}|{Y}"]["hist_dics"].append(item)

    # calculate bin edges for all histogram plots
    print(f"min,max: {min}, {max}")
    print(f"hist_plots.items(): {hist_plots.items()}")
    for K, ITEM in hist_plots.items():
        print(ITEM.values())
        print(
            f'[np.array(x["data"]).flatten() for x in ITEM["hist_dics"]]: {[np.array(x["data"]).flatten() for x in ITEM["hist_dics"]]}'
        )
        bin_edges = Fig.get_Bins(
            Data=aux.flattenList(
                [np.array(x).flatten() for y in ITEM["hist_dics"] for x in y["data"]]
            ),
            Range=None,
            Nbins=nbins,
            log=logX,
            min=min,
            max=max,
            returnCenters=False,
            minNonZero=True,
            sameNumberPointsPerBin=False,
            prevent_same_bins=True,
            symmetric_zero=symmetric_zero,
        )
        print(ITEM["indexX"], ITEM["indexY"])
        indexX, indexY = ITEM["indexX"], ITEM["indexY"]
        ax = axs[indexX][indexY]
        for hist_dic_data in ITEM["hist_dics"]:
            print(f'hist_dic_data["data"]: {hist_dic_data["data"]}')
            index_values = hist_dic_data["index_rad"]
            index_folder = hist_dic_data["index_folder"]
            index_snapshot_number = hist_dic_data["index_snapshot_number"]
            if np.count_nonzero(aux.flattenList(hist_dic_data["data"])) == 0:
                print(f"all zeros: bin_edges: {bin_edges}")
            d = hist_dic_data["data"]
            w = hist_dic_data["weights"]
            if histogram_average_style is None:
                d = d[0]
                w = w[0]
            hist_dic = Fig.getHistogram(
                d,
                weights=w,
                binNumber=60,
                binLog=False,
                binRange=None,
                fit=fit,
                plot=True,
                plotSave=save_individual_histograms,
                plotFilename=f"../figures/hists/test_{folders_name_save[index_folder]}.pdf",
                plotdpi=150,
                plotColors=["lightcoral", "royalblue"],
                plotFontsize=fontsize,
                plotShow=True,
                bin_edges=bin_edges,
                normalize=histogram_normalize,
                include_outlier_bins=include_outlier_bins,
                ignore_zeros=histogram_ignore_zeros,
                average_style=histogram_average_style,
            )
            values_strings = (
                aux.latex_float(
                    values_borders[index_values], force_exponents=True, avoid_base=True,
                ),
            )
            values_strings = str(int(values_borders[index_values]))
            text_folder = folders_name_short[index_folder]
            if fit is not None:
                label = (
                    "$%s\!=\!%s\,%s,%s\,\sigma\!=\!%s\,\mathrm{km}\,\mathrm{s}^{-1}$"
                    % (
                        values_latex,
                        values_strings,
                        values_units_latex,
                        text_folder,
                        aux.latex_float(hist_dic["paras"][2]),
                    )
                )
            else:
                label = "$%s\!=\!%s\,%s,%s$" % (
                    values_latex,
                    values_strings,
                    values_units_latex,
                    text_folder,
                )
            print(f"histogram_differential {histogram_differential}")
            if histogram_differential:
                hist_y = hist_dic["hist"] / np.diff(hist_dic["bin_edges"])
                if variable_units_latex:
                    histogram_ylabel = "%s/%s\\,[%s/%s]" % (
                        histogram_ylabel_variable,
                        variable_latex,
                        histogram_ylabel_units,
                        variable_units_latex,
                    )
                else:
                    histogram_ylabel = "%s/%s" % (
                        histogram_ylabel_variable,
                        variable_latex,
                    )
            else:
                hist_y = hist_dic["hist"]
                histogram_ylabel = "%s" % (histogram_ylabel_variable)
                if histogram_ylabel_units:
                    histogram_ylabel += "\\,[%s]" % (histogram_ylabel_units)
            if plot_style == "bar":
                ax.bar(
                    hist_dic["bins"],
                    hist_y,
                    width=np.diff(hist_dic["bin_edges"]),
                    color=hist_dic_data["color"],
                    edgecolor="black",
                    label=label,
                    alpha=alpha,
                )
            elif plot_style == "line":
                ax.plot(
                    hist_dic["bins"],
                    hist_y,
                    color=hist_dic_data["color"],
                    label=label,
                    alpha=0.86,
                )
            else:
                raise Exception(f"plot_style: {plot_style} not known!")
            if hist_dic["err"] is not None:
                ax.fill_between(
                    hist_dic["bins"],
                    hist_dic["err"][1],
                    hist_dic["err"][0],
                    color=hist_dic_data["color"],
                    label=None,
                    alpha=alpha,
                )
            if fit == "Gaussian":
                paras, perror, fitfunction = Fit.fitSinglePeak(
                    hist_dic["bins"],
                    hist_dic["hist"] / np.diff(hist_dic["bin_edges"]),
                    hist_dic["hist"] / np.diff(hist_dic["bin_edges"]),
                    returnFitFunction=True,
                )
                x = np.linspace(
                    np.min(hist_dic["bins"]), np.max(hist_dic["bins"]), 1000
                )
                ax.plot(
                    x,
                    fitfunction(x, *paras),
                    color=colors_1[index_values],
                    alpha=alpha * 1.5,
                    path_effects=[
                        pe.Stroke(linewidth=2, foreground="black", alpha=alpha),
                        pe.Normal(),
                    ],
                )

        if logX:
            ax.set_xscale("log")
        if logY:
            ax.set_yscale("log")
        if (
            vary_along_x and indexX < len(vary_along_x_objects) - 1
        ) and not colormap_show_ylabel_always:
            ax.get_yaxis().set_ticks([])
        elif single_ylabel and indexX < len(vary_along_x_objects) - 1:
            ax.yaxis.tick_right()
            ax.set_yticklabels([])
        else:
            ax.yaxis.tick_right()
            ax.yaxis.set_label_position("right")
            ax.set_ylabel(
                "$%s$" % histogram_ylabel, rotation=270, labelpad=fontsize * 1.3,
            )
        if mindefault is None:
            x_vmin = np.min(hist_dic["bins"])
        else:
            x_vmin = mindefault
        if maxdefault is None:
            x_vmax = np.max(hist_dic["bins"])
        else:
            x_vmax = maxdefault
        if colormap_ylim is None:
            ylim_cur = list(ax.get_ylim())
            ylim_cur[1] = 2 * ylim_cur[1]
        else:
            ylim_cur = colormap_ylim
        ax.set_ylim(ylim_cur)
        ax.set_xlim([x_vmin, x_vmax])
        offset = Fig.set_ticks(
            ax,
            axis="x",
            max_tick_labels=5,
            vmin=x_vmin,
            vmax=x_vmax,
            log=logX,
            power=2,
            format="%g",
        )
        if offset is None and variable_units_latex:
            xlabel = "$%s\,[%s]$" % (variable_latex, variable_units_latex)
        elif offset is None:
            xlabel = "$%s$" % (variable_latex)
        elif offset and not variable_units_latex:
            xlabel = "$%s\,[%s]$" % (variable_latex, offset)
        else:
            xlabel = "$%s\,[%s\,%s]$" % (variable_latex, offset, variable_units_latex)
        print(f"indexY: {indexY}")
        print(f"len(vary_along_y_objects): {len(vary_along_y_objects)}")
        if single_xlabel and indexY < len(vary_along_y_objects) + 1 - 2:
            ax.set_xticklabels([])
        else:
            ax.set_xlabel(xlabel)
        # ax.set_ylim(0, 40)
        if histogram_show_legend:
            ax.legend()
    Fig.savePlot(fig, filename=filename, dpi=dpi)
    print(f"finished plots at {datetime.datetime.now()}")
    print(
        f"plotting took: {'%i'%((time.time()-start_time)//60)} min  {'%.2f'%((time.time()-start_time)%60)} s"
    )
