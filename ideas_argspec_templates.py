"""

"""


"""
usage cases of p Matrix --args ...
"""

"""
template (string): NePthB, DensityCentral, ... 
variables, Weights (list of strings): ElectronNumberDensity, Pressure, ...
projections (list of int): 1, 0, 2
folders (list of strings): M15TNG0p001, PerseusE2E44My10, ../output/ ../../E2E44/output/ 
    -> test if string is folder -> otherwise use strings as names of folder
rotations (in Deg) (list of floats): 45 90 180
INFO??? -> variables include INFO!
vmin, vmax (list of floats): -1e3, 1e2, nan, default
    -> need to allow exponent 'e' and None? or nan?, defaults?, save default values for variable + weight + projection 
    in file and take values from there
colormap (list of strings): Spectral, jet
depth, depth_max (if e.g., projection=99), 
Hierarchy:
    -> template
        -> variables + weights
            -> colormap
            -> (vmin, vmax) 
                -> log
        -> (projbox) -> height, width, depth, depth_max
        -> Folder
"""


"""
#python multi_show_main.py --meta DensityMacro --height 1000 --width 1000 --variable TemperatureInkV 
    -> DensityMacro + TemperatureInkV
#python multi_show_main.py --meta DensityMacro --height 1000 --width 1000 --variable _TemperatureInkV --vmin None
    -> DensityMacro + TemperatureInkV, --vmin=None -> overwrites: vmin=None for all now
#python multi_show_main.py --meta DensityMacro --height 1000 --width 1000 --variable REP_TemperatureInkV --vmin ADD__None
    -> DensityMacro + TemperatureInkV, --vmin=None -> adds vmin=None only for variable
    


"""

# OFFSET "FIX"
# https://stackoverflow.com/questions/31517156/adjust-exponent-text-after-setting-scientific-limits-on-matplotlib-axis
def format_exponent(ax, axis="y"):

    # Change the ticklabel format to scientific format
    ax.ticklabel_format(axis=axis, style="sci", scilimits=(-2, 2))

    # Get the appropriate axis
    if axis == "y":
        ax_axis = ax.yaxis
        x_pos = 0.0
        y_pos = 1.0
        horizontalalignment = "left"
        verticalalignment = "bottom"
    else:
        ax_axis = ax.xaxis
        x_pos = 1.0
        y_pos = -0.05
        horizontalalignment = "right"
        verticalalignment = "top"

    # Run plt.tight_layout() because otherwise the offset text doesn't update
    plt.tight_layout()
    ##### THIS IS A BUG
    ##### Well, at least it's sub-optimal because you might not
    ##### want to use tight_layout(). If anyone has a better way of
    ##### ensuring the offset text is updated appropriately
    ##### please comment!

    # Get the offset value
    offset = ax_axis.get_offset_text().get_text()

    if len(offset) > 0:
        # Get that exponent value and change it into latex format
        minus_sign = u"\u2212"
        expo = np.float(offset.replace(minus_sign, "-").split("e")[-1])
        offset_text = r"x$\mathregular{10^{%d}}$" % expo

        # Turn off the offset text that's calculated automatically
        ax_axis.offsetText.set_visible(False)

        # Add in a text box at the top of the y axis
        ax.text(
            x_pos,
            y_pos,
            offset_text,
            transform=ax.transAxes,
            horizontalalignment=horizontalalignment,
            verticalalignment=verticalalignment,
        )
    return ax
