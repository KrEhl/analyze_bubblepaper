import auxiliary_functions as aux
import numpy as np
import FigureMove as Fig
import os


def get_iterations(list, parameterlist, input):
    elements_parameter = []
    dic = {}
    key = "too_early"
    print(list)
    for element in list:
        if type(element) == str and element in parameterlist:
            if elements_parameter:
                dic[key] = elements_parameter
            key = element
            elements_parameter = []
        else:
            initially_iterable = aux.isIterable(element)
            element = convert_element(element, input)
            iterable = aux.isIterable(element)
            if iterable and not initially_iterable:
                elements_parameter.extend(element)
            else:
                elements_parameter.append(element)
    dic[key] = elements_parameter
    if "too_early" in dic:
        dic = dic.pop("too_early")
    return dic


def convert_element(element, dic):
    print(element)
    if type(element) == str:
        if element.startswith("ITLIN"):
            element = element.replace("ITLIN", "")
            element = element.split(":")
            if "OP" in element[0] and int(element[1]) == -1:
                element[0] = element[0].replace("OP", "")
                element = get_numbers_minusone(
                    dic,
                    start_time=int(element[0]),
                    time_number=int(element[2]),
                    max_times=False,
                )
            elif int(element[1]) == -1:
                max_num = get_numbers_minusone(
                    dic, start_time=int(element[0]), max_times=True
                )
                print(f"max_num: {max_num}")
                element = [int(el) for el in element]
                element[1] = max_num
                element = list(np.linspace(*element))
            else:
                element = [int(el) for el in element]
                element = list(np.linspace(*element))
            if len(element) > 1:
                pass
        elif element.startswith("IT"):
            element = element.replace("IT", "")
            element = element.split(":")
            if "OP" in element[0] and int(element[1]) == -1:
                element[0] = element[0].replace("OP", "")
                element = get_numbers_minusone(
                    dic,
                    start_time=int(element[0]),
                    time_interval=int(element[2]),
                    max_times=False,
                )
            elif int(element[1]) == -1:
                max_num = get_numbers_minusone(
                    dic, start_time=int(element[0]), max_times=True
                )
                print(f"max_num: {max_num}")
                element = [int(el) for el in element]
                element[1] = max_num
                element = list(np.arange(*element))
            elif len(element) > 1:
                element = [int(el) for el in element]
                element = list(np.arange(*element))
    return element


def get_numbers_minusone(
    dic, start_time, time_number=None, time_interval=None, max_times=True
):
    folders = dic["folder"] if "folder" in dic else None
    roots = dic["root"] if "root" in dic else None
    print(f"folders: {folders}")
    print(f"roots: {roots}")
    assert (
        folders
    ), f"no folder found in parameters! necessary for -1 numbers; dic: {dic}"
    folders = aux.makeIterable(folders, 1, strings_iterable=False)
    roots = aux.makeIterable(roots, 1, strings_iterable=False)
    folders = [os.path.join(r, f) for r, f in zip(roots, folders)]
    print(f"folders: {folders}")
    difference_time = dic["difference_time"] if "difference_time" in dic else 6
    return Fig.get_check_common_max_snapshot_number(
        folders,
        returnTime=True,
        useTime=True,
        difference_time=difference_time,
        time_interval=time_interval,
        time_number=time_number,
        start_time=start_time,
        max_times=max_times,
        try_to_get_more=False,
    )
