import auxiliary_functions as aux
from collections import OrderedDict
import yaml
import FigureMove as Fig
import iterator
from dictionary_modulation import merge_list_dic_to_single_dic


def add_element_to_list(lis, element, index=None, flatten_element=True):
    if index is None:
        return lis + element
    elif not flatten_element:
        lis[index] = element
        return lis
    elif flatten_element:
        return lis[slice(None, index)] + element + lis[slice(index + 1, None)]


def insert_macros(
    dic,
    identifier_macros,
    convert_macros_to_name,
    meta_macro_file,
    parameters,
    parser_types,
    super_key_order,
    last_keywords_to_replace=["snapshot_number"],
):
    """
    special action keys allowed: REP (necessary for at least single element to have k_replace element) -> replaces
        anything else here, ADD -> adds additional stuff
    automatic deduction of previous: -> variables that inherit macros overwrite by default
    automatic deduction of previous: -> non-inheriting variables add to meta

    Parameters
    ----------
    dic
    identifier_macros
    convert_macros_to_name
    meta_macro_file
    parameters
    super_key_order

    Returns
    -------

    """
    k_add = "ADD_"
    k_replace = [
        "REP_",
        "IT",
    ]  # replaces all parameters including REP_?? and all following on right 1 2 3 REP_4 5 (everythin >= 4)
    ignore_keyword = ["iterator"]
    add_keyword = lambda x: False if type(x) != str else True if k_add in x else False
    sub_keyword = (
        lambda x: False
        if type(x) != str
        else True
        if any([k in x for k in k_replace])
        else False
    )

    for k, v in dic.items():
        if not aux.isIterable(v):
            dic[k] = [v]

    replace_dic = {}
    for k, value_list in dic.items():
        print(k, value_list)
        if k in ignore_keyword:
            continue
        hit = False
        new_list = []
        for value in value_list:
            if sub_keyword(value) or hit:
                hit = True
                new_list.append(value)
        if hit:
            replace_dic[k] = new_list
    for k in replace_dic.keys():
        dic.pop(k)
    check_list = super_key_order + [
        x for x in list(dic.keys()) if x not in super_key_order
    ]
    print(f"check_list: {check_list}")
    for k in check_list:
        if k not in dic:
            continue
        value_list = dic[k]
        for v in value_list:
            if identifier_macros(v):
                macro_dic = _get_macro_dic(
                    k,
                    v,
                    convert_macros_to_name,
                    meta_macro_file,
                    parser_types,
                    parameters,
                )
                replacement_dic_entry = [v_dic for v_dic in dic[k] if v_dic != v]
                if replacement_dic_entry:
                    dic[k] = replacement_dic_entry
                else:
                    dic.pop(k)
                for k_macro, v_macro in macro_dic.items():
                    if k_macro in dic:
                        dic[k_macro].extend(v_macro)
                        # dic[k_macro] = dic[k_macro][::-1]
                    else:
                        dic[k_macro] = v_macro
    if "meta" in dic:
        dic.pop("meta")

    found_macro = False
    for k, value_list in dic.items():
        if k in ignore_keyword:
            continue
        for v in value_list:
            if identifier_macros(v) or sub_keyword(v):
                print("found macro in macro! -> calling insert_macros again!")
                found_macro = True
                break
    if found_macro:
        insert_macros(
            dic,
            identifier_macros,
            convert_macros_to_name,
            meta_macro_file,
            parameters,
            parser_types,
            super_key_order,
        )
    for k, values in replace_dic.items():
        if k in last_keywords_to_replace:
            continue
        conv_values = convert_values_macro(sub_keyword, values, dic.copy())
        dic[k] = [
            parser_types[parameters[k]["parse_type"]](str(v)) for v in conv_values
        ]
    for k, values in replace_dic.items():
        conv_values = convert_values_macro(sub_keyword, values, dic.copy())
        dic[k] = [
            parser_types[parameters[k]["parse_type"]](str(v)) for v in conv_values
        ]
    return dic


def convert_values_macro(sub_keyword, values, dic):
    conv_values = [v.replace("REP_", "") if sub_keyword(v) else v for v in values]
    conv_values = [iterator.convert_element(v, dic) for v in conv_values]
    return conv_values


def _get_macro_dic(
    variable, macro, convert_macros_to_name, meta_macro_file, parser_types, parameters
):
    macro = convert_macros_to_name(macro)
    if variable == "meta":
        macro_dic = load_macro(
            macro=macro,
            filename=meta_macro_file,
            parser_types=parser_types,
            parameters=parameters,
        )
    else:
        macro_dic = load_macro(
            macro=macro,
            filename=f"{variable}.yaml",
            parser_types=parser_types,
            parameters=parameters,
        )
    return macro_dic


def load_macro(macro, filename, parser_types, parameters):
    stream = open(filename, "r")
    macro_dic_or_list = None
    for dic in yaml.load(stream, Loader=yaml.FullLoader):
        for mac, value_dic in dic.items():
            if mac == macro:
                macro_dic_or_list = value_dic
                break
    if macro_dic_or_list is None:
        print(dic)
        print(mac)
        print(macro)
        print(value_dic)
        raise Exception(f"macro {macro} not found in file {filename}!")
    if type(macro_dic_or_list) == list:
        macro_dic = merge_list_dic_to_single_dic(macro_dic_or_list)
    else:
        macro_dic = macro_dic_or_list.copy()
    for k, value_list in macro_dic.items():
        macro_dic[k] = [
            parser_types[parameters[k]["parse_type"]](str(v)) for v in value_list
        ]
    return macro_dic
