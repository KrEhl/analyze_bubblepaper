import matplotlib as mpl

import debug
import feature_extraction

mpl.use("Agg")
import sys

sys.path.insert(0, "../analyze_clusternfw")
import numpy as np
import auxiliary_functions as aux
import copy
import Param as param
import FileWorks as FW
import dictionary_modulation
import os.path
import math
import FigureMove as Fig
import re
import multiprocessing as mp
import argparse_helpers
from SuperClasses import checkLoadNoMemory
import warnings


(
    GRID_FOLDER,
    LOADPLOTDATA,
    REMOVE_GRID_FOLDER_AFTER_PLOT,
    SAVE_GRID_WITH_PROCESSOR_ID,
) = aux.env_variables_snapshot()
if LOADPLOTDATA:
    if os.getenv("DEBUG_MULTISHOW"):
        LOADPLOTDATA = False


def printDebugObjectInitialisation(
    object, sizeX, sizeY, objectName, default_index, flexibleListSize=None
):
    if flexibleListSize is not None:
        print(
            f"object: {object}, sizeX:{sizeX}, sizeY:{sizeY}, objectName:{objectName}, default_index:{default_index}, flexibleListSize:{flexibleListSize}"
        )
    else:
        print(
            f"object: {object}, sizeX:{sizeX}, sizeY:{sizeY}, default_index:{default_index}, objectName:{objectName}"
        )


class SnapElements:
    def __init__(self, **kwrgs):
        self.__dict__ = FW.mergeDictionariesOverwrite(self.__dict__, kwrgs)
        self.translate_plot_type = {
            "s": "slice",
            0: "slice",
            "p": "projection",
            1: "projection",
            "pd": "projection_depth",
            "i": "integral",
            "pi": "pixel_integral",
            "if": "isolate_features",
            "rm": "rotation_measure",
            "p2": "projection_twice",
            "sz": "sz_map",
            "sze": "sz_effect",
            "sp": "sum_projection",
            "ex": "external",
        }
        self.viable_plot_types = dictionary_modulation.convert_dic_to_list(
            self.translate_plot_type
        )
        self.plots_deep = [
            "integral",
            "pixel_integral",
            "isolate_features",
            "rotation_measure",
            "sz_map",
            "sz_effect",
        ]
        self.image = None
        # if self.identifier_process:
        #     self.identifier_process = str(id(mp.current_process()))
        # else:
        #     self.identifier_process = ""
        if self.identifier_process:
            self.unique_idendity = str(id(mp.current_process()))

    def _set_resolution_box_quiver_contour(
        self, qc_res, qc_res_depth, qc_depth, qc_depth_max, qc_plot_type, name
    ):
        if qc_res is None:
            qc_res = np.max(self.res_slice)
        if qc_res_depth is None:
            qc_res_depth = self.res_depth
        if qc_depth is None:
            qc_depth = self.depth
        if qc_depth_max is None:
            qc_depth_max = self.depth_max

        qc_box = self.box_3d[:]
        if np.size(qc_res) == 1:
            if qc_plot_type == "slice":
                qc_box = self.box[:]
                qc_res = aux.resolution_calculator_projections(qc_res, qc_box)
            elif qc_plot_type in self.plots_deep:
                if qc_box[self.proj_axis] is None:
                    qc_box[self.proj_axis] = [x for x in qc_box if x is not None][0]
                qc_res = aux.resolution_calculator_projections(qc_res, qc_box)
                qc_res[self.proj_axis] = qc_res_depth
                qc_box[self.proj_axis] = qc_depth_max
            else:
                qc_box[self.proj_axis] = qc_depth
                qc_res = aux.resolution_calculator_projections(qc_res, qc_box)
                print(f"{name}: qc_depth: {qc_depth}")
                print(f"{name}: qc_res: {qc_res}")
                print(f"{name}: qc_box: {qc_box}")
        if np.size(qc_res) == 2 and qc_plot_type == "slice":
            pass
        elif np.size(qc_res) == 3:
            pass
        else:
            raise Exception(
                f"dissallowed box, res for {name}: qc_res:{qc_res}, "
                f"qc_plot_type:{qc_plot_type}, qc_depth:{qc_depth}"
            )
        return qc_res, qc_box

    def prepare_parameters(self):
        if "_INDIVIDUAL" in self.variable:
            if self.info is None:
                self.info = {}
            info_entries = (
                re.split(r"_INDIVIDUAL::*_?", self.variable)[1]
                .split("_")[0]
                .split("::")
            )
            for ent in info_entries:
                k, v = ent.split(":")
                self.info[k] = v
            self.variable = re.sub(r"(INDIVIDUAL[^_]*)_?", "", self.variable)
            self.variable = re.sub(
                r"_$", "", self.variable
            )  # remove trailing _ if present

        def clean_info(info):
            for k, v in info.items():
                info[k] = argparse_helpers.fancy_eval(v)
            return info

        if self.info is not None:
            self.info = clean_info(self.info)
        if self.variable_info is not None:
            self.variable_info = clean_info(self.variable_info)
        if self.quiver_info is not None:
            self.quiver_info = clean_info(self.quiver_info)
        if self.contour_info is not None:
            self.contour_info = clean_info(self.contour_info)

        if self.plot_type not in self.viable_plot_types:
            raise Exception(
                f"plot_type {self.plot_type} needs to be in viable_plot_types {self.viable_plot_types}!"
            )
        self.proj_axis = 3 - np.sum(self.axis)
        self.res, self.res_slice, self.box, self.box_3d = self._set_resolution_box(
            self.res,
            self.plot_type,
            self.box_3d,
            self.depth,
            self.depth_max,
            self.res_depth,
        )
        # self._set_resolution_box_main()
        # if self.quiver:
        # if self.quiver_plot_type in self.translate_plot_type:
        #     self.quiver_plot_type = self.translate_plot_type[self.quiver_plot_type]
        # if self.quiver_plot_type not in self.viable_plot_types:
        #     raise Exception(
        #         f"quiver_plot_type {self.quiver_plot_type} needs to be in viable_plot_types {self.viable_plot_types}!"
        #     )
        # self.quiver_res, self.quiver_box = self._set_resolution_box_quiver_contour(
        #     self.quiver_res,
        #     self.quiver_res_depth,
        #     self.quiver_depth,
        #     self.quiver_depth_max,
        #     self.quiver_plot_type,
        #     "quiver",
        # )
        # # if self.contour:
        # if self.contour_plot_type not in self.viable_plot_types:
        #     raise Exception(
        #         f"contour_plot_type {self.contour_plot_type} needs to be in viable_plot_types {self.viable_plot_types}!"
        #     )
        # if self.contour_plot_type in self.translate_plot_type:
        #     self.contour_plot_type = self.translate_plot_type[self.contour_plot_type]
        # self.contour_res, self.contour_box = self._set_resolution_box_quiver_contour(
        #     self.contour_res,
        #     self.contour_res_depth,
        #     self.contour_depth,
        #     self.contour_depth_max,
        #     self.contour_plot_type,
        #     "contour",
        # )
        (
            self.contour_res,
            self.contour_res_slice,
            self.contour_box,
            self.contour_box_3d,
        ) = self._set_resolution_box(
            self.contour_res,
            self.contour_plot_type,
            self.contour_box_3d,
            self.contour_depth,
            self.contour_depth_max,
            self.contour_res_depth,
        )
        (
            self.quiver_res,
            self.quiver_res_slice,
            self.quiver_box,
            self.quiver_box_3d,
        ) = self._set_resolution_box(
            self.quiver_res,
            self.quiver_plot_type,
            self.quiver_box_3d,
            self.quiver_depth,
            self.quiver_depth_max,
            self.quiver_res_depth,
        )
        # clean labels:
        self.variable_name = self.variable_name.replace("\\\\", "\\")
        self.variable_name_units = self.variable_name_units.replace("\\\\", "\\")
        self.quiver_variable_name = self.quiver_variable_name.replace("\\\\", "\\")
        self.quiver_variable_name_units = self.quiver_variable_name_units.replace(
            "\\\\", "\\"
        )

    def _set_resolution_box_main(self):
        if self.plot_type in self.translate_plot_type:
            self.plot_type = self.translate_plot_type[self.plot_type]
        if self.box_3d is None:
            if self.height is None or self.width is None or self.depth is None:
                raise Exception(
                    f"need to specify box_3d: {self.box_3d}, or height: {self.height} and width: {self.width} and depth: {self.depth}"
                )
            self.box_3d = [None, None, None]
            self.box_3d[self.axis[0]] = self.width
            self.box_3d[self.axis[1]] = self.height
            self.box_3d[self.proj_axis] = self.depth
            if self.plot_type in self.plots_deep:
                self.box_3d[self.proj_axis] = self.depth_max
        self.box = [self.box_3d[self.axis[0]], self.box_3d[self.axis[1]]]
        self.res_slice = aux.resolution_calculator_projections(
            np.max(self.res), self.box
        )
        if self.plot_type == "slice":
            self.res = [None for _ in range(3)]
            self.res[self.axis[0]] = self.res_slice[0]
            self.res[self.axis[1]] = self.res_slice[1]
            self.res[self.proj_axis] = self.res_depth
        elif (
            type(self.res) == int
            and not (self.box_3d[self.proj_axis] is None)
            and not (self.plot_type in self.plots_deep)
            and not (self.plot_type == "projection_depth")
        ):
            self.res = aux.resolution_calculator_projections(
                np.max(self.res), self.box_3d
            )
        elif self.plot_type in self.plots_deep or self.plot_type == "projection_depth":
            self.res = [None for _ in range(3)]
            self.res[self.axis[0]] = self.res_slice[0]
            self.res[self.axis[1]] = self.res_slice[1]
            self.res[self.proj_axis] = self.res_depth
        if len(self.res) != 3 or len(self.res_slice) != 2:
            raise Exception(
                f"res: {self.res} or res_slice: {self.res_slice} wrong format!"
            )

    def _set_resolution_box(self, res, plot_type, box_3d, depth, depth_max, res_depth):
        axis = self.axis[:]
        proj_axis = 3 - np.sum(axis)
        if plot_type in self.translate_plot_type:
            plot_type = self.translate_plot_type[plot_type]
        if box_3d is None:
            if self.height is None or self.width is None or depth is None:
                raise Exception(
                    f"need to specify box_3d: {box_3d}, or height: {self.height} and width: {self.width} and depth: {depth}"
                )
            box_3d = [None, None, None]
            box_3d[axis[0]] = self.width
            box_3d[axis[1]] = self.height
            box_3d[proj_axis] = depth
            if plot_type in self.plots_deep:
                box_3d[proj_axis] = depth_max
        box = [box_3d[axis[0]], box_3d[axis[1]]]
        res_slice = aux.resolution_calculator_projections(np.max(res), box)
        if plot_type == "slice":
            res = [None for _ in range(3)]
            res[axis[0]] = res_slice[0]
            res[axis[1]] = res_slice[1]
            res[proj_axis] = res_depth
        elif (
            type(res) == int
            and not (box_3d[proj_axis] is None)
            and not (plot_type in self.plots_deep)
            and not (plot_type == "projection_depth")
        ):
            res = aux.resolution_calculator_projections(np.max(res), box_3d)
        elif plot_type in self.plots_deep or plot_type == "projection_depth":
            res = [None for _ in range(3)]
            res[axis[0]] = res_slice[0]
            res[axis[1]] = res_slice[1]
            res[proj_axis] = res_depth
        if len(res) != 3 or len(res_slice) != 2:
            raise Exception(f"res: {res} or res_slice: {res_slice} wrong format!")
        return res, res_slice, box, box_3d

    def getSnap(self):
        if self.root is not None:
            self.folder = os.path.join(self.root, self.folder)
        self.snap, self.snap_filenumber = Fig.readSnap(
            self.folder,
            self.snapshot_number,
            FieldsToLoad=self.fields_loaded,
            useTime=self.use_time,
            lazy_load=True,
            differenceTime=self.difference_time,
            v=0,
            identifier_process=self.identifier_process,
            returnSnapNumber=True,
        )
        snap, time_string, time_float = Fig.getGeneralInformation(
            self.snap,
            returnNotStringTime=True,
            Precision=None,
            IC=self.ic,
            verbose=self.verbose,
        )
        self.time_string = time_string
        if self.time_format != "default":
            self.time_string = Fig.format_integer(time_float, self.time_format)
        self.time_float = time_float
        if self.center is None:
            self.center = self.snap.center
        if self.box_units == "kpc":
            self.length_to_unit_length_code = (
                param.KiloParsec_in_cm / self.snap.UnitLength_in_cm
            )
        else:
            self.length_to_unit_length_code = 1
            warnings.warn(f"self.box_units {self.box_units} not understood: "
                          f"setting self.length_to_unit_length_code = {self.length_to_unit_length_code}")
        self.box_limited_depth = self.snap.boxsize / self.length_to_unit_length_code
        #setting depth_max for normal plot, quiver, contour
        if self.depth_max is None:
            self.depth_max = self.box_limited_depth
        if self.quiver_depth_max is None:
            self.quiver_depth_max = self.box_limited_depth
        if self.contour_depth_max is None:
            self.contour_depth_max = self.box_limited_depth

        if (
            self.plot_type in self.plots_deep
            and self.box_3d[self.proj_axis] is None
        ):
            self.box_3d[self.proj_axis] = self.depth_max
        if (
            self.quiver_plot_type in self.plots_deep
            and self.quiver_box_3d[self.proj_axis] is None
        ):
            self.quiver_box_3d[self.proj_axis] = self.depth_max
        if (
            self.contour_plot_type in self.plots_deep
            and self.contour_box_3d[self.proj_axis] is None
        ):
            self.contour_box_3d[self.proj_axis] = self.depth_max
        
        def get_resolution_limited_res(depth):
            return math.ceil(
                depth
                * self.length_to_unit_length_code
                / np.min(snap.vol) ** (1.0 / 3.0)
            )
        
        if "vol" in self.snap.data.keys() and self.snap.data["vol"] is not None:
            self.resolution_limited_depth_max = get_resolution_limited_res(self.box_limited_depth)
        else:
            warnings.warn(
                "vol not found in snap;  setting self.resolution_limited_depth_max = 1024"
            )
            self.resolution_limited_depth_max = 1024


        def set_res_depth(res_depth, res, plot_type, depth_max):
            if self.verbose:
                print(f"res_depth: {res_depth}")
                print(f"res: {res}")
                print(f"plot_type: {plot_type}")
                print(f"depth_max: {depth_max}")
            if res_depth is None and plot_type in self.plots_deep:
                if not hasattr(self.snap, "vol"):
                    raise Exception("need to define max_depth if no volume present!")
                res_depth = get_resolution_limited_res(depth_max)
                if res[self.proj_axis] is None:
                    res[self.proj_axis] = res_depth
            if plot_type == "slice":
                # not used internally but fixes bug of not being able to save None in lists
                res[self.proj_axis] = 1
            return res_depth, res

        print(f"self.resolution_limited_depth_max: {self.resolution_limited_depth_max}")
        self.res_depth, self.res = set_res_depth(self.res_depth, self.res, self.plot_type, self.depth_max)
        self.quiver_res_depth, self.quiver_res = set_res_depth(self.quiver_res_depth, self.quiver_res, self.quiver_plot_type, self.quiver_depth_max)
        self.contour_res_depth, self.contour_res = set_res_depth(self.contour_res_depth, self.contour_res, self.contour_plot_type, self.contour_depth_max)
        print(f"res_depth: {self.res_depth}")
        # if self.res_depth is None:
        #     if not hasattr(self.snap, "vol"):
        #         raise Exception("need to define max_depth if no volume present!")
        #     self.res_depth = self.resolution_limited_depth_max
        #     if self.res[self.proj_axis] is None:
        #         self.res[self.proj_axis] = self.res_depth
        # if self.depth_max is None:
        #     self.depth_max = self.snap.boxsize
        # if self.quiver and self.quiver_plot_type in self.plots_deep:
        #     if self.quiver_res[self.proj_axis] is None:
        #         # self.quiver_res_depth = self.resolution_limited_depth_max
        #         self.quiver_res[self.proj_axis] = self.quiver_res_depth
        # if self.contour and self.contour_plot_type in self.plots_deep:
        #     if self.contour_res[self.proj_axis] is None:
        #         self.contour_res_depth = self.resolution_limited_depth_max
        #         self.contour_res[self.proj_axis] = self.contour_res_depth
        if self.box_units == "kpc":
            self._convert_length_to_code_units(self.length_to_unit_length_code)
        elif self.box_units == "code":
            pass
        else:
            raise Exception(f"box_units: {self.box_units} not understood!")

    def _convert_length_to_code_units(self, length_to_UnitLengthCode):
        self.length_to_UnitLengthCode = length_to_UnitLengthCode
        if self.scale_bar is not None and type(self.scale_bar) != str:
            self.scale_bar *= length_to_UnitLengthCode
        self.box_3d = [x * length_to_UnitLengthCode for x in self.box_3d]
        self.box = [x * length_to_UnitLengthCode for x in self.box]
        if self.depth_max is not None:
            self.depth_max *= length_to_UnitLengthCode
        if self.depth is not None:
            self.depth *= length_to_UnitLengthCode
        if self.quiver_depth_max is not None:
            self.quiver_depth_max *= length_to_UnitLengthCode
        if self.quiver_depth is not None:
            self.quiver_depth *= length_to_UnitLengthCode
        if self.contour_depth_max is not None:
            self.contour_depth_max *= length_to_UnitLengthCode
        if self.contour_depth is not None:
            self.contour_depth *= length_to_UnitLengthCode
        if self.quiver:
            self.quiver_box = [x * length_to_UnitLengthCode for x in self.quiver_box]
        if self.contour:
            self.contour_box = [x * length_to_UnitLengthCode for x in self.contour_box]
        if self.xlim:
            self.xlim = [x * length_to_UnitLengthCode for x in self.xlim]
        if self.ylim:
            self.ylim = [x * length_to_UnitLengthCode for x in self.ylim]

    def do_rotation_follow(self):
        self.snap.rotate_around_axis(
            rotationangle=self.rotation_early,
            center=self.snap.center,
            rotationaxis=self.rotation_axis_early,
        )
        if self.follow:
            if "focus" in self.follow:
                if "focus_upper" in self.follow:
                    self.center[0] = self.center[0] + self.box_3d[0] / 2.0
                else:
                    raise Exception(f"focus follow {self.follow} not understood!")
            else:
                raise Exception("self.follow not implemented yet")
                if self.follow == "alternative":
                    jet_position = aux.ScreenJetTracer(
                        self.snap,
                        bins=100,
                        up=True,
                        TracerThresh=1e-6,
                        CumTracThresh=[0.3, 0.01, 0.01],
                        AreaToCheck=[0.035, 0.02, 0.02],
                        center=self.center,
                        ensureOnRespectiveSide=True,
                    )
                else:
                    jet_position = aux.follow(
                        self.snap,
                        "JetDensityFraction",
                        upper=True,
                        relative_threshold=1e-3,
                        check_for_0=False,
                        convert_units=False,
                        relativeAxis=0,
                        weights="Volume",
                    )
                if "follow_flexible" not in self.follow:
                    self.center = avgPos
                else:
                    ProjHeightX = (
                        avgPos[0] - 0.5 / 0.67 + self.lowerBoundary + self.heightBubble
                    )
                    self.center[0] = (
                        self.center[0] + self.heightBubble - 1.0 / 2.0 * ProjHeightX
                    )

                if "follow_direction_perp" in self.follow:
                    if "AlternativeFollow" not in self.follow:
                        jet_position = aux.ScreenJetTracer(
                            self.snap,
                            bins=100,
                            up=True,
                            TracerThresh=1e-6,
                            CumTracThresh=[0.3, 0.01, 0.01],
                            AreaToCheck=[0.035, 0.02, 0.02],
                            center=self.center,
                            ensureOnRespectiveSide=True,
                        )
                else:
                    jet_position = aux.follow(
                        self.snap,
                        "JetDensityFraction",
                        upper=True,
                        relative_threshold=1e-3,
                        check_for_0=False,
                        convert_units=False,
                        relativeAxis=0,
                        weights="Volume",
                    )
                self.center[self.projAxis] = copy.deepcopy(avgPos)[self.projAxis]

                if self.follow_relative_pos is not None:
                    self.center[0] = (
                        self.center[0] + self.follow_relative_pos * self.ProjBox[0]
                    )
        # calc rotation if applicable
        self.snap.rotate_around_axis(
            rotationangle=self.rotation,
            center=self.snap.center,
            rotationaxis=self.rotation_axis,
        )
        # calc rotation late if applicable
        self.snap.rotate_around_axis(
            rotationangle=self.rotation_late,
            center=self.snap.center,
            rotationaxis=self.rotation_axis_late,
        )

    def do_plot(self):
        if self.verbose:
            print()
            print(f"self.variable: {self.variable}")
            print(f"self.weight: {self.weight}")
            print(f"self.info: {self.info}")
            print(f"self.plot_type: {self.plot_type}")
            print(f"self.center: {self.center}")
            print(f"self.res: {self.res}")
            print(f"self.res_slice: {self.res_slice}")
            print(f"self.box_3d: {self.box_3d}")
            print(f"self.xlim: {self.xlim}")
            print(f"self.ylim: {self.ylim}")
            print(f"self.box: {self.box}")
            print(f"self.name_sim_save: {self.name_sim_save}")
        self.data = self._get_plot_data(
            variable=self.variable,
            weight=self.weight,
            info=self.info,
            plot_type=self.plot_type,
            res=self.res,
            res_slice=self.res_slice,
            box_3d=self.box_3d,
            box_2d=self.box,
            name_sim_save=self.name_sim_save,
        )
        self.data_shape = np.shape(self.data)
        if len(self.data_shape) != 2:
            raise Exception("data is not two dimensional!")
        self._postprocess_data()
        # exit()

    def _postprocess_data(self):
        # if self.variable_factor is not None:
        #     self.data = self.data * self.variable_factor
        if not (
            self.data_manipulation is None
            or all(x is None for x in self.data_manipulation)
        ):
            for manipulation in self.data_manipulation:
                if "smooth" in manipulation:
                    from scipy import ndimage
                    from string_manipulation import dictionary_from_split

                    manipulation = manipulation.replace("smooth", "")
                    parameter_dic = dictionary_from_split(manipulation)
                    self.data = ndimage.gaussian_filter(
                        self.data, sigma=parameter_dic["sigma"], order=0
                    )
                    if self.verbose:
                        print(
                            f"smoothing image with gaussian of sigma: {parameter_dic['sigma']}"
                        )

        if self.variable_factor is not None:
            if self.verbose:
                print(
                    f"multiplying data by self.variable_factor ({self.variable_factor})"
                )
            self.data = self.data * self.variable_factor

    @checkLoadNoMemory(
        namesreturnvariables=["data"],
        loadPlotData=LOADPLOTDATA,  # LOADPLOTDATA,
        savePlotDataFolder=GRID_FOLDER,
        savePlotDataNameInclude=[
            "self.snap.file_name_abs",
            "res",
            "variable",
            "weight",
        ],
        Ignore=["name_sim_save"],
        checkadditional=[
            "self.axis",
            "self.center",
            "self.follow",
            "self.box_3d",
            "self.proj_axis",
            "self.folder",
            "self.snap.rotation_angle",
            "self.snap.rotation_axis",
            "self.snap.rotation_quiver_angle",
            "self.snap.rotation_quiver_axis",
            "self.snap.file_name_abs",
            "self.use_weight_thresholds",
        ],
        verbose=0,
        save_folder_id_process=SAVE_GRID_WITH_PROCESSOR_ID,
    )
    def _get_plot_data(
        self,
        variable,
        weight,
        info,
        plot_type,
        res,
        res_slice,
        box_3d,
        box_2d,
        name_sim_save,
    ):
        print(f"_get_plot_data:")
        print(f"variable: {variable}")
        print(f"weight: {weight}")
        print(f"plot_type: {plot_type}")
        print(f"info: {info}")
        print(f"res: {res}")
        print(f"res_slice: {res_slice}")
        print(f"box_3d: {box_3d}")
        print(f"box_2d: {box_2d}")
        if plot_type != "external":
            VARIABLE, INFO = Fig.getVariable(
                self.snap, variable, INFO=info, returnINFO=True, verbose=self.verbose
            )
            if self.verbose:
                print(
                    "variable min %g and max %g" % (np.min(VARIABLE), np.max(VARIABLE))
                )
        else:
            INFO = info.copy()
        if weight is not None and plot_type not in ["slice"]:
            WEIGHTS = Fig.getWeights(
                self.snap,
                weight,
                INFO=info,
                useWeightsThresholds=self.use_weight_thresholds,
            )
            if self.verbose:
                print("weights min%g and max %g" % (np.min(WEIGHTS), np.max(WEIGHTS)))
                print(
                    "number cells with nonero value of weights %g"
                    % np.size(WEIGHTS[WEIGHTS != 0])
                )
        else:
            WEIGHTS = None
        if self.follow is not None:
            if "follow_flexible" in self.follow:
                self.box_3d[0] = "needs to be changed!"
        if plot_type == "slice":
            data = Fig.do_Slice(
                self.snap,
                VARIABLE,
                res_slice,
                box_2d,
                self.center,
                self.threads,
                self.axis,
                verbose=self.verbose,
            )
        elif plot_type == "pixel_integral":
            histNameExt = self._get_histogram_name_pixel_int()
            data = Fig.do_PixelIntegral(
                self.snap,
                VARIABLE,
                WEIGHTS,
                res,
                box_3d,
                self.center,
                self.threads,
                self.axis,
                INFO=info,
                histNameExt=histNameExt,
                res_critical=self.res_critical,
            )
        elif plot_type == "isolate_features":
            bin_size = aux.checkIfInDictionary("isolateBinSize", info, 10)
            smoothing_bins = int(max(box_3d) / bin_size)
            print(f"smoothing_bins: {smoothing_bins}")
            data = feature_extraction.do_IsolateFeatures(
                self.snap,
                VARIABLE,
                box_3d,
                weight=WEIGHTS,
                resolution=res,
                center=self.center,
                numthreads=self.threads,
                axis=self.axis,
                INFO=info,
                verbose=self.verbose,
                smoothing_bins=smoothing_bins,
            )
        elif plot_type == "rotation_measure":
            data = Fig.do_RMmap(
                self.snap,
                VARIABLE,
                res,
                box_3d,
                self.center,
                self.threads,
                self.axis,
                INFO=info,
                res_critical=self.res_critical,
            )
        elif plot_type == "projection_twice":
            data = Fig.do_ProjectionTwice(
                self.snap,
                self.folder,
                self.fields_loaded,
                VARIABLE,
                WEIGHTS,
                res,
                box_3d,
                self.center,
                self.threads,
                self.axis,
                info,
                volumeIntegral=False,
                useTime=self.use_time,
                useWeightsThresholds=self.use_weight_thresholds,
                inputGrids=False,
                res_critical=self.res_critical,
            )
        elif plot_type == "sz_map":
            data = Fig.do_SZmap(
                self.snap,
                VARIABLE,
                res,
                box_3d,
                self.center,
                self.threads,
                self.axis,
                INFO=info,
            )
        elif plot_type == "sz_effect":
            sz = self._prepare_sz_effect_plot(VARIABLE)
            data = sz.get_SZEffect(
                variable=info["variable_to_get"],
                Variables=VARIABLE,
                spatialSpacingArrays_in_cm=box_3d[self.proj_axis]
                / res[self.proj_axis]
                / self.length_to_unit_length_code
                * self.snap.UnitLength_in_cm,
                integratingAxis=self.proj_axis,
                INFO=info,
            )
        elif plot_type == "sum_projection":
            # do projection individually for dic of arrays and sum them afterwards
            for variable in list(VARIABLE.keys()):
                VARIABLE[variable] = Fig.do_Projection(
                    self.snap,
                    VARIABLE[variable],
                    None,
                    res,
                    box_3d,
                    self.center,
                    self.threads,
                    self.axis,
                    INFO=info,
                    res_critical=self.res_critical,
                )
            data = np.zeros(np.shape(VARIABLE[list(VARIABLE.keys())[0]]))
            for variable in list(VARIABLE.keys()):
                data += VARIABLE[variable]
            if aux.checkExistenceKey(info, "AbsSZSignal", False):
                data = abs(data)
        elif plot_type == "integral":
            data = Fig.do_Projection(
                self.snap,
                VARIABLE,
                WEIGHTS,
                res,
                box_3d,
                self.center,
                self.threads,
                self.axis,
                sumAlongAxis=True,
                INFO=info,
                res_critical=self.res_critical,
            )
        elif plot_type == "projection" or plot_type == "projection_depth":
            data = Fig.do_Projection(
                self.snap,
                VARIABLE,
                WEIGHTS,
                res,
                box_3d,
                self.center,
                self.threads,
                self.axis,
                sumAlongAxis=False,
                INFO=INFO,
                res_critical=self.res_critical,
            )
        elif plot_type == "external":
            filename = os.path.join("Data/GridPlot/", name_sim_save, variable + ".txt")
            data = np.loadtxt(filename)
            data = data.T
        else:
            raise Exception(f"plot_type {plot_type} not understood!")
        if aux.checkExistenceKey(info, "ABS", False):
            data = abs(data)
        if np.isnan(np.sum(data)):
            raise Exception("Nan in self.data!")
        return data

    def get_vmin_vmax(self):
        if self.vmin_mapping == "default":
            vmin_mapping = lambda data, vmax: 10.0 ** (int(np.log10(vmax)) - 3)
        elif self.vmin_mapping == "min":
            vmin_mapping = lambda data, vmax: np.min(data)
        else:
            raise Exception(f"vmin_mapping: {self.vmin_mapping} not implemented")
        if self.log == "symlog":
            data = np.abs(self.data)
            print(f"abs data!")
        else:
            data = self.data
        self.vmin, self.vmax = self._getVminVmax(
            self.vmin,
            self.vmax,
            self.info,
            self.time_float,
            data,
            self.variable,
            self.model_vmin_vmax,
            vmin_mapping=vmin_mapping,
        )
        print(f"vmin: {self.vmin} vmax: {self.vmax}")
        return self.vmin, self.vmax

    def get_vmin_vmax_quiver(self):
        if not self.quiver:
            return
        self.quiver_vmin, self.quiver_vmax = self._getVminVmax(
            self.quiver_vmin,
            self.quiver_vmax,
            self.quiver_info,
            self.time_float,
            self.quiver_data["color"],
            self.quiver_variable,
            None,
            vmin_mapping=lambda data, vmax: np.percentile(data, 6),
            vmin_negative_mapping=lambda data, vmax: np.percentile(data, 6),
            vmax_mapping=lambda data: np.percentile(data, 94),
        )
        if self.verbose:
            print(f"determined quiver: vmin {self.quiver_vmin} vmax {self.quiver_vmax}")

    def _getVminVmax(
        self,
        vmin_initial,
        vmax_initial,
        info,
        time_float,
        data,
        variable,
        model_vmin_vmax,
        vmin_mapping=lambda data, vmax: 10.0 ** (int(np.log10(vmax)) - 3),
        vmax_mapping=lambda x: np.max(x),
        vmin_negative_mapping=lambda data, vmax: np.min(data),
    ):
        negativeVMin = False
        if type(vmin_initial) == str:
            print(f"vmin_initial is string!!: {vmin_initial}")
        if vmin_initial is not None and vmin_initial < 0:
            negativeVMin = True
        elif vmin_initial is None and np.min(data) < 0:
            negativeVMin = True
        if self.verbose:
            print("negativeVMin %i" % int(negativeVMin))
        if vmax_initial is None:
            vmax = vmax_mapping(data)  # np.max(data)
            if self.verbose:
                print("max data %g" % vmax)
        else:
            vmax = vmax_initial
            if model_vmin_vmax is not None:
                if variable in model_vmin_vmax:
                    vmax = info["modelfunc"](time_float, info["modelPara"])
        if (vmin_initial is None) and vmax != 0 and not negativeVMin:
            vmin = vmin_mapping(data, vmax)  # 10.0 ** (int(np.log10(vmax)) - 3)
            if self.verbose:
                print(vmin)
                debug.printDebug(data, name="data")
                if vmin < np.min(data):
                    vmin = np.min(data)
                print("--------------")
                print(
                    "missing fraction %g" % (np.sum(data[data < vmin]) / np.sum(data))
                )
                print("---------------")
        elif vmin_initial is None and negativeVMin:
            vmin = vmin_negative_mapping(data, vmax)  # np.min(data)
        else:
            vmin = vmin_initial
            if model_vmin_vmax is not None:
                if variable in model_vmin_vmax:
                    vmin = vmax * 1e-3
                    if self.verbose:
                        print(f"vmin for CR energy density: {vmin}")
        if vmin is None or vmax is None:
            if (
                aux.checkIfInDictionary("allow0VminVax", info, False)
                and np.min(data) == 0
                and np.max(data) == 0
            ):
                vmin = 0
                vmax = 0
            elif vmin is None and vmax == 0 and not negativeVMin:
                # if self.verbose:
                print(
                    "all values are 0 -> setting vmin=0.1, vmax=1 (so log is availale)"
                )
                vmin = 0.1
                vmax = 1
            else:
                if self.verbose:
                    print(vmin, vmax)
                    debug.printDebug(data, zero=True)
                raise Exception("vmin or vmax is None")
        if aux.checkIfInDictionary("vmin_vmax_symmetric_zero", info, True) and (
            vmin < 0 and vmax > 0
        ):
            vmin, vmax = -np.max([abs(vmin), vmax]), np.max([abs(vmin), vmax])
        return vmin, vmax

    def _prepare_sz_effect_plot(self, VARIABLE):
        for variable in list(VARIABLE.keys()):
            if variable == "velocity":
                VARIABLE[variable] = VARIABLE[variable][:, self.proj_axis]
            VARIABLE[variable] = Fig.do_Projection(
                self.snap,
                VARIABLE[variable],
                None,
                self.resolutionProjection,
                self.ProjBox,
                self.center,
                self.threads,
                self.axis,
                sumAlongAxis=True,
                INFO=self.info,
            )
        import SunyaevZeldovichSignal as sz

        if self.verbose:
            print("spatial spacing")
            print(self.ProjBox[0] / self.resolutionProjection[0])
        return sz

    def _get_histogram_name_pixel_int(self):
        histNameExt = ""
        if self.rotation_early is not None:
            histNameExt += "RotE%s%i_" % (
                aux.convert_params_to_string(self.rotation_axis_early, joinSymbol=""),
                self.rotation_early / np.pi * 180,
            )
        if self.rotation is not None:
            histNameExt += "Rot%s%i_" % (
                aux.convert_params_to_string(self.rotation_axis, joinSymbol=""),
                self.rotation / np.pi * 180,
            )
        histNameExt += "%s" % self.variable
        histNameExt += "%s" % self.weight
        return histNameExt

    def do_plot_contour(self):
        if not self.contour:
            self.contour_data = None
            return
        print(f"do_plot_contour")
        print(f"contour_variable: {self.contour_variable}")
        print(f"self.contour_weight : {self.contour_weight}")
        print(f"self.contour_res{self.contour_res}")
        print(f"self.contour_res_slice{self.contour_res_slice}")
        print(f"self.contour_box_3d{self.contour_box_3d}")
        print(f"self.contour_box{self.contour_box}")
        self.contour_data = self._get_plot_data(
            variable=self.contour_variable,
            weight=self.contour_weight,
            info=self.contour_info,
            plot_type=self.contour_plot_type,
            res=self.contour_res,
            res_slice=self.contour_res_slice,
            box_3d=self.contour_box_3d,
            box_2d=self.contour_box,
            name_sim_save=self.name_sim_save,
        )
        if self.verbose:
            print(f"self.center: {self.center}")
            print(f"self.axis : {self.axis}")
            print(f"self.contour_info : {self.contour_info}")
            print(f"self.contour_box : {self.contour_box}")
        self.contour_data = aux.get_contour_data(
            self.contour_data,
            self.axis,
            self.contour_box_3d,
            center=self.center,
            filter=self.contour_filter,
            info=self.contour_info,
            return_dic=True,
        )

    def do_plot_quiver(self):
        if not self.quiver:
            self.quiver_data = None
            return
        U = self._get_plot_data(
            variable=self.quiver_variable + str(self.axis[0]),
            weight=self.quiver_weight,
            info=self.quiver_info,
            plot_type=self.quiver_plot_type,
            res=self.quiver_res,
            res_slice=self.quiver_res_slice,
            box_3d=self.quiver_box_3d,
            box_2d=self.quiver_box,
            name_sim_save=self.name_sim_save,
        )
        V = self._get_plot_data(
            variable=self.quiver_variable + str(self.axis[1]),
            weight=self.quiver_weight,
            info=self.quiver_info,
            plot_type=self.quiver_plot_type,
            res=self.quiver_res,
            res_slice=self.quiver_res_slice,
            box_3d=self.quiver_box_3d,
            box_2d=self.quiver_box,
            name_sim_save=self.name_sim_save,
        )

        self.quiver_data = aux.get_quiver_stream_data(
            U,
            V,
            self.quiver_box_3d,
            self.center,
            self.axis,
            skipN=self.quiver_skip_n,
            info=self.quiver_info,
            normalize=self.quiver_normalization,
            normalize_length=True,
            return_dic=True,
        )
        self.quiver_data["color"] = np.sqrt(
            self.quiver_data["U"] ** 2 + self.quiver_data["V"] ** 2
        )

    def get_show_data(self):
        data_dic = {}
        self.show_data_data = None
        import gridplot_setups

        if self.show_data_setup is None or self.show_data == "never":
            return
        data_names = self.show_data_setup.split("_")
        for n in data_names:
            if n.startswith("GRID"):
                setups = n.replace("GRID", "").split(";")
                for s in setups:
                    print(f"looking at setup: {s}")
                    (
                        i,
                        SetupNam,
                        variableLabel,
                        INFO,
                        setup_style,
                    ) = gridplot_setups.get_grid_variable(
                        0, float(s), [], [], {}, param.MAXSEARCHRADIUS
                    )
                    (
                        relevantElements,
                        filename,
                        header,
                        function,
                        functionParameters,
                    ) = gridplot_setups.retrieve_parameters_from_info(
                        INFO, pre_string_key="0-_"
                    )
                    filename = gridplot_setups.get_filename(
                        filename, self.folder, functionParameters
                    )
                    relevantElements = [
                        gridplot_setups.RetrivingValuesFromStrings(
                            x, self.snap, INFO, self.snap_filenumber, folder=None
                        )
                        for x in relevantElements
                    ]
                    print(f"functionParameters: {functionParameters}")
                    functionParameters = [
                        gridplot_setups.RetrivingValuesFromStrings(
                            x, self.snap, INFO, self.snap_filenumber, folder=None
                        )
                        for x in functionParameters
                    ]
                    print(f"relevantElements: {relevantElements}")
                    print(f"function: {function}")
                    values = FW.check_retrieve_write_values_csv_fast(
                        filename,
                        header,
                        function,
                        functionParameters,
                        dtypesFloat=float,
                        relevantElementsNumbers=np.arange(
                            len(relevantElements)
                        ).tolist(),
                        relevantElements=[relevantElements],
                        removeFile=False,
                        arrayToCheckIndex=0,
                        ifAllFoundReturn=[None],
                    )
                    data = {
                        "SetupNam": SetupNam[0],
                        "values": values[0][0],
                        "variableLabel": variableLabel[0],
                    }
                    data_dic[s] = data
        self.show_data_data = data_dic
        return

    def do_data_plot(self):
        if self.data_plot is None:
            return
        if "histogram" in self.data_plot:
            filename = Fig.generateFilename(
                (
                    aux.getSimName(self.snap, includeFileName=True),
                    self.variable,
                    self.weight,
                    self.res,
                    self.plot_type,
                ),
                ("Si", "Var", "Wei", "Res", "ProjTyp"),
            )
            Fig.getHistogram(
                self.data,
                plotFilename=os.path.join(self.save_folder, f"DataPlots/{filename}"),
                plotShow=False,
            )

    def clean_up(self):
        if self.center is None:
            self.center = [self.snap.boxsize / 2.0 for i in range(3)]
            if self.verbose:
                print(
                    "center is None in cleanup, taking self.snap.boxsize/2.",
                    self.center,
                )
        self.extent = Fig.getExtension(self.box_3d, self.center, self.axis)
        self.data_shape_figure = np.zeros(2)
        self.extent_figure = np.zeros(4)
        if self.ylim:
            if len(self.ylim) != 2:
                raise Exception(
                    f"need to give two elements to define ylim! (currently: {self.ylim})"
                )
            y_center = (self.extent[3] - self.extent[2]) / 2.0 + self.extent[2]
            self.extent_figure[3] = y_center + self.ylim[1]
            self.extent_figure[2] = y_center + self.ylim[0]
            self.data_shape_figure[0] = (
                self.data_shape[0]
                * (self.ylim[1] - self.ylim[0])
                / (self.extent[3] - self.extent[2])
            )
        else:
            self.data_shape_figure[0] = self.data_shape[0]
            self.extent_figure[3] = self.extent[3]
            self.extent_figure[2] = self.extent[2]
        if self.xlim:
            if len(self.xlim) != 2:
                raise Exception(
                    f"need to give two elements to define ylim! (currently: {self.xlim})"
                )
            x_center = (self.extent[1] - self.extent[0]) / 2.0 + self.extent[0]
            self.extent_figure[1] = x_center + self.xlim[1]
            self.extent_figure[0] = x_center + self.xlim[0]
            self.data_shape_figure[1] = (
                self.data_shape[1]
                * (self.xlim[1] - self.xlim[0])
                / (self.extent[1] - self.extent[0])
            )
        else:
            self.data_shape_figure[1] = self.data_shape[1]
            self.extent_figure[1] = self.extent[1]
            self.extent_figure[0] = self.extent[0]
        del self.snap
        # exit()
