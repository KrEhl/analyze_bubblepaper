import numpy as np
import os
import warnings
from matplotlib import pyplot as plt
import matplotlib.patheffects as path_effects
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import ImageGrid
import FigureMove as Fig
import glob

import auxiliary_functions as aux
import AxesGeneration
import debug
import feature_extraction

(
    GRID_FOLDER,
    LOADPLOTDATA,
    REMOVE_GRID_FOLDER_AFTER_PLOT,
    SAVE_GRID_WITH_PROCESSOR_ID,
) = aux.env_variables_snapshot()
mpl.use("Agg")

print(f"REMOVE_GRID_FOLDER_AFTER_PLOT: {REMOVE_GRID_FOLDER_AFTER_PLOT}")
print(f"GRID_FOLDER: {GRID_FOLDER}")


class PlotElement:
    def __init__(
        self, data_elements,
    ):
        self.data_elements = data_elements
        self.shown_time = []
        self.shown_sim = []

    def _get_field(self, field, flat=False, remove_none=False):
        lis = [[x.__dict__[field] for x in y] for y in self.data_elements[:][:]]
        if flat:
            lis = aux.flatten(lis)
        if flat and remove_none:
            if type(lis[0]) != list:
                lis = [x for x in lis if x is not None]
            else:
                lis = [[x for x in y if x is not None] for y in lis]
        elif remove_none:
            if type(lis[0][0]) != list:
                lis = [[x for x in y if x is not None] for y in lis]
            else:
                lis = [[[x for x in y if x is not None] for y in z] for z in list]
        return lis

    def _field_all_equal(self, field, equal_to=None):
        lis = self._get_field(field, flat=True)
        if equal_to is None:
            check = all(x == lis[0] for x in lis)
        else:
            check = all(x == equal_to for x in lis)
        return check

    def parameter_setup(self):
        self.nrows_ncols = list(np.shape(self.data_elements))[::-1]
        self._colorbar_parameters()

    def _colorbar_parameters(self):
        self.show_colorbar = any(
            bool(x) for x in self._get_field("show_colorbar", flat=True)
        )
        self.colorbar_single = False
        self.colorbar_vertical = False
        if (
            self._field_all_equal("variable")
            and self._field_all_equal("vmin")
            and self._field_all_equal("vmax")
            and self._field_all_equal("info")
            and self._field_all_equal("log")
            and self._field_all_equal("colormap")
        ) or self._field_all_equal("common_vmin_vmax_axis", equal_to="xy"):
            self.colorbar_vertical = False
            self.colorbar_single = True
        elif self._field_all_equal("common_vmin_vmax_axis", equal_to="x"):
            self.colorbar_vertical = True
        elif self._field_all_equal("common_vmin_vmax_axis", equal_to="y"):
            self.colorbar_vertical = False
        elif self._field_all_equal("common_vmin_vmax_axis", equal_to=None):
            self.colorbar_vertical = True
        self.colormap_orientation = "horizontal"
        if self.colorbar_vertical:
            self.colormap_orientation = "vertical"

        self.quiver_colorbar = False
        if (
            all(self._get_field("quiver_colorbar", flat=True))
            and any(self._get_field("quiver", flat=True))
            and not any(self._get_field("quiver_color", flat=True))
        ):
            self.quiver_colorbar = True
            pass
        self.colorbar_above_single = False
        if self.quiver_colorbar:
            if (
                self._field_all_equal("quiver_variable", equal_to=None)
                and self._field_all_equal("quiver_vmin", equal_to=None)
                and self._field_all_equal("quiver_vmax", equal_to=None)
            ):
                self.colorbar_above_single = True

    def initialize_figure(self, sizeX, sizeY):
        import matplotlib as mpl

        element = self.data_elements[0][0]
        # recommend TkAgg, Agg
        BACKEND = os.environ.get("BACKEND")
        if BACKEND is None:
            BACKEND = "Agg"
        print(f"BACKEND: {BACKEND}")
        # mpl.use(BACKEND)
        mpl.use("Agg")
        # screen_width = int(os.getenv("SCREEN_WIDTH_PIXEL"))
        # screen_height = int(os.getenv("SCREEN_HEIGHT_PIXEL"))
        figsizex = 5
        # figsizex = screen_width / dpi * 5 / 7
        approx_figsizey = (sizeY + 1) / sizeX * figsizex
        if self.colorbar_vertical:
            approx_figsizey = sizeY / (sizeX + 1) * figsizex
        #
        # if approx_figsizey * dpi > screen_height:
        #     print(self._get_field("data_shape"))
        #     figsizex = 7 / 8 * screen_height / dpi * sizeX / (sizeY + 0.5)
        #     if self.colorbar_vertical:
        #         figsizex = 7 / 8 * screen_height / dpi * (sizeX + 0.5) / (sizeY)
        #
        self.fontsize = element.fontsize  # 6  # * 2.8/sizeX
        if self.fontsize is None:
            if self.colorbar_vertical:
                self.fontsize = 8
            else:
                if sizeY == 1:
                    self.fontsize = 7
                else:
                    self.fontsize = 4.2
                if sizeX == 1:
                    self.fontsize = 14
                if sizeX > 3 and sizeY > 3:
                    self.fontsize = 4
        print(f"fontsize: {self.fontsize}")
        self._set_rcParams()

        length_colorbar_percent = element.length_colorbar_percent
        width_colorbar = element.width_colorbar
        colorbar_offset = element.colorbar_offset
        dpi = element.dpi

        # if approx_figsizey * dpi > screen_height:
        #     warnings.warn(
        #         f"figsize x ({approx_figsizey}) too large to portray on display"
        #     )
        left = top = bottom = right = 0.01
        if self.colorbar_vertical:
            right = 0.13
            if width_colorbar is None:
                width_colorbar = 0.02
            if colorbar_offset is None:
                colorbar_offset = 0.01
            if length_colorbar_percent is None:
                length_colorbar_percent = 0.7
            length_colorbar_horizontal_percent = 0
        else:
            bottom = 0.12
            if sizeY == 1 and sizeX > 1:
                if width_colorbar is None:
                    width_colorbar = 0.04
                if colorbar_offset is None:
                    colorbar_offset = 0.03
                if length_colorbar_percent is None:
                    length_colorbar_percent = 0.7
            elif sizeY == 1 and sizeX == 1:
                if width_colorbar is None:
                    width_colorbar = 0.04
                if colorbar_offset is None:
                    colorbar_offset = 0.03
                if length_colorbar_percent is None:
                    length_colorbar_percent = 0.7
            elif sizeY == 1:
                if width_colorbar is None:
                    width_colorbar = 0.02
                if colorbar_offset is None:
                    colorbar_offset = 0.01
                if length_colorbar_percent is None:
                    length_colorbar_percent = 0.75
            else:
                if width_colorbar is None:
                    width_colorbar = 0.02
                if colorbar_offset is None:
                    colorbar_offset = 0.01
                if length_colorbar_percent is None:
                    length_colorbar_percent = 0.75
            length_colorbar_horizontal_percent = length_colorbar_percent
        if self.quiver_colorbar:
            top = 0.13
        if not self.show_colorbar:
            bottom = left = right = top = 0
        (
            self.figure,
            self.axes,
            self.axes_colorbar,
            self.axes_colorbar_above,
        ) = AxesGeneration.getAxesList(
            self._get_field("data_shape_figure"),
            width_colorbar_horizontal=width_colorbar,
            width_colorbar_vertical=width_colorbar,
            length_colorbar_vertical_percent=length_colorbar_percent,
            length_colorbar_horizontal_percent=length_colorbar_horizontal_percent,
            length_colorbar_above_percent=length_colorbar_percent,
            width_colorbar_above=width_colorbar,
            hspace=0,
            right=right,
            left=left,
            bottom=bottom,
            top=top,
            colorbar_vertical_offset=colorbar_offset,
            colorbar_horizontal_offset=colorbar_offset,
            colorbar_above_offset=colorbar_offset,
            wspace=0.0,
            figsizex="A4",
            colorbarVertical=self.colorbar_vertical,
            SingleColorbar=self.colorbar_single,
            ShowAxisTicks=False,
            show_colorbar_above=self.quiver_colorbar,
            single_colorbar_above=self.colorbar_above_single,
            show_colorbar=self.show_colorbar,
            dpi=dpi,
        )
        print(f"axes_colorbar_above: {self.axes_colorbar_above}")

    def _set_rcParams(self):
        font = {"family": "Times New Roman", "serif": ["Arial"]}  # , "size": fontsize}
        ticks_font = {"family": "sans-serif", "serif": ["Helvetica"]}  # ,
        font = {"family": "serif", "serif": ["Arial"]}
        mpl.rc("text", usetex=True)
        mpl.rc("text.latex", preamble=[r"\usepackage{bm}"])
        # mpl.rc['text.latex.unicode'] = True
        # mpl.rcParams["text.latex.preamble"] = [
        #     r"\usepackage{amsmath,bm}\newcommand{\sbf}[1]{\textsf{\textbf{#1}}}"
        # ]
        # mpl.rcParams["figure.dpi"] = 450
        # mpl.rcParams["axes.labelsize"] = fontsize
        # mpl.rcParams["legend.fontsize"] = fontsize
        # mpl.rcParams["xtick.labelsize"] = fontsize
        # mpl.rcParams["ytick.labelsize"] =fontsize
        mpl.rcParams["font.size"] = self.fontsize  # * figsizex/3
        mpl.rc("font", **font)
        # mpl.rc("ticks_font", **ticks_font)

    def plot_data(self, X, Y):
        element = self.data_elements[X][Y]
        yy = np.linspace(
            element.extent[2], element.extent[3], np.shape(element.data)[0] + 1,
        )
        xx = np.linspace(
            element.extent[0], element.extent[1], np.shape(element.data)[1] + 1,
        )

        norm = None
        vmax = element.vmax
        vmin = element.vmin
        if element.vmin is not None and element.log != "symlog":
            data = element.data.astype(np.float32)
            element.data[data < vmin] = vmin
            debug.printDebug(element.data, "element.data after vmin zero")
        if element.vmax != 0 and element.log == "symlog":
            debug.printDebug(element.data, name="element.data symlog")
            norm = mpl.colors.SymLogNorm(linthresh=vmin, vmax=vmax, vmin=-vmax)
            vmin = None
            vmax = None
        elif element.vmax != 0 and element.log:
            norm = mpl.colors.LogNorm(vmax=vmax, vmin=vmin,)
            vmin = None
            vmax = None
        alpha = 1
        if element.quiver_type == "lic":
            alpha = 0.6
        element.image = self.axes[X][Y].pcolormesh(
            xx.astype(np.float32),
            yy.astype(np.float32),
            element.data.astype(np.float32),
            cmap=element.colormap,
            shading="flat",
            norm=norm,
            rasterized=element.rasterized,
            vmin=vmin,
            vmax=vmax,
            alpha=alpha,
        )
        if element.xlim:
            if len(element.xlim) != 2:
                raise Exception(
                    f"need to give two elements to define xlim! (currently: {element.xlim})"
                )
        self.axes[X][Y].set_xlim(element.extent_figure[0], element.extent_figure[1])
        if element.ylim:
            if len(element.ylim) != 2:
                raise Exception(
                    f"need to give two elements to define ylim! (currently: {element.ylim})"
                )
        self.axes[X][Y].set_ylim(element.extent_figure[2], element.extent_figure[3])

    def text(self, X, Y):
        self._text_time_folder(X, Y)
        self._data_text(X, Y)
        self._scale_bar(X, Y)
        self._draw_objects(X, Y)
        self._text_top(X, Y)

    def _text_top(self, X, Y):
        element = self.data_elements[X][Y]
        if Y != 0 or element.text_top is None:
            return
        text = self.axes[X][Y].text(
            0.5,
            1.05,
            element.text_top,
            horizontalalignment="center",
            verticalalignment="bottom",
            rotation="horizontal",
            transform=self.axes[X][Y].transAxes,
            # fontsize=fontsizeTime,
        )

    def _draw_objects(self, X, Y):
        element = self.data_elements[X][Y]
        if element.draw_object is None or all(x is None for x in element.draw_object):
            return
        if not aux.isIterable(element.draw_object):
            raise Exception(
                f"expect list of strings to define drawing objects! (got: {element.draw_object})"
            )
        for draw_object in element.draw_object:
            if "ellipse" in draw_object:
                draw_object = draw_object.replace("ellipse", "")
                from matplotlib.patches import Ellipse
                from string_manipulation import dictionary_from_split

                parameter_dic = dictionary_from_split(draw_object)
                x_center = (
                    element.extent_figure[1] - element.extent_figure[0]
                ) / 2 + element.extent_figure[0]
                x = x_center + parameter_dic["x"] * element.length_to_UnitLengthCode
                y_center = (
                    element.extent_figure[3] - element.extent_figure[2]
                ) / 2 + element.extent_figure[2]
                y = y_center + parameter_dic["y"] * element.length_to_UnitLengthCode
                width = parameter_dic["width"] * element.length_to_UnitLengthCode
                height = parameter_dic["height"] * element.length_to_UnitLengthCode
                ellipse = Ellipse(
                    [x, y],
                    width,
                    height,
                    angle=0,
                    fill=False,
                    color="white",
                    linestyle="--",
                    lw=1,
                )
                self.axes[X][Y].add_patch(ellipse)

    def _text_time_folder(self, X, Y):
        element = self.data_elements[X][Y]
        if element.show_time == "never" and element.show_folder == "never":
            return
        text_time = ""
        if element.show_time == "default" or element.show_time == "once":
            if element.time_string in self.shown_time:
                pass
            else:
                text_time = " ".join([text_time, element.time_string])
                self.shown_time.append(element.time_string)
        text_folder = ""
        if element.show_folder == "default" or element.show_folder == "once":
            if element.name_sim in self.shown_sim:
                pass
            else:
                text_folder = " ".join([text_folder, element.name_sim])
                self.shown_sim.append(element.name_sim)
        elif element.show_folder == "always":
            text_folder = " ".join([text_folder, element.name_sim])
            self.shown_sim.append(element.name_sim)
        if not text_folder and not text_time:
            return

        Fig._text_time_folder(
            self.axes[X][Y],
            text=text_folder,
            text_color=element.text_color,
            text_position=element.text_position,
            text_style=element.text_style,
            fontsize=self.fontsize,
        )
        Fig._text_time_folder(
            self.axes[X][Y],
            text=text_time,
            text_color=element.text_color,
            text_position=element.text_position_time,
            text_style=element.text_style,
            fontsize=self.fontsize,
        )

    def _data_text(self, X, Y):
        element = self.data_elements[X][Y]
        if element.show_data == "never" or element.show_data_data is None:
            return
        text = ""
        for index, (k, dic) in enumerate(element.show_data_data.items()):
            text += "$"
            text += dic["variableLabel"]
            text += "="
            value = dic["values"]
            value = float(value)
            # value = round(value, 0 if np.log10(abs(value))>0 else np.log10(abs(value))+1)
            text += "%.2g" % (value)
            if index != len(element.show_data_data) - 1:
                text += "$\n"
            else:
                text += "$"
        # text = r"$%s$" %(text)
        print(text)
        from_left = 0.9
        from_bottom = 0.9
        verticalalignment = "top"
        horizontalalignment = "right"
        if element.show_data == "left":
            verticalalignment = "top"
            from_left = 0.03
            from_bottom = 0.9
            horizontalalignment = "left"
        text = self.axes[X][Y].text(
            from_left,
            from_bottom,
            text,
            horizontalalignment=horizontalalignment,
            verticalalignment=verticalalignment,
            transform=self.axes[X][Y].transAxes,
            color=element.text_color,
            alpha=1.0,
            rasterized=True,
        )
        self._set_text_effects(text, element.text_style)

    def _set_text_effects(self, text, style):
        if "border" in style:
            text.set_path_effects(
                [
                    path_effects.Stroke(
                        linewidth=self.fontsize / 6.0, foreground="black"
                    ),
                    path_effects.Normal(),
                ]
            )

    def _scale_bar(self, X, Y):
        element = self.data_elements[X][Y]
        if element.scale_bar is None:
            return
        if not (X == 0 and Y == 0):
            return
        box_x = element.extent_figure[1] - element.extent_figure[0]
        box_y = element.extent_figure[3] - element.extent_figure[2]
        if element.scale_bar == "default":
            element.scale_bar = box_x / 4
        rel_offset_x = 0.16
        rel_offset_y = 0.85
        pos1 = element.extent_figure[0] + rel_offset_x * box_x + element.scale_bar
        pos0 = element.extent_figure[0] + rel_offset_x * box_x
        rel_offset_x_right = 0
        rel_offset_x_left = 0.07
        if element.scale_bar_position == "default":
            element.scale_bar_position = "upperright"
        if element.scale_bar_position == "lowerleft":
            rel_offset_y = 0.07
        if element.scale_bar_position == "upperright":
            rel_offset_x_right = 0.07
        if element.scale_bar_position == "lowerright":
            rel_offset_x_right = 0.07
            rel_offset_y = 0.07
        if element.scale_bar_position == "centerleft":
            rel_offset_y = 0.5
        if "right" in element.scale_bar_position:
            pos0 = (
                element.extent_figure[0]
                + box_x
                - rel_offset_x_right * box_x
                - element.scale_bar
            )
            pos1 = element.extent_figure[0] + box_x - rel_offset_x_right * box_x
        elif "left" in element.scale_bar_position:
            pos0 = element.extent_figure[0] + rel_offset_x_left * box_x
            pos1 = (
                element.extent_figure[0] + element.scale_bar + rel_offset_x_left * box_x
            )
        pos = [
            pos0,
            pos1,
            element.extent_figure[2] + rel_offset_x * box_y,
            element.extent_figure[2] + rel_offset_y * box_y,
        ]
        color = element.text_color
        if element.scale_bar_color is not None:
            color = element.scale_bar_color
        if "thinborder" in element.text_style:
            outline = [path_effects.withStroke(linewidth=0.3, foreground="black")]
        elif "border" in element.text_style:
            outline = [path_effects.withStroke(linewidth=4, foreground="black")]
        else:
            outline = None
        print(f"outline: {outline}")
        self.axes[X][Y].plot(
            (pos[0], pos[1]),
            (pos[3], pos[3]),
            "-",
            color=color,
            linewidth=1,
            path_effects=outline,
        )
        if element.scale_bar_show_label:
            text = self.axes[X][Y].text(
                pos[0] + (pos[1] - pos[0]) / 2.0,
                element.extent_figure[2] + rel_offset_y * box_y + 0.03 * box_y,
                "%i kpc"
                % np.ceil(element.scale_bar / element.length_to_UnitLengthCode),
                # fontsize=fontsizeScaleBar,
                horizontalalignment="center",
                color=color,
            )
            self._set_text_effects(text, element.text_style)

    def plot_colorbars_above(self, X, Y, max_tick_labels=5):
        element = self.data_elements[X][Y]
        if not element.quiver or element.quiver_color or not element.quiver_colorbar:
            return
        if self.colorbar_above_single:
            index = 0
            if not (X == 0 and Y == 0):
                return
        else:
            index = X
            if Y != 0:
                return

        if element.quiver_log:
            major, minor = Fig.setLogTicks(
                element.quiver_vmin,
                element.quiver_vmax,
                numberMax=max_tick_labels,
                minormajorOverlap=True,
                returnOnlyMajorTicks=False,
            )
            if len(major) < 2:
                ticks = minor
            else:
                ticks = major
            format = mpl.ticker.LogFormatterMathtext()
        else:
            ticks = Fig.setLinTicksAlt(
                element.quiver_vmin,
                element.quiver_vmax,
                numberMax=max_tick_labels,
                symmetricAroundZero=True,
            )
            format = mpl.ticker.ScalarFormatter()
        if element.quiver_colorbar:
            if element.quiver_norm is not None:
                cbar = plt.colorbar(
                    mpl.cm.ScalarMappable(
                        norm=element.quiver_norm, cmap=element.quiver_colormap
                    ),
                    cax=self.axes_colorbar_above[index],
                    orientation="horizontal",
                    # ticks=np.array(ticks, dtype=np.float64),
                    format=format,
                )
            else:
                cbar = plt.colorbar(
                    element.quiver_image,
                    cax=self.axes_colorbar_above[index],
                    orientation="horizontal",
                    ticks=ticks,
                    format=format,
                )

            cbar.ax.set_yticklabels([])
            cbar.ax.get_yaxis().set_visible(False)
            units = ""
            # if not element.quiver_log:
            units = self.get_offset_units(
                cbar, ticks, element.quiver_variable_name_units, element.quiver_log
            )
            cbar.ax.set_xlabel(r"${}{}$".format(element.quiver_variable_name, units),)
            cbar.ax.xaxis.set_ticks_position("top")
            cbar.ax.xaxis.set_label_position("top")

    def get_offset_units(
        self, cbar, ticks, name_units, log, colorbar_vertical=False, power=2
    ):
        offset = ""
        units = ""
        if not log:
            max_power = int(np.floor(np.log10(np.max(abs(ticks)))))
            if abs(max_power) > power:
                if not colorbar_vertical:
                    cbar.ax.set_xticklabels([x / 10 ** max_power for x in ticks])
                else:
                    cbar.ax.set_yticklabels([x / 10 ** max_power for x in ticks])
                offset = "10^{%i}" % (max_power)
        if name_units and offset:
            units = "\\ [{}\\ {}]".format(offset, name_units)
        elif name_units:
            units = "\\ [{}]".format(name_units)
        elif offset:
            units = "\\times\\,{}".format(offset)
        return units

    def plot_colorbars(self, X, Y, max_tick_labels=5):
        if not self.show_colorbar:
            return
        if self.colorbar_single:
            index = 0
            if not (X == 0 and Y == 0):
                return
        elif self.colorbar_vertical:
            index = Y
            if X != 0:
                return
        else:
            index = X
            if Y != 0:
                return
        element = self.data_elements[X][Y]

        if element.log:
            if element.log == "symlog":
                max_tick_labels = max_tick_labels // 2
            major, minor = Fig.setLogTicks(
                element.vmin,
                element.vmax,
                numberMax=max_tick_labels,
                minormajorOverlap=True,
                returnOnlyMajorTicks=False,
            )
            if element.log == "symlog":
                major = [-x for x in major][::-1] + list(major)
                minor = [-x for x in minor][::-1] + list(minor)
            print(f"colormap: log: {element.log}")
            print(f"colormap: major: {major}: minor: {minor}")
            if len(major) < 2:
                ticks = minor
            else:
                ticks = major
            ticks = np.float64(ticks)
            print(f"ticks {ticks}")
            print(f"type ticks {type(ticks[0])}")
            format = mpl.ticker.LogFormatterMathtext()
        else:
            ticks = Fig.setLinTicksAlt(
                element.vmin,
                element.vmax,
                numberMax=max_tick_labels,
                symmetricAroundZero=True,
            )
            format = mpl.ticker.ScalarFormatter()
            print(
                f"linticks: {ticks}, self.colorbar_vertical: {self.colorbar_vertical}"
            )
        cbar = plt.colorbar(
            element.image,
            cax=self.axes_colorbar[index],
            orientation=self.colormap_orientation,
            ticks=ticks,
            format=format,
        )
        units = self.get_offset_units(
            cbar,
            ticks,
            element.variable_name_units,
            element.log,
            self.colorbar_vertical,
        )
        print(f"units:  {units}")
        if self.colorbar_vertical:
            self.axes_colorbar[index].set_xticklabels([])
            self.axes_colorbar[index].get_xaxis().set_visible(False)
        else:
            self.axes_colorbar[index].set_yticklabels([])
            self.axes_colorbar[index].get_yaxis().set_visible(False)
        if self.colorbar_vertical:
            cbar.ax.set_ylabel(
                r"${}{}$".format(element.variable_name, units),
                rotation=270,
                labelpad=self.fontsize * 1.3,
            )
        else:
            cbar.ax.set_xlabel(r"${}{}$".format(element.variable_name, units),)

    def plot_contour(self, X, Y):
        element = self.data_elements[X][Y]
        if not element.contour:
            return
        data = element.contour_data
        contour_image = self.axes[X][Y].contour(
            data["X"],
            data["Y"],
            data["Z"],
            element.contour_levels,
            colors=element.contour_colors,
            linewidths=element.contour_linewidth,
            # alpha=ContourAlpha,
        )
        element.contour_image = contour_image
        # exit()
        print(
            f"np.shape(element.contour_image.allsegs): {np.shape(element.contour_image.allsegs)}"
        )
        print(f"element.contour_image.allsegs {element.contour_image.allsegs}")
        for i, d in enumerate(element.contour_image.allsegs):
            print(f"i:{i}: {d[:8]}")
        print(f"element.contour_image.levels: {element.contour_image.levels}")
        print(f"element.contour_image.allkinds: {element.contour_image.allkinds}")
        print(f"element.contour_levels: {element.contour_levels}")
        print(dir(element.contour_image))
        # exit()
        # aux.printDebug(element.contour_image.allsegs, dontShow=[], name="element.contour_image.allsegs")
        if element.contour_ellipse and all(
            x == y for x, y in zip(element.contour_levels, element.contour_image.levels)
        ):
            (
                element.contour_fit_params,
                element.contour_fit_coords,
                element.contour_fit_volume,
            ) = feature_extraction.get_all_ellipse_fit(
                element.contour_image.allsegs[0], min_volume=40
            )
            for vol, coords_raw in zip(
                element.contour_fit_volume, element.contour_fit_coords
            ):
                coords = []
                for c in coords_raw:
                    if self.coord_in_figure(c, X, Y):
                        coords.append(c)
                coords = np.array(coords)
                print(f"extent_figure: {element.extent_figure}")
                print(f"coords: {coords}")
                print(f"extend: {element.extent}")
                if len(coords) > 0:
                    self.axes[X][Y].scatter(*coords.T, linewidth=1, s=2)
                    # self.axes[X][Y].text(
                    #     *coords[0], f"{int(vol)}", fontsize=element.fontsize * 2 / 3
                    # )

    def coord_in_figure(self, c, X, Y):
        element = self.data_elements[X][Y]
        return (
            c[0] > element.extent_figure[0]
            and c[0] < element.extent_figure[1]
            and c[1] > element.extent_figure[2]
            and c[1] < element.extent_figure[3]
        )

    def plot_quiver(self, X, Y):
        element = self.data_elements[X][Y]
        data = element.quiver_data
        element.quiver_norm = None
        if not element.quiver:
            return
        if element.quiver_type == "streamplot":
            quiver_image = self.axes[X][Y].streamplot(
                data["X"],
                data["Y"],
                data["U"],
                data["V"],
                color=element.quiver_color,
                cmap=element.quiver_colormap,
                # alpha=element.quiver_alpha,
            )
        elif element.quiver_type == "lic":
            import lic

            def convert_data(d):
                if not element.quiver_log:
                    norm = mpl.colors.Normalize(vmin=-vmax, vmax=vmax)
                else:
                    norm = mpl.colors.SymLogNorm(vmin=-vmax, vmax=vmax, linthresh=vmin)
                # d = np.clip(
                # d, element.quiver_vmin, element.quiver_vmax
                # )
                d = norm(d)
                element.quiver_norm = norm
                return np.array(d, dtype=np.float64)

            x = convert_data(data["U"])
            y = convert_data(data["V"])
            np.save("x.npy", x)
            np.save("y.npy", y)
            debug.printDebug(data["X"], name='data["X"]')
            print(f"x: {x}")
            print(f"y: {y}")
            print(f"extent_figure: {element.extent_figure}")
            print(f"extent: {element.extent}")
            quiver_image = lic.lic(x, y, length=40)
            quiver_image = self.axes[X][Y].imshow(
                quiver_image,
                origin="lower",
                cmap=element.quiver_colormap,
                alpha=1,
                extent=element.extent_figure,
                interpolation="none",
                aspect="auto",
            )
        else:
            vmin = element.quiver_vmin
            vmax = element.quiver_vmax
            if element.quiver_colormap and element.quiver_color:
                raise Exception(
                    f"cannot use quiver_colormap {element.quiver_colormap} and quiver_color {element.quiver_color} at the same time!"
                )
            elif element.quiver_color:
                quiver_image = self.axes[X][Y].quiver(
                    data["X"],
                    data["Y"],
                    data["U"] / element.quiver_vmax,
                    data["V"] / element.quiver_vmax,
                    angles="xy",
                    pivot="mid",
                    scale=np.shape(data["X"])[0] * 2,
                    scale_units="width",
                    color=element.quiver_color,
                    # width=0.0018,
                )
            elif element.quiver_colormap:
                color_data = np.clip(
                    data["color"], element.quiver_vmin, element.quiver_vmax
                )
                if not element.quiver_log:
                    norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
                else:
                    norm = mpl.colors.LogNorm(vmin=vmin, vmax=vmax)
                quiver_image = self.axes[X][Y].quiver(
                    data["X"],
                    data["Y"],
                    data["U"],  # / element.quiver_vmax,
                    data["V"],  # / element.quiver_vmax,
                    color_data,
                    cmap=element.quiver_colormap,
                    norm=norm,  # mpl.colors.Normalize(vmin=vmin, vmax=vmax),
                    # scale=element.quiver_vmin * element.quiver_res[0],
                    # scale_units="height",
                    # scale_units='inches',
                    # angles="xy",
                    # pivot="mid",
                    # scale=np.shape(data["X"])[0],
                    # scale_units="width",
                    # color=element.quiver_color,
                )
        element.quiver_image = quiver_image

    def _get_grid_index(self, X, Y):
        return X + (Y % self.nrows_ncols[1]) * self.nrows_ncols[1]

    def show(self):
        if self.data_elements[0][0].show_plot:
            plt.show()

    def save(self):
        # plt.draw()
        element = self.data_elements[0][0]
        filename = element.filename
        aux.create_dirs_for_file(filename)
        aux.checkExistence_delete_file(filename)
        print(f".. saving {filename}")
        self.figure.savefig(filename, dpi=self.figure.get_dpi(), bbox_inches="tight")
        plt.close(self.figure)
        if os.path.exists(GRID_FOLDER) and REMOVE_GRID_FOLDER_AFTER_PLOT:
            folder = "%s" % GRID_FOLDER
            if SAVE_GRID_WITH_PROCESSOR_ID:
                folder = os.path.join(folder, element.unique_idendity)
            files = glob.glob("%s/*" % (folder))
            if element.verbose:
                print(f"initially files: {files}")
            for f in files:
                if os.path.isfile(f):
                    os.remove(f)
            files = glob.glob("%s/*" % (folder))
            if element.verbose:
                print(f"after removal: files {files}")
