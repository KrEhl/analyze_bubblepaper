import matplotlib as mpl
import datetime
import time

mpl.use("Qt5Agg")
import math

import numpy as np

import FigureMove as Fig
import auxiliary_functions as aux
from multi_show_data import SnapElements, printDebugObjectInitialisation
from multi_show_figure import PlotElement
from parameters_multishow import get_parameter_dic_multishow
import macros
import iterator
import itertools
import Multithread as mt
import os
import textwrap

class PlotObjects:
    def __init__(
        self,
        variable="Density",
        variable_name="\\rho",
        variable_name_units="\\mathrm{g}\\,\\mathrm{cm}^{-3}",
        index_variable="x",
        variable_factor=None,
        data_manipulation=None,
        snapshot_number=[0, 400],
        index_snapshot_number="y",
        folder="/home/kristian/Analysis/SnapsTest/PerseusCorrect/R7/E45/X5M1ASFCMy15Ver2020Up10p3p2020/output/",
        root=None,
        name_sim_save="test",
        name_sim="test",
        ic=False,
        index_folder="all",
        box_3d=None,
        height=100,
        width=100,
        depth=4,
        depth_max=None,
        box_units="kpc",
        center=None,
        weight=None,
        use_weight_thresholds=False,
        axis=[2, 0],
        res=300,
        res_depth=None,
        res_critical=1024,
        variable_info=None,
        plot_type="slice",
        info=None,
        #        useWeightsThresholds=False, -> use in INFO
        threads=4,
        vmin=None,
        vmax=None,
        vmin_mapping="default",
        model_vmin_vmax=None,
        fields_loaded=None,
        scale_bar="default",
        scale_bar_position="lowerleft",
        scale_bar_color=None,
        scale_bar_show_label=True,
        use_time=True,
        difference_time=6,
        lengthX=3,
        follow=None,  # focus_upper
        rotation_early=None,
        rotation=None,
        rotation_late=None,
        rotation_axis_early=[0, 0, 1],
        rotation_axis=[1, 0, 0],
        rotation_axis_late=[0, 0, 1],
        data_plot=None,
        quiver=False,
        quiver_type="quiver",
        quiver_colorbar=True,
        quiver_info=None,
        quiver_variable="MagneticField",
        quiver_variable_name="B",
        quiver_variable_name_units="\\mathrm{G}",
        quiver_weight=None,
        quiver_box_3d=None,
        quiver_res=128,  # -> preprocess to have list with len(list)=3
        quiver_res_depth=None,  # -> preprocess to have list with len(list)=3
        quiver_vmin=None,
        quiver_vmax=None,
        quiver_plot_type="slice",
        quiver_depth=4,
        quiver_depth_max=None,
        quiver_normalization=None,
        quiver_scale=1,
        quiver_log=False,
        quiver_colormap="Blues",
        quiver_color=None,
        quiver_alpha=0.5,
        quiver_skip_n=0,
        #        AlternativeBfieldFlag=True, -> quiver='alternative'
        # useMaxQuiver=False, -> quiver_info
        # lengthnormaliseQuiver=None,
        # follow_direction_perp=False, -> INFO
        # show_depth=False, -> INFO
        # follow_center=False, -> INFO
        contour=False,
        contour_info=None,
        contour_variable="Density",
        contour_levels=None,
        contour_colors=None,
        contour_plot_type="slice",
        contour_linewidth=1.0,
        contour_weight=None,
        contour_box_3d=None,
        contour_res=128,
        contour_res_depth=None,
        contour_filter=None,
        # ContourFilternum=None, -> specify number above
        # modelVmax=False, -> vmin = 'model'
        # modelVmin=False, -> vmax = 'model'
        # rewrite_times=False, -> info
        # differenceTime=2, -> info
        # follow_flexible=False,
        contour_depth=4,
        contour_depth_max=None,
        contour_ellipse=False,
        # modelfunc=None,
        # modelPara=None,
        # modelvariables=None,
        # previousCenter=None,
        # garanteeUp=False,
        # posFactorGlobal=None,
        save_folder="../figures/MultiProjections/",
        save_subfolder="MatrixPlot/",
        save_file_type=".pdf",
        common_vmin_vmax_axis=None,
        log=True,
        colormap="Spectral",
        xlim=None,
        ylim=None,
        draw_object=None,
        show_colorbar=True,
        rasterized=True,
        show_data="left",
        show_data_setup=None,
        show_time="default",
        time_format="default",
        show_folder="default",
        text_color="black",  # #708090
        text_style="",
        text_top=None,
        fontsize=14,
        dpi=250,
        length_colorbar_percent=None,
        width_colorbar=None,
        colorbar_offset=None,
        text_position="top left",
        text_position_time="bottom right",
        verbose=1,
        show_plot=False,
        parameter_as_index=None,
        show=None,
        identifier_process=True,
        check_file_exists=False,
    ):
        self.done = False
        self.variables_all = locals().copy()
        self.variables_all.pop("self")
        self.parameter_dic = get_parameter_dic_multishow()
        if show is not None:
            print("")
            if np.size(show) == 1:
                show = [show]
            for key, val in self.variables_all.items():
                if any([x in key for x in show]):
                    print(f"{key}: {val}")
            exit()
        # check if all parameters in dic present locally and vice versa
        if set(self.parameter_dic.keys()) - set(self.variables_all.keys()):
            raise Exception(
                f"parameters: {set(self.parameter_dic.keys()) - set(self.variables_all.keys())} missing in parameter_dic"
            )
        if set(self.variables_all.keys()) - set(self.parameter_dic.keys()):
            raise Exception(
                f"parameters: {set(self.variables_all.keys()) - set(self.parameter_dic.keys())} missing in self.variables_all"
            )

        self.possible_indices = ["x", "y", "xy", "all"]

        indices_dics = (
            self.get_indices_from_other_variables_to_check_for_plot_dimension()
        )

        self.sizeX, self.sizeY = self._get_size_plot(
            self.variables_all["variable"],
            self.variables_all["snapshot_number"],
            self.variables_all["folder"],
            lengthX,
            index_variable,
            index_snapshot_number,
            index_folder,
            indices_dics,
        )
        for k, sub_dic in self.parameter_dic.items():
            if sub_dic["initialize"] == "info":
                if self.variables_all[k] is None:
                    self.variables_all[k] = {}
                # if self.variables_all[k] is not None:
                self.variables_all[k] = aux.INFOSorter(
                    self.variables_all[k], FinalDimension=[self.sizeX, self.sizeY]
                )
        stop = False
        for k, sub_dic in self.parameter_dic.items():
            if stop:
                # exit()
                pass
            if k == "rotation":
                stop = True
            default_index = None
            if "index_initialize" in sub_dic:
                default_index = self.variables_all[sub_dic["index_initialize"]]
            if k in [x["key"] for x in indices_dics]:
                default_index = indices_dics[
                    [index for index, x in enumerate(indices_dics) if k == x["key"]][0]
                ]["index"]
            if "index_inherit" in sub_dic:
                if sub_dic["index_inherit"] in [x["key"] for x in indices_dics]:
                    default_index = indices_dics[
                        [
                            index
                            for index, x in enumerate(indices_dics)
                            if sub_dic["index_inherit"] == x["key"]
                        ][0]
                    ]["index"]
            if sub_dic["initialize"] == "single":
                self.variables_all[k] = self.genericObjectInitialisation(
                    self.variables_all[k],
                    self.sizeX,
                    self.sizeY,
                    k,
                    default_index=default_index,
                )
            elif sub_dic["initialize"] in ["list", "list_flexible"]:
                flexibleListSize = False
                if sub_dic["initialize"] == "list_flexible":
                    flexibleListSize = True
                self.variables_all[k] = self.genericObjectInitialisationList(
                    self.variables_all[k],
                    self.sizeX,
                    self.sizeY,
                    k,
                    flexibleListSize=flexibleListSize,
                    default_index=default_index,
                )
            elif sub_dic["initialize"] == "single_or_list":
                try:
                    self.variables_all[k] = self.genericObjectInitialisation(
                        self.variables_all[k],
                        self.sizeX,
                        self.sizeY,
                        k,
                        default_index=default_index,
                    )
                except:
                    self.variables_all[k] = self.genericObjectInitialisationList(
                        self.variables_all[k],
                        self.sizeX,
                        self.sizeY,
                        k,
                        default_index=default_index,
                    )
        self.snapelements = Fig.initialiseListNew(self.sizeX, self.sizeY, 0)
        self.VminVmax = Fig.initialiseListNew(self.sizeX, self.sizeY, 2)

    def _get_field(
        self,
        field,
        flat=False,
        remove_none=False,
        get_along_index=None,
        reduce_length=False,
        max_length=None,
        min_similarity=0.8,
        unique=False,
    ):
        lis = [[x.__dict__[field] for x in y] for y in self.snapelements[:][:]]
        list_of_list = True
        if not (get_along_index is None or get_along_index == "all"):
            if get_along_index == "x":
                lis = [
                    x.__dict__[field]
                    for x in [
                        self.snapelements[i][0] for i in range(len(self.snapelements))
                    ]
                ]
                list_of_list = False
            elif get_along_index == "y":
                lis = [x.__dict__[field] for x in self.snapelements[0][:]]
                list_of_list = False
        if flat:
            lis = aux.flatten(lis, list_of_list=list_of_list)
        if flat and remove_none:
            if type(lis[0]) != list:
                lis = [x for x in lis if x is not None]
            else:
                lis = [[x for x in y if x is not None] for y in lis]
        elif remove_none:
            if type(lis[0][0]) != list:
                lis = [[x for x in y if x is not None] for y in lis]
            else:
                lis = [[[x for x in y if x is not None] for y in z] for z in list]
        if unique:
            lis = list(np.unique(lis))
        if reduce_length:
            print(f"lis: {lis}")
            if max_length is None or len("-".join(lis)) > max_length:
                lis = aux.strings_reduce_by_matches(lis, min_similarity=min_similarity)
                lis = lis[0] + lis[1]
        return lis

    def _field_all_equal(self, field, equal_to=None):
        lis = self._get_field(field, flat=True)
        if equal_to is None:
            check = all(x == lis[0] for x in lis)
        else:
            check = all(x == equal_to for x in lis)
        return check

    def _get_name(self):
        element = self.snapelements[0][0]
        file_type = element.save_file_type
        labels = []
        values = []
        labels.append("Va")
        values.append(
            self._get_field(
                "variable",
                flat=True,
                get_along_index=element.index_variable,
                reduce_length=True,
                unique=True,
            )
        )
        labels.append("Num")
        values.append(self._get_field("snapshot_number", flat=True, unique=True))
        labels.append("Fol")
        values.append(
            self._get_field(
                "name_sim_save",
                flat=True,
                unique=True,
                reduce_length=True,
                max_length=100,
            )
        )
        labels.append("H")
        values.append(self._get_field("height", flat=True, unique=True))
        labels.append("W")
        values.append(self._get_field("width", flat=True, unique=True))
        labels.append("D")
        values.append(self._get_field("depth", flat=True, unique=True))
        labels.append("R")
        values.append(
            [
                np.max(res)
                for res in self._get_field(
                    "res",
                    flat=True,
                    remove_none=True,
                    get_along_index=element.index_variable,
                )
            ]
        )
        labels.append("A")
        values.append(self._get_field("axis", flat=True, unique=True))
        labels.append("Plot")
        values.append(
            [
                x[:3].capitalize()
                for x in self._get_field(
                    "plot_type",
                    flat=True,
                    get_along_index=element.index_variable,
                    unique=True,
                )
            ]
        )
        if len(self._get_field("colormap", flat=True)) == 1:
            labels.append("CM")
            values.append(self._get_field("colormap", flat=True))
        if not element.show_colorbar:
            labels.append("No")
            values.append(["Colorbar"])
        if self._field_all_equal(
            "show_time", equal_to="never"
        ) and self._field_all_equal("show_folder", equal_to="never"):
            labels.append("No")
            values.append(["Labels"])
        name_fragments = [
            "{}{}".format(
                nam,
                aux.convert_params_to_string(
                    x, replace=[".", "/", ":"], replace_with=["p", "", ""]
                ),
            )
            for nam, x in zip(labels, values,)
        ]
        name = "_".join(name_fragments)
        name = aux.shortenFileName(name)
        name += f"dpi{element.dpi}"
        name += file_type
        return name

    def create_data(self):
        for X in range(self.sizeX):
            for Y in range(self.sizeY):
                self.snapelements[X][Y] = SnapElements(
                    **{
                        k: v[X][Y]
                        for k, v in self.variables_all.items()
                        if "parameter" not in self.parameter_dic[k]["initialize"]
                    },
                    **{
                        k: v
                        for k, v in self.variables_all.items()
                        if "parameter" in self.parameter_dic[k]["initialize"]
                    },
                )
                self.snapelements[X][Y].prepare_parameters()

        # split up to catch early errors
        self.assign_check_filename()
        if self.done:
            return
        counter = 0
        for X in range(self.sizeX):
            for Y in range(self.sizeY):
                self.snapelements[X][Y].getSnap()
                self.snapelements[X][Y].do_rotation_follow()
                self.snapelements[X][Y].do_plot()
                self.VminVmax[X][Y][0], self.VminVmax[X][Y][1] = self.snapelements[X][
                    Y
                ].get_vmin_vmax()
                self.snapelements[X][Y].do_plot_contour()
                self.snapelements[X][Y].do_plot_quiver()
                self.snapelements[X][Y].get_vmin_vmax_quiver()
                self.snapelements[X][Y].do_data_plot()
                self.snapelements[X][Y].get_show_data()
                self.snapelements[X][Y].clean_up()
                counter += 1
                print(f"finished plot {counter}/{self.sizeX * self.sizeY}")
        self.find_common_vmin_vmax_axis()
        vmins, vmaxs = self.sortVminVmax(self.snapelements[X][Y].common_vmin_vmax_axis)
        self.findQuiverVminVmax()
        for X in range(self.sizeX):
            for Y in range(self.sizeY):
                self.snapelements[X][Y].vmin = vmins[X][Y]
                self.snapelements[X][Y].vmax = vmaxs[X][Y]

    def assign_check_filename(self):
        basename = self._get_name()
        for X in range(self.sizeX):
            for Y in range(self.sizeY):
                self.snapelements[X][Y].basename = basename
                filename = os.path.join(
                    self.snapelements[X][Y].save_folder,
                    self.snapelements[X][Y].save_subfolder,
                    basename,
                )
                self.snapelements[X][Y].filename = filename
        print(f"... working on {filename}")
        length_filename = len(basename)
        if length_filename > 255:
            raise Exception(f"filename too long! \n {basename}")
        if os.path.exists(filename) and self.snapelements[X][Y].check_file_exists:
            self.done = True

    def findQuiverVminVmax(self):
        vmin = self._get_field("quiver_vmin")
        vmax = self._get_field("quiver_vmax")

    def find_common_vmin_vmax_axis(self):
        if not self._field_all_equal("common_vmin_vmax_axis"):
            raise Exception("common_vmin_vmax_axis needs to be same for all snapshots!")
        if not self._field_all_equal("common_vmin_vmax_axis", None):
            return
        if (
            self._field_all_equal("variable")
            and self._field_all_equal("info")
            and self._field_all_equal("log")
            and self._field_all_equal("colormap")
        ):
            for x in aux.flatten(self.snapelements):
                x.common_vmin_vmax_axis = "xy"
        else:
            if not self._field_all_equal("index_variable"):
                raise Exception("index variable varies between snapshots!")
            index_variable = self.snapelements[0][0].index_variable
            common_vmin_vmax_axis = index_variable
            if index_variable == "xy":
                raise Exception(
                    "common_vmin_vmax_axis setting ambiguous, please specify!"
                )
            if index_variable == "all":
                common_vmin_vmax_axis = "xy"
            elif index_variable == "x":
                common_vmin_vmax_axis = "y"
            elif index_variable == "y":
                common_vmin_vmax_axis = "x"
            for x in aux.flatten(self.snapelements):
                x.common_vmin_vmax_axis = common_vmin_vmax_axis

    def _field_all_equal(self, field, equal_to=None):
        lis = self._get_field(field, flat=True)
        if equal_to is None:
            check = all(x == lis[0] for x in lis)
        else:
            check = all(x == equal_to for x in lis)
        return check

    def sortVminVmax(
        self,
        common_vmin_vmax_axis,
        saveVminVmax=False,
        header=None,
        filename="test.txt",
    ):
        if saveVminVmax and header is None:
            print(header)
            exit("need header to save vminvmax!")

        sizeX, sizeY = np.shape(self.VminVmax)[0], np.shape(self.VminVmax)[1]
        if not common_vmin_vmax_axis == "x" and common_vmin_vmax_axis is not None:
            vmin = [self.VminVmax[i][0][0] for i in range(sizeX)]
            vmax = [self.VminVmax[i][0][1] for i in range(sizeX)]

        if common_vmin_vmax_axis is None:
            vmin = [[x[0] for x in y] for y in self.VminVmax]
            vmax = [[x[1] for x in y] for y in self.VminVmax]

        elif common_vmin_vmax_axis == "xy":
            vmin = self.VminVmax[0][0][0]
            vmax = self.VminVmax[0][0][1]
            for i in range(sizeX):
                for j in range(sizeY):
                    if self.VminVmax[i][j][0] < vmin:
                        vmin = self.VminVmax[i][j][0]
                    if self.VminVmax[i][j][1] > vmax:
                        vmax = self.VminVmax[i][j][1]
            if vmin == vmax:
                vmax = 2 * vmin + 1
                if vmin == 0:
                    vmax = 1
            vmin = [[vmin for y in range(sizeY)] for x in range(sizeX)]
            vmax = [[vmax for y in range(sizeY)] for x in range(sizeX)]

        elif common_vmin_vmax_axis == "y":
            for i in range(sizeX):
                for j in range(sizeY):
                    if self.VminVmax[i][j][0] < vmin[i]:
                        vmin[i] = self.VminVmax[i][j][0]
                    if self.VminVmax[i][j][1] > vmax[i]:
                        vmax[i] = self.VminVmax[i][j][1]
            vmin = [[x for i in range(sizeY)] for x in vmin]
            vmax = [[x for i in range(sizeY)] for x in vmax]

        elif common_vmin_vmax_axis == "x":
            vmin = [self.VminVmax[0][i][0] for i in range(sizeY)]
            vmax = [self.VminVmax[0][i][1] for i in range(sizeY)]
            for i in range(sizeX):
                for j in range(sizeY):
                    if self.VminVmax[i][j][0] < vmin[j]:
                        vmin[j] = self.VminVmax[i][j][0]
                    if self.VminVmax[i][j][1] > vmax[j]:
                        vmax[j] = self.VminVmax[i][j][1]
            vmin = [[x for x in vmin] for i in range(sizeX)]
            vmax = [[x for x in vmax] for i in range(sizeX)]
        else:
            raise Exception(
                f"common_vmin_vmax_axis: {common_vmin_vmax_axis} not implemented yet in sortVminVmax()"
            )
        if saveVminVmax:
            vminSave = np.array(vmin).flatten()
            vmaxSave = np.array(vmax).flatten()
            combined = list(vminSave) + list(vmaxSave)
            if np.size(combined) / 2 != np.size(header):
                print(combined)
                print(header)
                exit("incorrect header size to save vmin vmaxes!")
            else:
                import MultiImshow as MI

                header = ["%sVmin" % x for x in header] + ["%sVmax" % x for x in header]
                MI.appendFile(filename, tuple(combined), tuple(header))
        return vmin, vmax

    def genericObjectInitialisationList(
        self,
        object,
        sizeX,
        sizeY,
        objectName="object",
        flexibleListSize=False,
        default_index=None,
    ):
        if object is None or type(object[0]) != list and type(object[0]) != np.ndarray:
            object = [[object for y in range(sizeY)] for x in range(sizeX)]
        else:
            if object[-1] not in self.possible_indices and default_index is not None:
                object += [default_index]
            if object[-1] == "x":
                object = [[x for y in range(sizeY)] for x in object[:-1]]
            elif object[-1] == "y":
                object = [[y for y in object[:-1]] for x in range(sizeX)]
            elif object[-1] == "all":
                object = [[object[:-1][0] for y in range(sizeY)] for x in range(sizeX)]
            elif object[-1] == "xy":
                object = object[:-1]
                object = [
                    [object[y * sizeX + x] for y in range(sizeY)] for x in range(sizeX)
                ]
            else:
                printDebugObjectInitialisation(
                    object, sizeX, sizeY, objectName, default_index, flexibleListSize
                )
                raise Exception(
                    "have to specify 'x', 'y', 'all' or 'xy' at the end of list"
                )

            if not flexibleListSize:
                if np.shape(object)[:-1] != (sizeX, sizeY):
                    printDebugObjectInitialisation(
                        object,
                        sizeX,
                        sizeY,
                        objectName,
                        default_index,
                        flexibleListSize,
                    )
                    raise Exception(
                        "expected different shape for object, specified too few/many items in list"
                    )
            else:
                if len(np.shape(object)) == 2:
                    if np.shape(object)[:] != (sizeX, sizeY):
                        printDebugObjectInitialisation(
                            object,
                            sizeX,
                            sizeY,
                            objectName,
                            default_index,
                            flexibleListSize,
                        )
                        raise Exception(
                            "expected different shape for object, specified too few/many items in list"
                        )
                elif len(np.shape(object)) > 2:
                    if np.shape(object)[:-1] != (sizeX, sizeY):
                        printDebugObjectInitialisation(
                            object,
                            sizeX,
                            sizeY,
                            objectName,
                            default_index,
                            flexibleListSize,
                        )
                        raise Exception(
                            "expected different shape for object, specified too few/many items in list"
                        )
        return object

    def genericObjectInitialisation(
        self, object, sizeX, sizeY, objectName="object", default_index=None
    ):
        print(
            f"object, sizeX, sizeY, objectName, default_index: {object, sizeX, sizeY, objectName, default_index}"
        )
        if type(object) != list and type(object) != np.ndarray:
            object = [[object for y in range(sizeY)] for x in range(sizeX)]
        else:
            if object[-1] not in self.possible_indices and default_index is not None:
                object += [default_index]
            if object[-1] == "x":
                object = [[x for y in range(sizeY)] for x in object[:-1]]
            elif object[-1] == "y":
                object = [[y for y in object[:-1]] for x in range(sizeX)]
            elif object[-1] == "all":
                object = object[0]
                object = [[object for y in range(sizeY)] for x in range(sizeX)]
            elif object[-1] == "xy":
                object = object[:-1]
                object = [
                    [object[y * sizeX + x] for y in range(sizeY)] for x in range(sizeX)
                ]
            else:
                printDebugObjectInitialisation(
                    object, sizeX, sizeY, objectName, default_index
                )
                raise Exception(
                    "have to specify 'x', 'y', 'all' or 'xy' at the end of list"
                )

            if np.shape(object)[:] != (sizeX, sizeY):
                printDebugObjectInitialisation(
                    object, sizeX, sizeY, objectName, default_index
                )
                raise Exception(
                    "expected different shape for %s, specified too few/many items in list"
                    % objectName
                )
        return object

    def plot_data(self):
        self.plot_element = PlotElement(self.snapelements)
        self.plot_element.parameter_setup()
        self.plot_element.initialize_figure(sizeX=self.sizeX, sizeY=self.sizeY)
        for X in range(self.sizeX):
            for Y in range(self.sizeY):
                self.plot_element.plot_data(X, Y)
                self.plot_element.plot_colorbars(X, Y)
                self.plot_element.plot_contour(X, Y)
                self.plot_element.plot_quiver(X, Y)
                self.plot_element.plot_colorbars_above(X, Y)
                self.plot_element.text(X, Y)
        self.plot_element.save()
        self.plot_element.show()
        return

    def get_indices_from_other_variables_to_check_for_plot_dimension(self):
        dics = []
        parameter_as_index = self.variables_all.pop("parameter_as_index")
        for key, value in self.variables_all.items():
            if "index_inherit" not in self.parameter_dic[key]:
                is_index, index = self.is_get_index_axis_generally(value)
                if is_index:
                    dic = {}
                    dic[f"index"] = index
                    dic[f"key"] = key
                    dic[f"value"] = value
                    dics.append(dic)
        if parameter_as_index is None:
            pass
        elif aux.isIterable(parameter_as_index):
            keys = parameter_as_index[::2]
            indices = parameter_as_index[1::2]
            for key, index in zip(keys, indices):
                dic = {}
                dic[f"index"] = index
                dic[f"key"] = key
                dic[f"value"] = self.variables_all[key]
                dics.append(dic)
        return dics

    def is_get_index_axis_generally(self, var):
        """

        Parameters
        ----------
        var
        name

        Returns: is_index_present, index else None
        -------

        """
        var_index = None
        if not aux.isIterable(var):
            return False, var_index
        if type(var) != str and var[-1] in self.possible_indices:
            var_index = var.pop(-1)
            return True, var_index
        return False, var_index

    def _get_index_axis(self, var, index, name):
        if not aux.isIterable(var):
            return index
        if type(var) != str and var[-1] in self.possible_indices:
            var_index = var.pop(-1)
            if index != var_index:
                raise Exception(
                    f"{name}: cannot specify different index in variables: {var} and index_variables: {index}"
                )
            index = var_index
        elif index is None:
            raise Exception(
                f"{name}: neither index: {index} directly specified nor in variable: {var}"
            )
        return index

    def _get_size_plot(
        self,
        variables,
        snapshot_number,
        folder,
        lengthX,
        index_variables,
        index_snapshot_number,
        index_folder,
        extra_indices_dic_list,
    ):
        index_variables = self._get_index_axis(variables, index_variables, "variables")
        index_snapshot_number = self._get_index_axis(
            snapshot_number, index_snapshot_number, "snapshot_number"
        )
        index_folder = self._get_index_axis(folder, index_folder, "folder")
        all_indices = [index_variables, index_snapshot_number, index_folder] + [
            x["index"] for x in extra_indices_dic_list
        ]
        all_vars = [
            x if aux.isIterable(x) else [x]
            for x in [variables, snapshot_number, folder]
        ] + [x["value"] for x in extra_indices_dic_list]
        all_lengths = [len(x) if aux.isIterable(x) else 1 for x in all_vars]

        def debugPrint():
            print(
                f"index_variables, index_snapshot_number, index_folder: {all_indices}"
            )
            print(f"variables, snapshot_number, folder: {all_vars}")
            print(f"all_lengths: {all_lengths}")
            print(f"extra_indices_dic_list: {extra_indices_dic_list}")

        for index in self.possible_indices:
            mask = np.array(all_indices) == index
            lengths = [len(x) for x in np.array(all_vars)[mask]]
            if lengths:
                if any([x != lengths[0] for x in lengths]):
                    debugPrint()
                    raise Exception(
                        "same index used for different vars with different lengths!"
                    )
        size_x = None
        size_y = None
        if "x" in all_indices:
            size_x = np.array(all_lengths)[np.array(all_indices) == "x"][0]
        if "y" in all_indices:
            size_y = np.array(all_lengths)[np.array(all_indices) == "y"][0]
        if "xy" in all_indices:
            size_xy = np.array(all_lengths)[np.array(all_indices) == "xy"][0]
            if size_xy % lengthX != 0:
                print(f"xy will have residual plots with lengthX: {lengthX}")
            old_size_x = size_x
            old_size_y = size_y
            size_x = lengthX
            if old_size_x is not None and old_size_x != size_x:
                debugPrint()
                raise Exception("inconsistent index defintions")
            size_y = math.ceil(size_xy / lengthX)
            if old_size_y is not None and old_size_y != size_y:
                debugPrint()
                raise Exception("inconsistent index defintions")
        if size_x is None or size_y is None:
            debugPrint()
            raise Exception(
                f"size_x: {size_x} or size_y: {size_y} couldnt be determined!"
            )
        return size_x, size_y


def main_loop(input):
    PlotObj = PlotObjects(**input)
    PlotObj.create_data()
    if PlotObj.done:
        del PlotObj
        return
    PlotObj.plot_data()
    del PlotObj


def clean_input_list(input_list):
    print(input_list)
    for input in input_list:
        print(f"input:")
        print(input)
    # raise Exception("nit implemtend")


if __name__ == "__main__":
    import argparse
    import argparse_helpers

    start_time = time.time()
    parser = argparse.ArgumentParser(
        prog = 'PROG',
        formatter_class = argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
        generate images from snapshots
        use macros as specficied in corresponding .yaml files 
        (Indicate negative scientific notation with either N,n,M,m)
        --show VARIABLE to look for specifc parameter names
        '''),
    )
    parameter_dic = get_parameter_dic_multishow()
    # here most keyword arguments are imported and parsed
    for k, sub_dic in parameter_dic.items():
        parse_type = aux.checkExistenceKey(sub_dic, "parse_type", None)
        if parse_type == "fancy":
            parse_type = argparse_helpers.fancy_eval
        nargs = aux.checkExistenceKey(sub_dic, "nargs", None)
        parser.add_argument(f"--{k}", type=parse_type, nargs=nargs)
    parser.add_argument(f"--meta", type=argparse_helpers.fancy_eval, nargs="+")
    parser.add_argument(
        f"--iterator", type=argparse_helpers.fancy_eval, nargs="+", default=[None]
    )
    parser.add_argument(
        f"--threads_main", type=argparse_helpers.fancy_eval, nargs=None, default=4
    )
    input = parser.parse_args().__dict__.copy()
    input = {k: v for k, v in input.items() if v is not None}
    # clean unnecessary double lists
    for k, v in input.copy().items():
        if aux.isIterable(v):
            if len(v) == 1:
                if aux.isIterable(v[0]):
                    input[k] = v[0]

    identifier_macros = lambda x: False if type(x) != str else x.startswith("_")
    convert_macros_to_name = lambda x: x[1:]
    # input = macros.insert_macros2(
    #     input,
    #     identifier_macros=identifier_macros,
    #     convert_macros_to_name=convert_macros_to_name,
    #     meta_macro_file="mulit_show_meta_macros.yaml",
    #     parameters=get_parameter_dic_multishow(),
    #     parser_types={"fancy": argparse_helpers.fancy_eval},
    #     super_key_order=["meta", "variable", "quiver", "contour"],
    #     default_values=aux.get_default_args(PlotObjects.__init__),
    # )
    print(f"input: {input}")
    input = macros.insert_macros(
        input,
        identifier_macros=identifier_macros,
        convert_macros_to_name=convert_macros_to_name,
        meta_macro_file="multi_show_meta_macros.yaml",
        parameters=get_parameter_dic_multishow(),
        parser_types={"fancy": argparse_helpers.fancy_eval},
        super_key_order=["meta", "variable", "quiver", "contour"],
    )
    print(f"input after macros: {input}")
    input = {
        k: v[0] if type(v) == list and len(v) == 1 else v for k, v in input.items()
    }
    print(f"input after macros 2: {input}")
    threads_main = input.pop("threads_main")
    if input["iterator"] is None:
        input_list = [input]
    else:
        iterations = iterator.get_iterations(
            input["iterator"], get_parameter_dic_multishow(), input.copy()
        )
        print(f"iterations; {iterations}")
        input_list = []
        index_iterations = list(
            itertools.product(*[np.arange(len(x[1])) for x in iterations.items()])
        )
        for indices in index_iterations:
            version_input = input.copy()
            for i, (k, value_list) in enumerate(iterations.items()):
                version_input[k] = value_list[indices[i]]
            input_list.append(version_input)
    [x.pop("iterator") for x in input_list]
    clean_input_list(input_list)
    if threads_main > 1 and len(input_list) > 1:
        if len(input_list) < threads_main:
            threads_main = len(input_list)
        bd = mt.BigData(
            main_loop,
            [x for x in np.arange(len(input_list))],
            [input_list],
            1,
            None,
            "numpy",
        )
        bd.do_multi(threads_main)
    else:
        for input in input_list:
            main_loop(input)
    print(f"finished plots at {datetime.datetime.now()}")
    print(
        f"plotting took: {'%i'%((time.time()-start_time)//60)} min  {'%.2f'%((time.time()-start_time)%60)} s"
    )
