git pull

#p Radprofs.py --setup nexraywei temperaturexraywei timescalesextended --folder _perseusfid --numbers 10  --split_setups_in_panels setup --fontsize 10 --xmin_show 9
#p PhaseDiagramGeneric.py --folder _perseusfid --setup tempnecenter100 --numbers 1250  --paper --axes_text "Jet is active"
#p PhaseDiagramGeneric.py --folder _perseusfid --setup tempnecenter100 --numbers 1040  --paper --axes_text "Jet is inactive" 

#p Radprofs.py --folder _perseuspaperminhighres --setup nexraywei temperaturexraywei entropyxraywei thermalpressurexraywei --numbers_min 400 --numbers_max 2000 --numbers_step 400 --split_setups_in_panels foldersetup --length_colorbar_horizontal_percent 0.9 --differencetime 10 --threads 15 --xmin_show 2 --xmax_show 700 --fontsize 11 --width_colorbar_horizontal 0.018 --locFolderLegend "upper right" 
#p Radprofs.py --folder _perseuspaperminhighresnobh --setup nexraywei temperaturexraywei entropyxraywei thermalpressurexraywei --numbers_min 400 --numbers_max 2000 --numbers_step 400 --split_setups_in_panels foldersetup --length_colorbar_horizontal_percent 0.9 --differencetime 10 --threads 15 --xmin_show 2 --xmax_show 700 --fontsize 11 --width_colorbar_horizontal 0.018 --locFolderLegend "upper right" --overplot_folder [5] --overplot_folder_location_folder [1] --overplot_plot_color black --overplot_folder_include_times 1200 --overplot_plot_tickstyle dashed --overplot_plot_alpha 0.6
## debug:
#p Radprofs.py --folder _perseusduohighresnobh --setup nexraywei temperaturexraywei entropyxrayweidebug entropyxrayweidividedebug thermalpressurexraywei --numbers_min 400 --numbers_max 2000 --numbers_step 400 --split_setups_in_panels foldersetup --length_colorbar_horizontal_percent 0.9 --differencetime 10 --threads 15 --xmin_show 0.5 --xmax_show 700 --fontsize 9 --width_colorbar_horizontal 0.018 --locFolderLegend "upper right" --use_own_ticks_x True --number_max_x_ticks 6 --use_own_ticks_y True --number_max_y_ticks 6
#p Radprofs.py --folder _perseuspaperminhighresnobh --setup ne temperature entropy thermalpressure sfrcumsum --numbers_min 400 --numbers_max 2000 --numbers_step 400 --split_setups_in_panels foldersetup --length_colorbar_horizontal_percent 0.9 --differencetime 10 --threads 15 --xmin_show 5e-1 --xmax_show 700 --fontsize 9 --width_colorbar_horizontal 0.018 --locFolderLegend "upper right" 
#p Radprofs.py --folder _perseuspapertripplehighres --setup ne temperature entropy thermalpressure sfrcumsum --numbers_min 400 --numbers_max 2000 --numbers_step 400 --split_setups_in_panels foldersetup --length_colorbar_horizontal_percent 0.9 --differencetime 10 --threads 15 --xmin_show 1e-1 --xmax_show 700 --fontsize 9 --width_colorbar_horizontal 0.018 --locFolderLegend "upper right" 

#p Radprofs.py --folder _perseuspaperminhighres --setup tcoolxraywei coolingfreefalltimeratioxraywei colgasmassctB100 --numbers_min 400 --numbers_max 2000 --numbers_step 400 --split_setups_in_panels foldersetup --length_colorbar_horizontal_percent 0.9 --differencetime 10 --threads 15 --xmin_show 2 --xmax_show 200 --fontsize 11 --width_colorbar_horizontal 0.018 --locFolderLegend "upper left" --nbins 60  --colorbar_horizontal_offset 0.1 #xmax_show 700 #starforminggasmasssumnorm

#p GridPlotParallel.py --folder _perseuspapercoolmin --setup wang --paper --xmax 2100 --single_xlabel --space_below 0.04 --time_values_max 2000 --show_reductions_x_max 2000 --show_reductions_x_min 500 --alpha_folder True
#p GridPlotParallel.py --folder _perseuspapercoolmin --setup wangp --paper --xmax 2100 --single_xlabel --space_below 0.04 --time_values_max 2000 --show_reductions_x_max 2000 --show_reductions_x_min 500 --alpha_folder True
#p GridPlotParallel.py --folder _perseuspapermindense --setup gasphasesreduced --paper --fontsize 11 --wspace 0.08 --space_below 0.09 --alpha_folder True --legend_folder_position "lower right"

#p multi_show_main.py --folder _perseuspaperduodense --index_snapshot_number all --index_folder y --index_variable x --meta _ellipseexplanation --snapshot_number 1710 --show_time once --threads 16 --res_depth REP_8192 --res_critical 2048 --scale_bar 50 --height REP_200 --width REP_200 --contour_res RES_133 

#p GridPlotParallel.py --folder _perseuspapermindense --setup jetpowerhist --paper --split_by_folder y --fontsize 9 --wspace 0 --space_below 0.016 --single_xlabel --single_ylabel --minimalist --time_values_min 500 --time_values_max 2000 --hist_bin_number 70 #--ignore_histogram_weights                                                                                                                                       
#p GridPlotParallel.py --folder _perseuspapermindense --setup jetpowervscooling --fontsize 10 --paper --time_values_min 500 --time_values_max 2000 #--skip_folders [[],[1]]

#p histogram_plot.py --folder _perseuspapertrippledense --numbers 1000 1300 1500 2000 --averaging_range 40 --setup angularmomentumglobal --fontsize 10 --plot_style line --histogram_average_style medianpercentiles --nbins 50 --plot_color_as_circle --paper

#p Radprofs.py --folder _perseusfid --setup pthsfr pthcold pthhot --fontsize 10 --width_colorbar_horizontal 0.018 --xmin_show 2 --xmax_show 100 --ymin 8e-13 --ymax 4e-9 --numbers_min 800 --numbers_max 2000 --numbers_step 400 --differencetime 10 --colorbar_horizontal_offset 0.23 --time_average_range 160 --split_setups_in_panels setupfolder --single_xlabel False --single_ylabel False --fontsize 8 --colors_to_sample_from_colorbar 400 800 1200 1600 2000 --axes_text "$\\mathrm{cold}\\ \\mathrm{phase}$" "$\\mathrm{warm}\\ \\mathrm{phase}$" "$\\mathrm{hot}\\ \\mathrm{phase}$" --axes_text_position "far upper left"

#p Radprofs.py --folder _perseusfid --setup pthsfr pthcold pthhot --fontsize 10 --width_colorbar_horizontal 0.018 --xmin_show 2 --xmax_show 100 --ymin 8e-13 --ymax 4e-9 --numbers_min 800 --numbers_max 2000 --numbers_step 400 --differencetime 10 --colorbar_horizontal_offset 0.23 --time_average_range 160 --split_setups_in_panels setup --single_xlabel False --single_ylabel False --show_single_folder True --fontsize 8 --colors_to_sample_from_colorbar 400 800 1200 1600 2000 --axes_text "$\\mathrm{cold}\\ \\mathrm{phase}$" "$\\mathrm{warm}\\ \\mathrm{phase}$" "$\\mathrm{hot}\\ \\mathrm{phase}$" --axes_text_position "far upper left"

#p GridPlotParallel.py --folder _perseuspaperduovarypsame --setup wangp --paper --xmax 2100 --single_xlabel --space_below 0.04 --time_values_max 2000 --show_reductions_x_max 2000 --show_reductions_x_min 500 --grouping_color 3 3 --grouping_alphas 3 3
#debug:
#p GridPlotParallel.py --folder _perseuspapertripplehighres --setup wangp --paper --xmax 2100 --single_xlabel --space_below 0.04 --time_values_max 2000 --show_reductions_x_max 2000 --show_reductions_x_min 500 --grouping_color 2 2 2 --grouping_alphas 2 2 2
#p GridPlotParallel.py --folder _perseuspaperduovarypsame --setup gasphasesreduced --paper --fontsize 11 --wspace 0.08 --space_below 0.09 --alpha_folder True --grouping_color 3 3 --grouping_alphas 3 3

#p multi_show_main.py --folder _perseusfidhighres --meta _jetfeedbackoverview --snapshot_number 1000 1200 1400 1600 --res_depth REP_8192 --threads 32 --height 120 --width 120 --res 512 --text_position_time "top right" --fontsize 12 --text_color white --res_critical 2048
#p multi_show_main.py --folder _perseusfid --meta _jetfeedbackoverview --snapshot_number 1000 1200 1400 1600 --res_depth REP_8192 --threads 32 --height 120 --width 120 --res 512 --text_position_time "top right" --fontsize 12 --res_critical 2048 --text_color white

#p GridPlotParallel.py --folder _perseuspaperhighres --setup wangp --paper --xmax 2100 --single_xlabel --space_below 0.04 --time_values_max 2000 --show_reductions_x_max 2000 --show_reductions_x_min 500  --grouping_color 2 2 --skip_colors 0 0 1 --grouping_alphas 2 2 --alpha_inverted

#p multi_show_main.py --meta _hitomivelocitiesfolders --folder _perseuspaperminhighresb --snapshot_number 1500 --threads 32 --threads_main 1 --res_critical 4096 --res_depth REP_8192 --depth_max 4000 --fontsize REP_12

#p histogram_plot.py --folder _perseuspaperhdres --numbers 1400 --setup angularmomentumglobal --fontsize 20 --show_histograms False --vary_along_x folder --vary_along_y False --heightwidth 25 --scale_bar 10

#p GridPlotParallel.py --folder _perseuspapervelocitydebugextended --setup kineticenergyreduced --paper --xmax 2100 --time_values_max 2000 --show_reductions_x_max 2000  --fontsize 10