import macros
import auxiliary_functions as aux
import argparse_helpers
import script_parameteres_general


def get_parameters_from_yaml(input):
    """

    Parameters
    ----------
    input: dic of input parameters

    Returns
    -------

    """
    for k, v in input.copy().items():
        if aux.isIterable(v):
            if len(v) == 1:
                if aux.isIterable(v[0]):
                    input[k] = v[0]

    print(f"input: {input}")
    identifier_macros = lambda x: False if type(x) != str else x.startswith("_")
    convert_macros_to_name = lambda x: x[1:]
    input = macros.insert_macros(
        input,
        identifier_macros=identifier_macros,
        convert_macros_to_name=convert_macros_to_name,
        meta_macro_file="multi_show_meta_macros.yaml",
        parameters=script_parameteres_general.get_parameter_dic(),
        parser_types={"fancy": argparse_helpers.fancy_eval},
        super_key_order=["meta", "variable", "quiver", "contour"],
    )
    input = {
        k: v[0] if type(v) == list and len(v) == 1 else v for k, v in input.items()
    }
    print(input)
