def get_parameter_dic():
    parameter_dic = {
        "root": {
            "parse_type": "fancy",
            "nargs": "+",
            "initialize": "single",
            "index_initialize": "index_variable",
        },
        "folder": {
            "parse_type": "fancy",
            "nargs": "+",
            "initialize": "single",
            "index_initialize": "index_variable",
            "inherits_macro": "variable",
        },
        "name_sim": {
            "parse_type": "fancy",
            "nargs": "+",
            "initialize": "single",
            "index_initialize": "index_variable",
            "inherits_macro": "variable",
        },
        "name_sim_save": {
            "parse_type": "fancy",
            "nargs": "+",
            "initialize": "single",
            "index_initialize": "index_variable",
            "inherits_macro": "variable",
        },
    }
    return parameter_dic
