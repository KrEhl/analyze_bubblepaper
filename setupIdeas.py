class Fourier:
    def __init__(self, spaceDim, fieldDim, filterFT=None, filterNummer=1):
        """
			Define all parameters relevant to the Fourier transform here
			Parameters relevant to plotting should be defined in the plotting routines
		"""

    def plotFT(self, ax=None, Nbins=100, abs=True):
        self.relevantParameters += locals()
        self.checkIfSaved()

        ax.plot()

    def checkIfSaved(self):
        pass


# should not use INFO variables as class attributes
# if specifically call methodName should be able to call 'variable' directly (not 'methodName_variable')
# save in hdf5 file as methodName_variable

# FT.INFO, should work
# do not save None attributes and/or attributes with same beginning as attributes
# have None attributes in self.__dict__
# when loading plot should initilize dics as INFO={}
# need to save dictionaries as dictNameDICT__parameter = ..
# adopt loading accordingly

# FT.getFieldSnap()
# FT.plotFT(powerspec=True, ax=ax)

# plotFT(automatically check if need to redo )


class MasterPlot:
    def __init__(self):
        return

    def loadIndivPlots(self):
        """
		load Templates and parameters for individual plots
		spawn them with this information
		givem them field self.order -> projection + contours or quiver could thus be individual plots -> makes them easier reusable, 
		need self.type to make clear how to be plotted later 
		"""
        return

    def spawnIndivPlots(self):
        """
		instantiate Plot classes for individual plots
		"""

    def sortPlots(self):
        """
		make sure simulations not imported multiple times
		"""
        return

    def executeIndivPlots(self):
        """
		do plots in predefined order (multithread some cpu light plots)
		"""
        return


class Plot(SC.SAVER):
    def __init__(self):
        return

    def get_Plot(self, ax=None, Nbins=100, abs=True):
        """
		get plot (and save) or load it 
		"""

    def prepare_InformationToDisplayPlot(self, ax=None, Nbins=100, abs=True):
        """
		make sure know everything for actual plot -> tick positions (probably should save self.ProjBox, self.axis,.... 
		for this or just use whats provided when plotting)
		"""


# structure for plot object: master.panel[0,2].plots[0].variable
# -> master spawns list of panels (which have axes, ) which handle individual plots which can have multiple plots per image, e.g. all radprofs on single panel
# -> master.panels should know how many panels exist
# -> master.panels.preparePlots() -> should call plots
# -> master.getPanels()
# -> master.panels[:,:].plots[:].


class MasterOrganizer:
    def __init__(self):
        return

    def getPanelIndices(self):

        self.indicies = calculateSth()

    def getPanels(self):
        self.panels = [[Plotter() for i in j] for l in i, l in zip(self.indicies)]

    @property
    def getPanelsList(self):
        self.panelsList = sth()

    def doPlots(self):
        for panel in self.panelsList:
            panel.getPlots()
            panel.checkCommonWork()
        self.checkCommonWork()
        self.buildSheduler()
        for panel in self.panelsList:
            self.distributeWork()

    def checkCommonWork():
        for panel in self.panelsList():
            self.neededSnap.append(panel.snapSNeeded)
            self.neededSnapID.append(panelID)


class Plotter:
    def __init__(self):
        return

    def getPlots():
        self.plots = [[Radprof(), Imshow()][i] for i in plotTypes]
        self.getSnapNames = getNames()
        # similar idea at least


# individual plot types
class Radprof:
    def __init__(self):
        return


class Imshow:
    def __init__(self):
        return


"""
-> Templates.txt: pyyaml should be able to save these to dictionary!

TEMPLATENAME: EnergyDensityProjectionBubbleSimulation

variable: EnergyDensity
weight: None
plottype: projection
depth: 0.4
res: 128 
resDepth: 256
axis: [2,0]

version: HighRes
res: 256
resDepth: 1024

{
    user PARSER to make plotting less tedious & make bash scripts more convenient!
    default usage: python MatrixPlot.py
                                --template EnergyDensityProjectionBubbleSimulation
    overwrite any part of template with more specific templates: python MatrixPlot.py
                                --template EnergyDensityProjectionBubbleSimulation
                                --resolutionTemplate highres
                                --boxTemplate large
    overwrite any part of template with specific field: python MatrixPlot.py
                                --template EnergyDensityProjectionBubbleSimulation
                                --res 1024
}
-> Simulations.txt:

SimName: R8
SimText: low res jet (does it work?)
SimFolder: /llustre/kristian/JetSIms/R6/..../output/
SimName: R7
SimText: medium res jet (does it work?)
SimFolder: /llustre/kristian/JetSIms/R7/..../output/
"""


# DEBUG TIME
"""
python -m cProfile -o profile.dat MatrixMultiImshow.py
python -m pstats profile.dat
help
usefule commands:
	stats 10
	sort time
"""

# NUMPY
"""
use numpy.writetxt/numpy.loadtxt
speed up long calculations: 
import numexpr
poly = numexpr.evaluate("((.25*x + .75)*x - 1.5)*x - 2")
use more threads:
numexpr.set_num_threads(n)
"""

"""
GridPlotClean:
parallelize data calculations
make possible to show ::2 values
open file: check what you need, retrieve all!
initialize lists with known number of plots at beginning of plot

zipped = ((filename1, filename2), (header))
X,Y, Labels = getData(zipped)
for filename, header, function in zipped():
	getCSV()

ax.plot(X[0], Y[0], label=labels[0])

retrieveDF: generate pandas df with correct dimensions: need to expose sortby variables, i.e. snapshot numbers/folders in single cell each
use convert RetrivingValuesFromStrings (hash to values (row)) to get parameters to check if all values already calculated
jobDF: expand list of snapshot, sort by ()
dataframe with hashes + job index -> give each processor job indices to work on (need to combine lists to single list for each variable afterwards?) 

"""
# CYTHON
"""
from distutils.core import setup
from Cython.Build import cythonize

setup(
     ext_modules=cythonize("SimpleExt.pyx", compiler_directives={'language_level' : "3"}),
)
python setup.py build_ext --inplace
#check consistency of types in cython file
cython -a filename.cyt
"""

# __ini_subclass__ -> use meta classes to check if necessary methods present in class

"""
GridPlotParallel.py
-- generate job list -> need to define type of job (.txt file/ snapshot based)
-- generate hashed table to find jobs using same snapshot
-- distribute jobs and finish them
-- retrieve results
"""


"""
macros for multi_show_main.py:

replace at location of macro:
e.g.:
variable = _Tracer (log=False), Density ()
log = False, default!
box = [[[20,30,20],[40,20,40]], [10,20,10]]
"""
