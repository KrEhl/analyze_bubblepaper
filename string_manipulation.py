import re
import difflib
import itertools
import numpy as np


def split_numbers_letters(combined):
    return re.split(r"([+-]?[0-9]*[.[0-9]+]?|[_:]+)", combined)


def clean_list(lis, remove=["", "_", ":"]):
    return [x for x in lis if x not in remove]


def dictionary_from_split(combined, split_type="numbers_letters"):
    if split_type == "numbers_letters":
        split = split_numbers_letters(combined)
    else:
        raise Exception(f"split_type: {split_type} not implemented yet!")
    split = clean_list(split)
    dic = {}
    key = split[slice(None, None, 2)]
    elements = split[slice(1, None, 2)]
    for k, el in zip(key, elements):
        if split_type == "numbers_letters":
            dic[k] = float(el)
    return dic


def reduce_same_starting_string(
    strings, delimeter_pre="--", delimeter_sub="-", delimeter_main="---"
):
    pairs = list(itertools.combinations(strings, 2))
    beginnings = []
    dic = {s: [] for s in strings}
    for p in pairs:
        for a, b, size in difflib.SequenceMatcher(None, *p).get_matching_blocks():
            if a == 0 and b == 0 and size > 1:
                match = p[0][:size]
                if match not in dic[p[0]]:
                    dic[p[0]].append(match)
                if match not in dic[p[1]]:
                    dic[p[1]].append(match)
                beginnings.append(match)
    matches = [x for y in dic.values() for x in y]
    matches = np.unique(matches).tolist()
    name = find_most_reduced_string(
        strings,
        matches,
        delimeter_pre=delimeter_pre,
        delimeter_sub=delimeter_sub,
        delimeter_main=delimeter_main,
    )
    return name


def find_most_reduced_string(
    strings, matches, delimeter_pre="---", delimeter_sub="-", delimeter_main="--"
):
    perm = list(itertools.permutations(matches, len(matches)))
    names_list = []
    for matches in perm:
        dic = {i: [] for i in matches}
        for s in strings:
            for m in matches:
                if s.startswith(m):
                    # will overwrite strings with same name
                    dic[m].append(s[len(m) :])
                    break
        name = get_reduced_string(
            dic,
            delimeter_pre=delimeter_pre,
            delimeter_sub=delimeter_sub,
            delimeter_main=delimeter_main,
        )
        names_list.append(name)
    return sorted(names_list, key=len)[0]


def get_reduced_string(
    dic,
    delimeter_match="MM",
    delimeter_pre="---",
    delimeter_sub="-",
    delimeter_main="--",
):
    s = []
    for k, v in dic.copy().items():
        if len(v) > 0:
            v = [delimeter_match if x == "" else x for x in v]
            s.append(k + delimeter_pre + delimeter_sub.join(v))
    return delimeter_main.join(s)


def character_index(string, match, latex=True):
    def reduce_string_latex(string):
        string = string.replace("\\", "")
        string = string.replace("{", "")
        string = string.replace("}", "")
        return string

    if latex:
        string = reduce_string_latex(string)
        match = reduce_string_latex(match)
    return string.index(match)


def align_texts(texts, align_at, latex=True):
    if latex:
        delimeter = "\\ "
    else:
        delimeter = " "
    import auxiliary_functions as aux

    for a in aux.makeIterableArbitrary(align_at, 1):
        if a is not None:
            if all([a in t for t in texts]):
                current_position = [character_index(t, a) for t in texts]
                new_position = max(current_position)
                texts = [
                    t[0:c] + delimeter * (new_position - c) + t[c:]
                    for c, t in zip(current_position, texts)
                ]
    return texts
