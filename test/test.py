import unittest
from fractions import Fraction
# from my_sum import sum
from AxesGeneration import getAxesList


class TestSum(unittest.TestCase):
    def test_list_int(self):
        """
        Test that it can sum a list of integers
        """
        data = [1, 2, 3]
        result = sum(data)
        self.assertEqual(result, 6)

    def test_list_fraction(self):
        """
        Test that it can sum a list of fractions
        """
        data = [Fraction(1, 4), Fraction(1, 4), Fraction(2, 4)]
        result = sum(data)
        self.assertEqual(result, 1)

    def test_bad_type(self):
        data = "banana"
        with self.assertRaises(TypeError):
            result = sum(data)


class TestAxes(unittest.TestCase):
    def test_axes_proportions(self):
        """
        Test that generated axes have desired ratio up to certain precision
        """
        height = 12
        width = 7
        data_shape_plots = [[[height, width]] * 5] * 3
        fig, axes, axes_colorbar, axes_colorbar_above = getAxesList(data_shape_plots)
        ax = axes[0][0]
        bbox = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        bwidth, bheight = bbox.width, bbox.height
        self.assertAlmostEqual(height/width, bheight/bwidth, places=3)


if __name__ == '__main__':
    unittest.main()