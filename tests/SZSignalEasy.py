import matplotlib as mpl

# font = {'family' : 'sans-serif',
#        'serif': ['Helvetica']}#,
#        'size'   : fontsize}
markersize = 1.6
# fontsize = 30
fontsize = 8
# markersize = 2.8
font = {"family": "serif", "serif": ["Arial"], "size": fontsize}
ticks_font = {"family": "sans-serif", "serif": ["Helvetica"]}  # ,
mpl.rc("text", usetex=True)
# mpl.rcParams['text.latex.unicode'] = True
# mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rcParams["text.latex.preamble"] = [
    r"\usepackage{amsmath,bm}\newcommand{\sbf}[1]{\textsf{\textbf{#1}}}"
]
# mpl.rc('text.latex',preamble=r'\usepackage{cmbright}')
mpl.rc("font", **font)
mpl.use("Agg")
import arepo
import auxiliary_functions as aux
import FigureMove as Fig
import numpy as np
import matplotlib.pyplot as plt

INFO = {}
FakeBubble = True
EmptyBubble = False

Colors = ["#516017", "#000911", "#003865", "#ffd03e", "#de1c24", "#b3fedf"]
numthreads = 16

res = 512
snapNum = 19

# snap = Fig.quickImport(snapNum)
# snap = aux.RotatePositions(snap,[snap.BoxSize/2.]*3,np.pi/3.,[0,0,1])
box = 1.0
filename = "snapNum%i_" % snapNum

fig = plt.figure()
ax = plt.gca()

for angleIndex, angle in enumerate(
    [0, 1.0 / 9 * np.pi, 2.0 / 9 * np.pi, np.pi / 3.0, 7 / 18.0 * np.pi]
):  # np.pi*5/18,,4/9.*np.pi
    snap = Fig.quickImport(snapNum)
    snap = aux.RotatePositions(snap, [snap.BoxSize / 2.0] * 3, angle, [0, 0, 1])
    SZ = snap.u * snap.part0.rho
    color = Colors[angleIndex]
    filename += "%sdeg_" % aux.ReplaceDotInName(angle / np.pi * 180.0, "%.0f")
    print filename
    for numberIndex, num in enumerate([0, 1, 2]):
        label = "%.0f deg " % (angle / np.pi * 180.0)
        if num == 0:  # empty bubble
            label += "empty bubble "
            if angleIndex == 0:
                filename += "emptybubble_"
            mask = snap.Jet_Tracer < 1e-3
            snap.addField("emptybubble", [1, 0, 0, 0, 0, 0])
            snap.emptybubble[:] = 0
            snap.emptybubble[mask] = SZ[mask]
            gridEmpty = snap.get_Agrid(
                snap.emptybubble,
                center=[snap.BoxSize / 2.0] * 3,
                numthreads=numthreads,
                res=res,
                box=box,
            )
            summed = np.sum(gridEmpty["grid"], axis=1)
            tickStyle = "--"
        elif num == 1:  # fakebubble
            label += "empty fake bubble "
            if angleIndex == 0:
                filename += "EmptyFakeBubble_"
            centerVal = 0.5 / 0.67
            INFO["FaBubRadiusU"] = [0.114, 0.086, 0.086]
            LowerValu = 0.08  # 114
            INFO["FaBubRadiusU"] = [0.104, 0.086, 0.086]
            LowerValu = 0.097
            LowerValu = 0.08  # 114
            INFO["FaBubCenterU"] = [
                centerVal + LowerValu + INFO["FaBubRadiusU"][0],
                centerVal,
                centerVal,
            ]
            INFO["FaBubCenterD"] = INFO["FaBubCenterU"]
            INFO["FaBubRadiusD"] = INFO["FaBubRadiusU"]
            bub = aux.getFakeBubble(snap, INFO=INFO)
            mask = np.invert(bub)
            snap.addField("emptybubbleFake", [1, 0, 0, 0, 0, 0])
            snap.emptybubbleFake[:] = 0
            snap.emptybubbleFake[mask] = SZ[mask]
            gridEmptyFake = snap.get_Agrid(
                snap.emptybubbleFake,
                center=[snap.BoxSize / 2.0] * 3,
                numthreads=numthreads,
                res=res,
                box=box,
            )
            summed = np.sum(gridEmptyFake["grid"], axis=1)
            tickStyle = "o"
        elif num == 2:
            label += "fid "
            if angleIndex == 0:
                filename += "Fid_"
            grid = snap.get_Agrid(
                SZ,
                center=[snap.BoxSize / 2.0] * 3,
                numthreads=numthreads,
                res=res,
                box=box,
            )
            summed = np.sum(grid["grid"], axis=1)
            tickStyle = "-"
        # try:
        #    ax=plt.gca()
        # except:
        ax.plot(
            np.linspace(0, box / 2.0 * 1e3, res / 2),
            summed[res / 2 :, res / 2 : res / 2 + 1],
            tickStyle,
            color=color,
            label="%s" % label,
            linewidth=markersize,
            markersize=markersize,
        )
        offset = 20
        # ax.plot(np.linspace(0,box/2.*1e3,res/2), summed[res/2:,res/2+offset:res/2+1+offset],label='%s offset +%.1f kpc'%(label,offset*1/0.67/res*1e3))
        # ax.plot(np.linspace(0,box/2.*1e3,res/2), summed[res/2:,res/2-offset:res/2+1-offset],label='%s offset -%.1f kpc'%(label,offset*1/0.67/res*1e3))
ax.legend()
savename = "figures/Thresh%s.pdf" % filename
aux.create_dirs_for_file(savename)
aux.checkExistence_delete_file(savename)
fig.savefig(savename, dpi=350)
# plt.show()
