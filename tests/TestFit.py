import scipy.optimize as optimize
import numpy as np
import arepo
import FigureMove as Fig
import auxiliary_functions as aux

snap = Fig.quickImport(18)

ICM = snap.part0.Jet_Tracer < 1e-3
BoxSize = 0.01
center = (snap.part0.pos[:, 0] - 0.5 / 0.67) ** 2 / 0.4 ** 2 + (
    snap.part0.pos[:, 1] - 0.5 / 0.67
) ** 2 / 0.3 ** 2 + (snap.part0.pos[:, 2] - 0.5 / 0.67) ** 2 / 0.3 ** 2 < 1


def triaxP(r, r1, r2, r3, beta, P0):
    x1 = r[:, 0]
    x2 = r[:, 1]
    x3 = r[:, 2]
    return P0 * (1 + (x1 / r1) ** 2 + (x2 / r2) ** 2 + (x3 / r3) ** 2) ** (
        -3 * beta / 2.0
    )


r = snap.part0.pos[:, :] - 0.5 / 0.67
rMasked = r[np.logical_and(ICM, center)]
p = aux.get_ElectronNumberDensity(
    snap, considering_h=True, no_conversion=False
)  # aux.get_ThermalPressure(snap, considering_h = True, no_conversion = False)
pMasked = p[np.logical_and(ICM, center)]


# guess = (0.1,0.1,0.1,4.,1e-9)
guess = (0.1, 0.1, 0.1, 1.0, 1e-2)

params, pcov = optimize.curve_fit(triaxP, rMasked, pMasked, guess)
print params
print pcov
A = np.array([(19, 20, 24), (10, 40, 28), (10, 50, 31)])


def func(data, a, b):
    return data[:, 0] * data[:, 1] * a + b


guess = (1, 1)
params, pcov = optimize.curve_fit(func, A[:, :2], A[:, 2], guess)
print (params)
