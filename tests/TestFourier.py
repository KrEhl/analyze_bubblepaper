import FigureMove as Fig
import FourierClass as FC
import matplotlib.pyplot as plt
import TheorySnapClass as TS

folder = "../../TestSim/R7E45X5M1ASFCMy15/"
folder = "/home/kristian/analysis/TestSim/R7E45X5M1ASFCMy15/"
folder = "../../SnapsTest/PerseusCorrect/R7/E45/X5M1ASFCMy15Ver2020Up10p3p2020/output/"

# snap = Fig.quickImport(0, folder=folder)

n = 22
box = 1.0

# snapTheo = TS.TheorySnapShot(n=n, boxsize=box, spaceDim=3)
time, folder, useTime = 50, folder, True
snap = (time, folder, useTime)

loadPlotData = False
Xrange = [None, None]  # [1e0, 1e2]
Yrange = [None, None]
plotVariableInKspace = True
INFO = {"RMmap_resDepth": 64}
# Create a figure
fig, ax = plt.subplots()
# Create a subplot
# ftTheo = FC.FTObj(spaceDim=3, fieldDim=1, n=n, delta=box/n)
# ftTheo.plotFT(coordinate='kAbs', snap=None, variable='MagneticFieldTurbulent1D', ax=ax, savePlot=True, initializeVariableInKspace=True, loadPlotData=loadPlotData, Xrange=Xrange, plotVariableInKspace=plotVariableInKspace,
# 	xlog=True, ylog=True, color='purple')
ft = FC.FTObj(spaceDim=2, fieldDim=1, n=n, delta=box / n)
# ft.plotFT(coordinate='kAbs', snap=None, variable='MagneticFieldTurbulent1D', ax=ax, savePlot=False, initializeVariableInKspace=False, loadPlotData=loadPlotData, Xrange=Xrange, plotVariableInKspace=plotVariableInKspace,
# 	xlog=True, ylog=True, color='navy', savePlotFilename='1.pdf', Yrange=Yrange)
# ft = FC.FTObj(spaceDim=3, fieldDim=1, n=n, delta=box/n)
ax.set_title("SqrtAbsoluteConstantMagneticFieldEnergyDensity")
ft.plotFT(
    coordinate="kAbs",
    snap=snap,
    fieldType="RMmap",
    variable="SqrtAbsoluteConstantMagneticFieldEnergyDensity",
    ax=ax,
    savePlot=True,
    initializeVariableInKspace=False,
    loadPlotData=loadPlotData,
    Xrange=Xrange,
    plotVariableInKspace=plotVariableInKspace,
    xlog=True,
    ylog=True,
    color="grey",
    savePlotFilename="2.pdf",
    Yrange=Yrange,
    label="theo",
    INFO=INFO,
)
ft.plotField()

# ft = FC.FTObj(spaceDim=3, fieldDim=1, n=n, delta=box/n)
# ft.plotFT(coordinate='kAbs', snap=snap, variable='SqrtAbsoluteConstantMagneticFieldEnergyDensity', ax=ax, savePlot=True, initializeVariableInKspace=False, loadPlotData=loadPlotData, Xrange=Xrange, plotVariableInKspace=plotVariableInKspace,
# 	xlog=True, ylog=True, color='pink', savePlotFilename='testThis.pdf', Yrange=Yrange, showlegend=1, label='snap')
# ft = FC.FTObj(spaceDim=3, fieldDim=1, n=n, delta=box/n)
# ft.plotFT(coordinate='kAbs', snap=snap, variable='MagneticFieldY', ax=ax, savePlot=True, initializeVariableInKspace=False, loadPlotData=loadPlotData, Xrange=Xrange, plotVariableInKspace=plotVariableInKspace,
# 	xlog=True, ylog=True, color='pink', savePlotFilename='testThis.pdf', Yrange=Yrange, showlegend=1, label='snap')
