import matplotlib as mpl

fontsize = 8  # 28
markersize = 4 * fontsize / 22.0
linewidth = 0.6 * markersize
font = {"family": "serif", "serif": ["Arial"], "size": fontsize}
ticks_font = {"family": "sans-serif", "serif": ["Helvetica"]}  # ,
mpl.rc("text", usetex=True)
mpl.rcParams["text.latex.preamble"] = [
    r"\usepackage{amsmath,bm}\newcommand{\sbf}[1]{\textsf{\textbf{#1}}}"
]
mpl.rc("font", **font)
import numpy as np
import FigureMove as Fig
import auxiliary_functions as aux
import matplotlib.pyplot as plt

number = 251
number = 500
number = 310
snap = Fig.quickImport(
    number,
    # folder="/home/kristian/analysis/TestSim/CoolRuns/M15/R5/08_cooling_jet_CCphases_RQEff1M4_BHhsml2kpc/output",
    folder="/home/kristian/analysis/TestSim/CoolRuns/Perseus/R6/13_cooling_jet_mhd_Ep1Et1_BHhsml2kpc_NgbDif4_XB0p01_Lmin30/output/",
)
snap.data["ElectronNumberDensity"] = aux.get_value("ElectronNumberDensity", snap)
snap.data["Temperature"] = aux.get_value("Temperature", snap)
X, Y, H = Fig.PhaseDiagram(
    snap,
    "ElectronNumberDensity",
    "Temperature",
    "Mass",
    cmap="tab20c",
    colormap_log=1,
    Xbins=200,
    Ybins=200,
    select="StarFormationRate>0",
    return_fields="center",
    logX=False,
    logY=False,
)
print(snap.data["ElectronNumberDensity"] / aux.get_value("Density", snap))
mask = snap.sfr > 0
f = lambda a, b, c, x: a * (1 - (np.exp(-(x / b))) ** (c))
f = lambda a, b, c, x: a * np.exp(-b * np.exp(-c * x))
use_log_fit = False
flog = lambda a, b, c, d, x: a * x + (b + d * x) * np.log(x + 1) ** c
p0log = [12, 0.01, 2.1, 0]
flog = lambda a, b, c, x: a * x + (b) * np.log(x + 1) ** c
p0log = [0.1, 40, 5]
flog = lambda a, b, c, x: a - b * np.exp(-c * x)
# p0s = [[10], [0.2, 2, 20], [0.1, 1, 10]]
f = lambda a, b, c, d, e, x: a * (x ** b + c * x ** d) ** e
f = lambda a, b, c, d, e, x: a * (b + c * x ** d) ** e
# f = lambda a, b, c, d, e, f, x: a*(1+(x/b)**2)**c + d*(1+(x/e)**2)**f
factor = 1.1
p0 = [100000.0, 0.1, 100.0, 1, 0.3]
p0 = [23.7783, 0.236819, 35.721, 0.236819, 2.28061]
p0 = [79.709, -64.096, 83.306, 0.063278, 2.3570]
p0s = [[10000], [1], [100], [1, 4], [0.5]]
# p0 = [6e5, 3, 0.3]
# p0log = [3, 4, 2]
# p0log = [12, 0.01, 2.1, 0]

# f = lambda a, b, c, d, x: a*(d + x + np.log10(b*(x+1)+1))**c
# p0 = [379689.03463204, 5571.43818088, 1114.34620943]
# p0 = [1e6, 1e3, 1e3]
# p0 = [7e2, 2, 1, 1]
# f = lambda a, b, x: a*np.log10(x+1)**b
# p0 = [1e4, 5]
# bounds = (0, [1e6, 1e1, 1e1])
# bounds = (None, None)
bounds = None


def objective(x, a, b, c, d, e, f):
    return (a * x) + (b * x ** 2) + (c * x ** 3) + (d * x ** 4) + (e * x ** 5) + f


# p0 = None
# f = lambda a, b, c, d, e, f, x: objective(x, a, b, c, d, e, f)
# f = lambda a, b, c, x: a*(x/b)**c
# p0 = [1e5, 1, 0.2]
x, y = X[H != 0], Y[H != 0]
unq, mask = np.unique(np.round(x, 4), return_index=True)
mask = slice(None)
print(mask)
x = x[mask]
y = y[mask]
fig, axes = plt.subplots(3, 2)
print(f"x: {x}")
print(f"y: {y}")
st = ""
for xs, ys in zip(x, y):
    st += "{%g,%g}," % (xs, ys)
print(st)
print(r"{%g,%g},")
print(f"np.log(y) {np.log(y)}")
if use_log_fit:
    y = np.log(y)
para, perr = aux.fitFunction(x, y, p0, f, method="curve_fit", maxfev=int(1e8))
print("params")
print("-----")
print(para)
print("-----")
axes[0][0].plot(x, y, label="data")
axes[0][0].set_xscale("log")
axes[0][0].set_yscale("log")
y_fit = f(*para, x)
if use_log_fit:
    y_fit = np.exp(y_fit)
y_fit *= factor
axes[0][0].plot(x, y_fit, label="fit")
axes[0][0].legend()
# plt.ylim([min([np.nanmin(y), np.nanmin(y_fit)]), max([np.nanmax(y), np.nanmax(y_fit)])])
axes[1][0].plot(x, y_fit, label="fit")
axes[1][0].legend()
# axes[0][1].plot(x, np.log(y), label="log data")
# axes[0][1].legend()
ax = axes[1][1]
ax.plot(x, f(*p0, x), label=f"initial parameters\n {p0}")
ax.set_xscale("log")
ax.set_yscale("log")
ax.legend()
ax = axes[2][0]
for ind, pp in enumerate(p0s):
    for p in pp:
        paras = [x if i != ind else p for i, x in enumerate(p0)]
        ax.plot(x, f(*paras, x), label=f"{paras}")
ax.set_xscale("log")
ax.set_yscale("log")
ax.legend()
ax = axes[2][1]
ax.plot(x, y, label="data")
ax.set_xscale("log")
ax.set_yscale("log")
ax.legend()
# plt.subplots_adjust(hspace=0.3)
print(".. saving")
fig.savefig("fit_eq_state_arepo.pdf")
