import lic
import matplotlib.pyplot as plt
import numpy as np

x = 10 ** np.linspace(-3, 3, 120000) * -1
xx = np.sin(x) * np.cos(x) + x
yy = x ** 2
Nskip = 1
xx = np.load("x.npy")[::Nskip, ::Nskip]
yy = np.load("y.npy")[::Nskip, ::Nskip]

# lic_result = lic.lic(xx.reshape(300, 400), yy.reshape(300, 400), length=30)
lic_result = lic.lic(xx, yy, length=30)
x = np.linspace(2500, 3000, xx.shape[0])
y = np.linspace(2500, 3000, yy.shape[1])
plt.pcolormesh(
    x, y, np.sqrt(xx ** 2 + yy ** 2), cmap="gray"
)  # , origin='lower', extent=[2500,3000,2500,3000])
im = plt.imshow(
    lic_result, origin="lower", cmap="Blues", alpha=0.2, extent=[2500, 3000, 2500, 3000]
)
plt.colorbar(im)

plt.show()
