import time
import re
import argparse_helpers
import argparse
import os
import numpy as np
import auxiliary_functions as aux
import FigureMove as Fig


def list_missing_files(folder):
    files = os.listdir(folder)
    nums = []
    if len(files) == 0:
        print(f"no files found in {folder}!")
        return
    for f in files:
        if f.endswith("pdf"):
            nums.append(int(re.findall("Nu([\d]+)_", f)[0]))
    nums = np.array(nums, dtype=int)
    num_min = np.min(nums)
    num_max = np.max(nums)
    spacing = int(np.min(np.diff(np.sort(nums))))
    potential_numbers = np.arange(num_min, num_max, spacing)
    missing = [x for x in potential_numbers if x not in nums]
    print(f"min numbers: {num_min}")
    print(f"max numbers: {num_max}")
    print(f"found in total {len(nums)}")
    print(f"missing in total {len(missing)}")
    with aux.print_array_on_one_line():
        print(f"missing numbers: {np.array(missing)}")
    return


def suggest_plots(folder, number_range, differenceTime=16):
    numbers, times = Fig.load_time_file(folder).T
    MIN, MAX, NUMS = number_range.split(":")
    if not MIN:
        MIN = 0
    if not MAX:
        MAX = -1
    if not NUMS:
        raise Exception(
            f"how many plots required? NUMS: {NUMS} (MIN, MAX, NUMS: {MIN, MAX, NUMS})"
        )
    if MAX == -1:
        MAX = np.max(times)
    STEP = (np.max(times) - np.min(times)) / (len(times) - 1)
    STEP = round(STEP)
    print(numbers, times)
    print(f"STEP, {STEP}")
    NUMS = int(NUMS)
    MIN, MAX = [float(x) for x in (MIN, MAX)]
    print(f"NUMS {NUMS}")
    MIN_number = Fig.getSnapNumber(folder, MIN, differenceTime=differenceTime)
    MAX_number = Fig.getSnapNumber(folder, MAX, differenceTime=differenceTime)
    nums = (round(x) for x in np.linspace(MIN_number, MAX_number, NUMS))
    times_to_plot = []
    for n in nums:
        times_to_plot.append(times[numbers == float(n)][0])
    print(f"times_to_plot {times_to_plot}")
    print(f"{times_to_plot[0]}:{times_to_plot[-1]}:{np.diff(times_to_plot)}")
    return


if __name__ == "__main__":
    start_time = time.time()
    parser = argparse.ArgumentParser(
        description="what's missing folder",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        f"--folder", type=argparse_helpers.fancy_eval, nargs=None, default="./"
    )
    parser.add_argument(
        f"--setup",
        type=argparse_helpers.fancy_eval,
        nargs=None,
        default="missing_numbers",
    )
    input = parser.parse_args().__dict__.copy()
    input = {k: v for k, v in input.items() if v is not None}
    folder = input["folder"]
    setup = input["setup"]
    if setup == "missing_numbers":
        list_missing_files(folder)
    elif "suggest" in setup:
        suggest_plots(folder, setup.replace("suggest", ""))
    else:
        raise Exception(f"setup: {setup} not understood!")
